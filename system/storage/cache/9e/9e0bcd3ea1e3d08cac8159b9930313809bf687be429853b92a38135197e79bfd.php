<?php

/* extension/module/oxy_theme_design.twig */
class __TwigTemplate_5c9aab986ffec3cdd1ee93404549e345f02eeb7ad4b9646835f3b4b077a735fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
                    
";
        // line 3
        if (twig_test_empty((isset($context["t1d_body_bg_color"]) ? $context["t1d_body_bg_color"] : null))) {
            $context["t1d_body_bg_color"] = "F6F6F6";
        }
        // line 4
        if (twig_test_empty((isset($context["t1d_headings_color"]) ? $context["t1d_headings_color"] : null))) {
            $context["t1d_headings_color"] = "424242";
        }
        // line 5
        if (twig_test_empty((isset($context["t1d_headings_border_color"]) ? $context["t1d_headings_border_color"] : null))) {
            $context["t1d_headings_border_color"] = "F3F3F3";
        }
        // line 6
        if (twig_test_empty((isset($context["t1d_body_text_color"]) ? $context["t1d_body_text_color"] : null))) {
            $context["t1d_body_text_color"] = "424242";
        }
        // line 7
        if (twig_test_empty((isset($context["t1d_light_text_color"]) ? $context["t1d_light_text_color"] : null))) {
            $context["t1d_light_text_color"] = "B6B6B6";
        }
        // line 8
        if (twig_test_empty((isset($context["t1d_other_links_color"]) ? $context["t1d_other_links_color"] : null))) {
            $context["t1d_other_links_color"] = "424242";
        }
        // line 9
        if (twig_test_empty((isset($context["t1d_links_hover_color"]) ? $context["t1d_links_hover_color"] : null))) {
            $context["t1d_links_hover_color"] = "3EACEA";
        }
        // line 10
        if (twig_test_empty((isset($context["t1d_icons_color"]) ? $context["t1d_icons_color"] : null))) {
            $context["t1d_icons_color"] = "424242";
        }
        // line 11
        if (twig_test_empty((isset($context["t1d_icons_hover_color"]) ? $context["t1d_icons_hover_color"] : null))) {
            $context["t1d_icons_hover_color"] = "3EACEA";
        }
        // line 12
        echo "
";
        // line 13
        if (twig_test_empty((isset($context["t1d_wrapper_frame_bg_color"]) ? $context["t1d_wrapper_frame_bg_color"] : null))) {
            $context["t1d_wrapper_frame_bg_color"] = "FFFFFF";
        }
        // line 14
        if (twig_test_empty((isset($context["t1d_content_column_bg_color"]) ? $context["t1d_content_column_bg_color"] : null))) {
            $context["t1d_content_column_bg_color"] = "FFFFFF";
        }
        // line 15
        if (twig_test_empty((isset($context["t1d_content_column_separator_color"]) ? $context["t1d_content_column_separator_color"] : null))) {
            $context["t1d_content_column_separator_color"] = "F3F3F3";
        }
        // line 16
        if (twig_test_empty((isset($context["t1d_content_column_hli_bg_color"]) ? $context["t1d_content_column_hli_bg_color"] : null))) {
            $context["t1d_content_column_hli_bg_color"] = "F3F3F3";
        }
        // line 17
        echo "
";
        // line 18
        if (twig_test_empty((isset($context["t1d_left_column_head_title_bg_color"]) ? $context["t1d_left_column_head_title_bg_color"] : null))) {
            $context["t1d_left_column_head_title_bg_color"] = "3EACEA";
        }
        // line 19
        if (twig_test_empty((isset($context["t1d_left_column_head_title_color"]) ? $context["t1d_left_column_head_title_color"] : null))) {
            $context["t1d_left_column_head_title_color"] = "FFFFFF";
        }
        // line 20
        if (twig_test_empty((isset($context["t1d_left_column_box_bg_color"]) ? $context["t1d_left_column_box_bg_color"] : null))) {
            $context["t1d_left_column_box_bg_color"] = "F3F3F3";
        }
        // line 21
        if (twig_test_empty((isset($context["t1d_left_column_box_text_color"]) ? $context["t1d_left_column_box_text_color"] : null))) {
            $context["t1d_left_column_box_text_color"] = "656565";
        }
        // line 22
        if (twig_test_empty((isset($context["t1d_left_column_box_links_color"]) ? $context["t1d_left_column_box_links_color"] : null))) {
            $context["t1d_left_column_box_links_color"] = "424242";
        }
        // line 23
        if (twig_test_empty((isset($context["t1d_left_column_box_links_color_hover"]) ? $context["t1d_left_column_box_links_color_hover"] : null))) {
            $context["t1d_left_column_box_links_color_hover"] = "3EACEA";
        }
        // line 24
        if (twig_test_empty((isset($context["t1d_left_column_box_separator_color"]) ? $context["t1d_left_column_box_separator_color"] : null))) {
            $context["t1d_left_column_box_separator_color"] = "F3F3F3";
        }
        // line 25
        echo "
";
        // line 26
        if (twig_test_empty((isset($context["t1d_right_column_head_title_bg_color"]) ? $context["t1d_right_column_head_title_bg_color"] : null))) {
            $context["t1d_right_column_head_title_bg_color"] = "3EACEA";
        }
        // line 27
        if (twig_test_empty((isset($context["t1d_right_column_head_title_color"]) ? $context["t1d_right_column_head_title_color"] : null))) {
            $context["t1d_right_column_head_title_color"] = "FFFFFF";
        }
        // line 28
        if (twig_test_empty((isset($context["t1d_right_column_box_bg_color"]) ? $context["t1d_right_column_box_bg_color"] : null))) {
            $context["t1d_right_column_box_bg_color"] = "F3F3F3";
        }
        // line 29
        if (twig_test_empty((isset($context["t1d_right_column_box_text_color"]) ? $context["t1d_right_column_box_text_color"] : null))) {
            $context["t1d_right_column_box_text_color"] = "656565";
        }
        // line 30
        if (twig_test_empty((isset($context["t1d_right_column_box_links_color"]) ? $context["t1d_right_column_box_links_color"] : null))) {
            $context["t1d_right_column_box_links_color"] = "424242";
        }
        // line 31
        if (twig_test_empty((isset($context["t1d_right_column_box_links_color_hover"]) ? $context["t1d_right_column_box_links_color_hover"] : null))) {
            $context["t1d_right_column_box_links_color_hover"] = "3EACEA";
        }
        // line 32
        if (twig_test_empty((isset($context["t1d_right_column_box_separator_color"]) ? $context["t1d_right_column_box_separator_color"] : null))) {
            $context["t1d_right_column_box_separator_color"] = "F3F3F3";
        }
        // line 33
        echo "
";
        // line 34
        if (twig_test_empty((isset($context["t1d_category_box_head_title_bg_color"]) ? $context["t1d_category_box_head_title_bg_color"] : null))) {
            $context["t1d_category_box_head_title_bg_color"] = "F1494B";
        }
        // line 35
        if (twig_test_empty((isset($context["t1d_category_box_head_title_color"]) ? $context["t1d_category_box_head_title_color"] : null))) {
            $context["t1d_category_box_head_title_color"] = "FFFFFF";
        }
        // line 36
        if (twig_test_empty((isset($context["t1d_category_box_box_bg_color"]) ? $context["t1d_category_box_box_bg_color"] : null))) {
            $context["t1d_category_box_box_bg_color"] = "F3F3F3";
        }
        // line 37
        if (twig_test_empty((isset($context["t1d_category_box_box_links_color"]) ? $context["t1d_category_box_box_links_color"] : null))) {
            $context["t1d_category_box_box_links_color"] = "424242";
        }
        // line 38
        if (twig_test_empty((isset($context["t1d_category_box_box_links_color_hover"]) ? $context["t1d_category_box_box_links_color_hover"] : null))) {
            $context["t1d_category_box_box_links_color_hover"] = "3EACEA";
        }
        // line 39
        if (twig_test_empty((isset($context["t1d_category_box_box_subcat_color"]) ? $context["t1d_category_box_box_subcat_color"] : null))) {
            $context["t1d_category_box_box_subcat_color"] = "666666";
        }
        // line 40
        if (twig_test_empty((isset($context["t1d_category_box_box_separator_color"]) ? $context["t1d_category_box_box_separator_color"] : null))) {
            $context["t1d_category_box_box_separator_color"] = "FFFFFF";
        }
        // line 41
        echo "
";
        // line 42
        if (twig_test_empty((isset($context["t1d_filter_box_head_title_bg_color"]) ? $context["t1d_filter_box_head_title_bg_color"] : null))) {
            $context["t1d_filter_box_head_title_bg_color"] = "424242";
        }
        // line 43
        if (twig_test_empty((isset($context["t1d_filter_box_head_title_color"]) ? $context["t1d_filter_box_head_title_color"] : null))) {
            $context["t1d_filter_box_head_title_color"] = "FFFFFF";
        }
        // line 44
        if (twig_test_empty((isset($context["t1d_filter_box_box_bg_color"]) ? $context["t1d_filter_box_box_bg_color"] : null))) {
            $context["t1d_filter_box_box_bg_color"] = "F3F3F3";
        }
        // line 45
        if (twig_test_empty((isset($context["t1d_filter_box_box_filter_title_color"]) ? $context["t1d_filter_box_box_filter_title_color"] : null))) {
            $context["t1d_filter_box_box_filter_title_color"] = "424242";
        }
        // line 46
        if (twig_test_empty((isset($context["t1d_filter_box_box_filter_name_color"]) ? $context["t1d_filter_box_box_filter_name_color"] : null))) {
            $context["t1d_filter_box_box_filter_name_color"] = "424242";
        }
        // line 47
        if (twig_test_empty((isset($context["t1d_filter_box_box_filter_name_color_hover"]) ? $context["t1d_filter_box_box_filter_name_color_hover"] : null))) {
            $context["t1d_filter_box_box_filter_name_color_hover"] = "3EACEA";
        }
        // line 48
        if (twig_test_empty((isset($context["t1d_filter_box_box_separator_color"]) ? $context["t1d_filter_box_box_separator_color"] : null))) {
            $context["t1d_filter_box_box_separator_color"] = "F3F3F3";
        }
        // line 49
        echo "
";
        // line 50
        if (twig_test_empty((isset($context["t1d_price_color"]) ? $context["t1d_price_color"] : null))) {
            $context["t1d_price_color"] = "424242";
        }
        // line 51
        if (twig_test_empty((isset($context["t1d_price_old_color"]) ? $context["t1d_price_old_color"] : null))) {
            $context["t1d_price_old_color"] = "B6B6B6";
        }
        // line 52
        if (twig_test_empty((isset($context["t1d_price_new_color"]) ? $context["t1d_price_new_color"] : null))) {
            $context["t1d_price_new_color"] = "F1494B";
        }
        // line 53
        echo "
";
        // line 54
        if (twig_test_empty((isset($context["t1d_button_bg_color"]) ? $context["t1d_button_bg_color"] : null))) {
            $context["t1d_button_bg_color"] = "F3F3F3";
        }
        // line 55
        if (twig_test_empty((isset($context["t1d_button_bg_hover_color"]) ? $context["t1d_button_bg_hover_color"] : null))) {
            $context["t1d_button_bg_hover_color"] = "3EACEA";
        }
        // line 56
        if (twig_test_empty((isset($context["t1d_button_text_color"]) ? $context["t1d_button_text_color"] : null))) {
            $context["t1d_button_text_color"] = "424242";
        }
        // line 57
        if (twig_test_empty((isset($context["t1d_button_text_hover_color"]) ? $context["t1d_button_text_hover_color"] : null))) {
            $context["t1d_button_text_hover_color"] = "FFFFFF";
        }
        // line 58
        if (twig_test_empty((isset($context["t1d_button_exclusive_bg_color"]) ? $context["t1d_button_exclusive_bg_color"] : null))) {
            $context["t1d_button_exclusive_bg_color"] = "3EACEA";
        }
        // line 59
        if (twig_test_empty((isset($context["t1d_button_exclusive_bg_hover_color"]) ? $context["t1d_button_exclusive_bg_hover_color"] : null))) {
            $context["t1d_button_exclusive_bg_hover_color"] = "3EACEA";
        }
        // line 60
        if (twig_test_empty((isset($context["t1d_button_exclusive_text_color"]) ? $context["t1d_button_exclusive_text_color"] : null))) {
            $context["t1d_button_exclusive_text_color"] = "FFFFFF";
        }
        // line 61
        if (twig_test_empty((isset($context["t1d_button_exclusive_text_hover_color"]) ? $context["t1d_button_exclusive_text_hover_color"] : null))) {
            $context["t1d_button_exclusive_text_hover_color"] = "FFFFFF";
        }
        // line 62
        echo "
";
        // line 63
        if (twig_test_empty((isset($context["t1d_dd_bg_color"]) ? $context["t1d_dd_bg_color"] : null))) {
            $context["t1d_dd_bg_color"] = "FFFFFF";
        }
        // line 64
        if (twig_test_empty((isset($context["t1d_dd_headings_color"]) ? $context["t1d_dd_headings_color"] : null))) {
            $context["t1d_dd_headings_color"] = "424242";
        }
        // line 65
        if (twig_test_empty((isset($context["t1d_dd_text_color"]) ? $context["t1d_dd_text_color"] : null))) {
            $context["t1d_dd_text_color"] = "656565";
        }
        // line 66
        if (twig_test_empty((isset($context["t1d_dd_light_text_color"]) ? $context["t1d_dd_light_text_color"] : null))) {
            $context["t1d_dd_light_text_color"] = "999999";
        }
        // line 67
        if (twig_test_empty((isset($context["t1d_dd_links_color"]) ? $context["t1d_dd_links_color"] : null))) {
            $context["t1d_dd_links_color"] = "999999";
        }
        // line 68
        if (twig_test_empty((isset($context["t1d_dd_links_hover_color"]) ? $context["t1d_dd_links_hover_color"] : null))) {
            $context["t1d_dd_links_hover_color"] = "3EACEA";
        }
        // line 69
        if (twig_test_empty((isset($context["t1d_dd_icons_color"]) ? $context["t1d_dd_icons_color"] : null))) {
            $context["t1d_dd_icons_color"] = "EBEBEB";
        }
        // line 70
        if (twig_test_empty((isset($context["t1d_dd_icons_hover_color"]) ? $context["t1d_dd_icons_hover_color"] : null))) {
            $context["t1d_dd_icons_hover_color"] = "3EACEA";
        }
        // line 71
        if (twig_test_empty((isset($context["t1d_dd_hli_bg_color"]) ? $context["t1d_dd_hli_bg_color"] : null))) {
            $context["t1d_dd_hli_bg_color"] = "F7F7F7";
        }
        // line 72
        if (twig_test_empty((isset($context["t1d_dd_separator_color"]) ? $context["t1d_dd_separator_color"] : null))) {
            $context["t1d_dd_separator_color"] = "F3F3F3";
        }
        // line 73
        echo "
";
        // line 74
        if (twig_test_empty((isset($context["t1d_top_area_bg_color"]) ? $context["t1d_top_area_bg_color"] : null))) {
            $context["t1d_top_area_bg_color"] = "FFFFFF";
        }
        // line 75
        if (twig_test_empty((isset($context["t1d_top_area_mini_bg_color"]) ? $context["t1d_top_area_mini_bg_color"] : null))) {
            $context["t1d_top_area_mini_bg_color"] = "424242";
        }
        // line 76
        if (twig_test_empty((isset($context["t1d_top_area_icons_color"]) ? $context["t1d_top_area_icons_color"] : null))) {
            $context["t1d_top_area_icons_color"] = "555555";
        }
        // line 77
        if (twig_test_empty((isset($context["t1d_top_area_icons_color_hover"]) ? $context["t1d_top_area_icons_color_hover"] : null))) {
            $context["t1d_top_area_icons_color_hover"] = "3EACEA";
        }
        // line 78
        if (twig_test_empty((isset($context["t1d_top_area_icons_separator_color"]) ? $context["t1d_top_area_icons_separator_color"] : null))) {
            $context["t1d_top_area_icons_separator_color"] = "F3F3F3";
        }
        // line 79
        if (twig_test_empty((isset($context["t1d_top_area_search_bg_color"]) ? $context["t1d_top_area_search_bg_color"] : null))) {
            $context["t1d_top_area_search_bg_color"] = "FFFFFF";
        }
        // line 80
        if (twig_test_empty((isset($context["t1d_top_area_search_border_color"]) ? $context["t1d_top_area_search_border_color"] : null))) {
            $context["t1d_top_area_search_border_color"] = "F3F3F3";
        }
        // line 81
        if (twig_test_empty((isset($context["t1d_top_area_search_color"]) ? $context["t1d_top_area_search_color"] : null))) {
            $context["t1d_top_area_search_color"] = "B6B6B6";
        }
        // line 82
        if (twig_test_empty((isset($context["t1d_top_area_search_color_active"]) ? $context["t1d_top_area_search_color_active"] : null))) {
            $context["t1d_top_area_search_color_active"] = "424242";
        }
        // line 83
        if (twig_test_empty((isset($context["t1d_top_area_search_icon_color"]) ? $context["t1d_top_area_search_icon_color"] : null))) {
            $context["t1d_top_area_search_icon_color"] = "424242";
        }
        // line 84
        echo "
";
        // line 85
        if (twig_test_empty((isset($context["t1d_top_area_tb_bg_color"]) ? $context["t1d_top_area_tb_bg_color"] : null))) {
            $context["t1d_top_area_tb_bg_color"] = "424242";
        }
        // line 86
        if (twig_test_empty((isset($context["t1d_top_area_tb_bottom_border_color"]) ? $context["t1d_top_area_tb_bottom_border_color"] : null))) {
            $context["t1d_top_area_tb_bottom_border_color"] = "424242";
        }
        // line 87
        if (twig_test_empty((isset($context["t1d_top_area_tb_text_color"]) ? $context["t1d_top_area_tb_text_color"] : null))) {
            $context["t1d_top_area_tb_text_color"] = "CCCCCC";
        }
        // line 88
        if (twig_test_empty((isset($context["t1d_top_area_tb_link_color"]) ? $context["t1d_top_area_tb_link_color"] : null))) {
            $context["t1d_top_area_tb_link_color"] = "CCCCCC";
        }
        // line 89
        if (twig_test_empty((isset($context["t1d_top_area_tb_link_color_hover"]) ? $context["t1d_top_area_tb_link_color_hover"] : null))) {
            $context["t1d_top_area_tb_link_color_hover"] = "FFFFFF";
        }
        // line 90
        if (twig_test_empty((isset($context["t1d_top_area_tb_icons_color"]) ? $context["t1d_top_area_tb_icons_color"] : null))) {
            $context["t1d_top_area_tb_icons_color"] = "FFFFFF";
        }
        // line 91
        echo "
";
        // line 92
        if (twig_test_empty((isset($context["t1d_mm_bg_color"]) ? $context["t1d_mm_bg_color"] : null))) {
            $context["t1d_mm_bg_color"] = "424242";
        }
        // line 93
        if (twig_test_empty((isset($context["t1d_mm_separator_color"]) ? $context["t1d_mm_separator_color"] : null))) {
            $context["t1d_mm_separator_color"] = "555555";
        }
        // line 94
        if (twig_test_empty((isset($context["t1d_mm_border_top_color"]) ? $context["t1d_mm_border_top_color"] : null))) {
            $context["t1d_mm_border_top_color"] = "555555";
        }
        // line 95
        if (twig_test_empty((isset($context["t1d_mm_border_bottom_color"]) ? $context["t1d_mm_border_bottom_color"] : null))) {
            $context["t1d_mm_border_bottom_color"] = "555555";
        }
        // line 96
        echo "
";
        // line 97
        if (twig_test_empty((isset($context["t1d_mm1_bg_color"]) ? $context["t1d_mm1_bg_color"] : null))) {
            $context["t1d_mm1_bg_color"] = "3EACEA";
        }
        // line 98
        if (twig_test_empty((isset($context["t1d_mm1_bg_hover_color"]) ? $context["t1d_mm1_bg_hover_color"] : null))) {
            $context["t1d_mm1_bg_hover_color"] = "3EACEA";
        }
        // line 99
        if (twig_test_empty((isset($context["t1d_mm1_link_color"]) ? $context["t1d_mm1_link_color"] : null))) {
            $context["t1d_mm1_link_color"] = "FFFFFF";
        }
        // line 100
        if (twig_test_empty((isset($context["t1d_mm1_link_hover_color"]) ? $context["t1d_mm1_link_hover_color"] : null))) {
            $context["t1d_mm1_link_hover_color"] = "FFFFFF";
        }
        // line 101
        echo "
";
        // line 102
        if (twig_test_empty((isset($context["t1d_mm2_bg_color"]) ? $context["t1d_mm2_bg_color"] : null))) {
            $context["t1d_mm2_bg_color"] = "F1494B";
        }
        // line 103
        if (twig_test_empty((isset($context["t1d_mm2_bg_hover_color"]) ? $context["t1d_mm2_bg_hover_color"] : null))) {
            $context["t1d_mm2_bg_hover_color"] = "F1494B";
        }
        // line 104
        if (twig_test_empty((isset($context["t1d_mm2_link_color"]) ? $context["t1d_mm2_link_color"] : null))) {
            $context["t1d_mm2_link_color"] = "FFFFFF";
        }
        // line 105
        if (twig_test_empty((isset($context["t1d_mm2_link_hover_color"]) ? $context["t1d_mm2_link_hover_color"] : null))) {
            $context["t1d_mm2_link_hover_color"] = "FFFFFF";
        }
        // line 106
        echo "
";
        // line 107
        if (twig_test_empty((isset($context["t1d_mm3_bg_color"]) ? $context["t1d_mm3_bg_color"] : null))) {
            $context["t1d_mm3_bg_color"] = "424242";
        }
        // line 108
        if (twig_test_empty((isset($context["t1d_mm3_bg_hover_color"]) ? $context["t1d_mm3_bg_hover_color"] : null))) {
            $context["t1d_mm3_bg_hover_color"] = "424242";
        }
        // line 109
        if (twig_test_empty((isset($context["t1d_mm3_link_color"]) ? $context["t1d_mm3_link_color"] : null))) {
            $context["t1d_mm3_link_color"] = "FFFFFF";
        }
        // line 110
        if (twig_test_empty((isset($context["t1d_mm3_link_hover_color"]) ? $context["t1d_mm3_link_hover_color"] : null))) {
            $context["t1d_mm3_link_hover_color"] = "B6B6B6";
        }
        // line 111
        echo "
";
        // line 112
        if (twig_test_empty((isset($context["t1d_mm6_bg_color"]) ? $context["t1d_mm6_bg_color"] : null))) {
            $context["t1d_mm6_bg_color"] = "424242";
        }
        // line 113
        if (twig_test_empty((isset($context["t1d_mm6_bg_hover_color"]) ? $context["t1d_mm6_bg_hover_color"] : null))) {
            $context["t1d_mm6_bg_hover_color"] = "424242";
        }
        // line 114
        if (twig_test_empty((isset($context["t1d_mm6_link_color"]) ? $context["t1d_mm6_link_color"] : null))) {
            $context["t1d_mm6_link_color"] = "FFFFFF";
        }
        // line 115
        if (twig_test_empty((isset($context["t1d_mm6_link_hover_color"]) ? $context["t1d_mm6_link_hover_color"] : null))) {
            $context["t1d_mm6_link_hover_color"] = "B6B6B6";
        }
        // line 116
        echo "
";
        // line 117
        if (twig_test_empty((isset($context["t1d_mm5_bg_color"]) ? $context["t1d_mm5_bg_color"] : null))) {
            $context["t1d_mm5_bg_color"] = "424242";
        }
        // line 118
        if (twig_test_empty((isset($context["t1d_mm5_bg_hover_color"]) ? $context["t1d_mm5_bg_hover_color"] : null))) {
            $context["t1d_mm5_bg_hover_color"] = "424242";
        }
        // line 119
        if (twig_test_empty((isset($context["t1d_mm5_link_color"]) ? $context["t1d_mm5_link_color"] : null))) {
            $context["t1d_mm5_link_color"] = "FFFFFF";
        }
        // line 120
        if (twig_test_empty((isset($context["t1d_mm5_link_hover_color"]) ? $context["t1d_mm5_link_hover_color"] : null))) {
            $context["t1d_mm5_link_hover_color"] = "B6B6B6";
        }
        // line 121
        echo "
";
        // line 122
        if (twig_test_empty((isset($context["t1d_mm4_bg_color"]) ? $context["t1d_mm4_bg_color"] : null))) {
            $context["t1d_mm4_bg_color"] = "424242";
        }
        // line 123
        if (twig_test_empty((isset($context["t1d_mm4_bg_hover_color"]) ? $context["t1d_mm4_bg_hover_color"] : null))) {
            $context["t1d_mm4_bg_hover_color"] = "424242";
        }
        // line 124
        if (twig_test_empty((isset($context["t1d_mm4_link_color"]) ? $context["t1d_mm4_link_color"] : null))) {
            $context["t1d_mm4_link_color"] = "FFFFFF";
        }
        // line 125
        if (twig_test_empty((isset($context["t1d_mm4_link_hover_color"]) ? $context["t1d_mm4_link_hover_color"] : null))) {
            $context["t1d_mm4_link_hover_color"] = "B6B6B6";
        }
        // line 126
        echo "
";
        // line 127
        if (twig_test_empty((isset($context["t1d_mm_sub_bg_color"]) ? $context["t1d_mm_sub_bg_color"] : null))) {
            $context["t1d_mm_sub_bg_color"] = "FFFFFF";
        }
        // line 128
        if (twig_test_empty((isset($context["t1d_mm_sub_titles_bg_color"]) ? $context["t1d_mm_sub_titles_bg_color"] : null))) {
            $context["t1d_mm_sub_titles_bg_color"] = "F3F3F3";
        }
        // line 129
        if (twig_test_empty((isset($context["t1d_mm_sub_text_color"]) ? $context["t1d_mm_sub_text_color"] : null))) {
            $context["t1d_mm_sub_text_color"] = "656565";
        }
        // line 130
        if (twig_test_empty((isset($context["t1d_mm_sub_link_color"]) ? $context["t1d_mm_sub_link_color"] : null))) {
            $context["t1d_mm_sub_link_color"] = "424242";
        }
        // line 131
        if (twig_test_empty((isset($context["t1d_mm_sub_link_hover_color"]) ? $context["t1d_mm_sub_link_hover_color"] : null))) {
            $context["t1d_mm_sub_link_hover_color"] = "3EACEA";
        }
        // line 132
        if (twig_test_empty((isset($context["t1d_mm_sub_subcategory_color"]) ? $context["t1d_mm_sub_subcategory_color"] : null))) {
            $context["t1d_mm_sub_subcategory_color"] = "999999";
        }
        // line 133
        echo "
";
        // line 134
        if (twig_test_empty((isset($context["t1d_mid_prod_box_sale_icon_color"]) ? $context["t1d_mid_prod_box_sale_icon_color"] : null))) {
            $context["t1d_mid_prod_box_sale_icon_color"] = "F1494B";
        }
        // line 135
        if (twig_test_empty((isset($context["t1d_mid_prod_box_new_icon_color"]) ? $context["t1d_mid_prod_box_new_icon_color"] : null))) {
            $context["t1d_mid_prod_box_new_icon_color"] = "3EACEA";
        }
        // line 136
        if (twig_test_empty((isset($context["t1d_mid_prod_box_out_of_stock_icon_color"]) ? $context["t1d_mid_prod_box_out_of_stock_icon_color"] : null))) {
            $context["t1d_mid_prod_box_out_of_stock_icon_color"] = "FFFFFF";
        }
        // line 137
        if (twig_test_empty((isset($context["t1d_mid_prod_stars_color"]) ? $context["t1d_mid_prod_stars_color"] : null))) {
            $context["t1d_mid_prod_stars_color"] = "3EACEA";
        }
        // line 138
        echo "
";
        // line 139
        if (twig_test_empty((isset($context["t1d_f6_bg_color"]) ? $context["t1d_f6_bg_color"] : null))) {
            $context["t1d_f6_bg_color"] = "373737";
        }
        // line 140
        if (twig_test_empty((isset($context["t1d_f6_text_color"]) ? $context["t1d_f6_text_color"] : null))) {
            $context["t1d_f6_text_color"] = "FFFFFF";
        }
        // line 141
        if (twig_test_empty((isset($context["t1d_f6_link_color"]) ? $context["t1d_f6_link_color"] : null))) {
            $context["t1d_f6_link_color"] = "B6B6B6";
        }
        // line 142
        if (twig_test_empty((isset($context["t1d_f6_link_hover_color"]) ? $context["t1d_f6_link_hover_color"] : null))) {
            $context["t1d_f6_link_hover_color"] = "FFFFFF";
        }
        // line 143
        if (twig_test_empty((isset($context["t1d_f6_border_top_color"]) ? $context["t1d_f6_border_top_color"] : null))) {
            $context["t1d_f6_border_top_color"] = "2D2D2D";
        }
        // line 144
        echo "
";
        // line 145
        if (twig_test_empty((isset($context["t1d_f2_bg_color"]) ? $context["t1d_f2_bg_color"] : null))) {
            $context["t1d_f2_bg_color"] = "2F2F2F";
        }
        // line 146
        if (twig_test_empty((isset($context["t1d_f2_titles_color"]) ? $context["t1d_f2_titles_color"] : null))) {
            $context["t1d_f2_titles_color"] = "FFFFFF";
        }
        // line 147
        if (twig_test_empty((isset($context["t1d_f2_titles_border_color"]) ? $context["t1d_f2_titles_border_color"] : null))) {
            $context["t1d_f2_titles_border_color"] = "373737";
        }
        // line 148
        if (twig_test_empty((isset($context["t1d_f2_text_color"]) ? $context["t1d_f2_text_color"] : null))) {
            $context["t1d_f2_text_color"] = "6E6E6E";
        }
        // line 149
        if (twig_test_empty((isset($context["t1d_f2_link_color"]) ? $context["t1d_f2_link_color"] : null))) {
            $context["t1d_f2_link_color"] = "B6B6B6";
        }
        // line 150
        if (twig_test_empty((isset($context["t1d_f2_link_hover_color"]) ? $context["t1d_f2_link_hover_color"] : null))) {
            $context["t1d_f2_link_hover_color"] = "FFFFFF";
        }
        // line 151
        if (twig_test_empty((isset($context["t1d_f2_border_top_color"]) ? $context["t1d_f2_border_top_color"] : null))) {
            $context["t1d_f2_border_top_color"] = "373737";
        }
        // line 152
        echo "
";
        // line 153
        if (twig_test_empty((isset($context["t1d_f3_bg_color"]) ? $context["t1d_f3_bg_color"] : null))) {
            $context["t1d_f3_bg_color"] = "2F2F2F";
        }
        // line 154
        if (twig_test_empty((isset($context["t1d_f3_text_color"]) ? $context["t1d_f3_text_color"] : null))) {
            $context["t1d_f3_text_color"] = "6E6E6E";
        }
        // line 155
        if (twig_test_empty((isset($context["t1d_f3_link_color"]) ? $context["t1d_f3_link_color"] : null))) {
            $context["t1d_f3_link_color"] = "B6B6B6";
        }
        // line 156
        if (twig_test_empty((isset($context["t1d_f3_link_hover_color"]) ? $context["t1d_f3_link_hover_color"] : null))) {
            $context["t1d_f3_link_hover_color"] = "FFFFFF";
        }
        // line 157
        if (twig_test_empty((isset($context["t1d_f3_icons_bg_color"]) ? $context["t1d_f3_icons_bg_color"] : null))) {
            $context["t1d_f3_icons_bg_color"] = "333333";
        }
        // line 158
        if (twig_test_empty((isset($context["t1d_f3_border_top_color"]) ? $context["t1d_f3_border_top_color"] : null))) {
            $context["t1d_f3_border_top_color"] = "373737";
        }
        // line 159
        echo "
";
        // line 160
        if (twig_test_empty((isset($context["t1d_f4_bg_color"]) ? $context["t1d_f4_bg_color"] : null))) {
            $context["t1d_f4_bg_color"] = "2F2F2F";
        }
        // line 161
        if (twig_test_empty((isset($context["t1d_f4_text_color"]) ? $context["t1d_f4_text_color"] : null))) {
            $context["t1d_f4_text_color"] = "6E6E6E";
        }
        // line 162
        if (twig_test_empty((isset($context["t1d_f4_link_color"]) ? $context["t1d_f4_link_color"] : null))) {
            $context["t1d_f4_link_color"] = "B6B6B6";
        }
        // line 163
        if (twig_test_empty((isset($context["t1d_f4_link_hover_color"]) ? $context["t1d_f4_link_hover_color"] : null))) {
            $context["t1d_f4_link_hover_color"] = "FFFFFF";
        }
        // line 164
        if (twig_test_empty((isset($context["t1d_f4_border_top_color"]) ? $context["t1d_f4_border_top_color"] : null))) {
            $context["t1d_f4_border_top_color"] = "373737";
        }
        // line 165
        echo "
";
        // line 166
        if (twig_test_empty((isset($context["t1d_f5_bg_color"]) ? $context["t1d_f5_bg_color"] : null))) {
            $context["t1d_f5_bg_color"] = "2F2F2F";
        }
        // line 167
        if (twig_test_empty((isset($context["t1d_f5_text_color"]) ? $context["t1d_f5_text_color"] : null))) {
            $context["t1d_f5_text_color"] = "6E6E6E";
        }
        // line 168
        if (twig_test_empty((isset($context["t1d_f5_link_color"]) ? $context["t1d_f5_link_color"] : null))) {
            $context["t1d_f5_link_color"] = "B6B6B6";
        }
        // line 169
        if (twig_test_empty((isset($context["t1d_f5_link_hover_color"]) ? $context["t1d_f5_link_hover_color"] : null))) {
            $context["t1d_f5_link_hover_color"] = "FFFFFF";
        }
        // line 170
        if (twig_test_empty((isset($context["t1d_f5_border_top_color"]) ? $context["t1d_f5_border_top_color"] : null))) {
            $context["t1d_f5_border_top_color"] = "373737";
        }
        // line 171
        echo "

<style type=\"text/css\">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
.k_help {color:#999;background-color:#F5F5F5;font-size:10px;font-weight:normal;text-transform:uppercase;padding:15px;width:auto;display:block;margin-top:10px;margin-bottom:10px;border-radius:3px;}
span.k_help_tip {margin-left:10px;padding:4px 9px 3px;width:24px;display:inline;border-radius:2px;background-color:#1E91CF;color:#FFF;font-weight:bold;transition: all 0.15s ease-in 0s;opacity: 0.9; display: none;}
span.k_help_tip_support {display:block;}
span.k_help_tip:hover {opacity: 1;}
span.k_help_tip a {color:#FFF;font-size:12px;font-weight:bold;text-decoration:none;}
span.k_tooltip {cursor:pointer;}
.k_sep {background-color:#F7F7F7;}
.ptn {position:relative;width:40px;height:40px;float:left;margin-right:5px;margin-bottom:5px;}
.ptn_nr {position:absolute;bottom:0px;right:3px;color:#999;}
.prod_l {position:relative;width:81px;height:63px;float:left;margin-right:25px;margin-bottom:20px;}
.prod_l_nr {position:absolute;bottom:-17px;right:0px;}
table.form {margin-bottom:0;}
table.form div {text-align:left}
table.form b {color:#003A88;font-size:13px}
table.form > tbody > tr > td:first-child {text-align:right}
a.btn-default.link {text-decoration:none;margin-left:5px;margin-right:5px;color:#222222;background-color:#F0F0F0;border-color:#F0F0F0;transition: all 0.15s ease-in 0s;position:relative;}
a.btn-default.link:hover {background-color:#DEDEDE;border-color:#DEDEDE;}
a.btn-default.link:last-child {padding-right:45px;}
a.btn-default.link i {font-size:14px;margin-right:5px;}
a.btn-default.link span.k_help_tip {position:absolute;top:5px;right:5px;}
.htabs {margin-top:15px;}
table.form {
\twidth: 100%;
\tborder-collapse: collapse;
\tmargin-bottom: 20px;
}
table.form > tbody > tr > td:first-child {
\twidth: 200px;
}
table.form > tbody > tr > td {
\tpadding: 10px;
\tcolor: #000000;
\tborder-bottom: 1px dotted #CCCCCC;
}
label.control-label span:after {
\tdisplay: none;
}
legend {
\tbackground-color: #EEEEEE;
\tborder: none;
\tborder-radius: 3px;
}
fieldset legend {
\tmargin-top: 20px;
\tpadding: 20px 30px;
}
legend.bn {
\tborder-color: #FFFFFF;
\tpadding: 0;
\tmargin: 0;
}
legend span {
\tfont-size: 12px;
}
.nav-tabs {
\tborder-bottom: 3px solid #1E91CF;
}
.nav-tabs > li {
\tmargin-bottom: 0;
}
.nav-tabs > li > a:hover {
\tbackground-color: #E9E9E9;
\tborder-color: #E9E9E9;
}
.nav-tabs > li > a {
\tpadding: 15px 20px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
\tbackground-color: #1E91CF;
\tborder-color: #1E91CF;
\tcolor: #FFFFFF;
\tfont-weight: normal;
}
.nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
\tbackground-color: #1E91CF;
}
.nav > li > a {
\tbackground-color: #EEEEEE;
\tpadding: 15px 20px;
}
.nav > li > a:hover {
\tbackground-color: #E9E9E9;
\tcolor: inherit;
}
html, body {color:#222222;}
a {color:#666666;transition: all 0.15s ease-in 0s;}
a:hover, a:focus {color:#1E91CF;}
label {font-weight:normal;}
.form-control {display:inline-block;min-width:77px;}
.form-horizontal .form-group {
\tmargin-left: 0;
\tmargin-right: 0;
}
.form-group + .form-group {border-top: 1px dotted #EDEDED;}
.tab-content .form-horizontal label.col-sm-2{
\tpadding-left: 0;
}
textarea.form-control {width:100%;}
.panel-heading i {font-size: 14px;}
.table thead > tr > td, .table tbody > tr > td {vertical-align:top;}
</style>    

";
        // line 277
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "

<div id=\"content\">

  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-oxy-theme\" data-toggle=\"tooltip\" title=\"";
        // line 284
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 285
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 286
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 288
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 289
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 291
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
  ";
        // line 295
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 296
            echo "  <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 300
        echo "  <div class=\"panel panel-default\">
    <div class=\"panel-heading\">
\t\t<h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 302
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
\t</div>
    <div class=\"panel-body\">
    
    <form action=\"";
        // line 306
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-oxy-theme\">
    
    <input type=\"hidden\" name=\"store_id\" value=\"";
        // line 308
        echo (isset($context["store_id"]) ? $context["store_id"] : null);
        echo "\" />

        <div style=\"margin-top:10px; margin-bottom:25px;\">
\t\t
            <span style=\"margin-left:0px;\">Useful links:</span> 
            <a href=\"http://oxy.321cart.com/documentation/\" class=\"btn btn-default link\" target=\"_blank\"><i class=\"fa fa-book\"></i> OXY Documentation</a>
            <a href=\"http://oxy.321cart.com/landing/\" class=\"btn btn-default link\" target=\"_blank\"><i class=\"fa fa-television\"></i> OXY Demos</a>
            <a href=\"http://support.321cart.com/system/\" class=\"btn btn-default link\" target=\"_blank\"><i class=\"fa fa-support\"></i> OXY Support <span class=\"k_help_tip_support k_help_tip k_tooltip\" title=\"<br>If you need help, please contact us. We provide support only through our Support System.<br><br>Create a ticket and our support developer will respond as soon as possible.<br><br>Support requests are being processed on Monday to Friday.<br><br>\" data-toggle=\"tooltip\">?</span></a>

\t\t</div>

    
        <ul class=\"nav nav-tabs\">
          <li class=\"active\"><a href=\"#tab-colors\" data-toggle=\"tab\">Colors and Styles</a></li>
          <li><a href=\"#tab-backgrounds\" data-toggle=\"tab\">Background Images</a></li>
          <li><a href=\"#tab-fonts\" data-toggle=\"tab\">Fonts</a></li>
          <li><a href=\"#tab-skins\" data-toggle=\"tab\">Color Schemes</a></li>
        </ul>
        
        <div class=\"tab-content\">
        <!-- -->


        <div class=\"tab-pane active\" id=\"tab-colors\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"colors_settings_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-colors-general\" data-toggle=\"tab\">General</a></li>     
             <li><a href=\"#tab-colors-layout\" data-toggle=\"tab\">Layout</a></li>
             <li><a href=\"#tab-colors-header\" data-toggle=\"tab\">Header</a></li>
             <li><a href=\"#tab-colors-menu\" data-toggle=\"tab\">Main Menu</a></li>
             <li><a href=\"#tab-colors-midsection\" data-toggle=\"tab\">Midsection</a></li>
             <li><a href=\"#tab-colors-footer\" data-toggle=\"tab\">Footer</a></li>
             <li><a href=\"#tab-colors-prices\" data-toggle=\"tab\">Prices</a></li>
             <li><a href=\"#tab-colors-buttons\" data-toggle=\"tab\">Buttons</a></li>
             <li><a href=\"#tab-colors-dropdowns\" data-toggle=\"tab\">Dropdowns</a></li>
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-colors-general\" class=\"tab-pane fade in active\"> 
        
                    <fieldset> 
                        
                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Body background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_body_bg_color\" id=\"t1d_body_bg_color\" value=\"";
        // line 360
        echo (isset($context["t1d_body_bg_color"]) ? $context["t1d_body_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_01.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Headings color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_headings_color\" id=\"t1d_headings_color\" value=\"";
        // line 367
        echo (isset($context["t1d_headings_color"]) ? $context["t1d_headings_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_02.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show headings border bottom:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_headings_border_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 375
        if (((isset($context["t1d_headings_border_status"]) ? $context["t1d_headings_border_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 376
        if (((isset($context["t1d_headings_border_status"]) ? $context["t1d_headings_border_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_headings_border_status"]) ? $context["t1d_headings_border_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_03.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> 
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Headings border bottom color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_headings_border_color\" id=\"t1d_headings_border_color\" value=\"";
        // line 384
        echo (isset($context["t1d_headings_border_color"]) ? $context["t1d_headings_border_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Body text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_body_text_color\" id=\"t1d_body_text_color\" value=\"";
        // line 390
        echo (isset($context["t1d_body_text_color"]) ? $context["t1d_body_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_04.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Light text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_light_text_color\" id=\"t1d_light_text_color\" value=\"";
        // line 397
        echo (isset($context["t1d_light_text_color"]) ? $context["t1d_light_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_05.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Others links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_other_links_color\" id=\"t1d_other_links_color\" value=\"";
        // line 404
        echo (isset($context["t1d_other_links_color"]) ? $context["t1d_other_links_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_06.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_links_hover_color\" id=\"t1d_links_hover_color\" value=\"";
        // line 411
        echo (isset($context["t1d_links_hover_color"]) ? $context["t1d_links_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Others Icons color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_icons_color\" id=\"t1d_icons_color\" value=\"";
        // line 417
        echo (isset($context["t1d_icons_color"]) ? $context["t1d_icons_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_07.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Others Icons color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_icons_hover_color\" id=\"t1d_icons_hover_color\" value=\"";
        // line 424
        echo (isset($context["t1d_icons_hover_color"]) ? $context["t1d_icons_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Product Images style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_img_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"normal\"";
        // line 432
        if (((isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">Without Border</option>
                                    <option value=\"thumbnail-theme\"";
        // line 433
        if (((isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null) == "thumbnail-theme")) {
            echo "selected=\"selected\"";
        }
        echo ">With Border</option> 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset> 

        </div>

        <div id=\"tab-colors-layout\" class=\"tab-pane\"> 

                    <fieldset> 

                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Wrapper Frame color: 
                            <span class=\"k_help\">for \"Framed\", \"Full Width\"<br />layout styles</span> 
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_wrapper_frame_bg_color_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 454
        if (((isset($context["t1d_wrapper_frame_bg_color_status"]) ? $context["t1d_wrapper_frame_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 455
        if (((isset($context["t1d_wrapper_frame_bg_color_status"]) ? $context["t1d_wrapper_frame_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_wrapper_frame_bg_color_status"]) ? $context["t1d_wrapper_frame_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_09.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <legend class=\"bn\"></legend>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Wrapper Frame color: </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_wrapper_frame_bg_color\" id=\"t1d_wrapper_frame_bg_color\" value=\"";
        // line 464
        echo (isset($context["t1d_wrapper_frame_bg_color"]) ? $context["t1d_wrapper_frame_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Wrapper shadow:
                            <span class=\"k_help\">for \"Framed\" layout style</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_wrapper_shadow\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 473
        if (((isset($context["t1d_wrapper_shadow"]) ? $context["t1d_wrapper_shadow"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 474
        if (((isset($context["t1d_wrapper_shadow"]) ? $context["t1d_wrapper_shadow"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_wrapper_shadow"]) ? $context["t1d_wrapper_shadow"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_09.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Boxes shadow:
                            <span class=\"k_help\">for \"Boxed\" layout style</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_boxes_shadow\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 485
        if (((isset($context["t1d_boxes_shadow"]) ? $context["t1d_boxes_shadow"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 486
        if (((isset($context["t1d_boxes_shadow"]) ? $context["t1d_boxes_shadow"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_boxes_shadow"]) ? $context["t1d_boxes_shadow"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_09.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Content Column<a href=\"view/image/theme_img/help_oxy_theme/cas_10.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_content_column_bg_color\" id=\"t1d_content_column_bg_color\" value=\"";
        // line 497
        echo (isset($context["t1d_content_column_bg_color"]) ? $context["t1d_content_column_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> 
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator color:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_content_column_separator_color\" id=\"t1d_content_column_separator_color\" value=\"";
        // line 503
        echo (isset($context["t1d_content_column_separator_color"]) ? $context["t1d_content_column_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_13.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Buy Column background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_content_column_hli_buy_column\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 511
        if (((isset($context["t1d_content_column_hli_buy_column"]) ? $context["t1d_content_column_hli_buy_column"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 512
        if (((isset($context["t1d_content_column_hli_buy_column"]) ? $context["t1d_content_column_hli_buy_column"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option> 
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_12.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <legend class=\"bn\"></legend>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Buy Column background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_content_column_hli_bg_color\" id=\"t1d_content_column_hli_bg_color\" value=\"";
        // line 521
        echo (isset($context["t1d_content_column_hli_bg_color"]) ? $context["t1d_content_column_hli_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_11.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Left/Right Columns<a href=\"view/image/theme_img/help_oxy_theme/cas_10.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_sidebar_bg_color_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 532
        if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 533
        if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show separator:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_sidebar_separator_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 541
        if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 542
        if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Boxes margin bottom:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_sidebar_margin_bottom\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 550
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">0px</option>
                                    <option value=\"10\"";
        // line 551
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                    <option value=\"15\"";
        // line 552
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>
                                    <option value=\"20\"";
        // line 553
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option>
                                    <option value=\"25\"";
        // line 554
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "25")) {
            echo "selected=\"selected\"";
        }
        echo ">25px</option>
                                    <option value=\"30\"";
        // line 555
        if (((isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null) == "30")) {
            echo "selected=\"selected\"";
        }
        echo ">30px</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Left Column Box</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_head_title_bg_color\" id=\"t1d_left_column_head_title_bg_color\" value=\"";
        // line 565
        echo (isset($context["t1d_left_column_head_title_bg_color"]) ? $context["t1d_left_column_head_title_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_head_title_color\" id=\"t1d_left_column_head_title_color\" value=\"";
        // line 572
        echo (isset($context["t1d_left_column_head_title_color"]) ? $context["t1d_left_column_head_title_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_16.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Box background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_box_bg_color\" id=\"t1d_left_column_box_bg_color\" value=\"";
        // line 579
        echo (isset($context["t1d_left_column_box_bg_color"]) ? $context["t1d_left_column_box_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_box_text_color\" id=\"t1d_left_column_box_text_color\" value=\"";
        // line 586
        echo (isset($context["t1d_left_column_box_text_color"]) ? $context["t1d_left_column_box_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_20.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_box_links_color\" id=\"t1d_left_column_box_links_color\" value=\"";
        // line 593
        echo (isset($context["t1d_left_column_box_links_color"]) ? $context["t1d_left_column_box_links_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_19.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_box_links_color_hover\" id=\"t1d_left_column_box_links_color_hover\" value=\"";
        // line 600
        echo (isset($context["t1d_left_column_box_links_color_hover"]) ? $context["t1d_left_column_box_links_color_hover"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_left_column_box_separator_color\" id=\"t1d_left_column_box_separator_color\" value=\"";
        // line 606
        echo (isset($context["t1d_left_column_box_separator_color"]) ? $context["t1d_left_column_box_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_21.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Right Column Box</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_head_title_bg_color\" id=\"t1d_right_column_head_title_bg_color\" value=\"";
        // line 616
        echo (isset($context["t1d_right_column_head_title_bg_color"]) ? $context["t1d_right_column_head_title_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_head_title_color\" id=\"t1d_right_column_head_title_color\" value=\"";
        // line 623
        echo (isset($context["t1d_right_column_head_title_color"]) ? $context["t1d_right_column_head_title_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_16.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Box background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_box_bg_color\" id=\"t1d_right_column_box_bg_color\" value=\"";
        // line 630
        echo (isset($context["t1d_right_column_box_bg_color"]) ? $context["t1d_right_column_box_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_box_text_color\" id=\"t1d_right_column_box_text_color\" value=\"";
        // line 637
        echo (isset($context["t1d_right_column_box_text_color"]) ? $context["t1d_right_column_box_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_20.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_box_links_color\" id=\"t1d_right_column_box_links_color\" value=\"";
        // line 644
        echo (isset($context["t1d_right_column_box_links_color"]) ? $context["t1d_right_column_box_links_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_19.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_box_links_color_hover\" id=\"t1d_right_column_box_links_color_hover\" value=\"";
        // line 651
        echo (isset($context["t1d_right_column_box_links_color_hover"]) ? $context["t1d_right_column_box_links_color_hover"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_right_column_box_separator_color\" id=\"t1d_right_column_box_separator_color\" value=\"";
        // line 657
        echo (isset($context["t1d_right_column_box_separator_color"]) ? $context["t1d_right_column_box_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_21.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Category Box</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_head_title_bg_color\" id=\"t1d_category_box_head_title_bg_color\" value=\"";
        // line 667
        echo (isset($context["t1d_category_box_head_title_bg_color"]) ? $context["t1d_category_box_head_title_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_head_title_color\" id=\"t1d_category_box_head_title_color\" value=\"";
        // line 674
        echo (isset($context["t1d_category_box_head_title_color"]) ? $context["t1d_category_box_head_title_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_23.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Box background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_box_bg_color\" id=\"t1d_category_box_box_bg_color\" value=\"";
        // line 681
        echo (isset($context["t1d_category_box_box_bg_color"]) ? $context["t1d_category_box_box_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_25.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Categories links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_box_links_color\" id=\"t1d_category_box_box_links_color\" value=\"";
        // line 688
        echo (isset($context["t1d_category_box_box_links_color"]) ? $context["t1d_category_box_box_links_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_26.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Categories links color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_box_links_color_hover\" id=\"t1d_category_box_box_links_color_hover\" value=\"";
        // line 695
        echo (isset($context["t1d_category_box_box_links_color_hover"]) ? $context["t1d_category_box_box_links_color_hover"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subcategories links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_box_subcat_color\" id=\"t1d_category_box_box_subcat_color\" value=\"";
        // line 701
        echo (isset($context["t1d_category_box_box_subcat_color"]) ? $context["t1d_category_box_box_subcat_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_27.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_category_box_box_separator_color\" id=\"t1d_category_box_box_separator_color\" value=\"";
        // line 708
        echo (isset($context["t1d_category_box_box_separator_color"]) ? $context["t1d_category_box_box_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_28.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Filter Box</legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_head_title_bg_color\" id=\"t1d_filter_box_head_title_bg_color\" value=\"";
        // line 718
        echo (isset($context["t1d_filter_box_head_title_bg_color"]) ? $context["t1d_filter_box_head_title_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_head_title_color\" id=\"t1d_filter_box_head_title_color\" value=\"";
        // line 725
        echo (isset($context["t1d_filter_box_head_title_color"]) ? $context["t1d_filter_box_head_title_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_30.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Box background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_box_bg_color\" id=\"t1d_filter_box_box_bg_color\" value=\"";
        // line 732
        echo (isset($context["t1d_filter_box_box_bg_color"]) ? $context["t1d_filter_box_box_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_32.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Filter Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_box_filter_title_color\" id=\"t1d_filter_box_box_filter_title_color\" value=\"";
        // line 739
        echo (isset($context["t1d_filter_box_box_filter_title_color"]) ? $context["t1d_filter_box_box_filter_title_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_33.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Filter Name color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_box_filter_name_color\" id=\"t1d_filter_box_box_filter_name_color\" value=\"";
        // line 746
        echo (isset($context["t1d_filter_box_box_filter_name_color"]) ? $context["t1d_filter_box_box_filter_name_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_34.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Filter Name color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_box_filter_name_color_hover\" id=\"t1d_filter_box_box_filter_name_color_hover\" value=\"";
        // line 753
        echo (isset($context["t1d_filter_box_box_filter_name_color_hover"]) ? $context["t1d_filter_box_box_filter_name_color_hover"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_filter_box_box_separator_color\" id=\"t1d_filter_box_box_separator_color\" value=\"";
        // line 759
        echo (isset($context["t1d_filter_box_box_separator_color"]) ? $context["t1d_filter_box_box_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_35.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>
        
        <div id=\"tab-colors-header\" class=\"tab-pane\"> 

                    <fieldset>  

                    <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_top_area_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 778
        if (((isset($context["t1d_top_area_status"]) ? $context["t1d_top_area_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 779
        if (((isset($context["t1d_top_area_status"]) ? $context["t1d_top_area_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_top_area_status"]) ? $context["t1d_top_area_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_36.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_bg_color\" id=\"t1d_top_area_bg_color\" value=\"";
        // line 787
        echo (isset($context["t1d_top_area_bg_color"]) ? $context["t1d_top_area_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Fixed Header<br />background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_mini_bg_color\" id=\"t1d_top_area_mini_bg_color\" value=\"";
        // line 793
        echo (isset($context["t1d_top_area_mini_bg_color"]) ? $context["t1d_top_area_mini_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_37.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_icons_color\" id=\"t1d_top_area_icons_color\" value=\"";
        // line 800
        echo (isset($context["t1d_top_area_icons_color"]) ? $context["t1d_top_area_icons_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_38.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_icons_color_hover\" id=\"t1d_top_area_icons_color_hover\" value=\"";
        // line 807
        echo (isset($context["t1d_top_area_icons_color_hover"]) ? $context["t1d_top_area_icons_color_hover"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Icons separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_top_area_icons_separator_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 814
        if (((isset($context["t1d_top_area_icons_separator_status"]) ? $context["t1d_top_area_icons_separator_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 815
        if (((isset($context["t1d_top_area_icons_separator_status"]) ? $context["t1d_top_area_icons_separator_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_top_area_icons_separator_status"]) ? $context["t1d_top_area_icons_separator_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_41.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_icons_separator_color\" id=\"t1d_top_area_icons_separator_color\" value=\"";
        // line 823
        echo (isset($context["t1d_top_area_icons_separator_color"]) ? $context["t1d_top_area_icons_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Search Bar</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Search Bar background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_search_bg_color\" id=\"t1d_top_area_search_bg_color\" value=\"";
        // line 832
        echo (isset($context["t1d_top_area_search_bg_color"]) ? $context["t1d_top_area_search_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Search Bar border color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_search_border_color\" id=\"t1d_top_area_search_border_color\" value=\"";
        // line 838
        echo (isset($context["t1d_top_area_search_border_color"]) ? $context["t1d_top_area_search_border_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">\"Search\" color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_search_color\" id=\"t1d_top_area_search_color\" value=\"";
        // line 844
        echo (isset($context["t1d_top_area_search_color"]) ? $context["t1d_top_area_search_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">\"Search\" active color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_search_color_active\" id=\"t1d_top_area_search_color_active\" value=\"";
        // line 850
        echo (isset($context["t1d_top_area_search_color_active"]) ? $context["t1d_top_area_search_color_active"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Search Bar icon color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_search_icon_color\" id=\"t1d_top_area_search_icon_color\" value=\"";
        // line 856
        echo (isset($context["t1d_top_area_search_icon_color"]) ? $context["t1d_top_area_search_icon_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Top Bar</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_top_area_tb_bg_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 866
        if (((isset($context["t1d_top_area_tb_bg_status"]) ? $context["t1d_top_area_tb_bg_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 867
        if (((isset($context["t1d_top_area_tb_bg_status"]) ? $context["t1d_top_area_tb_bg_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_top_area_tb_bg_status"]) ? $context["t1d_top_area_tb_bg_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_41.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_tb_bg_color\" id=\"t1d_top_area_tb_bg_color\" value=\"";
        // line 875
        echo (isset($context["t1d_top_area_tb_bg_color"]) ? $context["t1d_top_area_tb_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show border bottom:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_top_area_tb_bottom_border_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 882
        if (((isset($context["t1d_top_area_tb_bottom_border_status"]) ? $context["t1d_top_area_tb_bottom_border_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 883
        if (((isset($context["t1d_top_area_tb_bottom_border_status"]) ? $context["t1d_top_area_tb_bottom_border_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_42.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border bottom color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_tb_bottom_border_color\" id=\"t1d_top_area_tb_bottom_border_color\" value=\"";
        // line 891
        echo (isset($context["t1d_top_area_tb_bottom_border_color"]) ? $context["t1d_top_area_tb_bottom_border_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_tb_text_color\" id=\"t1d_top_area_tb_text_color\" value=\"";
        // line 897
        echo (isset($context["t1d_top_area_tb_text_color"]) ? $context["t1d_top_area_tb_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_43.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_tb_link_color\" id=\"t1d_top_area_tb_link_color\" value=\"";
        // line 904
        echo (isset($context["t1d_top_area_tb_link_color"]) ? $context["t1d_top_area_tb_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_44.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_tb_link_color_hover\" id=\"t1d_top_area_tb_link_color_hover\" value=\"";
        // line 911
        echo (isset($context["t1d_top_area_tb_link_color_hover"]) ? $context["t1d_top_area_tb_link_color_hover"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_top_area_tb_icons_color\" id=\"t1d_top_area_tb_icons_color\" value=\"";
        // line 917
        echo (isset($context["t1d_top_area_tb_icons_color"]) ? $context["t1d_top_area_tb_icons_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_45.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>
        
        <div id=\"tab-colors-menu\" class=\"tab-pane\"> 

                    <fieldset>  

                        <legend>Main Menu Bar <a href=\"view/image/theme_img/help_oxy_theme/cas_47.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 936
        if (((isset($context["t1d_mm_bg_color_status"]) ? $context["t1d_mm_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 937
        if (((isset($context["t1d_mm_bg_color_status"]) ? $context["t1d_mm_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_bg_color_status"]) ? $context["t1d_mm_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_bg_color\" id=\"t1d_mm_bg_color\" value=\"";
        // line 944
        echo (isset($context["t1d_mm_bg_color"]) ? $context["t1d_mm_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_separator_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 953
        if (((isset($context["t1d_mm_separator_status"]) ? $context["t1d_mm_separator_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 954
        if (((isset($context["t1d_mm_separator_status"]) ? $context["t1d_mm_separator_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_separator_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 959
        if (((isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 960
        if (((isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 961
        if (((isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 962
        if (((isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 963
        if (((isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_separator_color\" id=\"t1d_mm_separator_color\" value=\"";
        // line 967
        echo (isset($context["t1d_mm_separator_color"]) ? $context["t1d_mm_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_48.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border top:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_border_top_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 977
        if (((isset($context["t1d_mm_border_top_status"]) ? $context["t1d_mm_border_top_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 978
        if (((isset($context["t1d_mm_border_top_status"]) ? $context["t1d_mm_border_top_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_border_top_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 983
        if (((isset($context["t1d_mm_border_top_size"]) ? $context["t1d_mm_border_top_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 984
        if (((isset($context["t1d_mm_border_top_size"]) ? $context["t1d_mm_border_top_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 985
        if (((isset($context["t1d_mm_border_top_size"]) ? $context["t1d_mm_border_top_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 986
        if (((isset($context["t1d_mm_border_top_size"]) ? $context["t1d_mm_border_top_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 987
        if (((isset($context["t1d_mm_border_top_size"]) ? $context["t1d_mm_border_top_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_border_top_color\" id=\"t1d_mm_border_top_color\" value=\"";
        // line 991
        echo (isset($context["t1d_mm_border_top_color"]) ? $context["t1d_mm_border_top_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_49.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border bottom:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_border_bottom_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1001
        if (((isset($context["t1d_mm_border_bottom_status"]) ? $context["t1d_mm_border_bottom_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1002
        if (((isset($context["t1d_mm_border_bottom_status"]) ? $context["t1d_mm_border_bottom_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_border_bottom_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1007
        if (((isset($context["t1d_mm_border_bottom_size"]) ? $context["t1d_mm_border_bottom_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1008
        if (((isset($context["t1d_mm_border_bottom_size"]) ? $context["t1d_mm_border_bottom_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1009
        if (((isset($context["t1d_mm_border_bottom_size"]) ? $context["t1d_mm_border_bottom_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1010
        if (((isset($context["t1d_mm_border_bottom_size"]) ? $context["t1d_mm_border_bottom_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1011
        if (((isset($context["t1d_mm_border_bottom_size"]) ? $context["t1d_mm_border_bottom_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_border_bottom_color\" id=\"t1d_mm_border_bottom_color\" value=\"";
        // line 1015
        echo (isset($context["t1d_mm_border_bottom_color"]) ? $context["t1d_mm_border_bottom_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_50.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Shadow:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_shadow_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1024
        if (((isset($context["t1d_mm_shadow_status"]) ? $context["t1d_mm_shadow_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1025
        if (((isset($context["t1d_mm_shadow_status"]) ? $context["t1d_mm_shadow_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_shadow_status"]) ? $context["t1d_mm_shadow_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Sort Down icon:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_sort_down_icon_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1034
        if (((isset($context["t1d_mm_sort_down_icon_status"]) ? $context["t1d_mm_sort_down_icon_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1035
        if (((isset($context["t1d_mm_sort_down_icon_status"]) ? $context["t1d_mm_sort_down_icon_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sort_down_icon_status"]) ? $context["t1d_mm_sort_down_icon_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>  
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Home Page Link</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm1_bg_color_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1047
        if (((isset($context["t1d_mm1_bg_color_status"]) ? $context["t1d_mm1_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1048
        if (((isset($context["t1d_mm1_bg_color_status"]) ? $context["t1d_mm1_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm1_bg_color_status"]) ? $context["t1d_mm1_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm1_bg_color\" id=\"t1d_mm1_bg_color\" value=\"";
        // line 1055
        echo (isset($context["t1d_mm1_bg_color"]) ? $context["t1d_mm1_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_52.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm1_bg_hover_color\" id=\"t1d_mm1_bg_hover_color\" value=\"";
        // line 1062
        echo (isset($context["t1d_mm1_bg_hover_color"]) ? $context["t1d_mm1_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm1_link_color\" id=\"t1d_mm1_link_color\" value=\"";
        // line 1069
        echo (isset($context["t1d_mm1_link_color"]) ? $context["t1d_mm1_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_53.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm1_link_hover_color\" id=\"t1d_mm1_link_hover_color\" value=\"";
        // line 1076
        echo (isset($context["t1d_mm1_link_hover_color"]) ? $context["t1d_mm1_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Categories</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm2_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1086
        if (((isset($context["t1d_mm2_bg_color_status"]) ? $context["t1d_mm2_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1087
        if (((isset($context["t1d_mm2_bg_color_status"]) ? $context["t1d_mm2_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm2_bg_color_status"]) ? $context["t1d_mm2_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm2_bg_color\" id=\"t1d_mm2_bg_color\" value=\"";
        // line 1094
        echo (isset($context["t1d_mm2_bg_color"]) ? $context["t1d_mm2_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm2_bg_hover_color\" id=\"t1d_mm2_bg_hover_color\" value=\"";
        // line 1100
        echo (isset($context["t1d_mm2_bg_hover_color"]) ? $context["t1d_mm2_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm2_link_color\" id=\"t1d_mm2_link_color\" value=\"";
        // line 1106
        echo (isset($context["t1d_mm2_link_color"]) ? $context["t1d_mm2_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm2_link_hover_color\" id=\"t1d_mm2_link_hover_color\" value=\"";
        // line 1112
        echo (isset($context["t1d_mm2_link_hover_color"]) ? $context["t1d_mm2_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Brands</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm3_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1122
        if (((isset($context["t1d_mm3_bg_color_status"]) ? $context["t1d_mm3_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1123
        if (((isset($context["t1d_mm3_bg_color_status"]) ? $context["t1d_mm3_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm3_bg_color\" id=\"t1d_mm3_bg_color\" value=\"";
        // line 1130
        echo (isset($context["t1d_mm3_bg_color"]) ? $context["t1d_mm3_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm3_bg_hover_color\" id=\"t1d_mm3_bg_hover_color\" value=\"";
        // line 1136
        echo (isset($context["t1d_mm3_bg_hover_color"]) ? $context["t1d_mm3_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm3_link_color\" id=\"t1d_mm3_link_color\" value=\"";
        // line 1142
        echo (isset($context["t1d_mm3_link_color"]) ? $context["t1d_mm3_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm3_link_hover_color\" id=\"t1d_mm3_link_hover_color\" value=\"";
        // line 1148
        echo (isset($context["t1d_mm3_link_hover_color"]) ? $context["t1d_mm3_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Custom Blocks</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm6_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1158
        if (((isset($context["t1d_mm6_bg_color_status"]) ? $context["t1d_mm6_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1159
        if (((isset($context["t1d_mm6_bg_color_status"]) ? $context["t1d_mm6_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm6_bg_color\" id=\"t1d_mm6_bg_color\" value=\"";
        // line 1166
        echo (isset($context["t1d_mm6_bg_color"]) ? $context["t1d_mm6_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm6_bg_hover_color\" id=\"t1d_mm6_bg_hover_color\" value=\"";
        // line 1172
        echo (isset($context["t1d_mm6_bg_hover_color"]) ? $context["t1d_mm6_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm6_link_color\" id=\"t1d_mm6_link_color\" value=\"";
        // line 1178
        echo (isset($context["t1d_mm6_link_color"]) ? $context["t1d_mm6_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm6_link_hover_color\" id=\"t1d_mm6_link_hover_color\" value=\"";
        // line 1184
        echo (isset($context["t1d_mm6_link_hover_color"]) ? $context["t1d_mm6_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Custom Dropdown Menus</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm5_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1194
        if (((isset($context["t1d_mm5_bg_color_status"]) ? $context["t1d_mm5_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1195
        if (((isset($context["t1d_mm5_bg_color_status"]) ? $context["t1d_mm5_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm5_bg_color\" id=\"t1d_mm5_bg_color\" value=\"";
        // line 1202
        echo (isset($context["t1d_mm5_bg_color"]) ? $context["t1d_mm5_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm5_bg_hover_color\" id=\"t1d_mm5_bg_hover_color\" value=\"";
        // line 1208
        echo (isset($context["t1d_mm5_bg_hover_color"]) ? $context["t1d_mm5_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm5_link_color\" id=\"t1d_mm5_link_color\" value=\"";
        // line 1214
        echo (isset($context["t1d_mm5_link_color"]) ? $context["t1d_mm5_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm5_link_hover_color\" id=\"t1d_mm5_link_hover_color\" value=\"";
        // line 1220
        echo (isset($context["t1d_mm5_link_hover_color"]) ? $context["t1d_mm5_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Custom Links</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm4_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1230
        if (((isset($context["t1d_mm4_bg_color_status"]) ? $context["t1d_mm4_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1231
        if (((isset($context["t1d_mm4_bg_color_status"]) ? $context["t1d_mm4_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm4_bg_color\" id=\"t1d_mm4_bg_color\" value=\"";
        // line 1238
        echo (isset($context["t1d_mm4_bg_color"]) ? $context["t1d_mm4_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm4_bg_hover_color\" id=\"t1d_mm4_bg_hover_color\" value=\"";
        // line 1244
        echo (isset($context["t1d_mm4_bg_hover_color"]) ? $context["t1d_mm4_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm4_link_color\" id=\"t1d_mm4_link_color\" value=\"";
        // line 1250
        echo (isset($context["t1d_mm4_link_color"]) ? $context["t1d_mm4_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm4_link_hover_color\" id=\"t1d_mm4_link_hover_color\" value=\"";
        // line 1256
        echo (isset($context["t1d_mm4_link_hover_color"]) ? $context["t1d_mm4_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Sub-Menus</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_sub_bg_color\" id=\"t1d_mm_sub_bg_color\" value=\"";
        // line 1265
        echo (isset($context["t1d_mm_sub_bg_color"]) ? $context["t1d_mm_sub_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_54.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Highlighted Items background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_sub_titles_bg_color\" id=\"t1d_mm_sub_titles_bg_color\" value=\"";
        // line 1272
        echo (isset($context["t1d_mm_sub_titles_bg_color"]) ? $context["t1d_mm_sub_titles_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_55.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_sub_text_color\" id=\"t1d_mm_sub_text_color\" value=\"";
        // line 1279
        echo (isset($context["t1d_mm_sub_text_color"]) ? $context["t1d_mm_sub_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_56.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_sub_link_color\" id=\"t1d_mm_sub_link_color\" value=\"";
        // line 1286
        echo (isset($context["t1d_mm_sub_link_color"]) ? $context["t1d_mm_sub_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_57.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_sub_link_hover_color\" id=\"t1d_mm_sub_link_hover_color\" value=\"";
        // line 1293
        echo (isset($context["t1d_mm_sub_link_hover_color"]) ? $context["t1d_mm_sub_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>                      
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Main Categories border bottom:<br /><span class=\"k_help\">for \"Horizontal\" style</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_sub_main_category_border_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1300
        if (((isset($context["t1d_mm_sub_main_category_border_status"]) ? $context["t1d_mm_sub_main_category_border_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1301
        if (((isset($context["t1d_mm_sub_main_category_border_status"]) ? $context["t1d_mm_sub_main_category_border_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sub_main_category_border_status"]) ? $context["t1d_mm_sub_main_category_border_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subcategories color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mm_sub_subcategory_color\" id=\"t1d_mm_sub_subcategory_color\" value=\"";
        // line 1309
        echo (isset($context["t1d_mm_sub_subcategory_color"]) ? $context["t1d_mm_sub_subcategory_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> 
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subcategories border bottom:<br /><span class=\"k_help\">for \"Horizontal\" style</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_sub_subcategory_border_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1317
        if (((isset($context["t1d_mm_sub_subcategory_border_status"]) ? $context["t1d_mm_sub_subcategory_border_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1318
        if (((isset($context["t1d_mm_sub_subcategory_border_status"]) ? $context["t1d_mm_sub_subcategory_border_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sub_subcategory_border_status"]) ? $context["t1d_mm_sub_subcategory_border_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Category Dropdown border:<br /><span class=\"k_help\">for \"Horizontal\"<br />and \"Vertical 2\" styles</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_sub_border_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1327
        if (((isset($context["t1d_mm_sub_border_status"]) ? $context["t1d_mm_sub_border_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1328
        if (((isset($context["t1d_mm_sub_border_status"]) ? $context["t1d_mm_sub_border_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Shadow:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_mm_sub_box_shadow\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1337
        if (((isset($context["t1d_mm_sub_box_shadow"]) ? $context["t1d_mm_sub_box_shadow"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1338
        if (((isset($context["t1d_mm_sub_box_shadow"]) ? $context["t1d_mm_sub_box_shadow"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sub_box_shadow"]) ? $context["t1d_mm_sub_box_shadow"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_59.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>
        
        <div id=\"tab-colors-midsection\" class=\"tab-pane\"> 

                    <fieldset>
                    
                    <legend class=\"bn\"></legend>
                    
                    <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Sale Badge color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mid_prod_box_sale_icon_color\" id=\"t1d_mid_prod_box_sale_icon_color\" value=\"";
        // line 1357
        echo (isset($context["t1d_mid_prod_box_sale_icon_color"]) ? $context["t1d_mid_prod_box_sale_icon_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_60.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">New Product Badge color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mid_prod_box_new_icon_color\" id=\"t1d_mid_prod_box_new_icon_color\" value=\"";
        // line 1364
        echo (isset($context["t1d_mid_prod_box_new_icon_color"]) ? $context["t1d_mid_prod_box_new_icon_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_61.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Out of Stock Badge color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mid_prod_box_out_of_stock_icon_color\" id=\"t1d_mid_prod_box_out_of_stock_icon_color\" value=\"";
        // line 1371
        echo (isset($context["t1d_mid_prod_box_out_of_stock_icon_color"]) ? $context["t1d_mid_prod_box_out_of_stock_icon_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_61.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Rating Stars color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_mid_prod_stars_color\" id=\"t1d_mid_prod_stars_color\" value=\"";
        // line 1378
        echo (isset($context["t1d_mid_prod_stars_color"]) ? $context["t1d_mid_prod_stars_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_62.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>
        
        <div id=\"tab-colors-footer\" class=\"tab-pane\"> 

                    <fieldset>
                        
                        <legend>Top Custom Block <a href=\"view/image/theme_img/help_oxy_theme/cas_73.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f6_bg_color_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1397
        if (((isset($context["t1d_f6_bg_color_status"]) ? $context["t1d_f6_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1398
        if (((isset($context["t1d_f6_bg_color_status"]) ? $context["t1d_f6_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f6_bg_color_status"]) ? $context["t1d_f6_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f6_bg_color\" id=\"t1d_f6_bg_color\" value=\"";
        // line 1405
        echo (isset($context["t1d_f6_bg_color"]) ? $context["t1d_f6_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f6_text_color\" id=\"t1d_f6_text_color\" value=\"";
        // line 1411
        echo (isset($context["t1d_f6_text_color"]) ? $context["t1d_f6_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f6_link_color\" id=\"t1d_f6_link_color\" value=\"";
        // line 1417
        echo (isset($context["t1d_f6_link_color"]) ? $context["t1d_f6_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f6_link_hover_color\" id=\"t1d_f6_link_hover_color\" value=\"";
        // line 1423
        echo (isset($context["t1d_f6_link_hover_color"]) ? $context["t1d_f6_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border top:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_f6_border_top_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1431
        if (((isset($context["t1d_f6_border_top_status"]) ? $context["t1d_f6_border_top_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1432
        if (((isset($context["t1d_f6_border_top_status"]) ? $context["t1d_f6_border_top_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_f6_border_top_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1437
        if (((isset($context["t1d_f6_border_top_size"]) ? $context["t1d_f6_border_top_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1438
        if (((isset($context["t1d_f6_border_top_size"]) ? $context["t1d_f6_border_top_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1439
        if (((isset($context["t1d_f6_border_top_size"]) ? $context["t1d_f6_border_top_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1440
        if (((isset($context["t1d_f6_border_top_size"]) ? $context["t1d_f6_border_top_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1441
        if (((isset($context["t1d_f6_border_top_size"]) ? $context["t1d_f6_border_top_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f6_border_top_color\" id=\"t1d_f6_border_top_color\" value=\"";
        // line 1445
        echo (isset($context["t1d_f6_border_top_color"]) ? $context["t1d_f6_border_top_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Information, Custom Columns <a href=\"view/image/theme_img/help_oxy_theme/cas_71.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f2_bg_color_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1455
        if (((isset($context["t1d_f2_bg_color_status"]) ? $context["t1d_f2_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1456
        if (((isset($context["t1d_f2_bg_color_status"]) ? $context["t1d_f2_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f2_bg_color_status"]) ? $context["t1d_f2_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_bg_color\" id=\"t1d_f2_bg_color\" value=\"";
        // line 1463
        echo (isset($context["t1d_f2_bg_color"]) ? $context["t1d_f2_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_titles_color\" id=\"t1d_f2_titles_color\" value=\"";
        // line 1469
        echo (isset($context["t1d_f2_titles_color"]) ? $context["t1d_f2_titles_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Titles border bottom:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_f2_titles_border_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1477
        if (((isset($context["t1d_f2_titles_border_status"]) ? $context["t1d_f2_titles_border_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1478
        if (((isset($context["t1d_f2_titles_border_status"]) ? $context["t1d_f2_titles_border_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f2_titles_border_status"]) ? $context["t1d_f2_titles_border_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_f2_titles_border_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1483
        if (((isset($context["t1d_f2_titles_border_size"]) ? $context["t1d_f2_titles_border_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1484
        if (((isset($context["t1d_f2_titles_border_size"]) ? $context["t1d_f2_titles_border_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1485
        if (((isset($context["t1d_f2_titles_border_size"]) ? $context["t1d_f2_titles_border_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1486
        if (((isset($context["t1d_f2_titles_border_size"]) ? $context["t1d_f2_titles_border_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1487
        if (((isset($context["t1d_f2_titles_border_size"]) ? $context["t1d_f2_titles_border_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_titles_border_color\" id=\"t1d_f2_titles_border_color\" value=\"";
        // line 1491
        echo (isset($context["t1d_f2_titles_border_color"]) ? $context["t1d_f2_titles_border_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_text_color\" id=\"t1d_f2_text_color\" value=\"";
        // line 1497
        echo (isset($context["t1d_f2_text_color"]) ? $context["t1d_f2_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_link_color\" id=\"t1d_f2_link_color\" value=\"";
        // line 1503
        echo (isset($context["t1d_f2_link_color"]) ? $context["t1d_f2_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_link_hover_color\" id=\"t1d_f2_link_hover_color\" value=\"";
        // line 1509
        echo (isset($context["t1d_f2_link_hover_color"]) ? $context["t1d_f2_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border top:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_f2_border_top_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1517
        if (((isset($context["t1d_f2_border_top_status"]) ? $context["t1d_f2_border_top_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1518
        if (((isset($context["t1d_f2_border_top_status"]) ? $context["t1d_f2_border_top_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f2_border_top_status"]) ? $context["t1d_f2_border_top_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_f2_border_top_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1523
        if (((isset($context["t1d_f2_border_top_size"]) ? $context["t1d_f2_border_top_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1524
        if (((isset($context["t1d_f2_border_top_size"]) ? $context["t1d_f2_border_top_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1525
        if (((isset($context["t1d_f2_border_top_size"]) ? $context["t1d_f2_border_top_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1526
        if (((isset($context["t1d_f2_border_top_size"]) ? $context["t1d_f2_border_top_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1527
        if (((isset($context["t1d_f2_border_top_size"]) ? $context["t1d_f2_border_top_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f2_border_top_color\" id=\"t1d_f2_border_top_color\" value=\"";
        // line 1531
        echo (isset($context["t1d_f2_border_top_color"]) ? $context["t1d_f2_border_top_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Powered by, Payment Images, Follow Us <a href=\"view/image/theme_img/help_oxy_theme/cas_72.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f3_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1541
        if (((isset($context["t1d_f3_bg_color_status"]) ? $context["t1d_f3_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1542
        if (((isset($context["t1d_f3_bg_color_status"]) ? $context["t1d_f3_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f3_bg_color_status"]) ? $context["t1d_f3_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f3_bg_color\" id=\"t1d_f3_bg_color\" value=\"";
        // line 1549
        echo (isset($context["t1d_f3_bg_color"]) ? $context["t1d_f3_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f3_text_color\" id=\"t1d_f3_text_color\" value=\"";
        // line 1555
        echo (isset($context["t1d_f3_text_color"]) ? $context["t1d_f3_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f3_link_color\" id=\"t1d_f3_link_color\" value=\"";
        // line 1561
        echo (isset($context["t1d_f3_link_color"]) ? $context["t1d_f3_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f3_link_hover_color\" id=\"t1d_f3_link_hover_color\" value=\"";
        // line 1567
        echo (isset($context["t1d_f3_link_hover_color"]) ? $context["t1d_f3_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Social Media icons background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f3_icons_bg_color\" id=\"t1d_f3_icons_bg_color\" value=\"";
        // line 1573
        echo (isset($context["t1d_f3_icons_bg_color"]) ? $context["t1d_f3_icons_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Social Media icons style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f3_icons_social_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1580
        if (((isset($context["t1d_f3_icons_social_style"]) ? $context["t1d_f3_icons_social_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">One color</option>
                                    <option value=\"1\"";
        // line 1581
        if (((isset($context["t1d_f3_icons_social_style"]) ? $context["t1d_f3_icons_social_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f3_icons_social_style"]) ? $context["t1d_f3_icons_social_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Multicolor</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Payment images style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f3_payment_images_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1589
        if (((isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Black and White</option>
                                    <option value=\"1\"";
        // line 1590
        if (((isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Color</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border top:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_f3_border_top_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1599
        if (((isset($context["t1d_f3_border_top_status"]) ? $context["t1d_f3_border_top_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1600
        if (((isset($context["t1d_f3_border_top_status"]) ? $context["t1d_f3_border_top_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f3_border_top_status"]) ? $context["t1d_f3_border_top_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_f3_border_top_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1605
        if (((isset($context["t1d_f3_border_top_size"]) ? $context["t1d_f3_border_top_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1606
        if (((isset($context["t1d_f3_border_top_size"]) ? $context["t1d_f3_border_top_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1607
        if (((isset($context["t1d_f3_border_top_size"]) ? $context["t1d_f3_border_top_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1608
        if (((isset($context["t1d_f3_border_top_size"]) ? $context["t1d_f3_border_top_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1609
        if (((isset($context["t1d_f3_border_top_size"]) ? $context["t1d_f3_border_top_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f3_border_top_color\" id=\"t1d_f3_border_top_color\" value=\"";
        // line 1613
        echo (isset($context["t1d_f3_border_top_color"]) ? $context["t1d_f3_border_top_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Bottom Custom Block <a href=\"view/image/theme_img/help_oxy_theme/cas_73.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f4_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1623
        if (((isset($context["t1d_f4_bg_color_status"]) ? $context["t1d_f4_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1624
        if (((isset($context["t1d_f4_bg_color_status"]) ? $context["t1d_f4_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f4_bg_color_status"]) ? $context["t1d_f4_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f4_bg_color\" id=\"t1d_f4_bg_color\" value=\"";
        // line 1631
        echo (isset($context["t1d_f4_bg_color"]) ? $context["t1d_f4_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f4_text_color\" id=\"t1d_f4_text_color\" value=\"";
        // line 1637
        echo (isset($context["t1d_f4_text_color"]) ? $context["t1d_f4_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f4_link_color\" id=\"t1d_f4_link_color\" value=\"";
        // line 1643
        echo (isset($context["t1d_f4_link_color"]) ? $context["t1d_f4_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f4_link_hover_color\" id=\"t1d_f4_link_hover_color\" value=\"";
        // line 1649
        echo (isset($context["t1d_f4_link_hover_color"]) ? $context["t1d_f4_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border top:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_f4_border_top_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1657
        if (((isset($context["t1d_f4_border_top_status"]) ? $context["t1d_f4_border_top_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1658
        if (((isset($context["t1d_f4_border_top_status"]) ? $context["t1d_f4_border_top_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f4_border_top_status"]) ? $context["t1d_f4_border_top_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_f4_border_top_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1663
        if (((isset($context["t1d_f4_border_top_size"]) ? $context["t1d_f4_border_top_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1664
        if (((isset($context["t1d_f4_border_top_size"]) ? $context["t1d_f4_border_top_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1665
        if (((isset($context["t1d_f4_border_top_size"]) ? $context["t1d_f4_border_top_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1666
        if (((isset($context["t1d_f4_border_top_size"]) ? $context["t1d_f4_border_top_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1667
        if (((isset($context["t1d_f4_border_top_size"]) ? $context["t1d_f4_border_top_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f4_border_top_color\" id=\"t1d_f4_border_top_color\" value=\"";
        // line 1671
        echo (isset($context["t1d_f4_border_top_color"]) ? $context["t1d_f4_border_top_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Sliding Bottom Custom Block <a href=\"view/image/theme_img/help_oxy_theme/cas_73.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_f5_bg_color_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1681
        if (((isset($context["t1d_f5_bg_color_status"]) ? $context["t1d_f5_bg_color_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1682
        if (((isset($context["t1d_f5_bg_color_status"]) ? $context["t1d_f5_bg_color_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_f5_bg_color_status"]) ? $context["t1d_f5_bg_color_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f5_bg_color\" id=\"t1d_f5_bg_color\" value=\"";
        // line 1689
        echo (isset($context["t1d_f5_bg_color"]) ? $context["t1d_f5_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f5_text_color\" id=\"t1d_f5_text_color\" value=\"";
        // line 1695
        echo (isset($context["t1d_f5_text_color"]) ? $context["t1d_f5_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f5_link_color\" id=\"t1d_f5_link_color\" value=\"";
        // line 1701
        echo (isset($context["t1d_f5_link_color"]) ? $context["t1d_f5_link_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Link color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f5_link_hover_color\" id=\"t1d_f5_link_hover_color\" value=\"";
        // line 1707
        echo (isset($context["t1d_f5_link_hover_color"]) ? $context["t1d_f5_link_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Border top:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                Show:
\t\t\t\t\t\t\t\t<select name=\"t1d_f5_border_top_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1715
        if (((isset($context["t1d_f5_border_top_status"]) ? $context["t1d_f5_border_top_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1716
        if (((isset($context["t1d_f5_border_top_status"]) ? $context["t1d_f5_border_top_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Size (px):
\t\t\t\t\t\t\t\t<select name=\"t1d_f5_border_top_size\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 1721
        if (((isset($context["t1d_f5_border_top_size"]) ? $context["t1d_f5_border_top_size"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t    \t<option value=\"2\"";
        // line 1722
        if (((isset($context["t1d_f5_border_top_size"]) ? $context["t1d_f5_border_top_size"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t    \t<option value=\"3\"";
        // line 1723
        if (((isset($context["t1d_f5_border_top_size"]) ? $context["t1d_f5_border_top_size"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t    \t<option value=\"4\"";
        // line 1724
        if (((isset($context["t1d_f5_border_top_size"]) ? $context["t1d_f5_border_top_size"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                           \t    \t<option value=\"5\"";
        // line 1725
        if (((isset($context["t1d_f5_border_top_size"]) ? $context["t1d_f5_border_top_size"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>  
\t\t\t\t\t\t\t    </select>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                Color:
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_f5_border_top_color\" id=\"t1d_f5_border_top_color\" value=\"";
        // line 1729
        echo (isset($context["t1d_f5_border_top_color"]) ? $context["t1d_f5_border_top_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>


                    </fieldset>

        </div>
        
        <div id=\"tab-colors-prices\" class=\"tab-pane\"> 

                    <fieldset>  

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Price color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_price_color\" id=\"t1d_price_color\" value=\"";
        // line 1745
        echo (isset($context["t1d_price_color"]) ? $context["t1d_price_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Old price color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_price_old_color\" id=\"t1d_price_old_color\" value=\"";
        // line 1751
        echo (isset($context["t1d_price_old_color"]) ? $context["t1d_price_old_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>  
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">New price color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_price_new_color\" id=\"t1d_price_new_color\" value=\"";
        // line 1757
        echo (isset($context["t1d_price_new_color"]) ? $context["t1d_price_new_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>                       

                    </fieldset>

        </div>
        
        <div id=\"tab-colors-buttons\" class=\"tab-pane\"> 

                    <fieldset>  
                    
                        <legend class=\"bn\"></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Button border radius:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_button_border_radius\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1775
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">0px</option>                        
                           \t    \t<option value=\"1\"";
        // line 1776
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1px</option>
                           \t    \t<option value=\"2\"";
        // line 1777
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">2px</option>
                          \t    \t<option value=\"3\"";
        // line 1778
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3px</option>
                           \t    \t<option value=\"4\"";
        // line 1779
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4px</option>
                           \t    \t<option value=\"5\"";
        // line 1780
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5px</option> 
                           \t    \t<option value=\"6\"";
        // line 1781
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">6px</option>
                           \t    \t<option value=\"7\"";
        // line 1782
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">7px</option>                           
                           \t    \t<option value=\"8\"";
        // line 1783
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">8px</option> 
                           \t    \t<option value=\"9\"";
        // line 1784
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "9")) {
            echo "selected=\"selected\"";
        }
        echo ">9px</option>
                           \t    \t<option value=\"10\"";
        // line 1785
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                    <option value=\"50\"";
        // line 1786
        if (((isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null) == "50")) {
            echo "selected=\"selected\"";
        }
        echo ">50%</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Button hover shadow:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_button_shadow_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1794
        if (((isset($context["t1d_button_shadow_status"]) ? $context["t1d_button_shadow_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1795
        if (((isset($context["t1d_button_shadow_status"]) ? $context["t1d_button_shadow_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_button_shadow_status"]) ? $context["t1d_button_shadow_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>    
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Default Buttons <a href=\"view/image/theme_img/help_oxy_theme/cas_74.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_button_bg_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1806
        if (((isset($context["t1d_button_bg_status"]) ? $context["t1d_button_bg_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1807
        if (((isset($context["t1d_button_bg_status"]) ? $context["t1d_button_bg_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_button_bg_status"]) ? $context["t1d_button_bg_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>   
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_bg_color\" id=\"t1d_button_bg_color\" value=\"";
        // line 1814
        echo (isset($context["t1d_button_bg_color"]) ? $context["t1d_button_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_bg_hover_color\" id=\"t1d_button_bg_hover_color\" value=\"";
        // line 1820
        echo (isset($context["t1d_button_bg_hover_color"]) ? $context["t1d_button_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_text_color\" id=\"t1d_button_text_color\" value=\"";
        // line 1826
        echo (isset($context["t1d_button_text_color"]) ? $context["t1d_button_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_text_hover_color\" id=\"t1d_button_text_hover_color\" value=\"";
        // line 1832
        echo (isset($context["t1d_button_text_hover_color"]) ? $context["t1d_button_text_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Exclusive Buttons <a href=\"view/image/theme_img/help_oxy_theme/cas_74.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_button_exclusive_bg_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1842
        if (((isset($context["t1d_button_exclusive_bg_status"]) ? $context["t1d_button_exclusive_bg_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1843
        if (((isset($context["t1d_button_exclusive_bg_status"]) ? $context["t1d_button_exclusive_bg_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_button_exclusive_bg_status"]) ? $context["t1d_button_exclusive_bg_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>   
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_exclusive_bg_color\" id=\"t1d_button_exclusive_bg_color\" value=\"";
        // line 1850
        echo (isset($context["t1d_button_exclusive_bg_color"]) ? $context["t1d_button_exclusive_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_exclusive_bg_hover_color\" id=\"t1d_button_exclusive_bg_hover_color\" value=\"";
        // line 1856
        echo (isset($context["t1d_button_exclusive_bg_hover_color"]) ? $context["t1d_button_exclusive_bg_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_exclusive_text_color\" id=\"t1d_button_exclusive_text_color\" value=\"";
        // line 1862
        echo (isset($context["t1d_button_exclusive_text_color"]) ? $context["t1d_button_exclusive_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_button_exclusive_text_hover_color\" id=\"t1d_button_exclusive_text_hover_color\" value=\"";
        // line 1868
        echo (isset($context["t1d_button_exclusive_text_hover_color"]) ? $context["t1d_button_exclusive_text_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>
        
        <div id=\"tab-colors-dropdowns\" class=\"tab-pane\"> 

                    <fieldset>  

                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_bg_color\" id=\"t1d_dd_bg_color\" value=\"";
        // line 1885
        echo (isset($context["t1d_dd_bg_color"]) ? $context["t1d_dd_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                <a href=\"view/image/theme_img/help_oxy_theme/cas_75.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Headings color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_headings_color\" id=\"t1d_dd_headings_color\" value=\"";
        // line 1892
        echo (isset($context["t1d_dd_headings_color"]) ? $context["t1d_dd_headings_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_text_color\" id=\"t1d_dd_text_color\" value=\"";
        // line 1898
        echo (isset($context["t1d_dd_text_color"]) ? $context["t1d_dd_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Light text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_light_text_color\" id=\"t1d_dd_light_text_color\" value=\"";
        // line 1904
        echo (isset($context["t1d_dd_light_text_color"]) ? $context["t1d_dd_light_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_links_color\" id=\"t1d_dd_links_color\" value=\"";
        // line 1910
        echo (isset($context["t1d_dd_links_color"]) ? $context["t1d_dd_links_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Links color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_links_hover_color\" id=\"t1d_dd_links_hover_color\" value=\"";
        // line 1916
        echo (isset($context["t1d_dd_links_hover_color"]) ? $context["t1d_dd_links_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_icons_color\" id=\"t1d_dd_icons_color\" value=\"";
        // line 1922
        echo (isset($context["t1d_dd_icons_color"]) ? $context["t1d_dd_icons_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_icons_hover_color\" id=\"t1d_dd_icons_hover_color\" value=\"";
        // line 1928
        echo (isset($context["t1d_dd_icons_hover_color"]) ? $context["t1d_dd_icons_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Highlighted fields background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_hli_bg_color\" id=\"t1d_dd_hli_bg_color\" value=\"";
        // line 1934
        echo (isset($context["t1d_dd_hli_bg_color"]) ? $context["t1d_dd_hli_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Separator color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1d_dd_separator_color\" id=\"t1d_dd_separator_color\" value=\"";
        // line 1940
        echo (isset($context["t1d_dd_separator_color"]) ? $context["t1d_dd_separator_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Shadow:
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_dd_shadow\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 1948
        if (((isset($context["t1d_dd_shadow"]) ? $context["t1d_dd_shadow"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1949
        if (((isset($context["t1d_dd_shadow"]) ? $context["t1d_dd_shadow"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_dd_shadow"]) ? $context["t1d_dd_shadow"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>

        
        </div>
        </div>
        
        </div>
        </div> 
          
          
          
          
        
        <div class=\"tab-pane\" id=\"tab-backgrounds\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"background_images_settings_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-backgrounds-body\" data-toggle=\"tab\">Body</a></li>
             <li><a href=\"#tab-backgrounds-header\" data-toggle=\"tab\">Header</a></li>
             <li><a href=\"#tab-backgrounds-menu\" data-toggle=\"tab\">Main Menu</a></li>
             <li><a href=\"#tab-backgrounds-footer\" data-toggle=\"tab\">Footer</a></li>                                       
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-backgrounds-body\" class=\"tab-pane active\">  
        
                    <fieldset>
                     
                        <legend>Body <a href=\"view/image/theme_img/help_oxy_theme/cas_77.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_body\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 1996
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1997
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_body"]) ? $context["t1d_pattern_body"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1999
        echo "                                    
                                    ";
        // line 2000
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2001
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_body"]) ? $context["t1d_pattern_body"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2003
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2011
        echo (isset($context["t1d_bg_image_thumb"]) ? $context["t1d_bg_image_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_custom\" value=\"";
        // line 2012
        echo (isset($context["t1d_bg_image_custom"]) ? $context["t1d_bg_image_custom"] : null);
        echo "\" id=\"t1d_bg_image_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2019
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2020
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2021
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2022
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2023
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2024
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2025
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2026
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2027
        if (((isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2035
        if (((isset($context["t1d_bg_image_repeat"]) ? $context["t1d_bg_image_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2036
        if (((isset($context["t1d_bg_image_repeat"]) ? $context["t1d_bg_image_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2037
        if (((isset($context["t1d_bg_image_repeat"]) ? $context["t1d_bg_image_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2038
        if (((isset($context["t1d_bg_image_repeat"]) ? $context["t1d_bg_image_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Attachment:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_attachment\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"scroll\"";
        // line 2046
        if (((isset($context["t1d_bg_image_attachment"]) ? $context["t1d_bg_image_attachment"] : null) == "scroll")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_attachment_s"]) ? $context["text_attachment_s"] : null);
        echo "</option>
                                    <option value=\"fixed\"";
        // line 2047
        if (((isset($context["t1d_bg_image_attachment"]) ? $context["t1d_bg_image_attachment"] : null) == "fixed")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_attachment_f"]) ? $context["text_attachment_f"] : null);
        echo "</option> 
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                    
                    </fieldset> 
          
        </div>
 
        <div id=\"tab-backgrounds-header\" class=\"tab-pane\">    

                    <fieldset>
                     
                        <legend>Header <a href=\"view/image/theme_img/help_oxy_theme/cas_78.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_ta\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2068
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2069
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_ta"]) ? $context["t1d_pattern_k_ta"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2071
        echo "                                    
                                    ";
        // line 2072
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2073
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_ta"]) ? $context["t1d_pattern_k_ta"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2075
        echo "                                  
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_ta_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2083
        echo (isset($context["t1d_bg_image_ta_thumb"]) ? $context["t1d_bg_image_ta_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_ta_custom\" value=\"";
        // line 2084
        echo (isset($context["t1d_bg_image_ta_custom"]) ? $context["t1d_bg_image_ta_custom"] : null);
        echo "\" id=\"t1d_bg_image_ta_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_ta_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2091
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2092
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2093
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2094
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2095
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2096
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2097
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2098
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2099
        if (((isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_ta_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2107
        if (((isset($context["t1d_bg_image_ta_repeat"]) ? $context["t1d_bg_image_ta_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2108
        if (((isset($context["t1d_bg_image_ta_repeat"]) ? $context["t1d_bg_image_ta_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2109
        if (((isset($context["t1d_bg_image_ta_repeat"]) ? $context["t1d_bg_image_ta_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2110
        if (((isset($context["t1d_bg_image_ta_repeat"]) ? $context["t1d_bg_image_ta_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Attachment:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_ta_attachment\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"scroll\"";
        // line 2118
        if (((isset($context["t1d_bg_image_ta_attachment"]) ? $context["t1d_bg_image_ta_attachment"] : null) == "scroll")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_attachment_s"]) ? $context["text_attachment_s"] : null);
        echo "</option>
                                    <option value=\"fixed\"";
        // line 2119
        if (((isset($context["t1d_bg_image_ta_attachment"]) ? $context["t1d_bg_image_ta_attachment"] : null) == "fixed")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_attachment_f"]) ? $context["text_attachment_f"] : null);
        echo "</option> 
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                    
                    </fieldset>

        </div>
        
        <div id=\"tab-backgrounds-menu\" class=\"tab-pane\">    

                    <fieldset>
                     
                        <legend>Main Menu <a href=\"view/image/theme_img/help_oxy_theme/cas_47.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_mm\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2140
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2141
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_mm"]) ? $context["t1d_pattern_k_mm"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2143
        echo "                                    
                                    ";
        // line 2144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2145
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_mm"]) ? $context["t1d_pattern_k_mm"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2147
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_mm_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2155
        echo (isset($context["t1d_bg_image_mm_thumb"]) ? $context["t1d_bg_image_mm_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_mm_custom\" value=\"";
        // line 2156
        echo (isset($context["t1d_bg_image_mm_custom"]) ? $context["t1d_bg_image_mm_custom"] : null);
        echo "\" id=\"t1d_bg_image_mm_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_mm_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2163
        if (((isset($context["t1d_bg_image_mm_repeat"]) ? $context["t1d_bg_image_mm_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2164
        if (((isset($context["t1d_bg_image_mm_repeat"]) ? $context["t1d_bg_image_mm_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2165
        if (((isset($context["t1d_bg_image_mm_repeat"]) ? $context["t1d_bg_image_mm_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2166
        if (((isset($context["t1d_bg_image_mm_repeat"]) ? $context["t1d_bg_image_mm_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                    
                    </fieldset>

        </div>
        
        <div id=\"tab-backgrounds-footer\" class=\"tab-pane\">    

                    <fieldset>
                     
                        <legend>Entire Footer <a href=\"view/image/theme_img/help_oxy_theme/cas_79.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_f1\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2187
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2188
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f1"]) ? $context["t1d_pattern_k_f1"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2190
        echo "                                    
                                    ";
        // line 2191
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2192
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f1"]) ? $context["t1d_pattern_k_f1"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2194
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-2\">                                
                                <a href=\"\" id=\"t1d_bg_image_f1_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2202
        echo (isset($context["t1d_bg_image_f1_thumb"]) ? $context["t1d_bg_image_f1_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_f1_custom\" value=\"";
        // line 2203
        echo (isset($context["t1d_bg_image_f1_custom"]) ? $context["t1d_bg_image_f1_custom"] : null);
        echo "\" id=\"t1d_bg_image_f1_custom\" />
                            </div>
                            <label class=\"col-sm-2 control-label\">Parallax scrolling effect:</label>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f1_parallax\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2208
        if (((isset($context["t1d_bg_image_f1_parallax"]) ? $context["t1d_bg_image_f1_parallax"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2209
        if (((isset($context["t1d_bg_image_f1_parallax"]) ? $context["t1d_bg_image_f1_parallax"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f1_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2217
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2218
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2219
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2220
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2221
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2222
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2223
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2224
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2225
        if (((isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f1_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2233
        if (((isset($context["t1d_bg_image_f1_repeat"]) ? $context["t1d_bg_image_f1_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2234
        if (((isset($context["t1d_bg_image_f1_repeat"]) ? $context["t1d_bg_image_f1_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2235
        if (((isset($context["t1d_bg_image_f1_repeat"]) ? $context["t1d_bg_image_f1_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2236
        if (((isset($context["t1d_bg_image_f1_repeat"]) ? $context["t1d_bg_image_f1_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Top Custom Block <a href=\"view/image/theme_img/help_oxy_theme/cas_82.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_f6\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2249
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2250
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f6"]) ? $context["t1d_pattern_k_f6"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2252
        echo "                                    
                                    ";
        // line 2253
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2254
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f6"]) ? $context["t1d_pattern_k_f6"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2256
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_f6_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2264
        echo (isset($context["t1d_bg_image_f6_thumb"]) ? $context["t1d_bg_image_f6_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_f6_custom\" value=\"";
        // line 2265
        echo (isset($context["t1d_bg_image_f6_custom"]) ? $context["t1d_bg_image_f6_custom"] : null);
        echo "\" id=\"t1d_bg_image_f6_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f6_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2272
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2273
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2274
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2275
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2276
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2277
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2278
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2279
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2280
        if (((isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f6_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2288
        if (((isset($context["t1d_bg_image_f6_repeat"]) ? $context["t1d_bg_image_f6_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>

                                    <option value=\"repeat-x\"";
        // line 2290
        if (((isset($context["t1d_bg_image_f6_repeat"]) ? $context["t1d_bg_image_f6_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2291
        if (((isset($context["t1d_bg_image_f6_repeat"]) ? $context["t1d_bg_image_f6_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2292
        if (((isset($context["t1d_bg_image_f6_repeat"]) ? $context["t1d_bg_image_f6_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <legend>Information, Custom Columns <a href=\"view/image/theme_img/help_oxy_theme/cas_80.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_f2\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2305
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2306
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f2"]) ? $context["t1d_pattern_k_f2"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2308
        echo "                                    
                                    ";
        // line 2309
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2310
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f2"]) ? $context["t1d_pattern_k_f2"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2312
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_f2_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2320
        echo (isset($context["t1d_bg_image_f2_thumb"]) ? $context["t1d_bg_image_f2_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_f2_custom\" value=\"";
        // line 2321
        echo (isset($context["t1d_bg_image_f2_custom"]) ? $context["t1d_bg_image_f2_custom"] : null);
        echo "\" id=\"t1d_bg_image_f2_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div> 
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f2_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2328
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2329
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2330
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2331
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2332
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2333
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2334
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2335
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2336
        if (((isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f2_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2344
        if (((isset($context["t1d_bg_image_f2_repeat"]) ? $context["t1d_bg_image_f2_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2345
        if (((isset($context["t1d_bg_image_f2_repeat"]) ? $context["t1d_bg_image_f2_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2346
        if (((isset($context["t1d_bg_image_f2_repeat"]) ? $context["t1d_bg_image_f2_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2347
        if (((isset($context["t1d_bg_image_f2_repeat"]) ? $context["t1d_bg_image_f2_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> 
                                             
                        <legend>Powered by, Payment Images, Follow Us <a href=\"view/image/theme_img/help_oxy_theme/cas_81.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_f3\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2360
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2361
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f3"]) ? $context["t1d_pattern_k_f3"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2363
        echo "                                    
                                    ";
        // line 2364
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2365
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f3"]) ? $context["t1d_pattern_k_f3"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2367
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_f3_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2375
        echo (isset($context["t1d_bg_image_f3_thumb"]) ? $context["t1d_bg_image_f3_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_f3_custom\" value=\"";
        // line 2376
        echo (isset($context["t1d_bg_image_f3_custom"]) ? $context["t1d_bg_image_f3_custom"] : null);
        echo "\" id=\"t1d_bg_image_f3_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f3_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2383
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2384
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2385
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2386
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2387
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2388
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2389
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2390
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2391
        if (((isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f3_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2399
        if (((isset($context["t1d_bg_image_f3_repeat"]) ? $context["t1d_bg_image_f3_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2400
        if (((isset($context["t1d_bg_image_f3_repeat"]) ? $context["t1d_bg_image_f3_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2401
        if (((isset($context["t1d_bg_image_f3_repeat"]) ? $context["t1d_bg_image_f3_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2402
        if (((isset($context["t1d_bg_image_f3_repeat"]) ? $context["t1d_bg_image_f3_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                                             
                        <legend>Bottom Custom Block <a href=\"view/image/theme_img/help_oxy_theme/cas_82.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_f4\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2415
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2416
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f4"]) ? $context["t1d_pattern_k_f4"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2418
        echo "                                    
                                    ";
        // line 2419
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2420
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f4"]) ? $context["t1d_pattern_k_f4"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2422
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_f4_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2430
        echo (isset($context["t1d_bg_image_f4_thumb"]) ? $context["t1d_bg_image_f4_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_f4_custom\" value=\"";
        // line 2431
        echo (isset($context["t1d_bg_image_f4_custom"]) ? $context["t1d_bg_image_f4_custom"] : null);
        echo "\" id=\"t1d_bg_image_f4_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f4_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2438
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2439
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2440
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2441
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2442
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2443
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2444
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2445
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2446
        if (((isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f4_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2454
        if (((isset($context["t1d_bg_image_f4_repeat"]) ? $context["t1d_bg_image_f4_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2455
        if (((isset($context["t1d_bg_image_f4_repeat"]) ? $context["t1d_bg_image_f4_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2456
        if (((isset($context["t1d_bg_image_f4_repeat"]) ? $context["t1d_bg_image_f4_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2457
        if (((isset($context["t1d_bg_image_f4_repeat"]) ? $context["t1d_bg_image_f4_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Sliding Bottom Custom Block <a href=\"view/image/theme_img/help_oxy_theme/cas_82.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\"></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_pattern_k_f5\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"none\"selected=\"selected\">none</option>
                                    
                                    ";
        // line 2470
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2471
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f5"]) ? $context["t1d_pattern_k_f5"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2473
        echo "                                    
                                    ";
        // line 2474
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2475
            echo "\t\t\t\t\t\t\t\t\t<option value=\"";
            echo $context["i"];
            echo "\" ";
            if (((isset($context["t1d_pattern_k_f5"]) ? $context["t1d_pattern_k_f5"] : null) == $context["i"])) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo $context["i"];
            echo "</option>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2477
        echo "                                    
\t\t\t\t\t\t\t\t</select>
                                <span class=\"k_help\">Select a pattern number.</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Upload your own pattern or background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1d_bg_image_f5_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2485
        echo (isset($context["t1d_bg_image_f5_thumb"]) ? $context["t1d_bg_image_f5_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1d_bg_image_f5_custom\" value=\"";
        // line 2486
        echo (isset($context["t1d_bg_image_f5_custom"]) ? $context["t1d_bg_image_f5_custom"] : null);
        echo "\" id=\"t1d_bg_image_f5_custom\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f5_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"top center\"";
        // line 2493
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "top center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tc"]) ? $context["text_position_tc"] : null);
        echo "</option>
                           \t    \t<option value=\"top left\"";
        // line 2494
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "top left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tl"]) ? $context["text_position_tl"] : null);
        echo "</option>
                           \t    \t<option value=\"top right\"";
        // line 2495
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "top right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_tr"]) ? $context["text_position_tr"] : null);
        echo "</option> 
                           \t    \t<option value=\"center\"";
        // line 2496
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                           \t    \t<option value=\"left\"";
        // line 2497
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
                           \t    \t<option value=\"right\"";
        // line 2498
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_r"]) ? $context["text_position_r"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom center\"";
        // line 2499
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "bottom center")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bc"]) ? $context["text_position_bc"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom left\"";
        // line 2500
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "bottom left")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_bl"]) ? $context["text_position_bl"] : null);
        echo "</option>
                           \t    \t<option value=\"bottom right\"";
        // line 2501
        if (((isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null) == "bottom right")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_br"]) ? $context["text_position_br"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Repeat:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1d_bg_image_f5_repeat\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"repeat\"";
        // line 2509
        if (((isset($context["t1d_bg_image_f5_repeat"]) ? $context["t1d_bg_image_f5_repeat"] : null) == "repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_r"]) ? $context["text_repeat_r"] : null);
        echo "</option>
                                    <option value=\"repeat-x\"";
        // line 2510
        if (((isset($context["t1d_bg_image_f5_repeat"]) ? $context["t1d_bg_image_f5_repeat"] : null) == "repeat-x")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_rx"]) ? $context["text_repeat_rx"] : null);
        echo "</option> 
                                    <option value=\"repeat-y\"";
        // line 2511
        if (((isset($context["t1d_bg_image_f5_repeat"]) ? $context["t1d_bg_image_f5_repeat"] : null) == "repeat-y")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_ry"]) ? $context["text_repeat_ry"] : null);
        echo "</option>
                                    <option value=\"no-repeat\"";
        // line 2512
        if (((isset($context["t1d_bg_image_f5_repeat"]) ? $context["t1d_bg_image_f5_repeat"] : null) == "no-repeat")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_repeat_nr"]) ? $context["text_repeat_nr"] : null);
        echo "</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>

        </div>



        <div class=\"vtabs-content\">            
        <table class=\"form\">
        
\t\t\t\t\t<tr>
\t\t\t\t\t    <td>
                        <br /><div>Transparent patterns:</div><br />
                        
                        <div style=\"float:left;margin-bottom:20px\">
                        ";
        // line 2531
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 75));
        foreach ($context['_seq'] as $context["_key"] => $context["bgp"]) {
            echo " 
                            <div class=\"ptn\"><img src=\"../catalog/view/theme/oxy/image/patterns/admin_thumb/p";
            // line 2532
            echo $context["bgp"];
            echo ".png\"><span class=\"ptn_nr\">";
            echo $context["bgp"];
            echo "</span></div> 
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bgp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2534
        echo "\t\t\t\t\t\t</div>

                        <div style=\"float:left;margin-bottom:20px\">Non-transparent patterns:</div>   
                        
                        <div style=\"float:left;\">
                        ";
        // line 2539
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(101, 379));
        foreach ($context['_seq'] as $context["_key"] => $context["bgp"]) {
            echo " 
                            <div class=\"ptn\"><img src=\"../catalog/view/theme/oxy/image/patterns/admin_thumb/p";
            // line 2540
            echo $context["bgp"];
            echo ".png\"><span class=\"ptn_nr\">";
            echo $context["bgp"];
            echo "</span></div> 
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['bgp'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2542
        echo "\t\t\t\t\t\t</div> 
                            
\t\t\t\t\t    </td>
\t\t\t\t\t</tr>
                    
        </table>  
        </div>
        
        </div>
        </div>
        
        </div>
        </div> 
        
        
        
        
        <div class=\"tab-pane\" id=\"tab-fonts\"> 
        <div class=\"row form-horizontal\">  

        <div class=\"col-sm-12\">
        <div class=\"tab-content\">
        
                    <fieldset>

                        <a href=\"http://www.google.com/webfonts/\" target=\"_blank\" class=\"btn btn-default link\" style=\"margin-left:0\">Google Web Fonts Collection &raquo;</a><br /><br />
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
                                    <th class=\"left\" width=\"20%\"></th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"20%\">Name:</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"15%\">";
        // line 2576
        echo (isset($context["text_weight"]) ? $context["text_weight"] : null);
        echo "</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"15%\">";
        // line 2577
        echo (isset($context["text_size"]) ? $context["text_size"] : null);
        echo "</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"15%\">";
        // line 2578
        echo (isset($context["text_uppercase"]) ? $context["text_uppercase"] : null);
        echo "</th>
                                    <th class=\"left\" width=\"15%\">Style:</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
                                <tbody>
                                    <tr>
\t\t\t\t\t\t\t\t\t<td>Body:</td>
\t\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t    <input type=\"text\" name=\"t1d_body_font\" value=\"";
        // line 2586
        echo (((isset($context["t1d_body_font"]) ? $context["t1d_body_font"] : null)) ? ((isset($context["t1d_body_font"]) ? $context["t1d_body_font"] : null)) : ("Lato"));
        echo "\" class=\"form-control\" />
                                    <br /><span class=\"k_help\">Default font:<br /><b>Lato</b></span>
                                    </td>
                                    <td></td>
                                    <td>
                                    <select name=\"t1d_body_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2592
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2593
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2594
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2595
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2596
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2597
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2598
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2599
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>      
                           \t            <option value=\"18\"";
        // line 2600
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
                           \t            <option value=\"19\"";
        // line 2601
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "19")) {
            echo "selected=\"selected\"";
        }
        echo ">19px</option>      
                           \t            <option value=\"20\"";
        // line 2602
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option>
                           \t            <option value=\"22\"";
        // line 2603
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "22")) {
            echo "selected=\"selected\"";
        }
        echo ">22px</option>      
                           \t            <option value=\"24\"";
        // line 2604
        if (((isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null) == "24")) {
            echo "selected=\"selected\"";
        }
        echo ">24px</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_body_font_uppercase\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"0\"";
        // line 2609
        if (((isset($context["t1d_body_font_uppercase"]) ? $context["t1d_body_font_uppercase"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                        <option value=\"1\"";
        // line 2610
        if (((isset($context["t1d_body_font_uppercase"]) ? $context["t1d_body_font_uppercase"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    </tr>
                                    
                                    <tr>
\t\t\t\t\t\t\t\t\t<td>Small text:</td>
\t\t\t\t\t\t\t\t\t<td></td>
                                    <td></td>
                                    <td>
                                    <select name=\"t1d_small_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2622
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2623
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2624
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2625
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2626
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2627
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2628
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2629
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>      
                           \t            <option value=\"18\"";
        // line 2630
        if (((isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                   
                                    <tr>
                                    <td>Headings and Product Names:</td>
\t\t\t\t\t\t\t\t\t<td>
                                    <input type=\"text\" name=\"t1d_title_font\" value=\"";
        // line 2640
        echo (((isset($context["t1d_title_font"]) ? $context["t1d_title_font"] : null)) ? ((isset($context["t1d_title_font"]) ? $context["t1d_title_font"] : null)) : ("Lato"));
        echo "\" class=\"form-control\" />
                                    <br /><span class=\"k_help\">Default font:<br /><b>Lato</b></span>
                                    </td>
                                    <td>
                                    <select name=\"t1d_title_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2645
        if (((isset($context["t1d_title_font_weight"]) ? $context["t1d_title_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2646
        if (((isset($context["t1d_title_font_weight"]) ? $context["t1d_title_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_title_font_weight"]) ? $context["t1d_title_font_weight"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
                                        <option value=\"900\"";
        // line 2647
        if (((isset($context["t1d_title_font_weight"]) ? $context["t1d_title_font_weight"] : null) == "900")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_fat"]) ? $context["text_fat"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    <td>
                                    <select name=\"t1d_title_font_uppercase\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"0\"";
        // line 2653
        if (((isset($context["t1d_title_font_uppercase"]) ? $context["t1d_title_font_uppercase"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                        <option value=\"1\"";
        // line 2654
        if (((isset($context["t1d_title_font_uppercase"]) ? $context["t1d_title_font_uppercase"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_title_font_uppercase"]) ? $context["t1d_title_font_uppercase"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    </tr>
                                    
                                    <tr>
                                    <td>Subtitles:</td>
\t\t\t\t\t\t\t\t\t<td>
                                    <input type=\"text\" name=\"t1d_subtitle_font\" value=\"";
        // line 2663
        echo (((isset($context["t1d_subtitle_font"]) ? $context["t1d_subtitle_font"] : null)) ? ((isset($context["t1d_subtitle_font"]) ? $context["t1d_subtitle_font"] : null)) : ("Lato"));
        echo "\" class=\"form-control\" />
                                    <br /><span class=\"k_help\">Default font:<br /><b>Lato</b></span>
                                    </td>
                                    <td>
                                    <select name=\"t1d_subtitle_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2668
        if (((isset($context["t1d_subtitle_font_weight"]) ? $context["t1d_subtitle_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2669
        if (((isset($context["t1d_subtitle_font_weight"]) ? $context["t1d_subtitle_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_subtitle_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2674
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2675
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2676
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2677
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2678
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2679
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2680
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2681
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>      
                           \t            <option value=\"18\"";
        // line 2682
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
                                        <option value=\"19\"";
        // line 2683
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "19")) {
            echo "selected=\"selected\"";
        }
        echo ">19px</option> 
                                        <option value=\"20\"";
        // line 2684
        if (((isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option> 
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_subtitle_font_uppercase\" class=\"form-control\">
                                        <option value=\"0\"";
        // line 2689
        if (((isset($context["t1d_subtitle_font_uppercase"]) ? $context["t1d_subtitle_font_uppercase"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                        <option value=\"1\"";
        // line 2690
        if (((isset($context["t1d_subtitle_font_uppercase"]) ? $context["t1d_subtitle_font_uppercase"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_subtitle_font_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2695
        if (((isset($context["t1d_subtitle_font_style"]) ? $context["t1d_subtitle_font_style"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">Normal</option>
                                        <option value=\"italic\"";
        // line 2696
        if (((isset($context["t1d_subtitle_font_style"]) ? $context["t1d_subtitle_font_style"] : null) == "italic")) {
            echo "selected=\"selected\"";
        }
        echo ">Italic</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    </tr>
                                    
                                    <tr>
                                    <td>Prices:</td>
\t\t\t\t\t\t\t\t\t<td></td>
                                    <td>
                                    <select name=\"t1d_price_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2706
        if (((isset($context["t1d_price_font_weight"]) ? $context["t1d_price_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2707
        if (((isset($context["t1d_price_font_weight"]) ? $context["t1d_price_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_price_font_weight"]) ? $context["t1d_price_font_weight"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                    
                                    <tr>
                                    <td>Buttons:</td>
\t\t\t\t\t\t\t\t\t<td></td>
                                    <td>
                                    <select name=\"t1d_button_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2720
        if (((isset($context["t1d_button_font_weight"]) ? $context["t1d_button_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2721
        if (((isset($context["t1d_button_font_weight"]) ? $context["t1d_button_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_button_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2726
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2727
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2728
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2729
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2730
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2731
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2732
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2733
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>      
                           \t            <option value=\"18\"";
        // line 2734
        if (((isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_button_font_uppercase\" class=\"form-control\">
                                        <option value=\"0\"";
        // line 2739
        if (((isset($context["t1d_button_font_uppercase"]) ? $context["t1d_button_font_uppercase"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                        <option value=\"1\"";
        // line 2740
        if (((isset($context["t1d_button_font_uppercase"]) ? $context["t1d_button_font_uppercase"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    </tr>
                                                                   
                                    <tr>
                                    <td>Main Menu Bar:</td>
\t\t\t\t\t\t\t\t\t<td></td>
                                    <td>
                                    <select name=\"t1d_mm_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2751
        if (((isset($context["t1d_mm_font_weight"]) ? $context["t1d_mm_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2752
        if (((isset($context["t1d_mm_font_weight"]) ? $context["t1d_mm_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_font_weight"]) ? $context["t1d_mm_font_weight"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
                                        <option value=\"900\"";
        // line 2753
        if (((isset($context["t1d_mm_font_weight"]) ? $context["t1d_mm_font_weight"] : null) == "900")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_fat"]) ? $context["text_fat"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_mm_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2758
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2759
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2760
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2761
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2762
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2763
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2764
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2765
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>
                           \t            <option value=\"18\"";
        // line 2766
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
                           \t            <option value=\"19\"";
        // line 2767
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "19")) {
            echo "selected=\"selected\"";
        }
        echo ">19px</option>      
                           \t            <option value=\"20\"";
        // line 2768
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option>
                           \t            <option value=\"22\"";
        // line 2769
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "22")) {
            echo "selected=\"selected\"";
        }
        echo ">22px</option>      
                           \t            <option value=\"24\"";
        // line 2770
        if (((isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null) == "24")) {
            echo "selected=\"selected\"";
        }
        echo ">24px</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_mm_font_uppercase\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"0\"";
        // line 2775
        if (((isset($context["t1d_mm_font_uppercase"]) ? $context["t1d_mm_font_uppercase"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                        <option value=\"1\"";
        // line 2776
        if (((isset($context["t1d_mm_font_uppercase"]) ? $context["t1d_mm_font_uppercase"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_font_uppercase"]) ? $context["t1d_mm_font_uppercase"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    </tr>
                                    
                                    <tr>
                                    <td>Sub-Menu Main Categories:</td>
\t\t\t\t\t\t\t\t\t<td></td>
                                    <td>
                                    <select name=\"t1d_mm_sub_main_cat_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2787
        if (((isset($context["t1d_mm_sub_main_cat_font_weight"]) ? $context["t1d_mm_sub_main_cat_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2788
        if (((isset($context["t1d_mm_sub_main_cat_font_weight"]) ? $context["t1d_mm_sub_main_cat_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sub_main_cat_font_weight"]) ? $context["t1d_mm_sub_main_cat_font_weight"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_mm_sub_main_cat_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2793
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2794
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2795
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2796
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2797
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2798
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2799
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2800
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>
                           \t            <option value=\"18\"";
        // line 2801
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
                                        <option value=\"19\"";
        // line 2802
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "19")) {
            echo "selected=\"selected\"";
        }
        echo ">19px</option>      
                           \t            <option value=\"20\"";
        // line 2803
        if (((isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                    
                                    <tr>
                                    <td>Sub-Menu Other Links:</td>
\t\t\t\t\t\t\t\t\t<td></td>
                                    <td>
                                    <select name=\"t1d_mm_sub_font_weight\" class=\"form-control\">
\t\t\t\t\t\t\t\t        <option value=\"normal\"";
        // line 2815
        if (((isset($context["t1d_mm_sub_font_weight"]) ? $context["t1d_mm_sub_font_weight"] : null) == "normal")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_normal"]) ? $context["text_normal"] : null);
        echo "</option>
                                        <option value=\"bold\"";
        // line 2816
        if (((isset($context["t1d_mm_sub_font_weight"]) ? $context["t1d_mm_sub_font_weight"] : null) == "bold")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_bold"]) ? $context["text_bold"] : null);
        echo "</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td>
                                    <select name=\"t1d_mm_sub_font_size\" class=\"form-control\">
                                        <option value=\"10\"";
        // line 2821
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                        <option value=\"11\"";
        // line 2822
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11px</option>
\t\t\t\t\t\t\t\t        <option value=\"12\"";
        // line 2823
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12px</option>
                           \t            <option value=\"13\"";
        // line 2824
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">13px</option>      
                           \t            <option value=\"14\"";
        // line 2825
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "14")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">14px</option>      
                           \t            <option value=\"15\"";
        // line 2826
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">15px</option>      
                           \t            <option value=\"16\"";
        // line 2827
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "16")) {
            echo "selected=\"selected\"";
        }
        echo ">16px</option>
                           \t            <option value=\"17\"";
        // line 2828
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "17")) {
            echo "selected=\"selected\"";
        }
        echo ">17px</option>
                           \t            <option value=\"18\"";
        // line 2829
        if (((isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null) == "18")) {
            echo "selected=\"selected\"";
        }
        echo ">18px</option>
\t\t\t\t\t\t\t        </select>
                                    </td>
                                    <td></td>
                                    <td></td>
                                    </tr>

                                </tbody>
\t\t\t\t\t\t\t</table>
                        
                        </div>                    
                        
                    </fieldset>
        
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        <div class=\"tab-pane\" id=\"tab-skins\"> 
        <div class=\"row form-horizontal\">  

        <div class=\"col-sm-12\">
        <div class=\"tab-content\">
        
            <label>Color Scheme: </label>           
            <select name=\"t1d_skin\" class=\"form-control\">
                 <option value=\"skin1-default\"";
        // line 2859
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin1-default")) {
            echo "selected=\"selected\"";
        }
        echo ">Default</option>
                 <option value=\"skin2-fashion\"";
        // line 2860
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin2-fashion")) {
            echo "selected=\"selected\"";
        }
        echo ">Fashion</option>  
                 <option value=\"skin3-sport\"";
        // line 2861
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin3-sport")) {
            echo "selected=\"selected\"";
        }
        echo ">Sport</option>
                 <option value=\"skin4-organic\"";
        // line 2862
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin4-organic")) {
            echo "selected=\"selected\"";
        }
        echo ">Organic</option>
                 <option value=\"skin5-kids\"";
        // line 2863
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin5-kids")) {
            echo "selected=\"selected\"";
        }
        echo ">Kids</option>                 
            </select> 
            
            <p><br /><br />       
            This option enable the selected color scheme in your store. By default, only \"Default\" color scheme can be edited in the admin panel. All other color schemes are pre-made and can not be edited in the admin panel.
            </p>
            <p><br />
            If you want to make your store have the same settings like our demo (layout, main menu, etc.), please import our demo content (for more information, see the \"Demo Content\" section in the theme documentation) or copy the settings of a theme modules from our demo. 
            </p>
            <p><br />
            We provide access to the admin sections:
            </p>
            <p>
            Extensions › Modules
            <br />
            System › Design › Layouts
            <br />
            System › Design › Banners
            </p>
            <p><br />
            Access to the admin panel for each demo:
            </p>
            <p>
            Username: demo
            <br />
            Password: demo
            </p>
        
        </div>
        </div>
        
        </div>
        </div> 
        
        




        <!-- -->         
        </div>  
        
        
    </form>
    </div>
  </div>
</div>

<script type=\"text/javascript\" src=\"view/javascript/jscolor/jscolor.js\"></script>
<script type=\"text/javascript\" src=\"view/javascript/poshytip/jquery.poshytip.js\"></script>
<link rel=\"stylesheet\" type=\"text/css\" href=\"view/javascript/poshytip/tip-twitter/tip-twitter.css\" />

";
        // line 2915
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/oxy_theme_design.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  6610 => 2915,  6553 => 2863,  6547 => 2862,  6541 => 2861,  6535 => 2860,  6529 => 2859,  6494 => 2829,  6488 => 2828,  6482 => 2827,  6476 => 2826,  6467 => 2825,  6461 => 2824,  6455 => 2823,  6449 => 2822,  6443 => 2821,  6431 => 2816,  6423 => 2815,  6406 => 2803,  6400 => 2802,  6394 => 2801,  6388 => 2800,  6382 => 2799,  6376 => 2798,  6367 => 2797,  6361 => 2796,  6355 => 2795,  6349 => 2794,  6343 => 2793,  6328 => 2788,  6320 => 2787,  6301 => 2776,  6295 => 2775,  6285 => 2770,  6279 => 2769,  6273 => 2768,  6267 => 2767,  6261 => 2766,  6255 => 2765,  6249 => 2764,  6243 => 2763,  6234 => 2762,  6228 => 2761,  6222 => 2760,  6216 => 2759,  6210 => 2758,  6198 => 2753,  6187 => 2752,  6179 => 2751,  6163 => 2740,  6157 => 2739,  6147 => 2734,  6141 => 2733,  6135 => 2732,  6129 => 2731,  6123 => 2730,  6117 => 2729,  6108 => 2728,  6102 => 2727,  6096 => 2726,  6084 => 2721,  6076 => 2720,  6053 => 2707,  6045 => 2706,  6030 => 2696,  6024 => 2695,  6014 => 2690,  6008 => 2689,  5998 => 2684,  5992 => 2683,  5986 => 2682,  5980 => 2681,  5974 => 2680,  5968 => 2679,  5959 => 2678,  5953 => 2677,  5947 => 2676,  5941 => 2675,  5935 => 2674,  5923 => 2669,  5915 => 2668,  5907 => 2663,  5890 => 2654,  5884 => 2653,  5871 => 2647,  5860 => 2646,  5852 => 2645,  5844 => 2640,  5829 => 2630,  5823 => 2629,  5817 => 2628,  5811 => 2627,  5805 => 2626,  5799 => 2625,  5790 => 2624,  5784 => 2623,  5778 => 2622,  5761 => 2610,  5755 => 2609,  5745 => 2604,  5739 => 2603,  5733 => 2602,  5727 => 2601,  5721 => 2600,  5715 => 2599,  5709 => 2598,  5703 => 2597,  5694 => 2596,  5688 => 2595,  5682 => 2594,  5676 => 2593,  5670 => 2592,  5661 => 2586,  5650 => 2578,  5646 => 2577,  5642 => 2576,  5606 => 2542,  5596 => 2540,  5590 => 2539,  5583 => 2534,  5573 => 2532,  5567 => 2531,  5541 => 2512,  5533 => 2511,  5525 => 2510,  5517 => 2509,  5502 => 2501,  5494 => 2500,  5486 => 2499,  5478 => 2498,  5470 => 2497,  5462 => 2496,  5454 => 2495,  5446 => 2494,  5438 => 2493,  5428 => 2486,  5422 => 2485,  5412 => 2477,  5397 => 2475,  5393 => 2474,  5390 => 2473,  5375 => 2471,  5371 => 2470,  5351 => 2457,  5343 => 2456,  5335 => 2455,  5327 => 2454,  5312 => 2446,  5304 => 2445,  5296 => 2444,  5288 => 2443,  5280 => 2442,  5272 => 2441,  5264 => 2440,  5256 => 2439,  5248 => 2438,  5238 => 2431,  5232 => 2430,  5222 => 2422,  5207 => 2420,  5203 => 2419,  5200 => 2418,  5185 => 2416,  5181 => 2415,  5161 => 2402,  5153 => 2401,  5145 => 2400,  5137 => 2399,  5122 => 2391,  5114 => 2390,  5106 => 2389,  5098 => 2388,  5090 => 2387,  5082 => 2386,  5074 => 2385,  5066 => 2384,  5058 => 2383,  5048 => 2376,  5042 => 2375,  5032 => 2367,  5017 => 2365,  5013 => 2364,  5010 => 2363,  4995 => 2361,  4991 => 2360,  4971 => 2347,  4963 => 2346,  4955 => 2345,  4947 => 2344,  4932 => 2336,  4924 => 2335,  4916 => 2334,  4908 => 2333,  4900 => 2332,  4892 => 2331,  4884 => 2330,  4876 => 2329,  4868 => 2328,  4858 => 2321,  4852 => 2320,  4842 => 2312,  4827 => 2310,  4823 => 2309,  4820 => 2308,  4805 => 2306,  4801 => 2305,  4781 => 2292,  4773 => 2291,  4765 => 2290,  4756 => 2288,  4741 => 2280,  4733 => 2279,  4725 => 2278,  4717 => 2277,  4709 => 2276,  4701 => 2275,  4693 => 2274,  4685 => 2273,  4677 => 2272,  4667 => 2265,  4661 => 2264,  4651 => 2256,  4636 => 2254,  4632 => 2253,  4629 => 2252,  4614 => 2250,  4610 => 2249,  4590 => 2236,  4582 => 2235,  4574 => 2234,  4566 => 2233,  4551 => 2225,  4543 => 2224,  4535 => 2223,  4527 => 2222,  4519 => 2221,  4511 => 2220,  4503 => 2219,  4495 => 2218,  4487 => 2217,  4474 => 2209,  4468 => 2208,  4460 => 2203,  4454 => 2202,  4444 => 2194,  4429 => 2192,  4425 => 2191,  4422 => 2190,  4407 => 2188,  4403 => 2187,  4375 => 2166,  4367 => 2165,  4359 => 2164,  4351 => 2163,  4341 => 2156,  4335 => 2155,  4325 => 2147,  4310 => 2145,  4306 => 2144,  4303 => 2143,  4288 => 2141,  4284 => 2140,  4256 => 2119,  4248 => 2118,  4233 => 2110,  4225 => 2109,  4217 => 2108,  4209 => 2107,  4194 => 2099,  4186 => 2098,  4178 => 2097,  4170 => 2096,  4162 => 2095,  4154 => 2094,  4146 => 2093,  4138 => 2092,  4130 => 2091,  4120 => 2084,  4114 => 2083,  4104 => 2075,  4089 => 2073,  4085 => 2072,  4082 => 2071,  4067 => 2069,  4063 => 2068,  4035 => 2047,  4027 => 2046,  4012 => 2038,  4004 => 2037,  3996 => 2036,  3988 => 2035,  3973 => 2027,  3965 => 2026,  3957 => 2025,  3949 => 2024,  3941 => 2023,  3933 => 2022,  3925 => 2021,  3917 => 2020,  3909 => 2019,  3899 => 2012,  3893 => 2011,  3883 => 2003,  3868 => 2001,  3864 => 2000,  3861 => 1999,  3846 => 1997,  3842 => 1996,  3787 => 1949,  3781 => 1948,  3770 => 1940,  3761 => 1934,  3752 => 1928,  3743 => 1922,  3734 => 1916,  3725 => 1910,  3716 => 1904,  3707 => 1898,  3698 => 1892,  3688 => 1885,  3668 => 1868,  3659 => 1862,  3650 => 1856,  3641 => 1850,  3626 => 1843,  3620 => 1842,  3607 => 1832,  3598 => 1826,  3589 => 1820,  3580 => 1814,  3565 => 1807,  3559 => 1806,  3540 => 1795,  3534 => 1794,  3521 => 1786,  3515 => 1785,  3509 => 1784,  3503 => 1783,  3497 => 1782,  3491 => 1781,  3485 => 1780,  3479 => 1779,  3473 => 1778,  3464 => 1777,  3458 => 1776,  3452 => 1775,  3431 => 1757,  3422 => 1751,  3413 => 1745,  3394 => 1729,  3385 => 1725,  3379 => 1724,  3373 => 1723,  3367 => 1722,  3361 => 1721,  3351 => 1716,  3345 => 1715,  3334 => 1707,  3325 => 1701,  3316 => 1695,  3307 => 1689,  3292 => 1682,  3286 => 1681,  3273 => 1671,  3264 => 1667,  3258 => 1666,  3252 => 1665,  3246 => 1664,  3240 => 1663,  3227 => 1658,  3221 => 1657,  3210 => 1649,  3201 => 1643,  3192 => 1637,  3183 => 1631,  3168 => 1624,  3162 => 1623,  3149 => 1613,  3140 => 1609,  3134 => 1608,  3128 => 1607,  3122 => 1606,  3116 => 1605,  3103 => 1600,  3097 => 1599,  3080 => 1590,  3074 => 1589,  3058 => 1581,  3052 => 1580,  3042 => 1573,  3033 => 1567,  3024 => 1561,  3015 => 1555,  3006 => 1549,  2991 => 1542,  2985 => 1541,  2972 => 1531,  2963 => 1527,  2957 => 1526,  2951 => 1525,  2945 => 1524,  2939 => 1523,  2926 => 1518,  2920 => 1517,  2909 => 1509,  2900 => 1503,  2891 => 1497,  2882 => 1491,  2873 => 1487,  2867 => 1486,  2861 => 1485,  2855 => 1484,  2849 => 1483,  2836 => 1478,  2830 => 1477,  2819 => 1469,  2810 => 1463,  2795 => 1456,  2789 => 1455,  2776 => 1445,  2767 => 1441,  2761 => 1440,  2755 => 1439,  2749 => 1438,  2743 => 1437,  2733 => 1432,  2727 => 1431,  2716 => 1423,  2707 => 1417,  2698 => 1411,  2689 => 1405,  2674 => 1398,  2668 => 1397,  2646 => 1378,  2636 => 1371,  2626 => 1364,  2616 => 1357,  2589 => 1338,  2583 => 1337,  2569 => 1328,  2563 => 1327,  2546 => 1318,  2540 => 1317,  2529 => 1309,  2513 => 1301,  2507 => 1300,  2497 => 1293,  2487 => 1286,  2477 => 1279,  2467 => 1272,  2457 => 1265,  2445 => 1256,  2436 => 1250,  2427 => 1244,  2418 => 1238,  2406 => 1231,  2400 => 1230,  2387 => 1220,  2378 => 1214,  2369 => 1208,  2360 => 1202,  2348 => 1195,  2342 => 1194,  2329 => 1184,  2320 => 1178,  2311 => 1172,  2302 => 1166,  2290 => 1159,  2284 => 1158,  2271 => 1148,  2262 => 1142,  2253 => 1136,  2244 => 1130,  2232 => 1123,  2226 => 1122,  2213 => 1112,  2204 => 1106,  2195 => 1100,  2186 => 1094,  2171 => 1087,  2165 => 1086,  2152 => 1076,  2142 => 1069,  2132 => 1062,  2122 => 1055,  2107 => 1048,  2101 => 1047,  2081 => 1035,  2075 => 1034,  2058 => 1025,  2052 => 1024,  2040 => 1015,  2031 => 1011,  2025 => 1010,  2019 => 1009,  2013 => 1008,  2007 => 1007,  1997 => 1002,  1991 => 1001,  1978 => 991,  1969 => 987,  1963 => 986,  1957 => 985,  1951 => 984,  1945 => 983,  1935 => 978,  1929 => 977,  1916 => 967,  1907 => 963,  1901 => 962,  1895 => 961,  1889 => 960,  1883 => 959,  1873 => 954,  1867 => 953,  1855 => 944,  1840 => 937,  1834 => 936,  1812 => 917,  1803 => 911,  1793 => 904,  1783 => 897,  1774 => 891,  1761 => 883,  1755 => 882,  1745 => 875,  1729 => 867,  1723 => 866,  1710 => 856,  1701 => 850,  1692 => 844,  1683 => 838,  1674 => 832,  1662 => 823,  1646 => 815,  1640 => 814,  1630 => 807,  1620 => 800,  1610 => 793,  1601 => 787,  1585 => 779,  1579 => 778,  1557 => 759,  1548 => 753,  1538 => 746,  1528 => 739,  1518 => 732,  1508 => 725,  1498 => 718,  1485 => 708,  1475 => 701,  1466 => 695,  1456 => 688,  1446 => 681,  1436 => 674,  1426 => 667,  1413 => 657,  1404 => 651,  1394 => 644,  1384 => 637,  1374 => 630,  1364 => 623,  1354 => 616,  1341 => 606,  1332 => 600,  1322 => 593,  1312 => 586,  1302 => 579,  1292 => 572,  1282 => 565,  1267 => 555,  1261 => 554,  1252 => 553,  1246 => 552,  1240 => 551,  1234 => 550,  1218 => 542,  1212 => 541,  1196 => 533,  1190 => 532,  1176 => 521,  1162 => 512,  1156 => 511,  1145 => 503,  1136 => 497,  1117 => 486,  1111 => 485,  1092 => 474,  1086 => 473,  1074 => 464,  1057 => 455,  1051 => 454,  1025 => 433,  1019 => 432,  1008 => 424,  998 => 417,  989 => 411,  979 => 404,  969 => 397,  959 => 390,  950 => 384,  934 => 376,  928 => 375,  917 => 367,  907 => 360,  852 => 308,  847 => 306,  840 => 302,  836 => 300,  828 => 296,  826 => 295,  820 => 291,  809 => 289,  805 => 288,  800 => 286,  794 => 285,  790 => 284,  780 => 277,  672 => 171,  668 => 170,  664 => 169,  660 => 168,  656 => 167,  652 => 166,  649 => 165,  645 => 164,  641 => 163,  637 => 162,  633 => 161,  629 => 160,  626 => 159,  622 => 158,  618 => 157,  614 => 156,  610 => 155,  606 => 154,  602 => 153,  599 => 152,  595 => 151,  591 => 150,  587 => 149,  583 => 148,  579 => 147,  575 => 146,  571 => 145,  568 => 144,  564 => 143,  560 => 142,  556 => 141,  552 => 140,  548 => 139,  545 => 138,  541 => 137,  537 => 136,  533 => 135,  529 => 134,  526 => 133,  522 => 132,  518 => 131,  514 => 130,  510 => 129,  506 => 128,  502 => 127,  499 => 126,  495 => 125,  491 => 124,  487 => 123,  483 => 122,  480 => 121,  476 => 120,  472 => 119,  468 => 118,  464 => 117,  461 => 116,  457 => 115,  453 => 114,  449 => 113,  445 => 112,  442 => 111,  438 => 110,  434 => 109,  430 => 108,  426 => 107,  423 => 106,  419 => 105,  415 => 104,  411 => 103,  407 => 102,  404 => 101,  400 => 100,  396 => 99,  392 => 98,  388 => 97,  385 => 96,  381 => 95,  377 => 94,  373 => 93,  369 => 92,  366 => 91,  362 => 90,  358 => 89,  354 => 88,  350 => 87,  346 => 86,  342 => 85,  339 => 84,  335 => 83,  331 => 82,  327 => 81,  323 => 80,  319 => 79,  315 => 78,  311 => 77,  307 => 76,  303 => 75,  299 => 74,  296 => 73,  292 => 72,  288 => 71,  284 => 70,  280 => 69,  276 => 68,  272 => 67,  268 => 66,  264 => 65,  260 => 64,  256 => 63,  253 => 62,  249 => 61,  245 => 60,  241 => 59,  237 => 58,  233 => 57,  229 => 56,  225 => 55,  221 => 54,  218 => 53,  214 => 52,  210 => 51,  206 => 50,  203 => 49,  199 => 48,  195 => 47,  191 => 46,  187 => 45,  183 => 44,  179 => 43,  175 => 42,  172 => 41,  168 => 40,  164 => 39,  160 => 38,  156 => 37,  152 => 36,  148 => 35,  144 => 34,  141 => 33,  137 => 32,  133 => 31,  129 => 30,  125 => 29,  121 => 28,  117 => 27,  113 => 26,  110 => 25,  106 => 24,  102 => 23,  98 => 22,  94 => 21,  90 => 20,  86 => 19,  82 => 18,  79 => 17,  75 => 16,  71 => 15,  67 => 14,  63 => 13,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  28 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ header }}*/
/*                     */
/* {% if t1d_body_bg_color is empty %}{% set t1d_body_bg_color = 'F6F6F6' %}{% endif %}*/
/* {% if t1d_headings_color is empty %}{% set t1d_headings_color = '424242' %}{% endif %}*/
/* {% if t1d_headings_border_color is empty %}{% set t1d_headings_border_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_body_text_color is empty %}{% set t1d_body_text_color = '424242' %}{% endif %}*/
/* {% if t1d_light_text_color is empty %}{% set t1d_light_text_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_other_links_color is empty %}{% set t1d_other_links_color = '424242' %}{% endif %}*/
/* {% if t1d_links_hover_color is empty %}{% set t1d_links_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_icons_color is empty %}{% set t1d_icons_color = '424242' %}{% endif %}*/
/* {% if t1d_icons_hover_color is empty %}{% set t1d_icons_hover_color = '3EACEA' %}{% endif %}*/
/* */
/* {% if t1d_wrapper_frame_bg_color is empty %}{% set t1d_wrapper_frame_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_content_column_bg_color is empty %}{% set t1d_content_column_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_content_column_separator_color is empty %}{% set t1d_content_column_separator_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_content_column_hli_bg_color is empty %}{% set t1d_content_column_hli_bg_color = 'F3F3F3' %}{% endif %}*/
/* */
/* {% if t1d_left_column_head_title_bg_color is empty %}{% set t1d_left_column_head_title_bg_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_left_column_head_title_color is empty %}{% set t1d_left_column_head_title_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_left_column_box_bg_color is empty %}{% set t1d_left_column_box_bg_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_left_column_box_text_color is empty %}{% set t1d_left_column_box_text_color = '656565' %}{% endif %}*/
/* {% if t1d_left_column_box_links_color is empty %}{% set t1d_left_column_box_links_color = '424242' %}{% endif %}*/
/* {% if t1d_left_column_box_links_color_hover is empty %}{% set t1d_left_column_box_links_color_hover = '3EACEA' %}{% endif %}*/
/* {% if t1d_left_column_box_separator_color is empty %}{% set t1d_left_column_box_separator_color = 'F3F3F3' %}{% endif %}*/
/* */
/* {% if t1d_right_column_head_title_bg_color is empty %}{% set t1d_right_column_head_title_bg_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_right_column_head_title_color is empty %}{% set t1d_right_column_head_title_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_right_column_box_bg_color is empty %}{% set t1d_right_column_box_bg_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_right_column_box_text_color is empty %}{% set t1d_right_column_box_text_color = '656565' %}{% endif %}*/
/* {% if t1d_right_column_box_links_color is empty %}{% set t1d_right_column_box_links_color = '424242' %}{% endif %}*/
/* {% if t1d_right_column_box_links_color_hover is empty %}{% set t1d_right_column_box_links_color_hover = '3EACEA' %}{% endif %}*/
/* {% if t1d_right_column_box_separator_color is empty %}{% set t1d_right_column_box_separator_color = 'F3F3F3' %}{% endif %}*/
/* */
/* {% if t1d_category_box_head_title_bg_color is empty %}{% set t1d_category_box_head_title_bg_color = 'F1494B' %}{% endif %}*/
/* {% if t1d_category_box_head_title_color is empty %}{% set t1d_category_box_head_title_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_category_box_box_bg_color is empty %}{% set t1d_category_box_box_bg_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_category_box_box_links_color is empty %}{% set t1d_category_box_box_links_color = '424242' %}{% endif %}*/
/* {% if t1d_category_box_box_links_color_hover is empty %}{% set t1d_category_box_box_links_color_hover = '3EACEA' %}{% endif %}*/
/* {% if t1d_category_box_box_subcat_color is empty %}{% set t1d_category_box_box_subcat_color = '666666' %}{% endif %}*/
/* {% if t1d_category_box_box_separator_color is empty %}{% set t1d_category_box_box_separator_color = 'FFFFFF' %}{% endif %}*/
/* */
/* {% if t1d_filter_box_head_title_bg_color is empty %}{% set t1d_filter_box_head_title_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_filter_box_head_title_color is empty %}{% set t1d_filter_box_head_title_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_filter_box_box_bg_color is empty %}{% set t1d_filter_box_box_bg_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_filter_box_box_filter_title_color is empty %}{% set t1d_filter_box_box_filter_title_color = '424242' %}{% endif %}*/
/* {% if t1d_filter_box_box_filter_name_color is empty %}{% set t1d_filter_box_box_filter_name_color = '424242' %}{% endif %}*/
/* {% if t1d_filter_box_box_filter_name_color_hover is empty %}{% set t1d_filter_box_box_filter_name_color_hover = '3EACEA' %}{% endif %}*/
/* {% if t1d_filter_box_box_separator_color is empty %}{% set t1d_filter_box_box_separator_color = 'F3F3F3' %}{% endif %}*/
/* */
/* {% if t1d_price_color is empty %}{% set t1d_price_color = '424242' %}{% endif %}*/
/* {% if t1d_price_old_color is empty %}{% set t1d_price_old_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_price_new_color is empty %}{% set t1d_price_new_color = 'F1494B' %}{% endif %}*/
/* */
/* {% if t1d_button_bg_color is empty %}{% set t1d_button_bg_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_button_bg_hover_color is empty %}{% set t1d_button_bg_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_button_text_color is empty %}{% set t1d_button_text_color = '424242' %}{% endif %}*/
/* {% if t1d_button_text_hover_color is empty %}{% set t1d_button_text_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_button_exclusive_bg_color is empty %}{% set t1d_button_exclusive_bg_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_button_exclusive_bg_hover_color is empty %}{% set t1d_button_exclusive_bg_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_button_exclusive_text_color is empty %}{% set t1d_button_exclusive_text_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_button_exclusive_text_hover_color is empty %}{% set t1d_button_exclusive_text_hover_color = 'FFFFFF' %}{% endif %}*/
/* */
/* {% if t1d_dd_bg_color is empty %}{% set t1d_dd_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_dd_headings_color is empty %}{% set t1d_dd_headings_color = '424242' %}{% endif %}*/
/* {% if t1d_dd_text_color is empty %}{% set t1d_dd_text_color = '656565' %}{% endif %}*/
/* {% if t1d_dd_light_text_color is empty %}{% set t1d_dd_light_text_color = '999999' %}{% endif %}*/
/* {% if t1d_dd_links_color is empty %}{% set t1d_dd_links_color = '999999' %}{% endif %}*/
/* {% if t1d_dd_links_hover_color is empty %}{% set t1d_dd_links_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_dd_icons_color is empty %}{% set t1d_dd_icons_color = 'EBEBEB' %}{% endif %}*/
/* {% if t1d_dd_icons_hover_color is empty %}{% set t1d_dd_icons_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_dd_hli_bg_color is empty %}{% set t1d_dd_hli_bg_color = 'F7F7F7' %}{% endif %}*/
/* {% if t1d_dd_separator_color is empty %}{% set t1d_dd_separator_color = 'F3F3F3' %}{% endif %}*/
/* */
/* {% if t1d_top_area_bg_color is empty %}{% set t1d_top_area_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_top_area_mini_bg_color is empty %}{% set t1d_top_area_mini_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_top_area_icons_color is empty %}{% set t1d_top_area_icons_color = '555555' %}{% endif %}*/
/* {% if t1d_top_area_icons_color_hover is empty %}{% set t1d_top_area_icons_color_hover = '3EACEA' %}{% endif %}*/
/* {% if t1d_top_area_icons_separator_color is empty %}{% set t1d_top_area_icons_separator_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_top_area_search_bg_color is empty %}{% set t1d_top_area_search_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_top_area_search_border_color is empty %}{% set t1d_top_area_search_border_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_top_area_search_color is empty %}{% set t1d_top_area_search_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_top_area_search_color_active is empty %}{% set t1d_top_area_search_color_active = '424242' %}{% endif %}*/
/* {% if t1d_top_area_search_icon_color is empty %}{% set t1d_top_area_search_icon_color = '424242' %}{% endif %}*/
/* */
/* {% if t1d_top_area_tb_bg_color is empty %}{% set t1d_top_area_tb_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_top_area_tb_bottom_border_color is empty %}{% set t1d_top_area_tb_bottom_border_color = '424242' %}{% endif %}*/
/* {% if t1d_top_area_tb_text_color is empty %}{% set t1d_top_area_tb_text_color = 'CCCCCC' %}{% endif %}*/
/* {% if t1d_top_area_tb_link_color is empty %}{% set t1d_top_area_tb_link_color = 'CCCCCC' %}{% endif %}*/
/* {% if t1d_top_area_tb_link_color_hover is empty %}{% set t1d_top_area_tb_link_color_hover = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_top_area_tb_icons_color is empty %}{% set t1d_top_area_tb_icons_color = 'FFFFFF' %}{% endif %}*/
/* */
/* {% if t1d_mm_bg_color is empty %}{% set t1d_mm_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_mm_separator_color is empty %}{% set t1d_mm_separator_color = '555555' %}{% endif %}*/
/* {% if t1d_mm_border_top_color is empty %}{% set t1d_mm_border_top_color = '555555' %}{% endif %}*/
/* {% if t1d_mm_border_bottom_color is empty %}{% set t1d_mm_border_bottom_color = '555555' %}{% endif %}*/
/* */
/* {% if t1d_mm1_bg_color is empty %}{% set t1d_mm1_bg_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_mm1_bg_hover_color is empty %}{% set t1d_mm1_bg_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_mm1_link_color is empty %}{% set t1d_mm1_link_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm1_link_hover_color is empty %}{% set t1d_mm1_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* */
/* {% if t1d_mm2_bg_color is empty %}{% set t1d_mm2_bg_color = 'F1494B' %}{% endif %}*/
/* {% if t1d_mm2_bg_hover_color is empty %}{% set t1d_mm2_bg_hover_color = 'F1494B' %}{% endif %}*/
/* {% if t1d_mm2_link_color is empty %}{% set t1d_mm2_link_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm2_link_hover_color is empty %}{% set t1d_mm2_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* */
/* {% if t1d_mm3_bg_color is empty %}{% set t1d_mm3_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_mm3_bg_hover_color is empty %}{% set t1d_mm3_bg_hover_color = '424242' %}{% endif %}*/
/* {% if t1d_mm3_link_color is empty %}{% set t1d_mm3_link_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm3_link_hover_color is empty %}{% set t1d_mm3_link_hover_color = 'B6B6B6' %}{% endif %}*/
/* */
/* {% if t1d_mm6_bg_color is empty %}{% set t1d_mm6_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_mm6_bg_hover_color is empty %}{% set t1d_mm6_bg_hover_color = '424242' %}{% endif %}*/
/* {% if t1d_mm6_link_color is empty %}{% set t1d_mm6_link_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm6_link_hover_color is empty %}{% set t1d_mm6_link_hover_color = 'B6B6B6' %}{% endif %}*/
/* */
/* {% if t1d_mm5_bg_color is empty %}{% set t1d_mm5_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_mm5_bg_hover_color is empty %}{% set t1d_mm5_bg_hover_color = '424242' %}{% endif %}*/
/* {% if t1d_mm5_link_color is empty %}{% set t1d_mm5_link_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm5_link_hover_color is empty %}{% set t1d_mm5_link_hover_color = 'B6B6B6' %}{% endif %}*/
/* */
/* {% if t1d_mm4_bg_color is empty %}{% set t1d_mm4_bg_color = '424242' %}{% endif %}*/
/* {% if t1d_mm4_bg_hover_color is empty %}{% set t1d_mm4_bg_hover_color = '424242' %}{% endif %}*/
/* {% if t1d_mm4_link_color is empty %}{% set t1d_mm4_link_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm4_link_hover_color is empty %}{% set t1d_mm4_link_hover_color = 'B6B6B6' %}{% endif %}*/
/* */
/* {% if t1d_mm_sub_bg_color is empty %}{% set t1d_mm_sub_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mm_sub_titles_bg_color is empty %}{% set t1d_mm_sub_titles_bg_color = 'F3F3F3' %}{% endif %}*/
/* {% if t1d_mm_sub_text_color is empty %}{% set t1d_mm_sub_text_color = '656565' %}{% endif %}*/
/* {% if t1d_mm_sub_link_color is empty %}{% set t1d_mm_sub_link_color = '424242' %}{% endif %}*/
/* {% if t1d_mm_sub_link_hover_color is empty %}{% set t1d_mm_sub_link_hover_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_mm_sub_subcategory_color is empty %}{% set t1d_mm_sub_subcategory_color = '999999' %}{% endif %}*/
/* */
/* {% if t1d_mid_prod_box_sale_icon_color is empty %}{% set t1d_mid_prod_box_sale_icon_color = 'F1494B' %}{% endif %}*/
/* {% if t1d_mid_prod_box_new_icon_color is empty %}{% set t1d_mid_prod_box_new_icon_color = '3EACEA' %}{% endif %}*/
/* {% if t1d_mid_prod_box_out_of_stock_icon_color is empty %}{% set t1d_mid_prod_box_out_of_stock_icon_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_mid_prod_stars_color is empty %}{% set t1d_mid_prod_stars_color = '3EACEA' %}{% endif %}*/
/* */
/* {% if t1d_f6_bg_color is empty %}{% set t1d_f6_bg_color = '373737' %}{% endif %}*/
/* {% if t1d_f6_text_color is empty %}{% set t1d_f6_text_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f6_link_color is empty %}{% set t1d_f6_link_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_f6_link_hover_color is empty %}{% set t1d_f6_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f6_border_top_color is empty %}{% set t1d_f6_border_top_color = '2D2D2D' %}{% endif %}*/
/* */
/* {% if t1d_f2_bg_color is empty %}{% set t1d_f2_bg_color = '2F2F2F' %}{% endif %}*/
/* {% if t1d_f2_titles_color is empty %}{% set t1d_f2_titles_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f2_titles_border_color is empty %}{% set t1d_f2_titles_border_color = '373737' %}{% endif %}*/
/* {% if t1d_f2_text_color is empty %}{% set t1d_f2_text_color = '6E6E6E' %}{% endif %}*/
/* {% if t1d_f2_link_color is empty %}{% set t1d_f2_link_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_f2_link_hover_color is empty %}{% set t1d_f2_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f2_border_top_color is empty %}{% set t1d_f2_border_top_color = '373737' %}{% endif %}*/
/* */
/* {% if t1d_f3_bg_color is empty %}{% set t1d_f3_bg_color = '2F2F2F' %}{% endif %}*/
/* {% if t1d_f3_text_color is empty %}{% set t1d_f3_text_color = '6E6E6E' %}{% endif %}*/
/* {% if t1d_f3_link_color is empty %}{% set t1d_f3_link_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_f3_link_hover_color is empty %}{% set t1d_f3_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f3_icons_bg_color is empty %}{% set t1d_f3_icons_bg_color = '333333' %}{% endif %}*/
/* {% if t1d_f3_border_top_color is empty %}{% set t1d_f3_border_top_color = '373737' %}{% endif %}*/
/* */
/* {% if t1d_f4_bg_color is empty %}{% set t1d_f4_bg_color = '2F2F2F' %}{% endif %}*/
/* {% if t1d_f4_text_color is empty %}{% set t1d_f4_text_color = '6E6E6E' %}{% endif %}*/
/* {% if t1d_f4_link_color is empty %}{% set t1d_f4_link_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_f4_link_hover_color is empty %}{% set t1d_f4_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f4_border_top_color is empty %}{% set t1d_f4_border_top_color = '373737' %}{% endif %}*/
/* */
/* {% if t1d_f5_bg_color is empty %}{% set t1d_f5_bg_color = '2F2F2F' %}{% endif %}*/
/* {% if t1d_f5_text_color is empty %}{% set t1d_f5_text_color = '6E6E6E' %}{% endif %}*/
/* {% if t1d_f5_link_color is empty %}{% set t1d_f5_link_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1d_f5_link_hover_color is empty %}{% set t1d_f5_link_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1d_f5_border_top_color is empty %}{% set t1d_f5_border_top_color = '373737' %}{% endif %}*/
/* */
/* */
/* <style type="text/css">*/
/* .color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}*/
/* .k_help {color:#999;background-color:#F5F5F5;font-size:10px;font-weight:normal;text-transform:uppercase;padding:15px;width:auto;display:block;margin-top:10px;margin-bottom:10px;border-radius:3px;}*/
/* span.k_help_tip {margin-left:10px;padding:4px 9px 3px;width:24px;display:inline;border-radius:2px;background-color:#1E91CF;color:#FFF;font-weight:bold;transition: all 0.15s ease-in 0s;opacity: 0.9; display: none;}*/
/* span.k_help_tip_support {display:block;}*/
/* span.k_help_tip:hover {opacity: 1;}*/
/* span.k_help_tip a {color:#FFF;font-size:12px;font-weight:bold;text-decoration:none;}*/
/* span.k_tooltip {cursor:pointer;}*/
/* .k_sep {background-color:#F7F7F7;}*/
/* .ptn {position:relative;width:40px;height:40px;float:left;margin-right:5px;margin-bottom:5px;}*/
/* .ptn_nr {position:absolute;bottom:0px;right:3px;color:#999;}*/
/* .prod_l {position:relative;width:81px;height:63px;float:left;margin-right:25px;margin-bottom:20px;}*/
/* .prod_l_nr {position:absolute;bottom:-17px;right:0px;}*/
/* table.form {margin-bottom:0;}*/
/* table.form div {text-align:left}*/
/* table.form b {color:#003A88;font-size:13px}*/
/* table.form > tbody > tr > td:first-child {text-align:right}*/
/* a.btn-default.link {text-decoration:none;margin-left:5px;margin-right:5px;color:#222222;background-color:#F0F0F0;border-color:#F0F0F0;transition: all 0.15s ease-in 0s;position:relative;}*/
/* a.btn-default.link:hover {background-color:#DEDEDE;border-color:#DEDEDE;}*/
/* a.btn-default.link:last-child {padding-right:45px;}*/
/* a.btn-default.link i {font-size:14px;margin-right:5px;}*/
/* a.btn-default.link span.k_help_tip {position:absolute;top:5px;right:5px;}*/
/* .htabs {margin-top:15px;}*/
/* table.form {*/
/* 	width: 100%;*/
/* 	border-collapse: collapse;*/
/* 	margin-bottom: 20px;*/
/* }*/
/* table.form > tbody > tr > td:first-child {*/
/* 	width: 200px;*/
/* }*/
/* table.form > tbody > tr > td {*/
/* 	padding: 10px;*/
/* 	color: #000000;*/
/* 	border-bottom: 1px dotted #CCCCCC;*/
/* }*/
/* label.control-label span:after {*/
/* 	display: none;*/
/* }*/
/* legend {*/
/* 	background-color: #EEEEEE;*/
/* 	border: none;*/
/* 	border-radius: 3px;*/
/* }*/
/* fieldset legend {*/
/* 	margin-top: 20px;*/
/* 	padding: 20px 30px;*/
/* }*/
/* legend.bn {*/
/* 	border-color: #FFFFFF;*/
/* 	padding: 0;*/
/* 	margin: 0;*/
/* }*/
/* legend span {*/
/* 	font-size: 12px;*/
/* }*/
/* .nav-tabs {*/
/* 	border-bottom: 3px solid #1E91CF;*/
/* }*/
/* .nav-tabs > li {*/
/* 	margin-bottom: 0;*/
/* }*/
/* .nav-tabs > li > a:hover {*/
/* 	background-color: #E9E9E9;*/
/* 	border-color: #E9E9E9;*/
/* }*/
/* .nav-tabs > li > a {*/
/* 	padding: 15px 20px;*/
/* }*/
/* .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {*/
/* 	background-color: #1E91CF;*/
/* 	border-color: #1E91CF;*/
/* 	color: #FFFFFF;*/
/* 	font-weight: normal;*/
/* }*/
/* .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {*/
/* 	background-color: #1E91CF;*/
/* }*/
/* .nav > li > a {*/
/* 	background-color: #EEEEEE;*/
/* 	padding: 15px 20px;*/
/* }*/
/* .nav > li > a:hover {*/
/* 	background-color: #E9E9E9;*/
/* 	color: inherit;*/
/* }*/
/* html, body {color:#222222;}*/
/* a {color:#666666;transition: all 0.15s ease-in 0s;}*/
/* a:hover, a:focus {color:#1E91CF;}*/
/* label {font-weight:normal;}*/
/* .form-control {display:inline-block;min-width:77px;}*/
/* .form-horizontal .form-group {*/
/* 	margin-left: 0;*/
/* 	margin-right: 0;*/
/* }*/
/* .form-group + .form-group {border-top: 1px dotted #EDEDED;}*/
/* .tab-content .form-horizontal label.col-sm-2{*/
/* 	padding-left: 0;*/
/* }*/
/* textarea.form-control {width:100%;}*/
/* .panel-heading i {font-size: 14px;}*/
/* .table thead > tr > td, .table tbody > tr > td {vertical-align:top;}*/
/* </style>    */
/* */
/* {{ column_left }}*/
/* */
/* <div id="content">*/
/* */
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-oxy-theme" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*   {% if error_warning %}*/
/*   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/* 		<h3 class="panel-title"><i class="fa fa-pencil"></i> {{ heading_title }}</h3>*/
/* 	</div>*/
/*     <div class="panel-body">*/
/*     */
/*     <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-oxy-theme">*/
/*     */
/*     <input type="hidden" name="store_id" value="{{ store_id }}" />*/
/* */
/*         <div style="margin-top:10px; margin-bottom:25px;">*/
/* 		*/
/*             <span style="margin-left:0px;">Useful links:</span> */
/*             <a href="http://oxy.321cart.com/documentation/" class="btn btn-default link" target="_blank"><i class="fa fa-book"></i> OXY Documentation</a>*/
/*             <a href="http://oxy.321cart.com/landing/" class="btn btn-default link" target="_blank"><i class="fa fa-television"></i> OXY Demos</a>*/
/*             <a href="http://support.321cart.com/system/" class="btn btn-default link" target="_blank"><i class="fa fa-support"></i> OXY Support <span class="k_help_tip_support k_help_tip k_tooltip" title="<br>If you need help, please contact us. We provide support only through our Support System.<br><br>Create a ticket and our support developer will respond as soon as possible.<br><br>Support requests are being processed on Monday to Friday.<br><br>" data-toggle="tooltip">?</span></a>*/
/* */
/* 		</div>*/
/* */
/*     */
/*         <ul class="nav nav-tabs">*/
/*           <li class="active"><a href="#tab-colors" data-toggle="tab">Colors and Styles</a></li>*/
/*           <li><a href="#tab-backgrounds" data-toggle="tab">Background Images</a></li>*/
/*           <li><a href="#tab-fonts" data-toggle="tab">Fonts</a></li>*/
/*           <li><a href="#tab-skins" data-toggle="tab">Color Schemes</a></li>*/
/*         </ul>*/
/*         */
/*         <div class="tab-content">*/
/*         <!-- -->*/
/* */
/* */
/*         <div class="tab-pane active" id="tab-colors"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="colors_settings_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-colors-general" data-toggle="tab">General</a></li>     */
/*              <li><a href="#tab-colors-layout" data-toggle="tab">Layout</a></li>*/
/*              <li><a href="#tab-colors-header" data-toggle="tab">Header</a></li>*/
/*              <li><a href="#tab-colors-menu" data-toggle="tab">Main Menu</a></li>*/
/*              <li><a href="#tab-colors-midsection" data-toggle="tab">Midsection</a></li>*/
/*              <li><a href="#tab-colors-footer" data-toggle="tab">Footer</a></li>*/
/*              <li><a href="#tab-colors-prices" data-toggle="tab">Prices</a></li>*/
/*              <li><a href="#tab-colors-buttons" data-toggle="tab">Buttons</a></li>*/
/*              <li><a href="#tab-colors-dropdowns" data-toggle="tab">Dropdowns</a></li>*/
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-colors-general" class="tab-pane fade in active"> */
/*         */
/*                     <fieldset> */
/*                         */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Body background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_body_bg_color" id="t1d_body_bg_color" value="{{ t1d_body_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_01.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Headings color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_headings_color" id="t1d_headings_color" value="{{ t1d_headings_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_02.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show headings border bottom:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_headings_border_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_headings_border_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_headings_border_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_headings_border_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_03.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div> */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Headings border bottom color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_headings_border_color" id="t1d_headings_border_color" value="{{ t1d_headings_border_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Body text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_body_text_color" id="t1d_body_text_color" value="{{ t1d_body_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_04.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Light text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_light_text_color" id="t1d_light_text_color" value="{{ t1d_light_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_05.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Others links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_other_links_color" id="t1d_other_links_color" value="{{ t1d_other_links_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_06.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_links_hover_color" id="t1d_links_hover_color" value="{{ t1d_links_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Others Icons color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_icons_color" id="t1d_icons_color" value="{{ t1d_icons_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_07.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Others Icons color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_icons_hover_color" id="t1d_icons_hover_color" value="{{ t1d_icons_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Product Images style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_img_style" class="form-control">*/
/* 									<option value="normal"{% if t1d_img_style == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>Without Border</option>*/
/*                                     <option value="thumbnail-theme"{% if t1d_img_style == 'thumbnail-theme' %}{{ "selected=\"selected\"" }}{% endif %}>With Border</option> */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset> */
/* */
/*         </div>*/
/* */
/*         <div id="tab-colors-layout" class="tab-pane"> */
/* */
/*                     <fieldset> */
/* */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Wrapper Frame color: */
/*                             <span class="k_help">for "Framed", "Full Width"<br />layout styles</span> */
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_wrapper_frame_bg_color_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_wrapper_frame_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_wrapper_frame_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_wrapper_frame_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_09.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <legend class="bn"></legend>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Wrapper Frame color: </label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_wrapper_frame_bg_color" id="t1d_wrapper_frame_bg_color" value="{{ t1d_wrapper_frame_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Wrapper shadow:*/
/*                             <span class="k_help">for "Framed" layout style</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_wrapper_shadow" class="form-control">*/
/*                                     <option value="0"{% if t1d_wrapper_shadow == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_wrapper_shadow == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_wrapper_shadow == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_09.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Boxes shadow:*/
/*                             <span class="k_help">for "Boxed" layout style</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_boxes_shadow" class="form-control">*/
/*                                     <option value="0"{% if t1d_boxes_shadow == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_boxes_shadow == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_boxes_shadow == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_09.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Content Column<a href="view/image/theme_img/help_oxy_theme/cas_10.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_content_column_bg_color" id="t1d_content_column_bg_color" value="{{ t1d_content_column_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div> */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator color:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1d_content_column_separator_color" id="t1d_content_column_separator_color" value="{{ t1d_content_column_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_13.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Buy Column background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_content_column_hli_buy_column" class="form-control">*/
/*                                     <option value="0"{% if t1d_content_column_hli_buy_column == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_content_column_hli_buy_column == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option> */
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_12.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <legend class="bn"></legend>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Buy Column background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_content_column_hli_bg_color" id="t1d_content_column_hli_bg_color" value="{{ t1d_content_column_hli_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_11.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Left/Right Columns<a href="view/image/theme_img/help_oxy_theme/cas_10.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_sidebar_bg_color_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_sidebar_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_sidebar_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_sidebar_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show separator:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_sidebar_separator_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_sidebar_separator_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_sidebar_separator_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_sidebar_separator_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Boxes margin bottom:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_sidebar_margin_bottom" class="form-control">*/
/* 									<option value="0"{% if t1d_sidebar_margin_bottom == '0' %}{{ "selected=\"selected\"" }}{% endif %}>0px</option>*/
/*                                     <option value="10"{% if t1d_sidebar_margin_bottom == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                     <option value="15"{% if t1d_sidebar_margin_bottom == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>*/
/*                                     <option value="20"{% if t1d_sidebar_margin_bottom == '20' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_sidebar_margin_bottom == '' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option>*/
/*                                     <option value="25"{% if t1d_sidebar_margin_bottom == '25' %}{{ "selected=\"selected\"" }}{% endif %}>25px</option>*/
/*                                     <option value="30"{% if t1d_sidebar_margin_bottom == '30' %}{{ "selected=\"selected\"" }}{% endif %}>30px</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Left Column Box</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_head_title_bg_color" id="t1d_left_column_head_title_bg_color" value="{{ t1d_left_column_head_title_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_head_title_color" id="t1d_left_column_head_title_color" value="{{ t1d_left_column_head_title_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_16.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Box background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_box_bg_color" id="t1d_left_column_box_bg_color" value="{{ t1d_left_column_box_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_box_text_color" id="t1d_left_column_box_text_color" value="{{ t1d_left_column_box_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_20.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_box_links_color" id="t1d_left_column_box_links_color" value="{{ t1d_left_column_box_links_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_19.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_box_links_color_hover" id="t1d_left_column_box_links_color_hover" value="{{ t1d_left_column_box_links_color_hover }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_left_column_box_separator_color" id="t1d_left_column_box_separator_color" value="{{ t1d_left_column_box_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_21.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Right Column Box</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_head_title_bg_color" id="t1d_right_column_head_title_bg_color" value="{{ t1d_right_column_head_title_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_head_title_color" id="t1d_right_column_head_title_color" value="{{ t1d_right_column_head_title_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_16.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Box background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_box_bg_color" id="t1d_right_column_box_bg_color" value="{{ t1d_right_column_box_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_box_text_color" id="t1d_right_column_box_text_color" value="{{ t1d_right_column_box_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_20.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_box_links_color" id="t1d_right_column_box_links_color" value="{{ t1d_right_column_box_links_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_19.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_box_links_color_hover" id="t1d_right_column_box_links_color_hover" value="{{ t1d_right_column_box_links_color_hover }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_right_column_box_separator_color" id="t1d_right_column_box_separator_color" value="{{ t1d_right_column_box_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_21.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Category Box</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_head_title_bg_color" id="t1d_category_box_head_title_bg_color" value="{{ t1d_category_box_head_title_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_head_title_color" id="t1d_category_box_head_title_color" value="{{ t1d_category_box_head_title_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_23.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Box background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_box_bg_color" id="t1d_category_box_box_bg_color" value="{{ t1d_category_box_box_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_25.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Categories links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_box_links_color" id="t1d_category_box_box_links_color" value="{{ t1d_category_box_box_links_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_26.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Categories links color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_box_links_color_hover" id="t1d_category_box_box_links_color_hover" value="{{ t1d_category_box_box_links_color_hover }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subcategories links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_box_subcat_color" id="t1d_category_box_box_subcat_color" value="{{ t1d_category_box_box_subcat_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_27.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_category_box_box_separator_color" id="t1d_category_box_box_separator_color" value="{{ t1d_category_box_box_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_28.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Filter Box</legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_head_title_bg_color" id="t1d_filter_box_head_title_bg_color" value="{{ t1d_filter_box_head_title_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_head_title_color" id="t1d_filter_box_head_title_color" value="{{ t1d_filter_box_head_title_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_30.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Box background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_box_bg_color" id="t1d_filter_box_box_bg_color" value="{{ t1d_filter_box_box_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_32.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Filter Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_box_filter_title_color" id="t1d_filter_box_box_filter_title_color" value="{{ t1d_filter_box_box_filter_title_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_33.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Filter Name color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_box_filter_name_color" id="t1d_filter_box_box_filter_name_color" value="{{ t1d_filter_box_box_filter_name_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_34.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Filter Name color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_box_filter_name_color_hover" id="t1d_filter_box_box_filter_name_color_hover" value="{{ t1d_filter_box_box_filter_name_color_hover }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_filter_box_box_separator_color" id="t1d_filter_box_box_separator_color" value="{{ t1d_filter_box_box_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_35.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-header" class="tab-pane"> */
/* */
/*                     <fieldset>  */
/* */
/*                     <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_top_area_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_top_area_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_top_area_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_top_area_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_36.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_bg_color" id="t1d_top_area_bg_color" value="{{ t1d_top_area_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Fixed Header<br />background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_mini_bg_color" id="t1d_top_area_mini_bg_color" value="{{ t1d_top_area_mini_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_37.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_icons_color" id="t1d_top_area_icons_color" value="{{ t1d_top_area_icons_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_38.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_icons_color_hover" id="t1d_top_area_icons_color_hover" value="{{ t1d_top_area_icons_color_hover }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Icons separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_top_area_icons_separator_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_top_area_icons_separator_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_top_area_icons_separator_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_top_area_icons_separator_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_icons_separator_color" id="t1d_top_area_icons_separator_color" value="{{ t1d_top_area_icons_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Search Bar</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Search Bar background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_search_bg_color" id="t1d_top_area_search_bg_color" value="{{ t1d_top_area_search_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Search Bar border color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_search_border_color" id="t1d_top_area_search_border_color" value="{{ t1d_top_area_search_border_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">"Search" color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_search_color" id="t1d_top_area_search_color" value="{{ t1d_top_area_search_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">"Search" active color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_search_color_active" id="t1d_top_area_search_color_active" value="{{ t1d_top_area_search_color_active }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Search Bar icon color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_search_icon_color" id="t1d_top_area_search_icon_color" value="{{ t1d_top_area_search_icon_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Top Bar</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_top_area_tb_bg_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_top_area_tb_bg_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_top_area_tb_bg_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_top_area_tb_bg_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_tb_bg_color" id="t1d_top_area_tb_bg_color" value="{{ t1d_top_area_tb_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show border bottom:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_top_area_tb_bottom_border_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_top_area_tb_bottom_border_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_top_area_tb_bottom_border_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_42.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border bottom color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_tb_bottom_border_color" id="t1d_top_area_tb_bottom_border_color" value="{{ t1d_top_area_tb_bottom_border_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_tb_text_color" id="t1d_top_area_tb_text_color" value="{{ t1d_top_area_tb_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_43.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_tb_link_color" id="t1d_top_area_tb_link_color" value="{{ t1d_top_area_tb_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_44.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_tb_link_color_hover" id="t1d_top_area_tb_link_color_hover" value="{{ t1d_top_area_tb_link_color_hover }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_top_area_tb_icons_color" id="t1d_top_area_tb_icons_color" value="{{ t1d_top_area_tb_icons_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_45.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-menu" class="tab-pane"> */
/* */
/*                     <fieldset>  */
/* */
/*                         <legend>Main Menu Bar <a href="view/image/theme_img/help_oxy_theme/cas_47.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_bg_color" id="t1d_mm_bg_color" value="{{ t1d_mm_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_mm_separator_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm_separator_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_separator_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_mm_separator_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_mm_separator_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_mm_separator_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_mm_separator_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_mm_separator_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_mm_separator_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_mm_separator_color" id="t1d_mm_separator_color" value="{{ t1d_mm_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_48.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border top:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_mm_border_top_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm_border_top_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_border_top_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_mm_border_top_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_mm_border_top_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_mm_border_top_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_mm_border_top_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_mm_border_top_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_mm_border_top_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_mm_border_top_color" id="t1d_mm_border_top_color" value="{{ t1d_mm_border_top_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_49.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border bottom:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_mm_border_bottom_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm_border_bottom_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_border_bottom_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_mm_border_bottom_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_mm_border_bottom_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_mm_border_bottom_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_mm_border_bottom_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_mm_border_bottom_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_mm_border_bottom_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_mm_border_bottom_color" id="t1d_mm_border_bottom_color" value="{{ t1d_mm_border_bottom_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_50.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Shadow:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_shadow_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_mm_shadow_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_shadow_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_shadow_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Sort Down icon:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_sort_down_icon_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_mm_sort_down_icon_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_sort_down_icon_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sort_down_icon_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>  */
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Home Page Link</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm1_bg_color_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_mm1_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm1_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm1_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm1_bg_color" id="t1d_mm1_bg_color" value="{{ t1d_mm1_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_52.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm1_bg_hover_color" id="t1d_mm1_bg_hover_color" value="{{ t1d_mm1_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm1_link_color" id="t1d_mm1_link_color" value="{{ t1d_mm1_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_53.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm1_link_hover_color" id="t1d_mm1_link_hover_color" value="{{ t1d_mm1_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Categories</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm2_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm2_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm2_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm2_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm2_bg_color" id="t1d_mm2_bg_color" value="{{ t1d_mm2_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm2_bg_hover_color" id="t1d_mm2_bg_hover_color" value="{{ t1d_mm2_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm2_link_color" id="t1d_mm2_link_color" value="{{ t1d_mm2_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm2_link_hover_color" id="t1d_mm2_link_hover_color" value="{{ t1d_mm2_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Brands</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm3_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm3_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm3_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm3_bg_color" id="t1d_mm3_bg_color" value="{{ t1d_mm3_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm3_bg_hover_color" id="t1d_mm3_bg_hover_color" value="{{ t1d_mm3_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm3_link_color" id="t1d_mm3_link_color" value="{{ t1d_mm3_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm3_link_hover_color" id="t1d_mm3_link_hover_color" value="{{ t1d_mm3_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Custom Blocks</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm6_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm6_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm6_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm6_bg_color" id="t1d_mm6_bg_color" value="{{ t1d_mm6_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm6_bg_hover_color" id="t1d_mm6_bg_hover_color" value="{{ t1d_mm6_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm6_link_color" id="t1d_mm6_link_color" value="{{ t1d_mm6_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm6_link_hover_color" id="t1d_mm6_link_hover_color" value="{{ t1d_mm6_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Custom Dropdown Menus</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm5_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm5_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm5_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm5_bg_color" id="t1d_mm5_bg_color" value="{{ t1d_mm5_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm5_bg_hover_color" id="t1d_mm5_bg_hover_color" value="{{ t1d_mm5_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm5_link_color" id="t1d_mm5_link_color" value="{{ t1d_mm5_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm5_link_hover_color" id="t1d_mm5_link_hover_color" value="{{ t1d_mm5_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Custom Links</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm4_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm4_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm4_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm4_bg_color" id="t1d_mm4_bg_color" value="{{ t1d_mm4_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm4_bg_hover_color" id="t1d_mm4_bg_hover_color" value="{{ t1d_mm4_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm4_link_color" id="t1d_mm4_link_color" value="{{ t1d_mm4_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm4_link_hover_color" id="t1d_mm4_link_hover_color" value="{{ t1d_mm4_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Sub-Menus</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_sub_bg_color" id="t1d_mm_sub_bg_color" value="{{ t1d_mm_sub_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_54.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Highlighted Items background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_sub_titles_bg_color" id="t1d_mm_sub_titles_bg_color" value="{{ t1d_mm_sub_titles_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_55.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_sub_text_color" id="t1d_mm_sub_text_color" value="{{ t1d_mm_sub_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_56.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_sub_link_color" id="t1d_mm_sub_link_color" value="{{ t1d_mm_sub_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_57.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_sub_link_hover_color" id="t1d_mm_sub_link_hover_color" value="{{ t1d_mm_sub_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>                      */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Main Categories border bottom:<br /><span class="k_help">for "Horizontal" style</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_sub_main_category_border_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_mm_sub_main_category_border_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_sub_main_category_border_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sub_main_category_border_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subcategories color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mm_sub_subcategory_color" id="t1d_mm_sub_subcategory_color" value="{{ t1d_mm_sub_subcategory_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div> */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subcategories border bottom:<br /><span class="k_help">for "Horizontal" style</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_sub_subcategory_border_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_mm_sub_subcategory_border_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_sub_subcategory_border_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sub_subcategory_border_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Category Dropdown border:<br /><span class="k_help">for "Horizontal"<br />and "Vertical 2" styles</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_sub_border_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_mm_sub_border_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/* 								    <option value="1"{% if t1d_mm_sub_border_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Shadow:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_mm_sub_box_shadow" class="form-control">*/
/* 								    <option value="0"{% if t1d_mm_sub_box_shadow == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_mm_sub_box_shadow == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sub_box_shadow == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_59.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-midsection" class="tab-pane"> */
/* */
/*                     <fieldset>*/
/*                     */
/*                     <legend class="bn"></legend>*/
/*                     */
/*                     <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Sale Badge color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mid_prod_box_sale_icon_color" id="t1d_mid_prod_box_sale_icon_color" value="{{ t1d_mid_prod_box_sale_icon_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_60.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">New Product Badge color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mid_prod_box_new_icon_color" id="t1d_mid_prod_box_new_icon_color" value="{{ t1d_mid_prod_box_new_icon_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_61.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Out of Stock Badge color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mid_prod_box_out_of_stock_icon_color" id="t1d_mid_prod_box_out_of_stock_icon_color" value="{{ t1d_mid_prod_box_out_of_stock_icon_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_61.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Rating Stars color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_mid_prod_stars_color" id="t1d_mid_prod_stars_color" value="{{ t1d_mid_prod_stars_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_62.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-footer" class="tab-pane"> */
/* */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Top Custom Block <a href="view/image/theme_img/help_oxy_theme/cas_73.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f6_bg_color_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_f6_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f6_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f6_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f6_bg_color" id="t1d_f6_bg_color" value="{{ t1d_f6_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f6_text_color" id="t1d_f6_text_color" value="{{ t1d_f6_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f6_link_color" id="t1d_f6_link_color" value="{{ t1d_f6_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f6_link_hover_color" id="t1d_f6_link_hover_color" value="{{ t1d_f6_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border top:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_f6_border_top_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f6_border_top_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f6_border_top_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_f6_border_top_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_f6_border_top_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_f6_border_top_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_f6_border_top_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_f6_border_top_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_f6_border_top_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_f6_border_top_color" id="t1d_f6_border_top_color" value="{{ t1d_f6_border_top_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Information, Custom Columns <a href="view/image/theme_img/help_oxy_theme/cas_71.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f2_bg_color_status" class="form-control">*/
/*                                     <option value="0"{% if t1d_f2_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f2_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f2_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f2_bg_color" id="t1d_f2_bg_color" value="{{ t1d_f2_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f2_titles_color" id="t1d_f2_titles_color" value="{{ t1d_f2_titles_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Titles border bottom:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_f2_titles_border_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f2_titles_border_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f2_titles_border_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f2_titles_border_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_f2_titles_border_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_f2_titles_border_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_f2_titles_border_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_f2_titles_border_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_f2_titles_border_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_f2_titles_border_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_f2_titles_border_color" id="t1d_f2_titles_border_color" value="{{ t1d_f2_titles_border_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f2_text_color" id="t1d_f2_text_color" value="{{ t1d_f2_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f2_link_color" id="t1d_f2_link_color" value="{{ t1d_f2_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f2_link_hover_color" id="t1d_f2_link_hover_color" value="{{ t1d_f2_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border top:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_f2_border_top_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f2_border_top_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f2_border_top_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f2_border_top_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_f2_border_top_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_f2_border_top_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_f2_border_top_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_f2_border_top_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_f2_border_top_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_f2_border_top_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_f2_border_top_color" id="t1d_f2_border_top_color" value="{{ t1d_f2_border_top_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Powered by, Payment Images, Follow Us <a href="view/image/theme_img/help_oxy_theme/cas_72.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f3_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f3_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f3_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f3_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f3_bg_color" id="t1d_f3_bg_color" value="{{ t1d_f3_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f3_text_color" id="t1d_f3_text_color" value="{{ t1d_f3_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f3_link_color" id="t1d_f3_link_color" value="{{ t1d_f3_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f3_link_hover_color" id="t1d_f3_link_hover_color" value="{{ t1d_f3_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Social Media icons background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f3_icons_bg_color" id="t1d_f3_icons_bg_color" value="{{ t1d_f3_icons_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Social Media icons style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f3_icons_social_style" class="form-control">*/
/* 									<option value="0"{% if t1d_f3_icons_social_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>One color</option>*/
/*                                     <option value="1"{% if t1d_f3_icons_social_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f3_icons_social_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Multicolor</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Payment images style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f3_payment_images_style" class="form-control">*/
/* 								    <option value="0"{% if t1d_f3_payment_images_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Black and White</option>*/
/*                                     <option value="1"{% if t1d_f3_payment_images_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f3_payment_images_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Color</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border top:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_f3_border_top_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f3_border_top_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f3_border_top_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f3_border_top_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_f3_border_top_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_f3_border_top_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_f3_border_top_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_f3_border_top_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_f3_border_top_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_f3_border_top_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_f3_border_top_color" id="t1d_f3_border_top_color" value="{{ t1d_f3_border_top_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Bottom Custom Block <a href="view/image/theme_img/help_oxy_theme/cas_73.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f4_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f4_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f4_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f4_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f4_bg_color" id="t1d_f4_bg_color" value="{{ t1d_f4_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f4_text_color" id="t1d_f4_text_color" value="{{ t1d_f4_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f4_link_color" id="t1d_f4_link_color" value="{{ t1d_f4_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f4_link_hover_color" id="t1d_f4_link_hover_color" value="{{ t1d_f4_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border top:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_f4_border_top_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f4_border_top_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f4_border_top_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f4_border_top_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_f4_border_top_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_f4_border_top_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_f4_border_top_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_f4_border_top_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_f4_border_top_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_f4_border_top_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_f4_border_top_color" id="t1d_f4_border_top_color" value="{{ t1d_f4_border_top_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Sliding Bottom Custom Block <a href="view/image/theme_img/help_oxy_theme/cas_73.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_f5_bg_color_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f5_bg_color_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f5_bg_color_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_f5_bg_color_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f5_bg_color" id="t1d_f5_bg_color" value="{{ t1d_f5_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f5_text_color" id="t1d_f5_text_color" value="{{ t1d_f5_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f5_link_color" id="t1d_f5_link_color" value="{{ t1d_f5_link_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Link color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_f5_link_hover_color" id="t1d_f5_link_hover_color" value="{{ t1d_f5_link_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Border top:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 Show:*/
/* 								<select name="t1d_f5_border_top_status" class="form-control">*/
/* 								    <option value="0"{% if t1d_f5_border_top_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_f5_border_top_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Size (px):*/
/* 								<select name="t1d_f5_border_top_size" class="form-control">*/
/* 								    <option value="1"{% if t1d_f5_border_top_size == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            	    	<option value="2"{% if t1d_f5_border_top_size == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            	    	<option value="3"{% if t1d_f5_border_top_size == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            	    	<option value="4"{% if t1d_f5_border_top_size == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                            	    	<option value="5"{% if t1d_f5_border_top_size == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>  */
/* 							    </select>*/
/*                                 &nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 Color:*/
/* 								<input type="text" name="t1d_f5_border_top_color" id="t1d_f5_border_top_color" value="{{ t1d_f5_border_top_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-prices" class="tab-pane"> */
/* */
/*                     <fieldset>  */
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Price color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_price_color" id="t1d_price_color" value="{{ t1d_price_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Old price color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_price_old_color" id="t1d_price_old_color" value="{{ t1d_price_old_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>  */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">New price color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_price_new_color" id="t1d_price_new_color" value="{{ t1d_price_new_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>                       */
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-buttons" class="tab-pane"> */
/* */
/*                     <fieldset>  */
/*                     */
/*                         <legend class="bn"></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Button border radius:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_button_border_radius" class="form-control">*/
/* 								    <option value="0"{% if t1d_button_border_radius == '0' %}{{ "selected=\"selected\"" }}{% endif %}>0px</option>                        */
/*                            	    	<option value="1"{% if t1d_button_border_radius == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1px</option>*/
/*                            	    	<option value="2"{% if t1d_button_border_radius == '2' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_button_border_radius == '' %}{{ "selected=\"selected\"" }}{% endif %}>2px</option>*/
/*                           	    	<option value="3"{% if t1d_button_border_radius == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3px</option>*/
/*                            	    	<option value="4"{% if t1d_button_border_radius == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4px</option>*/
/*                            	    	<option value="5"{% if t1d_button_border_radius == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5px</option> */
/*                            	    	<option value="6"{% if t1d_button_border_radius == '6' %}{{ "selected=\"selected\"" }}{% endif %}>6px</option>*/
/*                            	    	<option value="7"{% if t1d_button_border_radius == '7' %}{{ "selected=\"selected\"" }}{% endif %}>7px</option>                           */
/*                            	    	<option value="8"{% if t1d_button_border_radius == '8' %}{{ "selected=\"selected\"" }}{% endif %}>8px</option> */
/*                            	    	<option value="9"{% if t1d_button_border_radius == '9' %}{{ "selected=\"selected\"" }}{% endif %}>9px</option>*/
/*                            	    	<option value="10"{% if t1d_button_border_radius == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                     <option value="50"{% if t1d_button_border_radius == '50' %}{{ "selected=\"selected\"" }}{% endif %}>50%</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Button hover shadow:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_button_shadow_status" class="form-control">*/
/* 									<option value="0"{% if t1d_button_shadow_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_button_shadow_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_button_shadow_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>    */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Default Buttons <a href="view/image/theme_img/help_oxy_theme/cas_74.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_button_bg_status" class="form-control">*/
/* 									<option value="0"{% if t1d_button_bg_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_button_bg_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_button_bg_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>   */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_bg_color" id="t1d_button_bg_color" value="{{ t1d_button_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_bg_hover_color" id="t1d_button_bg_hover_color" value="{{ t1d_button_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_text_color" id="t1d_button_text_color" value="{{ t1d_button_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_text_hover_color" id="t1d_button_text_hover_color" value="{{ t1d_button_text_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Exclusive Buttons <a href="view/image/theme_img/help_oxy_theme/cas_74.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_button_exclusive_bg_status" class="form-control">*/
/* 									<option value="0"{% if t1d_button_exclusive_bg_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_button_exclusive_bg_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_button_exclusive_bg_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>   */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_exclusive_bg_color" id="t1d_button_exclusive_bg_color" value="{{ t1d_button_exclusive_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_exclusive_bg_hover_color" id="t1d_button_exclusive_bg_hover_color" value="{{ t1d_button_exclusive_bg_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_exclusive_text_color" id="t1d_button_exclusive_text_color" value="{{ t1d_button_exclusive_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_button_exclusive_text_hover_color" id="t1d_button_exclusive_text_hover_color" value="{{ t1d_button_exclusive_text_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-colors-dropdowns" class="tab-pane"> */
/* */
/*                     <fieldset>  */
/* */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_bg_color" id="t1d_dd_bg_color" value="{{ t1d_dd_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/cas_75.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Headings color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_headings_color" id="t1d_dd_headings_color" value="{{ t1d_dd_headings_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_text_color" id="t1d_dd_text_color" value="{{ t1d_dd_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Light text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_light_text_color" id="t1d_dd_light_text_color" value="{{ t1d_dd_light_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_links_color" id="t1d_dd_links_color" value="{{ t1d_dd_links_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Links color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_links_hover_color" id="t1d_dd_links_hover_color" value="{{ t1d_dd_links_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_icons_color" id="t1d_dd_icons_color" value="{{ t1d_dd_icons_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_icons_hover_color" id="t1d_dd_icons_hover_color" value="{{ t1d_dd_icons_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Highlighted fields background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_hli_bg_color" id="t1d_dd_hli_bg_color" value="{{ t1d_dd_hli_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Separator color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1d_dd_separator_color" id="t1d_dd_separator_color" value="{{ t1d_dd_separator_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Shadow:*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_dd_shadow" class="form-control">*/
/* 								    <option value="0"{% if t1d_dd_shadow == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_dd_shadow == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_dd_shadow == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/* */
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div> */
/*           */
/*           */
/*           */
/*           */
/*         */
/*         <div class="tab-pane" id="tab-backgrounds"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="background_images_settings_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-backgrounds-body" data-toggle="tab">Body</a></li>*/
/*              <li><a href="#tab-backgrounds-header" data-toggle="tab">Header</a></li>*/
/*              <li><a href="#tab-backgrounds-menu" data-toggle="tab">Main Menu</a></li>*/
/*              <li><a href="#tab-backgrounds-footer" data-toggle="tab">Footer</a></li>                                       */
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-backgrounds-body" class="tab-pane active">  */
/*         */
/*                     <fieldset>*/
/*                      */
/*                         <legend>Body <a href="view/image/theme_img/help_oxy_theme/cas_77.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_body" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_body == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_body == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_custom" value="{{ t1d_bg_image_custom }}" id="t1d_bg_image_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Attachment:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_attachment" class="form-control">*/
/* 								    <option value="scroll"{% if t1d_bg_image_attachment == 'scroll' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_attachment_s }}</option>*/
/*                                     <option value="fixed"{% if t1d_bg_image_attachment == 'fixed' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_attachment_f }}</option> */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                     */
/*                     </fieldset> */
/*           */
/*         </div>*/
/*  */
/*         <div id="tab-backgrounds-header" class="tab-pane">    */
/* */
/*                     <fieldset>*/
/*                      */
/*                         <legend>Header <a href="view/image/theme_img/help_oxy_theme/cas_78.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_ta" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_ta == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_ta == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                   */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_ta_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_ta_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_ta_custom" value="{{ t1d_bg_image_ta_custom }}" id="t1d_bg_image_ta_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_ta_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_ta_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_ta_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_ta_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_ta_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_ta_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_ta_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_ta_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_ta_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_ta_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_ta_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_ta_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_ta_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_ta_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_ta_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Attachment:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_ta_attachment" class="form-control">*/
/* 								    <option value="scroll"{% if t1d_bg_image_ta_attachment == 'scroll' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_attachment_s }}</option>*/
/*                                     <option value="fixed"{% if t1d_bg_image_ta_attachment == 'fixed' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_attachment_f }}</option> */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                     */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-backgrounds-menu" class="tab-pane">    */
/* */
/*                     <fieldset>*/
/*                      */
/*                         <legend>Main Menu <a href="view/image/theme_img/help_oxy_theme/cas_47.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_mm" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_mm == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_mm == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_mm_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_mm_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_mm_custom" value="{{ t1d_bg_image_mm_custom }}" id="t1d_bg_image_mm_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_mm_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_mm_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_mm_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_mm_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_mm_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                     */
/*                     </fieldset>*/
/* */
/*         </div>*/
/*         */
/*         <div id="tab-backgrounds-footer" class="tab-pane">    */
/* */
/*                     <fieldset>*/
/*                      */
/*                         <legend>Entire Footer <a href="view/image/theme_img/help_oxy_theme/cas_79.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_f1" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f1 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f1 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-2">                                */
/*                                 <a href="" id="t1d_bg_image_f1_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_f1_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_f1_custom" value="{{ t1d_bg_image_f1_custom }}" id="t1d_bg_image_f1_custom" />*/
/*                             </div>*/
/*                             <label class="col-sm-2 control-label">Parallax scrolling effect:</label>*/
/*                             <div class="col-sm-6">*/
/* 								<select name="t1d_bg_image_f1_parallax" class="form-control">*/
/* 									<option value="0"{% if t1d_bg_image_f1_parallax == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1d_bg_image_f1_parallax == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f1_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_f1_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_f1_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_f1_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_f1_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_f1_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_f1_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_f1_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_f1_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_f1_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f1_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_f1_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_f1_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_f1_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_f1_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Top Custom Block <a href="view/image/theme_img/help_oxy_theme/cas_82.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_f6" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f6 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f6 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_f6_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_f6_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_f6_custom" value="{{ t1d_bg_image_f6_custom }}" id="t1d_bg_image_f6_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f6_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_f6_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_f6_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_f6_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_f6_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_f6_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_f6_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_f6_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_f6_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_f6_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f6_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_f6_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/* */
/*                                     <option value="repeat-x"{% if t1d_bg_image_f6_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_f6_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_f6_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <legend>Information, Custom Columns <a href="view/image/theme_img/help_oxy_theme/cas_80.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_f2" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f2 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f2 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_f2_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_f2_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_f2_custom" value="{{ t1d_bg_image_f2_custom }}" id="t1d_bg_image_f2_custom" />*/
/* 					        </div>*/
/* 				        </div> */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f2_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_f2_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_f2_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_f2_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_f2_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_f2_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_f2_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_f2_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_f2_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_f2_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f2_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_f2_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_f2_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_f2_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_f2_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div> */
/*                                              */
/*                         <legend>Powered by, Payment Images, Follow Us <a href="view/image/theme_img/help_oxy_theme/cas_81.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_f3" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f3 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f3 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_f3_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_f3_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_f3_custom" value="{{ t1d_bg_image_f3_custom }}" id="t1d_bg_image_f3_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f3_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_f3_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_f3_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_f3_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_f3_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_f3_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_f3_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_f3_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_f3_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_f3_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f3_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_f3_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_f3_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_f3_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_f3_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                                              */
/*                         <legend>Bottom Custom Block <a href="view/image/theme_img/help_oxy_theme/cas_82.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_f4" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f4 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f4 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_f4_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_f4_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_f4_custom" value="{{ t1d_bg_image_f4_custom }}" id="t1d_bg_image_f4_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f4_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_f4_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_f4_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_f4_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_f4_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_f4_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_f4_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_f4_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_f4_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_f4_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f4_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_f4_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_f4_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_f4_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_f4_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Sliding Bottom Custom Block <a href="view/image/theme_img/help_oxy_theme/cas_82.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label"></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_pattern_k_f5" class="form-control">*/
/* 									<option value="none"selected="selected">none</option>*/
/*                                     */
/*                                     {% for i in 1..75 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f5 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     {% for i in 101..379 %}*/
/* 									<option value="{{ i }}" {% if t1d_pattern_k_f5 == i %}{{ "selected=\"selected\"" }}{% endif %}>{{ i }}</option>*/
/* 									{% endfor %}*/
/*                                     */
/* 								</select>*/
/*                                 <span class="k_help">Select a pattern number.</span>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Upload your own pattern or background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1d_bg_image_f5_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1d_bg_image_f5_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1d_bg_image_f5_custom" value="{{ t1d_bg_image_f5_custom }}" id="t1d_bg_image_f5_custom" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f5_position" class="form-control">*/
/* 								    <option value="top center"{% if t1d_bg_image_f5_position == 'top center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tc }}</option>*/
/*                            	    	<option value="top left"{% if t1d_bg_image_f5_position == 'top left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tl }}</option>*/
/*                            	    	<option value="top right"{% if t1d_bg_image_f5_position == 'top right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_tr }}</option> */
/*                            	    	<option value="center"{% if t1d_bg_image_f5_position == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                            	    	<option value="left"{% if t1d_bg_image_f5_position == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/*                            	    	<option value="right"{% if t1d_bg_image_f5_position == 'right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_r }}</option>*/
/*                            	    	<option value="bottom center"{% if t1d_bg_image_f5_position == 'bottom center' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bc }}</option>*/
/*                            	    	<option value="bottom left"{% if t1d_bg_image_f5_position == 'bottom left' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_bl }}</option>*/
/*                            	    	<option value="bottom right"{% if t1d_bg_image_f5_position == 'bottom right' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_br }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Repeat:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1d_bg_image_f5_repeat" class="form-control">*/
/* 								    <option value="repeat"{% if t1d_bg_image_f5_repeat == 'repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_r }}</option>*/
/*                                     <option value="repeat-x"{% if t1d_bg_image_f5_repeat == 'repeat-x' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_rx }}</option> */
/*                                     <option value="repeat-y"{% if t1d_bg_image_f5_repeat == 'repeat-y' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_ry }}</option>*/
/*                                     <option value="no-repeat"{% if t1d_bg_image_f5_repeat == 'no-repeat' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_repeat_nr }}</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/* */
/*         </div>*/
/* */
/* */
/* */
/*         <div class="vtabs-content">            */
/*         <table class="form">*/
/*         */
/* 					<tr>*/
/* 					    <td>*/
/*                         <br /><div>Transparent patterns:</div><br />*/
/*                         */
/*                         <div style="float:left;margin-bottom:20px">*/
/*                         {% for bgp in 1..75 %} */
/*                             <div class="ptn"><img src="../catalog/view/theme/oxy/image/patterns/admin_thumb/p{{ bgp }}.png"><span class="ptn_nr">{{ bgp }}</span></div> */
/* 						{% endfor %}*/
/* 						</div>*/
/* */
/*                         <div style="float:left;margin-bottom:20px">Non-transparent patterns:</div>   */
/*                         */
/*                         <div style="float:left;">*/
/*                         {% for bgp in 101..379 %} */
/*                             <div class="ptn"><img src="../catalog/view/theme/oxy/image/patterns/admin_thumb/p{{ bgp }}.png"><span class="ptn_nr">{{ bgp }}</span></div> */
/* 						{% endfor %}*/
/* 						</div> */
/*                             */
/* 					    </td>*/
/* 					</tr>*/
/*                     */
/*         </table>  */
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div> */
/*         */
/*         */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-fonts"> */
/*         <div class="row form-horizontal">  */
/* */
/*         <div class="col-sm-12">*/
/*         <div class="tab-content">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <a href="http://www.google.com/webfonts/" target="_blank" class="btn btn-default link" style="margin-left:0">Google Web Fonts Collection &raquo;</a><br /><br />*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 								<thead>*/
/* 									<tr>*/
/*                                     <th class="left" width="20%"></th>*/
/* 									<th class="left" width="20%">Name:</th>*/
/* 									<th class="left" width="15%">{{ text_weight }}</th>*/
/* 									<th class="left" width="15%">{{ text_size }}</th>*/
/* 									<th class="left" width="15%">{{ text_uppercase }}</th>*/
/*                                     <th class="left" width="15%">Style:</th>*/
/* 									</tr>*/
/* 								</thead>*/
/*                                 <tbody>*/
/*                                     <tr>*/
/* 									<td>Body:</td>*/
/* 									<td>*/
/* 								    <input type="text" name="t1d_body_font" value="{{ t1d_body_font ? t1d_body_font : 'Lato' }}" class="form-control" />*/
/*                                     <br /><span class="k_help">Default font:<br /><b>Lato</b></span>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_body_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_body_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_body_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_body_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_body_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_body_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_body_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_body_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_body_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_body_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>      */
/*                            	            <option value="18"{% if t1d_body_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/*                            	            <option value="19"{% if t1d_body_font_size == '19' %}{{ "selected=\"selected\"" }}{% endif %}>19px</option>      */
/*                            	            <option value="20"{% if t1d_body_font_size == '20' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option>*/
/*                            	            <option value="22"{% if t1d_body_font_size == '22' %}{{ "selected=\"selected\"" }}{% endif %}>22px</option>      */
/*                            	            <option value="24"{% if t1d_body_font_size == '24' %}{{ "selected=\"selected\"" }}{% endif %}>24px</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_body_font_uppercase" class="form-control">*/
/* 								        <option value="0"{% if t1d_body_font_uppercase == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                         <option value="1"{% if t1d_body_font_uppercase == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                     */
/*                                     <tr>*/
/* 									<td>Small text:</td>*/
/* 									<td></td>*/
/*                                     <td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_small_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_small_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_small_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_small_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_small_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_small_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_small_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_small_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_small_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_small_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>      */
/*                            	            <option value="18"{% if t1d_small_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                    */
/*                                     <tr>*/
/*                                     <td>Headings and Product Names:</td>*/
/* 									<td>*/
/*                                     <input type="text" name="t1d_title_font" value="{{ t1d_title_font ? t1d_title_font : 'Lato' }}" class="form-control" />*/
/*                                     <br /><span class="k_help">Default font:<br /><b>Lato</b></span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_title_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_title_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_title_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_title_font_weight == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/*                                         <option value="900"{% if t1d_title_font_weight == '900' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_fat }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_title_font_uppercase" class="form-control">*/
/* 								        <option value="0"{% if t1d_title_font_uppercase == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                         <option value="1"{% if t1d_title_font_uppercase == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_title_font_uppercase == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                     */
/*                                     <tr>*/
/*                                     <td>Subtitles:</td>*/
/* 									<td>*/
/*                                     <input type="text" name="t1d_subtitle_font" value="{{ t1d_subtitle_font ? t1d_subtitle_font : 'Lato' }}" class="form-control" />*/
/*                                     <br /><span class="k_help">Default font:<br /><b>Lato</b></span>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_subtitle_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_subtitle_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_subtitle_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_subtitle_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_subtitle_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_subtitle_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_subtitle_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_subtitle_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_subtitle_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_subtitle_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_subtitle_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_subtitle_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_subtitle_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>      */
/*                            	            <option value="18"{% if t1d_subtitle_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/*                                         <option value="19"{% if t1d_subtitle_font_size == '19' %}{{ "selected=\"selected\"" }}{% endif %}>19px</option> */
/*                                         <option value="20"{% if t1d_subtitle_font_size == '20' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option> */
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_subtitle_font_uppercase" class="form-control">*/
/*                                         <option value="0"{% if t1d_subtitle_font_uppercase == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                         <option value="1"{% if t1d_subtitle_font_uppercase == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_subtitle_font_style" class="form-control">*/
/* 								        <option value="normal"{% if t1d_subtitle_font_style == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>Normal</option>*/
/*                                         <option value="italic"{% if t1d_subtitle_font_style == 'italic' %}{{ "selected=\"selected\"" }}{% endif %}>Italic</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     </tr>*/
/*                                     */
/*                                     <tr>*/
/*                                     <td>Prices:</td>*/
/* 									<td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_price_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_price_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_price_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_price_font_weight == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                     */
/*                                     <tr>*/
/*                                     <td>Buttons:</td>*/
/* 									<td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_button_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_button_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_button_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_button_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_button_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_button_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_button_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_button_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_button_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_button_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_button_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_button_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_button_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>      */
/*                            	            <option value="18"{% if t1d_button_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_button_font_uppercase" class="form-control">*/
/*                                         <option value="0"{% if t1d_button_font_uppercase == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                         <option value="1"{% if t1d_button_font_uppercase == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                                                    */
/*                                     <tr>*/
/*                                     <td>Main Menu Bar:</td>*/
/* 									<td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_mm_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_mm_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_font_weight == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/*                                         <option value="900"{% if t1d_mm_font_weight == '900' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_fat }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_mm_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_mm_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_mm_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_mm_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_mm_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_mm_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_mm_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_mm_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>*/
/*                            	            <option value="18"{% if t1d_mm_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/*                            	            <option value="19"{% if t1d_mm_font_size == '19' %}{{ "selected=\"selected\"" }}{% endif %}>19px</option>      */
/*                            	            <option value="20"{% if t1d_mm_font_size == '20' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option>*/
/*                            	            <option value="22"{% if t1d_mm_font_size == '22' %}{{ "selected=\"selected\"" }}{% endif %}>22px</option>      */
/*                            	            <option value="24"{% if t1d_mm_font_size == '24' %}{{ "selected=\"selected\"" }}{% endif %}>24px</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_font_uppercase" class="form-control">*/
/* 								        <option value="0"{% if t1d_mm_font_uppercase == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                         <option value="1"{% if t1d_mm_font_uppercase == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_font_uppercase == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                     */
/*                                     <tr>*/
/*                                     <td>Sub-Menu Main Categories:</td>*/
/* 									<td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_sub_main_cat_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_mm_sub_main_cat_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_mm_sub_main_cat_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sub_main_cat_font_weight == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_sub_main_cat_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_mm_sub_main_cat_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_mm_sub_main_cat_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_mm_sub_main_cat_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_mm_sub_main_cat_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_mm_sub_main_cat_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sub_main_cat_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_mm_sub_main_cat_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_mm_sub_main_cat_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_mm_sub_main_cat_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>*/
/*                            	            <option value="18"{% if t1d_mm_sub_main_cat_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/*                                         <option value="19"{% if t1d_mm_sub_main_cat_font_size == '19' %}{{ "selected=\"selected\"" }}{% endif %}>19px</option>      */
/*                            	            <option value="20"{% if t1d_mm_sub_main_cat_font_size == '20' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/*                                     */
/*                                     <tr>*/
/*                                     <td>Sub-Menu Other Links:</td>*/
/* 									<td></td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_sub_font_weight" class="form-control">*/
/* 								        <option value="normal"{% if t1d_mm_sub_font_weight == 'normal' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_normal }}</option>*/
/*                                         <option value="bold"{% if t1d_mm_sub_font_weight == 'bold' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_bold }}</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <select name="t1d_mm_sub_font_size" class="form-control">*/
/*                                         <option value="10"{% if t1d_mm_sub_font_size == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                         <option value="11"{% if t1d_mm_sub_font_size == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11px</option>*/
/* 								        <option value="12"{% if t1d_mm_sub_font_size == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12px</option>*/
/*                            	            <option value="13"{% if t1d_mm_sub_font_size == '13' %}{{ "selected=\"selected\"" }}{% endif %}>13px</option>      */
/*                            	            <option value="14"{% if t1d_mm_sub_font_size == '14' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1d_mm_sub_font_size == '' %}{{ "selected=\"selected\"" }}{% endif %}>14px</option>      */
/*                            	            <option value="15"{% if t1d_mm_sub_font_size == '15' %}{{ "selected=\"selected\"" }}{% endif %}>15px</option>      */
/*                            	            <option value="16"{% if t1d_mm_sub_font_size == '16' %}{{ "selected=\"selected\"" }}{% endif %}>16px</option>*/
/*                            	            <option value="17"{% if t1d_mm_sub_font_size == '17' %}{{ "selected=\"selected\"" }}{% endif %}>17px</option>*/
/*                            	            <option value="18"{% if t1d_mm_sub_font_size == '18' %}{{ "selected=\"selected\"" }}{% endif %}>18px</option>*/
/* 							        </select>*/
/*                                     </td>*/
/*                                     <td></td>*/
/*                                     <td></td>*/
/*                                     </tr>*/
/* */
/*                                 </tbody>*/
/* 							</table>*/
/*                         */
/*                         </div>                    */
/*                         */
/*                     </fieldset>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-skins"> */
/*         <div class="row form-horizontal">  */
/* */
/*         <div class="col-sm-12">*/
/*         <div class="tab-content">*/
/*         */
/*             <label>Color Scheme: </label>           */
/*             <select name="t1d_skin" class="form-control">*/
/*                  <option value="skin1-default"{% if t1d_skin == 'skin1-default' %}{{ "selected=\"selected\"" }}{% endif %}>Default</option>*/
/*                  <option value="skin2-fashion"{% if t1d_skin == 'skin2-fashion' %}{{ "selected=\"selected\"" }}{% endif %}>Fashion</option>  */
/*                  <option value="skin3-sport"{% if t1d_skin == 'skin3-sport' %}{{ "selected=\"selected\"" }}{% endif %}>Sport</option>*/
/*                  <option value="skin4-organic"{% if t1d_skin == 'skin4-organic' %}{{ "selected=\"selected\"" }}{% endif %}>Organic</option>*/
/*                  <option value="skin5-kids"{% if t1d_skin == 'skin5-kids' %}{{ "selected=\"selected\"" }}{% endif %}>Kids</option>                 */
/*             </select> */
/*             */
/*             <p><br /><br />       */
/*             This option enable the selected color scheme in your store. By default, only "Default" color scheme can be edited in the admin panel. All other color schemes are pre-made and can not be edited in the admin panel.*/
/*             </p>*/
/*             <p><br />*/
/*             If you want to make your store have the same settings like our demo (layout, main menu, etc.), please import our demo content (for more information, see the "Demo Content" section in the theme documentation) or copy the settings of a theme modules from our demo. */
/*             </p>*/
/*             <p><br />*/
/*             We provide access to the admin sections:*/
/*             </p>*/
/*             <p>*/
/*             Extensions › Modules*/
/*             <br />*/
/*             System › Design › Layouts*/
/*             <br />*/
/*             System › Design › Banners*/
/*             </p>*/
/*             <p><br />*/
/*             Access to the admin panel for each demo:*/
/*             </p>*/
/*             <p>*/
/*             Username: demo*/
/*             <br />*/
/*             Password: demo*/
/*             </p>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div> */
/*         */
/*         */
/* */
/* */
/* */
/* */
/*         <!-- -->         */
/*         </div>  */
/*         */
/*         */
/*     </form>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* */
/* <script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>*/
/* <script type="text/javascript" src="view/javascript/poshytip/jquery.poshytip.js"></script>*/
/* <link rel="stylesheet" type="text/css" href="view/javascript/poshytip/tip-twitter/tip-twitter.css" />*/
/* */
/* {{ footer }}*/
/* */
