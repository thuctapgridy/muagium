<?php

/* oxy/template/product/category.twig */
class __TwigTemplate_69a6dcc1b048dde1e3a64328ff0f5fe2f9a5ed9c6bedaa9a6b9e534ba50fbcb8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "

";
        // line 3
        if (((isset($context["t1o_category_title_position"]) ? $context["t1o_category_title_position"] : null) == 0)) {
            // line 4
            echo "<div id=\"title_above_content\" class=\"category_top_title\" ";
            if ((((isset($context["t1o_category_img_status"]) ? $context["t1o_category_img_status"] : null) == 1) && (isset($context["thumb"]) ? $context["thumb"] : null))) {
                echo "style=\"background-image: url(";
                echo (isset($context["thumb"]) ? $context["thumb"] : null);
                echo ")\"";
            }
            echo " ";
            if (((isset($context["t1o_category_img_parallax"]) ? $context["t1o_category_img_parallax"] : null) == 1)) {
                echo "data-stellar-background-ratio=\"0.5\"";
            }
            echo ">
<div class=\"container full-width-container\">
<h1>";
            // line 6
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h1>
";
            // line 7
            if ((((isset($context["t1o_category_desc_status"]) ? $context["t1o_category_desc_status"] : null) == 1) && (isset($context["description"]) ? $context["description"] : null))) {
                // line 8
                echo "<div class=\"cat-description-above-content subtitle\">
";
                // line 9
                echo (isset($context["description"]) ? $context["description"] : null);
                echo "
</div>
";
            }
            // line 12
            echo "</div>
</div>
";
        }
        // line 15
        echo "
<div id=\"product-category\" class=\"container full-width-container\">
  <ul class=\"breadcrumb\">
    ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 19
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "  </ul>
  <div class=\"row\">";
        // line 22
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 23
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 24
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 25
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 26
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 27
            echo "    ";
        } else {
            // line 28
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 29
            echo "    ";
        }
        // line 30
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">
    
      ";
        // line 32
        if (((isset($context["t1o_category_title_position"]) ? $context["t1o_category_title_position"] : null) == 1)) {
            // line 33
            echo "      <div class=\"row content-padd\">
        <div class=\"col-sm-";
            // line 34
            if (((((isset($context["t1o_category_desc_status"]) ? $context["t1o_category_desc_status"] : null) == 1) && ((isset($context["t1o_category_img_status"]) ? $context["t1o_category_img_status"] : null) == 1)) && (isset($context["thumb"]) ? $context["thumb"] : null))) {
                echo "8";
            } else {
                echo "12";
            }
            echo " cat-description\">
          <h1>";
            // line 35
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h1>
          ";
            // line 36
            if ((((isset($context["t1o_category_desc_status"]) ? $context["t1o_category_desc_status"] : null) == 1) && (isset($context["description"]) ? $context["description"] : null))) {
                // line 37
                echo "          <div class=\"subtitle\">";
                echo (isset($context["description"]) ? $context["description"] : null);
                echo "</div>
          ";
            }
            // line 38
            echo "   
        </div>
        ";
            // line 40
            if ((((isset($context["t1o_category_img_status"]) ? $context["t1o_category_img_status"] : null) == 1) && (isset($context["thumb"]) ? $context["thumb"] : null))) {
                // line 41
                echo "        <div class=\"col-sm-4 cat-img\">
          <img src=\"";
                // line 42
                echo (isset($context["thumb"]) ? $context["thumb"] : null);
                echo "\" alt=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" title=\"";
                echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
                echo "\" class=\"img-thumbnail\" />
        </div>
        ";
            }
            // line 45
            echo "      </div>
      ";
        }
        // line 47
        echo " 
      ";
        // line 48
        if (((isset($context["t1o_category_subcategories_status"]) ? $context["t1o_category_subcategories_status"] : null) == 1)) {
            // line 49
            echo "      ";
            if (((isset($context["t1o_category_subcategories_style"]) ? $context["t1o_category_subcategories_style"] : null) == 0)) {
                // line 50
                echo "      
      ";
                // line 51
                if ((isset($context["categories"]) ? $context["categories"] : null)) {
                    // line 52
                    echo "      <div class=\"category-list row\">
      <div class=\"category-list-carousel\">
      ";
                    // line 54
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                        // line 55
                        echo "\t\t<div>
          ";
                        // line 56
                        if ($this->getAttribute($context["category"], "thumb", array())) {
                            // line 57
                            echo "\t\t  <a href=\"";
                            echo $this->getAttribute($context["category"], "href", array());
                            echo "\"><div class=\"image\"><img src=\"";
                            echo $this->getAttribute($context["category"], "thumb", array());
                            echo "\" alt=\"";
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "\" />
          <div class=\"subcatname\">";
                            // line 58
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "</div>
          </div></a>
          ";
                        } else {
                            // line 61
                            echo "          <a href=\"";
                            echo $this->getAttribute($context["category"], "href", array());
                            echo "\">
          <div class=\"subcatname\">";
                            // line 62
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "</div>
          </a>
\t\t  ";
                        }
                        // line 65
                        echo "\t\t</div>
      ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 67
                    echo "      </div>
      </div>     
      ";
                }
                // line 70
                echo "<script type=\"text/javascript\"><!--
\$('.category-list-carousel').owlCarousel({
\titems: '";
                // line 72
                echo (isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null);
                echo "',
\titemsMobile : [479, 2],
\tautoPlay: '";
                // line 74
                echo (isset($context["t1o_category_subcategories_autoplay"]) ? $context["t1o_category_subcategories_autoplay"] : null);
                echo "',
\tsingleItem: false,
\tscrollPerPage: false,
\tpagination: false,
\tnavigation: true,
\tnavigationText: ['<i class=\"fa fa-chevron-left fa-5x\"></i>', '<i class=\"fa fa-chevron-right fa-5x\"></i>']
});
--></script>
   
      ";
            } else {
                // line 84
                echo "      
      ";
                // line 85
                if ((isset($context["categories"]) ? $context["categories"] : null)) {
                    // line 86
                    echo "      <h3>";
                    echo (isset($context["text_refine"]) ? $context["text_refine"] : null);
                    echo "</h3>
      ";
                    // line 87
                    if ((twig_length_filter($this->env, (isset($context["categories"]) ? $context["categories"] : null)) <= 5)) {
                        // line 88
                        echo "      <div class=\"row category-list-oc\">
        <div class=\"col-sm-3\">
          <ul>
            ";
                        // line 91
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 92
                            echo "            <li><a href=\"";
                            echo $this->getAttribute($context["category"], "href", array());
                            echo "\">";
                            echo $this->getAttribute($context["category"], "name", array());
                            echo "</a></li>
            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 94
                        echo "          </ul>
        </div>
      </div>
      ";
                    } else {
                        // line 98
                        echo "      <div class=\"row category-list-oc\">
        ";
                        // line 99
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(twig_array_batch((isset($context["categories"]) ? $context["categories"] : null), twig_round((twig_length_filter($this->env, (isset($context["categories"]) ? $context["categories"] : null)) / 4), 1, "ceil")));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 100
                            echo "        <div class=\"col-sm-3\">
          <ul>
            ";
                            // line 102
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                                // line 103
                                echo "            <li><a href=\"";
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a></li>
            ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 105
                            echo "          </ul>
        </div>
        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 108
                        echo "      </div>
      ";
                    }
                    // line 110
                    echo "      ";
                }
                // line 111
                echo "  
      ";
            }
            // line 113
            echo "      ";
        }
        // line 114
        echo "      
      ";
        // line 115
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "

      ";
        // line 117
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 118
            echo "      <div class=\"row content-padd padd-t-b-25 product-filter\">
        <div class=\"col-md-4\">
          <div class=\"btn-group hidden-xs\">
            <button type=\"button\" id=\"list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 121
            echo (isset($context["button_list"]) ? $context["button_list"] : null);
            echo "\"><i class=\"fa fa-th-list\"></i></button>
            <button type=\"button\" id=\"small-list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 122
            echo $this->getAttribute((isset($context["t1o_text_small_list"]) ? $context["t1o_text_small_list"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "\"><i class=\"fa fa-list\"></i></button>
            <button type=\"button\" id=\"grid-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 123
            echo (isset($context["button_grid"]) ? $context["button_grid"] : null);
            echo "\"><i class=\"fa fa-th\"></i></button>
            <button type=\"button\" id=\"gallery-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 124
            echo $this->getAttribute((isset($context["t1o_text_gallery"]) ? $context["t1o_text_gallery"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "\"><i class=\"fa fa-th-large\"></i></button>
          </div>
        </div>
        <div class=\"col-md-4\">
          <a href=\"";
            // line 128
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\" id=\"compare-total\" class=\"btn btn-default\">";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo "</a>
        </div>
        <div class=\"col-md-2 text-right\">
          <select id=\"input-sort\" class=\"form-control\" onchange=\"location = this.value;\">

              ";
            // line 133
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 134
                echo "              ";
                if (($this->getAttribute($context["sorts"], "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
                    // line 135
                    echo "
              <option value=\"";
                    // line 136
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>

              ";
                } else {
                    // line 139
                    echo "
              <option value=\"";
                    // line 140
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
              
              ";
                }
                // line 143
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 144
            echo "
          </select>
        </div>
        <div class=\"col-md-2 text-right\">
          <select id=\"input-limit\" class=\"form-control\" onchange=\"location = this.value;\">
              
              ";
            // line 150
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 151
                echo "              ";
                if (($this->getAttribute($context["limits"], "value", array()) == (isset($context["limit"]) ? $context["limit"] : null))) {
                    // line 152
                    echo "
              <option value=\"";
                    // line 153
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>

              ";
                } else {
                    // line 156
                    echo "
              <option value=\"";
                    // line 157
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>

              ";
                }
                // line 160
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 161
            echo "
            </select>
        </div>
      </div>

      <div class=\"row product-items category-product-items \">
        ";
            // line 167
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 168
                echo "        <div class=\"product-layout product-list col-xs-12\">
          <div class=\"product-thumb\">
          
            <div class=\"image\">
            
            ";
                // line 173
                if ((($this->getAttribute($context["product"], "out_of_stock_quantity", array()) <= 0) && ((isset($context["t1o_out_of_stock_badge_status"]) ? $context["t1o_out_of_stock_badge_status"] : null) == 1))) {
                    echo " 
            <span class=\"badge out-of-stock\"><span>";
                    // line 174
                    echo $this->getAttribute($context["product"], "out_of_stock_badge", array());
                    echo "</span></span>
            ";
                }
                // line 175
                echo " 
              
            <span class=\"badge-wrapper\">
            
            ";
                // line 179
                if (((isset($context["t1o_sale_badge_status"]) ? $context["t1o_sale_badge_status"] : null) == 1)) {
                    echo "\t
            ";
                    // line 180
                    if (($this->getAttribute($context["product"], "special", array()) && ((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == 0))) {
                        // line 181
                        echo "            <span class=\"badge sale\">";
                        echo $this->getAttribute((isset($context["t1o_text_sale"]) ? $context["t1o_text_sale"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</span>
            ";
                    }
                    // line 182
                    echo " 
            ";
                    // line 183
                    if (($this->getAttribute($context["product"], "special", array()) && ((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == 1))) {
                        echo "            
            ";
                        // line 184
                        $context["res"] = (($this->getAttribute($context["product"], "val1", array()) / $this->getAttribute($context["product"], "val2", array())) * 100);
                        // line 185
                        echo "            ";
                        $context["res"] = (100 - (isset($context["res"]) ? $context["res"] : null));
                        // line 186
                        echo "            <span class=\"badge sale\">-";
                        echo twig_round((isset($context["res"]) ? $context["res"] : null), 0, "common");
                        echo "%</span>
            ";
                    }
                    // line 188
                    echo "            ";
                }
                // line 189
                echo "      
            ";
                // line 190
                if (((isset($context["t1o_new_badge_status"]) ? $context["t1o_new_badge_status"] : null) == 1)) {
                    echo "\t
            ";
                    // line 191
                    $context["days"] = ((twig_round($this->getAttribute($context["product"], "endDate2", array()), 0, "common") / 86400) - (twig_round($this->getAttribute($context["product"], "startDate1", array()), 0, "common") / 86400));
                    // line 192
                    echo "            ";
                    $context["newproductdays"] = 30;
                    // line 193
                    echo "            ";
                    if (((isset($context["days"]) ? $context["days"] : null) < (isset($context["newproductdays"]) ? $context["newproductdays"] : null))) {
                        // line 194
                        echo "            <span class=\"badge new\">";
                        echo $this->getAttribute((isset($context["t1o_text_new_prod"]) ? $context["t1o_text_new_prod"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</span>
            ";
                    }
                    // line 196
                    echo "            ";
                }
                // line 197
                echo "            
            </span>
            
            ";
                // line 200
                if (((isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null) == "product-box-style-3")) {
                    // line 201
                    echo "            <div class=\"flybar-top\">  
            <div class=\"flybar-top-items\">
            <p class=\"description\">";
                    // line 203
                    echo $this->getAttribute($context["product"], "description", array());
                    echo "</p>
            <div class=\"rating\">
              ";
                    // line 205
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 206
                        echo "              ";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                            // line 207
                            echo "              <span class=\"fa fa-stack fa-g\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
              ";
                        } else {
                            // line 209
                            echo "              <span class=\"fa fa-stack fa-y\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
              ";
                        }
                        // line 211
                        echo "              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 212
                    echo "            </div>
            </div>   
            </div>
            ";
                }
                // line 216
                echo "      
            <div class=\"flybar\">  
            <div class=\"flybar-items\">
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 219
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "\" onclick=\"cart.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array());
                echo "');\" class=\"btn btn-default\"><i class=\"fa fa-shopping-bag\"></i></button>
            <a class=\"btn btn-default quickview\" href=\"";
                // line 220
                echo $this->getAttribute($context["product"], "quickview", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo $this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo "\"><i class=\"fa fa-search\"></i></a>
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 221
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"wishlist\"><i class=\"fa fa-heart\"></i></button>
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 222
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"compare\"><i class=\"fa fa-retweet\"></i></button>
            </div>   
            </div>
            
            ";
                // line 226
                if ($this->getAttribute($context["product"], "thumb_swap", array())) {
                    // line 227
                    echo "            <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\">
            <img src=\"";
                    // line 228
                    echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                    echo "\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive img-";
                    echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                    echo " lazyload\" />
            <img src=\"";
                    // line 229
                    echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                    echo "\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb_swap", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive thumb_swap img-";
                    echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                    echo " lazyload\" />
            </a>
            ";
                } else {
                    // line 232
                    echo "            <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                    echo "\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive img-";
                    echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                    echo " lazyload\" /></a>
            ";
                }
                // line 234
                echo "            </div>
            
            <div>
            
              <div class=\"caption\">
                <div class=\"name\"><h4><a href=\"";
                // line 239
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></h4></div>
                <div class=\"product_box_brand\">";
                // line 240
                if ($this->getAttribute($context["product"], "brand", array())) {
                    echo "<a href=\"";
                    echo $this->getAttribute($context["product"], "brand_url", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "brand", array());
                    echo "</a>";
                }
                echo "</div>
                <p class=\"description\">";
                // line 241
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>

                <div class=\"rating\">
                  ";
                // line 244
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 245
                    echo "                  ";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        // line 246
                        echo "                  <span class=\"fa fa-stack fa-g\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
                  ";
                    } else {
                        // line 248
                        echo "                  <span class=\"fa fa-stack fa-y\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
                  ";
                    }
                    // line 250
                    echo "                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 251
                echo "                </div>
                
                ";
                // line 253
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 254
                    echo "                <p class=\"price\">
                  ";
                    // line 255
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 256
                        echo "                  ";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
                  ";
                    } else {
                        // line 258
                        echo "                  <span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span> <span class=\"price-old\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
                  ";
                    }
                    // line 260
                    echo "                </p>
                ";
                }
                // line 262
                echo "                
                <div class=\"product-list-buttons\">
                  <button type=\"button\" onclick=\"cart.add('";
                // line 264
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array());
                echo "');\" class=\"btn btn-default cart\"><i class=\"fa fa-shopping-bag\"></i> <span>";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button>
                  <a class=\"btn btn-default quickview list-quickview\" href=\"";
                // line 265
                echo $this->getAttribute($context["product"], "quickview", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo $this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo "\"><i class=\"fa fa-search\"></i></a>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 266
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"btn btn-default list-wishlist\"><i class=\"fa fa-heart\"></i></button>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 267
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"btn btn-default list-compare\"><i class=\"fa fa-retweet\"></i></button>
                </div>
                
              </div>
              
            </div>
          </div>
          
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 277
            echo "      </div>
      <div class=\"row pagination-box content-padd padd-t-b-30\">
        <div class=\"col-sm-6 text-left\">";
            // line 279
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "</div>
        <div class=\"col-sm-6 text-right\">";
            // line 280
            echo (isset($context["results"]) ? $context["results"] : null);
            echo "</div>
      </div>
      ";
        }
        // line 283
        echo "      ";
        if (( !(isset($context["categories"]) ? $context["categories"] : null) &&  !(isset($context["products"]) ? $context["products"] : null))) {
            // line 284
            echo "      <p>";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
      <div class=\"buttons\">
        <div class=\"pull-right\"><a href=\"";
            // line 286
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a></div>
      </div>
      ";
        }
        // line 289
        echo "      ";
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 290
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
";
        // line 292
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " 
";
    }

    public function getTemplateName()
    {
        return "oxy/template/product/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  834 => 292,  829 => 290,  824 => 289,  816 => 286,  810 => 284,  807 => 283,  801 => 280,  797 => 279,  793 => 277,  775 => 267,  769 => 266,  763 => 265,  755 => 264,  751 => 262,  747 => 260,  739 => 258,  733 => 256,  731 => 255,  728 => 254,  726 => 253,  722 => 251,  716 => 250,  712 => 248,  708 => 246,  705 => 245,  701 => 244,  695 => 241,  685 => 240,  679 => 239,  672 => 234,  656 => 232,  642 => 229,  630 => 228,  625 => 227,  623 => 226,  614 => 222,  608 => 221,  602 => 220,  594 => 219,  589 => 216,  583 => 212,  577 => 211,  573 => 209,  569 => 207,  566 => 206,  562 => 205,  557 => 203,  553 => 201,  551 => 200,  546 => 197,  543 => 196,  537 => 194,  534 => 193,  531 => 192,  529 => 191,  525 => 190,  522 => 189,  519 => 188,  513 => 186,  510 => 185,  508 => 184,  504 => 183,  501 => 182,  495 => 181,  493 => 180,  489 => 179,  483 => 175,  478 => 174,  474 => 173,  467 => 168,  463 => 167,  455 => 161,  449 => 160,  441 => 157,  438 => 156,  430 => 153,  427 => 152,  424 => 151,  420 => 150,  412 => 144,  406 => 143,  398 => 140,  395 => 139,  387 => 136,  384 => 135,  381 => 134,  377 => 133,  367 => 128,  360 => 124,  356 => 123,  352 => 122,  348 => 121,  343 => 118,  341 => 117,  336 => 115,  333 => 114,  330 => 113,  326 => 111,  323 => 110,  319 => 108,  311 => 105,  300 => 103,  296 => 102,  292 => 100,  288 => 99,  285 => 98,  279 => 94,  268 => 92,  264 => 91,  259 => 88,  257 => 87,  252 => 86,  250 => 85,  247 => 84,  234 => 74,  229 => 72,  225 => 70,  220 => 67,  213 => 65,  207 => 62,  202 => 61,  196 => 58,  187 => 57,  185 => 56,  182 => 55,  178 => 54,  174 => 52,  172 => 51,  169 => 50,  166 => 49,  164 => 48,  161 => 47,  157 => 45,  147 => 42,  144 => 41,  142 => 40,  138 => 38,  132 => 37,  130 => 36,  126 => 35,  118 => 34,  115 => 33,  113 => 32,  107 => 30,  104 => 29,  101 => 28,  98 => 27,  95 => 26,  92 => 25,  89 => 24,  87 => 23,  83 => 22,  80 => 21,  69 => 19,  65 => 18,  60 => 15,  55 => 12,  49 => 9,  46 => 8,  44 => 7,  40 => 6,  26 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ header }}*/
/* */
/* {% if t1o_category_title_position == 0 %}*/
/* <div id="title_above_content" class="category_top_title" {% if t1o_category_img_status == 1 and thumb %}style="background-image: url({{ thumb }})"{% endif %} {% if t1o_category_img_parallax == 1 %}data-stellar-background-ratio="0.5"{% endif %}>*/
/* <div class="container full-width-container">*/
/* <h1>{{ heading_title }}</h1>*/
/* {% if t1o_category_desc_status == 1 and description %}*/
/* <div class="cat-description-above-content subtitle">*/
/* {{ description }}*/
/* </div>*/
/* {% endif %}*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* <div id="product-category" class="container full-width-container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">*/
/*     */
/*       {% if t1o_category_title_position == 1 %}*/
/*       <div class="row content-padd">*/
/*         <div class="col-sm-{% if t1o_category_desc_status == 1 and t1o_category_img_status == 1 and thumb %}8{% else %}12{% endif %} cat-description">*/
/*           <h1>{{ heading_title }}</h1>*/
/*           {% if t1o_category_desc_status == 1 and description %}*/
/*           <div class="subtitle">{{ description }}</div>*/
/*           {% endif %}   */
/*         </div>*/
/*         {% if t1o_category_img_status == 1 and thumb %}*/
/*         <div class="col-sm-4 cat-img">*/
/*           <img src="{{ thumb }}" alt="{{ heading_title }}" title="{{ heading_title }}" class="img-thumbnail" />*/
/*         </div>*/
/*         {% endif %}*/
/*       </div>*/
/*       {% endif %}*/
/*  */
/*       {% if t1o_category_subcategories_status == 1 %}*/
/*       {% if t1o_category_subcategories_style == 0 %}*/
/*       */
/*       {% if categories %}*/
/*       <div class="category-list row">*/
/*       <div class="category-list-carousel">*/
/*       {% for category in categories %}*/
/* 		<div>*/
/*           {% if category.thumb %}*/
/* 		  <a href="{{ category.href }}"><div class="image"><img src="{{ category.thumb }}" alt="{{ category.name }}" />*/
/*           <div class="subcatname">{{ category.name }}</div>*/
/*           </div></a>*/
/*           {% else %}*/
/*           <a href="{{ category.href }}">*/
/*           <div class="subcatname">{{ category.name }}</div>*/
/*           </a>*/
/* 		  {% endif %}*/
/* 		</div>*/
/*       {% endfor %}*/
/*       </div>*/
/*       </div>     */
/*       {% endif %}*/
/* <script type="text/javascript"><!--*/
/* $('.category-list-carousel').owlCarousel({*/
/* 	items: '{{ t1o_category_subcategories_per_row }}',*/
/* 	itemsMobile : [479, 2],*/
/* 	autoPlay: '{{ t1o_category_subcategories_autoplay }}',*/
/* 	singleItem: false,*/
/* 	scrollPerPage: false,*/
/* 	pagination: false,*/
/* 	navigation: true,*/
/* 	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']*/
/* });*/
/* --></script>*/
/*    */
/*       {% else %}*/
/*       */
/*       {% if categories %}*/
/*       <h3>{{ text_refine }}</h3>*/
/*       {% if categories|length <= 5 %}*/
/*       <div class="row category-list-oc">*/
/*         <div class="col-sm-3">*/
/*           <ul>*/
/*             {% for category in categories %}*/
/*             <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*         </div>*/
/*       </div>*/
/*       {% else %}*/
/*       <div class="row category-list-oc">*/
/*         {% for category in categories|batch((categories|length / 4)|round(1, 'ceil')) %}*/
/*         <div class="col-sm-3">*/
/*           <ul>*/
/*             {% for category in categories %}*/
/*             <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*         </div>*/
/*         {% endfor %}*/
/*       </div>*/
/*       {% endif %}*/
/*       {% endif %}*/
/*   */
/*       {% endif %}*/
/*       {% endif %}*/
/*       */
/*       {{ content_top }}*/
/* */
/*       {% if products %}*/
/*       <div class="row content-padd padd-t-b-25 product-filter">*/
/*         <div class="col-md-4">*/
/*           <div class="btn-group hidden-xs">*/
/*             <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="{{ button_list }}"><i class="fa fa-th-list"></i></button>*/
/*             <button type="button" id="small-list-view" class="btn btn-default" data-toggle="tooltip" title="{{ t1o_text_small_list[lang_id] }}"><i class="fa fa-list"></i></button>*/
/*             <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="{{ button_grid }}"><i class="fa fa-th"></i></button>*/
/*             <button type="button" id="gallery-view" class="btn btn-default" data-toggle="tooltip" title="{{ t1o_text_gallery[lang_id] }}"><i class="fa fa-th-large"></i></button>*/
/*           </div>*/
/*         </div>*/
/*         <div class="col-md-4">*/
/*           <a href="{{ compare }}" id="compare-total" class="btn btn-default">{{ text_compare }}</a>*/
/*         </div>*/
/*         <div class="col-md-2 text-right">*/
/*           <select id="input-sort" class="form-control" onchange="location = this.value;">*/
/* */
/*               {% for sorts in sorts %}*/
/*               {% if sorts.value == '%s-%s'|format(sort, order) %}*/
/* */
/*               <option value="{{ sorts.href }}" selected="selected">{{ sorts.text }}</option>*/
/* */
/*               {% else %}*/
/* */
/*               <option value="{{ sorts.href }}">{{ sorts.text }}</option>*/
/*               */
/*               {% endif %}*/
/*               {% endfor %}*/
/* */
/*           </select>*/
/*         </div>*/
/*         <div class="col-md-2 text-right">*/
/*           <select id="input-limit" class="form-control" onchange="location = this.value;">*/
/*               */
/*               {% for limits in limits %}*/
/*               {% if limits.value == limit %}*/
/* */
/*               <option value="{{ limits.href }}" selected="selected">{{ limits.text }}</option>*/
/* */
/*               {% else %}*/
/* */
/*               <option value="{{ limits.href }}">{{ limits.text }}</option>*/
/* */
/*               {% endif %}*/
/*               {% endfor %}*/
/* */
/*             </select>*/
/*         </div>*/
/*       </div>*/
/* */
/*       <div class="row product-items category-product-items ">*/
/*         {% for product in products %}*/
/*         <div class="product-layout product-list col-xs-12">*/
/*           <div class="product-thumb">*/
/*           */
/*             <div class="image">*/
/*             */
/*             {% if product.out_of_stock_quantity <= 0 and t1o_out_of_stock_badge_status == 1 %} */
/*             <span class="badge out-of-stock"><span>{{ product.out_of_stock_badge }}</span></span>*/
/*             {% endif %} */
/*               */
/*             <span class="badge-wrapper">*/
/*             */
/*             {% if t1o_sale_badge_status == 1 %}	*/
/*             {% if product.special and t1o_sale_badge_type == 0 %}*/
/*             <span class="badge sale">{{ t1o_text_sale[lang_id] }}</span>*/
/*             {% endif %} */
/*             {% if product.special and t1o_sale_badge_type == 1 %}            */
/*             {% set res = (product.val1 / product.val2) * 100 %}*/
/*             {% set res = 100 - res %}*/
/*             <span class="badge sale">-{{ res|round(0, 'common') }}%</span>*/
/*             {% endif %}*/
/*             {% endif %}*/
/*       */
/*             {% if t1o_new_badge_status == 1 %}	*/
/*             {% set days = (product.endDate2|round(0, 'common') / 86400) - (product.startDate1|round(0, 'common') /86400) %}*/
/*             {% set newproductdays = 30 %}*/
/*             {% if days < newproductdays %}*/
/*             <span class="badge new">{{ t1o_text_new_prod[lang_id] }}</span>*/
/*             {% endif %}*/
/*             {% endif %}*/
/*             */
/*             </span>*/
/*             */
/*             {% if t1o_category_prod_box_style == 'product-box-style-3' %}*/
/*             <div class="flybar-top">  */
/*             <div class="flybar-top-items">*/
/*             <p class="description">{{ product.description }}</p>*/
/*             <div class="rating">*/
/*               {% for i in 1..5 %}*/
/*               {% if product.rating < i %}*/
/*               <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*               {% else %}*/
/*               <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*               {% endif %}*/
/*               {% endfor %}*/
/*             </div>*/
/*             </div>   */
/*             </div>*/
/*             {% endif %}*/
/*       */
/*             <div class="flybar">  */
/*             <div class="flybar-items">*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}', '{{ product.minimum }}');" class="btn btn-default"><i class="fa fa-shopping-bag"></i></button>*/
/*             <a class="btn btn-default quickview" href="{{ product.quickview }}" data-toggle="tooltip" title="{{ t1o_text_quickview[lang_id] }}"><i class="fa fa-search"></i></a>*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');" class="wishlist"><i class="fa fa-heart"></i></button>*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');" class="compare"><i class="fa fa-retweet"></i></button>*/
/*             </div>   */
/*             </div>*/
/*             */
/*             {% if product.thumb_swap %}*/
/*             <a href="{{ product.href }}">*/
/*             <img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive img-{{ t1d_img_style }} lazyload" />*/
/*             <img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb_swap }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive thumb_swap img-{{ t1d_img_style }} lazyload" />*/
/*             </a>*/
/*             {% else %}*/
/*             <a href="{{ product.href }}"><img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive img-{{ t1d_img_style }} lazyload" /></a>*/
/*             {% endif %}*/
/*             </div>*/
/*             */
/*             <div>*/
/*             */
/*               <div class="caption">*/
/*                 <div class="name"><h4><a href="{{ product.href }}">{{ product.name }}</a></h4></div>*/
/*                 <div class="product_box_brand">{% if product.brand %}<a href="{{ product.brand_url }}">{{ product.brand }}</a>{% endif %}</div>*/
/*                 <p class="description">{{ product.description }}</p>*/
/* */
/*                 <div class="rating">*/
/*                   {% for i in 1..5 %}*/
/*                   {% if product.rating < i %}*/
/*                   <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*                   {% else %}*/
/*                   <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*                   {% endif %}*/
/*                   {% endfor %}*/
/*                 </div>*/
/*                 */
/*                 {% if product.price %}*/
/*                 <p class="price">*/
/*                   {% if not product.special %}*/
/*                   {{ product.price }}*/
/*                   {% else %}*/
/*                   <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/*                   {% endif %}*/
/*                 </p>*/
/*                 {% endif %}*/
/*                 */
/*                 <div class="product-list-buttons">*/
/*                   <button type="button" onclick="cart.add('{{ product.product_id }}', '{{ product.minimum }}');" class="btn btn-default cart"><i class="fa fa-shopping-bag"></i> <span>{{ button_cart }}</span></button>*/
/*                   <a class="btn btn-default quickview list-quickview" href="{{ product.quickview }}" data-toggle="tooltip" title="{{ t1o_text_quickview[lang_id] }}"><i class="fa fa-search"></i></a>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>*/
/*                 </div>*/
/*                 */
/*               </div>*/
/*               */
/*             </div>*/
/*           </div>*/
/*           */
/*         </div>*/
/*         {% endfor %}*/
/*       </div>*/
/*       <div class="row pagination-box content-padd padd-t-b-30">*/
/*         <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*         <div class="col-sm-6 text-right">{{ results }}</div>*/
/*       </div>*/
/*       {% endif %}*/
/*       {% if not categories and not products %}*/
/*       <p>{{ text_empty }}</p>*/
/*       <div class="buttons">*/
/*         <div class="pull-right"><a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a></div>*/
/*       </div>*/
/*       {% endif %}*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* {{ footer }} */
/* */
