<?php

/* oxy/template/extension/module/carousel.twig */
class __TwigTemplate_eea3b30635e637ce3f59de649100fd52cab072571519e07c022c3e6cda2fe39f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"full-width-container\">
<div id=\"carousel";
        // line 3
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "\" class=\"owl-carousel carousel-module\">
  ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["banners"]) ? $context["banners"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 5
            echo "  <div class=\"item text-center\">
    ";
            // line 6
            if ($this->getAttribute($context["banner"], "link", array())) {
                // line 7
                echo "    <a href=\"";
                echo $this->getAttribute($context["banner"], "link", array());
                echo "\"><img src=\"";
                echo $this->getAttribute($context["banner"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["banner"], "title", array());
                echo "\" class=\"img-responsive\" /></a>
    ";
            } else {
                // line 9
                echo "    <img src=\"";
                echo $this->getAttribute($context["banner"], "image", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["banner"], "title", array());
                echo "\" class=\"img-responsive\" />
    ";
            }
            // line 11
            echo "  </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "</div>
</div>
<script type=\"text/javascript\"><!--
\$('#carousel";
        // line 16
        echo (isset($context["module"]) ? $context["module"] : null);
        echo "').owlCarousel({
\titems: '";
        // line 17
        echo (isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null);
        echo "',
\titemsMobile : [479, 2],
\tautoPlay: 3000,
\tsingleItem: false,
\tscrollPerPage: false,
\tpagination: false,
\tnavigation: true,
\tnavigationText: ['<i class=\"fa fa-chevron-left fa-5x\"></i>', '<i class=\"fa fa-chevron-right fa-5x\"></i>']
});
--></script>";
    }

    public function getTemplateName()
    {
        return "oxy/template/extension/module/carousel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 17,  66 => 16,  61 => 13,  54 => 11,  46 => 9,  36 => 7,  34 => 6,  31 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }
}
/* */
/* <div class="full-width-container">*/
/* <div id="carousel{{ module }}" class="owl-carousel carousel-module">*/
/*   {% for banner in banners %}*/
/*   <div class="item text-center">*/
/*     {% if banner.link %}*/
/*     <a href="{{ banner.link }}"><img src="{{ banner.image }}" alt="{{ banner.title }}" class="img-responsive" /></a>*/
/*     {% else %}*/
/*     <img src="{{ banner.image }}" alt="{{ banner.title }}" class="img-responsive" />*/
/*     {% endif %}*/
/*   </div>*/
/*   {% endfor %}*/
/* </div>*/
/* </div>*/
/* <script type="text/javascript"><!--*/
/* $('#carousel{{ module }}').owlCarousel({*/
/* 	items: '{{ t1o_carousel_items_per_row }}',*/
/* 	itemsMobile : [479, 2],*/
/* 	autoPlay: 3000,*/
/* 	singleItem: false,*/
/* 	scrollPerPage: false,*/
/* 	pagination: false,*/
/* 	navigation: true,*/
/* 	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']*/
/* });*/
/* --></script>*/
