<?php

/* oxy/template/common/header.twig */
class __TwigTemplate_1e7473d68e5cf8c2bd7b9a79fc92f9404011c25383e96c386c559a18b25fe351 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir=\"";
        // line 3
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie8\"><![endif]-->
<!--[if IE 9 ]><html dir=\"";
        // line 4
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\" class=\"ie9\"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir=\"";
        // line 6
        echo (isset($context["direction"]) ? $context["direction"] : null);
        echo "\" lang=\"";
        echo (isset($context["lang"]) ? $context["lang"] : null);
        echo "\">
<!--<![endif]-->
<head>
<meta charset=\"UTF-8\" />
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
<title>";
        // line 12
        echo (isset($context["title"]) ? $context["title"] : null);
        echo "</title>
<base href=\"";
        // line 13
        echo (isset($context["base"]) ? $context["base"] : null);
        echo "\" />
";
        // line 14
        if ((isset($context["description"]) ? $context["description"] : null)) {
            // line 15
            echo "<meta name=\"description\" content=\"";
            echo (isset($context["description"]) ? $context["description"] : null);
            echo "\" />
";
        }
        // line 17
        if ((isset($context["keywords"]) ? $context["keywords"] : null)) {
            // line 18
            echo "<meta name=\"keywords\" content=\"";
            echo (isset($context["keywords"]) ? $context["keywords"] : null);
            echo "\" />
";
        }
        // line 20
        echo "<script src=\"catalog/view/javascript/jquery/jquery-2.1.1.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/bootstrap/js/bootstrap.min.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\" />
<link href=\"catalog/view/theme/oxy/stylesheet/stylesheet.css\" rel=\"stylesheet\">
<link href=\"catalog/view/theme/oxy/stylesheet/stylesheet-small-screens.css\" rel=\"stylesheet\">
";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["styles"]) ? $context["styles"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 27
            echo "<link href=\"";
            echo $this->getAttribute($context["style"], "href", array());
            echo "\" type=\"text/css\" rel=\"";
            echo $this->getAttribute($context["style"], "rel", array());
            echo "\" media=\"";
            echo $this->getAttribute($context["style"], "media", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "<script src=\"catalog/view/javascript/common.js\" type=\"text/javascript\"></script>
<script src=\"catalog/view/theme/oxy/js/custom-theme.js\" type=\"text/javascript\"></script>
<link href=\"catalog/view/javascript/jquery/owl-carousel/owl.carousel.css\" rel=\"stylesheet\" media=\"screen\" />
<link href=\"catalog/view/javascript/jquery/owl-carousel/owl.transitions.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js\" type=\"text/javascript\"></script>
<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/oxy/stylesheet/cloud-zoom.css\" />
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/cloud-zoom.js\"></script>

<link href=\"catalog/view/javascript/jquery/swiper/css/swiper.min.css\" rel=\"stylesheet\" media=\"screen\" />
<link href=\"catalog/view/javascript/jquery/swiper/css/opencart.css\" rel=\"stylesheet\" media=\"screen\" />
<script src=\"catalog/view/javascript/jquery/swiper/js/swiper.jquery.js\" type=\"text/javascript\"></script>

<link rel=\"stylesheet\" property=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/oxy/stylesheet/dcaccordion.css\" />\t
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/jquery.dcjqaccordion.js\"></script>
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/lazyload/lazysizes.min.js\"></script>
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/modernizr.custom.js\"></script>

";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["links"]) ? $context["links"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 47
            echo "<link href=\"";
            echo $this->getAttribute($context["link"], "href", array());
            echo "\" rel=\"";
            echo $this->getAttribute($context["link"], "rel", array());
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["scripts"]) ? $context["scripts"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["script"]) {
            // line 50
            echo "<script src=\"";
            echo $context["script"];
            echo "\" type=\"text/javascript\"></script>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['script'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
";
        // line 53
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin1-default")) {
            // line 54
            echo "<style type=\"text/css\">

/*  Body background color and pattern  */
body {
\tbackground-color: ";
            // line 58
            echo (isset($context["t1d_body_bg_color"]) ? $context["t1d_body_bg_color"] : null);
            echo ";
\t
";
            // line 60
            if (($this->getAttribute((isset($context["_SERVER"]) ? $context["_SERVER"] : null), "HTTPS", array()) && ($this->getAttribute((isset($context["_SERVER"]) ? $context["_SERVER"] : null), "HTTPS", array()) != "off"))) {
                // line 61
                echo "\t";
                $context["path_image"] = ((isset($context["config_ssl"]) ? $context["config_ssl"] : null) . "image/");
            } else {
                // line 62
                echo "  
\t";
                // line 63
                $context["path_image"] = ((isset($context["config_url"]) ? $context["config_url"] : null) . "image/");
            }
            // line 65
            echo "
";
            // line 66
            if (((isset($context["t1d_bg_image_custom"]) ? $context["t1d_bg_image_custom"] : null) != "")) {
                // line 67
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_custom"]) ? $context["t1d_bg_image_custom"] : null));
                echo "\");
";
            } elseif ((            // line 68
(isset($context["t1d_pattern_body"]) ? $context["t1d_pattern_body"] : null) != "none")) {
                // line 69
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_body"]) ? $context["t1d_pattern_body"] : null);
                echo ".png\");
";
            } else {
                // line 71
                echo "\tbackground-image: none;
";
            }
            // line 73
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_position"]) ? $context["t1d_bg_image_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 74
            echo (isset($context["t1d_bg_image_repeat"]) ? $context["t1d_bg_image_repeat"] : null);
            echo ";
\tbackground-attachment: ";
            // line 75
            echo (isset($context["t1d_bg_image_attachment"]) ? $context["t1d_bg_image_attachment"] : null);
            echo ";
}

/*  Headings color  */
h1, h2, h3, h4, h5, h6, .panel-default > .panel-heading, .product-thumb h4 a, #column-left .product-thumb h4 a, #column-right .product-thumb h4 a, .table > thead, .nav-tabs > li > a, legend, #search .form-control, .product-right-sm-info span.p-title, .product-right-sm-related .name a, #tab-review strong, #content .filters a.list-group-item, #content .product-right-sm-related.panel-default > .panel-heading {color: ";
            // line 79
            echo (isset($context["t1d_headings_color"]) ? $context["t1d_headings_color"] : null);
            echo ";}
";
            // line 80
            if (((isset($context["t1d_headings_border_status"]) ? $context["t1d_headings_border_status"] : null) == 1)) {
                // line 81
                echo "#content h1::before, #content h2::before {border-bottom: 1px solid ";
                echo (isset($context["t1d_headings_border_color"]) ? $context["t1d_headings_border_color"] : null);
                echo ";}
#content h1, #content h2 {padding-bottom: 15px;}
#content .panel-inline-title h2 {margin-bottom: 10px;}
";
            } else {
                // line 85
                echo "#content h1, #content h2 {padding-bottom: 0;}
#content .panel-inline-title h2 {padding-bottom: 10px;}
";
            }
            // line 88
            echo "
/*  Body text color  */
body, #content .tab-content p, #content .product-right-sm-info .modal-body p, ul.pf span, #content .product-buy p, .form-control, span.badge.out-of-stock, #content .flybar-top p.description, #tab-specification .table > tbody > tr > td + td {color: ";
            // line 90
            echo (isset($context["t1d_body_text_color"]) ? $context["t1d_body_text_color"] : null);
            echo ";}

/*  Light text color  */
small, .subtitle, .breadcrumb a, .breadcrumb > li + li:before, .cat-description, .cat-description-above-content, .product-thumb .product_box_brand a, ul.pf li, .product-buy ul.pp, .prev-name, .next-name, .product-right-sm-info span.p-subtitle, .product-right-sm-related .product_box_brand a, .category-slider-items .subcat a, #content .product-right-sm-info .modal-body h1 + p, #features .modal-body h1 + p, .cookie-message, .alert-success, .alert-info, .dropdown-highlight, .sitemap > ul > li > ul > li > ul > li > a, .pagination-box .col-sm-6.text-right, .category-slider-item .subcat li.all a, .rating-date, #content .tltblog .row .col-sm-9 h4 + div p, #content h3 + .row .col-sm-6.col-xs-12 .product-thumb .caption h4 + p {color: ";
            // line 93
            echo (isset($context["t1d_light_text_color"]) ? $context["t1d_light_text_color"] : null);
            echo ";}

/*  Links color  */
a, a:focus, .list-group a {color: ";
            // line 96
            echo (isset($context["t1d_other_links_color"]) ? $context["t1d_other_links_color"] : null);
            echo ";}

/*  Links hover color  */
a:hover, .breadcrumb a:hover, .dropdown-highlight:hover, .dropdown-highlight:focus, .category-slider-items .subcat li:hover a, .table .btn, .table .btn-primary, .table .btn-primary:hover, .table .btn-primary:focus, .list-group a.active, .list-group a.active:hover, .list-group a:hover, .category-list .image:hover, .category-slider-item .subcat li.all a:hover, ul.pf span.stock {color: ";
            // line 99
            echo (isset($context["t1d_links_hover_color"]) ? $context["t1d_links_hover_color"] : null);
            echo ";}

/*  Icons color  */
#menu_brands .image i, .brand-slider-items .image i, .accordion li.dcjq-parent-li > a + .dcjq-icon, .panel-category-dropdown li a i {color: ";
            // line 102
            echo (isset($context["t1d_icons_color"]) ? $context["t1d_icons_color"] : null);
            echo ";}

/*  Icons hover color  */
#search .input-group-addon .btn:hover, .theme-modal .modal-body .close, .alert-success i, .alert-success .close, .alert-info i, .alert-info .close, .accordion li.dcjq-parent-li > a + .dcjq-icon:hover, .theme-modal-popup .modal-body .close {color: ";
            // line 105
            echo (isset($context["t1d_icons_hover_color"]) ? $context["t1d_icons_hover_color"] : null);
            echo ";}
.mfp-close {color: ";
            // line 106
            echo (isset($context["t1d_icons_hover_color"]) ? $context["t1d_icons_hover_color"] : null);
            echo "!important;}
#toTop:hover, #bar, .swiper-pagination-bullet-active {background-color: ";
            // line 107
            echo (isset($context["t1d_icons_hover_color"]) ? $context["t1d_icons_hover_color"] : null);
            echo "!important;}

/*  Wrapper  */
";
            // line 110
            if (((isset($context["t1d_wrapper_frame_bg_color_status"]) ? $context["t1d_wrapper_frame_bg_color_status"] : null) == "1")) {
                // line 111
                echo ".wrapper.framed, .wrapper.full-width {
\tbackground-color: ";
                // line 112
                echo (isset($context["t1d_wrapper_frame_bg_color"]) ? $context["t1d_wrapper_frame_bg_color"] : null);
                echo ";
}
";
            }
            // line 115
            if (((isset($context["t1d_wrapper_shadow"]) ? $context["t1d_wrapper_shadow"] : null) == "1")) {
                // line 116
                echo ".wrapper.framed {box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);}
";
            }
            // line 118
            if (((isset($context["t1d_boxes_shadow"]) ? $context["t1d_boxes_shadow"] : null) == "1")) {
                // line 119
                echo ".wrapper.boxed #column-left .panel, .wrapper.boxed #column-right .panel, .wrapper.boxed #content {box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);}
";
            }
            // line 121
            echo "
/*  Content Column  */
#content, #content .panel, .category-list + .panel-default > .panel-heading, .modal-content, .alert-success, .alert-info, .cookie-message, .custom_box, .theme-custom-products .panel-inline-items, .theme-lookbook-bg, .theme-banner-bg, .theme-gallery-bg, .theme-store-tv-bg, .highly-recommended-module-content, #tab-review .rating-text, #column-left .product-thumb .image img, #column-right .product-thumb .image img, #livesearch_search_results {
\tbackground-color: ";
            // line 124
            echo (isset($context["t1d_content_column_bg_color"]) ? $context["t1d_content_column_bg_color"] : null);
            echo ";
}
.tab-content .table-bordered, .tab-content .table-bordered > thead > tr > th, .tab-content .table-bordered > tbody > tr > th, .tab-content .table-bordered > tfoot > tr > th, .tab-content .table-bordered > thead > tr > td, .tab-content .table-bordered > tbody > tr > td, .tab-content .table-bordered > tfoot > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
\tbackground-color: ";
            // line 127
            echo (isset($context["t1d_content_column_bg_color"]) ? $context["t1d_content_column_bg_color"] : null);
            echo ";
\tborder-color: ";
            // line 128
            echo (isset($context["t1d_content_column_hli_bg_color"]) ? $context["t1d_content_column_hli_bg_color"] : null);
            echo ";
}
.well, .table-bordered, .table-bordered > thead > tr > th, .table-bordered > thead > tr > td, .table-bordered img {
\tborder: 1px solid ";
            // line 131
            echo (isset($context["t1d_content_column_bg_color"]) ? $context["t1d_content_column_bg_color"] : null);
            echo ";
\tbackground-color: ";
            // line 132
            echo (isset($context["t1d_content_column_hli_bg_color"]) ? $context["t1d_content_column_hli_bg_color"] : null);
            echo ";
}
";
            // line 134
            if (((isset($context["t1d_content_column_hli_buy_column"]) ? $context["t1d_content_column_hli_buy_column"] : null) == 1)) {
                // line 135
                echo ".product-buy .product-buy-wrapper {
\tbackground-color: ";
                // line 136
                echo (isset($context["t1d_content_column_hli_bg_color"]) ? $context["t1d_content_column_hli_bg_color"] : null);
                echo ";
\tpadding: 30px;
}
";
            }
            // line 140
            echo ".nav-tabs > li > a, .nav-tabs > li > a:hover, .sitemap > ul > li > a, .manufacturer-list h3, .search-panel, .product-page #product-tabs .tab-content, .contact-details-wrapper, #tab-review .table-bordered > tbody > tr > td {
\tbackground-color: ";
            // line 141
            echo (isset($context["t1d_content_column_hli_bg_color"]) ? $context["t1d_content_column_hli_bg_color"] : null);
            echo ";
}

#livesearch_search_results li, .modal-footer, .list-group a, .list-group a.active, .list-group a.active:hover, .list-group a:hover, a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus {
    border-top: 1px solid ";
            // line 145
            echo (isset($context["t1d_content_column_separator_color"]) ? $context["t1d_content_column_separator_color"] : null);
            echo ";\t
}\t
.modal-header {
    border-bottom: 1px solid ";
            // line 148
            echo (isset($context["t1d_content_column_separator_color"]) ? $context["t1d_content_column_separator_color"] : null);
            echo ";\t
}
.contact-map, .img-thumbnail-theme {
    border: 1px solid ";
            // line 151
            echo (isset($context["t1d_content_column_separator_color"]) ? $context["t1d_content_column_separator_color"] : null);
            echo ";\t
}
hr, #search .input-group-addon {
\tborder-color: ";
            // line 154
            echo (isset($context["t1d_content_column_separator_color"]) ? $context["t1d_content_column_separator_color"] : null);
            echo ";
}

/*  Left-Right Column Boxes  */
";
            // line 158
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "0")) {
                // line 159
                echo ".wrapper.boxed #column-left .panel-default > .panel-heading {
\tpadding: 25px 0 0;
}
.wrapper.boxed #column-left .panel-default > .list-group > ul > li > a, .wrapper.boxed #column-left .panel-default .accordion li.dcjq-parent-li ul li a, .wrapper.boxed #column-left .filters div.list-group-item {
\tpadding: 5px 0;
}
.wrapper.boxed #column-left .newsletter-block {
\tpadding: 25px 0;
}
.wrapper.boxed #column-left .product-items-0 > div {
\tpadding: 20px 0 0;
}
.wrapper.boxed #column-left .product-thumb {
\tpadding: 0;
}
.wrapper.boxed #column-left .product-items-1 .product-layout-slider {
\tpadding: 30px 0 20px;
}
.wrapper.boxed #column-left .product-items.owl-carousel .owl-controls .owl-buttons .owl-next {
\tright: 0!important;
}
.wrapper.boxed #column-left .product-items.owl-carousel .owl-controls .owl-buttons .owl-prev {
\tright: 35px!important;
}
";
            }
            // line 184
            echo "#column-left .panel, #column-right .panel {
\tmargin-bottom: ";
            // line 185
            echo (isset($context["t1d_sidebar_margin_bottom"]) ? $context["t1d_sidebar_margin_bottom"] : null);
            echo "px;
}

/*  Left Column Box  */
";
            // line 189
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
                // line 190
                echo "#column-left .panel-default > .panel-heading {
\tbackground-color: ";
                // line 191
                echo (isset($context["t1d_left_column_head_title_bg_color"]) ? $context["t1d_left_column_head_title_bg_color"] : null);
                echo ";
}
#column-left .panel, #column-left .owl-carousel {
\tbackground-color: ";
                // line 194
                echo (isset($context["t1d_left_column_box_bg_color"]) ? $context["t1d_left_column_box_bg_color"] : null);
                echo ";
}
";
            }
            // line 197
            echo "#column-left .panel-default > .panel-heading, #column-left .panel-default > .panel-heading h2 {color: ";
            echo (isset($context["t1d_left_column_head_title_color"]) ? $context["t1d_left_column_head_title_color"] : null);
            echo ";}
#column-left a {color: ";
            // line 198
            echo (isset($context["t1d_left_column_box_links_color"]) ? $context["t1d_left_column_box_links_color"] : null);
            echo ";}
#column-left a:hover {color: ";
            // line 199
            echo (isset($context["t1d_left_column_box_links_color_hover"]) ? $context["t1d_left_column_box_links_color_hover"] : null);
            echo ";}
#column-left {color: ";
            // line 200
            echo (isset($context["t1d_left_column_box_text_color"]) ? $context["t1d_left_column_box_text_color"] : null);
            echo ";}
";
            // line 201
            if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "1")) {
                // line 202
                echo "#column-left .panel, #column-left .owl-carousel.carousel-module, #column-left .owl-carousel.banner-module {
\tborder-bottom: 1px solid ";
                // line 203
                echo (isset($context["t1d_left_column_box_separator_color"]) ? $context["t1d_left_column_box_separator_color"] : null);
                echo "!important;
}
";
            }
            // line 206
            echo "
/*  Right Column Box  */
";
            // line 208
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
                // line 209
                echo "#column-right .panel-default > .panel-heading {
\tbackground-color: ";
                // line 210
                echo (isset($context["t1d_right_column_head_title_bg_color"]) ? $context["t1d_right_column_head_title_bg_color"] : null);
                echo ";
}
#column-right .panel, #column-right .owl-carousel {
\tbackground-color: ";
                // line 213
                echo (isset($context["t1d_right_column_box_bg_color"]) ? $context["t1d_right_column_box_bg_color"] : null);
                echo ";
}
";
            }
            // line 216
            echo "#column-right .panel-default > .panel-heading, #column-right .panel-default > .panel-heading h2 {color: ";
            echo (isset($context["t1d_right_column_head_title_color"]) ? $context["t1d_right_column_head_title_color"] : null);
            echo ";}
#column-right a {color: ";
            // line 217
            echo (isset($context["t1d_right_column_box_links_color"]) ? $context["t1d_right_column_box_links_color"] : null);
            echo ";}
#column-right a:hover {color: ";
            // line 218
            echo (isset($context["t1d_right_column_box_links_color_hover"]) ? $context["t1d_right_column_box_links_color_hover"] : null);
            echo ";}
#column-right {color: ";
            // line 219
            echo (isset($context["t1d_right_column_box_text_color"]) ? $context["t1d_right_column_box_text_color"] : null);
            echo ";}
";
            // line 220
            if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "1")) {
                // line 221
                echo "#column-right .panel, #column-right .owl-carousel.carousel-module, #column-right .owl-carousel.banner-module {border-bottom: 1px solid ";
                echo (isset($context["t1d_right_column_box_separator_color"]) ? $context["t1d_right_column_box_separator_color"] : null);
                echo "!important;}
";
            }
            // line 223
            echo "
/*  Category Box   */
";
            // line 225
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
                // line 226
                echo "#column-left .panel-default.panel-category > .panel-heading, #column-right .panel-default.panel-category > .panel-heading {
\tbackground-color: ";
                // line 227
                echo (isset($context["t1d_category_box_head_title_bg_color"]) ? $context["t1d_category_box_head_title_bg_color"] : null);
                echo ";
}
";
            }
            // line 230
            echo "#column-left .panel-default.panel-category, #column-right .panel-default.panel-category {
\t";
            // line 231
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
                // line 232
                echo "\tbackground-color: ";
                echo (isset($context["t1d_category_box_box_bg_color"]) ? $context["t1d_category_box_box_bg_color"] : null);
                echo ";
\t";
            }
            // line 234
            echo "\t";
            if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "1")) {
                // line 235
                echo "    border-bottom: 1px solid ";
                echo (isset($context["t1d_category_box_box_separator_color"]) ? $context["t1d_category_box_box_separator_color"] : null);
                echo "!important;
\t";
            }
            // line 237
            echo "}
";
            // line 238
            if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "1")) {
                // line 239
                echo ".panel-default.panel-category .accordion li.dcjq-parent-li > a.list-group-item {
\tborder-bottom: 1px solid ";
                // line 240
                echo (isset($context["t1d_category_box_box_separator_color"]) ? $context["t1d_category_box_box_separator_color"] : null);
                echo "!important;
}
";
            }
            // line 243
            echo "#column-left .panel-default.panel-category > .panel-heading h2, #column-right .panel-default.panel-category > .panel-heading h2 {
    color: ";
            // line 244
            echo (isset($context["t1d_category_box_head_title_color"]) ? $context["t1d_category_box_head_title_color"] : null);
            echo ";
}
#column-left .panel-default.panel-category a, #column-right .panel-default.panel-category a {color: ";
            // line 246
            echo (isset($context["t1d_category_box_box_links_color"]) ? $context["t1d_category_box_box_links_color"] : null);
            echo ";}
#column-left .panel-default.panel-category a:hover, #column-right .panel-default.panel-category a:hover, #column-left .panel-default.panel-category .cat-mod-child a:hover, #column-right .panel-default.panel-category .cat-mod-child a:hover {color: ";
            // line 247
            echo (isset($context["t1d_category_box_box_links_color_hover"]) ? $context["t1d_category_box_box_links_color_hover"] : null);
            echo ";}
#column-left .panel-default.panel-category .cat-mod-child a, #column-right .panel-default.panel-category .cat-mod-child a, #column-left .accordion li ul li a.list-group-item {color: ";
            // line 248
            echo (isset($context["t1d_category_box_box_subcat_color"]) ? $context["t1d_category_box_box_subcat_color"] : null);
            echo ";}

/*  Filter Box Content  */
";
            // line 251
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
                // line 252
                echo "#column-left .panel-default.filters > .panel-heading, #column-right .panel-default.filters > .panel-heading {
\tbackground-color: ";
                // line 253
                echo (isset($context["t1d_filter_box_head_title_bg_color"]) ? $context["t1d_filter_box_head_title_bg_color"] : null);
                echo ";
}
";
            }
            // line 256
            echo "#column-left .panel-default.filters, #column-right .panel-default.filters, #content .panel-default.filters {
\t";
            // line 257
            if (((isset($context["t1d_sidebar_bg_color_status"]) ? $context["t1d_sidebar_bg_color_status"] : null) == "1")) {
                // line 258
                echo "\tbackground-color: ";
                echo (isset($context["t1d_filter_box_box_bg_color"]) ? $context["t1d_filter_box_box_bg_color"] : null);
                echo ";
\t";
            }
            // line 260
            echo "\t";
            if (((isset($context["t1d_sidebar_separator_status"]) ? $context["t1d_sidebar_separator_status"] : null) == "1")) {
                // line 261
                echo "    border-bottom: 1px solid ";
                echo (isset($context["t1d_filter_box_box_separator_color"]) ? $context["t1d_filter_box_box_separator_color"] : null);
                echo "!important;
\t";
            }
            // line 263
            echo "}
#column-left .panel-default.filters > .panel-heading h2, #column-right .panel-default.filters > .panel-heading h2 {
    color: ";
            // line 265
            echo (isset($context["t1d_filter_box_head_title_color"]) ? $context["t1d_filter_box_head_title_color"] : null);
            echo ";
}
#column-left .panel-default.filters a, #column-right .panel-default.filters a, #content .panel-default.filters a {color: ";
            // line 267
            echo (isset($context["t1d_filter_box_box_filter_title_color"]) ? $context["t1d_filter_box_box_filter_title_color"] : null);
            echo ";}
#column-left .panel-default.filters label, #column-right .panel-default.filters label, #content .panel-default.filters label {color: ";
            // line 268
            echo (isset($context["t1d_filter_box_box_filter_name_color"]) ? $context["t1d_filter_box_box_filter_name_color"] : null);
            echo ";}
#column-left .panel-default.filters label:hover, #column-right .panel-default.filters label:hover, #content .panel-default.filters label:hover {color: ";
            // line 269
            echo (isset($context["t1d_filter_box_box_filter_name_color_hover"]) ? $context["t1d_filter_box_box_filter_name_color_hover"] : null);
            echo ";}
#content .panel-default.filters {
\tborder-bottom: 1px solid ";
            // line 271
            echo (isset($context["t1d_filter_box_box_separator_color"]) ? $context["t1d_filter_box_box_separator_color"] : null);
            echo ";
}


/*  HEADER  */
header {
";
            // line 277
            if (((isset($context["t1d_top_area_status"]) ? $context["t1d_top_area_status"] : null) == 1)) {
                // line 278
                echo "\tbackground-color: ";
                echo (isset($context["t1d_top_area_bg_color"]) ? $context["t1d_top_area_bg_color"] : null);
                echo ";
";
            }
            // line 280
            if (((isset($context["t1d_bg_image_ta_custom"]) ? $context["t1d_bg_image_ta_custom"] : null) != "")) {
                // line 281
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_ta_custom"]) ? $context["t1d_bg_image_ta_custom"] : null));
                echo "\");
";
            } elseif ((            // line 282
(isset($context["t1d_pattern_k_ta"]) ? $context["t1d_pattern_k_ta"] : null) != "none")) {
                // line 283
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_ta"]) ? $context["t1d_pattern_k_ta"] : null);
                echo ".png\");
";
            } else {
                // line 285
                echo "\tbackground-image: none;
";
            }
            // line 287
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_ta_position"]) ? $context["t1d_bg_image_ta_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 288
            echo (isset($context["t1d_bg_image_ta_repeat"]) ? $context["t1d_bg_image_ta_repeat"] : null);
            echo ";
\tbackground-attachment: ";
            // line 289
            echo (isset($context["t1d_bg_image_ta_attachment"]) ? $context["t1d_bg_image_ta_attachment"] : null);
            echo ";
}
.is-sticky #menu {
\tbackground-color: ";
            // line 292
            echo (isset($context["t1d_top_area_mini_bg_color"]) ? $context["t1d_top_area_mini_bg_color"] : null);
            echo ";
}
.button-i, #cart.buttons-header #cart-tt i, .header-custom-box:hover i {color: ";
            // line 294
            echo (isset($context["t1d_top_area_icons_color"]) ? $context["t1d_top_area_icons_color"] : null);
            echo ";}
#cart #cart-tt #cart-total {background-color: ";
            // line 295
            echo (isset($context["t1d_top_area_icons_color_hover"]) ? $context["t1d_top_area_icons_color_hover"] : null);
            echo ";}
.buttons-header:hover .button-i {color: ";
            // line 296
            echo (isset($context["t1d_top_area_icons_color_hover"]) ? $context["t1d_top_area_icons_color_hover"] : null);
            echo ";}
";
            // line 297
            if (((isset($context["t1d_top_area_icons_separator_status"]) ? $context["t1d_top_area_icons_separator_status"] : null) == 1)) {
                // line 298
                echo ".buttons-header, .menu-mobile-block {border-left: 1px solid ";
                echo (isset($context["t1d_top_area_icons_separator_color"]) ? $context["t1d_top_area_icons_separator_color"] : null);
                echo ";}
";
            }
            // line 300
            echo "#cart.buttons-header .button-i {color: ";
            echo (isset($context["t1d_top_area_icons_separator_color"]) ? $context["t1d_top_area_icons_separator_color"] : null);
            echo ";}

#search .form-control, #search .input-group-addon, #search .input-group-addon .btn {background-color: ";
            // line 302
            echo (isset($context["t1d_top_area_search_bg_color"]) ? $context["t1d_top_area_search_bg_color"] : null);
            echo ";}
#search.header-search-bar .input-lg, .modal-body #search {border: 1px solid ";
            // line 303
            echo (isset($context["t1d_top_area_search_border_color"]) ? $context["t1d_top_area_search_border_color"] : null);
            echo ";}
#search .form-control::-moz-placeholder {color: ";
            // line 304
            echo (isset($context["t1d_top_area_search_color"]) ? $context["t1d_top_area_search_color"] : null);
            echo ";}
#search .form-control {color: ";
            // line 305
            echo (isset($context["t1d_top_area_search_color_active"]) ? $context["t1d_top_area_search_color_active"] : null);
            echo ";}
#search .btn .button-i i, #search .input-group-addon .btn i {color: ";
            // line 306
            echo (isset($context["t1d_top_area_search_icon_color"]) ? $context["t1d_top_area_search_icon_color"] : null);
            echo ";}

/*  Top News */
#top-news-wrapper  {background-color: ";
            // line 309
            echo (isset($context["t1o_news_bg_color"]) ? $context["t1o_news_bg_color"] : null);
            echo ";}
#top-news-content li a  {color: ";
            // line 310
            echo (isset($context["t1o_news_bg_color"]) ? $context["t1o_news_bg_color"] : null);
            echo ";}
#top-news-content i {color: ";
            // line 311
            echo (isset($context["t1o_news_icons_color"]) ? $context["t1o_news_icons_color"] : null);
            echo ";}
#top-news-content span#top-news {color: ";
            // line 312
            echo (isset($context["t1o_news_word_color"]) ? $context["t1o_news_word_color"] : null);
            echo ";}
#top-news-content #news a {color: ";
            // line 313
            echo (isset($context["t1o_news_color"]) ? $context["t1o_news_color"] : null);
            echo ";}
#top-news-content #news a:hover {color: ";
            // line 314
            echo (isset($context["t1o_news_hover_color"]) ? $context["t1o_news_hover_color"] : null);
            echo ";}

/*  Top Bar  */
";
            // line 317
            if (((isset($context["t1d_top_area_tb_bg_status"]) ? $context["t1d_top_area_tb_bg_status"] : null) == 1)) {
                // line 318
                echo "#top {
\tbackground-color: ";
                // line 319
                echo (isset($context["t1d_top_area_tb_bg_color"]) ? $context["t1d_top_area_tb_bg_color"] : null);
                echo ";
}
";
            }
            // line 322
            echo "#top, .top-links li {
\tcolor: ";
            // line 323
            echo (isset($context["t1d_top_area_tb_text_color"]) ? $context["t1d_top_area_tb_text_color"] : null);
            echo ";
}
#top a, #top .btn-link, .top-links a, .top-links .fa-angle-down {
\tcolor: ";
            // line 326
            echo (isset($context["t1d_top_area_tb_link_color"]) ? $context["t1d_top_area_tb_link_color"] : null);
            echo ";
}
#top a:hover, #top .btn-link:hover, #top .btn-link:hover i, .top-links a:hover, .top-links a:hover i {
\tcolor: ";
            // line 329
            echo (isset($context["t1d_top_area_tb_link_color_hover"]) ? $context["t1d_top_area_tb_link_color_hover"] : null);
            echo ";
}
.top-links i {
\tcolor: ";
            // line 332
            echo (isset($context["t1d_top_area_tb_icons_color"]) ? $context["t1d_top_area_tb_icons_color"] : null);
            echo ";
} 
";
            // line 334
            if (((isset($context["t1d_top_area_tb_bottom_border_status"]) ? $context["t1d_top_area_tb_bottom_border_status"] : null) == 1)) {
                // line 335
                echo "#top {
\tborder-bottom: 1px solid ";
                // line 336
                echo (isset($context["t1d_top_area_tb_bottom_border_color"]) ? $context["t1d_top_area_tb_bottom_border_color"] : null);
                echo ";
}
";
            }
            // line 339
            echo "

/*  MAIN MENU */

/*  Main Menu Bar  */
#menu {
";
            // line 345
            if (((isset($context["t1d_mm_bg_color_status"]) ? $context["t1d_mm_bg_color_status"] : null) == 1)) {
                // line 346
                echo "\tbackground-color: ";
                echo (isset($context["t1d_mm_bg_color"]) ? $context["t1d_mm_bg_color"] : null);
                echo ";
";
            }
            // line 348
            if (((isset($context["t1d_mm_border_top_status"]) ? $context["t1d_mm_border_top_status"] : null) == 1)) {
                echo "\t
    border-top: ";
                // line 349
                echo (isset($context["t1d_mm_border_top_size"]) ? $context["t1d_mm_border_top_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_mm_border_top_color"]) ? $context["t1d_mm_border_top_color"] : null);
                echo ";\t
";
            }
            // line 351
            if (((isset($context["t1d_mm_border_bottom_status"]) ? $context["t1d_mm_border_bottom_status"] : null) == 1)) {
                echo "\t
    border-bottom: ";
                // line 352
                echo (isset($context["t1d_mm_border_bottom_size"]) ? $context["t1d_mm_border_bottom_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_mm_border_bottom_color"]) ? $context["t1d_mm_border_bottom_color"] : null);
                echo ";\t
";
            }
            // line 354
            echo "
";
            // line 355
            if ((((isset($context["t1d_bg_image_mm_custom"]) ? $context["t1d_bg_image_mm_custom"] : null) != "") && ((isset($context["t1d_mm_bg_color_status"]) ? $context["t1d_mm_bg_color_status"] : null) == 1))) {
                // line 356
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_mm_custom"]) ? $context["t1d_bg_image_mm_custom"] : null));
                echo "\");
";
            } elseif (((            // line 357
(isset($context["t1d_pattern_k_mm"]) ? $context["t1d_pattern_k_mm"] : null) != "none") && ((isset($context["t1d_mm_bg_color_status"]) ? $context["t1d_mm_bg_color_status"] : null) == 1))) {
                // line 358
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_mm"]) ? $context["t1d_pattern_k_mm"] : null);
                echo ".png\");
";
            } else {
                // line 360
                echo "\tbackground-image: none;
";
            }
            // line 362
            echo "\tbackground-repeat: ";
            echo (isset($context["t1d_bg_image_mm_repeat"]) ? $context["t1d_bg_image_mm_repeat"] : null);
            echo ";
}
";
            // line 364
            if (((isset($context["t1d_mm_separator_status"]) ? $context["t1d_mm_separator_status"] : null) == 1)) {
                // line 365
                echo "#homepage, .menu_oc, #menu_ver, #menu_ver_2, #menu_hor, #menu_inline, #menu_brands, .menu_links, .menu_custom_menu, .menu_custom_block, #menu .flexMenu-viewMore {
\tborder-left: ";
                // line 366
                echo (isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_mm_separator_color"]) ? $context["t1d_mm_separator_color"] : null);
                echo ";
}
.navbar .main-menu > li:last-child {
\tborder-right: ";
                // line 369
                echo (isset($context["t1d_mm_separator_size"]) ? $context["t1d_mm_separator_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_mm_separator_color"]) ? $context["t1d_mm_separator_color"] : null);
                echo ";
}
";
            }
            // line 372
            if (((isset($context["t1d_mm_shadow_status"]) ? $context["t1d_mm_shadow_status"] : null) == 1)) {
                // line 373
                echo "#menu {
    box-shadow: 0 10px 15px -10px rgba(0, 0, 0, 0.15);
}
";
            }
            // line 377
            if (((isset($context["t1d_mm_sort_down_icon_status"]) ? $context["t1d_mm_sort_down_icon_status"] : null) == 0)) {
                // line 378
                echo "#menu .fa-sort-down {
\tdisplay: none;
}
";
            }
            // line 382
            echo "
/*  Home Page Link  */
";
            // line 384
            if (((isset($context["t1d_mm1_bg_color_status"]) ? $context["t1d_mm1_bg_color_status"] : null) == 1)) {
                echo " 
#menu #homepage {
\tbackground-color: ";
                // line 386
                echo (isset($context["t1d_mm1_bg_color"]) ? $context["t1d_mm1_bg_color"] : null);
                echo ";
}
#menu #homepage:hover {
\tbackground-color: ";
                // line 389
                echo (isset($context["t1d_mm1_bg_hover_color"]) ? $context["t1d_mm1_bg_hover_color"] : null);
                echo ";
}\t
";
            }
            // line 392
            echo "#menu #homepage a {
\tcolor: ";
            // line 393
            echo (isset($context["t1d_mm1_link_color"]) ? $context["t1d_mm1_link_color"] : null);
            echo ";
}\t
#menu #homepage:hover a {
\tcolor: ";
            // line 396
            echo (isset($context["t1d_mm1_link_hover_color"]) ? $context["t1d_mm1_link_hover_color"] : null);
            echo ";
}

/*  Categories  */
";
            // line 400
            if (((isset($context["t1d_mm2_bg_color_status"]) ? $context["t1d_mm2_bg_color_status"] : null) == 1)) {
                // line 401
                echo ".menu_oc, #menu_ver, #menu_ver_2, #menu_hor, #menu_inline  {
\tbackground-color: ";
                // line 402
                echo (isset($context["t1d_mm2_bg_color"]) ? $context["t1d_mm2_bg_color"] : null);
                echo ";
}
.menu_oc:hover, #menu_ver:hover, #menu_ver_2:hover, #menu_hor:hover, #menu_inline:hover  {
\tbackground-color: ";
                // line 405
                echo (isset($context["t1d_mm2_bg_hover_color"]) ? $context["t1d_mm2_bg_hover_color"] : null);
                echo ";
}
";
            }
            // line 407
            echo " 
.menu_oc > a, #menu_ver > a, #menu_ver_2 > a, #menu_hor > a, #menu_inline > a,
#menu .flexMenu-popup.dropdown-menus .menu_oc > a, #menu .flexMenu-popup.dropdown-menus #menu_ver > a, #menu .flexMenu-popup.dropdown-menus #menu_ver_2 > a, #menu .flexMenu-popup.dropdown-menus #menu_hor > a, #menu .flexMenu-popup.dropdown-menus #menu_inline > a {
\tcolor: ";
            // line 410
            echo (isset($context["t1d_mm2_link_color"]) ? $context["t1d_mm2_link_color"] : null);
            echo ";
}\t
.menu_oc:hover > a, #menu_ver:hover > a, #menu_ver_2:hover > a, #menu_hor:hover > a, #menu_inline:hover > a,
#menu .flexMenu-popup.dropdown-menus .menu_oc:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_ver:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_ver_2:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_hor:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_inline:hover > a {
\tcolor: ";
            // line 414
            echo (isset($context["t1d_mm2_link_hover_color"]) ? $context["t1d_mm2_link_hover_color"] : null);
            echo ";
}
";
            // line 416
            if (((isset($context["t1o_menu_categories_home_visibility"]) ? $context["t1o_menu_categories_home_visibility"] : null) == 1)) {
                // line 417
                echo ".common-home #menu #menu_ver_2 > a + .dropdown-menus {opacity: 1; visibility: visible;}
.common-home .is-sticky #menu #menu_ver_2 > a + .dropdown-menus {opacity: 0; visibility: hidden;}
";
            }
            // line 420
            echo "
/*  Brands  */
";
            // line 422
            if (((isset($context["t1d_mm3_bg_color_status"]) ? $context["t1d_mm3_bg_color_status"] : null) == 1)) {
                echo " 
#menu_brands {
\tbackground-color: ";
                // line 424
                echo (isset($context["t1d_mm3_bg_color"]) ? $context["t1d_mm3_bg_color"] : null);
                echo ";
}
#menu_brands:hover {
\tbackground-color: ";
                // line 427
                echo (isset($context["t1d_mm3_bg_hover_color"]) ? $context["t1d_mm3_bg_hover_color"] : null);
                echo ";
}
";
            }
            // line 430
            echo "#menu_brands > a, #menu .flexMenu-popup.dropdown-menus #menu_brands > a {
\tcolor: ";
            // line 431
            echo (isset($context["t1d_mm3_link_color"]) ? $context["t1d_mm3_link_color"] : null);
            echo ";
}\t
#menu_brands:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_brands:hover > a {
\tcolor: ";
            // line 434
            echo (isset($context["t1d_mm3_link_hover_color"]) ? $context["t1d_mm3_link_hover_color"] : null);
            echo ";
}

/*  Custom Blocks  */
";
            // line 438
            if (((isset($context["t1d_mm6_bg_color_status"]) ? $context["t1d_mm6_bg_color_status"] : null) == 1)) {
                echo " 
.menu_custom_block {
\tbackground-color: ";
                // line 440
                echo (isset($context["t1d_mm6_bg_color"]) ? $context["t1d_mm6_bg_color"] : null);
                echo ";
}
.menu_custom_block:hover {
\tbackground-color: ";
                // line 443
                echo (isset($context["t1d_mm6_bg_hover_color"]) ? $context["t1d_mm6_bg_hover_color"] : null);
                echo ";
}
";
            }
            // line 446
            echo ".menu_custom_block > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_block > a {
\tcolor: ";
            // line 447
            echo (isset($context["t1d_mm6_link_color"]) ? $context["t1d_mm6_link_color"] : null);
            echo ";
}\t
.menu_custom_block:hover > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_block:hover > a {
\tcolor: ";
            // line 450
            echo (isset($context["t1d_mm6_link_hover_color"]) ? $context["t1d_mm6_link_hover_color"] : null);
            echo ";
}

/*  Custom Dropdown Menus  */
";
            // line 454
            if (((isset($context["t1d_mm5_bg_color_status"]) ? $context["t1d_mm5_bg_color_status"] : null) == 1)) {
                echo " 
.menu_custom_menu {
\tbackground-color: ";
                // line 456
                echo (isset($context["t1d_mm5_bg_color"]) ? $context["t1d_mm5_bg_color"] : null);
                echo ";
}
.menu_custom_menu:hover {
\tbackground-color: ";
                // line 459
                echo (isset($context["t1d_mm5_bg_hover_color"]) ? $context["t1d_mm5_bg_hover_color"] : null);
                echo ";
}
";
            }
            // line 461
            echo " 
.menu_custom_menu > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_menu > a {
\tcolor: ";
            // line 463
            echo (isset($context["t1d_mm5_link_color"]) ? $context["t1d_mm5_link_color"] : null);
            echo ";
}\t
.menu_custom_menu:hover > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_menu:hover > a {
\tcolor: ";
            // line 466
            echo (isset($context["t1d_mm5_link_hover_color"]) ? $context["t1d_mm5_link_hover_color"] : null);
            echo ";
}

/*  Custom Links  */
";
            // line 470
            if (((isset($context["t1d_mm4_bg_color_status"]) ? $context["t1d_mm4_bg_color_status"] : null) == 1)) {
                echo " 
.menu_links {
\tbackground-color: ";
                // line 472
                echo (isset($context["t1d_mm4_bg_color"]) ? $context["t1d_mm4_bg_color"] : null);
                echo ";
}\t
.menu_links:hover {
\tbackground-color: ";
                // line 475
                echo (isset($context["t1d_mm4_bg_hover_color"]) ? $context["t1d_mm4_bg_hover_color"] : null);
                echo ";
}\t
";
            }
            // line 478
            echo ".menu_links a, .flexMenu-viewMore a > i, #menu .flexMenu-popup.dropdown-menus .menu_links a {
\tcolor: ";
            // line 479
            echo (isset($context["t1d_mm4_link_color"]) ? $context["t1d_mm4_link_color"] : null);
            echo ";
}\t
.menu_links:hover a, .flexMenu-viewMore:hover a > i, #menu .flexMenu-popup.dropdown-menus .menu_links:hover a {
\tcolor: ";
            // line 482
            echo (isset($context["t1d_mm4_link_hover_color"]) ? $context["t1d_mm4_link_hover_color"] : null);
            echo ";
}

/*  Flex Menu  */
#menu .flexMenu-viewMore > a + .dropdown-menus {
    background-color: ";
            // line 487
            echo (isset($context["t1d_mm_bg_color"]) ? $context["t1d_mm_bg_color"] : null);
            echo ";
}

/*  Sub-Menu  */
#menu .dropdown-menu, #menu .dropdown-menus {
\tbackground-color: ";
            // line 492
            echo (isset($context["t1d_mm_sub_bg_color"]) ? $context["t1d_mm_sub_bg_color"] : null);
            echo ";
\tcolor: ";
            // line 493
            echo (isset($context["t1d_mm_sub_text_color"]) ? $context["t1d_mm_sub_text_color"] : null);
            echo ";
}
#menu .dropdown-highlight {
\tbackground-color: ";
            // line 496
            echo (isset($context["t1d_mm_sub_titles_bg_color"]) ? $context["t1d_mm_sub_titles_bg_color"] : null);
            echo ";
}
#menu .dropdown-menus a, #menu_brands .brand-item .name {
\tcolor: ";
            // line 499
            echo (isset($context["t1d_mm_sub_link_color"]) ? $context["t1d_mm_sub_link_color"] : null);
            echo ";
}\t
#menu .dropdown-menus a:hover, #menu .dropdown-menus li a:hover, #menu #menu_hor .dropdown-menus li.sub-cat:hover > a, #menu #menu_hor .dropdown-menus li.main-cat > a:hover, #menu_brands .brand-item:hover .name, #menu #menu_inline .dropdown-menus li .dropdown-menus a:hover, #menu #menu_ver .dropdown-menus .dropdown-menus a:hover, #menu #menu_ver_2 .dropdown-menus .dropdown-menus a:hover, #menu .menu_oc .dropdown-menus .dropdown-menus a:hover {
\tcolor: ";
            // line 502
            echo (isset($context["t1d_mm_sub_link_hover_color"]) ? $context["t1d_mm_sub_link_hover_color"] : null);
            echo ";
}
#menu #menu_hor .dropdown-menus li.sub-cat > a, #menu #menu_inline .dropdown-menus li .dropdown-menus a, #menu #menu_ver .dropdown-menus .dropdown-menus a, #menu #menu_ver_2 .dropdown-menus .dropdown-menus a, #menu .menu_oc .dropdown-menus .dropdown-menus a {
\tcolor: ";
            // line 505
            echo (isset($context["t1d_mm_sub_subcategory_color"]) ? $context["t1d_mm_sub_subcategory_color"] : null);
            echo ";
}
#menu .dropdown-menu, #menu .dropdown-menus {
";
            // line 508
            if (((isset($context["t1d_mm_sub_box_shadow"]) ? $context["t1d_mm_sub_box_shadow"] : null) == 1)) {
                // line 509
                echo "\tbox-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);
";
            } else {
                // line 511
                echo "\tbox-shadow: none;
";
            }
            // line 513
            echo "}
.navbar {text-align: ";
            // line 514
            echo (isset($context["t1o_menu_align"]) ? $context["t1o_menu_align"] : null);
            echo ";}

";
            // line 516
            if (((isset($context["t1d_mm_sub_border_status"]) ? $context["t1d_mm_sub_border_status"] : null) == 1)) {
                // line 517
                echo "#menu #menu_hor .dropdown-menus, #menu #menu_ver_2 > a + .dropdown-menus {
\tborder: 6px solid ";
                // line 518
                echo (isset($context["t1d_mm2_bg_hover_color"]) ? $context["t1d_mm2_bg_hover_color"] : null);
                echo " ;
}
#menu #menu_ver_2 .dropdown-menus .dropdown-menus {
\ttop: -6px;
\tmargin-left: 6px;
}
";
            }
            // line 525
            echo "
";
            // line 526
            if (((isset($context["t1d_mm_sub_main_category_border_status"]) ? $context["t1d_mm_sub_main_category_border_status"] : null) == 1)) {
                // line 527
                echo "#menu #menu_hor .dropdown-menus li.main-cat > a {
\tborder-bottom: 2px solid ";
                // line 528
                echo (isset($context["t1d_mm_sub_link_hover_color"]) ? $context["t1d_mm_sub_link_hover_color"] : null);
                echo ";
}
";
            }
            // line 531
            if (((isset($context["t1d_mm_sub_subcategory_border_status"]) ? $context["t1d_mm_sub_subcategory_border_status"] : null) == 1)) {
                // line 532
                echo "#menu #menu_hor .dropdown-menus li.sub-cat > a, #menu .menu_custom_menu .dropdown-menus a {
\tborder-bottom: 1px solid ";
                // line 533
                echo (isset($context["t1d_mm_sub_titles_bg_color"]) ? $context["t1d_mm_sub_titles_bg_color"] : null);
                echo ";
}
";
            }
            // line 536
            echo "
/*  Product Box  */
span.badge.sale {background-color: ";
            // line 538
            echo (isset($context["t1d_mid_prod_box_sale_icon_color"]) ? $context["t1d_mid_prod_box_sale_icon_color"] : null);
            echo ";}
span.badge.new {background-color: ";
            // line 539
            echo (isset($context["t1d_mid_prod_box_new_icon_color"]) ? $context["t1d_mid_prod_box_new_icon_color"] : null);
            echo ";}
span.badge.out-of-stock {background-color: ";
            // line 540
            echo (isset($context["t1d_mid_prod_box_out_of_stock_icon_color"]) ? $context["t1d_mid_prod_box_out_of_stock_icon_color"] : null);
            echo ";}
.rating .fa-y .fa-star {color: ";
            // line 541
            echo (isset($context["t1d_mid_prod_stars_color"]) ? $context["t1d_mid_prod_stars_color"] : null);
            echo ";}

/*  FOOTER  */
footer {
";
            // line 545
            if (((isset($context["t1d_bg_image_f1_custom"]) ? $context["t1d_bg_image_f1_custom"] : null) != "")) {
                // line 546
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_f1_custom"]) ? $context["t1d_bg_image_f1_custom"] : null));
                echo "\");
";
            } elseif ((            // line 547
(isset($context["t1d_pattern_k_f1"]) ? $context["t1d_pattern_k_f1"] : null) != "none")) {
                // line 548
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_f1"]) ? $context["t1d_pattern_k_f1"] : null);
                echo ".png\");
";
            } else {
                // line 550
                echo "\tbackground-image: none;
";
            }
            // line 552
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_f1_position"]) ? $context["t1d_bg_image_f1_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 553
            echo (isset($context["t1d_bg_image_f1_repeat"]) ? $context["t1d_bg_image_f1_repeat"] : null);
            echo ";
}

/*  Top Custom Block 1  */
#footer_custom_top_1 {
\tcolor: ";
            // line 558
            echo (isset($context["t1d_f6_text_color"]) ? $context["t1d_f6_text_color"] : null);
            echo ";
";
            // line 559
            if (((isset($context["t1d_f6_bg_color_status"]) ? $context["t1d_f6_bg_color_status"] : null) == 1)) {
                // line 560
                echo "\tbackground-color: ";
                echo (isset($context["t1d_f6_bg_color"]) ? $context["t1d_f6_bg_color"] : null);
                echo ";
";
            }
            // line 562
            if (((isset($context["t1d_bg_image_f6_custom"]) ? $context["t1d_bg_image_f6_custom"] : null) != "")) {
                // line 563
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_f6_custom"]) ? $context["t1d_bg_image_f6_custom"] : null));
                echo "\");
";
            } elseif ((            // line 564
(isset($context["t1d_pattern_k_f6"]) ? $context["t1d_pattern_k_f6"] : null) != "none")) {
                // line 565
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_f6"]) ? $context["t1d_pattern_k_f6"] : null);
                echo ".png\");
";
            } else {
                // line 567
                echo "\tbackground-image: none;
";
            }
            // line 569
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_f6_position"]) ? $context["t1d_bg_image_f6_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 570
            echo (isset($context["t1d_bg_image_f6_repeat"]) ? $context["t1d_bg_image_f6_repeat"] : null);
            echo ";
";
            // line 571
            if (((isset($context["t1d_f6_border_top_status"]) ? $context["t1d_f6_border_top_status"] : null) == 1)) {
                // line 572
                echo "\tborder-top: ";
                echo (isset($context["t1d_f6_border_top_size"]) ? $context["t1d_f6_border_top_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_f6_border_top_color"]) ? $context["t1d_f6_border_top_color"] : null);
                echo ";
";
            }
            // line 574
            echo "}
#footer_custom_top_1 a {color: ";
            // line 575
            echo (isset($context["t1d_f6_link_color"]) ? $context["t1d_f6_link_color"] : null);
            echo ";}
#footer_custom_top_1 a:hover {color: ";
            // line 576
            echo (isset($context["t1d_f6_link_hover_color"]) ? $context["t1d_f6_link_hover_color"] : null);
            echo ";}

/*  Information, Custom Column  */
#information {
\tcolor: ";
            // line 580
            echo (isset($context["t1d_f2_text_color"]) ? $context["t1d_f2_text_color"] : null);
            echo ";
";
            // line 581
            if (((isset($context["t1d_f2_bg_color_status"]) ? $context["t1d_f2_bg_color_status"] : null) == 1)) {
                // line 582
                echo "\tbackground-color: ";
                echo (isset($context["t1d_f2_bg_color"]) ? $context["t1d_f2_bg_color"] : null);
                echo ";
";
            }
            // line 584
            if (((isset($context["t1d_bg_image_f2_custom"]) ? $context["t1d_bg_image_f2_custom"] : null) != "")) {
                // line 585
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_f2_custom"]) ? $context["t1d_bg_image_f2_custom"] : null));
                echo "\");
";
            } elseif ((            // line 586
(isset($context["t1d_pattern_k_f2"]) ? $context["t1d_pattern_k_f2"] : null) != "none")) {
                // line 587
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_f2"]) ? $context["t1d_pattern_k_f2"] : null);
                echo ".png\");
";
            } else {
                // line 589
                echo "\tbackground-image: none;
";
            }
            // line 591
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_f2_position"]) ? $context["t1d_bg_image_f2_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 592
            echo (isset($context["t1d_bg_image_f2_repeat"]) ? $context["t1d_bg_image_f2_repeat"] : null);
            echo ";
";
            // line 593
            if (((isset($context["t1d_f2_border_top_status"]) ? $context["t1d_f2_border_top_status"] : null) == 1)) {
                // line 594
                echo "\tborder-top: ";
                echo (isset($context["t1d_f2_border_top_size"]) ? $context["t1d_f2_border_top_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_f2_border_top_color"]) ? $context["t1d_f2_border_top_color"] : null);
                echo ";
";
            }
            // line 596
            echo "}
#information h4 {
\tcolor: ";
            // line 598
            echo (isset($context["t1d_f2_titles_color"]) ? $context["t1d_f2_titles_color"] : null);
            echo ";
";
            // line 599
            if (((isset($context["t1d_f2_titles_border_status"]) ? $context["t1d_f2_titles_border_status"] : null) == 1)) {
                // line 600
                echo "\tborder-bottom: ";
                echo (isset($context["t1d_f2_titles_border_size"]) ? $context["t1d_f2_titles_border_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_f2_titles_border_color"]) ? $context["t1d_f2_titles_border_color"] : null);
                echo ";
\tmargin-bottom: 15px;
";
            }
            // line 603
            echo "}
#information a, #information span {color: ";
            // line 604
            echo (isset($context["t1d_f2_link_color"]) ? $context["t1d_f2_link_color"] : null);
            echo ";}
#information a:hover {color: ";
            // line 605
            echo (isset($context["t1d_f2_link_hover_color"]) ? $context["t1d_f2_link_hover_color"] : null);
            echo ";}

/*  Powered by, Payment Images, Follow Us  */
#powered {
\tcolor: ";
            // line 609
            echo (isset($context["t1d_f3_text_color"]) ? $context["t1d_f3_text_color"] : null);
            echo ";
";
            // line 610
            if (((isset($context["t1d_f3_bg_color_status"]) ? $context["t1d_f3_bg_color_status"] : null) == 1)) {
                // line 611
                echo "\tbackground-color: ";
                echo (isset($context["t1d_f3_bg_color"]) ? $context["t1d_f3_bg_color"] : null);
                echo ";
";
            }
            // line 613
            if (((isset($context["t1d_bg_image_f3_custom"]) ? $context["t1d_bg_image_f3_custom"] : null) != "")) {
                // line 614
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_f3_custom"]) ? $context["t1d_bg_image_f3_custom"] : null));
                echo "\");
";
            } elseif ((            // line 615
(isset($context["t1d_pattern_k_f3"]) ? $context["t1d_pattern_k_f3"] : null) != "none")) {
                // line 616
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_f3"]) ? $context["t1d_pattern_k_f3"] : null);
                echo ".png\");
";
            } else {
                // line 618
                echo "\tbackground-image: none;
";
            }
            // line 620
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_f3_position"]) ? $context["t1d_bg_image_f3_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 621
            echo (isset($context["t1d_bg_image_f3_repeat"]) ? $context["t1d_bg_image_f3_repeat"] : null);
            echo ";
";
            // line 622
            if (((isset($context["t1d_f3_border_top_status"]) ? $context["t1d_f3_border_top_status"] : null) == 1)) {
                // line 623
                echo "\tborder-top: ";
                echo (isset($context["t1d_f3_border_top_size"]) ? $context["t1d_f3_border_top_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_f3_border_top_color"]) ? $context["t1d_f3_border_top_color"] : null);
                echo ";
";
            }
            // line 625
            echo "}
#powered a {color: ";
            // line 626
            echo (isset($context["t1d_f3_link_color"]) ? $context["t1d_f3_link_color"] : null);
            echo ";}
#powered a:hover, #powered i:hover {color: ";
            // line 627
            echo (isset($context["t1d_f3_link_hover_color"]) ? $context["t1d_f3_link_hover_color"] : null);
            echo ";}
";
            // line 628
            if (((isset($context["t1d_f3_icons_social_style"]) ? $context["t1d_f3_icons_social_style"] : null) == 1)) {
                // line 629
                echo "#footer-social li.facebook {
\tbackground-color: #3B5998;
}
#footer-social li.twitter {
\tbackground-color: #4BB8E2;
}
#footer-social li.google {
\tbackground-color: #D73A1A;
}
#footer-social li.rrs {
\tbackground-color: #F29735;
}
#footer-social li.pinterest {
\tbackground-color: #C92026;
}
#footer-social li.vimeo {
\tbackground-color: #44A4E3;
}
#footer-social li.flickr {
\tbackground-color: #C3C3C3;
}
#footer-social li.linkedin {
\tbackground-color: #0087CD;
}
#footer-social li.youtube {
\tbackground-color: #DB3B1B;
}
#footer-social li.dribbble {
\tbackground-color: #DA467F;
}
#footer-social li.instagram {
\tbackground-color: #A27358;
}
#footer-social li.behance {
\tbackground-color: #00A8ED;
}
#footer-social li.skype {
\tbackground-color: #00AEF0;
}
#footer-social li.tumblr {
\tbackground-color: #3B5876;
}
#footer-social li.reddit {
\tbackground-color: #FF4400;
}
#footer-social li.vk {
\tbackground-color: #4C75A3;
}
";
            } else {
                // line 678
                echo "#footer-social li {background-color: ";
                echo (isset($context["t1d_f3_icons_bg_color"]) ? $context["t1d_f3_icons_bg_color"] : null);
                echo ";}
";
            }
            // line 680
            echo "
/*  Bottom Custom Block 1  */
#footer_custom_1 {
\tcolor: ";
            // line 683
            echo (isset($context["t1d_f4_text_color"]) ? $context["t1d_f4_text_color"] : null);
            echo ";
";
            // line 684
            if (((isset($context["t1d_f4_bg_color_status"]) ? $context["t1d_f4_bg_color_status"] : null) == 1)) {
                // line 685
                echo "\tbackground-color: ";
                echo (isset($context["t1d_f4_bg_color"]) ? $context["t1d_f4_bg_color"] : null);
                echo ";
";
            }
            // line 687
            if (((isset($context["t1d_bg_image_f4_custom"]) ? $context["t1d_bg_image_f4_custom"] : null) != "")) {
                // line 688
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_f4_custom"]) ? $context["t1d_bg_image_f4_custom"] : null));
                echo "\");
";
            } elseif ((            // line 689
(isset($context["t1d_pattern_k_f4"]) ? $context["t1d_pattern_k_f4"] : null) != "none")) {
                // line 690
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_f4"]) ? $context["t1d_pattern_k_f4"] : null);
                echo ".png\");
";
            } else {
                // line 692
                echo "\tbackground-image: none;
";
            }
            // line 694
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_f4_position"]) ? $context["t1d_bg_image_f4_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 695
            echo (isset($context["t1d_bg_image_f4_repeat"]) ? $context["t1d_bg_image_f4_repeat"] : null);
            echo ";
";
            // line 696
            if (((isset($context["t1d_f4_border_top_status"]) ? $context["t1d_f4_border_top_status"] : null) == 1)) {
                // line 697
                echo "\tborder-top: ";
                echo (isset($context["t1d_f4_border_top_size"]) ? $context["t1d_f4_border_top_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_f4_border_top_color"]) ? $context["t1d_f4_border_top_color"] : null);
                echo ";
";
            }
            // line 699
            echo "}
#footer_custom_1 a {color: ";
            // line 700
            echo (isset($context["t1d_f4_link_color"]) ? $context["t1d_f4_link_color"] : null);
            echo ";}
#footer_custom_1 a:hover {color: ";
            // line 701
            echo (isset($context["t1d_f4_link_hover_color"]) ? $context["t1d_f4_link_hover_color"] : null);
            echo ";}

/*  Bottom Custom Block 2  */
#footer_custom_2 {
\tcolor: ";
            // line 705
            echo (isset($context["t1d_f5_text_color"]) ? $context["t1d_f5_text_color"] : null);
            echo ";
";
            // line 706
            if (((isset($context["t1d_f5_bg_color_status"]) ? $context["t1d_f5_bg_color_status"] : null) == 1)) {
                // line 707
                echo "\tbackground-color: ";
                echo (isset($context["t1d_f5_bg_color"]) ? $context["t1d_f5_bg_color"] : null);
                echo ";
";
            }
            // line 709
            if (((isset($context["t1d_bg_image_f5_custom"]) ? $context["t1d_bg_image_f5_custom"] : null) != "")) {
                // line 710
                echo "\tbackground-image: url(\"";
                echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1d_bg_image_f5_custom"]) ? $context["t1d_bg_image_f5_custom"] : null));
                echo "\");
";
            } elseif ((            // line 711
(isset($context["t1d_pattern_k_f5"]) ? $context["t1d_pattern_k_f5"] : null) != "none")) {
                // line 712
                echo "\tbackground-image: url(\"catalog/view/theme/oxy/image/patterns/p";
                echo (isset($context["t1d_pattern_k_f5"]) ? $context["t1d_pattern_k_f5"] : null);
                echo ".png\");
";
            } else {
                // line 714
                echo "\tbackground-image: none;
";
            }
            // line 716
            echo "\tbackground-position: ";
            echo (isset($context["t1d_bg_image_f5_position"]) ? $context["t1d_bg_image_f5_position"] : null);
            echo ";
\tbackground-repeat: ";
            // line 717
            echo (isset($context["t1d_bg_image_f5_repeat"]) ? $context["t1d_bg_image_f5_repeat"] : null);
            echo ";
";
            // line 718
            if (((isset($context["t1d_f5_border_top_status"]) ? $context["t1d_f5_border_top_status"] : null) == 1)) {
                // line 719
                echo "\tborder-top: ";
                echo (isset($context["t1d_f5_border_top_size"]) ? $context["t1d_f5_border_top_size"] : null);
                echo "px solid ";
                echo (isset($context["t1d_f5_border_top_color"]) ? $context["t1d_f5_border_top_color"] : null);
                echo ";
";
            }
            // line 721
            echo "}
#footer_custom_2 a {color: ";
            // line 722
            echo (isset($context["t1d_f5_link_color"]) ? $context["t1d_f5_link_color"] : null);
            echo ";}
#footer_custom_2 a:hover {color: ";
            // line 723
            echo (isset($context["t1d_f5_link_hover_color"]) ? $context["t1d_f5_link_hover_color"] : null);
            echo ";}


/*  Prices */
#content .product-thumb p.price, #cart span.price, #column-left .product-items .price, #column-right .product-items .price, .product-buy .price-reg, #content .product-right-sm-related p.price, .product-buy .price-old {color: ";
            // line 727
            echo (isset($context["t1d_price_color"]) ? $context["t1d_price_color"] : null);
            echo ";}
.price-old, .product-thumb .price-tax {color: ";
            // line 728
            echo (isset($context["t1d_price_old_color"]) ? $context["t1d_price_old_color"] : null);
            echo ";}
.price-new, .save-percent {color: ";
            // line 729
            echo (isset($context["t1d_price_new_color"]) ? $context["t1d_price_new_color"] : null);
            echo ";}

/*  Buttons */
.btn-default, button.wishlist, button.compare, .pagination > li > a, .pagination > li > span, #cart .btn-default, input#input-quantity, input.dec, input.inc, .owl-carousel .owl-buttons div, .theme-gallery-content .gallery-hover-box, .theme-store-tv-content .store-tv-hover-box, .btn.theme-banner-title.no-link:hover, #footer_custom_top_1 a.btn-default, footer_custom_1 a.btn-default, #information a.btn-default, #footer_custom_2 a.btn-default, #menu .dropdown-menus a.btn-default.menu-button, .product-right-sm-info span.p-icon, .swiper-pager div {
\t";
            // line 733
            if (((isset($context["t1d_button_bg_status"]) ? $context["t1d_button_bg_status"] : null) == 1)) {
                echo " 
\tbackground-color: ";
                // line 734
                echo (isset($context["t1d_button_bg_color"]) ? $context["t1d_button_bg_color"] : null);
                echo ";
\t";
            } else {
                // line 736
                echo "\tbackground-color: transparent;
\t";
            }
            // line 738
            echo "\tcolor: ";
            echo (isset($context["t1d_button_text_color"]) ? $context["t1d_button_text_color"] : null);
            echo ";
\tborder: 2px solid ";
            // line 739
            echo (isset($context["t1d_button_bg_color"]) ? $context["t1d_button_bg_color"] : null);
            echo ";
}
.btn-default:hover, #column-left .btn-default:hover, #column-right .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .btn-default.disabled, .btn-default[disabled], button.wishlist:hover, button.compare:hover, .pagination > li > a:hover, .pagination > li > a:focus, .pagination > li > a:active, .pagination > li > span:hover, #cart .btn-default:hover, input.dec:hover, input.inc:hover, .panel-inline-content .brand-slider-item:hover .btn-default, .panel-inline-content .category-slider-item:hover .btn-default, .category-list .image:hover .btn-default, .owl-carousel .owl-buttons div:hover, .theme-lookbook-item:hover .theme-lookbook-item-title, .theme-banner-item:hover .theme-banner-title, #footer_custom_top_1 a.btn-default:hover, footer_custom_1 a.btn-default:hover, #information a.btn-default:hover, #footer_custom_2 a.btn-default:hover, #menu .dropdown-menus a.btn-default.menu-button:hover, .btn-default.active:hover, .swiper-pager div:hover {
\tbackground-color: ";
            // line 742
            echo (isset($context["t1d_button_bg_hover_color"]) ? $context["t1d_button_bg_hover_color"] : null);
            echo ";
    color: ";
            // line 743
            echo (isset($context["t1d_button_text_hover_color"]) ? $context["t1d_button_text_hover_color"] : null);
            echo ";
\tborder: 2px solid ";
            // line 744
            echo (isset($context["t1d_button_bg_hover_color"]) ? $context["t1d_button_bg_hover_color"] : null);
            echo ";\t
\t";
            // line 745
            if (((isset($context["t1d_button_shadow_status"]) ? $context["t1d_button_shadow_status"] : null) == 1)) {
                // line 746
                echo "\tbox-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);
\t";
            }
            // line 748
            echo "}
.theme-gallery-content .gallery-hover-box i, .theme-store-tv-content .store-tv-hover-box i {
\tcolor: ";
            // line 750
            echo (isset($context["t1d_button_text_color"]) ? $context["t1d_button_text_color"] : null);
            echo ";
}
input[type=\"text\"]:focus, input[type=\"password\"]:focus, input[type=\"date\"]:focus, input[type=\"datetime\"]:focus, input[type=\"email\"]:focus, input[type=\"number\"]:focus, input[type=\"search\"]:focus, input[type=\"tel\"]:focus, input[type=\"time\"]:focus, input[type=\"url\"]:focus, textarea:focus, select:focus, .form-control:focus, .form-control {
\tbackground-color: ";
            // line 753
            echo (isset($context["t1d_content_column_bg_color"]) ? $context["t1d_content_column_bg_color"] : null);
            echo ";
\tcolor: ";
            // line 754
            echo (isset($context["t1d_button_text_color"]) ? $context["t1d_button_text_color"] : null);
            echo ";
\tborder: 1px solid ";
            // line 755
            echo (isset($context["t1d_button_bg_color"]) ? $context["t1d_button_bg_color"] : null);
            echo ";
}


.btn-primary, .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus, #cart .btn-primary, #footer_custom_top_1 a.btn-primary, footer_custom_1 a.btn-primary, #information a.btn-primary, .contact-block + .dropdown-menu li div.contact-links a.btn-primary, .compare-page .table .btn-primary, #footer_custom_2 a.btn-primary, .brand-slider-item:hover .btn-default.inline-name, #menu .dropdown-menus a.btn-primary.menu-button, #cart .checkout a.btn-default, .contact-block + .dropdown-menu li div.contact-links a.btn-default {
\t";
            // line 760
            if (((isset($context["t1d_button_exclusive_bg_status"]) ? $context["t1d_button_exclusive_bg_status"] : null) == 1)) {
                echo " 
\tbackground-color: ";
                // line 761
                echo (isset($context["t1d_button_exclusive_bg_color"]) ? $context["t1d_button_exclusive_bg_color"] : null);
                echo ";
\t";
            } else {
                // line 763
                echo "\tbackground-color: transparent;
\t";
            }
            // line 765
            echo "\tcolor: ";
            echo (isset($context["t1d_button_exclusive_text_color"]) ? $context["t1d_button_exclusive_text_color"] : null);
            echo ";
\tborder: 2px solid ";
            // line 766
            echo (isset($context["t1d_button_exclusive_bg_color"]) ? $context["t1d_button_exclusive_bg_color"] : null);
            echo ";
}
.nav-tabs > li.active > a:hover, .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus {
\t";
            // line 769
            if (((isset($context["t1d_button_exclusive_bg_status"]) ? $context["t1d_button_exclusive_bg_status"] : null) == 1)) {
                echo " 
\tbackground-color: ";
                // line 770
                echo (isset($context["t1d_button_exclusive_bg_color"]) ? $context["t1d_button_exclusive_bg_color"] : null);
                echo ";
\t";
            } else {
                // line 772
                echo "\tbackground-color: transparent;
\t";
            }
            // line 774
            echo "\tcolor: ";
            echo (isset($context["t1d_button_exclusive_text_color"]) ? $context["t1d_button_exclusive_text_color"] : null);
            echo ";
}
.ei-title h4 a.btn {
\tcolor: ";
            // line 777
            echo (isset($context["t1d_button_exclusive_text_color"]) ? $context["t1d_button_exclusive_text_color"] : null);
            echo "!important;
}

.btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled], #cart .btn-primary:hover, #footer_custom_top_1 a.btn-primary:hover, footer_custom_1 a.btn-primary:hover, #cart .checkout a.btn-default:hover, #information a.btn-primary:hover, .contact-block + .dropdown-menu li div.contact-links a.btn:hover, .compare-page .table .btn-primary:hover, #footer_custom_2 a.btn-primary:hover, .brand-slider-item .btn-default.inline-name, #menu .dropdown-menus a.btn-primary.menu-button:hover {
\tbackground-color: ";
            // line 781
            echo (isset($context["t1d_button_exclusive_bg_hover_color"]) ? $context["t1d_button_exclusive_bg_hover_color"] : null);
            echo ";
    color: ";
            // line 782
            echo (isset($context["t1d_button_exclusive_text_hover_color"]) ? $context["t1d_button_exclusive_text_hover_color"] : null);
            echo ";
\tborder: 2px solid ";
            // line 783
            echo (isset($context["t1d_button_exclusive_bg_hover_color"]) ? $context["t1d_button_exclusive_bg_hover_color"] : null);
            echo ";\t
\t";
            // line 784
            if (((isset($context["t1d_button_shadow_status"]) ? $context["t1d_button_shadow_status"] : null) == 1)) {
                // line 785
                echo "\tbox-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);
\t";
            }
            // line 787
            echo "}
.btn, .theme-modal-popup .modal-body .close, #product-tabs .nav-tabs li a {
\tborder-radius: ";
            // line 789
            echo (isset($context["t1d_button_border_radius"]) ? $context["t1d_button_border_radius"] : null);
            echo "px!important;
}
";
            // line 791
            if (((isset($context["t1d_button_shadow_status"]) ? $context["t1d_button_shadow_status"] : null) == 1)) {
                // line 792
                echo "#features .theme-modal:hover span.f-icon {
\tbox-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);
}
";
            }
            // line 796
            echo "
/*  Dropdowns  */
.dropdown-menu, .dropdown-menus, .my-account-dropdown-menu li, #cart .dropdown-menu, #menu-mobile, .prev, .next {background-color: ";
            // line 798
            echo (isset($context["t1d_dd_bg_color"]) ? $context["t1d_dd_bg_color"] : null);
            echo ";}
#cart span.name a, #cart span.quantity, .sidebar-nav > .sidebar-title, .sidebar-nav h5, #cart .dropdown-menu li div.cart-title .cart-total, #cart .table.cart-total span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .sidebar-nav li.category-mobile a {color: ";
            // line 799
            echo (isset($context["t1d_dd_headings_color"]) ? $context["t1d_dd_headings_color"] : null);
            echo ";}
#cart .table.cart-total > tbody > tr > td, .dropdown-menu .datepicker, .prev-name, .next-name, .contact-block + .dropdown-menu li div.contact-details, .sidebar-nav li a {color: ";
            // line 800
            echo (isset($context["t1d_dd_text_color"]) ? $context["t1d_dd_text_color"] : null);
            echo ";}
#cart .table.cart-total > tbody > tr > td:first-child, #cart .dropdown-menu li p, #cart .dropdown-menu li div.cart-title i, #cart .dropdown-menu small, #cart span.price, #cart button.item-remove, .sidebar-nav li.dcjq-parent-li ul li a, .contact-block + .dropdown-menu li div.contact-details .col-sm-6, .information-block + .dropdown-menu li div.information-details a, .prev-arrow a, .next-arrow a {color: ";
            // line 801
            echo (isset($context["t1d_dd_light_text_color"]) ? $context["t1d_dd_light_text_color"] : null);
            echo ";}
.dropdown-menu a, #top .dropdown-menu a, #top .dropdown-menu .btn-link, .bootstrap-datetimepicker-widget td.old, .bootstrap-datetimepicker-widget td.new, #column-left .panel-default.panel-category .dropdown-menus a, #column-right .panel-default.panel-category .dropdown-menus a {color: ";
            // line 802
            echo (isset($context["t1d_dd_links_color"]) ? $context["t1d_dd_links_color"] : null);
            echo ";}
.dropdown-menu a:hover, #top .dropdown-menu a:hover, .my-account-dropdown-menu li.logout a:hover, #top .dropdown-menu li:hover .btn-link, .information-block + .dropdown-menu li div.information-details a:hover, .prev-name:hover, .next-name:hover, #column-left .panel-default.panel-category .dropdown-menus a:hover, #column-right .panel-default.panel-category .dropdown-menus a:hover {color: ";
            // line 803
            echo (isset($context["t1d_dd_links_hover_color"]) ? $context["t1d_dd_links_hover_color"] : null);
            echo ";}
.top-links .my-account-dropdown-menu i {color: ";
            // line 804
            echo (isset($context["t1d_dd_icons_color"]) ? $context["t1d_dd_icons_color"] : null);
            echo ";}
.top-links .my-account-dropdown-menu li a:hover i, #cart button.item-remove:hover, #contact-toggle-close span, #menu-mobile-toggle-close span, #highly-recommended-module-close span, .sidebar-nav li.home-mobile a, .sidebar-nav li a:hover, .sidebar-nav li.dcjq-parent-li ul li a:hover, #cart .dropdown-menu li div.cart-title .cart-total span {color: ";
            // line 805
            echo (isset($context["t1d_dd_icons_hover_color"]) ? $context["t1d_dd_icons_hover_color"] : null);
            echo ";}
.sidebar-nav > .sidebar-title, .my-account-dropdown-menu li.logout, #cart .checkout, #cart .cart-title, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-links, .information-block + .dropdown-menu li div.information-title, .prev-name, .next-name {background-color: ";
            // line 806
            echo (isset($context["t1d_dd_hli_bg_color"]) ? $context["t1d_dd_hli_bg_color"] : null);
            echo ";}
.my-account-dropdown-menu li, #form-currency li, #form-language li, .information-block + .dropdown-menu li div.information-details ul li, .menu_custom_menu .dropdown-menus a, #cart .table-striped > tbody > tr, #accordion-mobile li.dcjq-parent-li, #accordion-mobile .home-mobile, #accordion-mobile .menu_links {border-bottom: 1px solid ";
            // line 807
            echo (isset($context["t1d_dd_separator_color"]) ? $context["t1d_dd_separator_color"] : null);
            echo ";}
.my-account-dropdown-menu li:nth-child(2n) {border-left: 1px solid ";
            // line 808
            echo (isset($context["t1d_dd_separator_color"]) ? $context["t1d_dd_separator_color"] : null);
            echo ";}
.dropdown-menu, .dropdown-menus, #livesearch_search_results, .modal-content {
";
            // line 810
            if (((isset($context["t1d_dd_shadow"]) ? $context["t1d_dd_shadow"] : null) == 1)) {
                // line 811
                echo "\tbox-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);
";
            } else {
                // line 813
                echo "    box-shadow: none;
";
            }
            // line 815
            echo "}
.prev, .next, .panel-category-dropdown .dropdown-menus {
";
            // line 817
            if (((isset($context["t1d_dd_shadow"]) ? $context["t1d_dd_shadow"] : null) == 1)) {
                // line 818
                echo "\tbox-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);
";
            } else {
                // line 820
                echo "    box-shadow: none;
";
            }
            // line 822
            echo "}

/*  Fonts  */

/*  Body  */
body, button, select, .form-control, .menu_label, .tooltip-inner { 
    font-family: ";
            // line 828
            echo (isset($context["t1d_body_font"]) ? $context["t1d_body_font"] : null);
            echo ",Arial,Helvetica,sans-serif; 
}
body, button, select, .form-control, #cart .table > tbody > tr > td, #menu .dropdown-menus, .category-slider-items .subcat a, .panel-category-dropdown .dropdown-menus a, .panel-inline-items .inline-name, .btn.theme-banner-title, .btn.theme-lookbook-item-title, .information-block + .dropdown-menu li div.information-details ul li a, .newsletter-block label, .filters label, #top .btn-group > .btn, .buttons-header, #cart > .btn, .btn-group > .dropdown-menu, .dropdown-menu, .dropdown-menus { 
\tfont-size: ";
            // line 831
            echo (isset($context["t1d_body_font_size"]) ? $context["t1d_body_font_size"] : null);
            echo "px;
}
body, input, button, select, .form-control, .dropdown-menu.my-account-dropdown-menu li a, .newsletter-block label { 
";
            // line 834
            if (((isset($context["t1d_body_font_uppercase"]) ? $context["t1d_body_font_uppercase"] : null) == 1)) {
                // line 835
                echo "    text-transform: uppercase;
";
            } else {
                // line 837
                echo "    text-transform: none;
";
            }
            // line 839
            echo "}

/*  Small Text  */
small, .small, label, #top-news-content, #top, #top #form-currency .btn-link, #top #form-language .btn-link, #menu .dropdown-menus a.see-all, .breadcrumb a, .product-thumb .product_box_brand a, .header-custom-box .header-custom-subtitle, span.badge.sale, span.badge.new, ul.pf-top li, .filters label, .filters a.list-group-item {
\tfont-size: ";
            // line 843
            echo (isset($context["t1d_small_font_size"]) ? $context["t1d_small_font_size"] : null);
            echo "px;
}

/*  Headings and Product Name  */
h1, h2, h3, h4, h5, h6, .panel-heading, .product-right-sm-info span.p-title, #features .f-title, .header-custom-title, legend, .sidebar-nav > .sidebar-title, #menu #menu_hor .dropdown-menus li.main-cat > a, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, #cart .dropdown-menu li p, #cart .dropdown-menu li div.cart-title, #cart span.name a, #cart span.quantity, #cart .table.cart-total > tbody > tr > td, .sidebar-nav li a, #tab-specification strong, #tab-review strong, #open-top-custom-block, .alert, .filters a.list-group-item, #cart #cart-tt #cart-total, #menu #homepage a, #menu .main-menu > li > a, #menu .dropdown-menus.flexMenu-popup > li > a, .price, .price-new, .price-old, .price-reg, .save-percent { 
    font-family: ";
            // line 848
            echo (isset($context["t1d_title_font"]) ? $context["t1d_title_font"] : null);
            echo ",Arial,Helvetica,sans-serif; 
}
h1, h2, h3, h4, h5, h6, .panel-heading, .product-right-sm-info span.p-title, #features .f-title, .header-custom-title, legend, .sidebar-nav > .sidebar-title, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, #cart .dropdown-menu li div.cart-title, #cart span.name a, #cart span.quantity, #cart .table.cart-total > tbody > tr > td, .sidebar-nav li a, #tab-specification strong, #tab-review strong, #open-top-custom-block, .alert, .filters a.list-group-item, #cart #cart-tt #cart-total { 
    font-weight: ";
            // line 851
            echo (isset($context["t1d_title_font_weight"]) ? $context["t1d_title_font_weight"] : null);
            echo "; 
}
h1, h2, h3, h4, h5, h6, .panel-heading, .product-right-sm-info span.p-title, #features .f-title, .header-custom-title, legend, .sidebar-nav > .sidebar-title, #menu #menu_hor .dropdown-menus li.main-cat > a, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, #cart .dropdown-menu li div.cart-title, #cart span.name a, #cart .table.cart-total > tbody > tr > td, .sidebar-nav li a, #tab-specification strong, #tab-review strong, #open-top-custom-block, .alert, .filters a.list-group-item  { 
";
            // line 854
            if (((isset($context["t1d_title_font_uppercase"]) ? $context["t1d_title_font_uppercase"] : null) == 1)) {
                // line 855
                echo "    text-transform: uppercase;
";
            } else {
                // line 857
                echo "    text-transform: none;
";
            }
            // line 859
            echo "}

/*  Subtitle  */
.subtitle, #content .tltblog .row .col-sm-9 h4 + div p {
\tfont-family: ";
            // line 863
            echo (isset($context["t1d_subtitle_font"]) ? $context["t1d_subtitle_font"] : null);
            echo ",Arial,Helvetica,sans-serif; 
    font-size: ";
            // line 864
            echo (isset($context["t1d_subtitle_font_size"]) ? $context["t1d_subtitle_font_size"] : null);
            echo "px;
    font-weight: ";
            // line 865
            echo (isset($context["t1d_subtitle_font_weight"]) ? $context["t1d_subtitle_font_weight"] : null);
            echo ";
\tfont-style: ";
            // line 866
            echo (isset($context["t1d_subtitle_font_style"]) ? $context["t1d_subtitle_font_style"] : null);
            echo ";
";
            // line 867
            if (((isset($context["t1d_subtitle_font_uppercase"]) ? $context["t1d_subtitle_font_uppercase"] : null) == 1)) {
                // line 868
                echo "    text-transform: uppercase;
";
            } else {
                // line 870
                echo "    text-transform: none;
";
            }
            // line 872
            echo "}

/*  Prices  */
.price, .price-new, .price-old, .price-reg, .save-percent { 
    font-weight: ";
            // line 876
            echo (isset($context["t1d_price_font_weight"]) ? $context["t1d_price_font_weight"] : null);
            echo "; 
}

/*  Buttons  */
.btn-default, .btn-primary, .btn-danger {
    font-size: ";
            // line 881
            echo (isset($context["t1d_button_font_size"]) ? $context["t1d_button_font_size"] : null);
            echo "px;
    font-weight: ";
            // line 882
            echo (isset($context["t1d_button_font_weight"]) ? $context["t1d_button_font_weight"] : null);
            echo "; 
";
            // line 883
            if (((isset($context["t1d_button_font_uppercase"]) ? $context["t1d_button_font_uppercase"] : null) == 1)) {
                // line 884
                echo "    text-transform: uppercase;
";
            } else {
                // line 886
                echo "    text-transform: none;
";
            }
            // line 888
            echo "}

/*  Main Menu Bar  */
#menu #homepage a, #menu .main-menu > li > a, #menu .dropdown-menus.flexMenu-popup > li > a {
\tfont-size: ";
            // line 892
            echo (isset($context["t1d_mm_font_size"]) ? $context["t1d_mm_font_size"] : null);
            echo "px;
    font-weight: ";
            // line 893
            echo (isset($context["t1d_mm_font_weight"]) ? $context["t1d_mm_font_weight"] : null);
            echo ";
";
            // line 894
            if (((isset($context["t1d_mm_font_uppercase"]) ? $context["t1d_mm_font_uppercase"] : null) == 1)) {
                // line 895
                echo "    text-transform: uppercase;
";
            } else {
                // line 897
                echo "    text-transform: none;
";
            }
            // line 899
            echo "}

/*  Sub-Menu Main Categories  */
#menu #menu_hor .dropdown-menus li.main-cat > a {
\tfont-size: ";
            // line 903
            echo (isset($context["t1d_mm_sub_main_cat_font_size"]) ? $context["t1d_mm_sub_main_cat_font_size"] : null);
            echo "px;
    font-weight: ";
            // line 904
            echo (isset($context["t1d_mm_sub_main_cat_font_weight"]) ? $context["t1d_mm_sub_main_cat_font_weight"] : null);
            echo ";
}

/*  Sub-Menu Other Links  */
#menu .dropdown-menus a {
\tfont-size: ";
            // line 909
            echo (isset($context["t1d_mm_sub_font_size"]) ? $context["t1d_mm_sub_font_size"] : null);
            echo "px;
    font-weight: ";
            // line 910
            echo (isset($context["t1d_mm_sub_font_weight"]) ? $context["t1d_mm_sub_font_weight"] : null);
            echo ";
}

</style>
";
        } else {
            // line 915
            echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/oxy/stylesheet/oxy-";
            echo (isset($context["t1d_skin"]) ? $context["t1d_skin"] : null);
            echo ".css\" />
";
        }
        // line 917
        echo "
<style type=\"text/css\">

/*  Layout  */
@media (max-width: 767px) {
#content .product-layout:nth-child(2n+1) {
\tclear: both;
}
}
@media (min-width: 768px) {
";
        // line 927
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "6")) {
            // line 928
            echo "#content .product-layout:nth-child(2n+1) {
\tclear: both;
}
";
        }
        // line 932
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "4")) {
            // line 933
            echo "#content .product-layout:nth-child(3n+1) {
\tclear: both;
}
";
        }
        // line 937
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "3")) {
            // line 938
            echo "#content .product-layout:nth-child(4n+1) {
\tclear: both;
}
";
        }
        // line 942
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "15")) {
            // line 943
            echo "#content .product-layout:nth-child(5n+1) {
\tclear: both;
}
";
        }
        // line 947
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "2")) {
            // line 948
            echo "#content .product-layout:nth-child(6n+1) {
\tclear: both;
}
";
        }
        // line 952
        echo "}

/*  Logo Creator  */
#logo .logo-creator a {color: ";
        // line 955
        echo (isset($context["t1o_text_logo_color"]) ? $context["t1o_text_logo_color"] : null);
        echo ";}
#logo i {color: ";
        // line 956
        echo (isset($context["t1o_text_logo_awesome_color"]) ? $context["t1o_text_logo_awesome_color"] : null);
        echo ";}

/*  Top Promo Message Slider  */
#top-custom-block-content {
\tdisplay:none;
}
#open-top-custom-block i {
\tcolor: ";
        // line 963
        echo (isset($context["t1o_top_custom_block_awesome_color"]) ? $context["t1o_top_custom_block_awesome_color"] : null);
        echo ";
}
#open-top-custom-block i.fa-plus, #open-top-custom-block i.fa-minus {
\tcolor: ";
        // line 966
        echo (isset($context["t1o_top_custom_block_title_color"]) ? $context["t1o_top_custom_block_title_color"] : null);
        echo ";
}

#open-top-custom-block {
\tbackground-color: ";
        // line 970
        echo (isset($context["t1o_top_custom_block_title_bar_bg_color"]) ? $context["t1o_top_custom_block_title_bar_bg_color"] : null);
        echo ";
\tcolor: ";
        // line 971
        echo (isset($context["t1o_top_custom_block_title_color"]) ? $context["t1o_top_custom_block_title_color"] : null);
        echo ";
";
        // line 972
        if (((isset($context["t1o_top_custom_block_bar_bg"]) ? $context["t1o_top_custom_block_bar_bg"] : null) != "")) {
            // line 973
            echo "\tbackground-image: url(\"";
            echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1o_top_custom_block_bar_bg"]) ? $context["t1o_top_custom_block_bar_bg"] : null));
            echo "\");
\tbackground-position: center top;
";
        } else {
            // line 976
            echo "\tbackground-image: none;
";
        }
        // line 978
        echo "}

#top-custom-block-wrapper {
\tbackground-color: ";
        // line 981
        echo (isset($context["t1o_top_custom_block_bg_color"]) ? $context["t1o_top_custom_block_bg_color"] : null);
        echo ";
\tcolor: ";
        // line 982
        echo (isset($context["t1o_top_custom_block_text_color"]) ? $context["t1o_top_custom_block_text_color"] : null);
        echo ";
";
        // line 983
        if (((isset($context["t1o_top_custom_block_bg"]) ? $context["t1o_top_custom_block_bg"] : null) != "")) {
            // line 984
            echo "\tbackground-image: url(\"";
            echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1o_top_custom_block_bg"]) ? $context["t1o_top_custom_block_bg"] : null));
            echo "\");
";
        } else {
            // line 986
            echo "\tbackground-image: none;
";
        }
        // line 988
        echo "}
";
        // line 989
        if (((isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null) == "framed")) {
            // line 990
            echo "#top-custom-block-wrapper {
    margin-bottom: 15px;
}
";
        }
        // line 994
        echo "

/*  Layout  */

";
        // line 998
        if (((isset($context["t1o_layout_l"]) ? $context["t1o_layout_l"] : null) == "2")) {
            // line 999
            echo ".wrapper.framed {
    max-width: 980px;
}
.wrapper.framed .is-sticky .container {
\twidth: 920px !important;
}
";
        }
        // line 1006
        echo ".wrapper.framed {
";
        // line 1007
        if (((isset($context["t1o_layout_framed_align"]) ? $context["t1o_layout_framed_align"] : null) == "0")) {
            // line 1008
            echo "\tmargin: 0 auto 30px;
";
        } else {
            // line 1010
            echo "    margin: 0 30px 30px;
";
        }
        // line 1012
        echo "}

.wrapper.full-width .full-width-container {
";
        // line 1015
        if (((isset($context["t1o_layout_full_width_max"]) ? $context["t1o_layout_full_width_max"] : null) == "1")) {
            // line 1016
            echo "\tmax-width: 1440px;
";
        } elseif ((        // line 1017
(isset($context["t1o_layout_full_width_max"]) ? $context["t1o_layout_full_width_max"] : null) == "2")) {
            // line 1018
            echo "    max-width: 1230px;
";
        } else {
            // line 1020
            echo "    max-width: inherit;
";
        }
        // line 1022
        echo "}

";
        // line 1024
        if (((isset($context["t1o_layout_catalog_mode"]) ? $context["t1o_layout_catalog_mode"] : null) == "1")) {
            // line 1025
            echo "#top .top-links, #top #currency, #cart, .price, .price-new, .price-old, .product-buy ul.pp, .flybar-items, .cart, .product-buy #product, .wishlist, .compare, #compare-total, span.badge.sale, .alert, .quickview-success {
    display: none;
}
.product-list .list-quickview {
\tmargin-top: 20px;
}
.product-grid > div:hover .flybar, .product-gallery > div:hover .flybar, #content .box-product > div:hover .flybar, .product-box-slider-flexslider ul > li:hover .flybar, .product-bottom-related-flexslider ul > li:hover .flybar {
\tbottom: -10px;
}
@media (max-width: 767px) {
.buttons-header {width: 50%;}
#search-block {border-left: none!important;}
}
";
        }
        // line 1039
        echo "
/*  Top Bar  */
";
        // line 1041
        if (((isset($context["t1o_top_bar_status"]) ? $context["t1o_top_bar_status"] : null) == "0")) {
            // line 1042
            echo "#top {display: none;}
";
        }
        // line 1044
        echo "
/*  MAIN MENU */

/*  Main Menu Labels  */
";
        // line 1048
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            echo " 
#menu_label";
            // line 1049
            echo $context["i"];
            echo ":after { background-color:";
            echo $this->getAttribute((isset($context["t1o_menu_labels_color"]) ? $context["t1o_menu_labels_color"] : null), $context["i"], array(), "array");
            echo "; }
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1051
        echo "
/*  Custom Bar below Main Menu  */
#custom-bar-wrapper {
\tbackground-color: ";
        // line 1054
        echo (isset($context["t1o_custom_bar_below_menu_bg_color"]) ? $context["t1o_custom_bar_below_menu_bg_color"] : null);
        echo ";
\tcolor: ";
        // line 1055
        echo (isset($context["t1o_custom_bar_below_menu_text_color"]) ? $context["t1o_custom_bar_below_menu_text_color"] : null);
        echo ";
";
        // line 1056
        if (((isset($context["t1o_custom_bar_below_menu_bg"]) ? $context["t1o_custom_bar_below_menu_bg"] : null) != "")) {
            // line 1057
            echo "\tbackground-image: url(\"";
            echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1o_custom_bar_below_menu_bg"]) ? $context["t1o_custom_bar_below_menu_bg"] : null));
            echo "\");
";
        } else {
            // line 1059
            echo "\tbackground-image: none;
";
        }
        // line 1061
        if (((isset($context["t1o_custom_bar_below_menu_bg_animation"]) ? $context["t1o_custom_bar_below_menu_bg_animation"] : null) == "1")) {
            // line 1062
            echo "    animation: animatedBackground 40s linear infinite;
";
        }
        // line 1064
        echo "}

/*  Category Page  */
.category_top_title h1 {color: ";
        // line 1067
        echo (isset($context["t1o_category_title_above_color"]) ? $context["t1o_category_title_above_color"] : null);
        echo ";}
";
        // line 1068
        if (((isset($context["t1o_category_title_position"]) ? $context["t1o_category_title_position"] : null) == 0)) {
            // line 1069
            echo "#content .category-list-carousel.owl-carousel {
\tmargin: 30px 0 0 0;
}
";
        }
        // line 1073
        echo "
/*  Product Box  */
";
        // line 1075
        if (((isset($context["t1o_category_prod_name_status"]) ? $context["t1o_category_prod_name_status"] : null) == 0)) {
            echo "\t
.product-grid .name, .product-gallery .name, #content .box-product .name, .product-bottom-related .name, .product-box-slider .name, .product-right-sm-related .name {display: none;}
";
        }
        // line 1078
        if (((isset($context["t1o_category_prod_brand_status"]) ? $context["t1o_category_prod_brand_status"] : null) == 0)) {
            echo "\t
.product_box_brand {display: none;}
";
        }
        // line 1081
        if (((isset($context["t1o_category_prod_price_status"]) ? $context["t1o_category_prod_price_status"] : null) == 0)) {
            echo "\t
.product-grid .price, #content .box-product .price, .product-list .price, #content .product-list.product-small-list .product-thumb .price, .product-gallery .price, .product-bottom-related .price, .product-box-slider .price, .product-right-sm-related p.price {display: none;}
";
        }
        // line 1084
        if (((isset($context["t1o_category_prod_quickview_status"]) ? $context["t1o_category_prod_quickview_status"] : null) == 0)) {
            echo "\t
a.list-quickview, .product-grid.product-box-style-1 .list-quickview, .product-grid a.quickview, .product-gallery a.quickview, .product-items-theme-featured .product-grid .flybar .flybar-items .quickview {display: none;}
";
        }
        // line 1087
        if (((isset($context["t1o_category_prod_cart_status"]) ? $context["t1o_category_prod_cart_status"] : null) == 0)) {
            echo "\t
.product-grid .cart, .product-grid.product-box-style-1 .caption .cart, .flybar .flybar-items button.btn-primary, #content .box-product .cart, .product-list .cart, .product-box-slider .cart, .product-items-theme-featured .product-grid .flybar .flybar-items button.btn-primary {display: none;}
";
        }
        // line 1090
        if (((isset($context["t1o_category_prod_ratings_status"]) ? $context["t1o_category_prod_ratings_status"] : null) == 0)) {
            echo "\t
.product-grid .rating, .product-list .rating, #content .product-list.product-small-list .product-thumb .rating, .product-gallery .rating, #content .box-product .rating, #column-left .box-product .rating, #column-right .box-product .rating, .product-box-slider .rating, .product-right-sm-related .rating {display: none;}
";
        }
        // line 1093
        if (((isset($context["t1o_category_prod_wis_com_status"]) ? $context["t1o_category_prod_wis_com_status"] : null) == 0)) {
            echo "\t
.product-list .list-wishlist, .product-grid .wishlist, .product-gallery .wishlist, .product-list .list-compare, .product-grid .compare, .product-gallery .compare {display: none;}
";
        }
        // line 1096
        if (((isset($context["t1o_category_prod_zoom_status"]) ? $context["t1o_category_prod_zoom_status"] : null) == 1)) {
            echo "\t
#content .product-grid div:hover .image a img, #content .box-product div:hover .image a img {
\ttransform: scale(1.1);
    -moz-transform: scale(1.1);
    -webkit-transform: scale(1.1);
    -o-transform: scale(1.1);
    -ms-transform: scale(1.1);
}
";
        }
        // line 1105
        if (((isset($context["t1o_category_prod_swap_status"]) ? $context["t1o_category_prod_swap_status"] : null) == 0)) {
            // line 1106
            echo ".thumb_swap {display:none}
";
        }
        // line 1108
        if (((isset($context["t1o_category_prod_shadow_status"]) ? $context["t1o_category_prod_shadow_status"] : null) == 1)) {
            // line 1109
            echo "#content .product-thumb:hover {box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);}
";
        }
        // line 1111
        if (((isset($context["t1o_category_prod_lift_up_status"]) ? $context["t1o_category_prod_lift_up_status"] : null) == 1)) {
            // line 1112
            echo "#content .product-grid .product-thumb:hover {transform: translateY(-5px);}
";
        }
        // line 1114
        echo ".product-grid .name, #content .box-product .name, #content .box-product .product_box_brand, .product-bottom-related .name, .product-box-slider .name, .product-grid .product_box_brand, .product-box-slider .product_box_brand, .product-bottom-related-flexslider .product_box_brand, .product-bottom-related-flexslider .rating, .tab-content-products .product_box_brand, .product-grid .price, #content .box-product .price, .product-box-slider .price, .product-bottom-related .price, .product-grid .rating, #content .box-product .rating, .product-box-slider .rating, .product-grid .cart, #content .box-product .cart, .product-box-slider .cart, .product-bottom-related .cart, .product-right-sm-related .name, .product-right-sm-related .product_box_brand, .product-right-sm-related .rating, .product-right-sm-related p.price, .product-grid .product-list-buttons {
";
        // line 1115
        if (((isset($context["t1o_category_prod_align"]) ? $context["t1o_category_prod_align"] : null) == 1)) {
            echo "\t
\ttext-align: left;
\t";
        } else {
            // line 1118
            echo "\ttext-align: center;
";
        }
        // line 1120
        echo "}
";
        // line 1121
        if ((((((isset($context["t1o_category_prod_name_status"]) ? $context["t1o_category_prod_name_status"] : null) == 0) && ((isset($context["t1o_category_prod_brand_status"]) ? $context["t1o_category_prod_brand_status"] : null) == 0)) && ((isset($context["t1o_category_prod_price_status"]) ? $context["t1o_category_prod_price_status"] : null) == 0)) && ((isset($context["t1o_category_prod_ratings_status"]) ? $context["t1o_category_prod_ratings_status"] : null) == 0))) {
            // line 1122
            echo ".product-grid .product-thumb { padding-bottom: 0; }
";
        }
        // line 1124
        if (((isset($context["t1o_layout_h_align"]) ? $context["t1o_layout_h_align"] : null) == 0)) {
            echo "\t
h1, h2, h3, .cat-description, .cat-description-above-content, #product-tabs .nav-tabs, .module-style-2 .panel-inline-title h2, .module-style-2 .panel-inline-subtitle {
\ttext-align: center;
}
.module-style-2 .panel-inline-content .panel-inline-subtitle {
\tpadding: 0 25%;
}
#content .product-buy h1 {
\ttext-align: left;
}
#content .product-buy h1:before {
\tmargin-left: 0;
}
.pf-top {
\ttext-align: center;
}
";
        }
        // line 1141
        echo "

/*  Product Page  */
";
        // line 1144
        if (((isset($context["t1o_product_prev_next_status"]) ? $context["t1o_product_prev_next_status"] : null) == 0)) {
            echo "  
.prev, .next {display: none;}
";
        }
        // line 1147
        echo "
";
        // line 1148
        if ((((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == 11) || ((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == 12))) {
            // line 1149
            echo "@media (min-width: 768px) {
.product-buy {
\tfloat: left;
}
.product-left {
\tfloat: right;
}
}
";
        }
        // line 1158
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == 13)) {
            // line 1159
            echo "#content .product-buy h2 {
\tmargin: 0 0 30px;
}
.product-special-price .col-md-4, span.rating-stars {
\twidth: 100%;
\tdisplay: block;
}
.product-buy .rating a {
\tmargin-left: 0;
\tmargin-right: 15px;
}
.product-buy .rating .fa-pencil {
\tmargin-right: 3px;
}
.col-sm-5.product-buy .product-buy-wrapper {
\tbackground-color: transparent;
\tpadding: 0;
}
.product-buy .form-group .btn-primary {
\tpadding: 15.5px 10px;
\twidth: 100%;
}
input.inc {
\tmargin-right: 0;
}
@media (min-width: 768px) and (max-width: 991px) {
.product-page .product-left, .product-page .product-buy {
\twidth: 100%;
}
}
";
        }
        // line 1190
        if (((isset($context["t1o_product_align"]) ? $context["t1o_product_align"] : null) == 0)) {
            // line 1191
            echo "#content .product-buy h1, #content .product-buy h2, ul.pf, .product-buy ul.pp, .product-buy .rating, .product-buy-custom-block, #product legend {
\ttext-align: center;
}
#content .product-buy h1:before {
\tmargin-left: 45% !important;
}
div.qty, .product-buy .form-group .btn-default.wcs {
\ttext-align: left;
}
";
        }
        // line 1201
        echo "
/*  Snapchat Widget  */
.snapchat_box {
    background-color: ";
        // line 1204
        echo (isset($context["t1o_snapchat_box_bg"]) ? $context["t1o_snapchat_box_bg"] : null);
        echo ";
}

/*  Video Widget  */
.video_box_wrapper #video_box_icon {
    background-color: ";
        // line 1209
        echo (isset($context["t1o_video_box_bg"]) ? $context["t1o_video_box_bg"] : null);
        echo ";
}
.video_box_wrapper .video_box {
    border: 4px solid ";
        // line 1212
        echo (isset($context["t1o_video_box_bg"]) ? $context["t1o_video_box_bg"] : null);
        echo ";
}
 
/*  Custom Content Widget  */
.custom_box_wrapper #custom_box_icon {
    background-color: ";
        // line 1217
        echo (isset($context["t1o_custom_box_bg"]) ? $context["t1o_custom_box_bg"] : null);
        echo ";
}
.custom_box_wrapper .custom_box {
    border: 4px solid ";
        // line 1220
        echo (isset($context["t1o_custom_box_bg"]) ? $context["t1o_custom_box_bg"] : null);
        echo ";
}


/*  FOOTER  */

/*  Bottom Custom Block 2  */
";
        // line 1227
        if (((isset($context["t1o_custom_bottom_2_status"]) ? $context["t1o_custom_bottom_2_status"] : null) == 1)) {
            // line 1228
            echo "@media (min-width: 768px) {
.wrapper.full-width footer {
\tmargin-bottom: ";
            // line 1230
            echo (isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null);
            echo "px;
}
#footer_custom_2 {
\theight: ";
            // line 1233
            echo (isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null);
            echo "px;
}
}
";
        }
        // line 1237
        echo "

/*  Other  */
";
        // line 1240
        if (((isset($context["t1o_eu_cookie_icon_status"]) ? $context["t1o_eu_cookie_icon_status"] : null) == 0)) {
            // line 1241
            echo "span.cookie-img {
\tdisplay: none;
}
";
        }
        // line 1245
        echo "
</style>

";
        // line 1248
        if (((isset($context["t1o_custom_css"]) ? $context["t1o_custom_css"] : null) != "")) {
            // line 1249
            echo "<style type=\"text/css\">
/*  Custom CSS */
";
            // line 1251
            echo (isset($context["t1o_custom_css"]) ? $context["t1o_custom_css"] : null);
            echo "
</style>
";
        }
        // line 1254
        echo "
<link href='//fonts.googleapis.com/css?family=";
        // line 1255
        echo twig_replace_filter((isset($context["t1d_body_font"]) ? $context["t1d_body_font"] : null), array(" " => "+"));
        echo ":400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        // line 1256
        if (((isset($context["t1d_title_font"]) ? $context["t1d_title_font"] : null) != (isset($context["t1d_body_font"]) ? $context["t1d_body_font"] : null))) {
            // line 1257
            echo "<link href='//fonts.googleapis.com/css?family=";
            echo twig_replace_filter((isset($context["t1d_title_font"]) ? $context["t1d_title_font"] : null), array(" " => "+"));
            echo ":400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        }
        // line 1259
        if (((isset($context["t1d_subtitle_font"]) ? $context["t1d_subtitle_font"] : null) != (isset($context["t1d_body_font"]) ? $context["t1d_body_font"] : null))) {
            // line 1260
            echo "<link href='//fonts.googleapis.com/css?family=";
            echo twig_replace_filter((isset($context["t1d_subtitle_font"]) ? $context["t1d_subtitle_font"] : null), array(" " => "+"));
            echo ":400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        }
        // line 1262
        echo "
";
        // line 1263
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin2-luxury")) {
            echo "\t
<link href='//fonts.googleapis.com/css?family=Merriweather:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        }
        // line 1266
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin3-furniture")) {
            echo "\t
<link href='//fonts.googleapis.com/css?family=Karla:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        }
        // line 1269
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin4-market")) {
            echo "\t
<link href='//fonts.googleapis.com/css?family=Montserrat:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Maven+Pro:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        }
        // line 1273
        if (((isset($context["t1d_skin"]) ? $context["t1d_skin"] : null) == "skin5-minimal")) {
            echo "\t
<link href='//fonts.googleapis.com/css?family=Montserrat:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>
";
        }
        // line 1276
        echo "
<script>
\$(function(){
  \$.stellar({
    horizontalScrolling: false,
\tverticalOffset: 0
  });
});
</script>

";
        // line 1286
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["analytics"]) ? $context["analytics"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["analytic"]) {
            // line 1287
            echo $context["analytic"];
            echo "
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['analytic'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1289
        echo "
</head>
<body class=\"";
        // line 1291
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">

";
        // line 1293
        if ((((isset($context["t1o_facebook_likebox_status"]) ? $context["t1o_facebook_likebox_status"] : null) == 1) && ((isset($context["t1o_facebook_likebox_id"]) ? $context["t1o_facebook_likebox_id"] : null) != ""))) {
            echo "  
<div class=\"facebook_wrapper hidden-xs\">
<div id=\"facebook_icon\"><i class=\"fa fa-facebook\"></i></div>
<div class=\"facebook_box\">
<div id=\"fb-root\"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = \"//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5\";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class=\"fb-page\" data-href=\"https://www.facebook.com/";
            // line 1305
            echo (isset($context["t1o_facebook_likebox_id"]) ? $context["t1o_facebook_likebox_id"] : null);
            echo "\" data-width=\"327\" data-height=\"389\" data-colorscheme=\"light\" data-tabs=\"timeline\" data-small-header=\"true\" data-adapt-container-width=\"true\" data-hide-cover=\"false\" data-show-facepile=\"true\"></div>
  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".facebook_wrapper\").hover(function(){            
        \$(\".facebook_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );        
        },
  function(){            
        \$(\".facebook_wrapper\").stop(true, false).animate({right: \"-335\" }, 800, 'easeInQuint' );        
        },1000);    
  }); 
  </script>
</div>
</div>
";
        }
        // line 1319
        echo " 
";
        // line 1320
        if ((((isset($context["t1o_twitter_block_status"]) ? $context["t1o_twitter_block_status"] : null) == 1) && ((isset($context["t1o_twitter_block_user"]) ? $context["t1o_twitter_block_user"] : null) != ""))) {
            echo " 
<div class=\"twitter_wrapper hidden-xs\">
<div id=\"twitter_icon\"><i class=\"fa fa-twitter\"></i></div>
<div class=\"twitter_box\">
  ";
            // line 1324
            if (((isset($context["t1o_twitter_block_user"]) ? $context["t1o_twitter_block_user"] : null) != "")) {
                // line 1325
                echo "  <p><a class=\"twitter-timeline\"  href=\"https://twitter.com/";
                echo (isset($context["t1o_twitter_block_user"]) ? $context["t1o_twitter_block_user"] : null);
                echo "\" data-chrome=\"noheader nofooter noborders noscrollbar transparent\" data-tweet-limit=\"";
                echo (isset($context["t1o_twitter_block_tweets"]) ? $context["t1o_twitter_block_tweets"] : null);
                echo "\"  data-widget-id=\"";
                echo (isset($context["t1o_twitter_block_widget_id"]) ? $context["t1o_twitter_block_widget_id"] : null);
                echo "\" data-theme=\"light\" data-related=\"twitterapi,twitter\" data-aria-polite=\"assertive\">Tweets by ";
                echo (isset($context["t1o_twitter_block_user"]) ? $context["t1o_twitter_block_user"] : null);
                echo "</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script></p>
  ";
            }
            // line 1328
            echo "  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".twitter_wrapper\").hover(function(){            
        \$(\".twitter_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );        
        },
  function(){            
        \$(\".twitter_wrapper\").stop(true, false).animate({right: \"-335\" }, 800, 'easeInQuint' );        
        },1000);    
  });
  </script>
</div>
</div>
";
        }
        // line 1341
        echo "
";
        // line 1342
        if ((((isset($context["t1o_googleplus_box_status"]) ? $context["t1o_googleplus_box_status"] : null) == 1) && ((isset($context["t1o_googleplus_box_user"]) ? $context["t1o_googleplus_box_user"] : null) != ""))) {
            // line 1343
            echo "<div class=\"googleplus_wrapper hidden-xs\">
<div id=\"googleplus_icon\"><i class=\"fa fa-google-plus\"></i></div>
<div class=\"googleplus_box\">
<script src=\"//apis.google.com/js/platform.js\" async defer></script>
<div class=\"g-page\" data-href=\"//plus.google.com/";
            // line 1347
            echo (isset($context["t1o_googleplus_box_user"]) ? $context["t1o_googleplus_box_user"] : null);
            echo "\" data-rel=\"publisher\"></div>
  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".googleplus_wrapper\").hover(function(){            
        \$(\".googleplus_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );        
        },
  function(){            
        \$(\".googleplus_wrapper\").stop(true, false).animate({right: \"-308\" }, 800, 'easeInQuint' );        
        },1000);    
  });  
  </script>
</div>
</div>
";
        }
        // line 1361
        echo "
";
        // line 1362
        if ((((isset($context["t1o_pinterest_box_status"]) ? $context["t1o_pinterest_box_status"] : null) == 1) && ((isset($context["t1o_pinterest_box_user"]) ? $context["t1o_pinterest_box_user"] : null) != ""))) {
            // line 1363
            echo "<div class=\"pinterest_wrapper hidden-xs\">
<div id=\"pinterest_icon\"><i class=\"fa fa-pinterest\"></i></div>
<div class=\"pinterest_box\">
<script async defer src=\"//assets.pinterest.com/js/pinit.js\"></script>
<a data-pin-do=\"embedUser\" data-pin-board-width=\"307\" data-pin-scale-height=\"390\" data-pin-scale-width=\"115\" href=\"//www.pinterest.com/";
            // line 1367
            echo (isset($context["t1o_pinterest_box_user"]) ? $context["t1o_pinterest_box_user"] : null);
            echo "/\"></a>
  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".pinterest_wrapper\").hover(function(){            
        \$(\".pinterest_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );        
        },
  function(){            
        \$(\".pinterest_wrapper\").stop(true, false).animate({right: \"-335\" }, 800, 'easeInQuint' );        
        },1000);    
  });
  </script>
</div>
</div>
";
        }
        // line 1381
        echo "
";
        // line 1382
        if ((((isset($context["t1o_snapchat_box_status"]) ? $context["t1o_snapchat_box_status"] : null) == 1) && ((isset($context["t1o_snapchat_box_code_custom"]) ? $context["t1o_snapchat_box_code_custom"] : null) != ""))) {
            echo " 
<div class=\"snapchat_wrapper hidden-xs\">
<div id=\"snapchat_icon\"><i class=\"fa fa-snapchat-ghost\" aria-hidden=\"true\"></i></div>
<div class=\"snapchat_box\">
<img src=\"";
            // line 1386
            echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1o_snapchat_box_code_custom"]) ? $context["t1o_snapchat_box_code_custom"] : null));
            echo "\" alt=\"Snapchat\" title=\"Snapchat\">
<h3 class=\"text-center\">";
            // line 1387
            echo (isset($context["t1o_snapchat_box_title"]) ? $context["t1o_snapchat_box_title"] : null);
            echo "</h3>
<p class=\"subtitle text-center\">";
            // line 1388
            echo (isset($context["t1o_snapchat_box_subtitle"]) ? $context["t1o_snapchat_box_subtitle"] : null);
            echo "</p>
  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".snapchat_wrapper\").hover(function(){            
        \$(\".snapchat_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );       
        },
  function(){            
        \$(\".snapchat_wrapper\").stop(true, false).animate({right: \"-320\" }, 800, 'easeInQuint' );      
        },1000);    
  }); 
  </script>
</div>
</div>
";
        }
        // line 1402
        echo "
";
        // line 1403
        if ((((isset($context["t1o_video_box_status"]) ? $context["t1o_video_box_status"] : null) == 1) && (isset($context["t1o_video_box_content"]) ? $context["t1o_video_box_content"] : null))) {
            echo "  
<div class=\"video_box_wrapper hidden-xs\">
<div id=\"video_box_icon\"><i class=\"fa fa-youtube-play\"></i></div>
<div class=\"video_box\">
  ";
            // line 1407
            echo (isset($context["t1o_video_box_content"]) ? $context["t1o_video_box_content"] : null);
            echo "
  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".video_box_wrapper\").hover(function(){            
        \$(\".video_box_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );        
        },
  function(){            
        \$(\".video_box_wrapper\").stop(true, false).animate({right: \"-588\" }, 800, 'easeInQuint' );      
        },1000);    
  });  
  </script>
</div>
</div>
";
        }
        // line 1421
        echo "
";
        // line 1422
        if ((((isset($context["t1o_custom_box_status"]) ? $context["t1o_custom_box_status"] : null) == 1) && (isset($context["t1o_custom_box_content"]) ? $context["t1o_custom_box_content"] : null))) {
            echo " 
<div class=\"custom_box_wrapper hidden-xs\">
<div id=\"custom_box_icon\">
<i class=\"fa fa-chevron-left\"></i>
</div>
<div class=\"custom_box\">
  ";
            // line 1428
            echo (isset($context["t1o_custom_box_content"]) ? $context["t1o_custom_box_content"] : null);
            echo "
  <script type=\"text/javascript\">    
  \$(function(){        
        \$(\".custom_box_wrapper\").hover(function(){            
        \$(\".custom_box_wrapper\").stop(true, false).animate({right: \"0\" }, 800, 'easeOutQuint' );        
        },
  function(){            
        \$(\".custom_box_wrapper\").stop(true, false).animate({right: \"-335\" }, 800, 'easeInQuint' );      
        },1000);    
  }); 
  </script>
</div>
</div>
";
        }
        // line 1442
        echo "
";
        // line 1443
        if (((isset($context["t1o_top_custom_block_status"]) ? $context["t1o_top_custom_block_status"] : null) == 1)) {
            // line 1444
            echo "<div id=\"top-custom-block-wrapper\">
   <div id=\"top-custom-block-content\">
   ";
            // line 1446
            echo (isset($context["t1o_top_custom_block_content"]) ? $context["t1o_top_custom_block_content"] : null);
            echo "
   </div>
   <a id=\"open-top-custom-block\" href=\"#\">
     ";
            // line 1449
            if (((isset($context["t1o_top_custom_block_awesome_status"]) ? $context["t1o_top_custom_block_awesome_status"] : null) == 1)) {
                // line 1450
                echo "     <i class=\"fa fa-";
                echo (isset($context["t1o_top_custom_block_awesome"]) ? $context["t1o_top_custom_block_awesome"] : null);
                echo "\"></i>
     ";
            }
            // line 1452
            echo "     ";
            echo $this->getAttribute((isset($context["t1o_top_custom_block_title"]) ? $context["t1o_top_custom_block_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "
     ";
            // line 1453
            if (((isset($context["t1o_top_custom_block_awesome_status"]) ? $context["t1o_top_custom_block_awesome_status"] : null) == 1)) {
                // line 1454
                echo "     <i class=\"fa fa-";
                echo (isset($context["t1o_top_custom_block_awesome"]) ? $context["t1o_top_custom_block_awesome"] : null);
                echo "\"></i>
     ";
            }
            // line 1456
            echo "     <i class=\"fa fa-plus\"></i>
     <i class=\"fa fa-minus\"></i>
   </a>  
</div>
<script type=\"text/javascript\">
\$('#open-top-custom-block').click(function() {
  \$(this).toggleClass('open');
    \$('#top-custom-block-content').slideToggle('fast');
\treturn false;
  });
</script>
";
        }
        // line 1468
        echo "
<div class=\"sidebar-opacity\"></div>
<div id=\"wrapper\" class=\"wrapper ";
        // line 1470
        echo (isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null);
        echo "\">

";
        // line 1472
        if (((isset($context["t1o_news_status"]) ? $context["t1o_news_status"] : null) == 1)) {
            // line 1473
            echo "<div id=\"top-news-wrapper\">
   <div id=\"top-news-content\" class=\"container\">
      <span id=\"top-news\"><i class=\"fa fa-list-alt\"></i> ";
            // line 1475
            echo $this->getAttribute((isset($context["t1o_text_news"]) ? $context["t1o_text_news"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo ":</span>
      <ul id=\"ticker\">
      ";
            // line 1477
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 10));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 1478
                echo "      ";
                if (($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), "status", array()) == 1)) {
                    // line 1479
                    echo "      ";
                    if ((($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array()) != "") || ($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                        // line 1480
                        echo "        <li>
\t\t  <a href=\"";
                        // line 1481
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), "url", array());
                        echo "\">
          ";
                        // line 1482
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                        echo "
          </a>
        </li>
      ";
                    }
                    // line 1486
                    echo "      ";
                }
                // line 1487
                echo "      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1488
            echo "      </ul> 
   </div> 
</div>
";
        }
        // line 1492
        echo "
<header class=\"";
        // line 1493
        echo (isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null);
        echo "\">

<nav id=\"top\">
  <div class=\"container\"> 
    <div class=\"top-links nav pull-left\">
      <ul class=\"list-inline\">
        ";
        // line 1499
        if (((isset($context["t1o_top_bar_welcome_status"]) ? $context["t1o_top_bar_welcome_status"] : null) == 1)) {
            // line 1500
            echo "        <li class=\"hidden-xs hidden-sm\">
        ";
            // line 1501
            if (((isset($context["t1o_top_bar_welcome_awesome"]) ? $context["t1o_top_bar_welcome_awesome"] : null) != "")) {
                // line 1502
                echo "          <i class=\"fa fa-";
                echo (isset($context["t1o_top_bar_welcome_awesome"]) ? $context["t1o_top_bar_welcome_awesome"] : null);
                echo "\"></i>
        ";
            } else {
                // line 1504
                echo "          <i class=\"fa fa-flag\"></i>
\t    ";
            }
            // line 1506
            echo "        <span>";
            echo $this->getAttribute((isset($context["t1o_top_bar_welcome"]) ? $context["t1o_top_bar_welcome"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</span></li>
        ";
        }
        // line 1508
        echo "        <li class=\"hidden-xs\"><a href=\"";
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\"><i class=\"fa fa-phone\"></i> <span>";
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "</span></a></li>
        <li><a href=\"";
        // line 1509
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\"><i class=\"fa fa-envelope\"></i> <span class=\"hidden-xs\">";
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "</span></a></li> 
      </ul>
    </div>
    
    ";
        // line 1513
        echo (isset($context["currency"]) ? $context["currency"] : null);
        echo "
    ";
        // line 1514
        echo (isset($context["language"]) ? $context["language"] : null);
        echo "
    
    <div class=\"top-links nav pull-right\">
    <ul class=\"list-inline\">
        <li class=\"dropdown\"><a href=\"";
        // line 1518
        echo (isset($context["account"]) ? $context["account"] : null);
        echo "\" title=\"";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-user\"></i> <span class=\"hidden-xs hidden-sm\">";
        echo (isset($context["text_account"]) ? $context["text_account"] : null);
        echo "</span> <i class=\"fa fa-angle-down\"></i></a>
          <ul class=\"dropdown-menu dropdown-menu-right my-account-dropdown-menu\">
            ";
        // line 1520
        if ((isset($context["logged"]) ? $context["logged"] : null)) {
            // line 1521
            echo "            <li class=\"col-xs-6\"><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\"><i class=\"fa fa-cog\"></i><br>";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>
            <li class=\"col-xs-6\"><a href=\"";
            // line 1522
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\"><i class=\"fa fa-inbox\"></i><br>";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>
            <li class=\"col-xs-6\"><a href=\"";
            // line 1523
            echo (isset($context["transaction"]) ? $context["transaction"] : null);
            echo "\"><i class=\"fa fa-credit-card\"></i><br>";
            echo (isset($context["text_transaction"]) ? $context["text_transaction"] : null);
            echo "</a></li>
            <li class=\"col-xs-6\"><a href=\"";
            // line 1524
            echo (isset($context["download"]) ? $context["download"] : null);
            echo "\"><i class=\"fa fa-download\"></i><br>";
            echo (isset($context["text_download"]) ? $context["text_download"] : null);
            echo "</a></li>
            <li class=\"logout\"><a href=\"";
            // line 1525
            echo (isset($context["logout"]) ? $context["logout"] : null);
            echo "\">";
            echo (isset($context["text_logout"]) ? $context["text_logout"] : null);
            echo "</a></li>
            ";
        } else {
            // line 1527
            echo "            <li class=\"col-xs-6 register\"><a href=\"";
            echo (isset($context["register"]) ? $context["register"] : null);
            echo "\"><i class=\"fa fa-pencil\"></i><br>";
            echo (isset($context["text_register"]) ? $context["text_register"] : null);
            echo "</a></li>
            <li class=\"col-xs-6\"><a href=\"";
            // line 1528
            echo (isset($context["login"]) ? $context["login"] : null);
            echo "\"><i class=\"fa fa-user\"></i><br>";
            echo (isset($context["text_login"]) ? $context["text_login"] : null);
            echo "</a></li>
            ";
        }
        // line 1530
        echo "          </ul>
        </li>
        <li><a href=\"";
        // line 1532
        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
        echo "\" id=\"wishlist-total\" title=\"";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "\"><i class=\"fa fa-heart\"></i> <span class=\"hidden-xs hidden-sm\">";
        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
        echo "</span></a></li>
        ";
        // line 1533
        if (((isset($context["t1o_top_bar_cart_link_status"]) ? $context["t1o_top_bar_cart_link_status"] : null) == 1)) {
            // line 1534
            echo "        <li><a href=\"";
            echo (isset($context["shopping_cart"]) ? $context["shopping_cart"] : null);
            echo "\" title=\"";
            echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
            echo "\"><i class=\"fa fa-shopping-cart hidden-md hidden-lg\"></i> <span class=\"hidden-xs hidden-sm\">";
            echo (isset($context["text_shopping_cart"]) ? $context["text_shopping_cart"] : null);
            echo "</span></a></li>
        ";
        }
        // line 1536
        echo "        <!--<li><a href=\"";
        echo (isset($context["checkout"]) ? $context["checkout"] : null);
        echo "\" title=\"";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "\"><i class=\"fa fa-share hidden-md hidden-lg\"></i> <span class=\"hidden-xs hidden-sm\">";
        echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
        echo "</span></a></li>-->      
      </ul>
    </div>
    </div>
</nav>
  
  <div class=\"container\">
    <div class=\"row\">
    
      <div class=\"col-sm-12 header-items\">
      
      <div class=\"dropdown contact-block-wrapper\">
      <a data-toggle=\"dropdown\" class=\"btn dropdown-toggle contact-block hidden-xs hidden-sm\">
      <div id=\"contact-block\" class=\"buttons-header\" data-toggle=\"tooltip\" title=\"";
        // line 1549
        echo $this->getAttribute((isset($context["t1o_text_contact_us"]) ? $context["t1o_text_contact_us"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "\">
        <div class=\"button-i\"><i class=\"fa fa-envelope\"></i></div>
      </div>
      </a>
      <ul class=\"dropdown-menu pull-right\">
        <li>
          <div class=\"contact-title\">";
        // line 1555
        echo $this->getAttribute((isset($context["t1o_text_contact_us"]) ? $context["t1o_text_contact_us"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "</div>
        </li>
        <li>
          <div class=\"contact-details\">
          
          <div class=\"row\">
          <div class=\"col-sm-6\">
          <span>";
        // line 1562
        echo $this->getAttribute((isset($context["t1o_text_menu_contact_tel"]) ? $context["t1o_text_menu_contact_tel"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "</span><br>
          ";
        // line 1563
        echo (isset($context["telephone"]) ? $context["telephone"] : null);
        echo "
          </div>
          <div class=\"col-sm-6\">
          <span>";
        // line 1566
        echo $this->getAttribute((isset($context["t1o_text_menu_contact_email"]) ? $context["t1o_text_menu_contact_email"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "</span><br>
          ";
        // line 1567
        echo (isset($context["email"]) ? $context["email"] : null);
        echo "
          </div>
          </div>
          
          <div class=\"row\">
          <div class=\"col-sm-6\">
          <span>";
        // line 1573
        echo $this->getAttribute((isset($context["t1o_text_menu_contact_address"]) ? $context["t1o_text_menu_contact_address"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "</span><br>
          ";
        // line 1574
        echo (isset($context["address"]) ? $context["address"] : null);
        echo "
          </div>
          <div class=\"col-sm-6\">
          ";
        // line 1577
        if ((isset($context["open"]) ? $context["open"] : null)) {
            // line 1578
            echo "          <span>";
            echo $this->getAttribute((isset($context["t1o_text_menu_contact_hours"]) ? $context["t1o_text_menu_contact_hours"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</span><br>
          ";
            // line 1579
            echo (isset($context["open"]) ? $context["open"] : null);
            echo "
          ";
        }
        // line 1581
        echo "          </div>
          </div>

          ";
        // line 1584
        if ((isset($context["comment"]) ? $context["comment"] : null)) {
            // line 1585
            echo "          <div class=\"row\">
          <div class=\"col-sm-12 comment\">
          ";
            // line 1587
            echo (isset($context["comment"]) ? $context["comment"] : null);
            echo "
          </div>
          </div>
          ";
        }
        // line 1591
        echo "          
          </div>
        </li>
        <li>
        <div class=\"contact-links\"><a href=\"https://maps.google.com/maps?q=";
        // line 1595
        echo twig_urlencode_filter((isset($context["geocode"]) ? $context["geocode"] : null));
        echo "&hl=";
        echo (isset($context["geocode_hl"]) ? $context["geocode_hl"] : null);
        echo "&t=m&z=15\" class=\"btn btn-xs btn-default popup-gmaps\" target=\"_blank\">";
        echo (isset($context["button_map"]) ? $context["button_map"] : null);
        echo "</a>&nbsp;&nbsp;&nbsp;<a href=\"";
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\" class=\"btn btn-xs btn-primary\">";
        echo $this->getAttribute((isset($context["t1o_text_menu_contact_form"]) ? $context["t1o_text_menu_contact_form"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "</a></div>
        </li>
      </ul>
      </div>
      <script>
      \$(document).ready(function() {
        \$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
          disableOn: 700,
          type: 'iframe',
          mainClass: 'mfp-fade',
          removalDelay: 160,
          preloader: false,
          fixedContentPos: false
        });
      });
\t  </script>
  
     
      <div class=\"dropdown information-block-wrapper\">      
      <a data-toggle=\"dropdown\" class=\"btn dropdown-toggle information-block hidden-xs hidden-sm\">
      <div id=\"information-block\" class=\"buttons-header\" data-toggle=\"tooltip\" title=\"";
        // line 1615
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "\">
        <div class=\"button-i\"><i class=\"fa fa-bars\"></i></div>
      </div>
      </a>
      <ul class=\"dropdown-menu pull-right\">
        <li>
          <div class=\"information-title\">";
        // line 1621
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "</div>
        </li>
        <li>
          <div class=\"information-details\">
          
          <div class=\"row\">
          <div class=\"col-sm-4\">
          <span>";
        // line 1628
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "</span><br>
          <ul>
          ";
        // line 1630
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            echo " 
          <li><a href=\"";
            // line 1631
            echo $this->getAttribute($context["information"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["information"], "title", array());
            echo "</a></li>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1633
        echo "          </ul>
          </div>
          <div class=\"col-sm-4\">
          <span>";
        // line 1636
        echo (isset($context["text_service"]) ? $context["text_service"] : null);
        echo "</span><br>
          <ul>
          ";
        // line 1638
        if (((isset($context["t1o_i_c_2_1_status"]) ? $context["t1o_i_c_2_1_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["contact"]) ? $context["contact"] : null);
            echo "\">";
            echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
            echo "</a></li>";
        }
        // line 1639
        echo "          ";
        if (((isset($context["t1o_i_c_2_2_status"]) ? $context["t1o_i_c_2_2_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["account"]) ? $context["account"] : null);
            echo "\">";
            echo (isset($context["text_account"]) ? $context["text_account"] : null);
            echo "</a></li>";
        }
        // line 1640
        echo "          ";
        if (((isset($context["t1o_i_c_2_3_status"]) ? $context["t1o_i_c_2_3_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["return"]) ? $context["return"] : null);
            echo "\">";
            echo (isset($context["text_return"]) ? $context["text_return"] : null);
            echo "</a></li>";
        }
        // line 1641
        echo "          ";
        if (((isset($context["t1o_i_c_2_4_status"]) ? $context["t1o_i_c_2_4_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["order"]) ? $context["order"] : null);
            echo "\">";
            echo (isset($context["text_order"]) ? $context["text_order"] : null);
            echo "</a></li>";
        }
        // line 1642
        echo "          ";
        if (((isset($context["t1o_i_c_2_5_status"]) ? $context["t1o_i_c_2_5_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
            echo "\">";
            echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
            echo "</a></li>";
        }
        // line 1643
        echo "          </ul>
          </div>
          <div class=\"col-sm-4\">
          <span>";
        // line 1646
        echo (isset($context["text_extra"]) ? $context["text_extra"] : null);
        echo "</span><br>
          <ul>
          ";
        // line 1648
        if (((isset($context["t1o_i_c_3_1_status"]) ? $context["t1o_i_c_3_1_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["t1o_text_menu_brands"]) ? $context["t1o_text_menu_brands"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a></li>";
        }
        // line 1649
        echo "          ";
        if (((isset($context["t1o_i_c_3_2_status"]) ? $context["t1o_i_c_3_2_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["voucher"]) ? $context["voucher"] : null);
            echo "\">";
            echo (isset($context["text_voucher"]) ? $context["text_voucher"] : null);
            echo "</a></li>";
        }
        // line 1650
        echo "          ";
        if (((isset($context["t1o_i_c_3_3_status"]) ? $context["t1o_i_c_3_3_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["affiliate"]) ? $context["affiliate"] : null);
            echo "\">";
            echo (isset($context["text_affiliate"]) ? $context["text_affiliate"] : null);
            echo "</a></li>";
        }
        // line 1651
        echo "          ";
        if (((isset($context["t1o_i_c_3_4_status"]) ? $context["t1o_i_c_3_4_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["special"]) ? $context["special"] : null);
            echo "\">";
            echo (isset($context["text_special"]) ? $context["text_special"] : null);
            echo "</a></li>";
        }
        // line 1652
        echo "          ";
        if (((isset($context["t1o_i_c_3_5_status"]) ? $context["t1o_i_c_3_5_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["newsletter"]) ? $context["newsletter"] : null);
            echo "\">";
            echo (isset($context["text_newsletter"]) ? $context["text_newsletter"] : null);
            echo "</a></li>";
        }
        // line 1653
        echo "          ";
        if (((isset($context["t1o_i_c_3_6_status"]) ? $context["t1o_i_c_3_6_status"] : null) == 1)) {
            echo "<li><a href=\"";
            echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
            echo "\">";
            echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
            echo "</a></li>";
        }
        // line 1654
        echo "          </ul>
          </div>
          </div>
          
          </div>
        </li>
      </ul>
      </div>

        <div id=\"logo\">
        <div id=\"logo-table\">
        <div id=\"logo-table-cell\">
          ";
        // line 1666
        if ((isset($context["logo"]) ? $context["logo"] : null)) {
            // line 1667
            echo "          <a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\"><img src=\"";
            echo (isset($context["logo"]) ? $context["logo"] : null);
            echo "\" title=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" alt=\"";
            echo (isset($context["name"]) ? $context["name"] : null);
            echo "\" /></a>
          ";
        } else {
            // line 1669
            echo "          <div class=\"logo-creator\"><a href=\"";
            echo (isset($context["home"]) ? $context["home"] : null);
            echo "\">";
            if (((isset($context["t1o_text_logo_awesome_status"]) ? $context["t1o_text_logo_awesome_status"] : null) == 1)) {
                echo "<i class=\"fa fa-";
                echo (isset($context["t1o_text_logo_awesome"]) ? $context["t1o_text_logo_awesome"] : null);
                echo "\"></i> ";
            }
            echo $this->getAttribute((isset($context["t1o_text_logo"]) ? $context["t1o_text_logo"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a></div>
          ";
        }
        // line 1671
        echo "        </div>
        </div>
        </div>
   
      ";
        // line 1675
        if ((((isset($context["t1o_header_custom_block_1_status"]) ? $context["t1o_header_custom_block_1_status"] : null) == 1) && ((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-2"))) {
            echo " 
        <div id=\"header-custom-box-wrapper\" class=\"btn hidden-xs hidden-sm hidden-md\">  
        <div id=\"header-custom-box-content\">      
        ";
            // line 1678
            echo (isset($context["t1o_header_custom_block_1_content"]) ? $context["t1o_header_custom_block_1_content"] : null);
            echo "
        </div>
        </div>
      ";
        }
        // line 1682
        echo "
      ";
        // line 1683
        echo (isset($context["cart"]) ? $context["cart"] : null);
        echo "
      ";
        // line 1684
        echo (isset($context["search"]) ? $context["search"] : null);
        echo "
      
      <a href=\"#menu-mobile-toggle\" class=\"btn menu-mobile-block\" id=\"menu-mobile-toggle\">
      <div id=\"menu-mobile-block\" class=\"buttons-header hidden-md hidden-lg\" data-toggle=\"tooltip\" title=\"";
        // line 1687
        echo $this->getAttribute((isset($context["t1o_text_menu_menu"]) ? $context["t1o_text_menu_menu"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "\">
        <div class=\"button-i\"><i class=\"fa fa-bars\"></i></div>
      </div>
      </a>
      <script>
        \$(\"#menu-mobile-toggle\").click(function(e) {
          e.preventDefault();
          \$(\"#wrapper\").toggleClass(\"menu-toggled\");
\t\t  \$(\".sidebar-opacity\").toggleClass(\"menu-toggled\");
        });
      </script>
      
      </div>
      
    </div>
  </div>


";
        // line 1705
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) != "header-style-3")) {
            // line 1706
            echo "
";
            // line 1707
            if (((isset($context["t1o_header_fixed_header_status"]) ? $context["t1o_header_fixed_header_status"] : null) == 1)) {
                // line 1708
                echo "<script>
  \$(window).load(function(){
    \$(\"#menu\").sticky({ topSpacing: 0 });
  });
</script>
";
            }
            // line 1714
            echo "
<nav id=\"menu\" class=\"navbar hidden-sm\">
      
  <div class=\"container\">
    <div class=\"collapse navbar-collapse navbar-ex1-collapse\">
      
      <ul class=\"main-menu menu flex\">

        <!-- Home Page Link -->
        ";
            // line 1723
            if (((isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null) != "dontshow")) {
                // line 1724
                echo "        <li id=\"homepage\" class=\"homepage-";
                echo (isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null);
                echo "\"><a href=\"";
                echo (isset($context["home"]) ? $context["home"] : null);
                echo "\"><i class=\"fa fa-home\"></i> <span>";
                echo (isset($context["text_home"]) ? $context["text_home"] : null);
                echo "</span></a></li>
        ";
            }
            // line 1725
            echo "  
 
        ";
            // line 1727
            if ((isset($context["categories"]) ? $context["categories"] : null)) {
                // line 1728
                echo "        ";
                if (((isset($context["t1o_menu_categories_status"]) ? $context["t1o_menu_categories_status"] : null) == 1)) {
                    // line 1729
                    echo "        
        ";
                    // line 1730
                    if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == 1)) {
                        echo "      
        <!-- Categories OpenCart Style -->
        ";
                        // line 1732
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 1733
                            echo "        ";
                            if ($this->getAttribute($context["category"], "children", array())) {
                                // line 1734
                                echo "        <li class=\"menu_oc\">
        <a href=\"";
                                // line 1735
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo " <i class=\"fa fa-sort-down\"></i></a>
          <div class=\"dropdown-menus\" ";
                                // line 1736
                                if ((((isset($context["t1o_menu_main_category_icon_status"]) ? $context["t1o_menu_main_category_icon_status"] : null) == 1) && $this->getAttribute($context["category"], "thumbbg", array()))) {
                                    echo "style=\"background-image: url(";
                                    echo $this->getAttribute($context["category"], "thumbbg", array());
                                    echo "); padding-right: ";
                                    echo (isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null);
                                    echo "px; padding-bottom: ";
                                    echo (isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null);
                                    echo "px;\"";
                                }
                                echo ">
              ";
                                // line 1737
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array()))));
                                foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                    // line 1738
                                    echo "              <ul class=\"list-unstyled\">
                ";
                                    // line 1739
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable($context["children"]);
                                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                        // line 1740
                                        echo "\t\t\t      \t<li><a href=\"";
                                        echo $this->getAttribute($context["child"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["child"], "name", array());
                                        echo "</a>
                    
                    ";
                                        // line 1742
                                        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == 1)) {
                                            // line 1743
                                            echo "                    ";
                                            if ($this->getAttribute($context["child"], "children_level_2", array())) {
                                                // line 1744
                                                echo "                    <div class=\"dropdown-menus\">
\t\t              <ul class=\"list-unstyled\">
\t\t\t            ";
                                                // line 1746
                                                $context['_parent'] = $context;
                                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                                foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                                    // line 1747
                                                    echo "\t\t\t\t          <li><a href=\"";
                                                    echo $this->getAttribute($context["child2"], "href", array());
                                                    echo "\">";
                                                    echo $this->getAttribute($context["child2"], "name", array());
                                                    echo "</a></li>
\t\t\t            ";
                                                }
                                                $_parent = $context['_parent'];
                                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                                $context = array_intersect_key($context, $_parent) + $_parent;
                                                // line 1749
                                                echo "\t\t\t          </ul>
\t\t            </div>
                    ";
                                            }
                                            // line 1752
                                            echo "                    ";
                                        }
                                        // line 1753
                                        echo "                    
\t\t\t        </li>
                ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 1756
                                    echo "              </ul>
              ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 1758
                                echo "            <a href=\"";
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\" class=\"see-all dropdown-highlight\">";
                                echo (isset($context["text_all"]) ? $context["text_all"] : null);
                                echo " ";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a> 
          </div>
        </li>
        ";
                            } else {
                                // line 1762
                                echo "        <li class=\"menu_oc\"><a href=\"";
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a></li>
        ";
                            }
                            // line 1764
                            echo "        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        echo "  
        <!-- Categories OpenCart Style - END -->  
        ";
                    }
                    // line 1767
                    echo "        
        ";
                    // line 1768
                    if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == 2)) {
                        echo "     
        <!-- Categories Vertical Style -->    
        <li id=\"menu_ver\">
        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> ";
                        // line 1771
                        echo $this->getAttribute((isset($context["t1o_text_menu_categories"]) ? $context["t1o_text_menu_categories"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</a>
        <div class=\"dropdown-menus\">
        <ul class=\"list-unstyled\">
        ";
                        // line 1774
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 1775
                            echo "        ";
                            if ($this->getAttribute($context["category"], "children", array())) {
                                // line 1776
                                echo "        <li>
        <a href=\"";
                                // line 1777
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a>
        <div class=\"dropdown-menus\">  
        ";
                                // line 1779
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array()))));
                                foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                    // line 1780
                                    echo "        <ul class=\"list-unstyled\" ";
                                    if ((((isset($context["t1o_menu_main_category_icon_status"]) ? $context["t1o_menu_main_category_icon_status"] : null) == 1) && $this->getAttribute($context["category"], "thumbbg", array()))) {
                                        echo "style=\"background-image: url(";
                                        echo $this->getAttribute($context["category"], "thumbbg", array());
                                        echo "); padding-right: ";
                                        echo (isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null);
                                        echo "px; padding-bottom: ";
                                        echo (isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null);
                                        echo "px;\"";
                                    }
                                    echo ">
          ";
                                    // line 1781
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable($context["children"]);
                                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                        // line 1782
                                        echo "          <li><a href=\"";
                                        echo $this->getAttribute($context["child"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["child"], "name", array());
                                        echo "</a>
          
          ";
                                        // line 1784
                                        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == 1)) {
                                            // line 1785
                                            echo "          ";
                                            if ($this->getAttribute($context["child"], "children_level_2", array())) {
                                                // line 1786
                                                echo "          <div class=\"dropdown-menus\">
\t\t    <ul class=\"list-unstyled\">
\t\t\t  ";
                                                // line 1788
                                                $context['_parent'] = $context;
                                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                                foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                                    // line 1789
                                                    echo "\t\t\t\t<li><a href=\"";
                                                    echo $this->getAttribute($context["child2"], "href", array());
                                                    echo "\">";
                                                    echo $this->getAttribute($context["child2"], "name", array());
                                                    echo "</a></li>
\t\t\t  ";
                                                }
                                                $_parent = $context['_parent'];
                                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                                $context = array_intersect_key($context, $_parent) + $_parent;
                                                // line 1791
                                                echo "\t\t\t</ul>
\t\t  </div>
          ";
                                            }
                                            // line 1794
                                            echo "          ";
                                        }
                                        // line 1795
                                        echo "          
\t\t  </li>
          ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 1797
                                    echo " 
        </ul>
        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 1799
                                echo " 
        </div>
        </li>
        ";
                            } else {
                                // line 1803
                                echo "        <li><a href=\"";
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a></li>
        ";
                            }
                            // line 1805
                            echo "        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 1806
                        echo "        </ul>
        </div>
        </li>
        <!-- Categories Vertical Style - END -->  
        ";
                    }
                    // line 1811
                    echo "        
        ";
                    // line 1812
                    if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == 5)) {
                        echo "      
        <!-- Categories Vertical 2 Style -->    
        <li id=\"menu_ver_2\">
        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> ";
                        // line 1815
                        echo $this->getAttribute((isset($context["t1o_text_menu_categories"]) ? $context["t1o_text_menu_categories"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</a>
        <div class=\"dropdown-menus\">
        <ul class=\"list-unstyled\">
        ";
                        // line 1818
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 1819
                            echo "        ";
                            if ($this->getAttribute($context["category"], "children", array())) {
                                // line 1820
                                echo "        <li>
        <a href=\"";
                                // line 1821
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a>
        <div class=\"dropdown-menus\">  
        ";
                                // line 1823
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array()))));
                                foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                    // line 1824
                                    echo "        <ul class=\"list-unstyled\" ";
                                    if ((((isset($context["t1o_menu_main_category_icon_status"]) ? $context["t1o_menu_main_category_icon_status"] : null) == 1) && $this->getAttribute($context["category"], "thumbbg", array()))) {
                                        echo "style=\"background-image: url(";
                                        echo $this->getAttribute($context["category"], "thumbbg", array());
                                        echo ");\"";
                                    }
                                    echo ">
          ";
                                    // line 1825
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable($context["children"]);
                                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                        // line 1826
                                        echo "          <li><a href=\"";
                                        echo $this->getAttribute($context["child"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["child"], "name", array());
                                        echo "</a>
          
          ";
                                        // line 1828
                                        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == 1)) {
                                            // line 1829
                                            echo "          ";
                                            if ($this->getAttribute($context["child"], "children_level_2", array())) {
                                                // line 1830
                                                echo "          <div class=\"dropdown-menus\">
\t\t    <ul class=\"list-unstyled\">
\t\t\t  ";
                                                // line 1832
                                                $context['_parent'] = $context;
                                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                                foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                                    // line 1833
                                                    echo "\t\t\t\t<li><a href=\"";
                                                    echo $this->getAttribute($context["child2"], "href", array());
                                                    echo "\">";
                                                    echo $this->getAttribute($context["child2"], "name", array());
                                                    echo "</a></li>
\t\t\t  ";
                                                }
                                                $_parent = $context['_parent'];
                                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                                $context = array_intersect_key($context, $_parent) + $_parent;
                                                // line 1835
                                                echo "\t\t\t</ul>
\t\t  </div>
          ";
                                            }
                                            // line 1838
                                            echo "          ";
                                        }
                                        // line 1839
                                        echo "
\t\t  </li>
          ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 1841
                                    echo " 
        </ul>
        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 1843
                                echo " 
        </div>
        </li>
        ";
                            } else {
                                // line 1847
                                echo "        <li><a href=\"";
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a></li>
        ";
                            }
                            // line 1849
                            echo "        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 1850
                        echo "        </ul>
        </div>
        </li>
        <!-- Categories Vertical 2 Style - END -->  
        ";
                    }
                    // line 1855
                    echo "        
        ";
                    // line 1856
                    if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == 3)) {
                        echo "      
        <!-- Categories Horizontal Style -->
        <li id=\"menu_hor\">
        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> ";
                        // line 1859
                        echo $this->getAttribute((isset($context["t1o_text_menu_categories"]) ? $context["t1o_text_menu_categories"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</a>
        <div class=\"dropdown-menus col-sm-12\">
        <div class=\"container\">
        
        ";
                        // line 1863
                        if (((isset($context["t1o_menu_categories_custom_block_left_status"]) ? $context["t1o_menu_categories_custom_block_left_status"] : null) == 1)) {
                            // line 1864
                            echo "        <div id=\"menu-categories-custom-block-left-wrapper\" class=\"col-sm-3 menu-categories-custom-block-side\">
           ";
                            // line 1865
                            echo (isset($context["t1o_menu_categories_custom_block_left_content"]) ? $context["t1o_menu_categories_custom_block_left_content"] : null);
                            echo "
        </div>
        ";
                        }
                        // line 1868
                        echo "
        ";
                        // line 1869
                        if ((((isset($context["t1o_menu_categories_custom_block_left_status"]) ? $context["t1o_menu_categories_custom_block_left_status"] : null) == 1) && ((isset($context["t1o_menu_categories_custom_block_right_status"]) ? $context["t1o_menu_categories_custom_block_right_status"] : null) == 1))) {
                            // line 1870
                            echo "\t      <div class=\"col-sm-6 menu-hor-categories-wrapper-6\">
        ";
                        } elseif (((                        // line 1871
(isset($context["t1o_menu_categories_custom_block_left_status"]) ? $context["t1o_menu_categories_custom_block_left_status"] : null) == 1) || ((isset($context["t1o_menu_categories_custom_block_right_status"]) ? $context["t1o_menu_categories_custom_block_right_status"] : null) == 1))) {
                            echo " 
          <div class=\"col-sm-9 menu-hor-categories-wrapper-9\">
        ";
                        } else {
                            // line 1874
                            echo "          <div class=\"col-sm-12 menu-hor-categories-wrapper-12\">
        ";
                        }
                        // line 1876
                        echo "        <div class=\"menu-hor-categories\">
        ";
                        // line 1877
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 1878
                            echo "        <ul class=\"list-unstyled ";
                            echo (isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null);
                            echo "\">
        ";
                            // line 1879
                            if ($this->getAttribute($context["category"], "children", array())) {
                                // line 1880
                                echo "        <li class=\"main-cat\">
        <a href=\"";
                                // line 1881
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a>
        ";
                                // line 1882
                                if ((((isset($context["t1o_menu_main_category_icon_status"]) ? $context["t1o_menu_main_category_icon_status"] : null) == 1) && $this->getAttribute($context["category"], "thumbbg", array()))) {
                                    // line 1883
                                    echo "        <a href=\"";
                                    echo $this->getAttribute($context["category"], "href", array());
                                    echo "\"><img src=\"";
                                    echo $this->getAttribute($context["category"], "thumbbg", array());
                                    echo "\" title=\"";
                                    echo $this->getAttribute($context["category"], "name", array());
                                    echo "\" alt=\"";
                                    echo $this->getAttribute($context["category"], "name", array());
                                    echo "\" /></a>
        ";
                                }
                                // line 1885
                                echo "        <div>   
        ";
                                // line 1886
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array()))));
                                foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                    // line 1887
                                    echo "        <ul class=\"list-unstyled\">
          ";
                                    // line 1888
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable($context["children"]);
                                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                        // line 1889
                                        echo "          <li class=\"sub-cat\"><a href=\"";
                                        echo $this->getAttribute($context["child"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["child"], "name", array());
                                        echo "</a></li>
            
          ";
                                        // line 1891
                                        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == 1)) {
                                            // line 1892
                                            echo "          ";
                                            if ($this->getAttribute($context["child"], "children_level_2", array())) {
                                                // line 1893
                                                echo "          ";
                                                $context['_parent'] = $context;
                                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                                foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                                    // line 1894
                                                    echo "          <li class=\"sub-cat sub-sub-cat\"><a href=\"";
                                                    echo $this->getAttribute($context["child2"], "href", array());
                                                    echo "\">";
                                                    echo $this->getAttribute($context["child2"], "name", array());
                                                    echo "</a></li>
          ";
                                                }
                                                $_parent = $context['_parent'];
                                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                                $context = array_intersect_key($context, $_parent) + $_parent;
                                                // line 1896
                                                echo "          ";
                                            }
                                            // line 1897
                                            echo "          ";
                                        }
                                        // line 1898
                                        echo "
          ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 1899
                                    echo " 
        </ul>
        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 1901
                                echo " 
        </div>
        </li>
        ";
                            } else {
                                // line 1905
                                echo "        <li class=\"main-cat\"><a href=\"";
                                echo $this->getAttribute($context["category"], "href", array());
                                echo "\">";
                                echo $this->getAttribute($context["category"], "name", array());
                                echo "</a></li>
        ";
                            }
                            // line 1907
                            echo "        </ul>
        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 1909
                        echo "        </div>
        </div>
        
        ";
                        // line 1912
                        if (((isset($context["t1o_menu_categories_custom_block_right_status"]) ? $context["t1o_menu_categories_custom_block_right_status"] : null) == 1)) {
                            echo " 
        <div id=\"menu-categories-custom-block-right-wrapper\" class=\"col-sm-3 menu-categories-custom-block-side\">
           ";
                            // line 1914
                            echo (isset($context["t1o_menu_categories_custom_block_right_content"]) ? $context["t1o_menu_categories_custom_block_right_content"] : null);
                            echo "
        </div>
        ";
                        }
                        // line 1917
                        echo "        
        ";
                        // line 1918
                        if (((isset($context["t1o_menu_categories_custom_block_status"]) ? $context["t1o_menu_categories_custom_block_status"] : null) == 1)) {
                            echo " 
        <div id=\"menu-categories-custom-block-bottom-wrapper\" class=\"col-sm-12\">
           ";
                            // line 1920
                            echo (isset($context["t1o_menu_categories_custom_block_content"]) ? $context["t1o_menu_categories_custom_block_content"] : null);
                            echo "
        </div>
        ";
                        }
                        // line 1923
                        echo "        
        </div>
        </div>

        </li>
        <!-- Categories Horizontal Style - END -->  
        ";
                    }
                    // line 1930
                    echo "        
        
        ";
                    // line 1932
                    if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == 4)) {
                        echo "     
        <!-- Categories Inline Style -->    
        <li id=\"menu_inline\">
        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"><i class=\"fa fa-bars\"></i> ";
                        // line 1935
                        echo $this->getAttribute((isset($context["t1o_text_menu_categories"]) ? $context["t1o_text_menu_categories"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</a>
        <div class=\"dropdown-menus col-sm-12\">
        ";
                        // line 1937
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                            // line 1938
                            echo "        <ul class=\"list-unstyled\">
        <li class=\"main-cat\">
        <a href=\"";
                            // line 1940
                            echo $this->getAttribute($context["category"], "href", array());
                            echo "\">";
                            echo $this->getAttribute($context["category"], "name", array());
                            if ($this->getAttribute($context["category"], "children", array())) {
                                echo " <i class=\"fa fa-sort-down\"></i>";
                            }
                            echo "</a>
        ";
                            // line 1941
                            if ($this->getAttribute($context["category"], "children", array())) {
                                // line 1942
                                echo "        <div class=\"dropdown-menus col-sm-12\">  
        ";
                                // line 1943
                                $context['_parent'] = $context;
                                $context['_seq'] = twig_ensure_traversable(twig_array_batch($this->getAttribute($context["category"], "children", array()), twig_length_filter($this->env, $this->getAttribute($context["category"], "children", array()))));
                                foreach ($context['_seq'] as $context["_key"] => $context["children"]) {
                                    // line 1944
                                    echo "        <ul class=\"list-unstyled\">
          ";
                                    // line 1945
                                    $context['_parent'] = $context;
                                    $context['_seq'] = twig_ensure_traversable($context["children"]);
                                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                                        // line 1946
                                        echo "          <li><a href=\"";
                                        echo $this->getAttribute($context["child"], "href", array());
                                        echo "\">";
                                        echo $this->getAttribute($context["child"], "name", array());
                                        echo " ";
                                        if ($this->getAttribute($context["child"], "children_level_2", array())) {
                                            echo "<i class=\"fa fa-sort-down\"></i>";
                                        }
                                        echo "</a>
               
          ";
                                        // line 1948
                                        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == 1)) {
                                            // line 1949
                                            echo "          ";
                                            if ($this->getAttribute($context["child"], "children_level_2", array())) {
                                                // line 1950
                                                echo "          <div class=\"dropdown-menus col-sm-12\">
\t\t    <ul class=\"list-unstyled\">
\t\t\t  ";
                                                // line 1952
                                                $context['_parent'] = $context;
                                                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["child"], "children_level_2", array()));
                                                foreach ($context['_seq'] as $context["_key"] => $context["child2"]) {
                                                    // line 1953
                                                    echo "\t\t\t\t<li><a href=\"";
                                                    echo $this->getAttribute($context["child2"], "href", array());
                                                    echo "\">";
                                                    echo $this->getAttribute($context["child2"], "name", array());
                                                    echo "</a></li>
\t\t\t  ";
                                                }
                                                $_parent = $context['_parent'];
                                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child2'], $context['_parent'], $context['loop']);
                                                $context = array_intersect_key($context, $_parent) + $_parent;
                                                // line 1955
                                                echo "\t\t\t</ul>
\t\t  </div>
          ";
                                            }
                                            // line 1958
                                            echo "          ";
                                        }
                                        // line 1959
                                        echo "    
\t\t  </li>
          ";
                                    }
                                    $_parent = $context['_parent'];
                                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                                    $context = array_intersect_key($context, $_parent) + $_parent;
                                    // line 1962
                                    echo "        </ul>
        ";
                                }
                                $_parent = $context['_parent'];
                                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['children'], $context['_parent'], $context['loop']);
                                $context = array_intersect_key($context, $_parent) + $_parent;
                                // line 1963
                                echo " 
        </div>
        ";
                            }
                            // line 1966
                            echo "        </li>
        </ul>
        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 1969
                        echo "        </div>
        </li>
        <!-- Categories Inline Style - END -->  
        ";
                    }
                    // line 1973
                    echo "        
        ";
                }
                // line 1974
                echo " 
        ";
            }
            // line 1976
            echo "
        <!-- Brands -->
        ";
            // line 1978
            if (((isset($context["t1o_menu_brands_status"]) ? $context["t1o_menu_brands_status"] : null) == 1)) {
                // line 1979
                echo "        <li id=\"menu_brands\">
          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 1980
                echo $this->getAttribute((isset($context["t1o_text_menu_brands"]) ? $context["t1o_text_menu_brands"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo " <i class=\"fa fa-sort-down\"></i></a>
          ";
                // line 1981
                if ((isset($context["manufacturers"]) ? $context["manufacturers"] : null)) {
                    // line 1982
                    echo "          <div class=\"dropdown-menus col-sm-12 ";
                    echo (isset($context["t1o_menu_brands_style"]) ? $context["t1o_menu_brands_style"] : null);
                    echo "\">
          <div class=\"container\">
              ";
                    // line 1984
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable((isset($context["manufacturers"]) ? $context["manufacturers"] : null));
                    foreach ($context['_seq'] as $context["_key"] => $context["manufacturer"]) {
                        // line 1985
                        echo "              <div class=\"brand-item col-sm-";
                        echo (isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null);
                        echo "\">
                <a href=\"";
                        // line 1986
                        echo $this->getAttribute($context["manufacturer"], "href", array());
                        echo "\">
                ";
                        // line 1987
                        if ($this->getAttribute($context["manufacturer"], "image", array())) {
                            // line 1988
                            echo "                <div class=\"image\"><img src=\"";
                            echo $this->getAttribute($context["manufacturer"], "image", array());
                            echo "\" title=\"";
                            echo $this->getAttribute($context["manufacturer"], "name", array());
                            echo "\" alt=\"";
                            echo $this->getAttribute($context["manufacturer"], "name", array());
                            echo "\" /></div>
                ";
                        } else {
                            // line 1990
                            echo "                <div class=\"image\"><i class=\"fa fa-camera\"></i></div>
                ";
                        }
                        // line 1992
                        echo "                <div class=\"name\">";
                        echo $this->getAttribute($context["manufacturer"], "name", array());
                        echo "</div>
                </a>  
              </div>
              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['manufacturer'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 1996
                    echo "          </div>
          </div>
          ";
                }
                // line 1999
                echo "        </li>
        ";
            }
            // line 2001
            echo "
        <!-- Custom Blocks -->
        ";
            // line 2003
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " 
        ";
                // line 2004
                if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array()) != ""))) {
                    echo "                      
        <li class=\"menu_custom_block\">        
          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                    // line 2006
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                    echo " <i class=\"fa fa-sort-down\"></i></a> 
          <div class=\"dropdown-menus col-sm-12\">
            <div class=\"container\">
                ";
                    // line 2009
                    echo $this->getAttribute((isset($context["t1o_menu_custom_block_htmlcontent"]) ? $context["t1o_menu_custom_block_htmlcontent"] : null), $context["i"], array(), "array");
                    echo "
            </div>       
          </div>  
        </li>                              
        ";
                }
                // line 2014
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2015
            echo "        
        <!-- Custom Dropdown Menus --> 
        ";
            // line 2017
            if ((((isset($context["t1o_menu_cm_status"]) ? $context["t1o_menu_cm_status"] : null) == 1) && $this->getAttribute((isset($context["t1o_menu_cm_title"]) ? $context["t1o_menu_cm_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"))) {
                echo "   
        <li class=\"menu_custom_menu\">       
          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 2019
                echo $this->getAttribute((isset($context["t1o_menu_cm_title"]) ? $context["t1o_menu_cm_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo " <i class=\"fa fa-sort-down\"></i></a>
          <div class=\"dropdown-menus\">
            <ul class=\"list-unstyled\">
              ";
                // line 2022
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 10));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 2023
                    echo "              ";
                    if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                        // line 2024
                        echo "                <li>
                  <a href=\"";
                        // line 2025
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "url", array());
                        echo "\" target=\"";
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "target", array());
                        echo "\">
\t\t      \t  ";
                        // line 2026
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                        echo "
                  </a>
                </li>
              ";
                    }
                    // line 2030
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 2031
                echo "            </ul>       
          </div>
        </li>         
        ";
            }
            // line 2035
            echo "        ";
            if ((((isset($context["t1o_menu_cm_2_status"]) ? $context["t1o_menu_cm_2_status"] : null) == 1) && $this->getAttribute((isset($context["t1o_menu_cm_2_title"]) ? $context["t1o_menu_cm_2_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"))) {
                echo "  
        <li class=\"menu_custom_menu\">       
          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 2037
                echo $this->getAttribute((isset($context["t1o_menu_cm_2_title"]) ? $context["t1o_menu_cm_2_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo " <i class=\"fa fa-sort-down\"></i></a>
          <div class=\"dropdown-menus\">
            <ul class=\"list-unstyled\">
              ";
                // line 2040
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 10));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 2041
                    echo "              ";
                    if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                        // line 2042
                        echo "                <li>
                  <a href=\"";
                        // line 2043
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "url", array());
                        echo "\" target=\"";
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "target", array());
                        echo "\">
\t\t      \t  ";
                        // line 2044
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                        echo "
                  </a>
                </li>
              ";
                    }
                    // line 2048
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 2049
                echo "            </ul>       
          </div>
        </li>         
        ";
            }
            // line 2053
            echo "        ";
            if ((((isset($context["t1o_menu_cm_3_status"]) ? $context["t1o_menu_cm_3_status"] : null) == 1) && $this->getAttribute((isset($context["t1o_menu_cm_3_title"]) ? $context["t1o_menu_cm_3_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"))) {
                echo "   
        <li class=\"menu_custom_menu\">       
          <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 2055
                echo $this->getAttribute((isset($context["t1o_menu_cm_3_title"]) ? $context["t1o_menu_cm_3_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo " <i class=\"fa fa-sort-down\"></i></a>
          <div class=\"dropdown-menus\">
            <ul class=\"list-unstyled\">
              ";
                // line 2058
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 10));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 2059
                    echo "              ";
                    if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                        // line 2060
                        echo "                <li>
                  <a href=\"";
                        // line 2061
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "url", array());
                        echo "\" target=\"";
                        echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "target", array());
                        echo "\">
\t\t      \t  ";
                        // line 2062
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                        echo "
                  </a>
                </li>
              ";
                    }
                    // line 2066
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 2067
                echo "            </ul>       
          </div>
        </li>         
        ";
            }
            // line 2071
            echo "        
        <!-- Custom Links -->
        ";
            // line 2073
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 10));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 2074
                echo "        ";
                if (((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array()) != "")) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                    echo "                        
        <li class=\"menu_links\">
          <a href=\"";
                    // line 2076
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "url", array());
                    echo "\" target=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "target", array());
                    echo "\">
            ";
                    // line 2077
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                    echo "
          </a>
        </li>                             
        ";
                }
                // line 2081
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2082
            echo "        
      </ul>
      
    </div>
  </div>
";
            // line 2087
            if (((isset($context["t1o_custom_bar_below_menu_status"]) ? $context["t1o_custom_bar_below_menu_status"] : null) == 1)) {
                echo " 
<div id=\"custom-bar-wrapper\">
   <div id=\"custom-bar-content\">
   ";
                // line 2090
                echo (isset($context["t1o_custom_bar_below_menu_content"]) ? $context["t1o_custom_bar_below_menu_content"] : null);
                echo "
   </div>
</div>
";
            }
            // line 2094
            echo "</nav>
";
        }
        // line 2096
        echo "
<script type=\"text/javascript\">
\$(document).ready(function() {
\t\$(\"#accordion-mobile\").dcAccordion({
\t\tdisableLink: false,\t
\t\tmenuClose: false,
\t\tautoClose: true,
\t\tautoExpand: true,\t\t
\t\tsaveState: false
\t});
});
</script> 
<div id=\"menu-mobile\" class=\"hidden-md hidden-lg\">
<ul class=\"accordion sidebar-nav list-group\" id=\"accordion-mobile\">  
      <li class=\"sidebar-title\">
      <a href=\"#menu-mobile-toggle-close\" id=\"menu-mobile-toggle-close\"><span aria-hidden=\"true\">&times;</span></a>";
        // line 2111
        echo $this->getAttribute((isset($context["t1o_text_menu_menu"]) ? $context["t1o_text_menu_menu"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "
      <script>
      \$(\"#menu-mobile-toggle-close, .sidebar-opacity\").click(function(e) {
        e.preventDefault();
        \$(\"#wrapper\").toggleClass(\"menu-toggled\");
\t\t\$(\".sidebar-opacity\").toggleClass(\"menu-toggled\");
      });
      </script>
    </li> 
    <li class=\"home-mobile\"><a href=\"";
        // line 2120
        echo (isset($context["home"]) ? $context["home"] : null);
        echo "\">";
        echo (isset($context["text_home"]) ? $context["text_home"] : null);
        echo "</a></li>
    ";
        // line 2121
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 2122
            echo "    <li class=\"category-mobile\">
        ";
            // line 2123
            if ($this->getAttribute($context["category"], "children", array())) {
                // line 2124
                echo "          <a href=\"";
                echo $this->getAttribute($context["category"], "href", array());
                echo "\" class=\"list-group-item\">";
                echo $this->getAttribute($context["category"], "name", array());
                echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
        ";
            } else {
                // line 2126
                echo "          <a href=\"";
                echo $this->getAttribute($context["category"], "href", array());
                echo "\" class=\"list-group-item\">";
                echo $this->getAttribute($context["category"], "name", array());
                echo "</a>
        ";
            }
            // line 2128
            echo "        ";
            if ($this->getAttribute($context["category"], "children", array())) {
                // line 2129
                echo "          <ul>
            ";
                // line 2130
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 2131
                    echo "            <li>
              <a href=\"";
                    // line 2132
                    echo $this->getAttribute($context["child"], "href", array());
                    echo "\" class=\"list-group-item\">";
                    echo $this->getAttribute($context["child"], "name", array());
                    echo "</a>
            </li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 2135
                echo "          </ul>
        ";
            }
            // line 2137
            echo "    </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2139
        echo "    ";
        if (((isset($context["t1o_menu_brands_status"]) ? $context["t1o_menu_brands_status"] : null) == 1)) {
            // line 2140
            echo "    <li class=\"menu_links\"><a href=\"";
            echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
            echo "\">";
            echo $this->getAttribute((isset($context["t1o_text_menu_brands"]) ? $context["t1o_text_menu_brands"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a></li>
    ";
        }
        // line 2142
        echo "    ";
        if ((((isset($context["t1o_menu_cm_status"]) ? $context["t1o_menu_cm_status"] : null) == 1) && $this->getAttribute((isset($context["t1o_menu_cm_title"]) ? $context["t1o_menu_cm_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"))) {
            echo "   
    <li>       
      <a href=\"#\" class=\"dropdown-toggle list-group-item\" data-toggle=\"dropdown\">";
            // line 2144
            echo $this->getAttribute((isset($context["t1o_menu_cm_title"]) ? $context["t1o_menu_cm_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
      <ul class=\"list-unstyled\">
        ";
            // line 2146
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 10));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 2147
                echo "        ";
                if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                    // line 2148
                    echo "          <li>
            <a href=\"";
                    // line 2149
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "url", array());
                    echo "\" target=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "target", array());
                    echo "\">
\t\t    ";
                    // line 2150
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                    echo "
            </a>
          </li>
        ";
                }
                // line 2154
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2155
            echo "      </ul>       
    </li>         
    ";
        }
        // line 2158
        echo "    ";
        if ((((isset($context["t1o_menu_cm_2_status"]) ? $context["t1o_menu_cm_2_status"] : null) == 1) && $this->getAttribute((isset($context["t1o_menu_cm_2_title"]) ? $context["t1o_menu_cm_2_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"))) {
            echo "   
    <li>       
      <a href=\"#\" class=\"dropdown-toggle list-group-item\" data-toggle=\"dropdown\">";
            // line 2160
            echo $this->getAttribute((isset($context["t1o_menu_cm_2_title"]) ? $context["t1o_menu_cm_2_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
      <ul class=\"list-unstyled\">
        ";
            // line 2162
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 10));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 2163
                echo "        ";
                if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                    // line 2164
                    echo "          <li>
            <a href=\"";
                    // line 2165
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "url", array());
                    echo "\" target=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "target", array());
                    echo "\">
\t\t    ";
                    // line 2166
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                    echo "
            </a>
          </li>
        ";
                }
                // line 2170
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2171
            echo "      </ul>       
    </li>         
    ";
        }
        // line 2174
        echo "    ";
        if ((((isset($context["t1o_menu_cm_3_status"]) ? $context["t1o_menu_cm_3_status"] : null) == 1) && $this->getAttribute((isset($context["t1o_menu_cm_3_title"]) ? $context["t1o_menu_cm_3_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"))) {
            echo "  
    <li>       
      <a href=\"#\" class=\"dropdown-toggle list-group-item\" data-toggle=\"dropdown\">";
            // line 2176
            echo $this->getAttribute((isset($context["t1o_menu_cm_3_title"]) ? $context["t1o_menu_cm_3_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
      <ul class=\"list-unstyled\">
        ";
            // line 2178
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 10));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 2179
                echo "        ";
                if ((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                    // line 2180
                    echo "          <li>
            <a href=\"";
                    // line 2181
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "url", array());
                    echo "\" target=\"";
                    echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "target", array());
                    echo "\">
\t\t    ";
                    // line 2182
                    echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                    echo "
            </a>
          </li>
        ";
                }
                // line 2186
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2187
            echo "      </ul>       
    </li>         
    ";
        }
        // line 2190
        echo "
    ";
        // line 2191
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2192
            echo "    ";
            if (((($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "status", array()) == 1) && ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array()) != "")) && ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "url", array()) != ""))) {
                echo "                        
    <li class=\"menu_links\">
      <a href=\"";
                // line 2194
                echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "url", array());
                echo "\" target=\"";
                echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "target", array());
                echo "\">
        ";
                // line 2195
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                echo "
      </a>
    </li>                             
    ";
            }
            // line 2199
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2200
        echo "    
    <li>
      <a href=\"#\" class=\"dropdown-toggle list-group-item\" data-toggle=\"dropdown\">";
        // line 2202
        echo (isset($context["text_information"]) ? $context["text_information"] : null);
        echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
      <ul class=\"list-unstyled\">
        ";
        // line 2204
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
            // line 2205
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["information"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["information"], "title", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2207
        echo "      </ul>
    </li>   
    <li><a href=\"";
        // line 2209
        echo (isset($context["contact"]) ? $context["contact"] : null);
        echo "\">";
        echo $this->getAttribute((isset($context["t1o_text_contact_us"]) ? $context["t1o_text_contact_us"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
        echo "</a></li>
</ul>
</div>

";
        // line 2213
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2214
            if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_labels"]) ? $context["t1o_menu_labels"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array())) {
                echo "                       
<span id=\"menu_label";
                // line 2215
                echo $context["i"];
                echo "\" class=\"menu_label\" style=\"background-color:";
                echo $this->getAttribute((isset($context["t1o_menu_labels_color"]) ? $context["t1o_menu_labels_color"] : null), $context["i"], array(), "array");
                echo ";\">
    ";
                // line 2216
                echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_labels"]) ? $context["t1o_menu_labels"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "title", array());
                echo "
</span> 
<script type=\"text/javascript\">
\$(document).ready(function(){
     \$('#menu_label";
                // line 2220
                echo $context["i"];
                echo "').prependTo(\"ul.main-menu > li:nth-child(";
                echo $context["i"];
                echo ") > a\");
});\t
</script>  
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2225
        echo "
<script type=\"text/javascript\">
\$('#menu_hor > a, #menu_ver > a, #menu_ver_2 > a, #menu_inline > a, #menu_brands > a, .menu_custom_menu > a, .menu_custom_block > a').click(function() {
  \$(this).toggleClass('open');
\treturn false;
  });
</script>

</header>";
    }

    public function getTemplateName()
    {
        return "oxy/template/common/header.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  5076 => 2225,  5063 => 2220,  5056 => 2216,  5050 => 2215,  5046 => 2214,  5042 => 2213,  5033 => 2209,  5029 => 2207,  5018 => 2205,  5014 => 2204,  5009 => 2202,  5005 => 2200,  4999 => 2199,  4992 => 2195,  4986 => 2194,  4980 => 2192,  4976 => 2191,  4973 => 2190,  4968 => 2187,  4962 => 2186,  4955 => 2182,  4949 => 2181,  4946 => 2180,  4943 => 2179,  4939 => 2178,  4934 => 2176,  4928 => 2174,  4923 => 2171,  4917 => 2170,  4910 => 2166,  4904 => 2165,  4901 => 2164,  4898 => 2163,  4894 => 2162,  4889 => 2160,  4883 => 2158,  4878 => 2155,  4872 => 2154,  4865 => 2150,  4859 => 2149,  4856 => 2148,  4853 => 2147,  4849 => 2146,  4844 => 2144,  4838 => 2142,  4830 => 2140,  4827 => 2139,  4820 => 2137,  4816 => 2135,  4805 => 2132,  4802 => 2131,  4798 => 2130,  4795 => 2129,  4792 => 2128,  4784 => 2126,  4776 => 2124,  4774 => 2123,  4771 => 2122,  4767 => 2121,  4761 => 2120,  4749 => 2111,  4732 => 2096,  4728 => 2094,  4721 => 2090,  4715 => 2087,  4708 => 2082,  4702 => 2081,  4695 => 2077,  4689 => 2076,  4683 => 2074,  4679 => 2073,  4675 => 2071,  4669 => 2067,  4663 => 2066,  4656 => 2062,  4650 => 2061,  4647 => 2060,  4644 => 2059,  4640 => 2058,  4634 => 2055,  4628 => 2053,  4622 => 2049,  4616 => 2048,  4609 => 2044,  4603 => 2043,  4600 => 2042,  4597 => 2041,  4593 => 2040,  4587 => 2037,  4581 => 2035,  4575 => 2031,  4569 => 2030,  4562 => 2026,  4556 => 2025,  4553 => 2024,  4550 => 2023,  4546 => 2022,  4540 => 2019,  4535 => 2017,  4531 => 2015,  4525 => 2014,  4517 => 2009,  4511 => 2006,  4506 => 2004,  4500 => 2003,  4496 => 2001,  4492 => 1999,  4487 => 1996,  4476 => 1992,  4472 => 1990,  4462 => 1988,  4460 => 1987,  4456 => 1986,  4451 => 1985,  4447 => 1984,  4441 => 1982,  4439 => 1981,  4435 => 1980,  4432 => 1979,  4430 => 1978,  4426 => 1976,  4422 => 1974,  4418 => 1973,  4412 => 1969,  4404 => 1966,  4399 => 1963,  4392 => 1962,  4384 => 1959,  4381 => 1958,  4376 => 1955,  4365 => 1953,  4361 => 1952,  4357 => 1950,  4354 => 1949,  4352 => 1948,  4340 => 1946,  4336 => 1945,  4333 => 1944,  4329 => 1943,  4326 => 1942,  4324 => 1941,  4315 => 1940,  4311 => 1938,  4307 => 1937,  4302 => 1935,  4296 => 1932,  4292 => 1930,  4283 => 1923,  4277 => 1920,  4272 => 1918,  4269 => 1917,  4263 => 1914,  4258 => 1912,  4253 => 1909,  4246 => 1907,  4238 => 1905,  4232 => 1901,  4224 => 1899,  4217 => 1898,  4214 => 1897,  4211 => 1896,  4200 => 1894,  4195 => 1893,  4192 => 1892,  4190 => 1891,  4182 => 1889,  4178 => 1888,  4175 => 1887,  4171 => 1886,  4168 => 1885,  4156 => 1883,  4154 => 1882,  4148 => 1881,  4145 => 1880,  4143 => 1879,  4138 => 1878,  4134 => 1877,  4131 => 1876,  4127 => 1874,  4121 => 1871,  4118 => 1870,  4116 => 1869,  4113 => 1868,  4107 => 1865,  4104 => 1864,  4102 => 1863,  4095 => 1859,  4089 => 1856,  4086 => 1855,  4079 => 1850,  4073 => 1849,  4065 => 1847,  4059 => 1843,  4051 => 1841,  4043 => 1839,  4040 => 1838,  4035 => 1835,  4024 => 1833,  4020 => 1832,  4016 => 1830,  4013 => 1829,  4011 => 1828,  4003 => 1826,  3999 => 1825,  3990 => 1824,  3986 => 1823,  3979 => 1821,  3976 => 1820,  3973 => 1819,  3969 => 1818,  3963 => 1815,  3957 => 1812,  3954 => 1811,  3947 => 1806,  3941 => 1805,  3933 => 1803,  3927 => 1799,  3919 => 1797,  3911 => 1795,  3908 => 1794,  3903 => 1791,  3892 => 1789,  3888 => 1788,  3884 => 1786,  3881 => 1785,  3879 => 1784,  3871 => 1782,  3867 => 1781,  3854 => 1780,  3850 => 1779,  3843 => 1777,  3840 => 1776,  3837 => 1775,  3833 => 1774,  3827 => 1771,  3821 => 1768,  3818 => 1767,  3808 => 1764,  3800 => 1762,  3788 => 1758,  3781 => 1756,  3773 => 1753,  3770 => 1752,  3765 => 1749,  3754 => 1747,  3750 => 1746,  3746 => 1744,  3743 => 1743,  3741 => 1742,  3733 => 1740,  3729 => 1739,  3726 => 1738,  3722 => 1737,  3710 => 1736,  3704 => 1735,  3701 => 1734,  3698 => 1733,  3694 => 1732,  3689 => 1730,  3686 => 1729,  3683 => 1728,  3681 => 1727,  3677 => 1725,  3667 => 1724,  3665 => 1723,  3654 => 1714,  3646 => 1708,  3644 => 1707,  3641 => 1706,  3639 => 1705,  3618 => 1687,  3612 => 1684,  3608 => 1683,  3605 => 1682,  3598 => 1678,  3592 => 1675,  3586 => 1671,  3573 => 1669,  3561 => 1667,  3559 => 1666,  3545 => 1654,  3536 => 1653,  3527 => 1652,  3518 => 1651,  3509 => 1650,  3500 => 1649,  3492 => 1648,  3487 => 1646,  3482 => 1643,  3473 => 1642,  3464 => 1641,  3455 => 1640,  3446 => 1639,  3438 => 1638,  3433 => 1636,  3428 => 1633,  3418 => 1631,  3412 => 1630,  3407 => 1628,  3397 => 1621,  3388 => 1615,  3357 => 1595,  3351 => 1591,  3344 => 1587,  3340 => 1585,  3338 => 1584,  3333 => 1581,  3328 => 1579,  3323 => 1578,  3321 => 1577,  3315 => 1574,  3311 => 1573,  3302 => 1567,  3298 => 1566,  3292 => 1563,  3288 => 1562,  3278 => 1555,  3269 => 1549,  3248 => 1536,  3238 => 1534,  3236 => 1533,  3228 => 1532,  3224 => 1530,  3217 => 1528,  3210 => 1527,  3203 => 1525,  3197 => 1524,  3191 => 1523,  3185 => 1522,  3178 => 1521,  3176 => 1520,  3167 => 1518,  3160 => 1514,  3156 => 1513,  3147 => 1509,  3140 => 1508,  3134 => 1506,  3130 => 1504,  3124 => 1502,  3122 => 1501,  3119 => 1500,  3117 => 1499,  3108 => 1493,  3105 => 1492,  3099 => 1488,  3093 => 1487,  3090 => 1486,  3083 => 1482,  3079 => 1481,  3076 => 1480,  3073 => 1479,  3070 => 1478,  3066 => 1477,  3061 => 1475,  3057 => 1473,  3055 => 1472,  3050 => 1470,  3046 => 1468,  3032 => 1456,  3026 => 1454,  3024 => 1453,  3019 => 1452,  3013 => 1450,  3011 => 1449,  3005 => 1446,  3001 => 1444,  2999 => 1443,  2996 => 1442,  2979 => 1428,  2970 => 1422,  2967 => 1421,  2950 => 1407,  2943 => 1403,  2940 => 1402,  2923 => 1388,  2919 => 1387,  2915 => 1386,  2908 => 1382,  2905 => 1381,  2888 => 1367,  2882 => 1363,  2880 => 1362,  2877 => 1361,  2860 => 1347,  2854 => 1343,  2852 => 1342,  2849 => 1341,  2834 => 1328,  2821 => 1325,  2819 => 1324,  2812 => 1320,  2809 => 1319,  2792 => 1305,  2777 => 1293,  2772 => 1291,  2768 => 1289,  2760 => 1287,  2756 => 1286,  2744 => 1276,  2738 => 1273,  2731 => 1269,  2725 => 1266,  2719 => 1263,  2716 => 1262,  2710 => 1260,  2708 => 1259,  2702 => 1257,  2700 => 1256,  2696 => 1255,  2693 => 1254,  2687 => 1251,  2683 => 1249,  2681 => 1248,  2676 => 1245,  2670 => 1241,  2668 => 1240,  2663 => 1237,  2656 => 1233,  2650 => 1230,  2646 => 1228,  2644 => 1227,  2634 => 1220,  2628 => 1217,  2620 => 1212,  2614 => 1209,  2606 => 1204,  2601 => 1201,  2589 => 1191,  2587 => 1190,  2554 => 1159,  2552 => 1158,  2541 => 1149,  2539 => 1148,  2536 => 1147,  2530 => 1144,  2525 => 1141,  2505 => 1124,  2501 => 1122,  2499 => 1121,  2496 => 1120,  2492 => 1118,  2486 => 1115,  2483 => 1114,  2479 => 1112,  2477 => 1111,  2473 => 1109,  2471 => 1108,  2467 => 1106,  2465 => 1105,  2453 => 1096,  2447 => 1093,  2441 => 1090,  2435 => 1087,  2429 => 1084,  2423 => 1081,  2417 => 1078,  2411 => 1075,  2407 => 1073,  2401 => 1069,  2399 => 1068,  2395 => 1067,  2390 => 1064,  2386 => 1062,  2384 => 1061,  2380 => 1059,  2374 => 1057,  2372 => 1056,  2368 => 1055,  2364 => 1054,  2359 => 1051,  2349 => 1049,  2343 => 1048,  2337 => 1044,  2333 => 1042,  2331 => 1041,  2327 => 1039,  2311 => 1025,  2309 => 1024,  2305 => 1022,  2301 => 1020,  2297 => 1018,  2295 => 1017,  2292 => 1016,  2290 => 1015,  2285 => 1012,  2281 => 1010,  2277 => 1008,  2275 => 1007,  2272 => 1006,  2263 => 999,  2261 => 998,  2255 => 994,  2249 => 990,  2247 => 989,  2244 => 988,  2240 => 986,  2234 => 984,  2232 => 983,  2228 => 982,  2224 => 981,  2219 => 978,  2215 => 976,  2208 => 973,  2206 => 972,  2202 => 971,  2198 => 970,  2191 => 966,  2185 => 963,  2175 => 956,  2171 => 955,  2166 => 952,  2160 => 948,  2158 => 947,  2152 => 943,  2150 => 942,  2144 => 938,  2142 => 937,  2136 => 933,  2134 => 932,  2128 => 928,  2126 => 927,  2114 => 917,  2108 => 915,  2100 => 910,  2096 => 909,  2088 => 904,  2084 => 903,  2078 => 899,  2074 => 897,  2070 => 895,  2068 => 894,  2064 => 893,  2060 => 892,  2054 => 888,  2050 => 886,  2046 => 884,  2044 => 883,  2040 => 882,  2036 => 881,  2028 => 876,  2022 => 872,  2018 => 870,  2014 => 868,  2012 => 867,  2008 => 866,  2004 => 865,  2000 => 864,  1996 => 863,  1990 => 859,  1986 => 857,  1982 => 855,  1980 => 854,  1974 => 851,  1968 => 848,  1960 => 843,  1954 => 839,  1950 => 837,  1946 => 835,  1944 => 834,  1938 => 831,  1932 => 828,  1924 => 822,  1920 => 820,  1916 => 818,  1914 => 817,  1910 => 815,  1906 => 813,  1902 => 811,  1900 => 810,  1895 => 808,  1891 => 807,  1887 => 806,  1883 => 805,  1879 => 804,  1875 => 803,  1871 => 802,  1867 => 801,  1863 => 800,  1859 => 799,  1855 => 798,  1851 => 796,  1845 => 792,  1843 => 791,  1838 => 789,  1834 => 787,  1830 => 785,  1828 => 784,  1824 => 783,  1820 => 782,  1816 => 781,  1809 => 777,  1802 => 774,  1798 => 772,  1793 => 770,  1789 => 769,  1783 => 766,  1778 => 765,  1774 => 763,  1769 => 761,  1765 => 760,  1757 => 755,  1753 => 754,  1749 => 753,  1743 => 750,  1739 => 748,  1735 => 746,  1733 => 745,  1729 => 744,  1725 => 743,  1721 => 742,  1715 => 739,  1710 => 738,  1706 => 736,  1701 => 734,  1697 => 733,  1690 => 729,  1686 => 728,  1682 => 727,  1675 => 723,  1671 => 722,  1668 => 721,  1660 => 719,  1658 => 718,  1654 => 717,  1649 => 716,  1645 => 714,  1639 => 712,  1637 => 711,  1632 => 710,  1630 => 709,  1624 => 707,  1622 => 706,  1618 => 705,  1611 => 701,  1607 => 700,  1604 => 699,  1596 => 697,  1594 => 696,  1590 => 695,  1585 => 694,  1581 => 692,  1575 => 690,  1573 => 689,  1568 => 688,  1566 => 687,  1560 => 685,  1558 => 684,  1554 => 683,  1549 => 680,  1543 => 678,  1492 => 629,  1490 => 628,  1486 => 627,  1482 => 626,  1479 => 625,  1471 => 623,  1469 => 622,  1465 => 621,  1460 => 620,  1456 => 618,  1450 => 616,  1448 => 615,  1443 => 614,  1441 => 613,  1435 => 611,  1433 => 610,  1429 => 609,  1422 => 605,  1418 => 604,  1415 => 603,  1406 => 600,  1404 => 599,  1400 => 598,  1396 => 596,  1388 => 594,  1386 => 593,  1382 => 592,  1377 => 591,  1373 => 589,  1367 => 587,  1365 => 586,  1360 => 585,  1358 => 584,  1352 => 582,  1350 => 581,  1346 => 580,  1339 => 576,  1335 => 575,  1332 => 574,  1324 => 572,  1322 => 571,  1318 => 570,  1313 => 569,  1309 => 567,  1303 => 565,  1301 => 564,  1296 => 563,  1294 => 562,  1288 => 560,  1286 => 559,  1282 => 558,  1274 => 553,  1269 => 552,  1265 => 550,  1259 => 548,  1257 => 547,  1252 => 546,  1250 => 545,  1243 => 541,  1239 => 540,  1235 => 539,  1231 => 538,  1227 => 536,  1221 => 533,  1218 => 532,  1216 => 531,  1210 => 528,  1207 => 527,  1205 => 526,  1202 => 525,  1192 => 518,  1189 => 517,  1187 => 516,  1182 => 514,  1179 => 513,  1175 => 511,  1171 => 509,  1169 => 508,  1163 => 505,  1157 => 502,  1151 => 499,  1145 => 496,  1139 => 493,  1135 => 492,  1127 => 487,  1119 => 482,  1113 => 479,  1110 => 478,  1104 => 475,  1098 => 472,  1093 => 470,  1086 => 466,  1080 => 463,  1076 => 461,  1070 => 459,  1064 => 456,  1059 => 454,  1052 => 450,  1046 => 447,  1043 => 446,  1037 => 443,  1031 => 440,  1026 => 438,  1019 => 434,  1013 => 431,  1010 => 430,  1004 => 427,  998 => 424,  993 => 422,  989 => 420,  984 => 417,  982 => 416,  977 => 414,  970 => 410,  965 => 407,  959 => 405,  953 => 402,  950 => 401,  948 => 400,  941 => 396,  935 => 393,  932 => 392,  926 => 389,  920 => 386,  915 => 384,  911 => 382,  905 => 378,  903 => 377,  897 => 373,  895 => 372,  887 => 369,  879 => 366,  876 => 365,  874 => 364,  868 => 362,  864 => 360,  858 => 358,  856 => 357,  851 => 356,  849 => 355,  846 => 354,  839 => 352,  835 => 351,  828 => 349,  824 => 348,  818 => 346,  816 => 345,  808 => 339,  802 => 336,  799 => 335,  797 => 334,  792 => 332,  786 => 329,  780 => 326,  774 => 323,  771 => 322,  765 => 319,  762 => 318,  760 => 317,  754 => 314,  750 => 313,  746 => 312,  742 => 311,  738 => 310,  734 => 309,  728 => 306,  724 => 305,  720 => 304,  716 => 303,  712 => 302,  706 => 300,  700 => 298,  698 => 297,  694 => 296,  690 => 295,  686 => 294,  681 => 292,  675 => 289,  671 => 288,  666 => 287,  662 => 285,  656 => 283,  654 => 282,  649 => 281,  647 => 280,  641 => 278,  639 => 277,  630 => 271,  625 => 269,  621 => 268,  617 => 267,  612 => 265,  608 => 263,  602 => 261,  599 => 260,  593 => 258,  591 => 257,  588 => 256,  582 => 253,  579 => 252,  577 => 251,  571 => 248,  567 => 247,  563 => 246,  558 => 244,  555 => 243,  549 => 240,  546 => 239,  544 => 238,  541 => 237,  535 => 235,  532 => 234,  526 => 232,  524 => 231,  521 => 230,  515 => 227,  512 => 226,  510 => 225,  506 => 223,  500 => 221,  498 => 220,  494 => 219,  490 => 218,  486 => 217,  481 => 216,  475 => 213,  469 => 210,  466 => 209,  464 => 208,  460 => 206,  454 => 203,  451 => 202,  449 => 201,  445 => 200,  441 => 199,  437 => 198,  432 => 197,  426 => 194,  420 => 191,  417 => 190,  415 => 189,  408 => 185,  405 => 184,  378 => 159,  376 => 158,  369 => 154,  363 => 151,  357 => 148,  351 => 145,  344 => 141,  341 => 140,  334 => 136,  331 => 135,  329 => 134,  324 => 132,  320 => 131,  314 => 128,  310 => 127,  304 => 124,  299 => 121,  295 => 119,  293 => 118,  289 => 116,  287 => 115,  281 => 112,  278 => 111,  276 => 110,  270 => 107,  266 => 106,  262 => 105,  256 => 102,  250 => 99,  244 => 96,  238 => 93,  232 => 90,  228 => 88,  223 => 85,  215 => 81,  213 => 80,  209 => 79,  202 => 75,  198 => 74,  193 => 73,  189 => 71,  183 => 69,  181 => 68,  176 => 67,  174 => 66,  171 => 65,  168 => 63,  165 => 62,  161 => 61,  159 => 60,  154 => 58,  148 => 54,  146 => 53,  143 => 52,  134 => 50,  130 => 49,  119 => 47,  115 => 46,  96 => 29,  83 => 27,  79 => 26,  71 => 20,  65 => 18,  63 => 17,  57 => 15,  55 => 14,  51 => 13,  47 => 12,  36 => 6,  29 => 4,  23 => 3,  19 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <!--[if IE]><![endif]-->*/
/* <!--[if IE 8 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie8"><![endif]-->*/
/* <!--[if IE 9 ]><html dir="{{ direction }}" lang="{{ lang }}" class="ie9"><![endif]-->*/
/* <!--[if (gt IE 9)|!(IE)]><!-->*/
/* <html dir="{{ direction }}" lang="{{ lang }}">*/
/* <!--<![endif]-->*/
/* <head>*/
/* <meta charset="UTF-8" />*/
/* <meta name="viewport" content="width=device-width, initial-scale=1">*/
/* <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* <title>{{ title }}</title>*/
/* <base href="{{ base }}" />*/
/* {% if description %}*/
/* <meta name="description" content="{{ description }}" />*/
/* {% endif %}*/
/* {% if keywords %}*/
/* <meta name="keywords" content="{{ keywords }}" />*/
/* {% endif %}*/
/* <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />*/
/* <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />*/
/* <link href="catalog/view/theme/oxy/stylesheet/stylesheet.css" rel="stylesheet">*/
/* <link href="catalog/view/theme/oxy/stylesheet/stylesheet-small-screens.css" rel="stylesheet">*/
/* {% for style in styles %}*/
/* <link href="{{ style.href }}" type="text/css" rel="{{ style.rel }}" media="{{ style.media }}" />*/
/* {% endfor %}*/
/* <script src="catalog/view/javascript/common.js" type="text/javascript"></script>*/
/* <script src="catalog/view/theme/oxy/js/custom-theme.js" type="text/javascript"></script>*/
/* <link href="catalog/view/javascript/jquery/owl-carousel/owl.carousel.css" rel="stylesheet" media="screen" />*/
/* <link href="catalog/view/javascript/jquery/owl-carousel/owl.transitions.css" rel="stylesheet" media="screen" />*/
/* <script src="catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>*/
/* <link rel="stylesheet" type="text/css" href="catalog/view/theme/oxy/stylesheet/cloud-zoom.css" />*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/cloud-zoom.js"></script>*/
/* */
/* <link href="catalog/view/javascript/jquery/swiper/css/swiper.min.css" rel="stylesheet" media="screen" />*/
/* <link href="catalog/view/javascript/jquery/swiper/css/opencart.css" rel="stylesheet" media="screen" />*/
/* <script src="catalog/view/javascript/jquery/swiper/js/swiper.jquery.js" type="text/javascript"></script>*/
/* */
/* <link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/theme/oxy/stylesheet/dcaccordion.css" />	*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/jquery.dcjqaccordion.js"></script>*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/lazyload/lazysizes.min.js"></script>*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/modernizr.custom.js"></script>*/
/* */
/* {% for link in links %}*/
/* <link href="{{ link.href }}" rel="{{ link.rel }}" />*/
/* {% endfor %}*/
/* {% for script in scripts %}*/
/* <script src="{{ script }}" type="text/javascript"></script>*/
/* {% endfor %}*/
/* */
/* {% if t1d_skin =='skin1-default' %}*/
/* <style type="text/css">*/
/* */
/* /*  Body background color and pattern  *//* */
/* body {*/
/* 	background-color: {{ t1d_body_bg_color }};*/
/* 	*/
/* {% if _SERVER.HTTPS and _SERVER.HTTPS != 'off' %}*/
/* 	{% set path_image = config_ssl ~ 'image/' %}*/
/* {% else %}  */
/* 	{% set path_image = config_url ~ 'image/' %}*/
/* {% endif %}*/
/* */
/* {% if t1d_bg_image_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_custom }}");*/
/* {% elseif t1d_pattern_body !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_body }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_position }};*/
/* 	background-repeat: {{ t1d_bg_image_repeat }};*/
/* 	background-attachment: {{ t1d_bg_image_attachment }};*/
/* }*/
/* */
/* /*  Headings color  *//* */
/* h1, h2, h3, h4, h5, h6, .panel-default > .panel-heading, .product-thumb h4 a, #column-left .product-thumb h4 a, #column-right .product-thumb h4 a, .table > thead, .nav-tabs > li > a, legend, #search .form-control, .product-right-sm-info span.p-title, .product-right-sm-related .name a, #tab-review strong, #content .filters a.list-group-item, #content .product-right-sm-related.panel-default > .panel-heading {color: {{ t1d_headings_color }};}*/
/* {% if t1d_headings_border_status == 1 %}*/
/* #content h1::before, #content h2::before {border-bottom: 1px solid {{ t1d_headings_border_color }};}*/
/* #content h1, #content h2 {padding-bottom: 15px;}*/
/* #content .panel-inline-title h2 {margin-bottom: 10px;}*/
/* {% else %}*/
/* #content h1, #content h2 {padding-bottom: 0;}*/
/* #content .panel-inline-title h2 {padding-bottom: 10px;}*/
/* {% endif %}*/
/* */
/* /*  Body text color  *//* */
/* body, #content .tab-content p, #content .product-right-sm-info .modal-body p, ul.pf span, #content .product-buy p, .form-control, span.badge.out-of-stock, #content .flybar-top p.description, #tab-specification .table > tbody > tr > td + td {color: {{ t1d_body_text_color }};}*/
/* */
/* /*  Light text color  *//* */
/* small, .subtitle, .breadcrumb a, .breadcrumb > li + li:before, .cat-description, .cat-description-above-content, .product-thumb .product_box_brand a, ul.pf li, .product-buy ul.pp, .prev-name, .next-name, .product-right-sm-info span.p-subtitle, .product-right-sm-related .product_box_brand a, .category-slider-items .subcat a, #content .product-right-sm-info .modal-body h1 + p, #features .modal-body h1 + p, .cookie-message, .alert-success, .alert-info, .dropdown-highlight, .sitemap > ul > li > ul > li > ul > li > a, .pagination-box .col-sm-6.text-right, .category-slider-item .subcat li.all a, .rating-date, #content .tltblog .row .col-sm-9 h4 + div p, #content h3 + .row .col-sm-6.col-xs-12 .product-thumb .caption h4 + p {color: {{ t1d_light_text_color }};}*/
/* */
/* /*  Links color  *//* */
/* a, a:focus, .list-group a {color: {{ t1d_other_links_color }};}*/
/* */
/* /*  Links hover color  *//* */
/* a:hover, .breadcrumb a:hover, .dropdown-highlight:hover, .dropdown-highlight:focus, .category-slider-items .subcat li:hover a, .table .btn, .table .btn-primary, .table .btn-primary:hover, .table .btn-primary:focus, .list-group a.active, .list-group a.active:hover, .list-group a:hover, .category-list .image:hover, .category-slider-item .subcat li.all a:hover, ul.pf span.stock {color: {{ t1d_links_hover_color }};}*/
/* */
/* /*  Icons color  *//* */
/* #menu_brands .image i, .brand-slider-items .image i, .accordion li.dcjq-parent-li > a + .dcjq-icon, .panel-category-dropdown li a i {color: {{ t1d_icons_color }};}*/
/* */
/* /*  Icons hover color  *//* */
/* #search .input-group-addon .btn:hover, .theme-modal .modal-body .close, .alert-success i, .alert-success .close, .alert-info i, .alert-info .close, .accordion li.dcjq-parent-li > a + .dcjq-icon:hover, .theme-modal-popup .modal-body .close {color: {{ t1d_icons_hover_color }};}*/
/* .mfp-close {color: {{ t1d_icons_hover_color }}!important;}*/
/* #toTop:hover, #bar, .swiper-pagination-bullet-active {background-color: {{ t1d_icons_hover_color }}!important;}*/
/* */
/* /*  Wrapper  *//* */
/* {% if t1d_wrapper_frame_bg_color_status =='1' %}*/
/* .wrapper.framed, .wrapper.full-width {*/
/* 	background-color: {{ t1d_wrapper_frame_bg_color }};*/
/* }*/
/* {% endif %}*/
/* {% if t1d_wrapper_shadow =='1' %}*/
/* .wrapper.framed {box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);}*/
/* {% endif %}*/
/* {% if t1d_boxes_shadow =='1' %}*/
/* .wrapper.boxed #column-left .panel, .wrapper.boxed #column-right .panel, .wrapper.boxed #content {box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);}*/
/* {% endif %}*/
/* */
/* /*  Content Column  *//* */
/* #content, #content .panel, .category-list + .panel-default > .panel-heading, .modal-content, .alert-success, .alert-info, .cookie-message, .custom_box, .theme-custom-products .panel-inline-items, .theme-lookbook-bg, .theme-banner-bg, .theme-gallery-bg, .theme-store-tv-bg, .highly-recommended-module-content, #tab-review .rating-text, #column-left .product-thumb .image img, #column-right .product-thumb .image img, #livesearch_search_results {*/
/* 	background-color: {{ t1d_content_column_bg_color }};*/
/* }*/
/* .tab-content .table-bordered, .tab-content .table-bordered > thead > tr > th, .tab-content .table-bordered > tbody > tr > th, .tab-content .table-bordered > tfoot > tr > th, .tab-content .table-bordered > thead > tr > td, .tab-content .table-bordered > tbody > tr > td, .tab-content .table-bordered > tfoot > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {*/
/* 	background-color: {{ t1d_content_column_bg_color }};*/
/* 	border-color: {{ t1d_content_column_hli_bg_color }};*/
/* }*/
/* .well, .table-bordered, .table-bordered > thead > tr > th, .table-bordered > thead > tr > td, .table-bordered img {*/
/* 	border: 1px solid {{ t1d_content_column_bg_color }};*/
/* 	background-color: {{ t1d_content_column_hli_bg_color }};*/
/* }*/
/* {% if t1d_content_column_hli_buy_column == 1 %}*/
/* .product-buy .product-buy-wrapper {*/
/* 	background-color: {{ t1d_content_column_hli_bg_color }};*/
/* 	padding: 30px;*/
/* }*/
/* {% endif %}*/
/* .nav-tabs > li > a, .nav-tabs > li > a:hover, .sitemap > ul > li > a, .manufacturer-list h3, .search-panel, .product-page #product-tabs .tab-content, .contact-details-wrapper, #tab-review .table-bordered > tbody > tr > td {*/
/* 	background-color: {{ t1d_content_column_hli_bg_color }};*/
/* }*/
/* */
/* #livesearch_search_results li, .modal-footer, .list-group a, .list-group a.active, .list-group a.active:hover, .list-group a:hover, a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus {*/
/*     border-top: 1px solid {{ t1d_content_column_separator_color }};	*/
/* }	*/
/* .modal-header {*/
/*     border-bottom: 1px solid {{ t1d_content_column_separator_color }};	*/
/* }*/
/* .contact-map, .img-thumbnail-theme {*/
/*     border: 1px solid {{ t1d_content_column_separator_color }};	*/
/* }*/
/* hr, #search .input-group-addon {*/
/* 	border-color: {{ t1d_content_column_separator_color }};*/
/* }*/
/* */
/* /*  Left-Right Column Boxes  *//* */
/* {% if t1d_sidebar_bg_color_status =='0' %}*/
/* .wrapper.boxed #column-left .panel-default > .panel-heading {*/
/* 	padding: 25px 0 0;*/
/* }*/
/* .wrapper.boxed #column-left .panel-default > .list-group > ul > li > a, .wrapper.boxed #column-left .panel-default .accordion li.dcjq-parent-li ul li a, .wrapper.boxed #column-left .filters div.list-group-item {*/
/* 	padding: 5px 0;*/
/* }*/
/* .wrapper.boxed #column-left .newsletter-block {*/
/* 	padding: 25px 0;*/
/* }*/
/* .wrapper.boxed #column-left .product-items-0 > div {*/
/* 	padding: 20px 0 0;*/
/* }*/
/* .wrapper.boxed #column-left .product-thumb {*/
/* 	padding: 0;*/
/* }*/
/* .wrapper.boxed #column-left .product-items-1 .product-layout-slider {*/
/* 	padding: 30px 0 20px;*/
/* }*/
/* .wrapper.boxed #column-left .product-items.owl-carousel .owl-controls .owl-buttons .owl-next {*/
/* 	right: 0!important;*/
/* }*/
/* .wrapper.boxed #column-left .product-items.owl-carousel .owl-controls .owl-buttons .owl-prev {*/
/* 	right: 35px!important;*/
/* }*/
/* {% endif %}*/
/* #column-left .panel, #column-right .panel {*/
/* 	margin-bottom: {{ t1d_sidebar_margin_bottom }}px;*/
/* }*/
/* */
/* /*  Left Column Box  *//* */
/* {% if t1d_sidebar_bg_color_status =='1' %}*/
/* #column-left .panel-default > .panel-heading {*/
/* 	background-color: {{ t1d_left_column_head_title_bg_color }};*/
/* }*/
/* #column-left .panel, #column-left .owl-carousel {*/
/* 	background-color: {{ t1d_left_column_box_bg_color }};*/
/* }*/
/* {% endif %}*/
/* #column-left .panel-default > .panel-heading, #column-left .panel-default > .panel-heading h2 {color: {{ t1d_left_column_head_title_color }};}*/
/* #column-left a {color: {{ t1d_left_column_box_links_color }};}*/
/* #column-left a:hover {color: {{ t1d_left_column_box_links_color_hover }};}*/
/* #column-left {color: {{ t1d_left_column_box_text_color }};}*/
/* {% if t1d_sidebar_separator_status =='1' %}*/
/* #column-left .panel, #column-left .owl-carousel.carousel-module, #column-left .owl-carousel.banner-module {*/
/* 	border-bottom: 1px solid {{ t1d_left_column_box_separator_color }}!important;*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Right Column Box  *//* */
/* {% if t1d_sidebar_bg_color_status =='1' %}*/
/* #column-right .panel-default > .panel-heading {*/
/* 	background-color: {{ t1d_right_column_head_title_bg_color }};*/
/* }*/
/* #column-right .panel, #column-right .owl-carousel {*/
/* 	background-color: {{ t1d_right_column_box_bg_color }};*/
/* }*/
/* {% endif %}*/
/* #column-right .panel-default > .panel-heading, #column-right .panel-default > .panel-heading h2 {color: {{ t1d_right_column_head_title_color }};}*/
/* #column-right a {color: {{ t1d_right_column_box_links_color }};}*/
/* #column-right a:hover {color: {{ t1d_right_column_box_links_color_hover }};}*/
/* #column-right {color: {{ t1d_right_column_box_text_color }};}*/
/* {% if t1d_sidebar_separator_status =='1' %}*/
/* #column-right .panel, #column-right .owl-carousel.carousel-module, #column-right .owl-carousel.banner-module {border-bottom: 1px solid {{ t1d_right_column_box_separator_color }}!important;}*/
/* {% endif %}*/
/* */
/* /*  Category Box   *//* */
/* {% if t1d_sidebar_bg_color_status =='1' %}*/
/* #column-left .panel-default.panel-category > .panel-heading, #column-right .panel-default.panel-category > .panel-heading {*/
/* 	background-color: {{ t1d_category_box_head_title_bg_color }};*/
/* }*/
/* {% endif %}*/
/* #column-left .panel-default.panel-category, #column-right .panel-default.panel-category {*/
/* 	{% if t1d_sidebar_bg_color_status =='1' %}*/
/* 	background-color: {{ t1d_category_box_box_bg_color }};*/
/* 	{% endif %}*/
/* 	{% if t1d_sidebar_separator_status =='1' %}*/
/*     border-bottom: 1px solid {{ t1d_category_box_box_separator_color }}!important;*/
/* 	{% endif %}*/
/* }*/
/* {% if t1d_sidebar_separator_status =='1' %}*/
/* .panel-default.panel-category .accordion li.dcjq-parent-li > a.list-group-item {*/
/* 	border-bottom: 1px solid {{ t1d_category_box_box_separator_color }}!important;*/
/* }*/
/* {% endif %}*/
/* #column-left .panel-default.panel-category > .panel-heading h2, #column-right .panel-default.panel-category > .panel-heading h2 {*/
/*     color: {{ t1d_category_box_head_title_color }};*/
/* }*/
/* #column-left .panel-default.panel-category a, #column-right .panel-default.panel-category a {color: {{ t1d_category_box_box_links_color }};}*/
/* #column-left .panel-default.panel-category a:hover, #column-right .panel-default.panel-category a:hover, #column-left .panel-default.panel-category .cat-mod-child a:hover, #column-right .panel-default.panel-category .cat-mod-child a:hover {color: {{ t1d_category_box_box_links_color_hover }};}*/
/* #column-left .panel-default.panel-category .cat-mod-child a, #column-right .panel-default.panel-category .cat-mod-child a, #column-left .accordion li ul li a.list-group-item {color: {{ t1d_category_box_box_subcat_color }};}*/
/* */
/* /*  Filter Box Content  *//* */
/* {% if t1d_sidebar_bg_color_status =='1' %}*/
/* #column-left .panel-default.filters > .panel-heading, #column-right .panel-default.filters > .panel-heading {*/
/* 	background-color: {{ t1d_filter_box_head_title_bg_color }};*/
/* }*/
/* {% endif %}*/
/* #column-left .panel-default.filters, #column-right .panel-default.filters, #content .panel-default.filters {*/
/* 	{% if t1d_sidebar_bg_color_status =='1' %}*/
/* 	background-color: {{ t1d_filter_box_box_bg_color }};*/
/* 	{% endif %}*/
/* 	{% if t1d_sidebar_separator_status =='1' %}*/
/*     border-bottom: 1px solid {{ t1d_filter_box_box_separator_color }}!important;*/
/* 	{% endif %}*/
/* }*/
/* #column-left .panel-default.filters > .panel-heading h2, #column-right .panel-default.filters > .panel-heading h2 {*/
/*     color: {{ t1d_filter_box_head_title_color }};*/
/* }*/
/* #column-left .panel-default.filters a, #column-right .panel-default.filters a, #content .panel-default.filters a {color: {{ t1d_filter_box_box_filter_title_color }};}*/
/* #column-left .panel-default.filters label, #column-right .panel-default.filters label, #content .panel-default.filters label {color: {{ t1d_filter_box_box_filter_name_color }};}*/
/* #column-left .panel-default.filters label:hover, #column-right .panel-default.filters label:hover, #content .panel-default.filters label:hover {color: {{ t1d_filter_box_box_filter_name_color_hover }};}*/
/* #content .panel-default.filters {*/
/* 	border-bottom: 1px solid {{ t1d_filter_box_box_separator_color }};*/
/* }*/
/* */
/* */
/* /*  HEADER  *//* */
/* header {*/
/* {% if t1d_top_area_status == 1 %}*/
/* 	background-color: {{ t1d_top_area_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_bg_image_ta_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_ta_custom }}");*/
/* {% elseif t1d_pattern_k_ta !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_ta }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_ta_position }};*/
/* 	background-repeat: {{ t1d_bg_image_ta_repeat }};*/
/* 	background-attachment: {{ t1d_bg_image_ta_attachment }};*/
/* }*/
/* .is-sticky #menu {*/
/* 	background-color: {{ t1d_top_area_mini_bg_color }};*/
/* }*/
/* .button-i, #cart.buttons-header #cart-tt i, .header-custom-box:hover i {color: {{ t1d_top_area_icons_color }};}*/
/* #cart #cart-tt #cart-total {background-color: {{ t1d_top_area_icons_color_hover }};}*/
/* .buttons-header:hover .button-i {color: {{ t1d_top_area_icons_color_hover }};}*/
/* {% if t1d_top_area_icons_separator_status == 1 %}*/
/* .buttons-header, .menu-mobile-block {border-left: 1px solid {{ t1d_top_area_icons_separator_color }};}*/
/* {% endif %}*/
/* #cart.buttons-header .button-i {color: {{ t1d_top_area_icons_separator_color }};}*/
/* */
/* #search .form-control, #search .input-group-addon, #search .input-group-addon .btn {background-color: {{ t1d_top_area_search_bg_color }};}*/
/* #search.header-search-bar .input-lg, .modal-body #search {border: 1px solid {{ t1d_top_area_search_border_color }};}*/
/* #search .form-control::-moz-placeholder {color: {{ t1d_top_area_search_color}};}*/
/* #search .form-control {color: {{ t1d_top_area_search_color_active }};}*/
/* #search .btn .button-i i, #search .input-group-addon .btn i {color: {{ t1d_top_area_search_icon_color }};}*/
/* */
/* /*  Top News *//* */
/* #top-news-wrapper  {background-color: {{ t1o_news_bg_color }};}*/
/* #top-news-content li a  {color: {{ t1o_news_bg_color }};}*/
/* #top-news-content i {color: {{ t1o_news_icons_color }};}*/
/* #top-news-content span#top-news {color: {{ t1o_news_word_color }};}*/
/* #top-news-content #news a {color: {{ t1o_news_color }};}*/
/* #top-news-content #news a:hover {color: {{ t1o_news_hover_color }};}*/
/* */
/* /*  Top Bar  *//* */
/* {% if t1d_top_area_tb_bg_status == 1 %}*/
/* #top {*/
/* 	background-color: {{ t1d_top_area_tb_bg_color }};*/
/* }*/
/* {% endif %}*/
/* #top, .top-links li {*/
/* 	color: {{ t1d_top_area_tb_text_color }};*/
/* }*/
/* #top a, #top .btn-link, .top-links a, .top-links .fa-angle-down {*/
/* 	color: {{ t1d_top_area_tb_link_color }};*/
/* }*/
/* #top a:hover, #top .btn-link:hover, #top .btn-link:hover i, .top-links a:hover, .top-links a:hover i {*/
/* 	color: {{ t1d_top_area_tb_link_color_hover }};*/
/* }*/
/* .top-links i {*/
/* 	color: {{ t1d_top_area_tb_icons_color }};*/
/* } */
/* {% if t1d_top_area_tb_bottom_border_status == 1 %}*/
/* #top {*/
/* 	border-bottom: 1px solid {{ t1d_top_area_tb_bottom_border_color }};*/
/* }*/
/* {% endif %}*/
/* */
/* */
/* /*  MAIN MENU *//* */
/* */
/* /*  Main Menu Bar  *//* */
/* #menu {*/
/* {% if t1d_mm_bg_color_status == 1 %}*/
/* 	background-color: {{ t1d_mm_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_mm_border_top_status == 1 %}	*/
/*     border-top: {{ t1d_mm_border_top_size }}px solid {{ t1d_mm_border_top_color }};	*/
/* {% endif %}*/
/* {% if t1d_mm_border_bottom_status == 1 %}	*/
/*     border-bottom: {{ t1d_mm_border_bottom_size }}px solid {{ t1d_mm_border_bottom_color }};	*/
/* {% endif %}*/
/* */
/* {% if t1d_bg_image_mm_custom !='' and t1d_mm_bg_color_status == 1 %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_mm_custom }}");*/
/* {% elseif t1d_pattern_k_mm !='none' and t1d_mm_bg_color_status == 1 %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_mm }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-repeat: {{ t1d_bg_image_mm_repeat }};*/
/* }*/
/* {% if t1d_mm_separator_status == 1 %}*/
/* #homepage, .menu_oc, #menu_ver, #menu_ver_2, #menu_hor, #menu_inline, #menu_brands, .menu_links, .menu_custom_menu, .menu_custom_block, #menu .flexMenu-viewMore {*/
/* 	border-left: {{ t1d_mm_separator_size }}px solid {{ t1d_mm_separator_color }};*/
/* }*/
/* .navbar .main-menu > li:last-child {*/
/* 	border-right: {{ t1d_mm_separator_size }}px solid {{ t1d_mm_separator_color }};*/
/* }*/
/* {% endif %}*/
/* {% if t1d_mm_shadow_status == 1 %}*/
/* #menu {*/
/*     box-shadow: 0 10px 15px -10px rgba(0, 0, 0, 0.15);*/
/* }*/
/* {% endif %}*/
/* {% if t1d_mm_sort_down_icon_status == 0 %}*/
/* #menu .fa-sort-down {*/
/* 	display: none;*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Home Page Link  *//* */
/* {% if t1d_mm1_bg_color_status == 1 %} */
/* #menu #homepage {*/
/* 	background-color: {{ t1d_mm1_bg_color }};*/
/* }*/
/* #menu #homepage:hover {*/
/* 	background-color: {{ t1d_mm1_bg_hover_color }};*/
/* }	*/
/* {% endif %}*/
/* #menu #homepage a {*/
/* 	color: {{ t1d_mm1_link_color }};*/
/* }	*/
/* #menu #homepage:hover a {*/
/* 	color: {{ t1d_mm1_link_hover_color }};*/
/* }*/
/* */
/* /*  Categories  *//* */
/* {% if t1d_mm2_bg_color_status == 1 %}*/
/* .menu_oc, #menu_ver, #menu_ver_2, #menu_hor, #menu_inline  {*/
/* 	background-color: {{ t1d_mm2_bg_color }};*/
/* }*/
/* .menu_oc:hover, #menu_ver:hover, #menu_ver_2:hover, #menu_hor:hover, #menu_inline:hover  {*/
/* 	background-color: {{ t1d_mm2_bg_hover_color }};*/
/* }*/
/* {% endif %} */
/* .menu_oc > a, #menu_ver > a, #menu_ver_2 > a, #menu_hor > a, #menu_inline > a,*/
/* #menu .flexMenu-popup.dropdown-menus .menu_oc > a, #menu .flexMenu-popup.dropdown-menus #menu_ver > a, #menu .flexMenu-popup.dropdown-menus #menu_ver_2 > a, #menu .flexMenu-popup.dropdown-menus #menu_hor > a, #menu .flexMenu-popup.dropdown-menus #menu_inline > a {*/
/* 	color: {{ t1d_mm2_link_color }};*/
/* }	*/
/* .menu_oc:hover > a, #menu_ver:hover > a, #menu_ver_2:hover > a, #menu_hor:hover > a, #menu_inline:hover > a,*/
/* #menu .flexMenu-popup.dropdown-menus .menu_oc:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_ver:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_ver_2:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_hor:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_inline:hover > a {*/
/* 	color: {{ t1d_mm2_link_hover_color }};*/
/* }*/
/* {% if t1o_menu_categories_home_visibility == 1 %}*/
/* .common-home #menu #menu_ver_2 > a + .dropdown-menus {opacity: 1; visibility: visible;}*/
/* .common-home .is-sticky #menu #menu_ver_2 > a + .dropdown-menus {opacity: 0; visibility: hidden;}*/
/* {% endif %}*/
/* */
/* /*  Brands  *//* */
/* {% if t1d_mm3_bg_color_status == 1 %} */
/* #menu_brands {*/
/* 	background-color: {{ t1d_mm3_bg_color }};*/
/* }*/
/* #menu_brands:hover {*/
/* 	background-color: {{ t1d_mm3_bg_hover_color }};*/
/* }*/
/* {% endif %}*/
/* #menu_brands > a, #menu .flexMenu-popup.dropdown-menus #menu_brands > a {*/
/* 	color: {{ t1d_mm3_link_color }};*/
/* }	*/
/* #menu_brands:hover > a, #menu .flexMenu-popup.dropdown-menus #menu_brands:hover > a {*/
/* 	color: {{ t1d_mm3_link_hover_color }};*/
/* }*/
/* */
/* /*  Custom Blocks  *//* */
/* {% if t1d_mm6_bg_color_status == 1 %} */
/* .menu_custom_block {*/
/* 	background-color: {{ t1d_mm6_bg_color }};*/
/* }*/
/* .menu_custom_block:hover {*/
/* 	background-color: {{ t1d_mm6_bg_hover_color }};*/
/* }*/
/* {% endif %}*/
/* .menu_custom_block > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_block > a {*/
/* 	color: {{ t1d_mm6_link_color }};*/
/* }	*/
/* .menu_custom_block:hover > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_block:hover > a {*/
/* 	color: {{ t1d_mm6_link_hover_color }};*/
/* }*/
/* */
/* /*  Custom Dropdown Menus  *//* */
/* {% if t1d_mm5_bg_color_status == 1 %} */
/* .menu_custom_menu {*/
/* 	background-color: {{ t1d_mm5_bg_color }};*/
/* }*/
/* .menu_custom_menu:hover {*/
/* 	background-color: {{ t1d_mm5_bg_hover_color }};*/
/* }*/
/* {% endif %} */
/* .menu_custom_menu > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_menu > a {*/
/* 	color: {{ t1d_mm5_link_color }};*/
/* }	*/
/* .menu_custom_menu:hover > a, #menu .flexMenu-popup.dropdown-menus .menu_custom_menu:hover > a {*/
/* 	color: {{ t1d_mm5_link_hover_color }};*/
/* }*/
/* */
/* /*  Custom Links  *//* */
/* {% if t1d_mm4_bg_color_status == 1 %} */
/* .menu_links {*/
/* 	background-color: {{ t1d_mm4_bg_color }};*/
/* }	*/
/* .menu_links:hover {*/
/* 	background-color: {{ t1d_mm4_bg_hover_color }};*/
/* }	*/
/* {% endif %}*/
/* .menu_links a, .flexMenu-viewMore a > i, #menu .flexMenu-popup.dropdown-menus .menu_links a {*/
/* 	color: {{ t1d_mm4_link_color }};*/
/* }	*/
/* .menu_links:hover a, .flexMenu-viewMore:hover a > i, #menu .flexMenu-popup.dropdown-menus .menu_links:hover a {*/
/* 	color: {{ t1d_mm4_link_hover_color }};*/
/* }*/
/* */
/* /*  Flex Menu  *//* */
/* #menu .flexMenu-viewMore > a + .dropdown-menus {*/
/*     background-color: {{ t1d_mm_bg_color }};*/
/* }*/
/* */
/* /*  Sub-Menu  *//* */
/* #menu .dropdown-menu, #menu .dropdown-menus {*/
/* 	background-color: {{ t1d_mm_sub_bg_color }};*/
/* 	color: {{ t1d_mm_sub_text_color }};*/
/* }*/
/* #menu .dropdown-highlight {*/
/* 	background-color: {{ t1d_mm_sub_titles_bg_color }};*/
/* }*/
/* #menu .dropdown-menus a, #menu_brands .brand-item .name {*/
/* 	color: {{ t1d_mm_sub_link_color }};*/
/* }	*/
/* #menu .dropdown-menus a:hover, #menu .dropdown-menus li a:hover, #menu #menu_hor .dropdown-menus li.sub-cat:hover > a, #menu #menu_hor .dropdown-menus li.main-cat > a:hover, #menu_brands .brand-item:hover .name, #menu #menu_inline .dropdown-menus li .dropdown-menus a:hover, #menu #menu_ver .dropdown-menus .dropdown-menus a:hover, #menu #menu_ver_2 .dropdown-menus .dropdown-menus a:hover, #menu .menu_oc .dropdown-menus .dropdown-menus a:hover {*/
/* 	color: {{ t1d_mm_sub_link_hover_color }};*/
/* }*/
/* #menu #menu_hor .dropdown-menus li.sub-cat > a, #menu #menu_inline .dropdown-menus li .dropdown-menus a, #menu #menu_ver .dropdown-menus .dropdown-menus a, #menu #menu_ver_2 .dropdown-menus .dropdown-menus a, #menu .menu_oc .dropdown-menus .dropdown-menus a {*/
/* 	color: {{ t1d_mm_sub_subcategory_color }};*/
/* }*/
/* #menu .dropdown-menu, #menu .dropdown-menus {*/
/* {% if t1d_mm_sub_box_shadow == 1 %}*/
/* 	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);*/
/* {% else %}*/
/* 	box-shadow: none;*/
/* {% endif %}*/
/* }*/
/* .navbar {text-align: {{ t1o_menu_align }};}*/
/* */
/* {% if t1d_mm_sub_border_status == 1 %}*/
/* #menu #menu_hor .dropdown-menus, #menu #menu_ver_2 > a + .dropdown-menus {*/
/* 	border: 6px solid {{ t1d_mm2_bg_hover_color }} ;*/
/* }*/
/* #menu #menu_ver_2 .dropdown-menus .dropdown-menus {*/
/* 	top: -6px;*/
/* 	margin-left: 6px;*/
/* }*/
/* {% endif %}*/
/* */
/* {% if t1d_mm_sub_main_category_border_status == 1 %}*/
/* #menu #menu_hor .dropdown-menus li.main-cat > a {*/
/* 	border-bottom: 2px solid {{ t1d_mm_sub_link_hover_color }};*/
/* }*/
/* {% endif %}*/
/* {% if t1d_mm_sub_subcategory_border_status == 1 %}*/
/* #menu #menu_hor .dropdown-menus li.sub-cat > a, #menu .menu_custom_menu .dropdown-menus a {*/
/* 	border-bottom: 1px solid {{ t1d_mm_sub_titles_bg_color }};*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Product Box  *//* */
/* span.badge.sale {background-color: {{ t1d_mid_prod_box_sale_icon_color }};}*/
/* span.badge.new {background-color: {{ t1d_mid_prod_box_new_icon_color }};}*/
/* span.badge.out-of-stock {background-color: {{ t1d_mid_prod_box_out_of_stock_icon_color }};}*/
/* .rating .fa-y .fa-star {color: {{ t1d_mid_prod_stars_color }};}*/
/* */
/* /*  FOOTER  *//* */
/* footer {*/
/* {% if t1d_bg_image_f1_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_f1_custom }}");*/
/* {% elseif t1d_pattern_k_f1 !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_f1 }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_f1_position }};*/
/* 	background-repeat: {{ t1d_bg_image_f1_repeat }};*/
/* }*/
/* */
/* /*  Top Custom Block 1  *//* */
/* #footer_custom_top_1 {*/
/* 	color: {{ t1d_f6_text_color }};*/
/* {% if t1d_f6_bg_color_status == 1 %}*/
/* 	background-color: {{ t1d_f6_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_bg_image_f6_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_f6_custom }}");*/
/* {% elseif t1d_pattern_k_f6 !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_f6 }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_f6_position }};*/
/* 	background-repeat: {{ t1d_bg_image_f6_repeat }};*/
/* {% if t1d_f6_border_top_status == 1 %}*/
/* 	border-top: {{ t1d_f6_border_top_size }}px solid {{ t1d_f6_border_top_color }};*/
/* {% endif %}*/
/* }*/
/* #footer_custom_top_1 a {color: {{ t1d_f6_link_color }};}*/
/* #footer_custom_top_1 a:hover {color: {{ t1d_f6_link_hover_color }};}*/
/* */
/* /*  Information, Custom Column  *//* */
/* #information {*/
/* 	color: {{ t1d_f2_text_color }};*/
/* {% if t1d_f2_bg_color_status == 1 %}*/
/* 	background-color: {{ t1d_f2_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_bg_image_f2_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_f2_custom }}");*/
/* {% elseif t1d_pattern_k_f2 !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_f2 }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_f2_position }};*/
/* 	background-repeat: {{ t1d_bg_image_f2_repeat }};*/
/* {% if t1d_f2_border_top_status == 1 %}*/
/* 	border-top: {{ t1d_f2_border_top_size }}px solid {{ t1d_f2_border_top_color }};*/
/* {% endif %}*/
/* }*/
/* #information h4 {*/
/* 	color: {{ t1d_f2_titles_color }};*/
/* {% if t1d_f2_titles_border_status == 1 %}*/
/* 	border-bottom: {{ t1d_f2_titles_border_size }}px solid {{ t1d_f2_titles_border_color }};*/
/* 	margin-bottom: 15px;*/
/* {% endif %}*/
/* }*/
/* #information a, #information span {color: {{ t1d_f2_link_color }};}*/
/* #information a:hover {color: {{ t1d_f2_link_hover_color }};}*/
/* */
/* /*  Powered by, Payment Images, Follow Us  *//* */
/* #powered {*/
/* 	color: {{ t1d_f3_text_color }};*/
/* {% if t1d_f3_bg_color_status == 1 %}*/
/* 	background-color: {{ t1d_f3_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_bg_image_f3_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_f3_custom }}");*/
/* {% elseif t1d_pattern_k_f3 !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_f3 }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_f3_position }};*/
/* 	background-repeat: {{ t1d_bg_image_f3_repeat }};*/
/* {% if t1d_f3_border_top_status == 1 %}*/
/* 	border-top: {{ t1d_f3_border_top_size }}px solid {{ t1d_f3_border_top_color }};*/
/* {% endif %}*/
/* }*/
/* #powered a {color: {{ t1d_f3_link_color }};}*/
/* #powered a:hover, #powered i:hover {color: {{ t1d_f3_link_hover_color }};}*/
/* {% if t1d_f3_icons_social_style == 1 %}*/
/* #footer-social li.facebook {*/
/* 	background-color: #3B5998;*/
/* }*/
/* #footer-social li.twitter {*/
/* 	background-color: #4BB8E2;*/
/* }*/
/* #footer-social li.google {*/
/* 	background-color: #D73A1A;*/
/* }*/
/* #footer-social li.rrs {*/
/* 	background-color: #F29735;*/
/* }*/
/* #footer-social li.pinterest {*/
/* 	background-color: #C92026;*/
/* }*/
/* #footer-social li.vimeo {*/
/* 	background-color: #44A4E3;*/
/* }*/
/* #footer-social li.flickr {*/
/* 	background-color: #C3C3C3;*/
/* }*/
/* #footer-social li.linkedin {*/
/* 	background-color: #0087CD;*/
/* }*/
/* #footer-social li.youtube {*/
/* 	background-color: #DB3B1B;*/
/* }*/
/* #footer-social li.dribbble {*/
/* 	background-color: #DA467F;*/
/* }*/
/* #footer-social li.instagram {*/
/* 	background-color: #A27358;*/
/* }*/
/* #footer-social li.behance {*/
/* 	background-color: #00A8ED;*/
/* }*/
/* #footer-social li.skype {*/
/* 	background-color: #00AEF0;*/
/* }*/
/* #footer-social li.tumblr {*/
/* 	background-color: #3B5876;*/
/* }*/
/* #footer-social li.reddit {*/
/* 	background-color: #FF4400;*/
/* }*/
/* #footer-social li.vk {*/
/* 	background-color: #4C75A3;*/
/* }*/
/* {% else %}*/
/* #footer-social li {background-color: {{ t1d_f3_icons_bg_color }};}*/
/* {% endif %}*/
/* */
/* /*  Bottom Custom Block 1  *//* */
/* #footer_custom_1 {*/
/* 	color: {{ t1d_f4_text_color }};*/
/* {% if t1d_f4_bg_color_status == 1 %}*/
/* 	background-color: {{ t1d_f4_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_bg_image_f4_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_f4_custom }}");*/
/* {% elseif t1d_pattern_k_f4 !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_f4 }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_f4_position }};*/
/* 	background-repeat: {{ t1d_bg_image_f4_repeat }};*/
/* {% if t1d_f4_border_top_status == 1 %}*/
/* 	border-top: {{ t1d_f4_border_top_size }}px solid {{ t1d_f4_border_top_color }};*/
/* {% endif %}*/
/* }*/
/* #footer_custom_1 a {color: {{ t1d_f4_link_color }};}*/
/* #footer_custom_1 a:hover {color: {{ t1d_f4_link_hover_color }};}*/
/* */
/* /*  Bottom Custom Block 2  *//* */
/* #footer_custom_2 {*/
/* 	color: {{ t1d_f5_text_color }};*/
/* {% if t1d_f5_bg_color_status == 1 %}*/
/* 	background-color: {{ t1d_f5_bg_color }};*/
/* {% endif %}*/
/* {% if t1d_bg_image_f5_custom !='' %}*/
/* 	background-image: url("{{ path_image ~ t1d_bg_image_f5_custom }}");*/
/* {% elseif t1d_pattern_k_f5 !='none' %}*/
/* 	background-image: url("catalog/view/theme/oxy/image/patterns/p{{ t1d_pattern_k_f5 }}.png");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* 	background-position: {{ t1d_bg_image_f5_position }};*/
/* 	background-repeat: {{ t1d_bg_image_f5_repeat }};*/
/* {% if t1d_f5_border_top_status == 1 %}*/
/* 	border-top: {{ t1d_f5_border_top_size }}px solid {{ t1d_f5_border_top_color }};*/
/* {% endif %}*/
/* }*/
/* #footer_custom_2 a {color: {{ t1d_f5_link_color }};}*/
/* #footer_custom_2 a:hover {color: {{ t1d_f5_link_hover_color }};}*/
/* */
/* */
/* /*  Prices *//* */
/* #content .product-thumb p.price, #cart span.price, #column-left .product-items .price, #column-right .product-items .price, .product-buy .price-reg, #content .product-right-sm-related p.price, .product-buy .price-old {color: {{ t1d_price_color }};}*/
/* .price-old, .product-thumb .price-tax {color: {{ t1d_price_old_color }};}*/
/* .price-new, .save-percent {color: {{ t1d_price_new_color }};}*/
/* */
/* /*  Buttons *//* */
/* .btn-default, button.wishlist, button.compare, .pagination > li > a, .pagination > li > span, #cart .btn-default, input#input-quantity, input.dec, input.inc, .owl-carousel .owl-buttons div, .theme-gallery-content .gallery-hover-box, .theme-store-tv-content .store-tv-hover-box, .btn.theme-banner-title.no-link:hover, #footer_custom_top_1 a.btn-default, footer_custom_1 a.btn-default, #information a.btn-default, #footer_custom_2 a.btn-default, #menu .dropdown-menus a.btn-default.menu-button, .product-right-sm-info span.p-icon, .swiper-pager div {*/
/* 	{% if t1d_button_bg_status == 1 %} */
/* 	background-color: {{ t1d_button_bg_color }};*/
/* 	{% else %}*/
/* 	background-color: transparent;*/
/* 	{% endif %}*/
/* 	color: {{ t1d_button_text_color }};*/
/* 	border: 2px solid {{ t1d_button_bg_color }};*/
/* }*/
/* .btn-default:hover, #column-left .btn-default:hover, #column-right .btn-default:hover, .btn-default:focus, .btn-default:active, .btn-default.active, .btn-default.disabled, .btn-default[disabled], button.wishlist:hover, button.compare:hover, .pagination > li > a:hover, .pagination > li > a:focus, .pagination > li > a:active, .pagination > li > span:hover, #cart .btn-default:hover, input.dec:hover, input.inc:hover, .panel-inline-content .brand-slider-item:hover .btn-default, .panel-inline-content .category-slider-item:hover .btn-default, .category-list .image:hover .btn-default, .owl-carousel .owl-buttons div:hover, .theme-lookbook-item:hover .theme-lookbook-item-title, .theme-banner-item:hover .theme-banner-title, #footer_custom_top_1 a.btn-default:hover, footer_custom_1 a.btn-default:hover, #information a.btn-default:hover, #footer_custom_2 a.btn-default:hover, #menu .dropdown-menus a.btn-default.menu-button:hover, .btn-default.active:hover, .swiper-pager div:hover {*/
/* 	background-color: {{ t1d_button_bg_hover_color }};*/
/*     color: {{ t1d_button_text_hover_color }};*/
/* 	border: 2px solid {{ t1d_button_bg_hover_color }};	*/
/* 	{% if t1d_button_shadow_status == 1 %}*/
/* 	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);*/
/* 	{% endif %}*/
/* }*/
/* .theme-gallery-content .gallery-hover-box i, .theme-store-tv-content .store-tv-hover-box i {*/
/* 	color: {{ t1d_button_text_color }};*/
/* }*/
/* input[type="text"]:focus, input[type="password"]:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="time"]:focus, input[type="url"]:focus, textarea:focus, select:focus, .form-control:focus, .form-control {*/
/* 	background-color: {{ t1d_content_column_bg_color }};*/
/* 	color: {{ t1d_button_text_color }};*/
/* 	border: 1px solid {{ t1d_button_bg_color }};*/
/* }*/
/* */
/* */
/* .btn-primary, .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus, #cart .btn-primary, #footer_custom_top_1 a.btn-primary, footer_custom_1 a.btn-primary, #information a.btn-primary, .contact-block + .dropdown-menu li div.contact-links a.btn-primary, .compare-page .table .btn-primary, #footer_custom_2 a.btn-primary, .brand-slider-item:hover .btn-default.inline-name, #menu .dropdown-menus a.btn-primary.menu-button, #cart .checkout a.btn-default, .contact-block + .dropdown-menu li div.contact-links a.btn-default {*/
/* 	{% if t1d_button_exclusive_bg_status == 1 %} */
/* 	background-color: {{ t1d_button_exclusive_bg_color }};*/
/* 	{% else %}*/
/* 	background-color: transparent;*/
/* 	{% endif %}*/
/* 	color: {{ t1d_button_exclusive_text_color }};*/
/* 	border: 2px solid {{ t1d_button_exclusive_bg_color }};*/
/* }*/
/* .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus {*/
/* 	{% if t1d_button_exclusive_bg_status == 1 %} */
/* 	background-color: {{ t1d_button_exclusive_bg_color }};*/
/* 	{% else %}*/
/* 	background-color: transparent;*/
/* 	{% endif %}*/
/* 	color: {{ t1d_button_exclusive_text_color }};*/
/* }*/
/* .ei-title h4 a.btn {*/
/* 	color: {{ t1d_button_exclusive_text_color }}!important;*/
/* }*/
/* */
/* .btn-primary:hover, .btn-primary:focus, .btn-primary:active, .btn-primary.active, .btn-primary.disabled, .btn-primary[disabled], #cart .btn-primary:hover, #footer_custom_top_1 a.btn-primary:hover, footer_custom_1 a.btn-primary:hover, #cart .checkout a.btn-default:hover, #information a.btn-primary:hover, .contact-block + .dropdown-menu li div.contact-links a.btn:hover, .compare-page .table .btn-primary:hover, #footer_custom_2 a.btn-primary:hover, .brand-slider-item .btn-default.inline-name, #menu .dropdown-menus a.btn-primary.menu-button:hover {*/
/* 	background-color: {{ t1d_button_exclusive_bg_hover_color }};*/
/*     color: {{ t1d_button_exclusive_text_hover_color }};*/
/* 	border: 2px solid {{ t1d_button_exclusive_bg_hover_color }};	*/
/* 	{% if t1d_button_shadow_status == 1 %}*/
/* 	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);*/
/* 	{% endif %}*/
/* }*/
/* .btn, .theme-modal-popup .modal-body .close, #product-tabs .nav-tabs li a {*/
/* 	border-radius: {{ t1d_button_border_radius }}px!important;*/
/* }*/
/* {% if t1d_button_shadow_status == 1 %}*/
/* #features .theme-modal:hover span.f-icon {*/
/* 	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Dropdowns  *//* */
/* .dropdown-menu, .dropdown-menus, .my-account-dropdown-menu li, #cart .dropdown-menu, #menu-mobile, .prev, .next {background-color: {{ t1d_dd_bg_color }};}*/
/* #cart span.name a, #cart span.quantity, .sidebar-nav > .sidebar-title, .sidebar-nav h5, #cart .dropdown-menu li div.cart-title .cart-total, #cart .table.cart-total span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .sidebar-nav li.category-mobile a {color: {{ t1d_dd_headings_color }};}*/
/* #cart .table.cart-total > tbody > tr > td, .dropdown-menu .datepicker, .prev-name, .next-name, .contact-block + .dropdown-menu li div.contact-details, .sidebar-nav li a {color: {{ t1d_dd_text_color }};}*/
/* #cart .table.cart-total > tbody > tr > td:first-child, #cart .dropdown-menu li p, #cart .dropdown-menu li div.cart-title i, #cart .dropdown-menu small, #cart span.price, #cart button.item-remove, .sidebar-nav li.dcjq-parent-li ul li a, .contact-block + .dropdown-menu li div.contact-details .col-sm-6, .information-block + .dropdown-menu li div.information-details a, .prev-arrow a, .next-arrow a {color: {{ t1d_dd_light_text_color }};}*/
/* .dropdown-menu a, #top .dropdown-menu a, #top .dropdown-menu .btn-link, .bootstrap-datetimepicker-widget td.old, .bootstrap-datetimepicker-widget td.new, #column-left .panel-default.panel-category .dropdown-menus a, #column-right .panel-default.panel-category .dropdown-menus a {color: {{ t1d_dd_links_color }};}*/
/* .dropdown-menu a:hover, #top .dropdown-menu a:hover, .my-account-dropdown-menu li.logout a:hover, #top .dropdown-menu li:hover .btn-link, .information-block + .dropdown-menu li div.information-details a:hover, .prev-name:hover, .next-name:hover, #column-left .panel-default.panel-category .dropdown-menus a:hover, #column-right .panel-default.panel-category .dropdown-menus a:hover {color: {{ t1d_dd_links_hover_color }};}*/
/* .top-links .my-account-dropdown-menu i {color: {{ t1d_dd_icons_color }};}*/
/* .top-links .my-account-dropdown-menu li a:hover i, #cart button.item-remove:hover, #contact-toggle-close span, #menu-mobile-toggle-close span, #highly-recommended-module-close span, .sidebar-nav li.home-mobile a, .sidebar-nav li a:hover, .sidebar-nav li.dcjq-parent-li ul li a:hover, #cart .dropdown-menu li div.cart-title .cart-total span {color: {{ t1d_dd_icons_hover_color }};}*/
/* .sidebar-nav > .sidebar-title, .my-account-dropdown-menu li.logout, #cart .checkout, #cart .cart-title, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-links, .information-block + .dropdown-menu li div.information-title, .prev-name, .next-name {background-color: {{ t1d_dd_hli_bg_color }};}*/
/* .my-account-dropdown-menu li, #form-currency li, #form-language li, .information-block + .dropdown-menu li div.information-details ul li, .menu_custom_menu .dropdown-menus a, #cart .table-striped > tbody > tr, #accordion-mobile li.dcjq-parent-li, #accordion-mobile .home-mobile, #accordion-mobile .menu_links {border-bottom: 1px solid {{ t1d_dd_separator_color }};}*/
/* .my-account-dropdown-menu li:nth-child(2n) {border-left: 1px solid {{ t1d_dd_separator_color }};}*/
/* .dropdown-menu, .dropdown-menus, #livesearch_search_results, .modal-content {*/
/* {% if t1d_dd_shadow == 1 %}*/
/* 	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);*/
/* {% else %}*/
/*     box-shadow: none;*/
/* {% endif %}*/
/* }*/
/* .prev, .next, .panel-category-dropdown .dropdown-menus {*/
/* {% if t1d_dd_shadow == 1 %}*/
/* 	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);*/
/* {% else %}*/
/*     box-shadow: none;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Fonts  *//* */
/* */
/* /*  Body  *//* */
/* body, button, select, .form-control, .menu_label, .tooltip-inner { */
/*     font-family: {{ t1d_body_font }},Arial,Helvetica,sans-serif; */
/* }*/
/* body, button, select, .form-control, #cart .table > tbody > tr > td, #menu .dropdown-menus, .category-slider-items .subcat a, .panel-category-dropdown .dropdown-menus a, .panel-inline-items .inline-name, .btn.theme-banner-title, .btn.theme-lookbook-item-title, .information-block + .dropdown-menu li div.information-details ul li a, .newsletter-block label, .filters label, #top .btn-group > .btn, .buttons-header, #cart > .btn, .btn-group > .dropdown-menu, .dropdown-menu, .dropdown-menus { */
/* 	font-size: {{ t1d_body_font_size }}px;*/
/* }*/
/* body, input, button, select, .form-control, .dropdown-menu.my-account-dropdown-menu li a, .newsletter-block label { */
/* {% if t1d_body_font_uppercase == 1 %}*/
/*     text-transform: uppercase;*/
/* {% else %}*/
/*     text-transform: none;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Small Text  *//* */
/* small, .small, label, #top-news-content, #top, #top #form-currency .btn-link, #top #form-language .btn-link, #menu .dropdown-menus a.see-all, .breadcrumb a, .product-thumb .product_box_brand a, .header-custom-box .header-custom-subtitle, span.badge.sale, span.badge.new, ul.pf-top li, .filters label, .filters a.list-group-item {*/
/* 	font-size: {{ t1d_small_font_size }}px;*/
/* }*/
/* */
/* /*  Headings and Product Name  *//* */
/* h1, h2, h3, h4, h5, h6, .panel-heading, .product-right-sm-info span.p-title, #features .f-title, .header-custom-title, legend, .sidebar-nav > .sidebar-title, #menu #menu_hor .dropdown-menus li.main-cat > a, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, #cart .dropdown-menu li p, #cart .dropdown-menu li div.cart-title, #cart span.name a, #cart span.quantity, #cart .table.cart-total > tbody > tr > td, .sidebar-nav li a, #tab-specification strong, #tab-review strong, #open-top-custom-block, .alert, .filters a.list-group-item, #cart #cart-tt #cart-total, #menu #homepage a, #menu .main-menu > li > a, #menu .dropdown-menus.flexMenu-popup > li > a, .price, .price-new, .price-old, .price-reg, .save-percent { */
/*     font-family: {{ t1d_title_font }},Arial,Helvetica,sans-serif; */
/* }*/
/* h1, h2, h3, h4, h5, h6, .panel-heading, .product-right-sm-info span.p-title, #features .f-title, .header-custom-title, legend, .sidebar-nav > .sidebar-title, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, #cart .dropdown-menu li div.cart-title, #cart span.name a, #cart span.quantity, #cart .table.cart-total > tbody > tr > td, .sidebar-nav li a, #tab-specification strong, #tab-review strong, #open-top-custom-block, .alert, .filters a.list-group-item, #cart #cart-tt #cart-total { */
/*     font-weight: {{ t1d_title_font_weight }}; */
/* }*/
/* h1, h2, h3, h4, h5, h6, .panel-heading, .product-right-sm-info span.p-title, #features .f-title, .header-custom-title, legend, .sidebar-nav > .sidebar-title, #menu #menu_hor .dropdown-menus li.main-cat > a, .information-block + .dropdown-menu li div.information-title, .information-block + .dropdown-menu li div.information-details span, .contact-block + .dropdown-menu li div.contact-title, .contact-block + .dropdown-menu li div.contact-details span, #cart .dropdown-menu li div.cart-title, #cart span.name a, #cart .table.cart-total > tbody > tr > td, .sidebar-nav li a, #tab-specification strong, #tab-review strong, #open-top-custom-block, .alert, .filters a.list-group-item  { */
/* {% if t1d_title_font_uppercase == 1 %}*/
/*     text-transform: uppercase;*/
/* {% else %}*/
/*     text-transform: none;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Subtitle  *//* */
/* .subtitle, #content .tltblog .row .col-sm-9 h4 + div p {*/
/* 	font-family: {{ t1d_subtitle_font }},Arial,Helvetica,sans-serif; */
/*     font-size: {{ t1d_subtitle_font_size }}px;*/
/*     font-weight: {{ t1d_subtitle_font_weight }};*/
/* 	font-style: {{ t1d_subtitle_font_style }};*/
/* {% if t1d_subtitle_font_uppercase == 1 %}*/
/*     text-transform: uppercase;*/
/* {% else %}*/
/*     text-transform: none;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Prices  *//* */
/* .price, .price-new, .price-old, .price-reg, .save-percent { */
/*     font-weight: {{ t1d_price_font_weight }}; */
/* }*/
/* */
/* /*  Buttons  *//* */
/* .btn-default, .btn-primary, .btn-danger {*/
/*     font-size: {{ t1d_button_font_size }}px;*/
/*     font-weight: {{ t1d_button_font_weight }}; */
/* {% if t1d_button_font_uppercase == 1 %}*/
/*     text-transform: uppercase;*/
/* {% else %}*/
/*     text-transform: none;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Main Menu Bar  *//* */
/* #menu #homepage a, #menu .main-menu > li > a, #menu .dropdown-menus.flexMenu-popup > li > a {*/
/* 	font-size: {{ t1d_mm_font_size }}px;*/
/*     font-weight: {{ t1d_mm_font_weight }};*/
/* {% if t1d_mm_font_uppercase == 1 %}*/
/*     text-transform: uppercase;*/
/* {% else %}*/
/*     text-transform: none;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Sub-Menu Main Categories  *//* */
/* #menu #menu_hor .dropdown-menus li.main-cat > a {*/
/* 	font-size: {{ t1d_mm_sub_main_cat_font_size }}px;*/
/*     font-weight: {{ t1d_mm_sub_main_cat_font_weight }};*/
/* }*/
/* */
/* /*  Sub-Menu Other Links  *//* */
/* #menu .dropdown-menus a {*/
/* 	font-size: {{ t1d_mm_sub_font_size }}px;*/
/*     font-weight: {{ t1d_mm_sub_font_weight }};*/
/* }*/
/* */
/* </style>*/
/* {% else %}*/
/* <link rel="stylesheet" type="text/css" href="catalog/view/theme/oxy/stylesheet/oxy-{{ t1d_skin }}.css" />*/
/* {% endif %}*/
/* */
/* <style type="text/css">*/
/* */
/* /*  Layout  *//* */
/* @media (max-width: 767px) {*/
/* #content .product-layout:nth-child(2n+1) {*/
/* 	clear: both;*/
/* }*/
/* }*/
/* @media (min-width: 768px) {*/
/* {% if t1o_product_grid_per_row =='6' %}*/
/* #content .product-layout:nth-child(2n+1) {*/
/* 	clear: both;*/
/* }*/
/* {% endif %}*/
/* {% if t1o_product_grid_per_row =='4' %}*/
/* #content .product-layout:nth-child(3n+1) {*/
/* 	clear: both;*/
/* }*/
/* {% endif %}*/
/* {% if t1o_product_grid_per_row =='3' %}*/
/* #content .product-layout:nth-child(4n+1) {*/
/* 	clear: both;*/
/* }*/
/* {% endif %}*/
/* {% if t1o_product_grid_per_row =='15' %}*/
/* #content .product-layout:nth-child(5n+1) {*/
/* 	clear: both;*/
/* }*/
/* {% endif %}*/
/* {% if t1o_product_grid_per_row =='2' %}*/
/* #content .product-layout:nth-child(6n+1) {*/
/* 	clear: both;*/
/* }*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Logo Creator  *//* */
/* #logo .logo-creator a {color: {{ t1o_text_logo_color }};}*/
/* #logo i {color: {{ t1o_text_logo_awesome_color }};}*/
/* */
/* /*  Top Promo Message Slider  *//* */
/* #top-custom-block-content {*/
/* 	display:none;*/
/* }*/
/* #open-top-custom-block i {*/
/* 	color: {{ t1o_top_custom_block_awesome_color }};*/
/* }*/
/* #open-top-custom-block i.fa-plus, #open-top-custom-block i.fa-minus {*/
/* 	color: {{ t1o_top_custom_block_title_color }};*/
/* }*/
/* */
/* #open-top-custom-block {*/
/* 	background-color: {{ t1o_top_custom_block_title_bar_bg_color }};*/
/* 	color: {{ t1o_top_custom_block_title_color }};*/
/* {% if t1o_top_custom_block_bar_bg !='' %}*/
/* 	background-image: url("{{ path_image ~ t1o_top_custom_block_bar_bg }}");*/
/* 	background-position: center top;*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* }*/
/* */
/* #top-custom-block-wrapper {*/
/* 	background-color: {{ t1o_top_custom_block_bg_color }};*/
/* 	color: {{ t1o_top_custom_block_text_color }};*/
/* {% if t1o_top_custom_block_bg !='' %}*/
/* 	background-image: url("{{ path_image ~ t1o_top_custom_block_bg }}");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* }*/
/* {% if t1o_layout_style =='framed' %}*/
/* #top-custom-block-wrapper {*/
/*     margin-bottom: 15px;*/
/* }*/
/* {% endif %}*/
/* */
/* */
/* /*  Layout  *//* */
/* */
/* {% if t1o_layout_l =='2' %}*/
/* .wrapper.framed {*/
/*     max-width: 980px;*/
/* }*/
/* .wrapper.framed .is-sticky .container {*/
/* 	width: 920px !important;*/
/* }*/
/* {% endif %}*/
/* .wrapper.framed {*/
/* {% if t1o_layout_framed_align =='0' %}*/
/* 	margin: 0 auto 30px;*/
/* {% else %}*/
/*     margin: 0 30px 30px;*/
/* {% endif %}*/
/* }*/
/* */
/* .wrapper.full-width .full-width-container {*/
/* {% if t1o_layout_full_width_max =='1' %}*/
/* 	max-width: 1440px;*/
/* {% elseif t1o_layout_full_width_max =='2' %}*/
/*     max-width: 1230px;*/
/* {% else %}*/
/*     max-width: inherit;*/
/* {% endif %}*/
/* }*/
/* */
/* {% if t1o_layout_catalog_mode =='1' %}*/
/* #top .top-links, #top #currency, #cart, .price, .price-new, .price-old, .product-buy ul.pp, .flybar-items, .cart, .product-buy #product, .wishlist, .compare, #compare-total, span.badge.sale, .alert, .quickview-success {*/
/*     display: none;*/
/* }*/
/* .product-list .list-quickview {*/
/* 	margin-top: 20px;*/
/* }*/
/* .product-grid > div:hover .flybar, .product-gallery > div:hover .flybar, #content .box-product > div:hover .flybar, .product-box-slider-flexslider ul > li:hover .flybar, .product-bottom-related-flexslider ul > li:hover .flybar {*/
/* 	bottom: -10px;*/
/* }*/
/* @media (max-width: 767px) {*/
/* .buttons-header {width: 50%;}*/
/* #search-block {border-left: none!important;}*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Top Bar  *//* */
/* {% if t1o_top_bar_status =='0' %}*/
/* #top {display: none;}*/
/* {% endif %}*/
/* */
/* /*  MAIN MENU *//* */
/* */
/* /*  Main Menu Labels  *//* */
/* {% for i in 1..15 %} */
/* #menu_label{{ i }}:after { background-color:{{ t1o_menu_labels_color[i] }}; }*/
/* {% endfor %}*/
/* */
/* /*  Custom Bar below Main Menu  *//* */
/* #custom-bar-wrapper {*/
/* 	background-color: {{ t1o_custom_bar_below_menu_bg_color }};*/
/* 	color: {{ t1o_custom_bar_below_menu_text_color }};*/
/* {% if t1o_custom_bar_below_menu_bg !='' %}*/
/* 	background-image: url("{{ path_image ~ t1o_custom_bar_below_menu_bg }}");*/
/* {% else %}*/
/* 	background-image: none;*/
/* {% endif %}*/
/* {% if t1o_custom_bar_below_menu_bg_animation =='1' %}*/
/*     animation: animatedBackground 40s linear infinite;*/
/* {% endif %}*/
/* }*/
/* */
/* /*  Category Page  *//* */
/* .category_top_title h1 {color: {{ t1o_category_title_above_color }};}*/
/* {% if t1o_category_title_position == 0 %}*/
/* #content .category-list-carousel.owl-carousel {*/
/* 	margin: 30px 0 0 0;*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Product Box  *//* */
/* {% if t1o_category_prod_name_status == 0 %}	*/
/* .product-grid .name, .product-gallery .name, #content .box-product .name, .product-bottom-related .name, .product-box-slider .name, .product-right-sm-related .name {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_brand_status == 0 %}	*/
/* .product_box_brand {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_price_status == 0 %}	*/
/* .product-grid .price, #content .box-product .price, .product-list .price, #content .product-list.product-small-list .product-thumb .price, .product-gallery .price, .product-bottom-related .price, .product-box-slider .price, .product-right-sm-related p.price {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_quickview_status == 0 %}	*/
/* a.list-quickview, .product-grid.product-box-style-1 .list-quickview, .product-grid a.quickview, .product-gallery a.quickview, .product-items-theme-featured .product-grid .flybar .flybar-items .quickview {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_cart_status == 0 %}	*/
/* .product-grid .cart, .product-grid.product-box-style-1 .caption .cart, .flybar .flybar-items button.btn-primary, #content .box-product .cart, .product-list .cart, .product-box-slider .cart, .product-items-theme-featured .product-grid .flybar .flybar-items button.btn-primary {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_ratings_status == 0 %}	*/
/* .product-grid .rating, .product-list .rating, #content .product-list.product-small-list .product-thumb .rating, .product-gallery .rating, #content .box-product .rating, #column-left .box-product .rating, #column-right .box-product .rating, .product-box-slider .rating, .product-right-sm-related .rating {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_wis_com_status == 0 %}	*/
/* .product-list .list-wishlist, .product-grid .wishlist, .product-gallery .wishlist, .product-list .list-compare, .product-grid .compare, .product-gallery .compare {display: none;}*/
/* {% endif %}*/
/* {% if t1o_category_prod_zoom_status == 1 %}	*/
/* #content .product-grid div:hover .image a img, #content .box-product div:hover .image a img {*/
/* 	transform: scale(1.1);*/
/*     -moz-transform: scale(1.1);*/
/*     -webkit-transform: scale(1.1);*/
/*     -o-transform: scale(1.1);*/
/*     -ms-transform: scale(1.1);*/
/* }*/
/* {% endif %}*/
/* {% if t1o_category_prod_swap_status == 0 %}*/
/* .thumb_swap {display:none}*/
/* {% endif %}*/
/* {% if t1o_category_prod_shadow_status == 1 %}*/
/* #content .product-thumb:hover {box-shadow: 0 2px 4px rgba(0, 0, 0, 0.2), 0 -1px 0 rgba(0, 0, 0, 0.02);}*/
/* {% endif %}*/
/* {% if t1o_category_prod_lift_up_status == 1 %}*/
/* #content .product-grid .product-thumb:hover {transform: translateY(-5px);}*/
/* {% endif %}*/
/* .product-grid .name, #content .box-product .name, #content .box-product .product_box_brand, .product-bottom-related .name, .product-box-slider .name, .product-grid .product_box_brand, .product-box-slider .product_box_brand, .product-bottom-related-flexslider .product_box_brand, .product-bottom-related-flexslider .rating, .tab-content-products .product_box_brand, .product-grid .price, #content .box-product .price, .product-box-slider .price, .product-bottom-related .price, .product-grid .rating, #content .box-product .rating, .product-box-slider .rating, .product-grid .cart, #content .box-product .cart, .product-box-slider .cart, .product-bottom-related .cart, .product-right-sm-related .name, .product-right-sm-related .product_box_brand, .product-right-sm-related .rating, .product-right-sm-related p.price, .product-grid .product-list-buttons {*/
/* {% if t1o_category_prod_align == 1 %}	*/
/* 	text-align: left;*/
/* 	{% else %}*/
/* 	text-align: center;*/
/* {% endif %}*/
/* }*/
/* {% if t1o_category_prod_name_status == 0 and t1o_category_prod_brand_status == 0 and t1o_category_prod_price_status == 0 and t1o_category_prod_ratings_status == 0 %}*/
/* .product-grid .product-thumb { padding-bottom: 0; }*/
/* {% endif %}*/
/* {% if t1o_layout_h_align == 0 %}	*/
/* h1, h2, h3, .cat-description, .cat-description-above-content, #product-tabs .nav-tabs, .module-style-2 .panel-inline-title h2, .module-style-2 .panel-inline-subtitle {*/
/* 	text-align: center;*/
/* }*/
/* .module-style-2 .panel-inline-content .panel-inline-subtitle {*/
/* 	padding: 0 25%;*/
/* }*/
/* #content .product-buy h1 {*/
/* 	text-align: left;*/
/* }*/
/* #content .product-buy h1:before {*/
/* 	margin-left: 0;*/
/* }*/
/* .pf-top {*/
/* 	text-align: center;*/
/* }*/
/* {% endif %}*/
/* */
/* */
/* /*  Product Page  *//* */
/* {% if t1o_product_prev_next_status == 0 %}  */
/* .prev, .next {display: none;}*/
/* {% endif %}*/
/* */
/* {% if t1o_layout_product_page == 11 or t1o_layout_product_page == 12 %}*/
/* @media (min-width: 768px) {*/
/* .product-buy {*/
/* 	float: left;*/
/* }*/
/* .product-left {*/
/* 	float: right;*/
/* }*/
/* }*/
/* {% endif %}*/
/* {% if t1o_layout_product_page == 13 %}*/
/* #content .product-buy h2 {*/
/* 	margin: 0 0 30px;*/
/* }*/
/* .product-special-price .col-md-4, span.rating-stars {*/
/* 	width: 100%;*/
/* 	display: block;*/
/* }*/
/* .product-buy .rating a {*/
/* 	margin-left: 0;*/
/* 	margin-right: 15px;*/
/* }*/
/* .product-buy .rating .fa-pencil {*/
/* 	margin-right: 3px;*/
/* }*/
/* .col-sm-5.product-buy .product-buy-wrapper {*/
/* 	background-color: transparent;*/
/* 	padding: 0;*/
/* }*/
/* .product-buy .form-group .btn-primary {*/
/* 	padding: 15.5px 10px;*/
/* 	width: 100%;*/
/* }*/
/* input.inc {*/
/* 	margin-right: 0;*/
/* }*/
/* @media (min-width: 768px) and (max-width: 991px) {*/
/* .product-page .product-left, .product-page .product-buy {*/
/* 	width: 100%;*/
/* }*/
/* }*/
/* {% endif %}*/
/* {% if t1o_product_align == 0 %}*/
/* #content .product-buy h1, #content .product-buy h2, ul.pf, .product-buy ul.pp, .product-buy .rating, .product-buy-custom-block, #product legend {*/
/* 	text-align: center;*/
/* }*/
/* #content .product-buy h1:before {*/
/* 	margin-left: 45% !important;*/
/* }*/
/* div.qty, .product-buy .form-group .btn-default.wcs {*/
/* 	text-align: left;*/
/* }*/
/* {% endif %}*/
/* */
/* /*  Snapchat Widget  *//* */
/* .snapchat_box {*/
/*     background-color: {{ t1o_snapchat_box_bg }};*/
/* }*/
/* */
/* /*  Video Widget  *//* */
/* .video_box_wrapper #video_box_icon {*/
/*     background-color: {{ t1o_video_box_bg }};*/
/* }*/
/* .video_box_wrapper .video_box {*/
/*     border: 4px solid {{ t1o_video_box_bg }};*/
/* }*/
/*  */
/* /*  Custom Content Widget  *//* */
/* .custom_box_wrapper #custom_box_icon {*/
/*     background-color: {{ t1o_custom_box_bg }};*/
/* }*/
/* .custom_box_wrapper .custom_box {*/
/*     border: 4px solid {{ t1o_custom_box_bg }};*/
/* }*/
/* */
/* */
/* /*  FOOTER  *//* */
/* */
/* /*  Bottom Custom Block 2  *//* */
/* {% if t1o_custom_bottom_2_status == 1 %}*/
/* @media (min-width: 768px) {*/
/* .wrapper.full-width footer {*/
/* 	margin-bottom: {{ t1o_custom_bottom_2_footer_margin }}px;*/
/* }*/
/* #footer_custom_2 {*/
/* 	height: {{ t1o_custom_bottom_2_footer_margin }}px;*/
/* }*/
/* }*/
/* {% endif %}*/
/* */
/* */
/* /*  Other  *//* */
/* {% if t1o_eu_cookie_icon_status == 0 %}*/
/* span.cookie-img {*/
/* 	display: none;*/
/* }*/
/* {% endif %}*/
/* */
/* </style>*/
/* */
/* {% if t1o_custom_css !='' %}*/
/* <style type="text/css">*/
/* /*  Custom CSS *//* */
/* {{ t1o_custom_css }}*/
/* </style>*/
/* {% endif %}*/
/* */
/* <link href='//fonts.googleapis.com/css?family={{ t1d_body_font|replace({' ': '+'}) }}:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% if t1d_title_font != t1d_body_font %}*/
/* <link href='//fonts.googleapis.com/css?family={{ t1d_title_font|replace({' ': '+'}) }}:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% endif %}*/
/* {% if t1d_subtitle_font != t1d_body_font %}*/
/* <link href='//fonts.googleapis.com/css?family={{ t1d_subtitle_font|replace({' ': '+'}) }}:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% endif %}*/
/* */
/* {% if t1d_skin =='skin2-luxury' %}	*/
/* <link href='//fonts.googleapis.com/css?family=Merriweather:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% endif %}*/
/* {% if t1d_skin =='skin3-furniture' %}	*/
/* <link href='//fonts.googleapis.com/css?family=Karla:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% endif %}*/
/* {% if t1d_skin =='skin4-market' %}	*/
/* <link href='//fonts.googleapis.com/css?family=Montserrat:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* <link href='//fonts.googleapis.com/css?family=Maven+Pro:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% endif %}*/
/* {% if t1d_skin =='skin5-minimal' %}	*/
/* <link href='//fonts.googleapis.com/css?family=Montserrat:400,400i,300,700,900&amp;subset=latin,cyrillic-ext,latin-ext,cyrillic,greek-ext,greek,vietnamese' rel='stylesheet' type='text/css'>*/
/* {% endif %}*/
/* */
/* <script>*/
/* $(function(){*/
/*   $.stellar({*/
/*     horizontalScrolling: false,*/
/* 	verticalOffset: 0*/
/*   });*/
/* });*/
/* </script>*/
/* */
/* {% for analytic in analytics %}*/
/* {{ analytic }}*/
/* {% endfor %}*/
/* */
/* </head>*/
/* <body class="{{ class }}">*/
/* */
/* {% if t1o_facebook_likebox_status == 1 and t1o_facebook_likebox_id != '' %}  */
/* <div class="facebook_wrapper hidden-xs">*/
/* <div id="facebook_icon"><i class="fa fa-facebook"></i></div>*/
/* <div class="facebook_box">*/
/* <div id="fb-root"></div>*/
/* <script>(function(d, s, id) {*/
/*   var js, fjs = d.getElementsByTagName(s)[0];*/
/*   if (d.getElementById(id)) return;*/
/*   js = d.createElement(s); js.id = id;*/
/*   js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";*/
/*   fjs.parentNode.insertBefore(js, fjs);*/
/* }(document, 'script', 'facebook-jssdk'));</script>*/
/* <div class="fb-page" data-href="https://www.facebook.com/{{ t1o_facebook_likebox_id }}" data-width="327" data-height="389" data-colorscheme="light" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".facebook_wrapper").hover(function(){            */
/*         $(".facebook_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );        */
/*         },*/
/*   function(){            */
/*         $(".facebook_wrapper").stop(true, false).animate({right: "-335" }, 800, 'easeInQuint' );        */
/*         },1000);    */
/*   }); */
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/*  */
/* {% if t1o_twitter_block_status == 1 and t1o_twitter_block_user != '' %} */
/* <div class="twitter_wrapper hidden-xs">*/
/* <div id="twitter_icon"><i class="fa fa-twitter"></i></div>*/
/* <div class="twitter_box">*/
/*   {% if t1o_twitter_block_user != '' %}*/
/*   <p><a class="twitter-timeline"  href="https://twitter.com/{{ t1o_twitter_block_user }}" data-chrome="noheader nofooter noborders noscrollbar transparent" data-tweet-limit="{{ t1o_twitter_block_tweets }}"  data-widget-id="{{ t1o_twitter_block_widget_id }}" data-theme="light" data-related="twitterapi,twitter" data-aria-polite="assertive">Tweets by {{ t1o_twitter_block_user }}</a>*/
/* <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></p>*/
/*   {% endif %}*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".twitter_wrapper").hover(function(){            */
/*         $(".twitter_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );        */
/*         },*/
/*   function(){            */
/*         $(".twitter_wrapper").stop(true, false).animate({right: "-335" }, 800, 'easeInQuint' );        */
/*         },1000);    */
/*   });*/
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_googleplus_box_status == 1 and t1o_googleplus_box_user != '' %}*/
/* <div class="googleplus_wrapper hidden-xs">*/
/* <div id="googleplus_icon"><i class="fa fa-google-plus"></i></div>*/
/* <div class="googleplus_box">*/
/* <script src="//apis.google.com/js/platform.js" async defer></script>*/
/* <div class="g-page" data-href="//plus.google.com/{{ t1o_googleplus_box_user }}" data-rel="publisher"></div>*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".googleplus_wrapper").hover(function(){            */
/*         $(".googleplus_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );        */
/*         },*/
/*   function(){            */
/*         $(".googleplus_wrapper").stop(true, false).animate({right: "-308" }, 800, 'easeInQuint' );        */
/*         },1000);    */
/*   });  */
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_pinterest_box_status == 1 and t1o_pinterest_box_user != '' %}*/
/* <div class="pinterest_wrapper hidden-xs">*/
/* <div id="pinterest_icon"><i class="fa fa-pinterest"></i></div>*/
/* <div class="pinterest_box">*/
/* <script async defer src="//assets.pinterest.com/js/pinit.js"></script>*/
/* <a data-pin-do="embedUser" data-pin-board-width="307" data-pin-scale-height="390" data-pin-scale-width="115" href="//www.pinterest.com/{{ t1o_pinterest_box_user }}/"></a>*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".pinterest_wrapper").hover(function(){            */
/*         $(".pinterest_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );        */
/*         },*/
/*   function(){            */
/*         $(".pinterest_wrapper").stop(true, false).animate({right: "-335" }, 800, 'easeInQuint' );        */
/*         },1000);    */
/*   });*/
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_snapchat_box_status == 1 and t1o_snapchat_box_code_custom != '' %} */
/* <div class="snapchat_wrapper hidden-xs">*/
/* <div id="snapchat_icon"><i class="fa fa-snapchat-ghost" aria-hidden="true"></i></div>*/
/* <div class="snapchat_box">*/
/* <img src="{{ path_image ~ t1o_snapchat_box_code_custom }}" alt="Snapchat" title="Snapchat">*/
/* <h3 class="text-center">{{ t1o_snapchat_box_title }}</h3>*/
/* <p class="subtitle text-center">{{ t1o_snapchat_box_subtitle }}</p>*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".snapchat_wrapper").hover(function(){            */
/*         $(".snapchat_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );       */
/*         },*/
/*   function(){            */
/*         $(".snapchat_wrapper").stop(true, false).animate({right: "-320" }, 800, 'easeInQuint' );      */
/*         },1000);    */
/*   }); */
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_video_box_status == 1 and t1o_video_box_content %}  */
/* <div class="video_box_wrapper hidden-xs">*/
/* <div id="video_box_icon"><i class="fa fa-youtube-play"></i></div>*/
/* <div class="video_box">*/
/*   {{ t1o_video_box_content }}*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".video_box_wrapper").hover(function(){            */
/*         $(".video_box_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );        */
/*         },*/
/*   function(){            */
/*         $(".video_box_wrapper").stop(true, false).animate({right: "-588" }, 800, 'easeInQuint' );      */
/*         },1000);    */
/*   });  */
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_custom_box_status == 1 and t1o_custom_box_content %} */
/* <div class="custom_box_wrapper hidden-xs">*/
/* <div id="custom_box_icon">*/
/* <i class="fa fa-chevron-left"></i>*/
/* </div>*/
/* <div class="custom_box">*/
/*   {{ t1o_custom_box_content }}*/
/*   <script type="text/javascript">    */
/*   $(function(){        */
/*         $(".custom_box_wrapper").hover(function(){            */
/*         $(".custom_box_wrapper").stop(true, false).animate({right: "0" }, 800, 'easeOutQuint' );        */
/*         },*/
/*   function(){            */
/*         $(".custom_box_wrapper").stop(true, false).animate({right: "-335" }, 800, 'easeInQuint' );      */
/*         },1000);    */
/*   }); */
/*   </script>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_top_custom_block_status == 1 %}*/
/* <div id="top-custom-block-wrapper">*/
/*    <div id="top-custom-block-content">*/
/*    {{ t1o_top_custom_block_content }}*/
/*    </div>*/
/*    <a id="open-top-custom-block" href="#">*/
/*      {% if t1o_top_custom_block_awesome_status == 1 %}*/
/*      <i class="fa fa-{{ t1o_top_custom_block_awesome }}"></i>*/
/*      {% endif %}*/
/*      {{ t1o_top_custom_block_title[lang_id] }}*/
/*      {% if t1o_top_custom_block_awesome_status == 1 %}*/
/*      <i class="fa fa-{{ t1o_top_custom_block_awesome }}"></i>*/
/*      {% endif %}*/
/*      <i class="fa fa-plus"></i>*/
/*      <i class="fa fa-minus"></i>*/
/*    </a>  */
/* </div>*/
/* <script type="text/javascript">*/
/* $('#open-top-custom-block').click(function() {*/
/*   $(this).toggleClass('open');*/
/*     $('#top-custom-block-content').slideToggle('fast');*/
/* 	return false;*/
/*   });*/
/* </script>*/
/* {% endif %}*/
/* */
/* <div class="sidebar-opacity"></div>*/
/* <div id="wrapper" class="wrapper {{ t1o_layout_style }}">*/
/* */
/* {% if t1o_news_status == 1 %}*/
/* <div id="top-news-wrapper">*/
/*    <div id="top-news-content" class="container">*/
/*       <span id="top-news"><i class="fa fa-list-alt"></i> {{ t1o_text_news[lang_id] }}:</span>*/
/*       <ul id="ticker">*/
/*       {% for i in 1..10 %}*/
/*       {% if t1o_news[i].status == 1 %}*/
/*       {% if t1o_news[i][lang_id].title !='' or t1o_news[i].url !='' %}*/
/*         <li>*/
/* 		  <a href="{{ t1o_news[i].url }}">*/
/*           {{ t1o_news[i][lang_id].title }}*/
/*           </a>*/
/*         </li>*/
/*       {% endif %}*/
/*       {% endif %}*/
/*       {% endfor %}*/
/*       </ul> */
/*    </div> */
/* </div>*/
/* {% endif %}*/
/* */
/* <header class="{{ t1o_header_style }}">*/
/* */
/* <nav id="top">*/
/*   <div class="container"> */
/*     <div class="top-links nav pull-left">*/
/*       <ul class="list-inline">*/
/*         {% if t1o_top_bar_welcome_status == 1 %}*/
/*         <li class="hidden-xs hidden-sm">*/
/*         {% if t1o_top_bar_welcome_awesome !='' %}*/
/*           <i class="fa fa-{{ t1o_top_bar_welcome_awesome }}"></i>*/
/*         {% else %}*/
/*           <i class="fa fa-flag"></i>*/
/* 	    {% endif %}*/
/*         <span>{{ t1o_top_bar_welcome[lang_id] }}</span></li>*/
/*         {% endif %}*/
/*         <li class="hidden-xs"><a href="{{ contact }}"><i class="fa fa-phone"></i> <span>{{ telephone }}</span></a></li>*/
/*         <li><a href="{{ contact }}"><i class="fa fa-envelope"></i> <span class="hidden-xs">{{ email }}</span></a></li> */
/*       </ul>*/
/*     </div>*/
/*     */
/*     {{ currency }}*/
/*     {{ language }}*/
/*     */
/*     <div class="top-links nav pull-right">*/
/*     <ul class="list-inline">*/
/*         <li class="dropdown"><a href="{{ account }}" title="{{ text_account }}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm">{{ text_account }}</span> <i class="fa fa-angle-down"></i></a>*/
/*           <ul class="dropdown-menu dropdown-menu-right my-account-dropdown-menu">*/
/*             {% if logged %}*/
/*             <li class="col-xs-6"><a href="{{ account }}"><i class="fa fa-cog"></i><br>{{ text_account }}</a></li>*/
/*             <li class="col-xs-6"><a href="{{ order }}"><i class="fa fa-inbox"></i><br>{{ text_order }}</a></li>*/
/*             <li class="col-xs-6"><a href="{{ transaction }}"><i class="fa fa-credit-card"></i><br>{{ text_transaction }}</a></li>*/
/*             <li class="col-xs-6"><a href="{{ download }}"><i class="fa fa-download"></i><br>{{ text_download }}</a></li>*/
/*             <li class="logout"><a href="{{ logout }}">{{ text_logout }}</a></li>*/
/*             {% else %}*/
/*             <li class="col-xs-6 register"><a href="{{ register }}"><i class="fa fa-pencil"></i><br>{{ text_register }}</a></li>*/
/*             <li class="col-xs-6"><a href="{{ login }}"><i class="fa fa-user"></i><br>{{ text_login }}</a></li>*/
/*             {% endif %}*/
/*           </ul>*/
/*         </li>*/
/*         <li><a href="{{ wishlist }}" id="wishlist-total" title="{{ text_wishlist }}"><i class="fa fa-heart"></i> <span class="hidden-xs hidden-sm">{{ text_wishlist }}</span></a></li>*/
/*         {% if t1o_top_bar_cart_link_status == 1 %}*/
/*         <li><a href="{{ shopping_cart }}" title="{{ text_shopping_cart }}"><i class="fa fa-shopping-cart hidden-md hidden-lg"></i> <span class="hidden-xs hidden-sm">{{ text_shopping_cart }}</span></a></li>*/
/*         {% endif %}*/
/*         <!--<li><a href="{{ checkout }}" title="{{ text_checkout }}"><i class="fa fa-share hidden-md hidden-lg"></i> <span class="hidden-xs hidden-sm">{{ text_checkout }}</span></a></li>-->      */
/*       </ul>*/
/*     </div>*/
/*     </div>*/
/* </nav>*/
/*   */
/*   <div class="container">*/
/*     <div class="row">*/
/*     */
/*       <div class="col-sm-12 header-items">*/
/*       */
/*       <div class="dropdown contact-block-wrapper">*/
/*       <a data-toggle="dropdown" class="btn dropdown-toggle contact-block hidden-xs hidden-sm">*/
/*       <div id="contact-block" class="buttons-header" data-toggle="tooltip" title="{{ t1o_text_contact_us[lang_id] }}">*/
/*         <div class="button-i"><i class="fa fa-envelope"></i></div>*/
/*       </div>*/
/*       </a>*/
/*       <ul class="dropdown-menu pull-right">*/
/*         <li>*/
/*           <div class="contact-title">{{ t1o_text_contact_us[lang_id] }}</div>*/
/*         </li>*/
/*         <li>*/
/*           <div class="contact-details">*/
/*           */
/*           <div class="row">*/
/*           <div class="col-sm-6">*/
/*           <span>{{ t1o_text_menu_contact_tel[lang_id] }}</span><br>*/
/*           {{ telephone }}*/
/*           </div>*/
/*           <div class="col-sm-6">*/
/*           <span>{{ t1o_text_menu_contact_email[lang_id] }}</span><br>*/
/*           {{ email }}*/
/*           </div>*/
/*           </div>*/
/*           */
/*           <div class="row">*/
/*           <div class="col-sm-6">*/
/*           <span>{{ t1o_text_menu_contact_address[lang_id] }}</span><br>*/
/*           {{ address }}*/
/*           </div>*/
/*           <div class="col-sm-6">*/
/*           {% if open %}*/
/*           <span>{{ t1o_text_menu_contact_hours[lang_id] }}</span><br>*/
/*           {{ open }}*/
/*           {% endif %}*/
/*           </div>*/
/*           </div>*/
/* */
/*           {% if comment %}*/
/*           <div class="row">*/
/*           <div class="col-sm-12 comment">*/
/*           {{ comment }}*/
/*           </div>*/
/*           </div>*/
/*           {% endif %}*/
/*           */
/*           </div>*/
/*         </li>*/
/*         <li>*/
/*         <div class="contact-links"><a href="https://maps.google.com/maps?q={{ geocode|url_encode }}&hl={{ geocode_hl }}&t=m&z=15" class="btn btn-xs btn-default popup-gmaps" target="_blank">{{ button_map }}</a>&nbsp;&nbsp;&nbsp;<a href="{{ contact }}" class="btn btn-xs btn-primary">{{ t1o_text_menu_contact_form[lang_id] }}</a></div>*/
/*         </li>*/
/*       </ul>*/
/*       </div>*/
/*       <script>*/
/*       $(document).ready(function() {*/
/*         $('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({*/
/*           disableOn: 700,*/
/*           type: 'iframe',*/
/*           mainClass: 'mfp-fade',*/
/*           removalDelay: 160,*/
/*           preloader: false,*/
/*           fixedContentPos: false*/
/*         });*/
/*       });*/
/* 	  </script>*/
/*   */
/*      */
/*       <div class="dropdown information-block-wrapper">      */
/*       <a data-toggle="dropdown" class="btn dropdown-toggle information-block hidden-xs hidden-sm">*/
/*       <div id="information-block" class="buttons-header" data-toggle="tooltip" title="{{ text_information }}">*/
/*         <div class="button-i"><i class="fa fa-bars"></i></div>*/
/*       </div>*/
/*       </a>*/
/*       <ul class="dropdown-menu pull-right">*/
/*         <li>*/
/*           <div class="information-title">{{ text_information }}</div>*/
/*         </li>*/
/*         <li>*/
/*           <div class="information-details">*/
/*           */
/*           <div class="row">*/
/*           <div class="col-sm-4">*/
/*           <span>{{ text_information }}</span><br>*/
/*           <ul>*/
/*           {% for information in informations %} */
/*           <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*           {% endfor %}*/
/*           </ul>*/
/*           </div>*/
/*           <div class="col-sm-4">*/
/*           <span>{{ text_service }}</span><br>*/
/*           <ul>*/
/*           {% if t1o_i_c_2_1_status == 1 %}<li><a href="{{ contact }}">{{ text_contact }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_2_status == 1 %}<li><a href="{{ account }}">{{ text_account }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_3_status == 1 %}<li><a href="{{ return }}">{{ text_return }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_4_status == 1 %}<li><a href="{{ order }}">{{ text_order }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_5_status == 1 %}<li><a href="{{ wishlist }}">{{ text_wishlist }}</a></li>{% endif %}*/
/*           </ul>*/
/*           </div>*/
/*           <div class="col-sm-4">*/
/*           <span>{{ text_extra }}</span><br>*/
/*           <ul>*/
/*           {% if t1o_i_c_3_1_status == 1 %}<li><a href="{{ manufacturer }}">{{ t1o_text_menu_brands[lang_id] }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_2_status == 1 %}<li><a href="{{ voucher }}">{{ text_voucher }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_3_status == 1 %}<li><a href="{{ affiliate }}">{{ text_affiliate }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_4_status == 1 %}<li><a href="{{ special }}">{{ text_special }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_5_status == 1 %}<li><a href="{{ newsletter }}">{{ text_newsletter }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_6_status == 1 %}<li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>{% endif %}*/
/*           </ul>*/
/*           </div>*/
/*           </div>*/
/*           */
/*           </div>*/
/*         </li>*/
/*       </ul>*/
/*       </div>*/
/* */
/*         <div id="logo">*/
/*         <div id="logo-table">*/
/*         <div id="logo-table-cell">*/
/*           {% if logo %}*/
/*           <a href="{{ home }}"><img src="{{ logo }}" title="{{ name }}" alt="{{ name }}" /></a>*/
/*           {% else %}*/
/*           <div class="logo-creator"><a href="{{ home }}">{% if t1o_text_logo_awesome_status == 1 %}<i class="fa fa-{{ t1o_text_logo_awesome }}"></i> {% endif %}{{ t1o_text_logo[lang_id] }}</a></div>*/
/*           {% endif %}*/
/*         </div>*/
/*         </div>*/
/*         </div>*/
/*    */
/*       {% if t1o_header_custom_block_1_status == 1 and t1o_header_style == 'header-style-2' %} */
/*         <div id="header-custom-box-wrapper" class="btn hidden-xs hidden-sm hidden-md">  */
/*         <div id="header-custom-box-content">      */
/*         {{ t1o_header_custom_block_1_content }}*/
/*         </div>*/
/*         </div>*/
/*       {% endif %}*/
/* */
/*       {{ cart }}*/
/*       {{ search }}*/
/*       */
/*       <a href="#menu-mobile-toggle" class="btn menu-mobile-block" id="menu-mobile-toggle">*/
/*       <div id="menu-mobile-block" class="buttons-header hidden-md hidden-lg" data-toggle="tooltip" title="{{ t1o_text_menu_menu[lang_id] }}">*/
/*         <div class="button-i"><i class="fa fa-bars"></i></div>*/
/*       </div>*/
/*       </a>*/
/*       <script>*/
/*         $("#menu-mobile-toggle").click(function(e) {*/
/*           e.preventDefault();*/
/*           $("#wrapper").toggleClass("menu-toggled");*/
/* 		  $(".sidebar-opacity").toggleClass("menu-toggled");*/
/*         });*/
/*       </script>*/
/*       */
/*       </div>*/
/*       */
/*     </div>*/
/*   </div>*/
/* */
/* */
/* {% if t1o_header_style != 'header-style-3' %}*/
/* */
/* {% if t1o_header_fixed_header_status == 1 %}*/
/* <script>*/
/*   $(window).load(function(){*/
/*     $("#menu").sticky({ topSpacing: 0 });*/
/*   });*/
/* </script>*/
/* {% endif %}*/
/* */
/* <nav id="menu" class="navbar hidden-sm">*/
/*       */
/*   <div class="container">*/
/*     <div class="collapse navbar-collapse navbar-ex1-collapse">*/
/*       */
/*       <ul class="main-menu menu flex">*/
/* */
/*         <!-- Home Page Link -->*/
/*         {% if t1o_menu_homepage != 'dontshow' %}*/
/*         <li id="homepage" class="homepage-{{ t1o_menu_homepage }}"><a href="{{ home }}"><i class="fa fa-home"></i> <span>{{ text_home }}</span></a></li>*/
/*         {% endif %}  */
/*  */
/*         {% if categories %}*/
/*         {% if t1o_menu_categories_status == 1 %}*/
/*         */
/*         {% if t1o_menu_categories_style == 1 %}      */
/*         <!-- Categories OpenCart Style -->*/
/*         {% for category in categories %}*/
/*         {% if category.children %}*/
/*         <li class="menu_oc">*/
/*         <a href="{{ category.href }}">{{ category.name }} <i class="fa fa-sort-down"></i></a>*/
/*           <div class="dropdown-menus" {% if t1o_menu_main_category_icon_status == 1 and category.thumbbg %}style="background-image: url({{ category.thumbbg }}); padding-right: {{ t1o_menu_main_category_padding_right }}px; padding-bottom: {{ t1o_menu_main_category_padding_bottom }}px;"{% endif %}>*/
/*               {% for children in category.children|batch(category.children|length) %}*/
/*               <ul class="list-unstyled">*/
/*                 {% for child in children %}*/
/* 			      	<li><a href="{{ child.href }}">{{ child.name }}</a>*/
/*                     */
/*                     {% if t1o_menu_categories_3_level == 1 %}*/
/*                     {% if child.children_level_2 %}*/
/*                     <div class="dropdown-menus">*/
/* 		              <ul class="list-unstyled">*/
/* 			            {% for child2 in child.children_level_2 %}*/
/* 				          <li><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/* 			            {% endfor %}*/
/* 			          </ul>*/
/* 		            </div>*/
/*                     {% endif %}*/
/*                     {% endif %}*/
/*                     */
/* 			        </li>*/
/*                 {% endfor %}*/
/*               </ul>*/
/*               {% endfor %}*/
/*             <a href="{{ category.href }}" class="see-all dropdown-highlight">{{ text_all }} {{ category.name }}</a> */
/*           </div>*/
/*         </li>*/
/*         {% else %}*/
/*         <li class="menu_oc"><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*         {% endif %}*/
/*         {% endfor %}  */
/*         <!-- Categories OpenCart Style - END -->  */
/*         {% endif %}*/
/*         */
/*         {% if t1o_menu_categories_style == 2 %}     */
/*         <!-- Categories Vertical Style -->    */
/*         <li id="menu_ver">*/
/*         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> {{ t1o_text_menu_categories[lang_id] }}</a>*/
/*         <div class="dropdown-menus">*/
/*         <ul class="list-unstyled">*/
/*         {% for category in categories %}*/
/*         {% if category.children %}*/
/*         <li>*/
/*         <a href="{{ category.href }}">{{ category.name }}</a>*/
/*         <div class="dropdown-menus">  */
/*         {% for children in category.children|batch(category.children|length) %}*/
/*         <ul class="list-unstyled" {% if t1o_menu_main_category_icon_status == 1 and category.thumbbg %}style="background-image: url({{ category.thumbbg }}); padding-right: {{ t1o_menu_main_category_padding_right }}px; padding-bottom: {{ t1o_menu_main_category_padding_bottom }}px;"{% endif %}>*/
/*           {% for child in children %}*/
/*           <li><a href="{{ child.href }}">{{ child.name }}</a>*/
/*           */
/*           {% if t1o_menu_categories_3_level == 1 %}*/
/*           {% if child.children_level_2 %}*/
/*           <div class="dropdown-menus">*/
/* 		    <ul class="list-unstyled">*/
/* 			  {% for child2 in child.children_level_2 %}*/
/* 				<li><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/* 			  {% endfor %}*/
/* 			</ul>*/
/* 		  </div>*/
/*           {% endif %}*/
/*           {% endif %}*/
/*           */
/* 		  </li>*/
/*           {% endfor %} */
/*         </ul>*/
/*         {% endfor %} */
/*         </div>*/
/*         </li>*/
/*         {% else %}*/
/*         <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*         {% endif %}*/
/*         {% endfor %}*/
/*         </ul>*/
/*         </div>*/
/*         </li>*/
/*         <!-- Categories Vertical Style - END -->  */
/*         {% endif %}*/
/*         */
/*         {% if t1o_menu_categories_style == 5 %}      */
/*         <!-- Categories Vertical 2 Style -->    */
/*         <li id="menu_ver_2">*/
/*         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> {{ t1o_text_menu_categories[lang_id] }}</a>*/
/*         <div class="dropdown-menus">*/
/*         <ul class="list-unstyled">*/
/*         {% for category in categories %}*/
/*         {% if category.children %}*/
/*         <li>*/
/*         <a href="{{ category.href }}">{{ category.name }}</a>*/
/*         <div class="dropdown-menus">  */
/*         {% for children in category.children|batch(category.children|length) %}*/
/*         <ul class="list-unstyled" {% if t1o_menu_main_category_icon_status == 1 and category.thumbbg %}style="background-image: url({{ category.thumbbg }});"{% endif %}>*/
/*           {% for child in children %}*/
/*           <li><a href="{{ child.href }}">{{ child.name }}</a>*/
/*           */
/*           {% if t1o_menu_categories_3_level == 1 %}*/
/*           {% if child.children_level_2 %}*/
/*           <div class="dropdown-menus">*/
/* 		    <ul class="list-unstyled">*/
/* 			  {% for child2 in child.children_level_2 %}*/
/* 				<li><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/* 			  {% endfor %}*/
/* 			</ul>*/
/* 		  </div>*/
/*           {% endif %}*/
/*           {% endif %}*/
/* */
/* 		  </li>*/
/*           {% endfor %} */
/*         </ul>*/
/*         {% endfor %} */
/*         </div>*/
/*         </li>*/
/*         {% else %}*/
/*         <li><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*         {% endif %}*/
/*         {% endfor %}*/
/*         </ul>*/
/*         </div>*/
/*         </li>*/
/*         <!-- Categories Vertical 2 Style - END -->  */
/*         {% endif %}*/
/*         */
/*         {% if t1o_menu_categories_style == 3 %}      */
/*         <!-- Categories Horizontal Style -->*/
/*         <li id="menu_hor">*/
/*         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> {{ t1o_text_menu_categories[lang_id] }}</a>*/
/*         <div class="dropdown-menus col-sm-12">*/
/*         <div class="container">*/
/*         */
/*         {% if t1o_menu_categories_custom_block_left_status == 1 %}*/
/*         <div id="menu-categories-custom-block-left-wrapper" class="col-sm-3 menu-categories-custom-block-side">*/
/*            {{ t1o_menu_categories_custom_block_left_content }}*/
/*         </div>*/
/*         {% endif %}*/
/* */
/*         {% if t1o_menu_categories_custom_block_left_status == 1 and t1o_menu_categories_custom_block_right_status == 1 %}*/
/* 	      <div class="col-sm-6 menu-hor-categories-wrapper-6">*/
/*         {% elseif t1o_menu_categories_custom_block_left_status == 1 or t1o_menu_categories_custom_block_right_status == 1 %} */
/*           <div class="col-sm-9 menu-hor-categories-wrapper-9">*/
/*         {% else %}*/
/*           <div class="col-sm-12 menu-hor-categories-wrapper-12">*/
/*         {% endif %}*/
/*         <div class="menu-hor-categories">*/
/*         {% for category in categories %}*/
/*         <ul class="list-unstyled {{ t1o_menu_categories_per_row }}">*/
/*         {% if category.children %}*/
/*         <li class="main-cat">*/
/*         <a href="{{ category.href }}">{{ category.name }}</a>*/
/*         {% if t1o_menu_main_category_icon_status == 1 and category.thumbbg %}*/
/*         <a href="{{ category.href }}"><img src="{{ category.thumbbg }}" title="{{ category.name }}" alt="{{ category.name }}" /></a>*/
/*         {% endif %}*/
/*         <div>   */
/*         {% for children in category.children|batch(category.children|length) %}*/
/*         <ul class="list-unstyled">*/
/*           {% for child in children %}*/
/*           <li class="sub-cat"><a href="{{ child.href }}">{{ child.name }}</a></li>*/
/*             */
/*           {% if t1o_menu_categories_3_level == 1 %}*/
/*           {% if child.children_level_2 %}*/
/*           {% for child2 in child.children_level_2 %}*/
/*           <li class="sub-cat sub-sub-cat"><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/*           {% endfor %}*/
/*           {% endif %}*/
/*           {% endif %}*/
/* */
/*           {% endfor %} */
/*         </ul>*/
/*         {% endfor %} */
/*         </div>*/
/*         </li>*/
/*         {% else %}*/
/*         <li class="main-cat"><a href="{{ category.href }}">{{ category.name }}</a></li>*/
/*         {% endif %}*/
/*         </ul>*/
/*         {% endfor %}*/
/*         </div>*/
/*         </div>*/
/*         */
/*         {% if t1o_menu_categories_custom_block_right_status == 1 %} */
/*         <div id="menu-categories-custom-block-right-wrapper" class="col-sm-3 menu-categories-custom-block-side">*/
/*            {{ t1o_menu_categories_custom_block_right_content }}*/
/*         </div>*/
/*         {% endif %}*/
/*         */
/*         {% if t1o_menu_categories_custom_block_status == 1 %} */
/*         <div id="menu-categories-custom-block-bottom-wrapper" class="col-sm-12">*/
/*            {{ t1o_menu_categories_custom_block_content }}*/
/*         </div>*/
/*         {% endif %}*/
/*         */
/*         </div>*/
/*         </div>*/
/* */
/*         </li>*/
/*         <!-- Categories Horizontal Style - END -->  */
/*         {% endif %}*/
/*         */
/*         */
/*         {% if t1o_menu_categories_style == 4 %}     */
/*         <!-- Categories Inline Style -->    */
/*         <li id="menu_inline">*/
/*         <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i> {{ t1o_text_menu_categories[lang_id] }}</a>*/
/*         <div class="dropdown-menus col-sm-12">*/
/*         {% for category in categories %}*/
/*         <ul class="list-unstyled">*/
/*         <li class="main-cat">*/
/*         <a href="{{ category.href }}">{{ category.name }}{% if category.children %} <i class="fa fa-sort-down"></i>{% endif %}</a>*/
/*         {% if category.children %}*/
/*         <div class="dropdown-menus col-sm-12">  */
/*         {% for children in category.children|batch(category.children|length) %}*/
/*         <ul class="list-unstyled">*/
/*           {% for child in children %}*/
/*           <li><a href="{{ child.href }}">{{ child.name }} {% if child.children_level_2 %}<i class="fa fa-sort-down"></i>{% endif %}</a>*/
/*                */
/*           {% if t1o_menu_categories_3_level == 1 %}*/
/*           {% if child.children_level_2 %}*/
/*           <div class="dropdown-menus col-sm-12">*/
/* 		    <ul class="list-unstyled">*/
/* 			  {% for child2 in child.children_level_2 %}*/
/* 				<li><a href="{{ child2.href }}">{{ child2.name }}</a></li>*/
/* 			  {% endfor %}*/
/* 			</ul>*/
/* 		  </div>*/
/*           {% endif %}*/
/*           {% endif %}*/
/*     */
/* 		  </li>*/
/*           {% endfor %}*/
/*         </ul>*/
/*         {% endfor %} */
/*         </div>*/
/*         {% endif %}*/
/*         </li>*/
/*         </ul>*/
/*         {% endfor %}*/
/*         </div>*/
/*         </li>*/
/*         <!-- Categories Inline Style - END -->  */
/*         {% endif %}*/
/*         */
/*         {% endif %} */
/*         {% endif %}*/
/* */
/*         <!-- Brands -->*/
/*         {% if t1o_menu_brands_status == 1 %}*/
/*         <li id="menu_brands">*/
/*           <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ t1o_text_menu_brands[lang_id] }} <i class="fa fa-sort-down"></i></a>*/
/*           {% if manufacturers %}*/
/*           <div class="dropdown-menus col-sm-12 {{ t1o_menu_brands_style }}">*/
/*           <div class="container">*/
/*               {% for manufacturer in manufacturers %}*/
/*               <div class="brand-item col-sm-{{ t1o_menu_brands_per_row }}">*/
/*                 <a href="{{ manufacturer.href }}">*/
/*                 {% if manufacturer.image %}*/
/*                 <div class="image"><img src="{{ manufacturer.image }}" title="{{ manufacturer.name }}" alt="{{ manufacturer.name }}" /></div>*/
/*                 {% else %}*/
/*                 <div class="image"><i class="fa fa-camera"></i></div>*/
/*                 {% endif %}*/
/*                 <div class="name">{{ manufacturer.name }}</div>*/
/*                 </a>  */
/*               </div>*/
/*               {% endfor %}*/
/*           </div>*/
/*           </div>*/
/*           {% endif %}*/
/*         </li>*/
/*         {% endif %}*/
/* */
/*         <!-- Custom Blocks -->*/
/*         {% for i in 1..5 %} */
/*         {% if t1o_menu_custom_block[i].status == 1 and t1o_menu_custom_block[i][lang_id].title !='' %}                      */
/*         <li class="menu_custom_block">        */
/*           <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ t1o_menu_custom_block[i][lang_id].title }} <i class="fa fa-sort-down"></i></a> */
/*           <div class="dropdown-menus col-sm-12">*/
/*             <div class="container">*/
/*                 {{ t1o_menu_custom_block_htmlcontent[i] }}*/
/*             </div>       */
/*           </div>  */
/*         </li>                              */
/*         {% endif %}*/
/*         {% endfor %}*/
/*         */
/*         <!-- Custom Dropdown Menus --> */
/*         {% if t1o_menu_cm_status == 1 and t1o_menu_cm_title[lang_id] %}   */
/*         <li class="menu_custom_menu">       */
/*           <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ t1o_menu_cm_title[lang_id] }} <i class="fa fa-sort-down"></i></a>*/
/*           <div class="dropdown-menus">*/
/*             <ul class="list-unstyled">*/
/*               {% for i in 1..10 %}*/
/*               {% if t1o_menu_cm_link[i].status == 1 and t1o_menu_cm_link[i].url !='' %}*/
/*                 <li>*/
/*                   <a href="{{ t1o_menu_cm_link[i].url }}" target="{{ t1o_menu_cm_link[i].target }}">*/
/* 		      	  {{ t1o_menu_cm_link[i][lang_id].title }}*/
/*                   </a>*/
/*                 </li>*/
/*               {% endif %}*/
/*               {% endfor %}*/
/*             </ul>       */
/*           </div>*/
/*         </li>         */
/*         {% endif %}*/
/*         {% if t1o_menu_cm_2_status == 1 and t1o_menu_cm_2_title[lang_id] %}  */
/*         <li class="menu_custom_menu">       */
/*           <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ t1o_menu_cm_2_title[lang_id] }} <i class="fa fa-sort-down"></i></a>*/
/*           <div class="dropdown-menus">*/
/*             <ul class="list-unstyled">*/
/*               {% for i in 1..10 %}*/
/*               {% if t1o_menu_cm_2_link[i].status == 1 and t1o_menu_cm_2_link[i].url !='' %}*/
/*                 <li>*/
/*                   <a href="{{ t1o_menu_cm_2_link[i].url }}" target="{{ t1o_menu_cm_2_link[i].target }}">*/
/* 		      	  {{ t1o_menu_cm_2_link[i][lang_id].title }}*/
/*                   </a>*/
/*                 </li>*/
/*               {% endif %}*/
/*               {% endfor %}*/
/*             </ul>       */
/*           </div>*/
/*         </li>         */
/*         {% endif %}*/
/*         {% if t1o_menu_cm_3_status == 1 and t1o_menu_cm_3_title[lang_id] %}   */
/*         <li class="menu_custom_menu">       */
/*           <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ t1o_menu_cm_3_title[lang_id] }} <i class="fa fa-sort-down"></i></a>*/
/*           <div class="dropdown-menus">*/
/*             <ul class="list-unstyled">*/
/*               {% for i in 1..10 %}*/
/*               {% if t1o_menu_cm_3_link[i].status == 1 and t1o_menu_cm_3_link[i].url !='' %}*/
/*                 <li>*/
/*                   <a href="{{ t1o_menu_cm_3_link[i].url }}" target="{{ t1o_menu_cm_3_link[i].target }}">*/
/* 		      	  {{ t1o_menu_cm_3_link[i][lang_id].title }}*/
/*                   </a>*/
/*                 </li>*/
/*               {% endif %}*/
/*               {% endfor %}*/
/*             </ul>       */
/*           </div>*/
/*         </li>         */
/*         {% endif %}*/
/*         */
/*         <!-- Custom Links -->*/
/*         {% for i in 1..10 %}*/
/*         {% if t1o_menu_link[i].status == 1 and t1o_menu_link[i][lang_id].title !='' and t1o_menu_link[i].url !='' %}                        */
/*         <li class="menu_links">*/
/*           <a href="{{ t1o_menu_link[i].url }}" target="{{ t1o_menu_link[i].target }}">*/
/*             {{ t1o_menu_link[i][lang_id].title }}*/
/*           </a>*/
/*         </li>                             */
/*         {% endif %}*/
/*         {% endfor %}*/
/*         */
/*       </ul>*/
/*       */
/*     </div>*/
/*   </div>*/
/* {% if t1o_custom_bar_below_menu_status == 1 %} */
/* <div id="custom-bar-wrapper">*/
/*    <div id="custom-bar-content">*/
/*    {{ t1o_custom_bar_below_menu_content }}*/
/*    </div>*/
/* </div>*/
/* {% endif %}*/
/* </nav>*/
/* {% endif %}*/
/* */
/* <script type="text/javascript">*/
/* $(document).ready(function() {*/
/* 	$("#accordion-mobile").dcAccordion({*/
/* 		disableLink: false,	*/
/* 		menuClose: false,*/
/* 		autoClose: true,*/
/* 		autoExpand: true,		*/
/* 		saveState: false*/
/* 	});*/
/* });*/
/* </script> */
/* <div id="menu-mobile" class="hidden-md hidden-lg">*/
/* <ul class="accordion sidebar-nav list-group" id="accordion-mobile">  */
/*       <li class="sidebar-title">*/
/*       <a href="#menu-mobile-toggle-close" id="menu-mobile-toggle-close"><span aria-hidden="true">&times;</span></a>{{ t1o_text_menu_menu[lang_id] }}*/
/*       <script>*/
/*       $("#menu-mobile-toggle-close, .sidebar-opacity").click(function(e) {*/
/*         e.preventDefault();*/
/*         $("#wrapper").toggleClass("menu-toggled");*/
/* 		$(".sidebar-opacity").toggleClass("menu-toggled");*/
/*       });*/
/*       </script>*/
/*     </li> */
/*     <li class="home-mobile"><a href="{{ home }}">{{ text_home }}</a></li>*/
/*     {% for category in categories %}*/
/*     <li class="category-mobile">*/
/*         {% if category.children %}*/
/*           <a href="{{ category.href }}" class="list-group-item">{{ category.name }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*         {% else %}*/
/*           <a href="{{ category.href }}" class="list-group-item">{{ category.name }}</a>*/
/*         {% endif %}*/
/*         {% if category.children %}*/
/*           <ul>*/
/*             {% for child in category.children %}*/
/*             <li>*/
/*               <a href="{{ child.href }}" class="list-group-item">{{ child.name }}</a>*/
/*             </li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*         {% endif %}*/
/*     </li>*/
/*     {% endfor %}*/
/*     {% if t1o_menu_brands_status == 1 %}*/
/*     <li class="menu_links"><a href="{{ manufacturer }}">{{ t1o_text_menu_brands[lang_id] }}</a></li>*/
/*     {% endif %}*/
/*     {% if t1o_menu_cm_status == 1 and t1o_menu_cm_title[lang_id] %}   */
/*     <li>       */
/*       <a href="#" class="dropdown-toggle list-group-item" data-toggle="dropdown">{{ t1o_menu_cm_title[lang_id] }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*       <ul class="list-unstyled">*/
/*         {% for i in 1..10 %}*/
/*         {% if t1o_menu_cm_link[i].status == 1 and t1o_menu_cm_link[i].url !='' %}*/
/*           <li>*/
/*             <a href="{{ t1o_menu_cm_link[i].url }}" target="{{ t1o_menu_cm_link[i].target }}">*/
/* 		    {{ t1o_menu_cm_link[i][lang_id].title }}*/
/*             </a>*/
/*           </li>*/
/*         {% endif %}*/
/*         {% endfor %}*/
/*       </ul>       */
/*     </li>         */
/*     {% endif %}*/
/*     {% if t1o_menu_cm_2_status == 1 and t1o_menu_cm_2_title[lang_id] %}   */
/*     <li>       */
/*       <a href="#" class="dropdown-toggle list-group-item" data-toggle="dropdown">{{ t1o_menu_cm_2_title[lang_id] }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*       <ul class="list-unstyled">*/
/*         {% for i in 1..10 %}*/
/*         {% if t1o_menu_cm_2_link[i].status == 1 and t1o_menu_cm_2_link[i].url !='' %}*/
/*           <li>*/
/*             <a href="{{ t1o_menu_cm_2_link[i].url }}" target="{{ t1o_menu_cm_2_link[i].target }}">*/
/* 		    {{ t1o_menu_cm_2_link[i][lang_id].title }}*/
/*             </a>*/
/*           </li>*/
/*         {% endif %}*/
/*         {% endfor %}*/
/*       </ul>       */
/*     </li>         */
/*     {% endif %}*/
/*     {% if t1o_menu_cm_3_status == 1 and t1o_menu_cm_3_title[lang_id] %}  */
/*     <li>       */
/*       <a href="#" class="dropdown-toggle list-group-item" data-toggle="dropdown">{{ t1o_menu_cm_3_title[lang_id] }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*       <ul class="list-unstyled">*/
/*         {% for i in 1..10 %}*/
/*         {% if t1o_menu_cm_3_link[i].status == 1 and t1o_menu_cm_3_link[i].url !='' %}*/
/*           <li>*/
/*             <a href="{{ t1o_menu_cm_3_link[i].url }}" target="{{ t1o_menu_cm_3_link[i].target }}">*/
/* 		    {{ t1o_menu_cm_3_link[i][lang_id].title }}*/
/*             </a>*/
/*           </li>*/
/*         {% endif %}*/
/*         {% endfor %}*/
/*       </ul>       */
/*     </li>         */
/*     {% endif %}*/
/* */
/*     {% for i in 1..10 %}*/
/*     {% if t1o_menu_link[i].status == 1 and t1o_menu_link[i][lang_id].title !='' and t1o_menu_link[i].url !='' %}                        */
/*     <li class="menu_links">*/
/*       <a href="{{ t1o_menu_link[i].url }}" target="{{ t1o_menu_link[i].target }}">*/
/*         {{ t1o_menu_link[i][lang_id].title }}*/
/*       </a>*/
/*     </li>                             */
/*     {% endif %}*/
/*     {% endfor %}*/
/*     */
/*     <li>*/
/*       <a href="#" class="dropdown-toggle list-group-item" data-toggle="dropdown">{{ text_information }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*       <ul class="list-unstyled">*/
/*         {% for information in informations %}*/
/*         <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </li>   */
/*     <li><a href="{{ contact }}">{{ t1o_text_contact_us[lang_id] }}</a></li>*/
/* </ul>*/
/* </div>*/
/* */
/* {% for i in 1..15 %}*/
/* {% if t1o_menu_labels[i][lang_id].title %}                       */
/* <span id="menu_label{{ i }}" class="menu_label" style="background-color:{{ t1o_menu_labels_color[i] }};">*/
/*     {{ t1o_menu_labels[i][lang_id].title }}*/
/* </span> */
/* <script type="text/javascript">*/
/* $(document).ready(function(){*/
/*      $('#menu_label{{ i }}').prependTo("ul.main-menu > li:nth-child({{ i }}) > a");*/
/* });	*/
/* </script>  */
/* {% endif %}*/
/* {% endfor %}*/
/* */
/* <script type="text/javascript">*/
/* $('#menu_hor > a, #menu_ver > a, #menu_ver_2 > a, #menu_inline > a, #menu_brands > a, .menu_custom_menu > a, .menu_custom_block > a').click(function() {*/
/*   $(this).toggleClass('open');*/
/* 	return false;*/
/*   });*/
/* </script>*/
/* */
/* </header>*/
