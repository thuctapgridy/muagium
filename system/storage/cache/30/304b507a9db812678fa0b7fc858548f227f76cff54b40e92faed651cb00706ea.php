<?php

/* oxy/template/common/footer.twig */
class __TwigTemplate_0e5629399b309e0f9fc351264d843b8e568ed23166347ee941d250c7c7751098 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<footer ";
        // line 2
        if (((isset($context["t1d_bg_image_f1_parallax"]) ? $context["t1d_bg_image_f1_parallax"] : null) == 1)) {
            echo "data-stellar-background-ratio=\"0.5\"";
        }
        echo ">

  ";
        // line 4
        if (((isset($context["t1o_custom_top_1_status"]) ? $context["t1o_custom_top_1_status"] : null) == 1)) {
            // line 5
            echo "  <div id=\"footer_custom_top_1\">
  <div class=\"container\">
  <div class=\"row come-item\">
  <div class=\"col-sm-12 padd-t-b-30\">
    ";
            // line 9
            echo (isset($context["t1o_custom_top_1_content"]) ? $context["t1o_custom_top_1_content"] : null);
            echo "
  </div>
  </div>
  </div>
  </div>
  ";
        }
        // line 15
        echo "
  ";
        // line 16
        if (((((((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) == 1) || ((isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null) == 1)) || ((isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null) == 1)) || ((isset($context["t1o_custom_1_status"]) ? $context["t1o_custom_1_status"] : null) == 1)) || ((isset($context["t1o_custom_2_status"]) ? $context["t1o_custom_2_status"] : null) == 1))) {
            // line 17
            echo "  <div id=\"information\">
  <div class=\"container\">
    <div class=\"row come-item\">
    ";
            // line 20
            if (((isset($context["t1o_custom_1_status"]) ? $context["t1o_custom_1_status"] : null) == 1)) {
                // line 21
                echo "      <div class=\"col-sm-";
                echo (isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null);
                echo " col-xs-12\">
        ";
                // line 22
                if ($this->getAttribute((isset($context["t1o_custom_1_title"]) ? $context["t1o_custom_1_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array")) {
                    // line 23
                    echo "        <h4>";
                    echo $this->getAttribute((isset($context["t1o_custom_1_title"]) ? $context["t1o_custom_1_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "</h4>
        ";
                }
                // line 25
                echo "        ";
                echo (isset($context["t1o_custom_1_content"]) ? $context["t1o_custom_1_content"] : null);
                echo "
      </div>
    ";
            }
            // line 28
            echo "      
    ";
            // line 29
            if (((((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) == 1) || ((isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null) == 1)) || ((isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null) == 1))) {
                // line 30
                echo "    <div class=\"col-sm-";
                echo (isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null);
                echo " col-xs-12\">
    <div id=\"information-block-containter\">
      ";
                // line 32
                if (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) == 1)) {
                    // line 33
                    echo "      <div class=\"col-sm-";
                    echo intval(floor((12 / (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) + (isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null)) + (isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null)))));
                    echo "  col-xs-12\">
        <h4>";
                    // line 34
                    echo (isset($context["text_information"]) ? $context["text_information"] : null);
                    echo "</h4>
        ";
                    // line 35
                    if ((isset($context["informations"]) ? $context["informations"] : null)) {
                        // line 36
                        echo "        <ul class=\"list-unstyled\">
          ";
                        // line 37
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable((isset($context["informations"]) ? $context["informations"] : null));
                        foreach ($context['_seq'] as $context["_key"] => $context["information"]) {
                            // line 38
                            echo "          <li><a href=\"";
                            echo $this->getAttribute($context["information"], "href", array());
                            echo "\">";
                            echo $this->getAttribute($context["information"], "title", array());
                            echo "</a></li>
          ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['information'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 40
                        echo "        </ul>
        ";
                    }
                    // line 42
                    echo "      </div>
      ";
                }
                // line 44
                echo "      ";
                if (((isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null) == 1)) {
                    // line 45
                    echo "      <div class=\"col-sm-";
                    echo intval(floor((12 / (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) + (isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null)) + (isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null)))));
                    echo "  col-xs-12\">
        <h4>";
                    // line 46
                    echo (isset($context["text_service"]) ? $context["text_service"] : null);
                    echo "</h4>
        <ul class=\"list-unstyled\">
          ";
                    // line 48
                    if (((isset($context["t1o_i_c_2_1_status"]) ? $context["t1o_i_c_2_1_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["contact"]) ? $context["contact"] : null);
                        echo "\">";
                        echo (isset($context["text_contact"]) ? $context["text_contact"] : null);
                        echo "</a></li>";
                    }
                    // line 49
                    echo "          ";
                    if (((isset($context["t1o_i_c_2_2_status"]) ? $context["t1o_i_c_2_2_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["account"]) ? $context["account"] : null);
                        echo "\">";
                        echo (isset($context["text_account"]) ? $context["text_account"] : null);
                        echo "</a></li>";
                    }
                    // line 50
                    echo "          ";
                    if (((isset($context["t1o_i_c_2_3_status"]) ? $context["t1o_i_c_2_3_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["return"]) ? $context["return"] : null);
                        echo "\">";
                        echo (isset($context["text_return"]) ? $context["text_return"] : null);
                        echo "</a></li>";
                    }
                    // line 51
                    echo "          ";
                    if (((isset($context["t1o_i_c_2_4_status"]) ? $context["t1o_i_c_2_4_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["order"]) ? $context["order"] : null);
                        echo "\">";
                        echo (isset($context["text_order"]) ? $context["text_order"] : null);
                        echo "</a></li>";
                    }
                    // line 52
                    echo "          ";
                    if (((isset($context["t1o_i_c_2_5_status"]) ? $context["t1o_i_c_2_5_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["wishlist"]) ? $context["wishlist"] : null);
                        echo "\">";
                        echo (isset($context["text_wishlist"]) ? $context["text_wishlist"] : null);
                        echo "</a></li>";
                    }
                    // line 53
                    echo "        </ul>
      </div>
      ";
                }
                // line 56
                echo "      ";
                if (((isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null) == 1)) {
                    // line 57
                    echo "      <div class=\"col-sm-";
                    echo intval(floor((12 / (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) + (isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null)) + (isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null)))));
                    echo "  col-xs-12\">
        <h4>";
                    // line 58
                    echo (isset($context["text_extra"]) ? $context["text_extra"] : null);
                    echo "</h4>
        <ul class=\"list-unstyled\">
          ";
                    // line 60
                    if (((isset($context["t1o_i_c_3_1_status"]) ? $context["t1o_i_c_3_1_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["manufacturer"]) ? $context["manufacturer"] : null);
                        echo "\">";
                        echo (isset($context["text_manufacturer"]) ? $context["text_manufacturer"] : null);
                        echo "</a></li>";
                    }
                    // line 61
                    echo "          ";
                    if (((isset($context["t1o_i_c_3_2_status"]) ? $context["t1o_i_c_3_2_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["voucher"]) ? $context["voucher"] : null);
                        echo "\">";
                        echo (isset($context["text_voucher"]) ? $context["text_voucher"] : null);
                        echo "</a></li>";
                    }
                    // line 62
                    echo "          ";
                    if (((isset($context["t1o_i_c_3_3_status"]) ? $context["t1o_i_c_3_3_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["affiliate"]) ? $context["affiliate"] : null);
                        echo "\">";
                        echo (isset($context["text_affiliate"]) ? $context["text_affiliate"] : null);
                        echo "</a></li>";
                    }
                    // line 63
                    echo "          ";
                    if (((isset($context["t1o_i_c_3_4_status"]) ? $context["t1o_i_c_3_4_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["special"]) ? $context["special"] : null);
                        echo "\">";
                        echo (isset($context["text_special"]) ? $context["text_special"] : null);
                        echo "</a></li>";
                    }
                    // line 64
                    echo "          ";
                    if (((isset($context["t1o_i_c_3_5_status"]) ? $context["t1o_i_c_3_5_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["newsletter"]) ? $context["newsletter"] : null);
                        echo "\">";
                        echo (isset($context["text_newsletter"]) ? $context["text_newsletter"] : null);
                        echo "</a></li>";
                    }
                    // line 65
                    echo "          ";
                    if (((isset($context["t1o_i_c_3_6_status"]) ? $context["t1o_i_c_3_6_status"] : null) == 1)) {
                        echo "<li><a href=\"";
                        echo (isset($context["sitemap"]) ? $context["sitemap"] : null);
                        echo "\">";
                        echo (isset($context["text_sitemap"]) ? $context["text_sitemap"] : null);
                        echo "</a></li>";
                    }
                    // line 66
                    echo "        </ul>
      </div>
      ";
                }
                // line 69
                echo "    </div>
    </div>
    ";
            }
            // line 72
            echo "      
      
      ";
            // line 74
            if (((isset($context["t1o_custom_2_status"]) ? $context["t1o_custom_2_status"] : null) == 1)) {
                // line 75
                echo "      <div class=\"col-sm-";
                echo (isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null);
                echo " col-xs-12\">
        ";
                // line 76
                if ($this->getAttribute((isset($context["t1o_custom_2_title"]) ? $context["t1o_custom_2_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array")) {
                    // line 77
                    echo "        <h4>";
                    echo $this->getAttribute((isset($context["t1o_custom_2_title"]) ? $context["t1o_custom_2_title"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "</h4>
        ";
                }
                // line 79
                echo "        
        ";
                // line 80
                if (((isset($context["t1o_newsletter_status"]) ? $context["t1o_newsletter_status"] : null) == 1)) {
                    // line 81
                    echo "        <div class=\"newsletter-block\">                   
        <form action=\"";
                    // line 82
                    echo (isset($context["t1o_newsletter_campaign_url"]) ? $context["t1o_newsletter_campaign_url"] : null);
                    echo "\" method=\"post\" id=\"mc-embedded-subscribe-form\" name=\"mc-embedded-subscribe-form\" class=\"validate\" target=\"_blank\" novalidate>
            <div id=\"mc_embed_signup_scroll\">
\t        <label for=\"mce-EMAIL\">";
                    // line 84
                    echo $this->getAttribute((isset($context["t1o_newsletter_promo_text"]) ? $context["t1o_newsletter_promo_text"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "</label>
\t        <input type=\"email\" value=\"\" name=\"EMAIL\" class=\"email form-group form-control\" id=\"mce-EMAIL\" placeholder=\"";
                    // line 85
                    echo $this->getAttribute((isset($context["t1o_newsletter_email"]) ? $context["t1o_newsletter_email"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "\" required>
            <div style=\"position: absolute; left: -5000px;\" aria-hidden=\"true\"><input type=\"text\" name=\"b_619755c76ad5fa60a96e52bec_c5fb795f8e\" tabindex=\"-1\" value=\"\"></div>
            <div class=\"clear\"><input type=\"submit\" value=\"";
                    // line 87
                    echo $this->getAttribute((isset($context["t1o_newsletter_subscribe"]) ? $context["t1o_newsletter_subscribe"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "\" name=\"subscribe\" id=\"mc-embedded-subscribe\" class=\"btn btn-primary\"></div>
            </div>
        </form>
        </div>
        ";
                }
                // line 92
                echo "        
        ";
                // line 93
                echo (isset($context["t1o_custom_2_content"]) ? $context["t1o_custom_2_content"] : null);
                echo "
        
      </div>
      ";
            }
            // line 97
            echo "    </div>
  </div>
  </div>
  ";
        }
        // line 101
        echo "  
  ";
        // line 102
        if (((((isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null) == 1) || ((isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null) == 1)) || ((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) == 1))) {
            // line 103
            echo "  <div id=\"powered\">
  <div class=\"container\">
    <div class=\"row come-item\">
    
      ";
            // line 107
            if (((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) == 1)) {
                // line 108
                echo "      <div id=\"footer-payment-wrapper\" class=\"col-sm-";
                echo intval(floor((12 / (((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) + (isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null)) + (isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null)))));
                echo "\">
        <ul id=\"footer-payment\" class=\"list-inline\">
            
        ";
                // line 111
                if (((isset($context["t1o_payment_block_custom_status"]) ? $context["t1o_payment_block_custom_status"] : null) == 1)) {
                    // line 112
                    echo "        
        ";
                    // line 113
                    if (($this->getAttribute((isset($context["_SERVER"]) ? $context["_SERVER"] : null), "HTTPS", array()) && ($this->getAttribute((isset($context["_SERVER"]) ? $context["_SERVER"] : null), "HTTPS", array()) != "off"))) {
                        // line 114
                        echo "\t\t\t";
                        $context["path_image"] = ((isset($context["config_ssl"]) ? $context["config_ssl"] : null) . "image/");
                        // line 115
                        echo "\t\t";
                    } else {
                        echo "  
\t\t\t";
                        // line 116
                        $context["path_image"] = ((isset($context["config_url"]) ? $context["config_url"] : null) . "image/");
                        // line 117
                        echo "\t\t";
                    }
                    // line 118
                    echo "        
        ";
                    // line 119
                    if (((isset($context["t1o_payment_block_custom"]) ? $context["t1o_payment_block_custom"] : null) != "")) {
                        // line 120
                        echo "        ";
                        if (((isset($context["t1o_payment_block_custom_url"]) ? $context["t1o_payment_block_custom_url"] : null) != "")) {
                            echo " 
\t\t<li><a href=\"";
                            // line 121
                            echo (isset($context["t1o_payment_block_custom_url"]) ? $context["t1o_payment_block_custom_url"] : null);
                            echo "\" target=\"_blank\">
\t\t\t<img src=\"";
                            // line 122
                            echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1o_payment_block_custom"]) ? $context["t1o_payment_block_custom"] : null));
                            echo "\" alt=\"Payment\" title=\"Payment\"></a>
        </li>
        ";
                        } else {
                            // line 125
                            echo "        <li>       
\t\t\t<img src=\"";
                            // line 126
                            echo ((isset($context["path_image"]) ? $context["path_image"] : null) . (isset($context["t1o_payment_block_custom"]) ? $context["t1o_payment_block_custom"] : null));
                            echo "\" alt=\"Payment\" title=\"Payment\">
        </li>
        ";
                        }
                        // line 129
                        echo "\t\t";
                    }
                    // line 130
                    echo "\t\t";
                }
                // line 131
                echo "        
        ";
                // line 132
                if (((isset($context["t1o_payment_paypal"]) ? $context["t1o_payment_paypal"] : null) == 1)) {
                    // line 133
                    echo "\t\t<li data-toggle=\"tooltip\" title=\"PayPal\">
        ";
                    // line 134
                    if (((isset($context["t1o_payment_paypal_url"]) ? $context["t1o_payment_paypal_url"] : null) != "")) {
                        // line 135
                        echo "            <a href=\"";
                        echo (isset($context["t1o_payment_paypal_url"]) ? $context["t1o_payment_paypal_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_paypal-";
                        // line 136
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"PayPal\" title=\"PayPal\"></a>
        ";
                    } else {
                        // line 138
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_paypal-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"PayPal\" title=\"PayPal\">
        ";
                    }
                    // line 140
                    echo "        </li>
\t\t";
                }
                // line 141
                echo " 
        
        ";
                // line 143
                if (((isset($context["t1o_payment_visa"]) ? $context["t1o_payment_visa"] : null) == 1)) {
                    // line 144
                    echo "        <li data-toggle=\"tooltip\" title=\"Visa\">
        ";
                    // line 145
                    if (((isset($context["t1o_payment_visa_url"]) ? $context["t1o_payment_visa_url"] : null) != "")) {
                        // line 146
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_visa_url"]) ? $context["t1o_payment_visa_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_visa-";
                        // line 147
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Visa\" title=\"Visa\"></a>
        ";
                    } else {
                        // line 149
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_visa-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Visa\" title=\"Visa\">
        ";
                    }
                    // line 151
                    echo "        </li>
\t\t";
                }
                // line 152
                echo "   
        
        ";
                // line 154
                if (((isset($context["t1o_payment_mastercard"]) ? $context["t1o_payment_mastercard"] : null) == 1)) {
                    // line 155
                    echo "        <li data-toggle=\"tooltip\" title=\"MasterCard\">
        ";
                    // line 156
                    if (((isset($context["t1o_payment_mastercard_url"]) ? $context["t1o_payment_mastercard_url"] : null) != "")) {
                        // line 157
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_mastercard_url"]) ? $context["t1o_payment_mastercard_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_mastercard-";
                        // line 158
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"MasterCard\" title=\"MasterCard\"></a>
        ";
                    } else {
                        // line 160
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_mastercard-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"MasterCard\" title=\"MasterCard\">
        ";
                    }
                    // line 162
                    echo "        </li>
\t\t";
                }
                // line 163
                echo " 
       
        ";
                // line 165
                if (((isset($context["t1o_payment_maestro"]) ? $context["t1o_payment_maestro"] : null) == 1)) {
                    // line 166
                    echo "        <li data-toggle=\"tooltip\" title=\"Maestro\">
        ";
                    // line 167
                    if (((isset($context["t1o_payment_maestro_url"]) ? $context["t1o_payment_maestro_url"] : null) != "")) {
                        // line 168
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_maestro_url"]) ? $context["t1o_payment_maestro_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_maestro-";
                        // line 169
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Maestro\" title=\"Maestro\"></a>
        ";
                    } else {
                        // line 171
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_maestro-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Maestro\" title=\"Maestro\">
        ";
                    }
                    // line 173
                    echo "        </li>
\t\t";
                }
                // line 175
                echo "       
        ";
                // line 176
                if (((isset($context["t1o_payment_discover"]) ? $context["t1o_payment_discover"] : null) == 1)) {
                    // line 177
                    echo "        <li data-toggle=\"tooltip\" title=\"Discover\">
        ";
                    // line 178
                    if (((isset($context["t1o_payment_discover_url"]) ? $context["t1o_payment_discover_url"] : null) != "")) {
                        // line 179
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_discover_url"]) ? $context["t1o_payment_discover_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_discover-";
                        // line 180
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Discover\" title=\"Discover\"></a>
        ";
                    } else {
                        // line 182
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_discover-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Discover\" title=\"Discover\">
        ";
                    }
                    // line 184
                    echo "        </li>
\t\t";
                }
                // line 185
                echo "                   
        
        ";
                // line 187
                if (((isset($context["t1o_payment_skrill"]) ? $context["t1o_payment_skrill"] : null) == 1)) {
                    // line 188
                    echo "        <li data-toggle=\"tooltip\" title=\"Skrill\">
        ";
                    // line 189
                    if (((isset($context["t1o_payment_skrill_url"]) ? $context["t1o_payment_skrill_url"] : null) != "")) {
                        // line 190
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_skrill_url"]) ? $context["t1o_payment_skrill_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_skrill-";
                        // line 191
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Skrill\" title=\"Skrill\"></a>
        ";
                    } else {
                        // line 193
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_skrill-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Skrill\" title=\"Skrill\">
        ";
                    }
                    // line 195
                    echo "        </li>
\t\t";
                }
                // line 196
                echo "   
        
        ";
                // line 198
                if (((isset($context["t1o_payment_american_express"]) ? $context["t1o_payment_american_express"] : null) == 1)) {
                    // line 199
                    echo "        <li data-toggle=\"tooltip\" title=\"American Express\">
        ";
                    // line 200
                    if (((isset($context["t1o_payment_american_express_url"]) ? $context["t1o_payment_american_express_url"] : null) != "")) {
                        // line 201
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_american_express_url"]) ? $context["t1o_payment_american_express_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_american_express-";
                        // line 202
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"American Express\" title=\"American Express\"></a>
        ";
                    } else {
                        // line 204
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_american_express-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"American Express\" title=\"American Express\">
        ";
                    }
                    // line 206
                    echo "        </li>
\t\t";
                }
                // line 207
                echo " 
                   
        ";
                // line 209
                if (((isset($context["t1o_payment_cirrus"]) ? $context["t1o_payment_cirrus"] : null) == 1)) {
                    // line 210
                    echo "        <li data-toggle=\"tooltip\" title=\"Cirrus\">
        ";
                    // line 211
                    if (((isset($context["t1o_payment_cirrus_url"]) ? $context["t1o_payment_cirrus_url"] : null) != "")) {
                        // line 212
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_cirrus_url"]) ? $context["t1o_payment_cirrus_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_cirrus-";
                        // line 213
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Cirrus\" title=\"Cirrus\"></a>
        ";
                    } else {
                        // line 215
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_cirrus-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Cirrus\" title=\"Cirrus\">
        ";
                    }
                    // line 217
                    echo "        </li>
\t\t";
                }
                // line 218
                echo "   
        
        ";
                // line 220
                if (((isset($context["t1o_payment_delta"]) ? $context["t1o_payment_delta"] : null) == 1)) {
                    // line 221
                    echo "        <li data-toggle=\"tooltip\" title=\"Delta\">
        ";
                    // line 222
                    if (((isset($context["t1o_payment_delta_url"]) ? $context["t1o_payment_delta_url"] : null) != "")) {
                        // line 223
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_delta_url"]) ? $context["t1o_payment_delta_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_delta-";
                        // line 224
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Delta\" title=\"Delta\"></a>
        ";
                    } else {
                        // line 226
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_delta-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Delta\" title=\"Delta\">
        ";
                    }
                    // line 228
                    echo "        </li>
\t\t";
                }
                // line 229
                echo "   
        
        ";
                // line 231
                if (((isset($context["t1o_payment_google"]) ? $context["t1o_payment_google"] : null) == 1)) {
                    // line 232
                    echo "        <li data-toggle=\"tooltip\" title=\"Google Wallet\">
        ";
                    // line 233
                    if (((isset($context["t1o_payment_google_url"]) ? $context["t1o_payment_google_url"] : null) != "")) {
                        // line 234
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_google_url"]) ? $context["t1o_payment_google_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_google-";
                        // line 235
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Google Wallet\" title=\"Google Wallet\"></a>
        ";
                    } else {
                        // line 237
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_google-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Google Wallet\" title=\"Google Wallet\">
        ";
                    }
                    // line 239
                    echo "        </li>
\t\t";
                }
                // line 241
                echo "        
        ";
                // line 242
                if (((isset($context["t1o_payment_2co"]) ? $context["t1o_payment_2co"] : null) == 1)) {
                    // line 243
                    echo "        <li data-toggle=\"tooltip\" title=\"2CheckOut\">
        ";
                    // line 244
                    if (((isset($context["t1o_payment_2co_url"]) ? $context["t1o_payment_2co_url"] : null) != "")) {
                        // line 245
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_2co_url"]) ? $context["t1o_payment_2co_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_2co-";
                        // line 246
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"2CheckOut\" title=\"2CheckOut\"></a>
        ";
                    } else {
                        // line 248
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_2co-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"2CheckOut\" title=\"2CheckOut\">
        ";
                    }
                    // line 250
                    echo "        </li>
\t\t";
                }
                // line 251
                echo " 
        
        ";
                // line 253
                if (((isset($context["t1o_payment_sage"]) ? $context["t1o_payment_sage"] : null) == 1)) {
                    // line 254
                    echo "        <li data-toggle=\"tooltip\" title=\"Sage\">
        ";
                    // line 255
                    if (((isset($context["t1o_payment_sage_url"]) ? $context["t1o_payment_sage_url"] : null) != "")) {
                        // line 256
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_sage_url"]) ? $context["t1o_payment_sage_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_sage-";
                        // line 257
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Sage\" title=\"Sage\"></a>
        ";
                    } else {
                        // line 259
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_sage-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Sage\" title=\"Sage\">
        ";
                    }
                    // line 261
                    echo "        </li>
\t\t";
                }
                // line 262
                echo "   
        
        ";
                // line 264
                if (((isset($context["t1o_payment_solo"]) ? $context["t1o_payment_solo"] : null) == 1)) {
                    // line 265
                    echo "        <li data-toggle=\"tooltip\" title=\"Solo\">
        ";
                    // line 266
                    if (((isset($context["t1o_payment_solo_url"]) ? $context["t1o_payment_solo_url"] : null) != "")) {
                        // line 267
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_solo_url"]) ? $context["t1o_payment_solo_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_solo-";
                        // line 268
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Solo\" title=\"Solo\"></a>
        ";
                    } else {
                        // line 270
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_solo-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Solo\" title=\"Solo\">
        ";
                    }
                    // line 272
                    echo "        </li>
\t\t";
                }
                // line 273
                echo " 
        
        ";
                // line 275
                if (((isset($context["t1o_payment_amazon"]) ? $context["t1o_payment_amazon"] : null) == 1)) {
                    // line 276
                    echo "        <li data-toggle=\"tooltip\" title=\"Amazon Payments\">
        ";
                    // line 277
                    if (((isset($context["t1o_payment_amazon_url"]) ? $context["t1o_payment_amazon_url"] : null) != "")) {
                        // line 278
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_amazon_url"]) ? $context["t1o_payment_amazon_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_amazon-";
                        // line 279
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Amazon Payments\" title=\"Amazon Payments\"></a>
        ";
                    } else {
                        // line 281
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_amazon-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Amazon Payments\" title=\"Amazon Payments\">
        ";
                    }
                    // line 283
                    echo "        </li>
\t\t";
                }
                // line 285
                echo "        
        ";
                // line 286
                if (((isset($context["t1o_payment_western_union"]) ? $context["t1o_payment_western_union"] : null) == 1)) {
                    // line 287
                    echo "        <li data-toggle=\"tooltip\" title=\"Western Union\">
        ";
                    // line 288
                    if (((isset($context["t1o_payment_western_union_url"]) ? $context["t1o_payment_western_union_url"] : null) != "")) {
                        // line 289
                        echo "\t\t\t<a href=\"";
                        echo (isset($context["t1o_payment_western_union_url"]) ? $context["t1o_payment_western_union_url"] : null);
                        echo "\" target=\"_blank\">
\t\t\t<img src=\"catalog/view/theme/oxy/image/payment/payment_image_western_union-";
                        // line 290
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Western Union\" title=\"Western Union\"></a>
        ";
                    } else {
                        // line 292
                        echo "            <img src=\"catalog/view/theme/oxy/image/payment/payment_image_western_union-";
                        echo (isset($context["t1d_f3_payment_images_style"]) ? $context["t1d_f3_payment_images_style"] : null);
                        echo ".png\" alt=\"Western Union\" title=\"Western Union\">
        ";
                    }
                    // line 294
                    echo "        </li>
\t\t";
                }
                // line 296
                echo "            
          </ul>
      </div>
      ";
            }
            // line 300
            echo "      
      ";
            // line 301
            if (((isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null) == 1)) {
                // line 302
                echo "      <div id=\"powered-content\" class=\"col-sm-";
                echo intval(floor((12 / (((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) + (isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null)) + (isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null)))));
                echo "\">
      ";
                // line 303
                echo (isset($context["t1o_powered_content"]) ? $context["t1o_powered_content"] : null);
                echo "
      </div>
      ";
            }
            // line 306
            echo "      
      ";
            // line 307
            if (((isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null) == 1)) {
                // line 308
                echo "      <div id=\"footer-social-wrapper\" class=\"col-sm-";
                echo intval(floor((12 / (((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) + (isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null)) + (isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null)))));
                echo "\">
      <ul id=\"footer-social\" class=\"list-inline\">

            ";
                // line 311
                if (((isset($context["t1o_facebook"]) ? $context["t1o_facebook"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Facebook\" class=\"facebook\"><a href=\"";
                    // line 312
                    echo (isset($context["t1o_facebook"]) ? $context["t1o_facebook"] : null);
                    echo "\" title=\"Facebook\" target=\"_blank\"><i class=\"fa fa-facebook\"></i></a></li>
            ";
                }
                // line 314
                echo "\t        ";
                if (((isset($context["t1o_twitter"]) ? $context["t1o_twitter"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Twitter\" class=\"twitter\"><a href=\"";
                    // line 315
                    echo (isset($context["t1o_twitter"]) ? $context["t1o_twitter"] : null);
                    echo "\" title=\"Twitter\" target=\"_blank\"><i class=\"fa fa-twitter\"></i></a></li>
            ";
                }
                // line 317
                echo "\t        ";
                if (((isset($context["t1o_googleplus"]) ? $context["t1o_googleplus"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Google+\" class=\"google\"><a href=\"";
                    // line 318
                    echo (isset($context["t1o_googleplus"]) ? $context["t1o_googleplus"] : null);
                    echo "\" title=\"Google+\" target=\"_blank\"><i class=\"fa fa-google-plus\"></i></a></li>
            ";
                }
                // line 320
                echo "\t        ";
                if (((isset($context["t1o_rss"]) ? $context["t1o_rss"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"RSS\" class=\"rrs\"><a href=\"";
                    // line 321
                    echo (isset($context["t1o_rss"]) ? $context["t1o_rss"] : null);
                    echo "\" title=\"RSS\" target=\"_blank\"><i class=\"fa fa-rss\"></i></a></li>
            ";
                }
                // line 323
                echo "\t        ";
                if (((isset($context["t1o_pinterest"]) ? $context["t1o_pinterest"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Pinterest\" class=\"pinterest\"><a href=\"";
                    // line 324
                    echo (isset($context["t1o_pinterest"]) ? $context["t1o_pinterest"] : null);
                    echo "\" title=\"Pinterest\" target=\"_blank\"><i class=\"fa fa-pinterest\"></i></a></li>
            ";
                }
                // line 326
                echo "\t        ";
                if (((isset($context["t1o_vimeo"]) ? $context["t1o_vimeo"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Vimeo\" class=\"vimeo\"><a href=\"";
                    // line 327
                    echo (isset($context["t1o_vimeo"]) ? $context["t1o_vimeo"] : null);
                    echo "\" title=\"Vimeo\" target=\"_blank\"><i class=\"fa fa-vimeo-square\"></i></a></li>
            ";
                }
                // line 328
                echo " 
\t        ";
                // line 329
                if (((isset($context["t1o_flickr"]) ? $context["t1o_flickr"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Flickr\" class=\"flickr\"><a href=\"";
                    // line 330
                    echo (isset($context["t1o_flickr"]) ? $context["t1o_flickr"] : null);
                    echo "\" title=\"Flickr\" target=\"_blank\"><i class=\"fa fa-flickr\"></i></a></li>
            ";
                }
                // line 331
                echo "  
\t        ";
                // line 332
                if (((isset($context["t1o_linkedin"]) ? $context["t1o_linkedin"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"LinkedIn\" class=\"linkedin\"><a href=\"";
                    // line 333
                    echo (isset($context["t1o_linkedin"]) ? $context["t1o_linkedin"] : null);
                    echo "\" title=\"LinkedIn\" target=\"_blank\"><i class=\"fa fa-linkedin\"></i></a></li>
            ";
                }
                // line 335
                echo "\t        ";
                if (((isset($context["t1o_youtube"]) ? $context["t1o_youtube"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"YouTube\" class=\"youtube\"><a href=\"";
                    // line 336
                    echo (isset($context["t1o_youtube"]) ? $context["t1o_youtube"] : null);
                    echo "\" title=\"YouTube\" target=\"_blank\"><i class=\"fa fa-youtube\"></i></a></li>
            ";
                }
                // line 338
                echo "\t        ";
                if (((isset($context["t1o_dribbble"]) ? $context["t1o_dribbble"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Dribbble\" class=\"dribbble\"><a href=\"";
                    // line 339
                    echo (isset($context["t1o_dribbble"]) ? $context["t1o_dribbble"] : null);
                    echo "\" title=\"Dribbble\" target=\"_blank\"><i class=\"fa fa-dribbble\"></i></a></li>
            ";
                }
                // line 341
                echo "            ";
                if (((isset($context["t1o_instagram"]) ? $context["t1o_instagram"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Instagram\" class=\"instagram\"><a href=\"";
                    // line 342
                    echo (isset($context["t1o_instagram"]) ? $context["t1o_instagram"] : null);
                    echo "\" title=\"Instagram\" target=\"_blank\"><i class=\"fa fa-instagram\"></i></a></li>
            ";
                }
                // line 343
                echo "   
            ";
                // line 344
                if (((isset($context["t1o_behance"]) ? $context["t1o_behance"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Behance\" class=\"behance\"><a href=\"";
                    // line 345
                    echo (isset($context["t1o_behance"]) ? $context["t1o_behance"] : null);
                    echo "\" title=\"Behance\" target=\"_blank\"><i class=\"fa fa-behance\"></i></a></li>
            ";
                }
                // line 346
                echo "   
            ";
                // line 347
                if (((isset($context["t1o_skype"]) ? $context["t1o_skype"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Skype\" class=\"skype\"><a href=\"skype:";
                    // line 348
                    echo (isset($context["t1o_skype"]) ? $context["t1o_skype"] : null);
                    echo "\" title=\"Skype\" target=\"_blank\"><i class=\"fa fa-skype\"></i></a></li>
            ";
                }
                // line 349
                echo "    
            ";
                // line 350
                if (((isset($context["t1o_tumblr"]) ? $context["t1o_tumblr"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Tumblr\" class=\"tumblr\"><a href=\"";
                    // line 351
                    echo (isset($context["t1o_tumblr"]) ? $context["t1o_tumblr"] : null);
                    echo "\" title=\"Tumblr\" target=\"_blank\"><i class=\"fa fa-tumblr\"></i></a></li>
            ";
                }
                // line 353
                echo "            ";
                if (((isset($context["t1o_reddit"]) ? $context["t1o_reddit"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"Reddit\" class=\"reddit\"><a href=\"";
                    // line 354
                    echo (isset($context["t1o_reddit"]) ? $context["t1o_reddit"] : null);
                    echo "\" title=\"Reddit\" target=\"_blank\"><i class=\"fa fa-reddit\"></i></a></li>
            ";
                }
                // line 356
                echo "            ";
                if (((isset($context["t1o_vk"]) ? $context["t1o_vk"] : null) != "")) {
                    echo " 
            <li data-toggle=\"tooltip\" title=\"VK\" class=\"vk\"><a href=\"";
                    // line 357
                    echo (isset($context["t1o_vk"]) ? $context["t1o_vk"] : null);
                    echo "\" title=\"VK\" target=\"_blank\"><i class=\"fa fa-vk\"></i></a></li>
            ";
                }
                // line 358
                echo "   
        
      </ul>
      </div>
      ";
            }
            // line 363
            echo "      
    </div>
  </div>
  </div>
  ";
        }
        // line 368
        echo "  
  ";
        // line 369
        if (((isset($context["t1o_custom_bottom_1_status"]) ? $context["t1o_custom_bottom_1_status"] : null) == 1)) {
            // line 370
            echo "  <div id=\"footer_custom_1\">
  <div class=\"container\">
  <div class=\"row come-item\">
  <div class=\"col-sm-12 padd-t-b-30\">
    ";
            // line 374
            echo (isset($context["t1o_custom_bottom_1_content"]) ? $context["t1o_custom_bottom_1_content"] : null);
            echo "
  </div>
  </div>
  </div>
  </div>
  ";
        }
        // line 380
        echo "
</footer>



<script src=\"catalog/view/theme/oxy/js/jquery.visible.min.js\" type=\"text/javascript\"></script>
<script type=\"text/javascript\">

var win = \$(window);

var allMods = \$(\".come-item\");

allMods.each(function(i, el) {
    
  if (\$(el).visible(true)) {
    \$(el).addClass(\"already-visible\"); 
  }
  
});

win.scroll(function(event) {
  
  allMods.each(function(i, el) {
    
    var el = \$(el);
    
    if (el.visible(true)) {
      el.addClass(\"come-in\"); 
    } else {
      el.removeClass(\"come-in already-visible\");
    }
    
  });
  
}); 
</script>
<script>

    // Product List
\t\$('#list-view').click(function() {
\t\t\$('#content .product-layout > .clearfix').remove();
        
\t\t\$('#content .category-product-items').attr('class', 'row product-items category-product-items product-list-wrapper');
\t\t\$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');

\t\tlocalStorage.setItem('display', 'list');
\t});
\t
\t// Product Small List
\t\$('#small-list-view').click(function() {
\t\t\$('#content .product-layout > .clearfix').remove();
        
\t\t\$('#content .category-product-items').attr('class', 'row product-items category-product-items product-list-wrapper');
\t\t\$('#content .product-layout').attr('class', 'product-layout product-list product-small-list col-xs-12');

\t\tlocalStorage.setItem('display', 'small-list');
\t});

\t// Product Grid
\t\$('#grid-view').click(function() {
\t\t\$('#content .product-layout > .clearfix').remove();

\t\t// What a shame bootstrap does not take into account dynamically loaded columns
\t\tcols = \$('#column-right, #column-left').length;

\t\t\t\$('#content .category-product-items').attr('class', 'row product-items category-product-items product-grid-wrapper');
\t\t\t\$('#content .product-layout').attr('class', 'product-layout product-grid ";
        // line 446
        echo (isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null);
        echo " col-lg-";
        echo (isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null);
        echo " col-md-";
        echo (isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null);
        echo " col-sm-6 col-xs-6');
\t\t
\t\t localStorage.setItem('display', 'grid');
\t});
\t
\t// Product Gallery
\t\$('#gallery-view').click(function() {
\t\t\$('#content .product-layout > .clearfix').remove();

\t\t// What a shame bootstrap does not take into account dynamically loaded columns
\t\tcols = \$('#column-right, #column-left').length;

\t\t\t\$('#content .category-product-items').attr('class', 'row product-items category-product-items product-gallery-wrapper');
\t\t\t\$('#content .product-layout').attr('class', 'product-layout product-gallery col-lg-";
        // line 459
        echo (isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null);
        echo " col-md-";
        echo (isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null);
        echo " col-sm-6 col-xs-6');

\t\t localStorage.setItem('display', 'gallery');
\t});

\tif (localStorage.getItem('display') == 'list') {
\t\t\$('#list-view').trigger('click');
\t} else if (localStorage.getItem('display') == 'small-list') {
\t\t\$('#small-list-view').trigger('click');
\t} else if (localStorage.getItem('display') == 'gallery') {
\t\t\$('#gallery-view').trigger('click');
\t} else {
\t\t\$('#grid-view').trigger('click');
\t}
\t
\t// Module Grid
\tcols2 = \$('#column-right, #column-left').length;

\t\t\$('#content .product-layout-grid').attr('class', 'product-layout product-grid ";
        // line 477
        echo (isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null);
        echo " col-lg-";
        echo (isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null);
        echo " col-md-";
        echo (isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null);
        echo " col-sm-6 col-xs-6');
\t\t\$('.product-layout-slider').attr('class', 'product-layout-slider  product-grid ";
        // line 478
        echo (isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null);
        echo " col-xs-12');

</script>
<script src=\"catalog/view/theme/oxy/js/tickerme.js\" type=\"text/javascript\"></script>
<script type=\"text/javascript\">
\$(function(){
\t\$('#ticker').tickerme();
});
</script>
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/jquery.easing-1.3.min.js\"></script>
";
        // line 488
        if (((isset($context["t1o_header_fixed_header_status"]) ? $context["t1o_header_fixed_header_status"] : null) == 1)) {
            // line 489
            echo "<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/jquery.sticky.js\"></script>
";
        }
        // line 491
        if (((isset($context["t1o_header_auto_suggest_status"]) ? $context["t1o_header_auto_suggest_status"] : null) == 1)) {
            // line 492
            echo "<link rel=\"stylesheet\" property=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/oxy/stylesheet/livesearch.css\" />
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/livesearch.js\"></script>
";
        }
        // line 495
        if (((isset($context["t1o_others_totop"]) ? $context["t1o_others_totop"] : null) == 1)) {
            echo "\t
<link rel=\"stylesheet\" property=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/oxy/stylesheet/ui.totop.css\" />
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/jquery.ui.totop.js\"></script>
<script type=\"text/javascript\">
\tjQuery(document).ready(function() {\t
\t\t\$().UItoTop({ easingType: 'easeOutQuart' });\t
\t});
</script>
";
        }
        // line 504
        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"catalog/view/theme/oxy/stylesheet/animate.css\" />
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/quickview/quickview.js\"></script>\t\t
<link rel=\"stylesheet\" property=\"stylesheet\" href=\"catalog/view/theme/oxy/js/quickview/fancybox/jquery.fancybox.css\" />
<script src=\"catalog/view/theme/oxy/js/quickview/fancybox/jquery.fancybox.pack.js\"></script>
<script src=\"catalog/view/theme/oxy/js/jquery.stellar.js\" type=\"text/javascript\"></script>
";
        // line 509
        if (((isset($context["t1o_eu_cookie_status"]) ? $context["t1o_eu_cookie_status"] : null) == 1)) {
            echo "\t
<script src=\"catalog/view/theme/oxy/js/jquery.eucookiebar.js\" type=\"text/javascript\"></script>
<script type=\"text/javascript\">
    \$(document).ready(function() {
      \$('.cookie-message').cookieBar({ closeButton : '.my-close-button' });
    });
</script>
<div class=\"cookie-message hidden-xs\">
  <span class=\"cookie-img\"><img src=\"catalog/view/theme/oxy/image/321_cookie.png\" alt=\"Cookie\" title=\"Cookie\"></span>
  ";
            // line 518
            echo (isset($context["t1o_eu_cookie_message"]) ? $context["t1o_eu_cookie_message"] : null);
            echo "
  <a class=\"my-close-button btn btn-primary\" href=\"#\">";
            // line 519
            echo $this->getAttribute((isset($context["t1o_eu_cookie_close"]) ? $context["t1o_eu_cookie_close"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a>
</div>
";
        }
        // line 522
        echo "
<link rel=\"stylesheet\" property=\"stylesheet\" type=\"text/css\" href=\"catalog/view/javascript/jquery/magnific/magnific-popup.css\" />\t
<script type=\"text/javascript\" src=\"catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js\"></script>
<script type=\"text/javascript\" src=\"catalog/view/theme/oxy/js/flexmenu.min.js\"></script>
<script>
\t\$('ul.menu.flex').flexMenu();
\t\$('ul.menu.flex-multi').flexMenu({
\t\tshowOnHover: false
\t})
</script>
";
        // line 532
        if (((isset($context["t1o_custom_js"]) ? $context["t1o_custom_js"] : null) != "")) {
            // line 533
            echo (isset($context["t1o_custom_js"]) ? $context["t1o_custom_js"] : null);
            echo "
";
        }
        // line 535
        echo "
</div>
";
        // line 537
        if ((((isset($context["t1o_custom_bottom_2_status"]) ? $context["t1o_custom_bottom_2_status"] : null) == 1) && ((isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null) == "full-width"))) {
            // line 538
            echo "<div id=\"footer_custom_2\" class=\"hidden-xs\">
<div class=\"container\">
<div class=\"row\">
<div class=\"col-sm-12 padd-t-b-30\">
";
            // line 542
            echo (isset($context["t1o_custom_bottom_2_content"]) ? $context["t1o_custom_bottom_2_content"] : null);
            echo "
</div>
</div>
</div>
</div>
";
        }
        // line 548
        echo "</body></html>";
    }

    public function getTemplateName()
    {
        return "oxy/template/common/footer.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1330 => 548,  1321 => 542,  1315 => 538,  1313 => 537,  1309 => 535,  1304 => 533,  1302 => 532,  1290 => 522,  1284 => 519,  1280 => 518,  1268 => 509,  1261 => 504,  1249 => 495,  1244 => 492,  1242 => 491,  1238 => 489,  1236 => 488,  1223 => 478,  1215 => 477,  1192 => 459,  1172 => 446,  1104 => 380,  1095 => 374,  1089 => 370,  1087 => 369,  1084 => 368,  1077 => 363,  1070 => 358,  1065 => 357,  1060 => 356,  1055 => 354,  1050 => 353,  1045 => 351,  1041 => 350,  1038 => 349,  1033 => 348,  1029 => 347,  1026 => 346,  1021 => 345,  1017 => 344,  1014 => 343,  1009 => 342,  1004 => 341,  999 => 339,  994 => 338,  989 => 336,  984 => 335,  979 => 333,  975 => 332,  972 => 331,  967 => 330,  963 => 329,  960 => 328,  955 => 327,  950 => 326,  945 => 324,  940 => 323,  935 => 321,  930 => 320,  925 => 318,  920 => 317,  915 => 315,  910 => 314,  905 => 312,  901 => 311,  894 => 308,  892 => 307,  889 => 306,  883 => 303,  878 => 302,  876 => 301,  873 => 300,  867 => 296,  863 => 294,  857 => 292,  852 => 290,  847 => 289,  845 => 288,  842 => 287,  840 => 286,  837 => 285,  833 => 283,  827 => 281,  822 => 279,  817 => 278,  815 => 277,  812 => 276,  810 => 275,  806 => 273,  802 => 272,  796 => 270,  791 => 268,  786 => 267,  784 => 266,  781 => 265,  779 => 264,  775 => 262,  771 => 261,  765 => 259,  760 => 257,  755 => 256,  753 => 255,  750 => 254,  748 => 253,  744 => 251,  740 => 250,  734 => 248,  729 => 246,  724 => 245,  722 => 244,  719 => 243,  717 => 242,  714 => 241,  710 => 239,  704 => 237,  699 => 235,  694 => 234,  692 => 233,  689 => 232,  687 => 231,  683 => 229,  679 => 228,  673 => 226,  668 => 224,  663 => 223,  661 => 222,  658 => 221,  656 => 220,  652 => 218,  648 => 217,  642 => 215,  637 => 213,  632 => 212,  630 => 211,  627 => 210,  625 => 209,  621 => 207,  617 => 206,  611 => 204,  606 => 202,  601 => 201,  599 => 200,  596 => 199,  594 => 198,  590 => 196,  586 => 195,  580 => 193,  575 => 191,  570 => 190,  568 => 189,  565 => 188,  563 => 187,  559 => 185,  555 => 184,  549 => 182,  544 => 180,  539 => 179,  537 => 178,  534 => 177,  532 => 176,  529 => 175,  525 => 173,  519 => 171,  514 => 169,  509 => 168,  507 => 167,  504 => 166,  502 => 165,  498 => 163,  494 => 162,  488 => 160,  483 => 158,  478 => 157,  476 => 156,  473 => 155,  471 => 154,  467 => 152,  463 => 151,  457 => 149,  452 => 147,  447 => 146,  445 => 145,  442 => 144,  440 => 143,  436 => 141,  432 => 140,  426 => 138,  421 => 136,  416 => 135,  414 => 134,  411 => 133,  409 => 132,  406 => 131,  403 => 130,  400 => 129,  394 => 126,  391 => 125,  385 => 122,  381 => 121,  376 => 120,  374 => 119,  371 => 118,  368 => 117,  366 => 116,  361 => 115,  358 => 114,  356 => 113,  353 => 112,  351 => 111,  344 => 108,  342 => 107,  336 => 103,  334 => 102,  331 => 101,  325 => 97,  318 => 93,  315 => 92,  307 => 87,  302 => 85,  298 => 84,  293 => 82,  290 => 81,  288 => 80,  285 => 79,  279 => 77,  277 => 76,  272 => 75,  270 => 74,  266 => 72,  261 => 69,  256 => 66,  247 => 65,  238 => 64,  229 => 63,  220 => 62,  211 => 61,  203 => 60,  198 => 58,  193 => 57,  190 => 56,  185 => 53,  176 => 52,  167 => 51,  158 => 50,  149 => 49,  141 => 48,  136 => 46,  131 => 45,  128 => 44,  124 => 42,  120 => 40,  109 => 38,  105 => 37,  102 => 36,  100 => 35,  96 => 34,  91 => 33,  89 => 32,  83 => 30,  81 => 29,  78 => 28,  71 => 25,  65 => 23,  63 => 22,  58 => 21,  56 => 20,  51 => 17,  49 => 16,  46 => 15,  37 => 9,  31 => 5,  29 => 4,  22 => 2,  19 => 1,);
    }
}
/* */
/* <footer {% if t1d_bg_image_f1_parallax == 1 %}data-stellar-background-ratio="0.5"{% endif %}>*/
/* */
/*   {% if t1o_custom_top_1_status == 1 %}*/
/*   <div id="footer_custom_top_1">*/
/*   <div class="container">*/
/*   <div class="row come-item">*/
/*   <div class="col-sm-12 padd-t-b-30">*/
/*     {{ t1o_custom_top_1_content }}*/
/*   </div>*/
/*   </div>*/
/*   </div>*/
/*   </div>*/
/*   {% endif %}*/
/* */
/*   {% if t1o_information_column_1_status == 1 or t1o_information_column_2_status == 1 or t1o_information_column_3_status == 1 or t1o_custom_1_status == 1 or t1o_custom_2_status == 1 %}*/
/*   <div id="information">*/
/*   <div class="container">*/
/*     <div class="row come-item">*/
/*     {% if t1o_custom_1_status == 1 %}*/
/*       <div class="col-sm-{{ t1o_custom_1_column_width }} col-xs-12">*/
/*         {% if t1o_custom_1_title[lang_id] %}*/
/*         <h4>{{ t1o_custom_1_title[lang_id] }}</h4>*/
/*         {% endif %}*/
/*         {{ t1o_custom_1_content }}*/
/*       </div>*/
/*     {% endif %}*/
/*       */
/*     {% if t1o_information_column_1_status == 1 or t1o_information_column_2_status == 1 or t1o_information_column_3_status == 1 %}*/
/*     <div class="col-sm-{{ t1o_information_block_width }} col-xs-12">*/
/*     <div id="information-block-containter">*/
/*       {% if t1o_information_column_1_status == 1 %}*/
/*       <div class="col-sm-{{ 12 // (t1o_information_column_1_status + t1o_information_column_2_status + t1o_information_column_3_status ) }}  col-xs-12">*/
/*         <h4>{{ text_information }}</h4>*/
/*         {% if informations %}*/
/*         <ul class="list-unstyled">*/
/*           {% for information in informations %}*/
/*           <li><a href="{{ information.href }}">{{ information.title }}</a></li>*/
/*           {% endfor %}*/
/*         </ul>*/
/*         {% endif %}*/
/*       </div>*/
/*       {% endif %}*/
/*       {% if t1o_information_column_2_status == 1 %}*/
/*       <div class="col-sm-{{ 12 // (t1o_information_column_1_status + t1o_information_column_2_status + t1o_information_column_3_status ) }}  col-xs-12">*/
/*         <h4>{{ text_service }}</h4>*/
/*         <ul class="list-unstyled">*/
/*           {% if t1o_i_c_2_1_status == 1 %}<li><a href="{{ contact }}">{{ text_contact }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_2_status == 1 %}<li><a href="{{ account }}">{{ text_account }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_3_status == 1 %}<li><a href="{{ return }}">{{ text_return }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_4_status == 1 %}<li><a href="{{ order }}">{{ text_order }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_2_5_status == 1 %}<li><a href="{{ wishlist }}">{{ text_wishlist }}</a></li>{% endif %}*/
/*         </ul>*/
/*       </div>*/
/*       {% endif %}*/
/*       {% if t1o_information_column_3_status == 1 %}*/
/*       <div class="col-sm-{{ 12 // (t1o_information_column_1_status + t1o_information_column_2_status + t1o_information_column_3_status ) }}  col-xs-12">*/
/*         <h4>{{ text_extra }}</h4>*/
/*         <ul class="list-unstyled">*/
/*           {% if t1o_i_c_3_1_status == 1 %}<li><a href="{{ manufacturer }}">{{ text_manufacturer }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_2_status == 1 %}<li><a href="{{ voucher }}">{{ text_voucher }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_3_status == 1 %}<li><a href="{{ affiliate }}">{{ text_affiliate }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_4_status == 1 %}<li><a href="{{ special }}">{{ text_special }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_5_status == 1 %}<li><a href="{{ newsletter }}">{{ text_newsletter }}</a></li>{% endif %}*/
/*           {% if t1o_i_c_3_6_status == 1 %}<li><a href="{{ sitemap }}">{{ text_sitemap }}</a></li>{% endif %}*/
/*         </ul>*/
/*       </div>*/
/*       {% endif %}*/
/*     </div>*/
/*     </div>*/
/*     {% endif %}*/
/*       */
/*       */
/*       {% if t1o_custom_2_status == 1 %}*/
/*       <div class="col-sm-{{ t1o_custom_2_column_width }} col-xs-12">*/
/*         {% if t1o_custom_2_title[lang_id] %}*/
/*         <h4>{{ t1o_custom_2_title[lang_id] }}</h4>*/
/*         {% endif %}*/
/*         */
/*         {% if t1o_newsletter_status == 1 %}*/
/*         <div class="newsletter-block">                   */
/*         <form action="{{ t1o_newsletter_campaign_url }}" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>*/
/*             <div id="mc_embed_signup_scroll">*/
/* 	        <label for="mce-EMAIL">{{ t1o_newsletter_promo_text[lang_id] }}</label>*/
/* 	        <input type="email" value="" name="EMAIL" class="email form-group form-control" id="mce-EMAIL" placeholder="{{ t1o_newsletter_email[lang_id] }}" required>*/
/*             <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_619755c76ad5fa60a96e52bec_c5fb795f8e" tabindex="-1" value=""></div>*/
/*             <div class="clear"><input type="submit" value="{{ t1o_newsletter_subscribe[lang_id] }}" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary"></div>*/
/*             </div>*/
/*         </form>*/
/*         </div>*/
/*         {% endif %}*/
/*         */
/*         {{ t1o_custom_2_content }}*/
/*         */
/*       </div>*/
/*       {% endif %}*/
/*     </div>*/
/*   </div>*/
/*   </div>*/
/*   {% endif %}*/
/*   */
/*   {% if t1o_powered_status == 1 or t1o_follow_us_status == 1 or t1o_payment_block_status == 1 %}*/
/*   <div id="powered">*/
/*   <div class="container">*/
/*     <div class="row come-item">*/
/*     */
/*       {% if t1o_payment_block_status == 1 %}*/
/*       <div id="footer-payment-wrapper" class="col-sm-{{ 12 // (t1o_payment_block_status + t1o_powered_status + t1o_follow_us_status) }}">*/
/*         <ul id="footer-payment" class="list-inline">*/
/*             */
/*         {% if t1o_payment_block_custom_status == 1 %}*/
/*         */
/*         {% if _SERVER.HTTPS and _SERVER.HTTPS != 'off' %}*/
/* 			{% set path_image = config_ssl ~ 'image/' %}*/
/* 		{% else %}  */
/* 			{% set path_image = config_url ~ 'image/' %}*/
/* 		{% endif %}*/
/*         */
/*         {% if t1o_payment_block_custom != '' %}*/
/*         {% if t1o_payment_block_custom_url != '' %} */
/* 		<li><a href="{{ t1o_payment_block_custom_url }}" target="_blank">*/
/* 			<img src="{{ path_image ~ t1o_payment_block_custom }}" alt="Payment" title="Payment"></a>*/
/*         </li>*/
/*         {% else %}*/
/*         <li>       */
/* 			<img src="{{ path_image ~ t1o_payment_block_custom }}" alt="Payment" title="Payment">*/
/*         </li>*/
/*         {% endif %}*/
/* 		{% endif %}*/
/* 		{% endif %}*/
/*         */
/*         {% if t1o_payment_paypal == 1 %}*/
/* 		<li data-toggle="tooltip" title="PayPal">*/
/*         {% if t1o_payment_paypal_url != '' %}*/
/*             <a href="{{ t1o_payment_paypal_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_paypal-{{ t1d_f3_payment_images_style }}.png" alt="PayPal" title="PayPal"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_paypal-{{ t1d_f3_payment_images_style }}.png" alt="PayPal" title="PayPal">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %} */
/*         */
/*         {% if t1o_payment_visa == 1 %}*/
/*         <li data-toggle="tooltip" title="Visa">*/
/*         {% if t1o_payment_visa_url != '' %}*/
/* 			<a href="{{ t1o_payment_visa_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_visa-{{ t1d_f3_payment_images_style }}.png" alt="Visa" title="Visa"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_visa-{{ t1d_f3_payment_images_style }}.png" alt="Visa" title="Visa">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}   */
/*         */
/*         {% if t1o_payment_mastercard == 1 %}*/
/*         <li data-toggle="tooltip" title="MasterCard">*/
/*         {% if t1o_payment_mastercard_url != '' %}*/
/* 			<a href="{{ t1o_payment_mastercard_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_mastercard-{{ t1d_f3_payment_images_style }}.png" alt="MasterCard" title="MasterCard"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_mastercard-{{ t1d_f3_payment_images_style }}.png" alt="MasterCard" title="MasterCard">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %} */
/*        */
/*         {% if t1o_payment_maestro == 1 %}*/
/*         <li data-toggle="tooltip" title="Maestro">*/
/*         {% if t1o_payment_maestro_url != '' %}*/
/* 			<a href="{{ t1o_payment_maestro_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_maestro-{{ t1d_f3_payment_images_style }}.png" alt="Maestro" title="Maestro"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_maestro-{{ t1d_f3_payment_images_style }}.png" alt="Maestro" title="Maestro">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}*/
/*        */
/*         {% if t1o_payment_discover == 1 %}*/
/*         <li data-toggle="tooltip" title="Discover">*/
/*         {% if t1o_payment_discover_url != '' %}*/
/* 			<a href="{{ t1o_payment_discover_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_discover-{{ t1d_f3_payment_images_style }}.png" alt="Discover" title="Discover"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_discover-{{ t1d_f3_payment_images_style }}.png" alt="Discover" title="Discover">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}                   */
/*         */
/*         {% if t1o_payment_skrill == 1 %}*/
/*         <li data-toggle="tooltip" title="Skrill">*/
/*         {% if t1o_payment_skrill_url != '' %}*/
/* 			<a href="{{ t1o_payment_skrill_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_skrill-{{ t1d_f3_payment_images_style }}.png" alt="Skrill" title="Skrill"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_skrill-{{ t1d_f3_payment_images_style }}.png" alt="Skrill" title="Skrill">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}   */
/*         */
/*         {% if t1o_payment_american_express == 1 %}*/
/*         <li data-toggle="tooltip" title="American Express">*/
/*         {% if t1o_payment_american_express_url != '' %}*/
/* 			<a href="{{ t1o_payment_american_express_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_american_express-{{ t1d_f3_payment_images_style }}.png" alt="American Express" title="American Express"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_american_express-{{ t1d_f3_payment_images_style }}.png" alt="American Express" title="American Express">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %} */
/*                    */
/*         {% if t1o_payment_cirrus == 1 %}*/
/*         <li data-toggle="tooltip" title="Cirrus">*/
/*         {% if t1o_payment_cirrus_url != '' %}*/
/* 			<a href="{{ t1o_payment_cirrus_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_cirrus-{{ t1d_f3_payment_images_style }}.png" alt="Cirrus" title="Cirrus"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_cirrus-{{ t1d_f3_payment_images_style }}.png" alt="Cirrus" title="Cirrus">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}   */
/*         */
/*         {% if t1o_payment_delta == 1 %}*/
/*         <li data-toggle="tooltip" title="Delta">*/
/*         {% if t1o_payment_delta_url != '' %}*/
/* 			<a href="{{ t1o_payment_delta_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_delta-{{ t1d_f3_payment_images_style }}.png" alt="Delta" title="Delta"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_delta-{{ t1d_f3_payment_images_style }}.png" alt="Delta" title="Delta">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}   */
/*         */
/*         {% if t1o_payment_google == 1 %}*/
/*         <li data-toggle="tooltip" title="Google Wallet">*/
/*         {% if t1o_payment_google_url != '' %}*/
/* 			<a href="{{ t1o_payment_google_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_google-{{ t1d_f3_payment_images_style }}.png" alt="Google Wallet" title="Google Wallet"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_google-{{ t1d_f3_payment_images_style }}.png" alt="Google Wallet" title="Google Wallet">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}*/
/*         */
/*         {% if t1o_payment_2co == 1 %}*/
/*         <li data-toggle="tooltip" title="2CheckOut">*/
/*         {% if t1o_payment_2co_url != '' %}*/
/* 			<a href="{{ t1o_payment_2co_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_2co-{{ t1d_f3_payment_images_style }}.png" alt="2CheckOut" title="2CheckOut"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_2co-{{ t1d_f3_payment_images_style }}.png" alt="2CheckOut" title="2CheckOut">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %} */
/*         */
/*         {% if t1o_payment_sage == 1 %}*/
/*         <li data-toggle="tooltip" title="Sage">*/
/*         {% if t1o_payment_sage_url != '' %}*/
/* 			<a href="{{ t1o_payment_sage_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_sage-{{ t1d_f3_payment_images_style }}.png" alt="Sage" title="Sage"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_sage-{{ t1d_f3_payment_images_style }}.png" alt="Sage" title="Sage">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}   */
/*         */
/*         {% if t1o_payment_solo == 1 %}*/
/*         <li data-toggle="tooltip" title="Solo">*/
/*         {% if t1o_payment_solo_url != '' %}*/
/* 			<a href="{{ t1o_payment_solo_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_solo-{{ t1d_f3_payment_images_style }}.png" alt="Solo" title="Solo"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_solo-{{ t1d_f3_payment_images_style }}.png" alt="Solo" title="Solo">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %} */
/*         */
/*         {% if t1o_payment_amazon == 1 %}*/
/*         <li data-toggle="tooltip" title="Amazon Payments">*/
/*         {% if t1o_payment_amazon_url != '' %}*/
/* 			<a href="{{ t1o_payment_amazon_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_amazon-{{ t1d_f3_payment_images_style }}.png" alt="Amazon Payments" title="Amazon Payments"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_amazon-{{ t1d_f3_payment_images_style }}.png" alt="Amazon Payments" title="Amazon Payments">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}*/
/*         */
/*         {% if t1o_payment_western_union == 1 %}*/
/*         <li data-toggle="tooltip" title="Western Union">*/
/*         {% if t1o_payment_western_union_url != '' %}*/
/* 			<a href="{{ t1o_payment_western_union_url }}" target="_blank">*/
/* 			<img src="catalog/view/theme/oxy/image/payment/payment_image_western_union-{{ t1d_f3_payment_images_style }}.png" alt="Western Union" title="Western Union"></a>*/
/*         {% else %}*/
/*             <img src="catalog/view/theme/oxy/image/payment/payment_image_western_union-{{ t1d_f3_payment_images_style }}.png" alt="Western Union" title="Western Union">*/
/*         {% endif %}*/
/*         </li>*/
/* 		{% endif %}*/
/*             */
/*           </ul>*/
/*       </div>*/
/*       {% endif %}*/
/*       */
/*       {% if t1o_powered_status == 1 %}*/
/*       <div id="powered-content" class="col-sm-{{ 12 // (t1o_payment_block_status + t1o_powered_status + t1o_follow_us_status) }}">*/
/*       {{ t1o_powered_content }}*/
/*       </div>*/
/*       {% endif %}*/
/*       */
/*       {% if t1o_follow_us_status == 1 %}*/
/*       <div id="footer-social-wrapper" class="col-sm-{{ 12 // (t1o_payment_block_status + t1o_powered_status + t1o_follow_us_status) }}">*/
/*       <ul id="footer-social" class="list-inline">*/
/* */
/*             {% if t1o_facebook != '' %} */
/*             <li data-toggle="tooltip" title="Facebook" class="facebook"><a href="{{ t1o_facebook }}" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_twitter != '' %} */
/*             <li data-toggle="tooltip" title="Twitter" class="twitter"><a href="{{ t1o_twitter }}" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_googleplus != '' %} */
/*             <li data-toggle="tooltip" title="Google+" class="google"><a href="{{ t1o_googleplus }}" title="Google+" target="_blank"><i class="fa fa-google-plus"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_rss != '' %} */
/*             <li data-toggle="tooltip" title="RSS" class="rrs"><a href="{{ t1o_rss }}" title="RSS" target="_blank"><i class="fa fa-rss"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_pinterest != '' %} */
/*             <li data-toggle="tooltip" title="Pinterest" class="pinterest"><a href="{{ t1o_pinterest }}" title="Pinterest" target="_blank"><i class="fa fa-pinterest"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_vimeo != '' %} */
/*             <li data-toggle="tooltip" title="Vimeo" class="vimeo"><a href="{{ t1o_vimeo }}" title="Vimeo" target="_blank"><i class="fa fa-vimeo-square"></i></a></li>*/
/*             {% endif %} */
/* 	        {% if t1o_flickr != '' %} */
/*             <li data-toggle="tooltip" title="Flickr" class="flickr"><a href="{{ t1o_flickr }}" title="Flickr" target="_blank"><i class="fa fa-flickr"></i></a></li>*/
/*             {% endif %}  */
/* 	        {% if t1o_linkedin != '' %} */
/*             <li data-toggle="tooltip" title="LinkedIn" class="linkedin"><a href="{{ t1o_linkedin }}" title="LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_youtube != '' %} */
/*             <li data-toggle="tooltip" title="YouTube" class="youtube"><a href="{{ t1o_youtube }}" title="YouTube" target="_blank"><i class="fa fa-youtube"></i></a></li>*/
/*             {% endif %}*/
/* 	        {% if t1o_dribbble != '' %} */
/*             <li data-toggle="tooltip" title="Dribbble" class="dribbble"><a href="{{ t1o_dribbble }}" title="Dribbble" target="_blank"><i class="fa fa-dribbble"></i></a></li>*/
/*             {% endif %}*/
/*             {% if t1o_instagram != '' %} */
/*             <li data-toggle="tooltip" title="Instagram" class="instagram"><a href="{{ t1o_instagram }}" title="Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>*/
/*             {% endif %}   */
/*             {% if t1o_behance != '' %} */
/*             <li data-toggle="tooltip" title="Behance" class="behance"><a href="{{ t1o_behance }}" title="Behance" target="_blank"><i class="fa fa-behance"></i></a></li>*/
/*             {% endif %}   */
/*             {% if t1o_skype != '' %} */
/*             <li data-toggle="tooltip" title="Skype" class="skype"><a href="skype:{{ t1o_skype }}" title="Skype" target="_blank"><i class="fa fa-skype"></i></a></li>*/
/*             {% endif %}    */
/*             {% if t1o_tumblr != '' %} */
/*             <li data-toggle="tooltip" title="Tumblr" class="tumblr"><a href="{{ t1o_tumblr }}" title="Tumblr" target="_blank"><i class="fa fa-tumblr"></i></a></li>*/
/*             {% endif %}*/
/*             {% if t1o_reddit != '' %} */
/*             <li data-toggle="tooltip" title="Reddit" class="reddit"><a href="{{ t1o_reddit }}" title="Reddit" target="_blank"><i class="fa fa-reddit"></i></a></li>*/
/*             {% endif %}*/
/*             {% if t1o_vk != '' %} */
/*             <li data-toggle="tooltip" title="VK" class="vk"><a href="{{ t1o_vk }}" title="VK" target="_blank"><i class="fa fa-vk"></i></a></li>*/
/*             {% endif %}   */
/*         */
/*       </ul>*/
/*       </div>*/
/*       {% endif %}*/
/*       */
/*     </div>*/
/*   </div>*/
/*   </div>*/
/*   {% endif %}*/
/*   */
/*   {% if t1o_custom_bottom_1_status == 1 %}*/
/*   <div id="footer_custom_1">*/
/*   <div class="container">*/
/*   <div class="row come-item">*/
/*   <div class="col-sm-12 padd-t-b-30">*/
/*     {{ t1o_custom_bottom_1_content }}*/
/*   </div>*/
/*   </div>*/
/*   </div>*/
/*   </div>*/
/*   {% endif %}*/
/* */
/* </footer>*/
/* */
/* */
/* */
/* <script src="catalog/view/theme/oxy/js/jquery.visible.min.js" type="text/javascript"></script>*/
/* <script type="text/javascript">*/
/* */
/* var win = $(window);*/
/* */
/* var allMods = $(".come-item");*/
/* */
/* allMods.each(function(i, el) {*/
/*     */
/*   if ($(el).visible(true)) {*/
/*     $(el).addClass("already-visible"); */
/*   }*/
/*   */
/* });*/
/* */
/* win.scroll(function(event) {*/
/*   */
/*   allMods.each(function(i, el) {*/
/*     */
/*     var el = $(el);*/
/*     */
/*     if (el.visible(true)) {*/
/*       el.addClass("come-in"); */
/*     } else {*/
/*       el.removeClass("come-in already-visible");*/
/*     }*/
/*     */
/*   });*/
/*   */
/* }); */
/* </script>*/
/* <script>*/
/* */
/*     // Product List*/
/* 	$('#list-view').click(function() {*/
/* 		$('#content .product-layout > .clearfix').remove();*/
/*         */
/* 		$('#content .category-product-items').attr('class', 'row product-items category-product-items product-list-wrapper');*/
/* 		$('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');*/
/* */
/* 		localStorage.setItem('display', 'list');*/
/* 	});*/
/* 	*/
/* 	// Product Small List*/
/* 	$('#small-list-view').click(function() {*/
/* 		$('#content .product-layout > .clearfix').remove();*/
/*         */
/* 		$('#content .category-product-items').attr('class', 'row product-items category-product-items product-list-wrapper');*/
/* 		$('#content .product-layout').attr('class', 'product-layout product-list product-small-list col-xs-12');*/
/* */
/* 		localStorage.setItem('display', 'small-list');*/
/* 	});*/
/* */
/* 	// Product Grid*/
/* 	$('#grid-view').click(function() {*/
/* 		$('#content .product-layout > .clearfix').remove();*/
/* */
/* 		// What a shame bootstrap does not take into account dynamically loaded columns*/
/* 		cols = $('#column-right, #column-left').length;*/
/* */
/* 			$('#content .category-product-items').attr('class', 'row product-items category-product-items product-grid-wrapper');*/
/* 			$('#content .product-layout').attr('class', 'product-layout product-grid {{ t1o_category_prod_box_style }} col-lg-{{ t1o_product_grid_per_row }} col-md-{{ t1o_product_grid_per_row }} col-sm-6 col-xs-6');*/
/* 		*/
/* 		 localStorage.setItem('display', 'grid');*/
/* 	});*/
/* 	*/
/* 	// Product Gallery*/
/* 	$('#gallery-view').click(function() {*/
/* 		$('#content .product-layout > .clearfix').remove();*/
/* */
/* 		// What a shame bootstrap does not take into account dynamically loaded columns*/
/* 		cols = $('#column-right, #column-left').length;*/
/* */
/* 			$('#content .category-product-items').attr('class', 'row product-items category-product-items product-gallery-wrapper');*/
/* 			$('#content .product-layout').attr('class', 'product-layout product-gallery col-lg-{{ t1o_product_grid_per_row }} col-md-{{ t1o_product_grid_per_row }} col-sm-6 col-xs-6');*/
/* */
/* 		 localStorage.setItem('display', 'gallery');*/
/* 	});*/
/* */
/* 	if (localStorage.getItem('display') == 'list') {*/
/* 		$('#list-view').trigger('click');*/
/* 	} else if (localStorage.getItem('display') == 'small-list') {*/
/* 		$('#small-list-view').trigger('click');*/
/* 	} else if (localStorage.getItem('display') == 'gallery') {*/
/* 		$('#gallery-view').trigger('click');*/
/* 	} else {*/
/* 		$('#grid-view').trigger('click');*/
/* 	}*/
/* 	*/
/* 	// Module Grid*/
/* 	cols2 = $('#column-right, #column-left').length;*/
/* */
/* 		$('#content .product-layout-grid').attr('class', 'product-layout product-grid {{ t1o_category_prod_box_style }} col-lg-{{ t1o_product_grid_per_row }} col-md-{{ t1o_product_grid_per_row }} col-sm-6 col-xs-6');*/
/* 		$('.product-layout-slider').attr('class', 'product-layout-slider  product-grid {{ t1o_category_prod_box_style }} col-xs-12');*/
/* */
/* </script>*/
/* <script src="catalog/view/theme/oxy/js/tickerme.js" type="text/javascript"></script>*/
/* <script type="text/javascript">*/
/* $(function(){*/
/* 	$('#ticker').tickerme();*/
/* });*/
/* </script>*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/jquery.easing-1.3.min.js"></script>*/
/* {% if t1o_header_fixed_header_status == 1 %}*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/jquery.sticky.js"></script>*/
/* {% endif %}*/
/* {% if t1o_header_auto_suggest_status == 1 %}*/
/* <link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/theme/oxy/stylesheet/livesearch.css" />*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/livesearch.js"></script>*/
/* {% endif %}*/
/* {% if t1o_others_totop == 1 %}	*/
/* <link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/theme/oxy/stylesheet/ui.totop.css" />*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/jquery.ui.totop.js"></script>*/
/* <script type="text/javascript">*/
/* 	jQuery(document).ready(function() {	*/
/* 		$().UItoTop({ easingType: 'easeOutQuart' });	*/
/* 	});*/
/* </script>*/
/* {% endif %}*/
/* <link rel="stylesheet" type="text/css" href="catalog/view/theme/oxy/stylesheet/animate.css" />*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/quickview/quickview.js"></script>		*/
/* <link rel="stylesheet" property="stylesheet" href="catalog/view/theme/oxy/js/quickview/fancybox/jquery.fancybox.css" />*/
/* <script src="catalog/view/theme/oxy/js/quickview/fancybox/jquery.fancybox.pack.js"></script>*/
/* <script src="catalog/view/theme/oxy/js/jquery.stellar.js" type="text/javascript"></script>*/
/* {% if t1o_eu_cookie_status == 1 %}	*/
/* <script src="catalog/view/theme/oxy/js/jquery.eucookiebar.js" type="text/javascript"></script>*/
/* <script type="text/javascript">*/
/*     $(document).ready(function() {*/
/*       $('.cookie-message').cookieBar({ closeButton : '.my-close-button' });*/
/*     });*/
/* </script>*/
/* <div class="cookie-message hidden-xs">*/
/*   <span class="cookie-img"><img src="catalog/view/theme/oxy/image/321_cookie.png" alt="Cookie" title="Cookie"></span>*/
/*   {{ t1o_eu_cookie_message }}*/
/*   <a class="my-close-button btn btn-primary" href="#">{{ t1o_eu_cookie_close[lang_id] }}</a>*/
/* </div>*/
/* {% endif %}*/
/* */
/* <link rel="stylesheet" property="stylesheet" type="text/css" href="catalog/view/javascript/jquery/magnific/magnific-popup.css" />	*/
/* <script type="text/javascript" src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>*/
/* <script type="text/javascript" src="catalog/view/theme/oxy/js/flexmenu.min.js"></script>*/
/* <script>*/
/* 	$('ul.menu.flex').flexMenu();*/
/* 	$('ul.menu.flex-multi').flexMenu({*/
/* 		showOnHover: false*/
/* 	})*/
/* </script>*/
/* {% if t1o_custom_js !='' %}*/
/* {{ t1o_custom_js }}*/
/* {% endif %}*/
/* */
/* </div>*/
/* {% if t1o_custom_bottom_2_status == 1 and t1o_layout_style == 'full-width' %}*/
/* <div id="footer_custom_2" class="hidden-xs">*/
/* <div class="container">*/
/* <div class="row">*/
/* <div class="col-sm-12 padd-t-b-30">*/
/* {{ t1o_custom_bottom_2_content }}*/
/* </div>*/
/* </div>*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* </body></html>*/
