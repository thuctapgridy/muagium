<?php

/* extension/module/oxy_theme_options.twig */
class __TwigTemplate_689dd537ba2a0f1978866586d2a84c3a0e7c7d8823066643e68f00ca3b14568d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
 
";
        // line 3
        if (twig_test_empty((isset($context["t1o_text_logo_color"]) ? $context["t1o_text_logo_color"] : null))) {
            $context["t1o_text_logo_color"] = "424242";
        }
        // line 4
        if (twig_test_empty((isset($context["t1o_text_logo_awesome_color"]) ? $context["t1o_text_logo_awesome_color"] : null))) {
            $context["t1o_text_logo_awesome_color"] = "F1494B";
        }
        // line 5
        if (twig_test_empty((isset($context["t1o_top_custom_block_title_bar_bg_color"]) ? $context["t1o_top_custom_block_title_bar_bg_color"] : null))) {
            $context["t1o_top_custom_block_title_bar_bg_color"] = "FFFFFF";
        }
        // line 6
        if (twig_test_empty((isset($context["t1o_top_custom_block_awesome_color"]) ? $context["t1o_top_custom_block_awesome_color"] : null))) {
            $context["t1o_top_custom_block_awesome_color"] = "F1494B";
        }
        // line 7
        if (twig_test_empty((isset($context["t1o_top_custom_block_title_color"]) ? $context["t1o_top_custom_block_title_color"] : null))) {
            $context["t1o_top_custom_block_title_color"] = "424242";
        }
        // line 8
        if (twig_test_empty((isset($context["t1o_top_custom_block_bg_color"]) ? $context["t1o_top_custom_block_bg_color"] : null))) {
            $context["t1o_top_custom_block_bg_color"] = "424242";
        }
        // line 9
        if (twig_test_empty((isset($context["t1o_top_custom_block_text_color"]) ? $context["t1o_top_custom_block_text_color"] : null))) {
            $context["t1o_top_custom_block_text_color"] = "FFFFFF";
        }
        // line 10
        if (twig_test_empty((isset($context["t1o_news_bg_color"]) ? $context["t1o_news_bg_color"] : null))) {
            $context["t1o_news_bg_color"] = "373737";
        }
        // line 11
        if (twig_test_empty((isset($context["t1o_news_icons_color"]) ? $context["t1o_news_icons_color"] : null))) {
            $context["t1o_news_icons_color"] = "FFFFFF";
        }
        // line 12
        if (twig_test_empty((isset($context["t1o_news_word_color"]) ? $context["t1o_news_word_color"] : null))) {
            $context["t1o_news_word_color"] = "FFFFFF";
        }
        // line 13
        if (twig_test_empty((isset($context["t1o_news_color"]) ? $context["t1o_news_color"] : null))) {
            $context["t1o_news_color"] = "B6B6B6";
        }
        // line 14
        if (twig_test_empty((isset($context["t1o_news_hover_color"]) ? $context["t1o_news_hover_color"] : null))) {
            $context["t1o_news_hover_color"] = "FFFFFF";
        }
        // line 15
        if (twig_test_empty((isset($context["t1o_custom_bar_below_menu_bg_color"]) ? $context["t1o_custom_bar_below_menu_bg_color"] : null))) {
            $context["t1o_custom_bar_below_menu_bg_color"] = "373737";
        }
        // line 16
        if (twig_test_empty((isset($context["t1o_custom_bar_below_menu_text_color"]) ? $context["t1o_custom_bar_below_menu_text_color"] : null))) {
            $context["t1o_custom_bar_below_menu_text_color"] = "FFFFFF";
        }
        // line 17
        if (twig_test_empty((isset($context["t1o_category_title_above_color"]) ? $context["t1o_category_title_above_color"] : null))) {
            $context["t1o_category_title_above_color"] = "424242";
        }
        // line 18
        if (twig_test_empty((isset($context["t1o_snapchat_box_bg"]) ? $context["t1o_snapchat_box_bg"] : null))) {
            $context["t1o_snapchat_box_bg"] = "000000";
        }
        // line 19
        if (twig_test_empty((isset($context["t1o_video_box_bg"]) ? $context["t1o_video_box_bg"] : null))) {
            $context["t1o_video_box_bg"] = "E22C29";
        }
        // line 20
        if (twig_test_empty((isset($context["t1o_custom_box_bg"]) ? $context["t1o_custom_box_bg"] : null))) {
            $context["t1o_custom_box_bg"] = "424242";
        }
        // line 21
        echo "
<style type=\"text/css\">
.color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}
.k_help {color:#999;background-color:#F5F5F5;font-size:10px;font-weight:normal;text-transform:uppercase;padding:15px;width:auto;display:block;margin-top:10px;margin-bottom:10px;border-radius:3px;}
span.k_help_tip {margin-left:10px;padding:4px 9px 3px;width:24px;display:inline;border-radius:2px;background-color:#1E91CF;color:#FFF;font-weight:bold;transition: all 0.15s ease-in 0s;opacity: 0.9; display: none;}
span.k_help_tip_support {display:block;}
span.k_help_tip:hover {opacity: 1;}
span.k_help_tip a {color:#FFF;font-size:12px;font-weight:bold;text-decoration:none;}
span.k_tooltip {cursor:pointer;}
.k_sep {background-color:#F7F7F7;}
.ptn {position:relative;width:40px;height:40px;float:left;margin-right:5px;margin-bottom:5px;}
.ptn_nr {position:absolute;bottom:0px;right:3px;}
.prod_l {position:relative;width:134px;height:134px;float:left;margin-right:25px;margin-bottom:30px;}
.prod_l_nr {position:absolute;bottom:-17px;right:0px;}
.header_s {position:relative;width:300px;height:auto;float:left;margin-right:25px;margin-bottom:45px;}
.header_s_nr {position:absolute;bottom:-22px;right:0px;}
.header_s:nth-child(2n+1) {clear: both;}
table.form {margin-bottom:0;}
table.form div {text-align:left}
table.form b {color:#003A88;font-size:13px}
table.form > tbody > tr > td:first-child {text-align:right}
a.btn-default.link {text-decoration:none;margin-left:5px;margin-right:5px;color:#222222;background-color:#F0F0F0;border-color:#F0F0F0;transition: all 0.15s ease-in 0s;position:relative;}
a.btn-default.link:hover {background-color:#DEDEDE;border-color:#DEDEDE;}
a.btn-default.link:last-child {padding-right:45px;}
a.btn-default.link i {font-size:14px;margin-right:5px;}
a.btn-default.link span.k_help_tip {position:absolute;top:5px;right:5px;}
.htabs {margin-top:15px;}
a.button-oxy-theme {background:#4BB8E2;color:#FFFFFF;padding:4px 12px;margin-left:5px;text-decoration:none;border-radius:3px;cursor:pointer;}
a.button-oxy-theme:hover {background:#ED5053;}
table.form {
\twidth: 100%;
\tborder-collapse: collapse;
\tmargin-bottom: 20px;
}
table.form > tbody > tr > td:first-child {
\twidth: 200px;
}
table.form > tbody > tr > td {
\tpadding: 10px;
\tcolor: #000000;
\tborder-bottom: 1px dotted #CCCCCC;
}
label.control-label span:after {
\tdisplay: none;
}
legend {
\tbackground-color: #EEEEEE;
\tborder: none;
\tborder-radius: 3px;
}
fieldset legend {
\tmargin-top: 20px;
\tpadding: 20px 30px;
}
legend.bn {
\tborder-color: #FFFFFF;
\tpadding: 0;
\tmargin: 0;
}
legend span {
\tfont-size: 12px;
}
.nav-tabs {
\tborder-bottom: 3px solid #1E91CF;
}
.nav-tabs > li {
\tmargin-bottom: 0;
}
.nav-tabs > li > a:hover {
\tbackground-color: #E9E9E9;
\tborder-color: #E9E9E9;
}
.nav-tabs > li > a {
\tpadding: 15px 20px;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
\tbackground-color: #1E91CF;
\tborder-color: #1E91CF;
\tcolor: #FFFFFF;
\tfont-weight: normal;
}
.nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
\tbackground-color: #1E91CF;
}
.nav > li > a {
\tbackground-color: #EEEEEE;
\tpadding: 15px 20px;
}
.nav > li > a:hover {
\tbackground-color: #E9E9E9;
\tcolor: inherit;
}
html, body {color:#222222;}
a {color:#666666;transition: all 0.15s ease-in 0s;}
a:hover, a:focus {color:#1E91CF;}
label {font-weight:normal;}
.form-control {display:inline-block;min-width:77px;}
.form-horizontal .form-group {
\tmargin-left: 0;
\tmargin-right: 0;
}
.form-group + .form-group {border-top: 1px dotted #EDEDED;}
.tab-content .form-horizontal label.col-sm-2{
\tpadding-left: 0;
}
textarea.form-control {width:100%;}
.panel-heading i {font-size: 14px;}
.table thead > tr > td, .table tbody > tr > td {vertical-align:top;}
</style>    

";
        // line 131
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "

<div id=\"content\">

  <div class=\"page-header\">
    <div class=\"container-fluid\">
      <div class=\"pull-right\">
        <button type=\"submit\" form=\"form-oxy-theme\" data-toggle=\"tooltip\" title=\"";
        // line 138
        echo (isset($context["button_save"]) ? $context["button_save"] : null);
        echo "\" class=\"btn btn-primary\"><i class=\"fa fa-save\"></i></button>
        <a href=\"";
        // line 139
        echo (isset($context["cancel"]) ? $context["cancel"] : null);
        echo "\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["button_cancel"]) ? $context["button_cancel"] : null);
        echo "\" class=\"btn btn-default\"><i class=\"fa fa-reply\"></i></a></div>
      <h1>";
        // line 140
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h1>
      <ul class=\"breadcrumb\">
        ";
        // line 142
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 143
            echo "        <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 145
        echo "      </ul>
    </div>
  </div>
  <div class=\"container-fluid\">
  ";
        // line 149
        if ((isset($context["error_warning"]) ? $context["error_warning"] : null)) {
            // line 150
            echo "  <div class=\"alert alert-danger\"><i class=\"fa fa-exclamation-circle\"></i> ";
            echo (isset($context["error_warning"]) ? $context["error_warning"] : null);
            echo "
    <button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>
  </div>
  ";
        }
        // line 154
        echo "  <div class=\"panel panel-default\">
    <div class=\"panel-heading\">
\t\t<h3 class=\"panel-title\"><i class=\"fa fa-pencil\"></i> ";
        // line 156
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h3>
\t</div>
    <div class=\"panel-body\">
    
    

    <form action=\"";
        // line 162
        echo (isset($context["action"]) ? $context["action"] : null);
        echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-oxy-theme\">
    
    <input type=\"hidden\" name=\"store_id\" value=\"";
        // line 164
        echo (isset($context["store_id"]) ? $context["store_id"] : null);
        echo "\" />

        <div style=\"margin-top:10px; margin-bottom:25px;\">
            <span style=\"margin-left:0px;\">Useful links:</span> 
            <a href=\"http://oxy.321cart.com/documentation/\" class=\"btn btn-default link\" target=\"_blank\"><i class=\"fa fa-book\"></i> OXY Documentation</a>
            <a href=\"http://oxy.321cart.com/landing/\" class=\"btn btn-default link\" target=\"_blank\"><i class=\"fa fa-television\"></i> OXY Demos</a>
            <a href=\"http://support.321cart.com/system/\" class=\"btn btn-default link\" target=\"_blank\"><i class=\"fa fa-support\"></i> OXY Support <span class=\"k_help_tip_support k_help_tip k_tooltip\" title=\"<br>If you need help, please contact us. We provide support only through our Support System.<br><br>Create a ticket and our support developer will respond as soon as possible.<br><br>Support requests are being processed on Monday to Friday.<br><br>\" data-toggle=\"tooltip\">?</span></a>
            

\t\t</div>

    
        <ul class=\"nav nav-tabs\">
          <li class=\"active\"><a href=\"#tab-options\" data-toggle=\"tab\">General Options</a></li>
          <li><a href=\"#tab-header\" data-toggle=\"tab\">Header</a></li>
          <li><a href=\"#tab-menu\" data-toggle=\"tab\">Main Menu</a></li>
          <li><a href=\"#tab-midsection\" data-toggle=\"tab\">Midsection</a></li>
          <li><a href=\"#tab-footer\" data-toggle=\"tab\">Footer</a></li>
          <li><a href=\"#tab-widgets\" data-toggle=\"tab\">Widgets</a></li>
          <li><a href=\"#tab-css\" data-toggle=\"tab\">Custom CSS/JavaScript</a></li>
          <li><a href=\"#tab-translate\" data-toggle=\"tab\">Theme Translate</a></li>
        </ul>
        
        <div class=\"tab-content\">
        <!-- -->
        
        <div class=\"tab-pane active\" id=\"tab-options\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"store_features_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-options-layout\" data-toggle=\"tab\">Layout</a></li>
             <li><a href=\"#tab-options-sliders\" data-toggle=\"tab\">Products Layout</a></li>
             <li><a href=\"#tab-options-others\" data-toggle=\"tab\">Others</a></li>                                       
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-options-layout\" class=\"tab-pane fade in active\"> 
        
                    <fieldset>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Layout style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_style\" class=\"form-control\">
                                    <option value=\"boxed\"";
        // line 212
        if (((isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null) == "boxed")) {
            echo "selected=\"selected\"";
        }
        echo ">Boxed</option>
\t\t\t\t\t\t\t\t\t<option value=\"framed\"";
        // line 213
        if (((isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null) == "framed")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Framed</option>
                                    <option value=\"full-width\"";
        // line 214
        if (((isset($context["t1o_layout_style"]) ? $context["t1o_layout_style"] : null) == "full-width")) {
            echo "selected=\"selected\"";
        }
        echo ">Full Width</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_01.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Maximum width:<br /><span class=\"k_help\">for \"Framed\" layout style</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_l\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 225
        if (((isset($context["t1o_layout_l"]) ? $context["t1o_layout_l"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1170px</option>
                                    <option value=\"2\"";
        // line 226
        if (((isset($context["t1o_layout_l"]) ? $context["t1o_layout_l"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">980px</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Align layout to:<br /><span class=\"k_help\">for \"Framed\" layout style</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_framed_align\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 236
        if (((isset($context["t1o_layout_framed_align"]) ? $context["t1o_layout_framed_align"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 237
        if (((isset($context["t1o_layout_framed_align"]) ? $context["t1o_layout_framed_align"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Product Blocks maximum width:
                            <span class=\"k_help\">for \"Full Width\"<br />layout styles</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_full_width_max\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 248
        if (((isset($context["t1o_layout_full_width_max"]) ? $context["t1o_layout_full_width_max"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Full Width</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 249
        if (((isset($context["t1o_layout_full_width_max"]) ? $context["t1o_layout_full_width_max"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_layout_full_width_max"]) ? $context["t1o_layout_full_width_max"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">1440px</option>
                                    <option value=\"2\"";
        // line 250
        if (((isset($context["t1o_layout_full_width_max"]) ? $context["t1o_layout_full_width_max"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">1170px</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_01.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a> 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Align Headings to:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_h_align\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 260
        if (((isset($context["t1o_layout_h_align"]) ? $context["t1o_layout_h_align"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_c"]) ? $context["text_position_c"] : null);
        echo "</option>
                                    <option value=\"1\"";
        // line 261
        if (((isset($context["t1o_layout_h_align"]) ? $context["t1o_layout_h_align"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_layout_h_align"]) ? $context["t1o_layout_h_align"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_position_l"]) ? $context["text_position_l"] : null);
        echo "</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Catalog Mode:<br /><span class=\"k_help\">excludes purchase options</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_catalog_mode\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 271
        if (((isset($context["t1o_layout_catalog_mode"]) ? $context["t1o_layout_catalog_mode"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 272
        if (((isset($context["t1o_layout_catalog_mode"]) ? $context["t1o_layout_catalog_mode"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option> 
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-options-sliders\" class=\"tab-pane\">  
        
                    <fieldset>
                    
                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Bestseller view:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_bestseller_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 292
        if (((isset($context["t1o_bestseller_style"]) ? $context["t1o_bestseller_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 293
        if (((isset($context["t1o_bestseller_style"]) ? $context["t1o_bestseller_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_bestseller_style"]) ? $context["t1o_bestseller_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Featured view:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_featured_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 301
        if (((isset($context["t1o_featured_style"]) ? $context["t1o_featured_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 302
        if (((isset($context["t1o_featured_style"]) ? $context["t1o_featured_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_featured_style"]) ? $context["t1o_featured_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Latest view:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_latest_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 310
        if (((isset($context["t1o_latest_style"]) ? $context["t1o_latest_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 311
        if (((isset($context["t1o_latest_style"]) ? $context["t1o_latest_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_latest_style"]) ? $context["t1o_latest_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Specials view:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_specials_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 319
        if (((isset($context["t1o_specials_style"]) ? $context["t1o_specials_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 320
        if (((isset($context["t1o_specials_style"]) ? $context["t1o_specials_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_specials_style"]) ? $context["t1o_specials_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">OXY Theme Most Viewed view:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_most_viewed_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 328
        if (((isset($context["t1o_most_viewed_style"]) ? $context["t1o_most_viewed_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 329
        if (((isset($context["t1o_most_viewed_style"]) ? $context["t1o_most_viewed_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_most_viewed_style"]) ? $context["t1o_most_viewed_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">OXY Theme Product Tabs view:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_tabs_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 337
        if (((isset($context["t1o_product_tabs_style"]) ? $context["t1o_product_tabs_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 338
        if (((isset($context["t1o_product_tabs_style"]) ? $context["t1o_product_tabs_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_tabs_style"]) ? $context["t1o_product_tabs_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<legend>Grid View <a href=\"view/image/theme_img/help_oxy_theme/go_02.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

\t\t\t\t\t\t
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Products per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_grid_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"12\"";
        // line 350
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"6\"";
        // line 351
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"4\"";
        // line 352
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"3\"";
        // line 353
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"15\"";
        // line 354
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "15")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"2\"";
        // line 355
        if (((isset($context["t1o_product_grid_per_row"]) ? $context["t1o_product_grid_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Slider View <a href=\"view/image/theme_img/help_oxy_theme/go_02.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Bestsellers per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_bestseller_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 366
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 367
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 368
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 369
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 370
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 371
        if (((isset($context["t1o_bestseller_per_row"]) ? $context["t1o_bestseller_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Featured per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_featured_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 379
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 380
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 381
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 382
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 383
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 384
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Latest per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_latest_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 392
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 393
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 394
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 395
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 396
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 397
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_latest_per_row"]) ? $context["t1o_latest_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Specials per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_specials_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 405
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 406
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 407
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 408
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 409
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 410
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_specials_per_row"]) ? $context["t1o_specials_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">OXY Theme Most Viewed per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_most_viewed_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 418
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 419
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 420
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 421
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 422
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 423
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_most_viewed_per_row"]) ? $context["t1o_most_viewed_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">OXY Theme Products Tabs - Products per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_tabs_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 431
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 432
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 433
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 434
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 435
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 436
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_tabs_per_row"]) ? $context["t1o_product_tabs_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Related Products per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_related_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 444
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 445
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 446
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 447
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 448
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 449
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_related_per_row"]) ? $context["t1o_related_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Carousel Items per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_carousel_items_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 457
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 458
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 459
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 460
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 461
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 462
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_carousel_items_per_row"]) ? $context["t1o_carousel_items_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-options-others\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show \"Scroll To Top\" button:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_others_totop\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 479
        if (((isset($context["t1o_others_totop"]) ? $context["t1o_others_totop"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 480
        if (((isset($context["t1o_others_totop"]) ? $context["t1o_others_totop"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_others_totop"]) ? $context["t1o_others_totop"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>    
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_03.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        <div class=\"tab-pane\" id=\"tab-header\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"store_features_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-header-general\" data-toggle=\"tab\">General</a></li>
             <li><a href=\"#tab-header-logo-creator\" data-toggle=\"tab\">Logo Creator</a></li>
             <li><a href=\"#tab-header-fixed-header\" data-toggle=\"tab\">Fixed Header</a></li>
             <li><a href=\"#tab-header-top-bar\" data-toggle=\"tab\">Top Bar</a></li>
             <li><a href=\"#tab-header-top-promo-bar\" data-toggle=\"tab\">Promo Message Bar</a></li>
             <li><a href=\"#tab-header-news\" data-toggle=\"tab\">News</a></li>
             <li><a href=\"#tab-header-custom-block\" data-toggle=\"tab\">Header Custom Block</a></li>
             <li><a href=\"#tab-header-search\" data-toggle=\"tab\">Search</a></li>                         
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-header-general\" class=\"tab-pane fade in active\">
        
                    <fieldset>
                    
                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Header Style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_header_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"header-style-1\"";
        // line 529
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-1")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 1</option>
                                    <option value=\"header-style-2\"";
        // line 530
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-2")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 2</option>
                                    <option value=\"header-style-3\"";
        // line 531
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-3")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 3</option>
                                    <option value=\"header-style-4\"";
        // line 532
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-4")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 4</option>
                                    <option value=\"header-style-5\"";
        // line 533
        if (((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-5")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 5</option>               
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
                            <div class=\"col-sm-12\">
                                ";
        // line 540
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["hs"]) {
            // line 541
            echo "                                <div class=\"header_s\"><img src=\"view/image/theme_img/hs_";
            echo $context["hs"];
            echo ".png\"><span class=\"t1o_help header_s_nr\">Style ";
            echo $context["hs"];
            echo "</span></div><br />
\t\t\t\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hs'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 543
        echo "\t\t\t\t\t\t    </div>   
                        </div>
                        
                    </fieldset>
                    
        </div>
        
        <div id=\"tab-header-logo-creator\" class=\"tab-pane\">
        
                    <fieldset>
                    
                        <span class=\"k_help\">Here you can create your own simple logo. If you have a logo in a graphical format, you can add it here: <b>System > Settings > Image > Store Logo</b></span>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Logo Text:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 559
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 560
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 561
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_logo[";
            // line 562
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_logo_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_logo"]) ? $context["t1o_text_logo"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_logo"]) ? $context["t1o_text_logo"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 565
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Logo Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_text_logo_color\" id=\"t1o_text_logo_color\" value=\"";
        // line 571
        echo (isset($context["t1o_text_logo_color"]) ? $context["t1o_text_logo_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Font Awesome Icon:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_text_logo_awesome_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 579
        if (((isset($context["t1o_text_logo_awesome_status"]) ? $context["t1o_text_logo_awesome_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 580
        if (((isset($context["t1o_text_logo_awesome_status"]) ? $context["t1o_text_logo_awesome_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_text_logo_awesome\" value=\"";
        // line 588
        echo (isset($context["t1o_text_logo_awesome"]) ? $context["t1o_text_logo_awesome"] : null);
        echo "\" class=\"form-control\" />
                                <span class=\"k_help\">Enter the name of an icon, for example: <b>shopping-bag</b></span>
                                <a href=\"http://fortawesome.github.io/Font-Awesome/icons/\" target=\"_blank\" class=\"link\" style=\"margin-left:5px\">Font Awesome Icon Collection &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_text_logo_awesome_color\" id=\"t1o_text_logo_awesome_color\" value=\"";
        // line 597
        echo (isset($context["t1o_text_logo_awesome_color"]) ? $context["t1o_text_logo_awesome_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>
                    
        </div>

        <div id=\"tab-header-fixed-header\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Fixed Header:<br /><span class=\"k_help\">Not available for Header Style 3</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_header_fixed_header_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 614
        if (((isset($context["t1o_header_fixed_header_status"]) ? $context["t1o_header_fixed_header_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 615
        if (((isset($context["t1o_header_fixed_header_status"]) ? $context["t1o_header_fixed_header_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_05.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-header-top-bar\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Top Bar <a href=\"view/image/theme_img/help_oxy_theme/go_04.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Top Bar:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_top_bar_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 635
        if (((isset($context["t1o_top_bar_status"]) ? $context["t1o_top_bar_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 636
        if (((isset($context["t1o_top_bar_status"]) ? $context["t1o_top_bar_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_top_bar_status"]) ? $context["t1o_top_bar_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show \"Shopping Cart\" link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_top_bar_cart_link_status\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 644
        if (((isset($context["t1o_top_bar_cart_link_status"]) ? $context["t1o_top_bar_cart_link_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 645
        if (((isset($context["t1o_top_bar_cart_link_status"]) ? $context["t1o_top_bar_cart_link_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>  
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Welcome Message:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_top_bar_welcome_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 653
        if (((isset($context["t1o_top_bar_welcome_status"]) ? $context["t1o_top_bar_welcome_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 654
        if (((isset($context["t1o_top_bar_welcome_status"]) ? $context["t1o_top_bar_welcome_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon for Welcome Message:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_bar_welcome_awesome\" value=\"";
        // line 661
        echo (isset($context["t1o_top_bar_welcome_awesome"]) ? $context["t1o_top_bar_welcome_awesome"] : null);
        echo "\" class=\"form-control\" />
                                <span class=\"k_help\">Enter the name of an icon, for example: <b>flag</b></span>
                                <a href=\"http://fortawesome.github.io/Font-Awesome/icons/\" target=\"_blank\" class=\"link\" style=\"margin-left:5px\">Font Awesome Icon Collection &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Welcome Message:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 669
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 670
            echo "\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 671
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                        <input type=\"text\" name=\"t1o_top_bar_welcome[";
            // line 672
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_top_bar_welcome_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_top_bar_welcome"]) ? $context["t1o_top_bar_welcome"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_top_bar_welcome"]) ? $context["t1o_top_bar_welcome"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Message\" class=\"form-control\" />  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 675
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-header-top-promo-bar\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Promo Message Bar <a href=\"view/image/theme_img/help_oxy_theme/go_07.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Promo Message Bar:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_top_custom_block_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 692
        if (((isset($context["t1o_top_custom_block_status"]) ? $context["t1o_top_custom_block_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 693
        if (((isset($context["t1o_top_custom_block_status"]) ? $context["t1o_top_custom_block_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Promo Message Bar background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_custom_block_title_bar_bg_color\" id=\"t1o_top_custom_block_title_bar_bg_color\" value=\"";
        // line 701
        echo (isset($context["t1o_top_custom_block_title_bar_bg_color"]) ? $context["t1o_top_custom_block_title_bar_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Promo Message Bar background image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1o_top_custom_block_bar_bg_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 708
        echo (isset($context["t1o_top_custom_block_bar_bg_thumb"]) ? $context["t1o_top_custom_block_bar_bg_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_top_custom_block_bar_bg\" value=\"";
        // line 709
        echo (isset($context["t1o_top_custom_block_bar_bg"]) ? $context["t1o_top_custom_block_bar_bg"] : null);
        echo "\" id=\"t1o_top_custom_block_bar_bg\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Font Awesome Icon:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_top_custom_block_awesome_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 717
        if (((isset($context["t1o_top_custom_block_awesome_status"]) ? $context["t1o_top_custom_block_awesome_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 718
        if (((isset($context["t1o_top_custom_block_awesome_status"]) ? $context["t1o_top_custom_block_awesome_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_custom_block_awesome\" value=\"";
        // line 726
        echo (isset($context["t1o_top_custom_block_awesome"]) ? $context["t1o_top_custom_block_awesome"] : null);
        echo "\" class=\"form-control\" />
                                <span class=\"k_help\">Enter the name of an icon, for example: <b>tags</b></span>
                                <a href=\"http://fortawesome.github.io/Font-Awesome/icons/\" target=\"_blank\" class=\"link\" style=\"margin-left:5px\">Font Awesome Icon Collection &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_custom_block_awesome_color\" id=\"t1o_top_custom_block_awesome_color\" value=\"";
        // line 735
        echo (isset($context["t1o_top_custom_block_awesome_color"]) ? $context["t1o_top_custom_block_awesome_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 742
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 743
            echo "\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 744
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                        <input type=\"text\" name=\"t1o_top_custom_block_title[";
            // line 745
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_top_custom_block_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_top_custom_block_title"]) ? $context["t1o_top_custom_block_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_top_custom_block_title"]) ? $context["t1o_top_custom_block_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 748
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_custom_block_title_color\" id=\"t1o_top_custom_block_title_color\" value=\"";
        // line 754
        echo (isset($context["t1o_top_custom_block_title_color"]) ? $context["t1o_top_custom_block_title_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_top_custom_block_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 763
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_top_custom_block_content_";
            // line 764
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 766
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 769
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_top_custom_block_content_";
            // line 770
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_top_custom_block_content[";
            // line 771
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_top_custom_block_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_top_custom_block_content"]) ? $context["t1o_top_custom_block_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_top_custom_block_content"]) ? $context["t1o_top_custom_block_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 774
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Background Image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1o_top_custom_block_bg_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 781
        echo (isset($context["t1o_top_custom_block_bg_thumb"]) ? $context["t1o_top_custom_block_bg_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_top_custom_block_bg\" value=\"";
        // line 782
        echo (isset($context["t1o_top_custom_block_bg"]) ? $context["t1o_top_custom_block_bg"] : null);
        echo "\" id=\"t1o_top_custom_block_bg\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_custom_block_bg_color\" id=\"t1o_top_custom_block_bg_color\" value=\"";
        // line 788
        echo (isset($context["t1o_top_custom_block_bg_color"]) ? $context["t1o_top_custom_block_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_top_custom_block_text_color\" id=\"t1o_top_custom_block_text_color\" value=\"";
        // line 794
        echo (isset($context["t1o_top_custom_block_text_color"]) ? $context["t1o_top_custom_block_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-header-news\" class=\"tab-pane\">
        
                    <fieldset>

\t\t\t\t\t\t<legend>News <a href=\"view/image/theme_img/help_oxy_theme/go_43.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show News Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_news_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 812
        if (((isset($context["t1o_news_status"]) ? $context["t1o_news_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 813
        if (((isset($context["t1o_news_status"]) ? $context["t1o_news_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
                                    <th class=\"left\" width=\"10%\">News</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"10%\">Show</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"40%\">Title</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"40%\">URL</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 830
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 831
            echo "                                    <tr>
\t\t\t\t\t\t\t\t\t<td>News ";
            // line 832
            echo $context["i"];
            echo ":</td>
\t\t\t\t\t\t\t\t\t<td>
                                    <select name=\"t1o_news[";
            // line 834
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 835
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                        <option value=\"1\"";
            // line 836
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    \t</select>
                                    </td>
                                    <td>
                                    ";
            // line 840
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 841
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 842
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_news[";
                // line 843
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_news_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" /> 
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 846
            echo "                                    </td>
                                    <td>
                                    <input type=\"text\" name=\"t1o_news[";
            // line 848
            echo $context["i"];
            echo "][url]\" value=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["t1o_news"]) ? $context["t1o_news"] : null), $context["i"], array(), "array"), "url", array());
            echo "\" class=\"form-control\" />
                                    </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 852
        echo "                                </tbody>
\t\t\t\t\t\t\t</table>
                        
                        </div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_news_bg_color\" id=\"t1o_news_bg_color\" value=\"";
        // line 860
        echo (isset($context["t1o_news_bg_color"]) ? $context["t1o_news_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Icons color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_news_icons_color\" id=\"t1o_news_icons_color\" value=\"";
        // line 866
        echo (isset($context["t1o_news_icons_color"]) ? $context["t1o_news_icons_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">\"News\" word color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_news_word_color\" id=\"t1o_news_word_color\" value=\"";
        // line 872
        echo (isset($context["t1o_news_word_color"]) ? $context["t1o_news_word_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">News color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_news_color\" id=\"t1o_news_color\" value=\"";
        // line 878
        echo (isset($context["t1o_news_color"]) ? $context["t1o_news_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">News color hover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_news_hover_color\" id=\"t1o_news_hover_color\" value=\"";
        // line 884
        echo (isset($context["t1o_news_hover_color"]) ? $context["t1o_news_hover_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-header-custom-block\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Header Custom Block (for Header Style 2) <a href=\"view/image/theme_img/help_oxy_theme/go_07.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Header Custom Block: </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_header_custom_block_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 902
        if (((isset($context["t1o_header_custom_block_1_status"]) ? $context["t1o_header_custom_block_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 903
        if (((isset($context["t1o_header_custom_block_1_status"]) ? $context["t1o_header_custom_block_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_header_custom_block_1_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 912
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_header_custom_block_1_content_";
            // line 913
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 915
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 918
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_header_custom_block_1_content_";
            // line 919
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_header_custom_block_1_content[";
            // line 920
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_header_custom_block_1_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_header_custom_block_1_content"]) ? $context["t1o_header_custom_block_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_header_custom_block_1_content"]) ? $context["t1o_header_custom_block_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 923
        echo "                                    
\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-header-search\" class=\"tab-pane\">
        
                    <fieldset>
                    
                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Quick Search Auto-Suggest:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_header_auto_suggest_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 943
        if (((isset($context["t1o_header_auto_suggest_status"]) ? $context["t1o_header_auto_suggest_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 944
        if (((isset($context["t1o_header_auto_suggest_status"]) ? $context["t1o_header_auto_suggest_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_header_auto_suggest_status"]) ? $context["t1o_header_auto_suggest_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>    
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_06.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
   
                        <legend>Popular Search <a href=\"view/image/theme_img/help_oxy_theme/go_07.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Popular Search: </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_header_popular_search_status\" class=\"form-control\"> 
                                    <option value=\"0\"";
        // line 956
        if (((isset($context["t1o_header_popular_search_status"]) ? $context["t1o_header_popular_search_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 957
        if (((isset($context["t1o_header_popular_search_status"]) ? $context["t1o_header_popular_search_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        ";
        // line 962
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 20));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 963
            echo "                        <div class=\"form-group\">
\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Popular Search ";
            // line 964
            echo $context["i"];
            echo ":</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
            // line 966
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo "\t
\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 968
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_header_popular_search[";
                // line 969
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][word]\" id=\"t1o_header_popular_search_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_word\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_header_popular_search"]) ? $context["t1o_header_popular_search"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "word", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_header_popular_search"]) ? $context["t1o_header_popular_search"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "word", array())) : (""));
                echo "\" placeholder=\"eg. Jeans\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 972
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 975
        echo "
\t\t\t\t\t</fieldset>   
        </div>
     
        </div>
        </div>
        
        </div>
        </div>

        
        
        

        <div class=\"tab-pane\" id=\"tab-menu\"> 
        <div class=\"row form-horizontal\"> 

        <div class=\"col-sm-2\">    
        <ul id=\"store_features_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-menu-general\" data-toggle=\"tab\">General</a></li>
             <li><a href=\"#tab-menu-home-link\" data-toggle=\"tab\">Home Link</a></li>
             <li><a href=\"#tab-menu-categories\" data-toggle=\"tab\">Categories</a></li>
             <li><a href=\"#tab-menu-brands\" data-toggle=\"tab\">Brands</a></li>
             <li><a href=\"#tab-menu-custom-blocks\" data-toggle=\"tab\">Custom Blocks</a></li>
             <li><a href=\"#tab-menu-custom-dropdown-menu\" data-toggle=\"tab\">Custom Dropdown Menus</a></li>
             <li><a href=\"#tab-menu-custom-links\" data-toggle=\"tab\">Custom Links</a></li>
             <li><a href=\"#tab-menu-labels\" data-toggle=\"tab\">Menu Labels</a></li>
             <li><a href=\"#tab-menu-custom-bar\" data-toggle=\"tab\">Custom Bar</a></li>                        
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-menu-general\" class=\"tab-pane fade in active\">
        
                    <fieldset>
                        
                        <legend class=\"bn\"></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Align Menu Items to:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_align\" class=\"form-control\">
                                    <option value=\"left\"";
        // line 1019
        if (((isset($context["t1o_menu_align"]) ? $context["t1o_menu_align"] : null) == "left")) {
            echo "selected=\"selected\"";
        }
        echo ">Left</option>
                                    <option value=\"center\"";
        // line 1020
        if (((isset($context["t1o_menu_align"]) ? $context["t1o_menu_align"] : null) == "center")) {
            echo "selected=\"selected\"";
        }
        echo ">Center</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-menu-home-link\" class=\"tab-pane\">
        
                    <fieldset>                        
                    
                        <legend>Home Page Link</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Home Page Link style:
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_homepage\" class=\"form-control\">
                                    <option value=\"dontshow\"";
        // line 1040
        if (((isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null) == "dontshow")) {
            echo "selected=\"selected\"";
        }
        echo ">Don't show</option>
                                    <option value=\"text\"";
        // line 1041
        if (((isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null) == "text")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Text</option>
\t\t\t\t\t\t\t\t\t<option value=\"icon\"";
        // line 1042
        if (((isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null) == "icon")) {
            echo "selected=\"selected\"";
        }
        echo ">Icon</option> 
                                    <option value=\"icontext\"";
        // line 1043
        if (((isset($context["t1o_menu_homepage"]) ? $context["t1o_menu_homepage"] : null) == "icontext")) {
            echo "selected=\"selected\"";
        }
        echo ">Icon + Text</option>          
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-menu-categories\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Categories</legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Categories:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1062
        if (((isset($context["t1o_menu_categories_status"]) ? $context["t1o_menu_categories_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1063
        if (((isset($context["t1o_menu_categories_status"]) ? $context["t1o_menu_categories_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_categories_status"]) ? $context["t1o_menu_categories_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>   
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Categories display style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 1072
        if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_opencart"]) ? $context["text_opencart"] : null);
        echo "</option>
                                    <option value=\"2\"";
        // line 1073
        if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_vertical"]) ? $context["text_vertical"] : null);
        echo "</option>
                                    <option value=\"5\"";
        // line 1074
        if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">Vertical 2</option>
                                    <option value=\"3\"";
        // line 1075
        if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_horizontal"]) ? $context["text_horizontal"] : null);
        echo "</option>
                                    <option value=\"4\"";
        // line 1076
        if (((isset($context["t1o_menu_categories_style"]) ? $context["t1o_menu_categories_style"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">Inline</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_08.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show category images:<br /><span class=\"k_help\">for Horizontal, Vertical, Vertical 2 and OpenCart styles</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_main_category_icon_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1086
        if (((isset($context["t1o_menu_main_category_icon_status"]) ? $context["t1o_menu_main_category_icon_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1087
        if (((isset($context["t1o_menu_main_category_icon_status"]) ? $context["t1o_menu_main_category_icon_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Category dropdowns padding right:<br /><span class=\"k_help\">for Vertical and OpenCart styles</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_main_category_padding_right\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1096
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">0px</option>
                                    <option value=\"10\"";
        // line 1097
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                    <option value=\"20\"";
        // line 1098
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option>
                                    <option value=\"30\"";
        // line 1099
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "30")) {
            echo "selected=\"selected\"";
        }
        echo ">30px</option>
                                    <option value=\"40\"";
        // line 1100
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "40")) {
            echo "selected=\"selected\"";
        }
        echo ">40px</option>
                                    <option value=\"50\"";
        // line 1101
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "50")) {
            echo "selected=\"selected\"";
        }
        echo ">50px</option>
                                    <option value=\"60\"";
        // line 1102
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "60")) {
            echo "selected=\"selected\"";
        }
        echo ">60px</option>
                                    <option value=\"70\"";
        // line 1103
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "70")) {
            echo "selected=\"selected\"";
        }
        echo ">70px</option>
                                    <option value=\"80\"";
        // line 1104
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "80")) {
            echo "selected=\"selected\"";
        }
        echo ">80px</option>
                                    <option value=\"90\"";
        // line 1105
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "90")) {
            echo "selected=\"selected\"";
        }
        echo ">90px</option>
                                    <option value=\"100\"";
        // line 1106
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "100")) {
            echo "selected=\"selected\"";
        }
        echo ">100px</option>
                                    <option value=\"110\"";
        // line 1107
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "110")) {
            echo "selected=\"selected\"";
        }
        echo ">110px</option>
                                    <option value=\"120\"";
        // line 1108
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "120")) {
            echo "selected=\"selected\"";
        }
        echo ">120px</option>
                                    <option value=\"130\"";
        // line 1109
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "130")) {
            echo "selected=\"selected\"";
        }
        echo ">130px</option>
                                    <option value=\"140\"";
        // line 1110
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "140")) {
            echo "selected=\"selected\"";
        }
        echo ">140px</option>
                                    <option value=\"150\"";
        // line 1111
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "150")) {
            echo "selected=\"selected\"";
        }
        echo ">150px</option>
                                    <option value=\"160\"";
        // line 1112
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "160")) {
            echo "selected=\"selected\"";
        }
        echo ">160px</option>
                                    <option value=\"170\"";
        // line 1113
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "170")) {
            echo "selected=\"selected\"";
        }
        echo ">170px</option>
                                    <option value=\"180\"";
        // line 1114
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "180")) {
            echo "selected=\"selected\"";
        }
        echo ">180px</option>
                                    <option value=\"190\"";
        // line 1115
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "190")) {
            echo "selected=\"selected\"";
        }
        echo ">190px</option>
                                    <option value=\"200\"";
        // line 1116
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "200")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_main_category_padding_right"]) ? $context["t1o_menu_main_category_padding_right"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">200px</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_09.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Category dropdowns padding bottom:<br /><span class=\"k_help\">for Vertical and OpenCart styles</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_main_category_padding_bottom\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1126
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">0px</option>
                                    <option value=\"10\"";
        // line 1127
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10px</option>
                                    <option value=\"20\"";
        // line 1128
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "20")) {
            echo "selected=\"selected\"";
        }
        echo ">20px</option>
                                    <option value=\"30\"";
        // line 1129
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "30")) {
            echo "selected=\"selected\"";
        }
        echo ">30px</option>
                                    <option value=\"40\"";
        // line 1130
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "40")) {
            echo "selected=\"selected\"";
        }
        echo ">40px</option>
                                    <option value=\"50\"";
        // line 1131
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "50")) {
            echo "selected=\"selected\"";
        }
        echo ">50px</option>
                                    <option value=\"60\"";
        // line 1132
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "60")) {
            echo "selected=\"selected\"";
        }
        echo ">60px</option>
                                    <option value=\"70\"";
        // line 1133
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "70")) {
            echo "selected=\"selected\"";
        }
        echo ">70px</option>
                                    <option value=\"80\"";
        // line 1134
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "80")) {
            echo "selected=\"selected\"";
        }
        echo ">80px</option>
                                    <option value=\"90\"";
        // line 1135
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "90")) {
            echo "selected=\"selected\"";
        }
        echo ">90px</option>
                                    <option value=\"100\"";
        // line 1136
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "100")) {
            echo "selected=\"selected\"";
        }
        echo ">100px</option>
                                    <option value=\"110\"";
        // line 1137
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "110")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">110px</option>
                                    <option value=\"120\"";
        // line 1138
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "120")) {
            echo "selected=\"selected\"";
        }
        echo ">120px</option>
                                    <option value=\"130\"";
        // line 1139
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "130")) {
            echo "selected=\"selected\"";
        }
        echo ">130px</option>
                                    <option value=\"140\"";
        // line 1140
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "140")) {
            echo "selected=\"selected\"";
        }
        echo ">140px</option>
                                    <option value=\"150\"";
        // line 1141
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "150")) {
            echo "selected=\"selected\"";
        }
        echo ">150px</option>
                                    <option value=\"160\"";
        // line 1142
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "160")) {
            echo "selected=\"selected\"";
        }
        echo ">160px</option>
                                    <option value=\"170\"";
        // line 1143
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "170")) {
            echo "selected=\"selected\"";
        }
        echo ">170px</option>
                                    <option value=\"180\"";
        // line 1144
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "180")) {
            echo "selected=\"selected\"";
        }
        echo ">180px</option>
                                    <option value=\"190\"";
        // line 1145
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "190")) {
            echo "selected=\"selected\"";
        }
        echo ">190px</option>
                                    <option value=\"200\"";
        // line 1146
        if (((isset($context["t1o_menu_main_category_padding_bottom"]) ? $context["t1o_menu_main_category_padding_bottom"] : null) == "200")) {
            echo "selected=\"selected\"";
        }
        echo ">200px</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_09.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a> 
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Categories per row:<br /><span class=\"k_help\">for Horizontal style</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"col-sm-4\"";
        // line 1156
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "col-sm-4")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                                    <option value=\"col-sm-3\"";
        // line 1157
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "col-sm-3")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                                    <option value=\"col-sm-5-pr\"";
        // line 1158
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "col-sm-5-pr")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                                    <option value=\"col-sm-2\"";
        // line 1159
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "col-sm-2")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
                                    <option value=\"col-sm-7-pr\"";
        // line 1160
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "col-sm-7-pr")) {
            echo "selected=\"selected\"";
        }
        echo ">7</option>
                                    <option value=\"col-sm-8-pr\"";
        // line 1161
        if (((isset($context["t1o_menu_categories_per_row"]) ? $context["t1o_menu_categories_per_row"] : null) == "col-sm-8-pr")) {
            echo "selected=\"selected\"";
        }
        echo ">8</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show 3 level category:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_3_level\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1170
        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1171
        if (((isset($context["t1o_menu_categories_3_level"]) ? $context["t1o_menu_categories_3_level"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Category dropdown on Home Page:<br /><span class=\"k_help\">for \"Vertical 2\" style</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_home_visibility\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 1180
        if (((isset($context["t1o_menu_categories_home_visibility"]) ? $context["t1o_menu_categories_home_visibility"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">On Hover</option>
                                    <option value=\"1\"";
        // line 1181
        if (((isset($context["t1o_menu_categories_home_visibility"]) ? $context["t1o_menu_categories_home_visibility"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_categories_home_visibility"]) ? $context["t1o_menu_categories_home_visibility"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Always Visible</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_08.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Custom Blocks for Horizontal style <a href=\"view/image/theme_img/help_oxy_theme/go_11.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Left Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_custom_block_left_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1193
        if (((isset($context["t1o_menu_categories_custom_block_left_status"]) ? $context["t1o_menu_categories_custom_block_left_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1194
        if (((isset($context["t1o_menu_categories_custom_block_left_status"]) ? $context["t1o_menu_categories_custom_block_left_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option> 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Left Custom Block Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_menu_categories_custom_block_left_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 1204
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_menu_categories_custom_block_left_content_";
            // line 1205
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1207
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 1210
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_menu_categories_custom_block_left_content_";
            // line 1211
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_menu_categories_custom_block_left_content[";
            // line 1212
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_menu_categories_custom_block_left_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_menu_categories_custom_block_left_content"]) ? $context["t1o_menu_categories_custom_block_left_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_categories_custom_block_left_content"]) ? $context["t1o_menu_categories_custom_block_left_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1215
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Right Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_custom_block_right_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1224
        if (((isset($context["t1o_menu_categories_custom_block_right_status"]) ? $context["t1o_menu_categories_custom_block_right_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1225
        if (((isset($context["t1o_menu_categories_custom_block_right_status"]) ? $context["t1o_menu_categories_custom_block_right_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Right Custom Block Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_menu_categories_custom_block_right_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 1235
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_menu_categories_custom_block_right_content_";
            // line 1236
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1238
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 1241
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_menu_categories_custom_block_right_content_";
            // line 1242
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_menu_categories_custom_block_right_content[";
            // line 1243
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_menu_categories_custom_block_right_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_menu_categories_custom_block_right_content"]) ? $context["t1o_menu_categories_custom_block_right_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_categories_custom_block_right_content"]) ? $context["t1o_menu_categories_custom_block_right_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1246
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Bottom Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_categories_custom_block_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1255
        if (((isset($context["t1o_menu_categories_custom_block_status"]) ? $context["t1o_menu_categories_custom_block_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1256
        if (((isset($context["t1o_menu_categories_custom_block_status"]) ? $context["t1o_menu_categories_custom_block_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Bottom Custom Block Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_menu_categories_custom_block_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 1266
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_menu_categories_custom_block_content_";
            // line 1267
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1269
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 1272
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_menu_categories_custom_block_content_";
            // line 1273
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_menu_categories_custom_block_content[";
            // line 1274
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_menu_categories_custom_block_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_menu_categories_custom_block_content"]) ? $context["t1o_menu_categories_custom_block_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_menu_categories_custom_block_content"]) ? $context["t1o_menu_categories_custom_block_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1277
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-menu-brands\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Brands <span class=\"k_help_tip k_tooltip\" title=\"Before you turn on this option, add at least one manufacturer.\" data-toggle=\"tooltip\">?</span></legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Brands:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_brands_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1296
        if (((isset($context["t1o_menu_brands_status"]) ? $context["t1o_menu_brands_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1297
        if (((isset($context["t1o_menu_brands_status"]) ? $context["t1o_menu_brands_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_brands_status"]) ? $context["t1o_menu_brands_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Brands display style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_brands_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"logoname\"";
        // line 1306
        if (((isset($context["t1o_menu_brands_style"]) ? $context["t1o_menu_brands_style"] : null) == "logoname")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_brand_logo_name"]) ? $context["text_brand_logo_name"] : null);
        echo "</option>  
                                    <option value=\"logo\"";
        // line 1307
        if (((isset($context["t1o_menu_brands_style"]) ? $context["t1o_menu_brands_style"] : null) == "logo")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_brand_logo"]) ? $context["text_brand_logo"] : null);
        echo "</option>
                                    <option value=\"name\"";
        // line 1308
        if (((isset($context["t1o_menu_brands_style"]) ? $context["t1o_menu_brands_style"] : null) == "name")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_brand_name"]) ? $context["text_brand_name"] : null);
        echo "</option>
                                             
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Brands per row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_brands_per_row\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"4\"";
        // line 1318
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                                    <option value=\"3\"";
        // line 1319
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option>
                                    <option value=\"2\"";
        // line 1320
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
                                    <option value=\"8-pr\"";
        // line 1321
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "8-pr")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">8</option>
                                    <option value=\"10-pr\"";
        // line 1322
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "10-pr")) {
            echo "selected=\"selected\"";
        }
        echo ">10</option>
                                    <option value=\"1\"";
        // line 1323
        if (((isset($context["t1o_menu_brands_per_row"]) ? $context["t1o_menu_brands_per_row"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">12</option>         
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-menu-custom-blocks\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        ";
        // line 1336
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 5));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1337
            echo "                        
                        <legend>Custom Block ";
            // line 1338
            echo $context["i"];
            echo "<a href=\"view/image/theme_img/help_oxy_theme/go_13.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Block ";
            // line 1341
            echo $context["i"];
            echo ":</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_custom_block[";
            // line 1343
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 1344
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                    <option value=\"1\"";
            // line 1345
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
            // line 1353
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 1354
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 1355
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_menu_custom_block[";
                // line 1356
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_menu_custom_block_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1359
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_menu_custom_block_";
            // line 1366
            echo $context["i"];
            echo "\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
            // line 1367
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_menu_custom_block_";
                // line 1368
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "\" data-toggle=\"tab\"><img src=\"language/";
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" alt=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" width=\"16px\" height=\"11px\" /> ";
                echo $this->getAttribute($context["language"], "name", array());
                echo "</a></li>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1370
            echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            // line 1373
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_menu_custom_block_";
                // line 1374
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "\" class=\"tab-pane\">
                                            <textarea name=\"t1o_menu_custom_block[";
                // line 1375
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][htmlcontent]\" id=\"t1o_menu_custom_block_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_content\">";
                echo (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_custom_block"]) ? $context["t1o_menu_custom_block"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
                echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1378
            echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1384
        echo "                        
                    </fieldset>
                    
        </div>
        
        <div id=\"tab-menu-custom-dropdown-menu\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Custom Dropdown Menu 1<a href=\"view/image/theme_img/help_oxy_theme/go_12.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Menu 1:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_cm_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1399
        if (((isset($context["t1o_menu_cm_status"]) ? $context["t1o_menu_cm_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 1400
        if (((isset($context["t1o_menu_cm_status"]) ? $context["t1o_menu_cm_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 1408
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 1409
            echo "\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 1410
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                        <input type=\"text\" name=\"t1o_menu_cm_title[";
            // line 1411
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_menu_cm_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_menu_cm_title"]) ? $context["t1o_menu_cm_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_menu_cm_title"]) ? $context["t1o_menu_cm_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1414
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
                                    <th class=\"left\" width=\"10%\">Link</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"10%\">Show</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">Title</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">URL</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"20%\">Open</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 1430
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1431
            echo "                                    <tr>
\t\t\t\t\t\t\t\t\t<td>Link ";
            // line 1432
            echo $context["i"];
            echo ":</td>
\t\t\t\t\t\t\t\t\t<td>
    
                                    <select name=\"t1o_menu_cm_link[";
            // line 1435
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 1436
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                        <option value=\"1\"";
            // line 1437
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    \t</select>

                                    </td>
                                    <td>
                                    
                                    ";
            // line 1443
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 1444
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 1445
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_menu_cm_link[";
                // line 1446
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_menu_cm_link_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1449
            echo "                                    
                                    </td>
                                    <td>
                                    
                                    <input type=\"text\" name=\"t1o_menu_cm_link[";
            // line 1453
            echo $context["i"];
            echo "][url]\" value=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "url", array());
            echo "\" class=\"form-control\" />
                                    
                                    </td>
                                    <td>
                                    
                                    <select name=\"t1o_menu_cm_link[";
            // line 1458
            echo $context["i"];
            echo "][target]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t<option value=\"_self\"";
            // line 1459
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "target", array()) == "_self")) {
                echo "selected=\"selected\"";
            }
            echo ">in the same frame</option> 
                                    \t<option value=\"_blank\"";
            // line 1460
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_link"]) ? $context["t1o_menu_cm_link"] : null), $context["i"], array(), "array"), "target", array()) == "_blank")) {
                echo "selected=\"selected\"";
            }
            echo ">in a new tab</option>
\t\t\t\t\t\t\t\t\t</select>

                                    </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1466
        echo "                                </tbody>
\t\t\t\t\t\t\t</table>
                        
                        </div>
                        
                        <legend>Custom Dropdown Menu 2</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Menu 2:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_cm_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1477
        if (((isset($context["t1o_menu_cm_2_status"]) ? $context["t1o_menu_cm_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 1478
        if (((isset($context["t1o_menu_cm_2_status"]) ? $context["t1o_menu_cm_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 1486
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 1487
            echo "\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 1488
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                        <input type=\"text\" name=\"t1o_menu_cm_2_title[";
            // line 1489
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_menu_cm_2_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_menu_cm_2_title"]) ? $context["t1o_menu_cm_2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_menu_cm_2_title"]) ? $context["t1o_menu_cm_2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1492
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
                                    <th class=\"left\" width=\"10%\">Link</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"10%\">Show</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">Title</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">URL</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"20%\">Open</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 1508
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1509
            echo "                                    <tr>
\t\t\t\t\t\t\t\t\t<td>Link ";
            // line 1510
            echo $context["i"];
            echo ":</td>
\t\t\t\t\t\t\t\t\t<td>
    
                                    <select name=\"t1o_menu_cm_2_link[";
            // line 1513
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 1514
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                        <option value=\"1\"";
            // line 1515
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    \t</select>

                                    </td>
                                    <td>
                                    
                                    ";
            // line 1521
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 1522
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 1523
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_menu_cm_2_link[";
                // line 1524
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_menu_cm_2_link_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1527
            echo "                                    
                                    </td>
                                    <td>
                                    
                                    <input type=\"text\" name=\"t1o_menu_cm_2_link[";
            // line 1531
            echo $context["i"];
            echo "][url]\" value=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "url", array());
            echo "\" class=\"form-control\" />
                                    
                                    </td>
                                    <td>
                                    
                                    <select name=\"t1o_menu_cm_2_link[";
            // line 1536
            echo $context["i"];
            echo "][target]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t<option value=\"_self\"";
            // line 1537
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "target", array()) == "_self")) {
                echo "selected=\"selected\"";
            }
            echo ">in the same frame</option> 
                                    \t<option value=\"_blank\"";
            // line 1538
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_2_link"]) ? $context["t1o_menu_cm_2_link"] : null), $context["i"], array(), "array"), "target", array()) == "_blank")) {
                echo "selected=\"selected\"";
            }
            echo ">in a new tab</option>
\t\t\t\t\t\t\t\t\t</select>
                                    
                                    </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1544
        echo "                                </tbody>
\t\t\t\t\t\t\t</table>
                        
                        </div>
                        
                        <legend>Custom Dropdown Menu 3</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Menu 3:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_menu_cm_3_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1555
        if (((isset($context["t1o_menu_cm_3_status"]) ? $context["t1o_menu_cm_3_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 1556
        if (((isset($context["t1o_menu_cm_3_status"]) ? $context["t1o_menu_cm_3_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t    <label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 1564
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 1565
            echo "\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 1566
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                        <input type=\"text\" name=\"t1o_menu_cm_3_title[";
            // line 1567
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_menu_cm_3_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_menu_cm_3_title"]) ? $context["t1o_menu_cm_3_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_menu_cm_3_title"]) ? $context["t1o_menu_cm_3_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />  
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1570
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
                                    <th class=\"left\" width=\"10%\">Link</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"10%\">Show</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">Title</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">URL</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"20%\">Open</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 1586
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1587
            echo "                                    <tr>
\t\t\t\t\t\t\t\t\t<td>Link ";
            // line 1588
            echo $context["i"];
            echo ":</td>
\t\t\t\t\t\t\t\t\t<td>
    
                                    <select name=\"t1o_menu_cm_3_link[";
            // line 1591
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 1592
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                        <option value=\"1\"";
            // line 1593
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    \t</select>

                                    </td>
                                    <td>
                                    
                                    ";
            // line 1599
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 1600
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 1601
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_menu_cm_3_link[";
                // line 1602
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_menu_cm_3_link_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1605
            echo "                                    
                                    </td>
                                    <td>
                                    
                                    <input type=\"text\" name=\"t1o_menu_cm_3_link[";
            // line 1609
            echo $context["i"];
            echo "][url]\" value=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "url", array());
            echo "\" class=\"form-control\" />
                                    
                                    </td>
                                    <td>
                                    
                                    <select name=\"t1o_menu_cm_3_link[";
            // line 1614
            echo $context["i"];
            echo "][target]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t<option value=\"_self\"";
            // line 1615
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "target", array()) == "_self")) {
                echo "selected=\"selected\"";
            }
            echo ">in the same frame</option> 
                                    \t<option value=\"_blank\"";
            // line 1616
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_cm_3_link"]) ? $context["t1o_menu_cm_3_link"] : null), $context["i"], array(), "array"), "target", array()) == "_blank")) {
                echo "selected=\"selected\"";
            }
            echo ">in a new tab</option>
\t\t\t\t\t\t\t\t\t</select>
                                    
                                    </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1622
        echo "                                </tbody>
\t\t\t\t\t\t\t</table>
                        
                        </div>
                        
                    </fieldset>
                    
        </div>
        
        <div id=\"tab-menu-custom-links\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Custom Links <a href=\"view/image/theme_img/help_oxy_theme/go_11.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
                                    <th class=\"left\" width=\"10%\">Link</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"10%\">Show</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">Title</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"30%\">URL</th>
\t\t\t\t\t\t\t\t\t<th class=\"left\" width=\"20%\">Open</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
                                <tbody>
\t\t\t\t\t\t\t\t\t";
        // line 1650
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 10));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1651
            echo "                                    <tr>
\t\t\t\t\t\t\t\t\t<td>Link ";
            // line 1652
            echo $context["i"];
            echo ":</td>
\t\t\t\t\t\t\t\t\t<td>
                                    <select name=\"t1o_menu_link[";
            // line 1654
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 1655
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                        <option value=\"1\"";
            // line 1656
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    \t</select>
                                    </td>
                                    <td>
                                    ";
            // line 1660
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 1661
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 1662
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_menu_link[";
                // line 1663
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_menu_link_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" /> 
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1666
            echo "                                    </td>
                                    <td>
                                    <input type=\"text\" name=\"t1o_menu_link[";
            // line 1668
            echo $context["i"];
            echo "][url]\" value=\"";
            echo $this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "url", array());
            echo "\" class=\"form-control\" />
                                    </td>
                                    <td>

                                    <select name=\"t1o_menu_link[";
            // line 1672
            echo $context["i"];
            echo "][target]\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t\t<option value=\"_self\"";
            // line 1673
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "target", array()) == "_self")) {
                echo "selected=\"selected\"";
            }
            echo ">in the same frame</option> 
                                    \t<option value=\"_blank\"";
            // line 1674
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_menu_link"]) ? $context["t1o_menu_link"] : null), $context["i"], array(), "array"), "target", array()) == "_blank")) {
                echo "selected=\"selected\"";
            }
            echo ">in a new tab</option>
\t\t\t\t\t\t\t\t\t</select>
                                    
                                    </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1680
        echo "                                </tbody>
\t\t\t\t\t\t\t</table>
                        
                        </div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-menu-labels\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Menu Labels <a href=\"view/image/theme_img/help_oxy_theme/go_14.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"table-responsive\">
                        
\t\t\t\t\t\t\t<table class=\"table table-hover\">
\t\t\t\t\t\t\t\t\t";
        // line 1698
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 15));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 1699
            echo "                                    <tr>
\t\t\t\t\t\t\t\t\t<td width=\"15%\" style=\"text-align:right;\">Menu Label ";
            // line 1700
            echo $context["i"];
            echo ":</td>
\t\t\t\t\t\t\t\t\t<td width=\"35%\">
                                    ";
            // line 1702
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo "\t
\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 1704
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_menu_labels[";
                // line 1705
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_menu_labels_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_labels"]) ? $context["t1o_menu_labels"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_menu_labels"]) ? $context["t1o_menu_labels"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Label\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1708
            echo "                                    </td>
                                    <td width=\"15%\" style=\"text-align:right;\">Background color:</td>
                                    <td width=\"35%\">
                                    <input type=\"text\" name=\"t1o_menu_labels_color[";
            // line 1711
            echo $context["i"];
            echo "]\" id=\"t1o_menu_labels_color_[";
            echo $context["i"];
            echo "]\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_menu_labels_color"]) ? $context["t1o_menu_labels_color"] : null), $context["i"], array(), "array")) ? ($this->getAttribute((isset($context["t1o_menu_labels_color"]) ? $context["t1o_menu_labels_color"] : null), $context["i"], array(), "array")) : ("#16B778"));
            echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
                                    </td>
                                    </tr>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1715
        echo "\t\t\t\t\t\t\t</table>
                        
                        </div>
                        
                    </fieldset>
                    
        </div>

        <div id=\"tab-menu-custom-bar\" class=\"tab-pane\">
        
                    <fieldset>
                        
                        <legend>Custom Bar below Main Menu <a href=\"view/image/theme_img/help_oxy_theme/go_15.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Bar:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_bar_below_menu_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1733
        if (((isset($context["t1o_custom_bar_below_menu_status"]) ? $context["t1o_custom_bar_below_menu_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1734
        if (((isset($context["t1o_custom_bar_below_menu_status"]) ? $context["t1o_custom_bar_below_menu_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_bar_below_menu_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 1744
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_bar_below_menu_content_";
            // line 1745
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1747
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 1750
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_bar_below_menu_content_";
            // line 1751
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_bar_below_menu_content[";
            // line 1752
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_bar_below_menu_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_bar_below_menu_content"]) ? $context["t1o_custom_bar_below_menu_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_bar_below_menu_content"]) ? $context["t1o_custom_bar_below_menu_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 1755
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Background Image:</label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1o_custom_bar_below_menu_bg_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 1762
        echo (isset($context["t1o_custom_bar_below_menu_bg_thumb"]) ? $context["t1o_custom_bar_below_menu_bg_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_custom_bar_below_menu_bg\" value=\"";
        // line 1763
        echo (isset($context["t1o_custom_bar_below_menu_bg"]) ? $context["t1o_custom_bar_below_menu_bg"] : null);
        echo "\" id=\"t1o_custom_bar_below_menu_bg\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background Image Animation:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_bar_below_menu_bg_animation\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1770
        if (((isset($context["t1o_custom_bar_below_menu_bg_animation"]) ? $context["t1o_custom_bar_below_menu_bg_animation"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1771
        if (((isset($context["t1o_custom_bar_below_menu_bg_animation"]) ? $context["t1o_custom_bar_below_menu_bg_animation"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_custom_bar_below_menu_bg_color\" id=\"t1o_custom_bar_below_menu_bg_color\" value=\"";
        // line 1778
        echo (isset($context["t1o_custom_bar_below_menu_bg_color"]) ? $context["t1o_custom_bar_below_menu_bg_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Text color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_custom_bar_below_menu_text_color\" id=\"t1o_custom_bar_below_menu_text_color\" value=\"";
        // line 1784
        echo (isset($context["t1o_custom_bar_below_menu_text_color"]) ? $context["t1o_custom_bar_below_menu_text_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset> 
     
        </div>
        
        </div>
        </div>
        
        </div>
        </div>




        <div class=\"tab-pane\" id=\"tab-midsection\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"midsection_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-midsection-category\" data-toggle=\"tab\">Category Page</a></li>
             <li><a href=\"#tab-midsection-product\" data-toggle=\"tab\">Product Page</a></li>
             <li><a href=\"#tab-midsection-contact\" data-toggle=\"tab\">Contact Page</a></li>
             <li><a href=\"#tab-midsection-lf\" data-toggle=\"tab\">Left/Right Column</a></li>                                      
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-midsection-category\" class=\"tab-pane fade in active\">  
        
                    <fieldset>
                    
                        <legend>Category Title <a href=\"view/image/theme_img/help_oxy_theme/go_16.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Category Title position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_title_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1826
        if (((isset($context["t1o_category_title_position"]) ? $context["t1o_category_title_position"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Above Content Column</option>
                                    <option value=\"1\"";
        // line 1827
        if (((isset($context["t1o_category_title_position"]) ? $context["t1o_category_title_position"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_title_position"]) ? $context["t1o_category_title_position"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Content Column</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Category Title color:<br /><span class=\"k_help\">for \"Above Content Column\" position</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_category_title_above_color\" id=\"t1o_category_title_above_color\" value=\"";
        // line 1834
        echo (isset($context["t1o_category_title_above_color"]) ? $context["t1o_category_title_above_color"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Category Info <a href=\"view/image/theme_img/help_oxy_theme/go_17.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show category description:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_desc_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1844
        if (((isset($context["t1o_category_desc_status"]) ? $context["t1o_category_desc_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1845
        if (((isset($context["t1o_category_desc_status"]) ? $context["t1o_category_desc_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_desc_status"]) ? $context["t1o_category_desc_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>  
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show category image:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_img_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1853
        if (((isset($context["t1o_category_img_status"]) ? $context["t1o_category_img_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1854
        if (((isset($context["t1o_category_img_status"]) ? $context["t1o_category_img_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
                            <label class=\"col-sm-2 control-label\">Parallax scrolling effect:<br /><span class=\"k_help\">for \"Above Content Column\" category title position</span></label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_img_parallax\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1862
        if (((isset($context["t1o_category_img_parallax"]) ? $context["t1o_category_img_parallax"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1863
        if (((isset($context["t1o_category_img_parallax"]) ? $context["t1o_category_img_parallax"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_18.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t        </div>
                        
                        <legend>Subcategories <a href=\"view/image/theme_img/help_oxy_theme/go_19.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show subcategories:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_subcategories_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1875
        if (((isset($context["t1o_category_subcategories_status"]) ? $context["t1o_category_subcategories_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1876
        if (((isset($context["t1o_category_subcategories_status"]) ? $context["t1o_category_subcategories_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_subcategories_status"]) ? $context["t1o_category_subcategories_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subcategories style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_subcategories_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1885
        if (((isset($context["t1o_category_subcategories_style"]) ? $context["t1o_category_subcategories_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">OXY Theme</option>
                                    <option value=\"1\"";
        // line 1886
        if (((isset($context["t1o_category_subcategories_style"]) ? $context["t1o_category_subcategories_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">OpenCart</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subcategories per row:<br /><span class=\"k_help\">for \"OXY Theme\" style</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_subcategories_per_row\" class=\"form-control\">
                           \t\t\t<option value=\"2\"";
        // line 1895
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 1896
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 1897
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 1898
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 1899
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
                                    <option value=\"7\"";
        // line 1900
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">7</option>
                                    <option value=\"8\"";
        // line 1901
        if (((isset($context["t1o_category_subcategories_per_row"]) ? $context["t1o_category_subcategories_per_row"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">8</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">AutoPlay:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_subcategories_autoplay\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"4000\"";
        // line 1910
        if (((isset($context["t1o_category_subcategories_autoplay"]) ? $context["t1o_category_subcategories_autoplay"] : null) == "4000")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
                                    <option value=\"false\"";
        // line 1911
        if (((isset($context["t1o_category_subcategories_autoplay"]) ? $context["t1o_category_subcategories_autoplay"] : null) == "false")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<legend>Product Box <a href=\"view/image/theme_img/help_oxy_theme/go_20.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Product Box style:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_box_style\" class=\"form-control\">
                                    <option value=\"product-box-style-1\"";
        // line 1922
        if (((isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null) == "product-box-style-1")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 1</option>
\t\t\t\t\t\t\t\t\t<option value=\"product-box-style-2\"";
        // line 1923
        if (((isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null) == "product-box-style-2")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 2</option>
                                    <option value=\"product-box-style-3\"";
        // line 1924
        if (((isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null) == "product-box-style-3")) {
            echo "selected=\"selected\"";
        }
        echo ">Style 3</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_01.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show sale badge:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_sale_badge_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1934
        if (((isset($context["t1o_sale_badge_status"]) ? $context["t1o_sale_badge_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1935
        if (((isset($context["t1o_sale_badge_status"]) ? $context["t1o_sale_badge_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_sale_badge_status"]) ? $context["t1o_sale_badge_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
                                    
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Sale badge type:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_sale_badge_type\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1945
        if (((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">SALE text</option>
                                    <option value=\"1\"";
        // line 1946
        if (((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Percent</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                                       
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show new product badge:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_new_badge_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1955
        if (((isset($context["t1o_new_badge_status"]) ? $context["t1o_new_badge_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1956
        if (((isset($context["t1o_new_badge_status"]) ? $context["t1o_new_badge_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_new_badge_status"]) ? $context["t1o_new_badge_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show \"Out of Stock\" badge:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_out_of_stock_badge_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1965
        if (((isset($context["t1o_out_of_stock_badge_status"]) ? $context["t1o_out_of_stock_badge_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1966
        if (((isset($context["t1o_out_of_stock_badge_status"]) ? $context["t1o_out_of_stock_badge_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_out_of_stock_badge_status"]) ? $context["t1o_out_of_stock_badge_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show product name:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_name_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1975
        if (((isset($context["t1o_category_prod_name_status"]) ? $context["t1o_category_prod_name_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1976
        if (((isset($context["t1o_category_prod_name_status"]) ? $context["t1o_category_prod_name_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_name_status"]) ? $context["t1o_category_prod_name_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show product brand:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_brand_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1985
        if (((isset($context["t1o_category_prod_brand_status"]) ? $context["t1o_category_prod_brand_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1986
        if (((isset($context["t1o_category_prod_brand_status"]) ? $context["t1o_category_prod_brand_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_brand_status"]) ? $context["t1o_category_prod_brand_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show product price:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_price_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 1995
        if (((isset($context["t1o_category_prod_price_status"]) ? $context["t1o_category_prod_price_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 1996
        if (((isset($context["t1o_category_prod_price_status"]) ? $context["t1o_category_prod_price_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_price_status"]) ? $context["t1o_category_prod_price_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option> 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show \"Quick View\" button:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_quickview_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2005
        if (((isset($context["t1o_category_prod_quickview_status"]) ? $context["t1o_category_prod_quickview_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2006
        if (((isset($context["t1o_category_prod_quickview_status"]) ? $context["t1o_category_prod_quickview_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_quickview_status"]) ? $context["t1o_category_prod_quickview_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show \"Add to Cart\" button:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_cart_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2015
        if (((isset($context["t1o_category_prod_cart_status"]) ? $context["t1o_category_prod_cart_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2016
        if (((isset($context["t1o_category_prod_cart_status"]) ? $context["t1o_category_prod_cart_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_cart_status"]) ? $context["t1o_category_prod_cart_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show rating stars:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_ratings_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2025
        if (((isset($context["t1o_category_prod_ratings_status"]) ? $context["t1o_category_prod_ratings_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2026
        if (((isset($context["t1o_category_prod_ratings_status"]) ? $context["t1o_category_prod_ratings_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_ratings_status"]) ? $context["t1o_category_prod_ratings_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show \"Add to Wishlist\", \"Add to Compare\" buttons:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_wis_com_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2035
        if (((isset($context["t1o_category_prod_wis_com_status"]) ? $context["t1o_category_prod_wis_com_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2036
        if (((isset($context["t1o_category_prod_wis_com_status"]) ? $context["t1o_category_prod_wis_com_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_wis_com_status"]) ? $context["t1o_category_prod_wis_com_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show zoom image effect:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_zoom_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2045
        if (((isset($context["t1o_category_prod_zoom_status"]) ? $context["t1o_category_prod_zoom_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2046
        if (((isset($context["t1o_category_prod_zoom_status"]) ? $context["t1o_category_prod_zoom_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show swap image effect:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_swap_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2055
        if (((isset($context["t1o_category_prod_swap_status"]) ? $context["t1o_category_prod_swap_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2056
        if (((isset($context["t1o_category_prod_swap_status"]) ? $context["t1o_category_prod_swap_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_swap_status"]) ? $context["t1o_category_prod_swap_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Box hover shadow:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_shadow_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2065
        if (((isset($context["t1o_category_prod_shadow_status"]) ? $context["t1o_category_prod_shadow_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2066
        if (((isset($context["t1o_category_prod_shadow_status"]) ? $context["t1o_category_prod_shadow_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_shadow_status"]) ? $context["t1o_category_prod_shadow_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Box hover lift up:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_lift_up_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2075
        if (((isset($context["t1o_category_prod_lift_up_status"]) ? $context["t1o_category_prod_lift_up_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2076
        if (((isset($context["t1o_category_prod_lift_up_status"]) ? $context["t1o_category_prod_lift_up_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Align items to:<br /><span class=\"k_help\">for Grid view</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_category_prod_align\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 2085
        if (((isset($context["t1o_category_prod_align"]) ? $context["t1o_category_prod_align"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Center</option>
                                    <option value=\"1\"";
        // line 2086
        if (((isset($context["t1o_category_prod_align"]) ? $context["t1o_category_prod_align"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_category_prod_align"]) ? $context["t1o_category_prod_align"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Left</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-midsection-product\" class=\"tab-pane\">  
        
                    <fieldset>
                        
                        <legend class=\"bn\"></legend> 
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Prev/Next products:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_prev_next_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2105
        if (((isset($context["t1o_product_prev_next_status"]) ? $context["t1o_product_prev_next_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2106
        if (((isset($context["t1o_product_prev_next_status"]) ? $context["t1o_product_prev_next_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_prev_next_status"]) ? $context["t1o_product_prev_next_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>  
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_21.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Product Page Layout</legend> 
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Product Page Layout:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_layout_product_page\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 2118
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 1</option>
                           \t\t\t<option value=\"2\"";
        // line 2119
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 2</option>
                           \t\t\t<option value=\"3\"";
        // line 2120
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 3</option>
                           \t\t\t<option value=\"4\"";
        // line 2121
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 4</option> 
                           \t\t\t<option value=\"5\"";
        // line 2122
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 5</option>
                           \t\t\t<option value=\"6\"";
        // line 2123
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 6</option>
                           \t\t\t<option value=\"7\"";
        // line 2124
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 7</option>    
                           \t\t\t<option value=\"8\"";
        // line 2125
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 8</option>
                           \t\t\t<option value=\"9\"";
        // line 2126
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "9")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 9</option>
                           \t\t\t<option value=\"10\"";
        // line 2127
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 10</option>
                                    <option value=\"11\"";
        // line 2128
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 11</option>
                                    <option value=\"12\"";
        // line 2129
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 12</option>
                                    <option value=\"13\"";
        // line 2130
        if (((isset($context["t1o_layout_product_page"]) ? $context["t1o_layout_product_page"] : null) == "13")) {
            echo "selected=\"selected\"";
        }
        echo ">Layout 13</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
                            <div class=\"col-sm-12\">
                                ";
        // line 2137
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["ppl"]) {
            echo " 
                                <div class=\"prod_l\"><img src=\"view/image/theme_img/pl_";
            // line 2138
            echo $context["ppl"];
            echo ".png\"><span class=\"t1o_help prod_l_nr\">Layout ";
            echo $context["ppl"];
            echo "</span></div> 
\t\t\t\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ppl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2139
        echo "\t
\t\t\t\t\t\t    </div>   
                            <div class=\"col-sm-12\">
                                ";
        // line 2142
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(5, 8));
        foreach ($context['_seq'] as $context["_key"] => $context["ppl"]) {
            // line 2143
            echo "                                <div class=\"prod_l\"><img src=\"view/image/theme_img/pl_";
            echo $context["ppl"];
            echo ".png\"><span class=\"t1o_help prod_l_nr\">Layout ";
            echo $context["ppl"];
            echo "</span></div> 
\t\t\t\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ppl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2144
        echo "\t
\t\t\t\t\t\t    </div>
                            <div class=\"col-sm-12\">
                                ";
        // line 2147
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(9, 12));
        foreach ($context['_seq'] as $context["_key"] => $context["ppl"]) {
            // line 2148
            echo "                                <div class=\"prod_l\"><img src=\"view/image/theme_img/pl_";
            echo $context["ppl"];
            echo ".png\"><span class=\"t1o_help prod_l_nr\">Layout ";
            echo $context["ppl"];
            echo "</span></div> 
\t\t\t\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ppl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2149
        echo "\t
\t\t\t\t\t\t    </div>
                            <div class=\"col-sm-12\">
                                ";
        // line 2152
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(13, 13));
        foreach ($context['_seq'] as $context["_key"] => $context["ppl"]) {
            // line 2153
            echo "                                <div class=\"prod_l\"><img src=\"view/image/theme_img/pl_";
            echo $context["ppl"];
            echo ".png\"><span class=\"t1o_help prod_l_nr\">Layout ";
            echo $context["ppl"];
            echo "</span></div> 
\t\t\t\t\t\t        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ppl'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2155
        echo "\t\t\t\t\t\t    </div>
                        </div>

\t\t\t\t\t\t<legend>Product Name <a href=\"view/image/theme_img/help_oxy_theme/go_22.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_name_position\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 2164
        if (((isset($context["t1o_product_name_position"]) ? $context["t1o_product_name_position"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Buy Column</option>
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 2165
        if (((isset($context["t1o_product_name_position"]) ? $context["t1o_product_name_position"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_name_position"]) ? $context["t1o_product_name_position"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Content Column</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Product Images</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Thumbnails in a row:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_additional_images\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"1\"";
        // line 2176
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                           \t\t\t<option value=\"2\"";
        // line 2177
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                           \t\t\t<option value=\"3\"";
        // line 2178
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
                           \t\t\t<option value=\"4\"";
        // line 2179
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4</option> 
                           \t\t\t<option value=\"5\"";
        // line 2180
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5</option>
                           \t\t\t<option value=\"6\"";
        // line 2181
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">6</option>
                           \t\t\t<option value=\"7\"";
        // line 2182
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">7</option>    
                           \t\t\t<option value=\"8\"";
        // line 2183
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">8</option>
                           \t\t\t<option value=\"9\"";
        // line 2184
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "9")) {
            echo "selected=\"selected\"";
        }
        echo ">9</option>
                           \t\t\t<option value=\"10\"";
        // line 2185
        if (((isset($context["t1o_additional_images"]) ? $context["t1o_additional_images"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10</option>
\t\t\t\t\t\t\t\t</select>  
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Buy Section</legend>
\t\t                    
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Align items to:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_align\" class=\"form-control\">
                                    <option value=\"0\"";
        // line 2196
        if (((isset($context["t1o_product_align"]) ? $context["t1o_product_align"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Center</option>
                                    <option value=\"1\"";
        // line 2197
        if (((isset($context["t1o_product_align"]) ? $context["t1o_product_align"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_align"]) ? $context["t1o_product_align"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Left</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_23.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show manufacturer logo:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_manufacturer_logo_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2207
        if (((isset($context["t1o_product_manufacturer_logo_status"]) ? $context["t1o_product_manufacturer_logo_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2208
        if (((isset($context["t1o_product_manufacturer_logo_status"]) ? $context["t1o_product_manufacturer_logo_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_manufacturer_logo_status"]) ? $context["t1o_product_manufacturer_logo_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_24.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Save Percent:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_save_percent_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2218
        if (((isset($context["t1o_product_save_percent_status"]) ? $context["t1o_product_save_percent_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2219
        if (((isset($context["t1o_product_save_percent_status"]) ? $context["t1o_product_save_percent_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_save_percent_status"]) ? $context["t1o_product_save_percent_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_25.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Tax:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_tax_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2229
        if (((isset($context["t1o_product_tax_status"]) ? $context["t1o_product_tax_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2230
        if (((isset($context["t1o_product_tax_status"]) ? $context["t1o_product_tax_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_tax_status"]) ? $context["t1o_product_tax_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_26.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show product viewed:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_viewed_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2240
        if (((isset($context["t1o_product_viewed_status"]) ? $context["t1o_product_viewed_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2241
        if (((isset($context["t1o_product_viewed_status"]) ? $context["t1o_product_viewed_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_viewed_status"]) ? $context["t1o_product_viewed_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_27.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Increment/Decrement a Quantity:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_i_c_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2251
        if (((isset($context["t1o_product_i_c_status"]) ? $context["t1o_product_i_c_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2252
        if (((isset($context["t1o_product_i_c_status"]) ? $context["t1o_product_i_c_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_i_c_status"]) ? $context["t1o_product_i_c_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_28.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Related Products</legend>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show related products:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_related_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2264
        if (((isset($context["t1o_product_related_status"]) ? $context["t1o_product_related_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2265
        if (((isset($context["t1o_product_related_status"]) ? $context["t1o_product_related_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_related_status"]) ? $context["t1o_product_related_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Related products position:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_related_position\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2274
        if (((isset($context["t1o_product_related_position"]) ? $context["t1o_product_related_position"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Right</option>
                                    <option value=\"1\"";
        // line 2275
        if (((isset($context["t1o_product_related_position"]) ? $context["t1o_product_related_position"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_related_position"]) ? $context["t1o_product_related_position"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Bottom</option>
\t\t\t\t\t\t\t\t</select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_29.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Related products style:<br /><span class=\"k_help\">only bottom position</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_related_style\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2285
        if (((isset($context["t1o_product_related_style"]) ? $context["t1o_product_related_style"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">Grid</option>
                                    <option value=\"1\"";
        // line 2286
        if (((isset($context["t1o_product_related_style"]) ? $context["t1o_product_related_style"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_product_related_style"]) ? $context["t1o_product_related_style"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Slider</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Feature Box 1<a href=\"view/image/theme_img/help_oxy_theme/go_30.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Custom Icon:<br /><span class=\"k_help\">Recommended dimensions<br>46 x 46px</span></label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1o_product_fb1_icon_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2296
        echo (isset($context["t1o_product_fb1_icon_thumb"]) ? $context["t1o_product_fb1_icon_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_product_fb1_icon\" value=\"";
        // line 2297
        echo (isset($context["t1o_product_fb1_icon"]) ? $context["t1o_product_fb1_icon"] : null);
        echo "\" id=\"t1o_product_fb1_icon\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div> 
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_product_fb1_awesome\" value=\"";
        // line 2304
        echo (isset($context["t1o_product_fb1_awesome"]) ? $context["t1o_product_fb1_awesome"] : null);
        echo "\" class=\"form-control\" />
                                <span class=\"k_help\">Enter the name of an icon, for example: <b>car</b></span>
                                <a href=\"http://fortawesome.github.io/Font-Awesome/icons/\" target=\"_blank\" class=\"link\" style=\"margin-left:5px\">Font Awesome Icon Collection &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2313
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2314
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">    
                                            <span class=\"input-group-addon\"><img src=\"language/";
            // line 2315
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_fb1_title[";
            // line 2316
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_fb1_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_fb1_title"]) ? $context["t1o_product_fb1_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_fb1_title"]) ? $context["t1o_product_fb1_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2319
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subtitle:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2325
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2326
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 2327
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_fb1_subtitle[";
            // line 2328
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_fb1_subtitle_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_fb1_subtitle"]) ? $context["t1o_product_fb1_subtitle"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_fb1_subtitle"]) ? $context["t1o_product_fb1_subtitle"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Subtitle\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2331
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_fb1_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2339
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_fb1_content_";
            // line 2340
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2342
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2345
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_fb1_content_";
            // line 2346
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_product_fb1_content[";
            // line 2347
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_product_fb1_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_product_fb1_content"]) ? $context["t1o_product_fb1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_product_fb1_content"]) ? $context["t1o_product_fb1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2350
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Feature Box 2</legend>
 
                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Custom Icon:<br /><span class=\"k_help\">Recommended dimensions<br>46 x 46px</span></label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1o_product_fb2_icon_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2360
        echo (isset($context["t1o_product_fb2_icon_thumb"]) ? $context["t1o_product_fb2_icon_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_product_fb2_icon\" value=\"";
        // line 2361
        echo (isset($context["t1o_product_fb2_icon"]) ? $context["t1o_product_fb2_icon"] : null);
        echo "\" id=\"t1o_product_fb2_icon\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div> 
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_product_fb2_awesome\" value=\"";
        // line 2368
        echo (isset($context["t1o_product_fb2_awesome"]) ? $context["t1o_product_fb2_awesome"] : null);
        echo "\" class=\"form-control\" />
                                <span class=\"k_help\">Enter the name of an icon, for example: <b>car</b></span>
                                <a href=\"http://fortawesome.github.io/Font-Awesome/icons/\" target=\"_blank\" class=\"link\" style=\"margin-left:5px\">Font Awesome Icon Collection &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2377
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2378
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">    
                                            <span class=\"input-group-addon\"><img src=\"language/";
            // line 2379
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_fb2_title[";
            // line 2380
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_fb2_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_fb2_title"]) ? $context["t1o_product_fb2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_fb2_title"]) ? $context["t1o_product_fb2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2383
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subtitle:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2389
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2390
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 2391
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_fb2_subtitle[";
            // line 2392
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_fb2_subtitle_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_fb2_subtitle"]) ? $context["t1o_product_fb2_subtitle"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_fb2_subtitle"]) ? $context["t1o_product_fb2_subtitle"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Subtitle\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2395
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_fb2_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2403
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_fb2_content_";
            // line 2404
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2406
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2409
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_fb2_content_";
            // line 2410
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_product_fb2_content[";
            // line 2411
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_product_fb2_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_product_fb2_content"]) ? $context["t1o_product_fb2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_product_fb2_content"]) ? $context["t1o_product_fb2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2414
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Feature Box 3</legend>

                        <div class=\"form-group\">
\t\t\t\t\t        <label class=\"col-sm-2 control-label\">Custom Icon:<br /><span class=\"k_help\">Recommended dimensions<br>46 x 46px</span></label>
\t\t\t\t\t        <div class=\"col-sm-10\">                                
                                <a href=\"\" id=\"t1o_product_fb3_icon_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 2424
        echo (isset($context["t1o_product_fb3_icon_thumb"]) ? $context["t1o_product_fb3_icon_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_product_fb3_icon\" value=\"";
        // line 2425
        echo (isset($context["t1o_product_fb3_icon"]) ? $context["t1o_product_fb3_icon"] : null);
        echo "\" id=\"t1o_product_fb3_icon\" />
\t\t\t\t\t        </div>
\t\t\t\t        </div> 
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Font Awesome Icon:</label>
                            <div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_product_fb3_awesome\" value=\"";
        // line 2432
        echo (isset($context["t1o_product_fb3_awesome"]) ? $context["t1o_product_fb3_awesome"] : null);
        echo "\" class=\"form-control\" />
                                <span class=\"k_help\">Enter the name of an icon, for example: <b>car</b></span>
                                <a href=\"http://fortawesome.github.io/Font-Awesome/icons/\" target=\"_blank\" class=\"link\" style=\"margin-left:5px\">Font Awesome Icon Collection &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2441
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2442
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">    
                                            <span class=\"input-group-addon\"><img src=\"language/";
            // line 2443
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_fb3_title[";
            // line 2444
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_fb3_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_fb3_title"]) ? $context["t1o_product_fb3_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_fb3_title"]) ? $context["t1o_product_fb3_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2447
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subtitle:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2453
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2454
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 2455
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_fb3_subtitle[";
            // line 2456
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_fb3_subtitle_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_fb3_subtitle"]) ? $context["t1o_product_fb3_subtitle"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_fb3_subtitle"]) ? $context["t1o_product_fb3_subtitle"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Subtitle\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2459
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_fb3_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2467
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_fb3_content_";
            // line 2468
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2470
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2473
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_fb3_content_";
            // line 2474
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_product_fb3_content[";
            // line 2475
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_product_fb3_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_product_fb3_content"]) ? $context["t1o_product_fb3_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_product_fb3_content"]) ? $context["t1o_product_fb3_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2478
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Custom Block - Under Main Image<a href=\"view/image/theme_img/help_oxy_theme/go_31.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_custom_block_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2489
        if (((isset($context["t1o_product_custom_block_1_status"]) ? $context["t1o_product_custom_block_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 2490
        if (((isset($context["t1o_product_custom_block_1_status"]) ? $context["t1o_product_custom_block_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2498
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2499
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">    
                                            <span class=\"input-group-addon\"><img src=\"language/";
            // line 2500
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_custom_block_1_title[";
            // line 2501
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_custom_block_1_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_custom_block_1_title"]) ? $context["t1o_product_custom_block_1_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_custom_block_1_title"]) ? $context["t1o_product_custom_block_1_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2504
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_custom_block_1_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2512
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_custom_block_1_content_";
            // line 2513
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2515
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2518
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_custom_block_1_content_";
            // line 2519
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_product_custom_block_1_content[";
            // line 2520
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_product_custom_block_1_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_product_custom_block_1_content"]) ? $context["t1o_product_custom_block_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_block_1_content"]) ? $context["t1o_product_custom_block_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2523
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Custom Block - Buy Section<a href=\"view/image/theme_img/help_oxy_theme/go_32.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_custom_block_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2534
        if (((isset($context["t1o_product_custom_block_2_status"]) ? $context["t1o_product_custom_block_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 2535
        if (((isset($context["t1o_product_custom_block_2_status"]) ? $context["t1o_product_custom_block_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2543
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2544
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">    
                                            <span class=\"input-group-addon\"><img src=\"language/";
            // line 2545
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_custom_block_2_title[";
            // line 2546
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_custom_block_2_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_custom_block_2_title"]) ? $context["t1o_product_custom_block_2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_custom_block_2_title"]) ? $context["t1o_product_custom_block_2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2549
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_custom_block_2_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2557
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_custom_block_2_content_";
            // line 2558
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2560
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2563
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_custom_block_2_content_";
            // line 2564
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_product_custom_block_2_content[";
            // line 2565
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_product_custom_block_2_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_product_custom_block_2_content"]) ? $context["t1o_product_custom_block_2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_block_2_content"]) ? $context["t1o_product_custom_block_2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2568
        echo "\t\t\t\t\t\t\t\t</div>
          
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>  
                        
                        <legend>Custom Block - Right Sidebar<a href=\"view/image/theme_img/help_oxy_theme/go_33.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_custom_block_3_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2579
        if (((isset($context["t1o_product_custom_block_3_status"]) ? $context["t1o_product_custom_block_3_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 2580
        if (((isset($context["t1o_product_custom_block_3_status"]) ? $context["t1o_product_custom_block_3_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2588
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2589
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">    
                                            <span class=\"input-group-addon\"><img src=\"language/";
            // line 2590
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_custom_block_3_title[";
            // line 2591
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_product_custom_block_3_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_product_custom_block_3_title"]) ? $context["t1o_product_custom_block_3_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_product_custom_block_3_title"]) ? $context["t1o_product_custom_block_3_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2594
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_custom_block_3_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2602
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_custom_block_3_content_";
            // line 2603
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2605
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2608
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_custom_block_3_content_";
            // line 2609
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_product_custom_block_3_content[";
            // line 2610
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_product_custom_block_3_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_product_custom_block_3_content"]) ? $context["t1o_product_custom_block_3_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_block_3_content"]) ? $context["t1o_product_custom_block_3_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2613
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        ";
        // line 2618
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 2619
            echo "                        
                        <legend>Custom Tab ";
            // line 2620
            echo $context["i"];
            echo "<a href=\"view/image/theme_img/help_oxy_theme/go_34.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Tab ";
            // line 2623
            echo $context["i"];
            echo ":</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_product_custom_tab[";
            // line 2625
            echo $context["i"];
            echo "][status]\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
            // line 2626
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_tab"]) ? $context["t1o_product_custom_tab"] : null), $context["i"], array(), "array"), "status", array()) == "0")) {
                echo "selected=\"selected\"";
            }
            echo ">No</option> 
                                    <option value=\"1\"";
            // line 2627
            if (($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_tab"]) ? $context["t1o_product_custom_tab"] : null), $context["i"], array(), "array"), "status", array()) == "1")) {
                echo "selected=\"selected\"";
            }
            echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
            // line 2635
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                // line 2636
                echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
                // line 2637
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" title=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_product_custom_tab[";
                // line 2638
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][title]\" id=\"t1o_product_custom_tab_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_title\" value=\"";
                echo (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_tab"]) ? $context["t1o_product_custom_tab"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_tab"]) ? $context["t1o_product_custom_tab"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "title", array())) : (""));
                echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2641
            echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_product_custom_tab_";
            // line 2648
            echo $context["i"];
            echo "\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
            // line 2649
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_product_custom_tab_";
                // line 2650
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "\" data-toggle=\"tab\"><img src=\"language/";
                echo $this->getAttribute($context["language"], "code", array());
                echo "/";
                echo $this->getAttribute($context["language"], "code", array());
                echo ".png\" alt=\"";
                echo $this->getAttribute($context["language"], "name", array());
                echo "\" width=\"16px\" height=\"11px\" /> ";
                echo $this->getAttribute($context["language"], "name", array());
                echo "</a></li>
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2652
            echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            // line 2655
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
                echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_product_custom_tab_";
                // line 2656
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "\" class=\"tab-pane\">
                                            <textarea name=\"t1o_product_custom_tab[";
                // line 2657
                echo $context["i"];
                echo "][";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "][htmlcontent]\" id=\"t1o_product_custom_tab_";
                echo $context["i"];
                echo "_";
                echo $this->getAttribute($context["language"], "language_id", array());
                echo "_content\">";
                echo (($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_tab"]) ? $context["t1o_product_custom_tab"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_product_custom_tab"]) ? $context["t1o_product_custom_tab"] : null), $context["i"], array(), "array"), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
                echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t\t
\t\t\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 2660
            echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2666
        echo "
\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-midsection-contact\" class=\"tab-pane\">  
        
                    <fieldset>
                        
                        <legend>Google Map <a href=\"view/image/theme_img/help_oxy_theme/go_35.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Google Map:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_contact_map_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"0\"";
        // line 2681
        if (((isset($context["t1o_contact_map_status"]) ? $context["t1o_contact_map_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2682
        if (((isset($context["t1o_contact_map_status"]) ? $context["t1o_contact_map_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Latitude, Longitude:<br /><span class=\"k_help\">For example:<br />51.5224954,-0.1720996</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_contact_map_ll\" value=\"";
        // line 2690
        echo (isset($context["t1o_contact_map_ll"]) ? $context["t1o_contact_map_ll"] : null);
        echo "\" class=\"form-control\" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href=\"http://itouchmap.com/latlong.html\" target=\"_blank\" class=\"link\">How to find Latitude and Longitude?</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Your Google Maps API key:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_contact_map_api\" value=\"";
        // line 2698
        echo (isset($context["t1o_contact_map_api"]) ? $context["t1o_contact_map_api"] : null);
        echo "\" class=\"form-control\" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href=\"https://developers.google.com/maps/documentation/javascript/get-api-key\" target=\"_blank\" class=\"link\">How to get your Google Maps API key?</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Map type:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_contact_map_type\" class=\"form-control\">
\t\t\t\t\t\t\t\t\t<option value=\"ROADMAP\"";
        // line 2707
        if (((isset($context["t1o_contact_map_type"]) ? $context["t1o_contact_map_type"] : null) == "ROADMAP")) {
            echo "selected=\"selected\"";
        }
        echo ">ROADMAP</option>
                            \t\t<option value=\"SATELLITE\"";
        // line 2708
        if (((isset($context["t1o_contact_map_type"]) ? $context["t1o_contact_map_type"] : null) == "SATELLITE")) {
            echo "selected=\"selected\"";
        }
        echo ">SATELLITE</option>
                            \t\t<option value=\"HYBRID\"";
        // line 2709
        if (((isset($context["t1o_contact_map_type"]) ? $context["t1o_contact_map_type"] : null) == "HYBRID")) {
            echo "selected=\"selected\"";
        }
        echo ">HYBRID</option>
                            \t\t<option value=\"TERRAIN\"";
        // line 2710
        if (((isset($context["t1o_contact_map_type"]) ? $context["t1o_contact_map_type"] : null) == "TERRAIN")) {
            echo "selected=\"selected\"";
        }
        echo ">TERRAIN</option> 
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-midsection-lf\" class=\"tab-pane\">  
        
                    <fieldset> 
        
                        <legend>Categories</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Categories display type:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_left_right_column_categories_type\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2729
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_opencart"]) ? $context["text_opencart"] : null);
        echo "</option> 
                                    <option value=\"1\"";
        // line 2730
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">";
        echo (isset($context["text_accordion"]) ? $context["text_accordion"] : null);
        echo "</option>  
                                    <option value=\"2\"";
        // line 2731
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">Dropdown</option> 
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_36.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
        
                    </fieldset>      
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        <div class=\"tab-pane\" id=\"tab-footer\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"footer_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-footer-top-custom-1\" data-toggle=\"tab\">Top Custom Block</a></li>
             <li><a href=\"#tab-footer-custom-column-1\" data-toggle=\"tab\">Custom Column 1</a></li>
             <li><a href=\"#tab-footer-information\" data-toggle=\"tab\">Information Block</a></li>
             <li><a href=\"#tab-footer-custom-column-2\" data-toggle=\"tab\">Custom Column 2 / Newsletter</a></li>
             <li><a href=\"#tab-footer-payment\" data-toggle=\"tab\">Payment Images</a></li>
             <li><a href=\"#tab-footer-powered\" data-toggle=\"tab\">Powered by</a></li>
             <li><a href=\"#tab-footer-follow\" data-toggle=\"tab\">Follow us</a></li>
             <li><a href=\"#tab-footer-bottom-custom-1\" data-toggle=\"tab\">Bottom Custom Block</a></li>
             <li><a href=\"#tab-footer-bottom-custom-2\" data-toggle=\"tab\">Sliding Bottom Custom Block</a></li>                                  
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-footer-top-custom-1\" class=\"tab-pane fade in active\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Top Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_top_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2779
        if (((isset($context["t1o_custom_top_1_status"]) ? $context["t1o_custom_top_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2780
        if (((isset($context["t1o_custom_top_1_status"]) ? $context["t1o_custom_top_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_46.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_top_1_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2790
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_top_1_content_";
            // line 2791
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2793
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2796
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_top_1_content_";
            // line 2797
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_top_1_content[";
            // line 2798
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_top_1_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_top_1_content"]) ? $context["t1o_custom_top_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_top_1_content"]) ? $context["t1o_custom_top_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2801
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                    
                    </fieldset>        
        
        </div>
        
        <div id=\"tab-footer-custom-column-1\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Column 1:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2818
        if (((isset($context["t1o_custom_1_status"]) ? $context["t1o_custom_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2819
        if (((isset($context["t1o_custom_1_status"]) ? $context["t1o_custom_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_41.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Custom Column 1 width:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_1_column_width\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 2829
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1/12</option>
                                    <option value=\"2\"";
        // line 2830
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2/12</option>
                                    <option value=\"3\"";
        // line 2831
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">3/12</option>
                                    <option value=\"4\"";
        // line 2832
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4/12</option>
                                    <option value=\"5\"";
        // line 2833
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5/12</option>
                                    <option value=\"6\"";
        // line 2834
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">6/12</option>
                                    <option value=\"7\"";
        // line 2835
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">7/12</option>
                                    <option value=\"8\"";
        // line 2836
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">8/12</option>
                                    <option value=\"9\"";
        // line 2837
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "9")) {
            echo "selected=\"selected\"";
        }
        echo ">9/12</option>
                                    <option value=\"10\"";
        // line 2838
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10/12</option>
                                    <option value=\"11\"";
        // line 2839
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11/12</option>
                                    <option value=\"12\"";
        // line 2840
        if (((isset($context["t1o_custom_1_column_width"]) ? $context["t1o_custom_1_column_width"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12/12</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 2848
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2849
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 2850
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_custom_1_title[";
            // line 2851
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_custom_1_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_custom_1_title"]) ? $context["t1o_custom_1_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_custom_1_title"]) ? $context["t1o_custom_1_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2854
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_1_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 2862
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 2863
            echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_1_content_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2865
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 2868
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_1_content_";
            // line 2869
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_1_content[";
            // line 2870
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_1_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_1_content"]) ? $context["t1o_custom_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_1_content"]) ? $context["t1o_custom_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 2873
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-footer-information\" class=\"tab-pane\">  
        
                    <fieldset>
                    
                        <legend>Information Block<a href=\"view/image/theme_img/help_oxy_theme/go_38.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Information Block width:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_information_block_width\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 2892
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1/12</option>
                                    <option value=\"2\"";
        // line 2893
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2/12</option>
                                    <option value=\"3\"";
        // line 2894
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3/12</option>
                                    <option value=\"4\"";
        // line 2895
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4/12</option>
                                    <option value=\"5\"";
        // line 2896
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5/12</option>
                                    <option value=\"6\"";
        // line 2897
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">6/12</option>
                                    <option value=\"7\"";
        // line 2898
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">7/12</option>
                                    <option value=\"8\"";
        // line 2899
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">8/12</option>
                                    <option value=\"9\"";
        // line 2900
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "9")) {
            echo "selected=\"selected\"";
        }
        echo ">9/12</option>
                                    <option value=\"10\"";
        // line 2901
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10/12</option>
                                    <option value=\"11\"";
        // line 2902
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11/12</option>
                                    <option value=\"12\"";
        // line 2903
        if (((isset($context["t1o_information_block_width"]) ? $context["t1o_information_block_width"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12/12</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<legend>Information Column<a href=\"view/image/theme_img/help_oxy_theme/go_38.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Information Column:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_information_column_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2914
        if (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2915
        if (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_information_column_1_status"]) ? $context["t1o_information_column_1_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <span class=\"k_help\">To disable the default OpenCart links, like <b>About Us</b>, <b>Delivery Information</b> or <b>Privacy Policy</b>, go to OpenCart Admin > Catalog > Information.</span>
                        
                        
                        <legend>Customer Service Column<a href=\"view/image/theme_img/help_oxy_theme/go_39.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Customer Service Column:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_information_column_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2928
        if (((isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2929
        if (((isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_information_column_2_status"]) ? $context["t1o_information_column_2_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Contact Us link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_2_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2938
        if (((isset($context["t1o_i_c_2_1_status"]) ? $context["t1o_i_c_2_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2939
        if (((isset($context["t1o_i_c_2_1_status"]) ? $context["t1o_i_c_2_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_2_1_status"]) ? $context["t1o_i_c_2_1_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">My Account link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_2_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2947
        if (((isset($context["t1o_i_c_2_2_status"]) ? $context["t1o_i_c_2_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2948
        if (((isset($context["t1o_i_c_2_2_status"]) ? $context["t1o_i_c_2_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_2_2_status"]) ? $context["t1o_i_c_2_2_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Returns link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_2_3_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2956
        if (((isset($context["t1o_i_c_2_3_status"]) ? $context["t1o_i_c_2_3_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2957
        if (((isset($context["t1o_i_c_2_3_status"]) ? $context["t1o_i_c_2_3_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_2_3_status"]) ? $context["t1o_i_c_2_3_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Order History link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_2_4_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2965
        if (((isset($context["t1o_i_c_2_4_status"]) ? $context["t1o_i_c_2_4_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2966
        if (((isset($context["t1o_i_c_2_4_status"]) ? $context["t1o_i_c_2_4_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_2_4_status"]) ? $context["t1o_i_c_2_4_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Wish List link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_2_5_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2974
        if (((isset($context["t1o_i_c_2_5_status"]) ? $context["t1o_i_c_2_5_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2975
        if (((isset($context["t1o_i_c_2_5_status"]) ? $context["t1o_i_c_2_5_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_2_5_status"]) ? $context["t1o_i_c_2_5_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <legend>Extras Column<a href=\"view/image/theme_img/help_oxy_theme/go_40.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a></legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Extras Column:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_information_column_3_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2986
        if (((isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2987
        if (((isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_information_column_3_status"]) ? $context["t1o_information_column_3_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Brands link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_3_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 2995
        if (((isset($context["t1o_i_c_3_1_status"]) ? $context["t1o_i_c_3_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 2996
        if (((isset($context["t1o_i_c_3_1_status"]) ? $context["t1o_i_c_3_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_3_1_status"]) ? $context["t1o_i_c_3_1_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Gift Vouchers link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_3_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3004
        if (((isset($context["t1o_i_c_3_2_status"]) ? $context["t1o_i_c_3_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3005
        if (((isset($context["t1o_i_c_3_2_status"]) ? $context["t1o_i_c_3_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_3_2_status"]) ? $context["t1o_i_c_3_2_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Affiliates link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_3_3_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3013
        if (((isset($context["t1o_i_c_3_3_status"]) ? $context["t1o_i_c_3_3_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3014
        if (((isset($context["t1o_i_c_3_3_status"]) ? $context["t1o_i_c_3_3_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_3_3_status"]) ? $context["t1o_i_c_3_3_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Specials link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_3_4_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3022
        if (((isset($context["t1o_i_c_3_4_status"]) ? $context["t1o_i_c_3_4_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3023
        if (((isset($context["t1o_i_c_3_4_status"]) ? $context["t1o_i_c_3_4_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_3_4_status"]) ? $context["t1o_i_c_3_4_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Newsletter link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_3_5_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3031
        if (((isset($context["t1o_i_c_3_5_status"]) ? $context["t1o_i_c_3_5_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3032
        if (((isset($context["t1o_i_c_3_5_status"]) ? $context["t1o_i_c_3_5_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_3_5_status"]) ? $context["t1o_i_c_3_5_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Site Map link:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_i_c_3_6_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3040
        if (((isset($context["t1o_i_c_3_6_status"]) ? $context["t1o_i_c_3_6_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3041
        if (((isset($context["t1o_i_c_3_6_status"]) ? $context["t1o_i_c_3_6_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_i_c_3_6_status"]) ? $context["t1o_i_c_3_6_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-footer-custom-column-2\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Column 2 / Newsletter:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3058
        if (((isset($context["t1o_custom_2_status"]) ? $context["t1o_custom_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3059
        if (((isset($context["t1o_custom_2_status"]) ? $context["t1o_custom_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_41.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Custom Column 2 / Newsletter width:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_2_column_width\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 3069
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1/12</option>
                                    <option value=\"2\"";
        // line 3070
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        echo ">2/12</option>
                                    <option value=\"3\"";
        // line 3071
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">3/12</option>
                                    <option value=\"4\"";
        // line 3072
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "4")) {
            echo "selected=\"selected\"";
        }
        echo ">4/12</option>
                                    <option value=\"5\"";
        // line 3073
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "5")) {
            echo "selected=\"selected\"";
        }
        echo ">5/12</option>
                                    <option value=\"6\"";
        // line 3074
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "6")) {
            echo "selected=\"selected\"";
        }
        echo ">6/12</option>
                                    <option value=\"7\"";
        // line 3075
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "7")) {
            echo "selected=\"selected\"";
        }
        echo ">7/12</option>
                                    <option value=\"8\"";
        // line 3076
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "8")) {
            echo "selected=\"selected\"";
        }
        echo ">8/12</option>
                                    <option value=\"9\"";
        // line 3077
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "9")) {
            echo "selected=\"selected\"";
        }
        echo ">9/12</option>
                                    <option value=\"10\"";
        // line 3078
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "10")) {
            echo "selected=\"selected\"";
        }
        echo ">10/12</option>
                                    <option value=\"11\"";
        // line 3079
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "11")) {
            echo "selected=\"selected\"";
        }
        echo ">11/12</option>
                                    <option value=\"12\"";
        // line 3080
        if (((isset($context["t1o_custom_2_column_width"]) ? $context["t1o_custom_2_column_width"] : null) == "12")) {
            echo "selected=\"selected\"";
        }
        echo ">12/12</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 3088
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 3089
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 3090
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_custom_2_title[";
            // line 3091
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_custom_2_title_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_custom_2_title"]) ? $context["t1o_custom_2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_custom_2_title"]) ? $context["t1o_custom_2_title"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Title\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3094
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Newsletter Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_newsletter_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3101
        if (((isset($context["t1o_newsletter_status"]) ? $context["t1o_newsletter_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3102
        if (((isset($context["t1o_newsletter_status"]) ? $context["t1o_newsletter_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_41.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Your MailChimp Campaign URL:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                <input type=\"text\" name=\"t1o_newsletter_campaign_url\" value=\"";
        // line 3111
        echo (isset($context["t1o_newsletter_campaign_url"]) ? $context["t1o_newsletter_campaign_url"] : null);
        echo "\" class=\"form-control\" style=\"width: 100%;\" />
                                <br /><span class=\"k_help\"><a href=\"https://mailchimp.com/\" target=\"_blank\" class=\"link\">MailChimp Website &raquo;</a></span>
                                <span class=\"k_help\"><a href=\"http://kb.mailchimp.com/\" target=\"_blank\" class=\"link\">MailChimp Knowledge Base &raquo;</a></span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Newsletter Promo text:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 3120
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 3121
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 3122
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_newsletter_promo_text[";
            // line 3123
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_newsletter_promo_text_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_newsletter_promo_text"]) ? $context["t1o_newsletter_promo_text"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_newsletter_promo_text"]) ? $context["t1o_newsletter_promo_text"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Newsletter Promo text\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3126
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">\"Your email address\" text:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 3132
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 3133
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 3134
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_newsletter_email[";
            // line 3135
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_newsletter_email_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_newsletter_email"]) ? $context["t1o_newsletter_email"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_newsletter_email"]) ? $context["t1o_newsletter_email"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Your email address\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3138
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">\"Subscribe\" text:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 3144
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 3145
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 3146
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_newsletter_subscribe[";
            // line 3147
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_newsletter_subscribe_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_newsletter_subscribe"]) ? $context["t1o_newsletter_subscribe"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_newsletter_subscribe"]) ? $context["t1o_newsletter_subscribe"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Subscribe\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3150
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Custom Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_2_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 3158
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 3159
            echo "\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_2_content_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3161
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 3164
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_2_content_";
            // line 3165
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_2_content[";
            // line 3166
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_2_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_2_content"]) ? $context["t1o_custom_2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_2_content"]) ? $context["t1o_custom_2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3169
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-footer-payment\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<legend class=\"bn\"></legend>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show payment images:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_block_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3188
        if (((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3189
        if (((isset($context["t1o_payment_block_status"]) ? $context["t1o_payment_block_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_45.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                        <legend>Custom payment image</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_block_custom_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3201
        if (((isset($context["t1o_payment_block_custom_status"]) ? $context["t1o_payment_block_custom_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3202
        if (((isset($context["t1o_payment_block_custom_status"]) ? $context["t1o_payment_block_custom_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Upload your payment image:<br /><span class=\"k_help\">Recommended height: 36px</span></label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<a href=\"\" id=\"t1o_payment_block_custom_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 3209
        echo (isset($context["t1o_payment_block_custom_thumb"]) ? $context["t1o_payment_block_custom_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_payment_block_custom\" value=\"";
        // line 3210
        echo (isset($context["t1o_payment_block_custom"]) ? $context["t1o_payment_block_custom"] : null);
        echo "\" id=\"t1o_payment_block_custom\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">url:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                <input type=\"text\" name=\"t1o_payment_block_custom_url\" value=\"";
        // line 3216
        echo (isset($context["t1o_payment_block_custom_url"]) ? $context["t1o_payment_block_custom_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>  
                        
                        <legend>OXY Theme payment images:</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">PayPal:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_paypal\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3226
        if (((isset($context["t1o_payment_paypal"]) ? $context["t1o_payment_paypal"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3227
        if (((isset($context["t1o_payment_paypal"]) ? $context["t1o_payment_paypal"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_paypal-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_paypal_url\" value=\"";
        // line 3233
        echo (isset($context["t1o_payment_paypal_url"]) ? $context["t1o_payment_paypal_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Visa:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_visa\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3240
        if (((isset($context["t1o_payment_visa"]) ? $context["t1o_payment_visa"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3241
        if (((isset($context["t1o_payment_visa"]) ? $context["t1o_payment_visa"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_visa-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_visa_url\" value=\"";
        // line 3247
        echo (isset($context["t1o_payment_visa_url"]) ? $context["t1o_payment_visa_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">MasterCard:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_mastercard\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3254
        if (((isset($context["t1o_payment_mastercard"]) ? $context["t1o_payment_mastercard"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3255
        if (((isset($context["t1o_payment_mastercard"]) ? $context["t1o_payment_mastercard"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_mastercard-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_mastercard_url\" value=\"";
        // line 3261
        echo (isset($context["t1o_payment_mastercard_url"]) ? $context["t1o_payment_mastercard_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Maestro:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_maestro\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3268
        if (((isset($context["t1o_payment_maestro"]) ? $context["t1o_payment_maestro"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3269
        if (((isset($context["t1o_payment_maestro"]) ? $context["t1o_payment_maestro"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_maestro-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_maestro_url\" value=\"";
        // line 3275
        echo (isset($context["t1o_payment_maestro_url"]) ? $context["t1o_payment_maestro_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Discover:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_discover\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3282
        if (((isset($context["t1o_payment_discover"]) ? $context["t1o_payment_discover"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3283
        if (((isset($context["t1o_payment_discover"]) ? $context["t1o_payment_discover"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_discover-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_discover_url\" value=\"";
        // line 3289
        echo (isset($context["t1o_payment_discover_url"]) ? $context["t1o_payment_discover_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Skrill:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_skrill\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3296
        if (((isset($context["t1o_payment_skrill"]) ? $context["t1o_payment_skrill"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3297
        if (((isset($context["t1o_payment_skrill"]) ? $context["t1o_payment_skrill"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_skrill-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_skrill_url\" value=\"";
        // line 3303
        echo (isset($context["t1o_payment_skrill_url"]) ? $context["t1o_payment_skrill_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">American Express:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_american_express\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3310
        if (((isset($context["t1o_payment_american_express"]) ? $context["t1o_payment_american_express"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3311
        if (((isset($context["t1o_payment_american_express"]) ? $context["t1o_payment_american_express"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_american_express-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_american_express_url\" value=\"";
        // line 3317
        echo (isset($context["t1o_payment_american_express_url"]) ? $context["t1o_payment_american_express_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Cirrus:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_cirrus\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3324
        if (((isset($context["t1o_payment_cirrus"]) ? $context["t1o_payment_cirrus"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3325
        if (((isset($context["t1o_payment_cirrus"]) ? $context["t1o_payment_cirrus"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_cirrus-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_cirrus_url\" value=\"";
        // line 3331
        echo (isset($context["t1o_payment_cirrus_url"]) ? $context["t1o_payment_cirrus_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Delta:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_delta\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3338
        if (((isset($context["t1o_payment_delta"]) ? $context["t1o_payment_delta"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 

                                    <option value=\"1\"";
        // line 3340
        if (((isset($context["t1o_payment_delta"]) ? $context["t1o_payment_delta"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_delta-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_delta_url\" value=\"";
        // line 3346
        echo (isset($context["t1o_payment_delta_url"]) ? $context["t1o_payment_delta_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Google:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_google\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3353
        if (((isset($context["t1o_payment_google"]) ? $context["t1o_payment_google"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3354
        if (((isset($context["t1o_payment_google"]) ? $context["t1o_payment_google"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_google-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_google_url\" value=\"";
        // line 3360
        echo (isset($context["t1o_payment_google_url"]) ? $context["t1o_payment_google_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">2CheckOut:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_2co\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3367
        if (((isset($context["t1o_payment_2co"]) ? $context["t1o_payment_2co"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3368
        if (((isset($context["t1o_payment_2co"]) ? $context["t1o_payment_2co"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_2co-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_2co_url\" value=\"";
        // line 3374
        echo (isset($context["t1o_payment_2co_url"]) ? $context["t1o_payment_2co_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Sage:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_sage\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3381
        if (((isset($context["t1o_payment_sage"]) ? $context["t1o_payment_sage"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3382
        if (((isset($context["t1o_payment_sage"]) ? $context["t1o_payment_sage"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_sage-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_sage_url\" value=\"";
        // line 3388
        echo (isset($context["t1o_payment_sage_url"]) ? $context["t1o_payment_sage_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Solo:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_solo\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3395
        if (((isset($context["t1o_payment_solo"]) ? $context["t1o_payment_solo"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3396
        if (((isset($context["t1o_payment_solo"]) ? $context["t1o_payment_solo"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_solo-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_solo_url\" value=\"";
        // line 3402
        echo (isset($context["t1o_payment_solo_url"]) ? $context["t1o_payment_solo_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Amazon Payments:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_amazon\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3409
        if (((isset($context["t1o_payment_amazon"]) ? $context["t1o_payment_amazon"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3410
        if (((isset($context["t1o_payment_amazon"]) ? $context["t1o_payment_amazon"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_amazon-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_amazon_url\" value=\"";
        // line 3416
        echo (isset($context["t1o_payment_amazon_url"]) ? $context["t1o_payment_amazon_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Western Union:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-2\">
\t\t\t\t\t\t\t\t<select name=\"t1o_payment_western_union\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3423
        if (((isset($context["t1o_payment_western_union"]) ? $context["t1o_payment_western_union"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option> 
                                    <option value=\"1\"";
        // line 3424
        if (((isset($context["t1o_payment_western_union"]) ? $context["t1o_payment_western_union"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>      
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
                            <div class=\"col-sm-1\"><img src=\"../catalog/view/theme/oxy/image/payment/payment_image_western_union-1.png\"></div>
                            <div class=\"col-sm-1 control-label\">url:</div>
                            <div class=\"col-sm-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_payment_western_union_url\" value=\"";
        // line 3430
        echo (isset($context["t1o_payment_western_union_url"]) ? $context["t1o_payment_western_union_url"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-footer-powered\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show powered by:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_powered_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3446
        if (((isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3447
        if (((isset($context["t1o_powered_status"]) ? $context["t1o_powered_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_42.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_powered_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 3457
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_powered_content_";
            // line 3458
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3460
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 3463
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_powered_content_";
            // line 3464
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_powered_content[";
            // line 3465
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_powered_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_powered_content"]) ? $context["t1o_powered_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_powered_content"]) ? $context["t1o_powered_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3468
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-footer-follow\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<legend>Follow Us</legend>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Follow Us Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_follow_us_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3487
        if (((isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3488
        if (((isset($context["t1o_follow_us_status"]) ? $context["t1o_follow_us_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_44.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Facebook:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_facebook\" value=\"";
        // line 3496
        echo (isset($context["t1o_facebook"]) ? $context["t1o_facebook"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Twitter:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_twitter\" value=\"";
        // line 3502
        echo (isset($context["t1o_twitter"]) ? $context["t1o_twitter"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Google+:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_googleplus\" value=\"";
        // line 3508
        echo (isset($context["t1o_googleplus"]) ? $context["t1o_googleplus"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">RSS:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_rss\" value=\"";
        // line 3514
        echo (isset($context["t1o_rss"]) ? $context["t1o_rss"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Pinterest:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_pinterest\" value=\"";
        // line 3520
        echo (isset($context["t1o_pinterest"]) ? $context["t1o_pinterest"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Vimeo:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_vimeo\" value=\"";
        // line 3526
        echo (isset($context["t1o_vimeo"]) ? $context["t1o_vimeo"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Flickr:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_flickr\" value=\"";
        // line 3532
        echo (isset($context["t1o_flickr"]) ? $context["t1o_flickr"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">LinkedIn:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_linkedin\" value=\"";
        // line 3538
        echo (isset($context["t1o_linkedin"]) ? $context["t1o_linkedin"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">YouTube:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_youtube\" value=\"";
        // line 3544
        echo (isset($context["t1o_youtube"]) ? $context["t1o_youtube"] : null);
        echo "\" class=\"form-control\" />

\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Dribbble:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_dribbble\" value=\"";
        // line 3551
        echo (isset($context["t1o_dribbble"]) ? $context["t1o_dribbble"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Instagram:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_instagram\" value=\"";
        // line 3557
        echo (isset($context["t1o_instagram"]) ? $context["t1o_instagram"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Behance:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_behance\" value=\"";
        // line 3563
        echo (isset($context["t1o_behance"]) ? $context["t1o_behance"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Skype username:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_skype\" value=\"";
        // line 3569
        echo (isset($context["t1o_skype"]) ? $context["t1o_skype"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Tumblr:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_tumblr\" value=\"";
        // line 3575
        echo (isset($context["t1o_tumblr"]) ? $context["t1o_tumblr"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Reddit:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_reddit\" value=\"";
        // line 3581
        echo (isset($context["t1o_reddit"]) ? $context["t1o_reddit"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">VK:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_vk\" value=\"";
        // line 3587
        echo (isset($context["t1o_vk"]) ? $context["t1o_vk"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
        
        <div id=\"tab-footer-bottom-custom-1\" class=\"tab-pane\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Bottom Custom Block:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_bottom_1_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3603
        if (((isset($context["t1o_custom_bottom_1_status"]) ? $context["t1o_custom_bottom_1_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3604
        if (((isset($context["t1o_custom_bottom_1_status"]) ? $context["t1o_custom_bottom_1_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_46.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_bottom_1_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 3614
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_bottom_1_content_";
            // line 3615
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3617
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 3620
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_bottom_1_content_";
            // line 3621
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_bottom_1_content[";
            // line 3622
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_bottom_1_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_bottom_1_content"]) ? $context["t1o_custom_bottom_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_bottom_1_content"]) ? $context["t1o_custom_bottom_1_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3625
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                    
                    </fieldset>        
        
        </div>
        
        <div id=\"tab-footer-bottom-custom-2\" class=\"tab-pane\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Sliding Bottom Custom Block:
                            <br /><span class=\"k_help\">for \"Full Width\" layout style</span>
                            </label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_bottom_2_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3644
        if (((isset($context["t1o_custom_bottom_2_status"]) ? $context["t1o_custom_bottom_2_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3645
        if (((isset($context["t1o_custom_bottom_2_status"]) ? $context["t1o_custom_bottom_2_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_46.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_bottom_2_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 3655
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_bottom_2_content_";
            // line 3656
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3658
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 3661
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_bottom_2_content_";
            // line 3662
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_bottom_2_content[";
            // line 3663
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_bottom_2_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_bottom_2_content"]) ? $context["t1o_custom_bottom_2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_bottom_2_content"]) ? $context["t1o_custom_bottom_2_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3666
        echo "\t\t\t\t\t\t\t\t</div>
                                <span class=\"k_help\">To add background image or pattern, go to <b>OXY Theme Settings - Design > Background Images > Footer > Sliding Bottom Custom Block</b>.</span>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Height:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_bottom_2_footer_margin\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"50\"";
        // line 3676
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "50")) {
            echo "selected=\"selected\"";
        }
        echo ">50px</option>
                                    <option value=\"100\"";
        // line 3677
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "100")) {
            echo "selected=\"selected\"";
        }
        echo ">100px</option>
                                    <option value=\"150\"";
        // line 3678
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "150")) {
            echo "selected=\"selected\"";
        }
        echo ">150px</option>
                                    <option value=\"200\"";
        // line 3679
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "200")) {
            echo "selected=\"selected\"";
        }
        echo ">200px</option>
                                    <option value=\"250\"";
        // line 3680
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "250")) {
            echo "selected=\"selected\"";
        }
        echo ">250px</option>
                                    <option value=\"300\"";
        // line 3681
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "300")) {
            echo "selected=\"selected\"";
        }
        echo ">300px</option>
                                    <option value=\"350\"";
        // line 3682
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "350")) {
            echo "selected=\"selected\"";
        }
        echo ">350px</option>
                                    <option value=\"400\"";
        // line 3683
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "400")) {
            echo "selected=\"selected\"";
        }
        echo ">400px</option>
                                    <option value=\"450\"";
        // line 3684
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "450")) {
            echo "selected=\"selected\"";
        }
        echo ">450px</option>
                                    <option value=\"500\"";
        // line 3685
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "500")) {
            echo "selected=\"selected\"";
        }
        echo ">500px</option>
                                    <option value=\"550\"";
        // line 3686
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "550")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">550px</option>
                                    <option value=\"600\"";
        // line 3687
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "600")) {
            echo "selected=\"selected\"";
        }
        echo ">600px</option>
                                    <option value=\"650\"";
        // line 3688
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "650")) {
            echo "selected=\"selected\"";
        }
        echo ">650px</option>
                                    <option value=\"700\"";
        // line 3689
        if (((isset($context["t1o_custom_bottom_2_footer_margin"]) ? $context["t1o_custom_bottom_2_footer_margin"] : null) == "700")) {
            echo "selected=\"selected\"";
        }
        echo ">700px</option>
\t\t\t\t\t\t\t    </select>
                                <a href=\"view/image/theme_img/help_oxy_theme/go_46.jpg\" target=\"_blank\"><span class=\"k_help_tip\">?</span></a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                    
                    </fieldset> 
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        <div class=\"tab-pane\" id=\"tab-widgets\"> 
        <div class=\"row form-horizontal\">  
        
        <div class=\"col-sm-2\">    
        <ul id=\"widgets_tabs\" class=\"nav nav-pills nav-stacked\">
             <li class=\"active\"><a href=\"#tab-widgets-facebook\" data-toggle=\"tab\">Facebook Widget</a></li>
             <li><a href=\"#tab-widgets-twitter\" data-toggle=\"tab\">Twitter Widget</a></li>
             <li><a href=\"#tab-widgets-googleplus\" data-toggle=\"tab\">Google+ Widget</a></li>
             <li><a href=\"#tab-widgets-pinterest\" data-toggle=\"tab\">Pinterest Widget</a></li>
             <li><a href=\"#tab-widgets-snapchat\" data-toggle=\"tab\">Snapchat Widget</a></li>
             <li><a href=\"#tab-widgets-video-box\" data-toggle=\"tab\">Video Box</a></li>
             <li><a href=\"#tab-widgets-custom-box\" data-toggle=\"tab\">Custom Content Box</a></li>
             <li><a href=\"#tab-widgets-cookie\" data-toggle=\"tab\">EU Cookie Message Widget</a></li>
        </ul> 
        </div>
        
        <div class=\"col-sm-10\">
        <div class=\"tab-content\">
        
        <div id=\"tab-widgets-facebook\" class=\"tab-pane fade in active\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Facebook Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_facebook_likebox_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3736
        if (((isset($context["t1o_facebook_likebox_status"]) ? $context["t1o_facebook_likebox_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3737
        if (((isset($context["t1o_facebook_likebox_status"]) ? $context["t1o_facebook_likebox_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Facebook FanPage ID:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_facebook_likebox_id\" value=\"";
        // line 3744
        echo (isset($context["t1o_facebook_likebox_id"]) ? $context["t1o_facebook_likebox_id"] : null);
        echo "\" class=\"form-control\" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href=\"http://findmyfacebookid.com/\" target=\"_blank\" class=\"link\">Find your Facebook ID &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>          

        </div>
 
        <div id=\"tab-widgets-twitter\" class=\"tab-pane\"> 
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Twitter Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_twitter_block_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3761
        if (((isset($context["t1o_twitter_block_status"]) ? $context["t1o_twitter_block_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3762
        if (((isset($context["t1o_twitter_block_status"]) ? $context["t1o_twitter_block_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Twitter username:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_twitter_block_user\" value=\"";
        // line 3769
        echo (isset($context["t1o_twitter_block_user"]) ? $context["t1o_twitter_block_user"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Widget ID:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_twitter_block_widget_id\" value=\"";
        // line 3775
        echo (isset($context["t1o_twitter_block_widget_id"]) ? $context["t1o_twitter_block_widget_id"] : null);
        echo "\" class=\"form-control\" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <a href=\"http://321cart.com/oxy/documentation/assets/images/screen_14.png\" target=\"_blank\" class=\"link\">Find your Widget ID &raquo;</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Tweet limit:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_twitter_block_tweets\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"1\"";
        // line 3783
        if (((isset($context["t1o_twitter_block_tweets"]) ? $context["t1o_twitter_block_tweets"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">1</option>
                                    <option value=\"2\"";
        // line 3784
        if (((isset($context["t1o_twitter_block_tweets"]) ? $context["t1o_twitter_block_tweets"] : null) == "2")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_twitter_block_tweets"]) ? $context["t1o_twitter_block_tweets"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">2</option>
                                    <option value=\"3\"";
        // line 3785
        if (((isset($context["t1o_twitter_block_tweets"]) ? $context["t1o_twitter_block_tweets"] : null) == "3")) {
            echo "selected=\"selected\"";
        }
        echo ">3</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>         

        </div>
        
        <div id=\"tab-widgets-googleplus\" class=\"tab-pane\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Google+ Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_googleplus_box_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3802
        if (((isset($context["t1o_googleplus_box_status"]) ? $context["t1o_googleplus_box_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3803
        if (((isset($context["t1o_googleplus_box_status"]) ? $context["t1o_googleplus_box_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Google+ User:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_googleplus_box_user\" value=\"";
        // line 3810
        echo (isset($context["t1o_googleplus_box_user"]) ? $context["t1o_googleplus_box_user"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>          

        </div>
        
        <div id=\"tab-widgets-pinterest\" class=\"tab-pane\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Pinterest Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_pinterest_box_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3826
        if (((isset($context["t1o_pinterest_box_status"]) ? $context["t1o_pinterest_box_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3827
        if (((isset($context["t1o_pinterest_box_status"]) ? $context["t1o_pinterest_box_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Pinterest User:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_pinterest_box_user\" value=\"";
        // line 3834
        echo (isset($context["t1o_pinterest_box_user"]) ? $context["t1o_pinterest_box_user"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>          

        </div>
        
        <div id=\"tab-widgets-snapchat\" class=\"tab-pane\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Snapchat Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_snapchat_box_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3850
        if (((isset($context["t1o_snapchat_box_status"]) ? $context["t1o_snapchat_box_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3851
        if (((isset($context["t1o_snapchat_box_status"]) ? $context["t1o_snapchat_box_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Upload your Snapchat Code image:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<a href=\"\" id=\"t1o_snapchat_box_code_custom_thumb\" data-toggle=\"image\" class=\"img-thumbnail\"><img src=\"";
        // line 3858
        echo (isset($context["t1o_snapchat_box_code_custom_thumb"]) ? $context["t1o_snapchat_box_code_custom_thumb"] : null);
        echo "\" alt=\"\" title=\"\" data-placeholder=\"";
        echo (isset($context["placeholder"]) ? $context["placeholder"] : null);
        echo "\" /></a>
                                <input type=\"hidden\" name=\"t1o_snapchat_box_code_custom\" value=\"";
        // line 3859
        echo (isset($context["t1o_snapchat_box_code_custom"]) ? $context["t1o_snapchat_box_code_custom"] : null);
        echo "\" id=\"t1o_snapchat_box_code_custom\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Title:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_snapchat_box_title\" value=\"";
        // line 3865
        echo (isset($context["t1o_snapchat_box_title"]) ? $context["t1o_snapchat_box_title"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Subtitle:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_snapchat_box_subtitle\" value=\"";
        // line 3871
        echo (isset($context["t1o_snapchat_box_subtitle"]) ? $context["t1o_snapchat_box_subtitle"] : null);
        echo "\" class=\"form-control\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_snapchat_box_bg\" id=\"t1o_snapchat_box_bg\" value=\"";
        // line 3877
        echo (isset($context["t1o_snapchat_box_bg"]) ? $context["t1o_snapchat_box_bg"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset> 
        
        </div>
  
        <div id=\"tab-widgets-video-box\" class=\"tab-pane\">
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Video Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_video_box_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3893
        if (((isset($context["t1o_video_box_status"]) ? $context["t1o_video_box_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3894
        if (((isset($context["t1o_video_box_status"]) ? $context["t1o_video_box_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_video_box_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 3903
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_video_box_content_";
            // line 3904
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3906
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 3909
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_video_box_content_";
            // line 3910
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_video_box_content[";
            // line 3911
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_video_box_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_video_box_content"]) ? $context["t1o_video_box_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_video_box_content"]) ? $context["t1o_video_box_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3914
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_video_box_bg\" id=\"t1o_video_box_bg\" value=\"";
        // line 3921
        echo (isset($context["t1o_video_box_bg"]) ? $context["t1o_video_box_bg"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>          

        </div>
        
        <div id=\"tab-widgets-custom-box\" class=\"tab-pane\"> 
        
                    <fieldset>

                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Custom Content Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_custom_box_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3937
        if (((isset($context["t1o_custom_box_status"]) ? $context["t1o_custom_box_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3938
        if (((isset($context["t1o_custom_box_status"]) ? $context["t1o_custom_box_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Content:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_custom_box_content\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 3947
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_custom_box_content_";
            // line 3948
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3950
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 3953
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_custom_box_content_";
            // line 3954
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_custom_box_content[";
            // line 3955
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_custom_box_content-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_custom_box_content"]) ? $context["t1o_custom_box_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_custom_box_content"]) ? $context["t1o_custom_box_content"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 3958
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Background color:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<input type=\"text\" name=\"t1o_custom_box_bg\" id=\"t1o_custom_box_bg\" value=\"";
        // line 3965
        echo (isset($context["t1o_custom_box_bg"]) ? $context["t1o_custom_box_bg"] : null);
        echo "\" class=\"color {required:false,hash:true}\" size=\"8\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

                    </fieldset>              

        </div>
        
        
        <div id=\"tab-widgets-cookie\" class=\"tab-pane\">  
        
                    <fieldset>

\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show EU Cookie Message Widget:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_eu_cookie_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3982
        if (((isset($context["t1o_eu_cookie_status"]) ? $context["t1o_eu_cookie_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3983
        if (((isset($context["t1o_eu_cookie_status"]) ? $context["t1o_eu_cookie_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Show Cookie Icon:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t<select name=\"t1o_eu_cookie_icon_status\" class=\"form-control\">
\t\t\t\t\t\t\t\t    <option value=\"0\"";
        // line 3992
        if (((isset($context["t1o_eu_cookie_icon_status"]) ? $context["t1o_eu_cookie_icon_status"] : null) == "0")) {
            echo "selected=\"selected\"";
        }
        echo ">No</option>
                                    <option value=\"1\"";
        // line 3993
        if (((isset($context["t1o_eu_cookie_icon_status"]) ? $context["t1o_eu_cookie_icon_status"] : null) == "1")) {
            echo "selected=\"selected\"";
        }
        if (((isset($context["t1o_eu_cookie_icon_status"]) ? $context["t1o_eu_cookie_icon_status"] : null) == "")) {
            echo "selected=\"selected\"";
        }
        echo ">Yes</option>
\t\t\t\t\t\t\t    </select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">EU Cookie Message:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t
                                <ul id=\"t1o_eu_cookie_message\" class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t\t";
        // line 4003
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\t\t\t\t\t\t\t\t\t\t<li><a href=\"#t1o_eu_cookie_message_";
            // line 4004
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" data-toggle=\"tab\"><img src=\"language/";
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" alt=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" width=\"16px\" height=\"11px\" /> ";
            echo $this->getAttribute($context["language"], "name", array());
            echo "</a></li>
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4006
        echo "\t\t\t\t\t\t\t\t</ul>

\t\t\t\t\t\t\t\t<div class=\"tab-content\">\t\t\t\t\t
\t\t\t\t\t\t\t\t\t";
        // line 4009
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t\t\t\t
\t\t\t\t\t\t\t\t\t\t<div id=\"t1o_eu_cookie_message_";
            // line 4010
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" class=\"tab-pane\">
\t\t\t\t\t\t\t\t\t\t\t<textarea name=\"t1o_eu_cookie_message[";
            // line 4011
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "][htmlcontent]\" id=\"t1o_eu_cookie_message-";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\">";
            echo (($this->getAttribute((isset($context["t1o_eu_cookie_message"]) ? $context["t1o_eu_cookie_message"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute($this->getAttribute((isset($context["t1o_eu_cookie_message"]) ? $context["t1o_eu_cookie_message"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array"), "htmlcontent", array())) : (""));
            echo "</textarea>
\t\t\t\t\t\t\t\t\t\t</div>\t\t\t
\t\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4014
        echo "\t\t\t\t\t\t\t\t</div>
                                
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">\"Close\" text:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4022
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4023
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4024
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_eu_cookie_close[";
            // line 4025
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_eu_cookie_close_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_eu_cookie_close"]) ? $context["t1o_eu_cookie_close"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_eu_cookie_close"]) ? $context["t1o_eu_cookie_close"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : (""));
            echo "\" placeholder=\"Close\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4028
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t</fieldset>        
        
        </div>
 
     
        </div>
        </div>
        
        </div>
        </div>
       

 
 
        
        
        <div class=\"tab-pane\" id=\"tab-css\"> 
        <div class=\"row form-horizontal\">  

        
        <div class=\"col-sm-12\">
        <div class=\"tab-content\">
        
                    <fieldset>         
        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Custom CSS:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                <textarea name=\"t1o_custom_css\" rows=\"10\" class=\"form-control\" />";
        // line 4059
        echo (isset($context["t1o_custom_css"]) ? $context["t1o_custom_css"] : null);
        echo "</textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> 
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Custom JavaScript:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
                                <textarea name=\"t1o_custom_js\" rows=\"10\" class=\"form-control\" />";
        // line 4065
        echo (isset($context["t1o_custom_js"]) ? $context["t1o_custom_js"] : null);
        echo "</textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div> 
 
                    </fieldset>                          
     
        </div>
        </div>
        
        </div>
        </div>
        
        
        
        
        
        <div class=\"tab-pane\" id=\"tab-translate\"> 
        <div class=\"row form-horizontal\">  

        
        <div class=\"col-sm-12\">
        <div class=\"tab-content\">
        
                    <fieldset>         
        
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Sale</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4093
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4094
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4095
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_sale[";
            // line 4096
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_sale_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_sale"]) ? $context["t1o_text_sale"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_sale"]) ? $context["t1o_text_sale"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Sale"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4099
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">New</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4104
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4105
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4106
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_new_prod[";
            // line 4107
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_new_prod_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_new_prod"]) ? $context["t1o_text_new_prod"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_new_prod"]) ? $context["t1o_text_new_prod"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("New"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4110
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Quick View</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4115
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4116
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4117
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_quickview[";
            // line 4118
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_quickview_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Quick View"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4121
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Shop Now</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4126
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4127
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4128
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_shop_now[";
            // line 4129
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_shop_now_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_shop_now"]) ? $context["t1o_text_shop_now"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_shop_now"]) ? $context["t1o_text_shop_now"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Shop Now"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4132
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">View Now</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4137
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4138
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4139
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_view[";
            // line 4140
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_view_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_view"]) ? $context["t1o_text_view"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_view"]) ? $context["t1o_text_view"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("View Now"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4143
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Next</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4148
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4149
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4150
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_next_product[";
            // line 4151
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_next_product_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_next_product"]) ? $context["t1o_text_next_product"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_next_product"]) ? $context["t1o_text_next_product"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Next"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4154
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Previous</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4159
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4160
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4161
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_previous_product[";
            // line 4162
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_previous_product_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_previous_product"]) ? $context["t1o_text_previous_product"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_previous_product"]) ? $context["t1o_text_previous_product"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Previous"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4165
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Product viewed:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4170
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4171
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4172
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_product_viewed[";
            // line 4173
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_product_viewed_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_product_viewed"]) ? $context["t1o_text_product_viewed"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_product_viewed"]) ? $context["t1o_text_product_viewed"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Product viewed:"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4176
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Special price:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4181
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4182
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4183
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_special_price[";
            // line 4184
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_special_price_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_special_price"]) ? $context["t1o_text_special_price"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_special_price"]) ? $context["t1o_text_special_price"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Special price:"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4187
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Old price:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4192
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4193
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4194
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_old_price[";
            // line 4195
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_old_price_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_old_price"]) ? $context["t1o_text_old_price"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_old_price"]) ? $context["t1o_text_old_price"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Old price:"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4198
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">You save:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4203
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4204
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4205
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_percent_saved[";
            // line 4206
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_percent_saved_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_percent_saved"]) ? $context["t1o_text_percent_saved"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_percent_saved"]) ? $context["t1o_text_percent_saved"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("You save:"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4209
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Send to a friend</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4214
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4215
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4216
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_product_friend[";
            // line 4217
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_product_friend_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_product_friend"]) ? $context["t1o_text_product_friend"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_product_friend"]) ? $context["t1o_text_product_friend"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Send to a friend"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4220
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Shop by Category</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4225
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4226
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4227
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_categories[";
            // line 4228
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_categories_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_categories"]) ? $context["t1o_text_menu_categories"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_categories"]) ? $context["t1o_text_menu_categories"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Shop by Category"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4231
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Brands</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4236
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4237
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4238
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_brands[";
            // line 4239
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_brands_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_brands"]) ? $context["t1o_text_menu_brands"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_brands"]) ? $context["t1o_text_menu_brands"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Brands"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4242
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Contact us</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4247
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4248
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4249
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_contact_us[";
            // line 4250
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_contact_us_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_contact_us"]) ? $context["t1o_text_contact_us"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_contact_us"]) ? $context["t1o_text_contact_us"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Contact us"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4253
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Address</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4258
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4259
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4260
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_contact_address[";
            // line 4261
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_contact_address_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_contact_address"]) ? $context["t1o_text_menu_contact_address"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_contact_address"]) ? $context["t1o_text_menu_contact_address"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Address"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4264
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">E-mail</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4269
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4270
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4271
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_contact_email[";
            // line 4272
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_contact_email_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_contact_email"]) ? $context["t1o_text_menu_contact_email"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_contact_email"]) ? $context["t1o_text_menu_contact_email"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("E-mail"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4275
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Telephone</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4280
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4281
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4282
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_contact_tel[";
            // line 4283
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_contact_tel_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_contact_tel"]) ? $context["t1o_text_menu_contact_tel"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_contact_tel"]) ? $context["t1o_text_menu_contact_tel"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Telephone"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4286
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Fax</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4291
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4292
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4293
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_contact_fax[";
            // line 4294
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_contact_fax_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_contact_fax"]) ? $context["t1o_text_menu_contact_fax"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_contact_fax"]) ? $context["t1o_text_menu_contact_fax"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Fax"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4297
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Opening Times</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4302
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4303
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4304
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_contact_hours[";
            // line 4305
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_contact_hours_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_contact_hours"]) ? $context["t1o_text_menu_contact_hours"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_contact_hours"]) ? $context["t1o_text_menu_contact_hours"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Opening Times"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4308
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Contact Form</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4313
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4314
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4315
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_contact_form[";
            // line 4316
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_contact_form_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_contact_form"]) ? $context["t1o_text_menu_contact_form"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_contact_form"]) ? $context["t1o_text_menu_contact_form"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Contact Form"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4319
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Menu</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4324
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4325
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4326
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_menu_menu[";
            // line 4327
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_menu_menu_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_menu_menu"]) ? $context["t1o_text_menu_menu"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_menu_menu"]) ? $context["t1o_text_menu_menu"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Menu"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4330
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">See all products by</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4335
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4336
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4337
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_see_all_products_by[";
            // line 4338
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_see_all_products_by_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_see_all_products_by"]) ? $context["t1o_text_see_all_products_by"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_see_all_products_by"]) ? $context["t1o_text_see_all_products_by"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("See all products by"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4341
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Bestseller</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4346
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4347
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4348
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_bestseller[";
            // line 4349
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_bestseller_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_bestseller"]) ? $context["t1o_text_bestseller"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_bestseller"]) ? $context["t1o_text_bestseller"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Bestseller"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4352
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Featured</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4357
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4358
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4359
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_featured[";
            // line 4360
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_featured_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_featured"]) ? $context["t1o_text_featured"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_featured"]) ? $context["t1o_text_featured"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Featured"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4363
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Latest</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4368
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4369
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4370
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_latest[";
            // line 4371
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_latest_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_latest"]) ? $context["t1o_text_latest"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_latest"]) ? $context["t1o_text_latest"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Latest"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4374
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Specials</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4379
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4380
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4381
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_special[";
            // line 4382
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_special_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_special"]) ? $context["t1o_text_special"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_special"]) ? $context["t1o_text_special"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Specials"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4385
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Most Viewed</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4390
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4391
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4392
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_most_viewed[";
            // line 4393
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_most_viewed_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_most_viewed"]) ? $context["t1o_text_most_viewed"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_most_viewed"]) ? $context["t1o_text_most_viewed"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Most Viewed"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4396
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">News</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4401
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4402
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4403
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_news[";
            // line 4404
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_news_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_news"]) ? $context["t1o_text_news"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_news"]) ? $context["t1o_text_news"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("News"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4407
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Gallery</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4412
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4413
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4414
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_gallery[";
            // line 4415
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_gallery_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_gallery"]) ? $context["t1o_text_gallery"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_gallery"]) ? $context["t1o_text_gallery"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Gallery"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4418
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Small List</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4423
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4424
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4425
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_small_list[";
            // line 4426
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_small_list_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_small_list"]) ? $context["t1o_text_small_list"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_small_list"]) ? $context["t1o_text_small_list"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Small List"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4429
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Your Cart:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4434
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4435
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4436
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_your_cart[";
            // line 4437
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_your_cart_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_your_cart"]) ? $context["t1o_text_your_cart"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_your_cart"]) ? $context["t1o_text_your_cart"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Your Cart:"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4440
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Popular Search:</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4445
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4446
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4447
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_popular_search[";
            // line 4448
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_popular_search_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_popular_search"]) ? $context["t1o_text_popular_search"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_popular_search"]) ? $context["t1o_text_popular_search"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Popular Search:"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4451
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
                        <div class=\"form-group\">
\t\t\t\t\t\t\t<label class=\"col-sm-2 control-label\">Advanced Search</label>
\t\t\t\t\t\t\t<div class=\"col-sm-10\">
\t\t\t\t\t\t\t\t";
        // line 4456
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            // line 4457
            echo "\t\t\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\"><img src=\"language/";
            // line 4458
            echo $this->getAttribute($context["language"], "code", array());
            echo "/";
            echo $this->getAttribute($context["language"], "code", array());
            echo ".png\" title=\"";
            echo $this->getAttribute($context["language"], "name", array());
            echo "\" /></span>
                                            <input type=\"text\" name=\"t1o_text_advanced_search[";
            // line 4459
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "]\" id=\"t1o_text_advanced_search_";
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "\" value=\"";
            echo (($this->getAttribute((isset($context["t1o_text_advanced_search"]) ? $context["t1o_text_advanced_search"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) ? ($this->getAttribute((isset($context["t1o_text_advanced_search"]) ? $context["t1o_text_advanced_search"] : null), $this->getAttribute($context["language"], "language_id", array()), array(), "array")) : ("Advanced Search"));
            echo "\" placeholder=\"\" class=\"form-control\" />
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4462
        echo "\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
 
                    </fieldset>                          
     
        </div>
        </div>
        
        </div>
        </div>


        <!-- -->         
        </div>  
        
        
    </form>
    </div>
  </div>
</div>

<script type=\"text/javascript\" src=\"view/javascript/jscolor/jscolor.js\"></script>
<script type=\"text/javascript\" src=\"view/javascript/poshytip/jquery.poshytip.js\"></script>
<link rel=\"stylesheet\" type=\"text/css\" href=\"view/javascript/poshytip/tip-twitter/tip-twitter.css\" />

<script type=\"text/javascript\" src=\"view/javascript/summernote/summernote.js\"></script>
<link href=\"view/javascript/summernote/summernote.css\" rel=\"stylesheet\" />
<script type=\"text/javascript\" src=\"view/javascript/summernote/opencart.js\"></script>  

<script type=\"text/javascript\"><!--\t\t\t\t\t
";
        // line 4492
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["languages"]) ? $context["languages"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["language"]) {
            echo "\t
\$('#t1o_top_custom_block_content-";
            // line 4493
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_header_custom_block_1_content-";
            // line 4494
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_bar_below_menu_content-";
            // line 4495
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_menu_categories_custom_block_left_content-";
            // line 4496
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_menu_categories_custom_block_right_content-";
            // line 4497
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_menu_categories_custom_block_content-";
            // line 4498
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_menu_custom_block_1_";
            // line 4499
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_menu_custom_block_2_";
            // line 4500
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_menu_custom_block_3_";
            // line 4501
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_menu_custom_block_4_";
            // line 4502
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_menu_custom_block_5_";
            // line 4503
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_product_fb1_content-";
            // line 4504
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_product_fb2_content-";
            // line 4505
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_product_fb3_content-";
            // line 4506
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_product_custom_block_1_content-";
            // line 4507
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_product_custom_block_2_content-";
            // line 4508
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_product_custom_block_3_content-";
            // line 4509
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_product_custom_tab_1_";
            // line 4510
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_product_custom_tab_2_";
            // line 4511
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_product_custom_tab_3_";
            // line 4512
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "_content').summernote({height: 300});
\$('#t1o_contact_custom_content-";
            // line 4513
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_fp_fb1_content-";
            // line 4514
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_fp_fb2_content-";
            // line 4515
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_fp_fb3_content-";
            // line 4516
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_fp_fb4_content-";
            // line 4517
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_top_1_content-";
            // line 4518
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_1_content-";
            // line 4519
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_2_content-";
            // line 4520
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_bottom_1_content-";
            // line 4521
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_bottom_2_content-";
            // line 4522
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_powered_content-";
            // line 4523
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_video_box_content-";
            // line 4524
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_custom_box_content-";
            // line 4525
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
\$('#t1o_eu_cookie_message-";
            // line 4526
            echo $this->getAttribute($context["language"], "language_id", array());
            echo "').summernote({height: 300});
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['language'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 4528
        echo "//--></script>
<script type=\"text/javascript\"><!--
\$('#t1o_top_custom_block_content li:first-child a').tab('show');
\$('#t1o_header_custom_block_1_content li:first-child a').tab('show');
\$('#t1o_custom_bar_below_menu_content li:first-child a').tab('show');
\$('#t1o_menu_categories_custom_block_left_content li:first-child a').tab('show');
\$('#t1o_menu_categories_custom_block_right_content li:first-child a').tab('show');
\$('#t1o_menu_categories_custom_block_content li:first-child a').tab('show');
\$('#t1o_menu_custom_block_1 li:first-child a').tab('show');
\$('#t1o_menu_custom_block_2 li:first-child a').tab('show');
\$('#t1o_menu_custom_block_3 li:first-child a').tab('show');
\$('#t1o_menu_custom_block_4 li:first-child a').tab('show');
\$('#t1o_menu_custom_block_5 li:first-child a').tab('show');
\$('#t1o_product_fb1_content li:first-child a').tab('show');
\$('#t1o_product_fb2_content li:first-child a').tab('show');
\$('#t1o_product_fb3_content li:first-child a').tab('show');
\$('#t1o_product_custom_block_1_content li:first-child a').tab('show');
\$('#t1o_product_custom_block_2_content li:first-child a').tab('show');
\$('#t1o_product_custom_block_3_content li:first-child a').tab('show');
\$('#t1o_product_custom_tab_1 li:first-child a').tab('show');
\$('#t1o_product_custom_tab_2 li:first-child a').tab('show');
\$('#t1o_product_custom_tab_3 li:first-child a').tab('show');
\$('#t1o_contact_custom_content li:first-child a').tab('show');
\$('#t1o_fp_fb1_content li:first-child a').tab('show');
\$('#t1o_fp_fb2_content li:first-child a').tab('show');
\$('#t1o_fp_fb3_content li:first-child a').tab('show');
\$('#t1o_fp_fb4_content li:first-child a').tab('show');
\$('#t1o_custom_top_1_content li:first-child a').tab('show');
\$('#t1o_custom_1_content li:first-child a').tab('show');
\$('#t1o_custom_2_content li:first-child a').tab('show');
\$('#t1o_custom_bottom_1_content li:first-child a').tab('show');
\$('#t1o_custom_bottom_2_content li:first-child a').tab('show');
\$('#t1o_powered_content li:first-child a').tab('show');
\$('#t1o_video_box_content li:first-child a').tab('show');
\$('#t1o_custom_box_content li:first-child a').tab('show');
\$('#t1o_eu_cookie_message li:first-child a').tab('show');
//--></script>
</div>

";
        // line 4567
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "extension/module/oxy_theme_options.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  10588 => 4567,  10547 => 4528,  10539 => 4526,  10535 => 4525,  10531 => 4524,  10527 => 4523,  10523 => 4522,  10519 => 4521,  10515 => 4520,  10511 => 4519,  10507 => 4518,  10503 => 4517,  10499 => 4516,  10495 => 4515,  10491 => 4514,  10487 => 4513,  10483 => 4512,  10479 => 4511,  10475 => 4510,  10471 => 4509,  10467 => 4508,  10463 => 4507,  10459 => 4506,  10455 => 4505,  10451 => 4504,  10447 => 4503,  10443 => 4502,  10439 => 4501,  10435 => 4500,  10431 => 4499,  10427 => 4498,  10423 => 4497,  10419 => 4496,  10415 => 4495,  10411 => 4494,  10407 => 4493,  10401 => 4492,  10369 => 4462,  10356 => 4459,  10348 => 4458,  10345 => 4457,  10341 => 4456,  10334 => 4451,  10321 => 4448,  10313 => 4447,  10310 => 4446,  10306 => 4445,  10299 => 4440,  10286 => 4437,  10278 => 4436,  10275 => 4435,  10271 => 4434,  10264 => 4429,  10251 => 4426,  10243 => 4425,  10240 => 4424,  10236 => 4423,  10229 => 4418,  10216 => 4415,  10208 => 4414,  10205 => 4413,  10201 => 4412,  10194 => 4407,  10181 => 4404,  10173 => 4403,  10170 => 4402,  10166 => 4401,  10159 => 4396,  10146 => 4393,  10138 => 4392,  10135 => 4391,  10131 => 4390,  10124 => 4385,  10111 => 4382,  10103 => 4381,  10100 => 4380,  10096 => 4379,  10089 => 4374,  10076 => 4371,  10068 => 4370,  10065 => 4369,  10061 => 4368,  10054 => 4363,  10041 => 4360,  10033 => 4359,  10030 => 4358,  10026 => 4357,  10019 => 4352,  10006 => 4349,  9998 => 4348,  9995 => 4347,  9991 => 4346,  9984 => 4341,  9971 => 4338,  9963 => 4337,  9960 => 4336,  9956 => 4335,  9949 => 4330,  9936 => 4327,  9928 => 4326,  9925 => 4325,  9921 => 4324,  9914 => 4319,  9901 => 4316,  9893 => 4315,  9890 => 4314,  9886 => 4313,  9879 => 4308,  9866 => 4305,  9858 => 4304,  9855 => 4303,  9851 => 4302,  9844 => 4297,  9831 => 4294,  9823 => 4293,  9820 => 4292,  9816 => 4291,  9809 => 4286,  9796 => 4283,  9788 => 4282,  9785 => 4281,  9781 => 4280,  9774 => 4275,  9761 => 4272,  9753 => 4271,  9750 => 4270,  9746 => 4269,  9739 => 4264,  9726 => 4261,  9718 => 4260,  9715 => 4259,  9711 => 4258,  9704 => 4253,  9691 => 4250,  9683 => 4249,  9680 => 4248,  9676 => 4247,  9669 => 4242,  9656 => 4239,  9648 => 4238,  9645 => 4237,  9641 => 4236,  9634 => 4231,  9621 => 4228,  9613 => 4227,  9610 => 4226,  9606 => 4225,  9599 => 4220,  9586 => 4217,  9578 => 4216,  9575 => 4215,  9571 => 4214,  9564 => 4209,  9551 => 4206,  9543 => 4205,  9540 => 4204,  9536 => 4203,  9529 => 4198,  9516 => 4195,  9508 => 4194,  9505 => 4193,  9501 => 4192,  9494 => 4187,  9481 => 4184,  9473 => 4183,  9470 => 4182,  9466 => 4181,  9459 => 4176,  9446 => 4173,  9438 => 4172,  9435 => 4171,  9431 => 4170,  9424 => 4165,  9411 => 4162,  9403 => 4161,  9400 => 4160,  9396 => 4159,  9389 => 4154,  9376 => 4151,  9368 => 4150,  9365 => 4149,  9361 => 4148,  9354 => 4143,  9341 => 4140,  9333 => 4139,  9330 => 4138,  9326 => 4137,  9319 => 4132,  9306 => 4129,  9298 => 4128,  9295 => 4127,  9291 => 4126,  9284 => 4121,  9271 => 4118,  9263 => 4117,  9260 => 4116,  9256 => 4115,  9249 => 4110,  9236 => 4107,  9228 => 4106,  9225 => 4105,  9221 => 4104,  9214 => 4099,  9201 => 4096,  9193 => 4095,  9190 => 4094,  9186 => 4093,  9155 => 4065,  9146 => 4059,  9113 => 4028,  9100 => 4025,  9092 => 4024,  9089 => 4023,  9085 => 4022,  9075 => 4014,  9062 => 4011,  9058 => 4010,  9052 => 4009,  9047 => 4006,  9031 => 4004,  9025 => 4003,  9007 => 3993,  9001 => 3992,  8987 => 3983,  8981 => 3982,  8961 => 3965,  8952 => 3958,  8939 => 3955,  8935 => 3954,  8929 => 3953,  8924 => 3950,  8908 => 3948,  8902 => 3947,  8888 => 3938,  8882 => 3937,  8863 => 3921,  8854 => 3914,  8841 => 3911,  8837 => 3910,  8831 => 3909,  8826 => 3906,  8810 => 3904,  8804 => 3903,  8790 => 3894,  8784 => 3893,  8765 => 3877,  8756 => 3871,  8747 => 3865,  8738 => 3859,  8732 => 3858,  8720 => 3851,  8714 => 3850,  8695 => 3834,  8683 => 3827,  8677 => 3826,  8658 => 3810,  8646 => 3803,  8640 => 3802,  8618 => 3785,  8609 => 3784,  8603 => 3783,  8592 => 3775,  8583 => 3769,  8571 => 3762,  8565 => 3761,  8545 => 3744,  8533 => 3737,  8527 => 3736,  8475 => 3689,  8469 => 3688,  8463 => 3687,  8454 => 3686,  8448 => 3685,  8442 => 3684,  8436 => 3683,  8430 => 3682,  8424 => 3681,  8418 => 3680,  8412 => 3679,  8406 => 3678,  8400 => 3677,  8394 => 3676,  8382 => 3666,  8369 => 3663,  8365 => 3662,  8359 => 3661,  8354 => 3658,  8338 => 3656,  8332 => 3655,  8317 => 3645,  8311 => 3644,  8290 => 3625,  8277 => 3622,  8273 => 3621,  8267 => 3620,  8262 => 3617,  8246 => 3615,  8240 => 3614,  8225 => 3604,  8219 => 3603,  8200 => 3587,  8191 => 3581,  8182 => 3575,  8173 => 3569,  8164 => 3563,  8155 => 3557,  8146 => 3551,  8136 => 3544,  8127 => 3538,  8118 => 3532,  8109 => 3526,  8100 => 3520,  8091 => 3514,  8082 => 3508,  8073 => 3502,  8064 => 3496,  8051 => 3488,  8045 => 3487,  8024 => 3468,  8011 => 3465,  8007 => 3464,  8001 => 3463,  7996 => 3460,  7980 => 3458,  7974 => 3457,  7959 => 3447,  7953 => 3446,  7934 => 3430,  7923 => 3424,  7917 => 3423,  7907 => 3416,  7896 => 3410,  7890 => 3409,  7880 => 3402,  7869 => 3396,  7863 => 3395,  7853 => 3388,  7842 => 3382,  7836 => 3381,  7826 => 3374,  7815 => 3368,  7809 => 3367,  7799 => 3360,  7788 => 3354,  7782 => 3353,  7772 => 3346,  7761 => 3340,  7754 => 3338,  7744 => 3331,  7733 => 3325,  7727 => 3324,  7717 => 3317,  7706 => 3311,  7700 => 3310,  7690 => 3303,  7679 => 3297,  7673 => 3296,  7663 => 3289,  7652 => 3283,  7646 => 3282,  7636 => 3275,  7625 => 3269,  7619 => 3268,  7609 => 3261,  7598 => 3255,  7592 => 3254,  7582 => 3247,  7571 => 3241,  7565 => 3240,  7555 => 3233,  7544 => 3227,  7538 => 3226,  7525 => 3216,  7516 => 3210,  7510 => 3209,  7498 => 3202,  7492 => 3201,  7475 => 3189,  7469 => 3188,  7448 => 3169,  7435 => 3166,  7431 => 3165,  7425 => 3164,  7420 => 3161,  7403 => 3159,  7399 => 3158,  7389 => 3150,  7376 => 3147,  7368 => 3146,  7365 => 3145,  7361 => 3144,  7353 => 3138,  7340 => 3135,  7332 => 3134,  7329 => 3133,  7325 => 3132,  7317 => 3126,  7304 => 3123,  7296 => 3122,  7293 => 3121,  7289 => 3120,  7277 => 3111,  7263 => 3102,  7257 => 3101,  7248 => 3094,  7235 => 3091,  7227 => 3090,  7224 => 3089,  7220 => 3088,  7207 => 3080,  7201 => 3079,  7195 => 3078,  7189 => 3077,  7183 => 3076,  7177 => 3075,  7171 => 3074,  7165 => 3073,  7159 => 3072,  7150 => 3071,  7144 => 3070,  7138 => 3069,  7123 => 3059,  7117 => 3058,  7092 => 3041,  7086 => 3040,  7070 => 3032,  7064 => 3031,  7048 => 3023,  7042 => 3022,  7026 => 3014,  7020 => 3013,  7004 => 3005,  6998 => 3004,  6982 => 2996,  6976 => 2995,  6960 => 2987,  6954 => 2986,  6935 => 2975,  6929 => 2974,  6913 => 2966,  6907 => 2965,  6891 => 2957,  6885 => 2956,  6869 => 2948,  6863 => 2947,  6847 => 2939,  6841 => 2938,  6824 => 2929,  6818 => 2928,  6797 => 2915,  6791 => 2914,  6775 => 2903,  6769 => 2902,  6763 => 2901,  6757 => 2900,  6751 => 2899,  6745 => 2898,  6736 => 2897,  6730 => 2896,  6724 => 2895,  6718 => 2894,  6712 => 2893,  6706 => 2892,  6685 => 2873,  6672 => 2870,  6668 => 2869,  6662 => 2868,  6657 => 2865,  6640 => 2863,  6636 => 2862,  6626 => 2854,  6613 => 2851,  6605 => 2850,  6602 => 2849,  6598 => 2848,  6585 => 2840,  6579 => 2839,  6573 => 2838,  6567 => 2837,  6561 => 2836,  6555 => 2835,  6549 => 2834,  6543 => 2833,  6537 => 2832,  6528 => 2831,  6522 => 2830,  6516 => 2829,  6501 => 2819,  6495 => 2818,  6476 => 2801,  6463 => 2798,  6459 => 2797,  6453 => 2796,  6448 => 2793,  6432 => 2791,  6426 => 2790,  6411 => 2780,  6405 => 2779,  6352 => 2731,  6341 => 2730,  6333 => 2729,  6309 => 2710,  6303 => 2709,  6297 => 2708,  6291 => 2707,  6279 => 2698,  6268 => 2690,  6255 => 2682,  6249 => 2681,  6232 => 2666,  6221 => 2660,  6204 => 2657,  6198 => 2656,  6192 => 2655,  6187 => 2652,  6169 => 2650,  6163 => 2649,  6159 => 2648,  6150 => 2641,  6133 => 2638,  6125 => 2637,  6122 => 2636,  6118 => 2635,  6105 => 2627,  6099 => 2626,  6095 => 2625,  6090 => 2623,  6084 => 2620,  6081 => 2619,  6077 => 2618,  6070 => 2613,  6057 => 2610,  6053 => 2609,  6047 => 2608,  6042 => 2605,  6026 => 2603,  6020 => 2602,  6010 => 2594,  5997 => 2591,  5989 => 2590,  5986 => 2589,  5982 => 2588,  5969 => 2580,  5963 => 2579,  5950 => 2568,  5937 => 2565,  5933 => 2564,  5927 => 2563,  5922 => 2560,  5906 => 2558,  5900 => 2557,  5890 => 2549,  5877 => 2546,  5869 => 2545,  5866 => 2544,  5862 => 2543,  5849 => 2535,  5843 => 2534,  5830 => 2523,  5817 => 2520,  5813 => 2519,  5807 => 2518,  5802 => 2515,  5786 => 2513,  5780 => 2512,  5770 => 2504,  5757 => 2501,  5749 => 2500,  5746 => 2499,  5742 => 2498,  5729 => 2490,  5723 => 2489,  5710 => 2478,  5697 => 2475,  5693 => 2474,  5687 => 2473,  5682 => 2470,  5666 => 2468,  5660 => 2467,  5650 => 2459,  5637 => 2456,  5629 => 2455,  5626 => 2454,  5622 => 2453,  5614 => 2447,  5601 => 2444,  5593 => 2443,  5590 => 2442,  5586 => 2441,  5574 => 2432,  5564 => 2425,  5558 => 2424,  5546 => 2414,  5533 => 2411,  5529 => 2410,  5523 => 2409,  5518 => 2406,  5502 => 2404,  5496 => 2403,  5486 => 2395,  5473 => 2392,  5465 => 2391,  5462 => 2390,  5458 => 2389,  5450 => 2383,  5437 => 2380,  5429 => 2379,  5426 => 2378,  5422 => 2377,  5410 => 2368,  5400 => 2361,  5394 => 2360,  5382 => 2350,  5369 => 2347,  5365 => 2346,  5359 => 2345,  5354 => 2342,  5338 => 2340,  5332 => 2339,  5322 => 2331,  5309 => 2328,  5301 => 2327,  5298 => 2326,  5294 => 2325,  5286 => 2319,  5273 => 2316,  5265 => 2315,  5262 => 2314,  5258 => 2313,  5246 => 2304,  5236 => 2297,  5230 => 2296,  5212 => 2286,  5206 => 2285,  5188 => 2275,  5182 => 2274,  5165 => 2265,  5159 => 2264,  5139 => 2252,  5133 => 2251,  5115 => 2241,  5109 => 2240,  5091 => 2230,  5085 => 2229,  5067 => 2219,  5061 => 2218,  5043 => 2208,  5037 => 2207,  5019 => 2197,  5013 => 2196,  4997 => 2185,  4991 => 2184,  4985 => 2183,  4979 => 2182,  4973 => 2181,  4967 => 2180,  4961 => 2179,  4952 => 2178,  4946 => 2177,  4940 => 2176,  4921 => 2165,  4915 => 2164,  4904 => 2155,  4893 => 2153,  4889 => 2152,  4884 => 2149,  4873 => 2148,  4869 => 2147,  4864 => 2144,  4853 => 2143,  4849 => 2142,  4844 => 2139,  4834 => 2138,  4828 => 2137,  4816 => 2130,  4810 => 2129,  4804 => 2128,  4795 => 2127,  4789 => 2126,  4783 => 2125,  4777 => 2124,  4771 => 2123,  4765 => 2122,  4759 => 2121,  4753 => 2120,  4747 => 2119,  4741 => 2118,  4721 => 2106,  4715 => 2105,  4688 => 2086,  4682 => 2085,  4668 => 2076,  4662 => 2075,  4645 => 2066,  4639 => 2065,  4622 => 2056,  4616 => 2055,  4602 => 2046,  4596 => 2045,  4579 => 2036,  4573 => 2035,  4556 => 2026,  4550 => 2025,  4533 => 2016,  4527 => 2015,  4510 => 2006,  4504 => 2005,  4487 => 1996,  4481 => 1995,  4464 => 1986,  4458 => 1985,  4441 => 1976,  4435 => 1975,  4418 => 1966,  4412 => 1965,  4395 => 1956,  4389 => 1955,  4372 => 1946,  4366 => 1945,  4348 => 1935,  4342 => 1934,  4327 => 1924,  4321 => 1923,  4315 => 1922,  4299 => 1911,  4293 => 1910,  4279 => 1901,  4273 => 1900,  4267 => 1899,  4258 => 1898,  4252 => 1897,  4246 => 1896,  4240 => 1895,  4226 => 1886,  4220 => 1885,  4203 => 1876,  4197 => 1875,  4180 => 1863,  4174 => 1862,  4161 => 1854,  4155 => 1853,  4139 => 1845,  4133 => 1844,  4120 => 1834,  4105 => 1827,  4099 => 1826,  4054 => 1784,  4045 => 1778,  4033 => 1771,  4027 => 1770,  4017 => 1763,  4011 => 1762,  4002 => 1755,  3989 => 1752,  3985 => 1751,  3979 => 1750,  3974 => 1747,  3958 => 1745,  3952 => 1744,  3937 => 1734,  3931 => 1733,  3911 => 1715,  3897 => 1711,  3892 => 1708,  3875 => 1705,  3867 => 1704,  3860 => 1702,  3855 => 1700,  3852 => 1699,  3848 => 1698,  3828 => 1680,  3814 => 1674,  3808 => 1673,  3804 => 1672,  3795 => 1668,  3791 => 1666,  3774 => 1663,  3766 => 1662,  3763 => 1661,  3759 => 1660,  3750 => 1656,  3744 => 1655,  3740 => 1654,  3735 => 1652,  3732 => 1651,  3728 => 1650,  3698 => 1622,  3684 => 1616,  3678 => 1615,  3674 => 1614,  3664 => 1609,  3658 => 1605,  3641 => 1602,  3633 => 1601,  3630 => 1600,  3626 => 1599,  3615 => 1593,  3609 => 1592,  3605 => 1591,  3599 => 1588,  3596 => 1587,  3592 => 1586,  3574 => 1570,  3561 => 1567,  3553 => 1566,  3550 => 1565,  3546 => 1564,  3533 => 1556,  3527 => 1555,  3514 => 1544,  3500 => 1538,  3494 => 1537,  3490 => 1536,  3480 => 1531,  3474 => 1527,  3457 => 1524,  3449 => 1523,  3446 => 1522,  3442 => 1521,  3431 => 1515,  3425 => 1514,  3421 => 1513,  3415 => 1510,  3412 => 1509,  3408 => 1508,  3390 => 1492,  3377 => 1489,  3369 => 1488,  3366 => 1487,  3362 => 1486,  3349 => 1478,  3343 => 1477,  3330 => 1466,  3316 => 1460,  3310 => 1459,  3306 => 1458,  3296 => 1453,  3290 => 1449,  3273 => 1446,  3265 => 1445,  3262 => 1444,  3258 => 1443,  3247 => 1437,  3241 => 1436,  3237 => 1435,  3231 => 1432,  3228 => 1431,  3224 => 1430,  3206 => 1414,  3193 => 1411,  3185 => 1410,  3182 => 1409,  3178 => 1408,  3165 => 1400,  3159 => 1399,  3142 => 1384,  3131 => 1378,  3114 => 1375,  3108 => 1374,  3102 => 1373,  3097 => 1370,  3079 => 1368,  3073 => 1367,  3069 => 1366,  3060 => 1359,  3043 => 1356,  3035 => 1355,  3032 => 1354,  3028 => 1353,  3015 => 1345,  3009 => 1344,  3005 => 1343,  3000 => 1341,  2994 => 1338,  2991 => 1337,  2987 => 1336,  2969 => 1323,  2963 => 1322,  2954 => 1321,  2948 => 1320,  2942 => 1319,  2936 => 1318,  2919 => 1308,  2911 => 1307,  2903 => 1306,  2886 => 1297,  2880 => 1296,  2859 => 1277,  2846 => 1274,  2842 => 1273,  2836 => 1272,  2831 => 1269,  2815 => 1267,  2809 => 1266,  2794 => 1256,  2788 => 1255,  2777 => 1246,  2764 => 1243,  2760 => 1242,  2754 => 1241,  2749 => 1238,  2733 => 1236,  2727 => 1235,  2712 => 1225,  2706 => 1224,  2695 => 1215,  2682 => 1212,  2678 => 1211,  2672 => 1210,  2667 => 1207,  2651 => 1205,  2645 => 1204,  2630 => 1194,  2624 => 1193,  2604 => 1181,  2598 => 1180,  2584 => 1171,  2578 => 1170,  2564 => 1161,  2558 => 1160,  2549 => 1159,  2543 => 1158,  2537 => 1157,  2531 => 1156,  2516 => 1146,  2510 => 1145,  2504 => 1144,  2498 => 1143,  2492 => 1142,  2486 => 1141,  2480 => 1140,  2474 => 1139,  2468 => 1138,  2459 => 1137,  2453 => 1136,  2447 => 1135,  2441 => 1134,  2435 => 1133,  2429 => 1132,  2423 => 1131,  2417 => 1130,  2411 => 1129,  2405 => 1128,  2399 => 1127,  2393 => 1126,  2375 => 1116,  2369 => 1115,  2363 => 1114,  2357 => 1113,  2351 => 1112,  2345 => 1111,  2339 => 1110,  2333 => 1109,  2327 => 1108,  2321 => 1107,  2315 => 1106,  2309 => 1105,  2303 => 1104,  2297 => 1103,  2291 => 1102,  2285 => 1101,  2279 => 1100,  2273 => 1099,  2267 => 1098,  2261 => 1097,  2255 => 1096,  2241 => 1087,  2235 => 1086,  2220 => 1076,  2209 => 1075,  2203 => 1074,  2195 => 1073,  2187 => 1072,  2170 => 1063,  2164 => 1062,  2140 => 1043,  2134 => 1042,  2125 => 1041,  2119 => 1040,  2094 => 1020,  2088 => 1019,  2042 => 975,  2034 => 972,  2017 => 969,  2009 => 968,  2002 => 966,  1997 => 964,  1994 => 963,  1990 => 962,  1980 => 957,  1974 => 956,  1954 => 944,  1948 => 943,  1926 => 923,  1913 => 920,  1909 => 919,  1903 => 918,  1898 => 915,  1882 => 913,  1876 => 912,  1862 => 903,  1856 => 902,  1835 => 884,  1826 => 878,  1817 => 872,  1808 => 866,  1799 => 860,  1789 => 852,  1777 => 848,  1773 => 846,  1756 => 843,  1748 => 842,  1745 => 841,  1741 => 840,  1732 => 836,  1726 => 835,  1722 => 834,  1717 => 832,  1714 => 831,  1710 => 830,  1688 => 813,  1682 => 812,  1661 => 794,  1652 => 788,  1643 => 782,  1637 => 781,  1628 => 774,  1615 => 771,  1611 => 770,  1605 => 769,  1600 => 766,  1584 => 764,  1578 => 763,  1566 => 754,  1558 => 748,  1545 => 745,  1537 => 744,  1534 => 743,  1530 => 742,  1520 => 735,  1508 => 726,  1495 => 718,  1489 => 717,  1478 => 709,  1472 => 708,  1462 => 701,  1449 => 693,  1443 => 692,  1424 => 675,  1411 => 672,  1403 => 671,  1400 => 670,  1396 => 669,  1385 => 661,  1373 => 654,  1367 => 653,  1354 => 645,  1348 => 644,  1332 => 636,  1326 => 635,  1301 => 615,  1295 => 614,  1275 => 597,  1263 => 588,  1250 => 580,  1244 => 579,  1233 => 571,  1225 => 565,  1212 => 562,  1204 => 561,  1201 => 560,  1197 => 559,  1179 => 543,  1168 => 541,  1164 => 540,  1152 => 533,  1146 => 532,  1140 => 531,  1131 => 530,  1125 => 529,  1068 => 480,  1062 => 479,  1037 => 462,  1031 => 461,  1025 => 460,  1019 => 459,  1013 => 458,  1007 => 457,  991 => 449,  985 => 448,  979 => 447,  973 => 446,  967 => 445,  961 => 444,  945 => 436,  939 => 435,  933 => 434,  927 => 433,  921 => 432,  915 => 431,  899 => 423,  893 => 422,  887 => 421,  881 => 420,  875 => 419,  869 => 418,  853 => 410,  847 => 409,  841 => 408,  835 => 407,  829 => 406,  823 => 405,  807 => 397,  801 => 396,  795 => 395,  789 => 394,  783 => 393,  777 => 392,  761 => 384,  755 => 383,  749 => 382,  743 => 381,  737 => 380,  731 => 379,  718 => 371,  712 => 370,  703 => 369,  697 => 368,  691 => 367,  685 => 366,  669 => 355,  663 => 354,  654 => 353,  648 => 352,  642 => 351,  636 => 350,  616 => 338,  610 => 337,  594 => 329,  588 => 328,  572 => 320,  566 => 319,  550 => 311,  544 => 310,  528 => 302,  522 => 301,  506 => 293,  500 => 292,  475 => 272,  469 => 271,  449 => 261,  441 => 260,  426 => 250,  417 => 249,  411 => 248,  393 => 237,  385 => 236,  370 => 226,  364 => 225,  348 => 214,  339 => 213,  333 => 212,  282 => 164,  277 => 162,  268 => 156,  264 => 154,  256 => 150,  254 => 149,  248 => 145,  237 => 143,  233 => 142,  228 => 140,  222 => 139,  218 => 138,  208 => 131,  96 => 21,  92 => 20,  88 => 19,  84 => 18,  80 => 17,  76 => 16,  72 => 15,  68 => 14,  64 => 13,  60 => 12,  56 => 11,  52 => 10,  48 => 9,  44 => 8,  40 => 7,  36 => 6,  32 => 5,  28 => 4,  24 => 3,  19 => 1,);
    }
}
/* {{ header }}*/
/*  */
/* {% if t1o_text_logo_color is empty %}{% set t1o_text_logo_color = '424242' %}{% endif %}*/
/* {% if t1o_text_logo_awesome_color is empty %}{% set t1o_text_logo_awesome_color = 'F1494B' %}{% endif %}*/
/* {% if t1o_top_custom_block_title_bar_bg_color is empty %}{% set t1o_top_custom_block_title_bar_bg_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1o_top_custom_block_awesome_color is empty %}{% set t1o_top_custom_block_awesome_color = 'F1494B' %}{% endif %}*/
/* {% if t1o_top_custom_block_title_color is empty %}{% set t1o_top_custom_block_title_color = '424242' %}{% endif %}*/
/* {% if t1o_top_custom_block_bg_color is empty %}{% set t1o_top_custom_block_bg_color = '424242' %}{% endif %}*/
/* {% if t1o_top_custom_block_text_color is empty %}{% set t1o_top_custom_block_text_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1o_news_bg_color is empty %}{% set t1o_news_bg_color = '373737' %}{% endif %}*/
/* {% if t1o_news_icons_color is empty %}{% set t1o_news_icons_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1o_news_word_color is empty %}{% set t1o_news_word_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1o_news_color is empty %}{% set t1o_news_color = 'B6B6B6' %}{% endif %}*/
/* {% if t1o_news_hover_color is empty %}{% set t1o_news_hover_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1o_custom_bar_below_menu_bg_color is empty %}{% set t1o_custom_bar_below_menu_bg_color = '373737' %}{% endif %}*/
/* {% if t1o_custom_bar_below_menu_text_color is empty %}{% set t1o_custom_bar_below_menu_text_color = 'FFFFFF' %}{% endif %}*/
/* {% if t1o_category_title_above_color is empty %}{% set t1o_category_title_above_color = '424242' %}{% endif %}*/
/* {% if t1o_snapchat_box_bg is empty %}{% set t1o_snapchat_box_bg = '000000' %}{% endif %}*/
/* {% if t1o_video_box_bg is empty %}{% set t1o_video_box_bg = 'E22C29' %}{% endif %}*/
/* {% if t1o_custom_box_bg is empty %}{% set t1o_custom_box_bg = '424242' %}{% endif %}*/
/* */
/* <style type="text/css">*/
/* .color {border:1px solid #CCC;border-radius:2px;margin-top:5px;padding:5px 6px 6px;}*/
/* .k_help {color:#999;background-color:#F5F5F5;font-size:10px;font-weight:normal;text-transform:uppercase;padding:15px;width:auto;display:block;margin-top:10px;margin-bottom:10px;border-radius:3px;}*/
/* span.k_help_tip {margin-left:10px;padding:4px 9px 3px;width:24px;display:inline;border-radius:2px;background-color:#1E91CF;color:#FFF;font-weight:bold;transition: all 0.15s ease-in 0s;opacity: 0.9; display: none;}*/
/* span.k_help_tip_support {display:block;}*/
/* span.k_help_tip:hover {opacity: 1;}*/
/* span.k_help_tip a {color:#FFF;font-size:12px;font-weight:bold;text-decoration:none;}*/
/* span.k_tooltip {cursor:pointer;}*/
/* .k_sep {background-color:#F7F7F7;}*/
/* .ptn {position:relative;width:40px;height:40px;float:left;margin-right:5px;margin-bottom:5px;}*/
/* .ptn_nr {position:absolute;bottom:0px;right:3px;}*/
/* .prod_l {position:relative;width:134px;height:134px;float:left;margin-right:25px;margin-bottom:30px;}*/
/* .prod_l_nr {position:absolute;bottom:-17px;right:0px;}*/
/* .header_s {position:relative;width:300px;height:auto;float:left;margin-right:25px;margin-bottom:45px;}*/
/* .header_s_nr {position:absolute;bottom:-22px;right:0px;}*/
/* .header_s:nth-child(2n+1) {clear: both;}*/
/* table.form {margin-bottom:0;}*/
/* table.form div {text-align:left}*/
/* table.form b {color:#003A88;font-size:13px}*/
/* table.form > tbody > tr > td:first-child {text-align:right}*/
/* a.btn-default.link {text-decoration:none;margin-left:5px;margin-right:5px;color:#222222;background-color:#F0F0F0;border-color:#F0F0F0;transition: all 0.15s ease-in 0s;position:relative;}*/
/* a.btn-default.link:hover {background-color:#DEDEDE;border-color:#DEDEDE;}*/
/* a.btn-default.link:last-child {padding-right:45px;}*/
/* a.btn-default.link i {font-size:14px;margin-right:5px;}*/
/* a.btn-default.link span.k_help_tip {position:absolute;top:5px;right:5px;}*/
/* .htabs {margin-top:15px;}*/
/* a.button-oxy-theme {background:#4BB8E2;color:#FFFFFF;padding:4px 12px;margin-left:5px;text-decoration:none;border-radius:3px;cursor:pointer;}*/
/* a.button-oxy-theme:hover {background:#ED5053;}*/
/* table.form {*/
/* 	width: 100%;*/
/* 	border-collapse: collapse;*/
/* 	margin-bottom: 20px;*/
/* }*/
/* table.form > tbody > tr > td:first-child {*/
/* 	width: 200px;*/
/* }*/
/* table.form > tbody > tr > td {*/
/* 	padding: 10px;*/
/* 	color: #000000;*/
/* 	border-bottom: 1px dotted #CCCCCC;*/
/* }*/
/* label.control-label span:after {*/
/* 	display: none;*/
/* }*/
/* legend {*/
/* 	background-color: #EEEEEE;*/
/* 	border: none;*/
/* 	border-radius: 3px;*/
/* }*/
/* fieldset legend {*/
/* 	margin-top: 20px;*/
/* 	padding: 20px 30px;*/
/* }*/
/* legend.bn {*/
/* 	border-color: #FFFFFF;*/
/* 	padding: 0;*/
/* 	margin: 0;*/
/* }*/
/* legend span {*/
/* 	font-size: 12px;*/
/* }*/
/* .nav-tabs {*/
/* 	border-bottom: 3px solid #1E91CF;*/
/* }*/
/* .nav-tabs > li {*/
/* 	margin-bottom: 0;*/
/* }*/
/* .nav-tabs > li > a:hover {*/
/* 	background-color: #E9E9E9;*/
/* 	border-color: #E9E9E9;*/
/* }*/
/* .nav-tabs > li > a {*/
/* 	padding: 15px 20px;*/
/* }*/
/* .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {*/
/* 	background-color: #1E91CF;*/
/* 	border-color: #1E91CF;*/
/* 	color: #FFFFFF;*/
/* 	font-weight: normal;*/
/* }*/
/* .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {*/
/* 	background-color: #1E91CF;*/
/* }*/
/* .nav > li > a {*/
/* 	background-color: #EEEEEE;*/
/* 	padding: 15px 20px;*/
/* }*/
/* .nav > li > a:hover {*/
/* 	background-color: #E9E9E9;*/
/* 	color: inherit;*/
/* }*/
/* html, body {color:#222222;}*/
/* a {color:#666666;transition: all 0.15s ease-in 0s;}*/
/* a:hover, a:focus {color:#1E91CF;}*/
/* label {font-weight:normal;}*/
/* .form-control {display:inline-block;min-width:77px;}*/
/* .form-horizontal .form-group {*/
/* 	margin-left: 0;*/
/* 	margin-right: 0;*/
/* }*/
/* .form-group + .form-group {border-top: 1px dotted #EDEDED;}*/
/* .tab-content .form-horizontal label.col-sm-2{*/
/* 	padding-left: 0;*/
/* }*/
/* textarea.form-control {width:100%;}*/
/* .panel-heading i {font-size: 14px;}*/
/* .table thead > tr > td, .table tbody > tr > td {vertical-align:top;}*/
/* </style>    */
/* */
/* {{ column_left }}*/
/* */
/* <div id="content">*/
/* */
/*   <div class="page-header">*/
/*     <div class="container-fluid">*/
/*       <div class="pull-right">*/
/*         <button type="submit" form="form-oxy-theme" data-toggle="tooltip" title="{{ button_save }}" class="btn btn-primary"><i class="fa fa-save"></i></button>*/
/*         <a href="{{ cancel }}" data-toggle="tooltip" title="{{ button_cancel }}" class="btn btn-default"><i class="fa fa-reply"></i></a></div>*/
/*       <h1>{{ heading_title }}</h1>*/
/*       <ul class="breadcrumb">*/
/*         {% for breadcrumb in breadcrumbs %}*/
/*         <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*     </div>*/
/*   </div>*/
/*   <div class="container-fluid">*/
/*   {% if error_warning %}*/
/*   <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ error_warning }}*/
/*     <button type="button" class="close" data-dismiss="alert">&times;</button>*/
/*   </div>*/
/*   {% endif %}*/
/*   <div class="panel panel-default">*/
/*     <div class="panel-heading">*/
/* 		<h3 class="panel-title"><i class="fa fa-pencil"></i> {{ heading_title }}</h3>*/
/* 	</div>*/
/*     <div class="panel-body">*/
/*     */
/*     */
/* */
/*     <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-oxy-theme">*/
/*     */
/*     <input type="hidden" name="store_id" value="{{ store_id }}" />*/
/* */
/*         <div style="margin-top:10px; margin-bottom:25px;">*/
/*             <span style="margin-left:0px;">Useful links:</span> */
/*             <a href="http://oxy.321cart.com/documentation/" class="btn btn-default link" target="_blank"><i class="fa fa-book"></i> OXY Documentation</a>*/
/*             <a href="http://oxy.321cart.com/landing/" class="btn btn-default link" target="_blank"><i class="fa fa-television"></i> OXY Demos</a>*/
/*             <a href="http://support.321cart.com/system/" class="btn btn-default link" target="_blank"><i class="fa fa-support"></i> OXY Support <span class="k_help_tip_support k_help_tip k_tooltip" title="<br>If you need help, please contact us. We provide support only through our Support System.<br><br>Create a ticket and our support developer will respond as soon as possible.<br><br>Support requests are being processed on Monday to Friday.<br><br>" data-toggle="tooltip">?</span></a>*/
/*             */
/* */
/* 		</div>*/
/* */
/*     */
/*         <ul class="nav nav-tabs">*/
/*           <li class="active"><a href="#tab-options" data-toggle="tab">General Options</a></li>*/
/*           <li><a href="#tab-header" data-toggle="tab">Header</a></li>*/
/*           <li><a href="#tab-menu" data-toggle="tab">Main Menu</a></li>*/
/*           <li><a href="#tab-midsection" data-toggle="tab">Midsection</a></li>*/
/*           <li><a href="#tab-footer" data-toggle="tab">Footer</a></li>*/
/*           <li><a href="#tab-widgets" data-toggle="tab">Widgets</a></li>*/
/*           <li><a href="#tab-css" data-toggle="tab">Custom CSS/JavaScript</a></li>*/
/*           <li><a href="#tab-translate" data-toggle="tab">Theme Translate</a></li>*/
/*         </ul>*/
/*         */
/*         <div class="tab-content">*/
/*         <!-- -->*/
/*         */
/*         <div class="tab-pane active" id="tab-options"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="store_features_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-options-layout" data-toggle="tab">Layout</a></li>*/
/*              <li><a href="#tab-options-sliders" data-toggle="tab">Products Layout</a></li>*/
/*              <li><a href="#tab-options-others" data-toggle="tab">Others</a></li>                                       */
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-options-layout" class="tab-pane fade in active"> */
/*         */
/*                     <fieldset>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Layout style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_style" class="form-control">*/
/*                                     <option value="boxed"{% if t1o_layout_style == 'boxed' %}{{ "selected=\"selected\"" }}{% endif %}>Boxed</option>*/
/* 									<option value="framed"{% if t1o_layout_style == 'framed' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_layout_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Framed</option>*/
/*                                     <option value="full-width"{% if t1o_layout_style == 'full-width' %}{{ "selected=\"selected\"" }}{% endif %}>Full Width</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_01.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Maximum width:<br /><span class="k_help">for "Framed" layout style</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_l" class="form-control">*/
/* 									<option value="1"{% if t1o_layout_l == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1170px</option>*/
/*                                     <option value="2"{% if t1o_layout_l == '2' %}{{ "selected=\"selected\"" }}{% endif %}>980px</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Align layout to:<br /><span class="k_help">for "Framed" layout style</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_framed_align" class="form-control">*/
/*                                     <option value="0"{% if t1o_layout_framed_align == '0' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/* 									<option value="1"{% if t1o_layout_framed_align == '1' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Product Blocks maximum width:*/
/*                             <span class="k_help">for "Full Width"<br />layout styles</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_full_width_max" class="form-control">*/
/*                                     <option value="0"{% if t1o_layout_full_width_max == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Full Width</option>*/
/* 									<option value="1"{% if t1o_layout_full_width_max == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_layout_full_width_max == '' %}{{ "selected=\"selected\"" }}{% endif %}>1440px</option>*/
/*                                     <option value="2"{% if t1o_layout_full_width_max == '2' %}{{ "selected=\"selected\"" }}{% endif %}>1170px</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_01.jpg" target="_blank"><span class="k_help_tip">?</span></a> */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Align Headings to:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_h_align" class="form-control">*/
/* 									<option value="0"{% if t1o_layout_h_align == '0' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_c }}</option>*/
/*                                     <option value="1"{% if t1o_layout_h_align == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_layout_h_align == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_position_l }}</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Catalog Mode:<br /><span class="k_help">excludes purchase options</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_catalog_mode" class="form-control">*/
/* 								    <option value="0"{% if t1o_layout_catalog_mode == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_layout_catalog_mode == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option> */
/* 								</select>  */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-options-sliders" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/*                     */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Bestseller view:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_bestseller_style" class="form-control">*/
/* 									<option value="0"{% if t1o_bestseller_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_bestseller_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_bestseller_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Featured view:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_featured_style" class="form-control">*/
/* 									<option value="0"{% if t1o_featured_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_featured_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_featured_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Latest view:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_latest_style" class="form-control">*/
/* 									<option value="0"{% if t1o_latest_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_latest_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_latest_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Specials view:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_specials_style" class="form-control">*/
/* 									<option value="0"{% if t1o_specials_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_specials_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_specials_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">OXY Theme Most Viewed view:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_most_viewed_style" class="form-control">*/
/* 									<option value="0"{% if t1o_most_viewed_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_most_viewed_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_most_viewed_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">OXY Theme Product Tabs view:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_tabs_style" class="form-control">*/
/* 									<option value="0"{% if t1o_product_tabs_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_product_tabs_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_tabs_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<legend>Grid View <a href="view/image/theme_img/help_oxy_theme/go_02.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/* 						*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Products per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_grid_per_row" class="form-control">*/
/* 									<option value="12"{% if t1o_product_grid_per_row == '12' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="6"{% if t1o_product_grid_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="4"{% if t1o_product_grid_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="3"{% if t1o_product_grid_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_grid_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="15"{% if t1o_product_grid_per_row == '15' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="2"{% if t1o_product_grid_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Slider View <a href="view/image/theme_img/help_oxy_theme/go_02.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Bestsellers per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_bestseller_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_bestseller_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_bestseller_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_bestseller_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_bestseller_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_bestseller_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_bestseller_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_bestseller_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Featured per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_featured_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_featured_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_featured_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_featured_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_featured_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_featured_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_featured_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_featured_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Latest per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_latest_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_latest_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_latest_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_latest_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_latest_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_latest_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_latest_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_latest_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Specials per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_specials_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_specials_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_specials_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_specials_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_specials_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_specials_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_specials_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_specials_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">OXY Theme Most Viewed per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_most_viewed_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_most_viewed_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_most_viewed_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_most_viewed_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_most_viewed_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_most_viewed_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_most_viewed_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_most_viewed_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">OXY Theme Products Tabs - Products per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_tabs_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_product_tabs_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_product_tabs_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_product_tabs_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_product_tabs_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_product_tabs_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_product_tabs_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_tabs_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Related Products per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_related_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_related_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_related_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_related_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_related_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_related_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_related_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_related_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Carousel Items per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_carousel_items_per_row" class="form-control">*/
/* 									<option value="1"{% if t1o_carousel_items_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_carousel_items_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_carousel_items_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_carousel_items_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_carousel_items_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_carousel_items_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_carousel_items_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-options-others" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show "Scroll To Top" button:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_others_totop" class="form-control">*/
/*                                     <option value="0"{% if t1o_others_totop == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_others_totop == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_others_totop == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>    */
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_03.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*  */
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-header"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="store_features_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-header-general" data-toggle="tab">General</a></li>*/
/*              <li><a href="#tab-header-logo-creator" data-toggle="tab">Logo Creator</a></li>*/
/*              <li><a href="#tab-header-fixed-header" data-toggle="tab">Fixed Header</a></li>*/
/*              <li><a href="#tab-header-top-bar" data-toggle="tab">Top Bar</a></li>*/
/*              <li><a href="#tab-header-top-promo-bar" data-toggle="tab">Promo Message Bar</a></li>*/
/*              <li><a href="#tab-header-news" data-toggle="tab">News</a></li>*/
/*              <li><a href="#tab-header-custom-block" data-toggle="tab">Header Custom Block</a></li>*/
/*              <li><a href="#tab-header-search" data-toggle="tab">Search</a></li>                         */
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-header-general" class="tab-pane fade in active">*/
/*         */
/*                     <fieldset>*/
/*                     */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Header Style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_header_style" class="form-control">*/
/* 									<option value="header-style-1"{% if t1o_header_style == 'header-style-1' %}{{ "selected=\"selected\"" }}{% endif %}>Style 1</option>*/
/*                                     <option value="header-style-2"{% if t1o_header_style == 'header-style-2' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_header_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Style 2</option>*/
/*                                     <option value="header-style-3"{% if t1o_header_style == 'header-style-3' %}{{ "selected=\"selected\"" }}{% endif %}>Style 3</option>*/
/*                                     <option value="header-style-4"{% if t1o_header_style == 'header-style-4' %}{{ "selected=\"selected\"" }}{% endif %}>Style 4</option>*/
/*                                     <option value="header-style-5"{% if t1o_header_style == 'header-style-5' %}{{ "selected=\"selected\"" }}{% endif %}>Style 5</option>               */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/*                             <div class="col-sm-12">*/
/*                                 {% for hs in 1..5 %}*/
/*                                 <div class="header_s"><img src="view/image/theme_img/hs_{{ hs }}.png"><span class="t1o_help header_s_nr">Style {{ hs }}</span></div><br />*/
/* 						        {% endfor %}*/
/* 						    </div>   */
/*                         </div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/*         */
/*         <div id="tab-header-logo-creator" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                     */
/*                         <span class="k_help">Here you can create your own simple logo. If you have a logo in a graphical format, you can add it here: <b>System > Settings > Image > Store Logo</b></span>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Logo Text:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_logo[{{ language.language_id }}]" id="t1o_text_logo_{{ language.language_id }}" value="{{ t1o_text_logo[language.language_id] ? t1o_text_logo[language.language_id] : '' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Logo Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_text_logo_color" id="t1o_text_logo_color" value="{{ t1o_text_logo_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Font Awesome Icon:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_text_logo_awesome_status" class="form-control">*/
/* 									<option value="0"{% if t1o_text_logo_awesome_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_text_logo_awesome_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1o_text_logo_awesome" value="{{ t1o_text_logo_awesome }}" class="form-control" />*/
/*                                 <span class="k_help">Enter the name of an icon, for example: <b>shopping-bag</b></span>*/
/*                                 <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_text_logo_awesome_color" id="t1o_text_logo_awesome_color" value="{{ t1o_text_logo_awesome_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-header-fixed-header" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Fixed Header:<br /><span class="k_help">Not available for Header Style 3</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_header_fixed_header_status" class="form-control">*/
/* 									<option value="0"{% if t1o_header_fixed_header_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_header_fixed_header_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_05.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-header-top-bar" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Top Bar <a href="view/image/theme_img/help_oxy_theme/go_04.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Top Bar:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_top_bar_status" class="form-control">*/
/* 									<option value="0"{% if t1o_top_bar_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_top_bar_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_top_bar_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show "Shopping Cart" link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_top_bar_cart_link_status" class="form-control">*/
/*                                     <option value="0"{% if t1o_top_bar_cart_link_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_top_bar_cart_link_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>  */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Welcome Message:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_top_bar_welcome_status" class="form-control">*/
/* 									<option value="0"{% if t1o_top_bar_welcome_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_top_bar_welcome_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon for Welcome Message:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_bar_welcome_awesome" value="{{ t1o_top_bar_welcome_awesome }}" class="form-control" />*/
/*                                 <span class="k_help">Enter the name of an icon, for example: <b>flag</b></span>*/
/*                                 <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 						    <label class="col-sm-2 control-label">Welcome Message:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 									<div class="input-group">*/
/* 										<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                         <input type="text" name="t1o_top_bar_welcome[{{ language.language_id }}]" id="t1o_top_bar_welcome_{{ language.language_id }}" value="{{ t1o_top_bar_welcome[language.language_id] ? t1o_top_bar_welcome[language.language_id] : '' }}" placeholder="Message" class="form-control" />  */
/* 									</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-header-top-promo-bar" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Promo Message Bar <a href="view/image/theme_img/help_oxy_theme/go_07.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Promo Message Bar:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_top_custom_block_status" class="form-control">*/
/* 									<option value="0"{% if t1o_top_custom_block_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_top_custom_block_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Promo Message Bar background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_custom_block_title_bar_bg_color" id="t1o_top_custom_block_title_bar_bg_color" value="{{ t1o_top_custom_block_title_bar_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Promo Message Bar background image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1o_top_custom_block_bar_bg_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_top_custom_block_bar_bg_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_top_custom_block_bar_bg" value="{{ t1o_top_custom_block_bar_bg }}" id="t1o_top_custom_block_bar_bg" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Font Awesome Icon:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_top_custom_block_awesome_status" class="form-control">*/
/* 									<option value="0"{% if t1o_top_custom_block_awesome_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_top_custom_block_awesome_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_custom_block_awesome" value="{{ t1o_top_custom_block_awesome }}" class="form-control" />*/
/*                                 <span class="k_help">Enter the name of an icon, for example: <b>tags</b></span>*/
/*                                 <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_custom_block_awesome_color" id="t1o_top_custom_block_awesome_color" value="{{ t1o_top_custom_block_awesome_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 						    <label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 									<div class="input-group">*/
/* 										<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                         <input type="text" name="t1o_top_custom_block_title[{{ language.language_id }}]" id="t1o_top_custom_block_title_{{ language.language_id }}" value="{{ t1o_top_custom_block_title[language.language_id] ? t1o_top_custom_block_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />  */
/* 									</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_custom_block_title_color" id="t1o_top_custom_block_title_color" value="{{ t1o_top_custom_block_title_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_top_custom_block_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_top_custom_block_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_top_custom_block_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_top_custom_block_content[{{ language.language_id }}][htmlcontent]" id="t1o_top_custom_block_content-{{ language.language_id }}">{{ t1o_top_custom_block_content[language.language_id] ? t1o_top_custom_block_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Background Image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1o_top_custom_block_bg_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_top_custom_block_bg_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_top_custom_block_bg" value="{{ t1o_top_custom_block_bg }}" id="t1o_top_custom_block_bg" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_custom_block_bg_color" id="t1o_top_custom_block_bg_color" value="{{ t1o_top_custom_block_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_top_custom_block_text_color" id="t1o_top_custom_block_text_color" value="{{ t1o_top_custom_block_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-header-news" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/* 						<legend>News <a href="view/image/theme_img/help_oxy_theme/go_43.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show News Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_news_status" class="form-control">*/
/* 									<option value="0"{% if t1o_news_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_news_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 								<thead>*/
/* 									<tr>*/
/*                                     <th class="left" width="10%">News</th>*/
/* 									<th class="left" width="10%">Show</th>*/
/* 									<th class="left" width="40%">Title</th>*/
/* 									<th class="left" width="40%">URL</th>*/
/* 									</tr>*/
/* 								</thead>*/
/*                                 <tbody>*/
/* 									{% for i in 1..10 %}*/
/*                                     <tr>*/
/* 									<td>News {{ i }}:</td>*/
/* 									<td>*/
/*                                     <select name="t1o_news[{{ i }}][status]" class="form-control">*/
/* 									    <option value="0"{% if t1o_news[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                         <option value="1"{% if t1o_news[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    	</select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     {% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_news[{{ i }}][{{ language.language_id }}][title]" id="t1o_news_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_news[i][language.language_id].title ? t1o_news[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" /> */
/* 										</div>*/
/* 									{% endfor %}*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <input type="text" name="t1o_news[{{ i }}][url]" value="{{ t1o_news[i].url }}" class="form-control" />*/
/*                                     </td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/*                                 </tbody>*/
/* 							</table>*/
/*                         */
/*                         </div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_news_bg_color" id="t1o_news_bg_color" value="{{ t1o_news_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Icons color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_news_icons_color" id="t1o_news_icons_color" value="{{ t1o_news_icons_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">"News" word color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_news_word_color" id="t1o_news_word_color" value="{{ t1o_news_word_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">News color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_news_color" id="t1o_news_color" value="{{ t1o_news_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">News color hover:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_news_hover_color" id="t1o_news_hover_color" value="{{ t1o_news_hover_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-header-custom-block" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Header Custom Block (for Header Style 2) <a href="view/image/theme_img/help_oxy_theme/go_07.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Header Custom Block: </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_header_custom_block_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_header_custom_block_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_header_custom_block_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_header_custom_block_1_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_header_custom_block_1_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_header_custom_block_1_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_header_custom_block_1_content[{{ language.language_id }}][htmlcontent]" id="t1o_header_custom_block_1_content-{{ language.language_id }}">{{ t1o_header_custom_block_1_content[language.language_id] ? t1o_header_custom_block_1_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/*                                     */
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-header-search" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                     */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Quick Search Auto-Suggest:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_header_auto_suggest_status" class="form-control">*/
/* 									<option value="0"{% if t1o_header_auto_suggest_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_header_auto_suggest_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_header_auto_suggest_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>    */
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_06.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*    */
/*                         <legend>Popular Search <a href="view/image/theme_img/help_oxy_theme/go_07.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Popular Search: </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_header_popular_search_status" class="form-control"> */
/*                                     <option value="0"{% if t1o_header_popular_search_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_header_popular_search_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         {% for i in 1..20 %}*/
/*                         <div class="form-group">*/
/* 						    <label class="col-sm-2 control-label">Popular Search {{ i }}:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}	*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_header_popular_search[{{ i }}][{{ language.language_id }}][word]" id="t1o_header_popular_search_{{ i }}_{{ language.language_id }}_word" value="{{ t1o_header_popular_search[i][language.language_id].word ? t1o_header_popular_search[i][language.language_id].word : '' }}" placeholder="eg. Jeans" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         {% endfor %}*/
/* */
/* 					</fieldset>   */
/*         </div>*/
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/* */
/*         */
/*         */
/*         */
/* */
/*         <div class="tab-pane" id="tab-menu"> */
/*         <div class="row form-horizontal"> */
/* */
/*         <div class="col-sm-2">    */
/*         <ul id="store_features_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-menu-general" data-toggle="tab">General</a></li>*/
/*              <li><a href="#tab-menu-home-link" data-toggle="tab">Home Link</a></li>*/
/*              <li><a href="#tab-menu-categories" data-toggle="tab">Categories</a></li>*/
/*              <li><a href="#tab-menu-brands" data-toggle="tab">Brands</a></li>*/
/*              <li><a href="#tab-menu-custom-blocks" data-toggle="tab">Custom Blocks</a></li>*/
/*              <li><a href="#tab-menu-custom-dropdown-menu" data-toggle="tab">Custom Dropdown Menus</a></li>*/
/*              <li><a href="#tab-menu-custom-links" data-toggle="tab">Custom Links</a></li>*/
/*              <li><a href="#tab-menu-labels" data-toggle="tab">Menu Labels</a></li>*/
/*              <li><a href="#tab-menu-custom-bar" data-toggle="tab">Custom Bar</a></li>                        */
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-menu-general" class="tab-pane fade in active">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend class="bn"></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Align Menu Items to:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_align" class="form-control">*/
/*                                     <option value="left"{% if t1o_menu_align == 'left' %}{{ "selected=\"selected\"" }}{% endif %}>Left</option>*/
/*                                     <option value="center"{% if t1o_menu_align == 'center' %}{{ "selected=\"selected\"" }}{% endif %}>Center</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-menu-home-link" class="tab-pane">*/
/*         */
/*                     <fieldset>                        */
/*                     */
/*                         <legend>Home Page Link</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Home Page Link style:*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_homepage" class="form-control">*/
/*                                     <option value="dontshow"{% if t1o_menu_homepage == 'dontshow' %}{{ "selected=\"selected\"" }}{% endif %}>Don't show</option>*/
/*                                     <option value="text"{% if t1o_menu_homepage == 'text' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_homepage == '' %}{{ "selected=\"selected\"" }}{% endif %}>Text</option>*/
/* 									<option value="icon"{% if t1o_menu_homepage == 'icon' %}{{ "selected=\"selected\"" }}{% endif %}>Icon</option> */
/*                                     <option value="icontext"{% if t1o_menu_homepage == 'icontext' %}{{ "selected=\"selected\"" }}{% endif %}>Icon + Text</option>          */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-menu-categories" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Categories</legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Categories:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_categories_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_categories_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_categories_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>   */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Categories display style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_style" class="form-control">*/
/* 									<option value="1"{% if t1o_menu_categories_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_opencart }}</option>*/
/*                                     <option value="2"{% if t1o_menu_categories_style == '2' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_vertical }}</option>*/
/*                                     <option value="5"{% if t1o_menu_categories_style == '5' %}{{ "selected=\"selected\"" }}{% endif %}>Vertical 2</option>*/
/*                                     <option value="3"{% if t1o_menu_categories_style == '3' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_categories_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_horizontal }}</option>*/
/*                                     <option value="4"{% if t1o_menu_categories_style == '4' %}{{ "selected=\"selected\"" }}{% endif %}>Inline</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_08.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show category images:<br /><span class="k_help">for Horizontal, Vertical, Vertical 2 and OpenCart styles</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_main_category_icon_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_main_category_icon_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_main_category_icon_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Category dropdowns padding right:<br /><span class="k_help">for Vertical and OpenCart styles</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_main_category_padding_right" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_main_category_padding_right == '0' %}{{ "selected=\"selected\"" }}{% endif %}>0px</option>*/
/*                                     <option value="10"{% if t1o_menu_main_category_padding_right == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                     <option value="20"{% if t1o_menu_main_category_padding_right == '20' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option>*/
/*                                     <option value="30"{% if t1o_menu_main_category_padding_right == '30' %}{{ "selected=\"selected\"" }}{% endif %}>30px</option>*/
/*                                     <option value="40"{% if t1o_menu_main_category_padding_right == '40' %}{{ "selected=\"selected\"" }}{% endif %}>40px</option>*/
/*                                     <option value="50"{% if t1o_menu_main_category_padding_right == '50' %}{{ "selected=\"selected\"" }}{% endif %}>50px</option>*/
/*                                     <option value="60"{% if t1o_menu_main_category_padding_right == '60' %}{{ "selected=\"selected\"" }}{% endif %}>60px</option>*/
/*                                     <option value="70"{% if t1o_menu_main_category_padding_right == '70' %}{{ "selected=\"selected\"" }}{% endif %}>70px</option>*/
/*                                     <option value="80"{% if t1o_menu_main_category_padding_right == '80' %}{{ "selected=\"selected\"" }}{% endif %}>80px</option>*/
/*                                     <option value="90"{% if t1o_menu_main_category_padding_right == '90' %}{{ "selected=\"selected\"" }}{% endif %}>90px</option>*/
/*                                     <option value="100"{% if t1o_menu_main_category_padding_right == '100' %}{{ "selected=\"selected\"" }}{% endif %}>100px</option>*/
/*                                     <option value="110"{% if t1o_menu_main_category_padding_right == '110' %}{{ "selected=\"selected\"" }}{% endif %}>110px</option>*/
/*                                     <option value="120"{% if t1o_menu_main_category_padding_right == '120' %}{{ "selected=\"selected\"" }}{% endif %}>120px</option>*/
/*                                     <option value="130"{% if t1o_menu_main_category_padding_right == '130' %}{{ "selected=\"selected\"" }}{% endif %}>130px</option>*/
/*                                     <option value="140"{% if t1o_menu_main_category_padding_right == '140' %}{{ "selected=\"selected\"" }}{% endif %}>140px</option>*/
/*                                     <option value="150"{% if t1o_menu_main_category_padding_right == '150' %}{{ "selected=\"selected\"" }}{% endif %}>150px</option>*/
/*                                     <option value="160"{% if t1o_menu_main_category_padding_right == '160' %}{{ "selected=\"selected\"" }}{% endif %}>160px</option>*/
/*                                     <option value="170"{% if t1o_menu_main_category_padding_right == '170' %}{{ "selected=\"selected\"" }}{% endif %}>170px</option>*/
/*                                     <option value="180"{% if t1o_menu_main_category_padding_right == '180' %}{{ "selected=\"selected\"" }}{% endif %}>180px</option>*/
/*                                     <option value="190"{% if t1o_menu_main_category_padding_right == '190' %}{{ "selected=\"selected\"" }}{% endif %}>190px</option>*/
/*                                     <option value="200"{% if t1o_menu_main_category_padding_right == '200' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_main_category_padding_right == '' %}{{ "selected=\"selected\"" }}{% endif %}>200px</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_09.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Category dropdowns padding bottom:<br /><span class="k_help">for Vertical and OpenCart styles</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_main_category_padding_bottom" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_main_category_padding_bottom == '0' %}{{ "selected=\"selected\"" }}{% endif %}>0px</option>*/
/*                                     <option value="10"{% if t1o_menu_main_category_padding_bottom == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10px</option>*/
/*                                     <option value="20"{% if t1o_menu_main_category_padding_bottom == '20' %}{{ "selected=\"selected\"" }}{% endif %}>20px</option>*/
/*                                     <option value="30"{% if t1o_menu_main_category_padding_bottom == '30' %}{{ "selected=\"selected\"" }}{% endif %}>30px</option>*/
/*                                     <option value="40"{% if t1o_menu_main_category_padding_bottom == '40' %}{{ "selected=\"selected\"" }}{% endif %}>40px</option>*/
/*                                     <option value="50"{% if t1o_menu_main_category_padding_bottom == '50' %}{{ "selected=\"selected\"" }}{% endif %}>50px</option>*/
/*                                     <option value="60"{% if t1o_menu_main_category_padding_bottom == '60' %}{{ "selected=\"selected\"" }}{% endif %}>60px</option>*/
/*                                     <option value="70"{% if t1o_menu_main_category_padding_bottom == '70' %}{{ "selected=\"selected\"" }}{% endif %}>70px</option>*/
/*                                     <option value="80"{% if t1o_menu_main_category_padding_bottom == '80' %}{{ "selected=\"selected\"" }}{% endif %}>80px</option>*/
/*                                     <option value="90"{% if t1o_menu_main_category_padding_bottom == '90' %}{{ "selected=\"selected\"" }}{% endif %}>90px</option>*/
/*                                     <option value="100"{% if t1o_menu_main_category_padding_bottom == '100' %}{{ "selected=\"selected\"" }}{% endif %}>100px</option>*/
/*                                     <option value="110"{% if t1o_menu_main_category_padding_bottom == '110' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_main_category_padding_bottom == '' %}{{ "selected=\"selected\"" }}{% endif %}>110px</option>*/
/*                                     <option value="120"{% if t1o_menu_main_category_padding_bottom == '120' %}{{ "selected=\"selected\"" }}{% endif %}>120px</option>*/
/*                                     <option value="130"{% if t1o_menu_main_category_padding_bottom == '130' %}{{ "selected=\"selected\"" }}{% endif %}>130px</option>*/
/*                                     <option value="140"{% if t1o_menu_main_category_padding_bottom == '140' %}{{ "selected=\"selected\"" }}{% endif %}>140px</option>*/
/*                                     <option value="150"{% if t1o_menu_main_category_padding_bottom == '150' %}{{ "selected=\"selected\"" }}{% endif %}>150px</option>*/
/*                                     <option value="160"{% if t1o_menu_main_category_padding_bottom == '160' %}{{ "selected=\"selected\"" }}{% endif %}>160px</option>*/
/*                                     <option value="170"{% if t1o_menu_main_category_padding_bottom == '170' %}{{ "selected=\"selected\"" }}{% endif %}>170px</option>*/
/*                                     <option value="180"{% if t1o_menu_main_category_padding_bottom == '180' %}{{ "selected=\"selected\"" }}{% endif %}>180px</option>*/
/*                                     <option value="190"{% if t1o_menu_main_category_padding_bottom == '190' %}{{ "selected=\"selected\"" }}{% endif %}>190px</option>*/
/*                                     <option value="200"{% if t1o_menu_main_category_padding_bottom == '200' %}{{ "selected=\"selected\"" }}{% endif %}>200px</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_09.jpg" target="_blank"><span class="k_help_tip">?</span></a> */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Categories per row:<br /><span class="k_help">for Horizontal style</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_per_row" class="form-control">*/
/* 									<option value="col-sm-4"{% if t1o_menu_categories_per_row == 'col-sm-4' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                                     <option value="col-sm-3"{% if t1o_menu_categories_per_row == 'col-sm-3' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                                     <option value="col-sm-5-pr"{% if t1o_menu_categories_per_row == 'col-sm-5-pr' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                                     <option value="col-sm-2"{% if t1o_menu_categories_per_row == 'col-sm-2' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_categories_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/*                                     <option value="col-sm-7-pr"{% if t1o_menu_categories_per_row == 'col-sm-7-pr' %}{{ "selected=\"selected\"" }}{% endif %}>7</option>*/
/*                                     <option value="col-sm-8-pr"{% if t1o_menu_categories_per_row == 'col-sm-8-pr' %}{{ "selected=\"selected\"" }}{% endif %}>8</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show 3 level category:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_3_level" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_categories_3_level == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_categories_3_level == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Category dropdown on Home Page:<br /><span class="k_help">for "Vertical 2" style</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_home_visibility" class="form-control">*/
/*                                     <option value="0"{% if t1o_menu_categories_home_visibility == '0' %}{{ "selected=\"selected\"" }}{% endif %}>On Hover</option>*/
/*                                     <option value="1"{% if t1o_menu_categories_home_visibility == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_categories_home_visibility == '' %}{{ "selected=\"selected\"" }}{% endif %}>Always Visible</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_08.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Custom Blocks for Horizontal style <a href="view/image/theme_img/help_oxy_theme/go_11.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Left Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_custom_block_left_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_categories_custom_block_left_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_categories_custom_block_left_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option> */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Left Custom Block Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_menu_categories_custom_block_left_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_menu_categories_custom_block_left_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_menu_categories_custom_block_left_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_menu_categories_custom_block_left_content[{{ language.language_id }}][htmlcontent]" id="t1o_menu_categories_custom_block_left_content-{{ language.language_id }}">{{ t1o_menu_categories_custom_block_left_content[language.language_id] ? t1o_menu_categories_custom_block_left_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Right Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_custom_block_right_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_categories_custom_block_right_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_categories_custom_block_right_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Right Custom Block Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_menu_categories_custom_block_right_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_menu_categories_custom_block_right_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_menu_categories_custom_block_right_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_menu_categories_custom_block_right_content[{{ language.language_id }}][htmlcontent]" id="t1o_menu_categories_custom_block_right_content-{{ language.language_id }}">{{ t1o_menu_categories_custom_block_right_content[language.language_id] ? t1o_menu_categories_custom_block_right_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Bottom Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_categories_custom_block_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_categories_custom_block_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_categories_custom_block_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Bottom Custom Block Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_menu_categories_custom_block_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_menu_categories_custom_block_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_menu_categories_custom_block_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_menu_categories_custom_block_content[{{ language.language_id }}][htmlcontent]" id="t1o_menu_categories_custom_block_content-{{ language.language_id }}">{{ t1o_menu_categories_custom_block_content[language.language_id] ? t1o_menu_categories_custom_block_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-menu-brands" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Brands <span class="k_help_tip k_tooltip" title="Before you turn on this option, add at least one manufacturer." data-toggle="tooltip">?</span></legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Brands:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_brands_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_brands_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_menu_brands_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_brands_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Brands display style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_brands_style" class="form-control">*/
/* 									<option value="logoname"{% if t1o_menu_brands_style == 'logoname' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_brand_logo_name }}</option>  */
/*                                     <option value="logo"{% if t1o_menu_brands_style == 'logo' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_brand_logo }}</option>*/
/*                                     <option value="name"{% if t1o_menu_brands_style == 'name' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_brand_name }}</option>*/
/*                                              */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Brands per row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_brands_per_row" class="form-control">*/
/* 									<option value="4"{% if t1o_menu_brands_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                                     <option value="3"{% if t1o_menu_brands_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>4</option>*/
/*                                     <option value="2"{% if t1o_menu_brands_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/*                                     <option value="8-pr"{% if t1o_menu_brands_per_row == '8-pr' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_menu_brands_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>8</option>*/
/*                                     <option value="10-pr"{% if t1o_menu_brands_per_row == '10-pr' %}{{ "selected=\"selected\"" }}{% endif %}>10</option>*/
/*                                     <option value="1"{% if t1o_menu_brands_per_row == '1' %}{{ "selected=\"selected\"" }}{% endif %}>12</option>         */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-menu-custom-blocks" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         {% for i in 1..5 %}*/
/*                         */
/*                         <legend>Custom Block {{ i }}<a href="view/image/theme_img/help_oxy_theme/go_13.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Block {{ i }}:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_custom_block[{{ i }}][status]" class="form-control">*/
/* 								    <option value="0"{% if t1o_menu_custom_block[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_menu_custom_block[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_menu_custom_block[{{ i }}][{{ language.language_id }}][title]" id="t1o_menu_custom_block_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_menu_custom_block[i][language.language_id].title ? t1o_menu_custom_block[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_menu_custom_block_{{ i }}" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_menu_custom_block_{{ i }}_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_menu_custom_block_{{ i }}_{{ language.language_id }}" class="tab-pane">*/
/*                                             <textarea name="t1o_menu_custom_block[{{ i }}][{{ language.language_id }}][htmlcontent]" id="t1o_menu_custom_block_{{ i }}_{{ language.language_id }}_content">{{ t1o_menu_custom_block[i][language.language_id] ? t1o_menu_custom_block[i][language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         {% endfor %}*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/*         */
/*         <div id="tab-menu-custom-dropdown-menu" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Custom Dropdown Menu 1<a href="view/image/theme_img/help_oxy_theme/go_12.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Menu 1:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_cm_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_cm_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_menu_cm_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 						    <label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 									<div class="input-group">*/
/* 										<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                         <input type="text" name="t1o_menu_cm_title[{{ language.language_id }}]" id="t1o_menu_cm_title_{{ language.language_id }}" value="{{ t1o_menu_cm_title[language.language_id] ? t1o_menu_cm_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />  */
/* 									</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 								<thead>*/
/* 									<tr>*/
/*                                     <th class="left" width="10%">Link</th>*/
/* 									<th class="left" width="10%">Show</th>*/
/* 									<th class="left" width="30%">Title</th>*/
/* 									<th class="left" width="30%">URL</th>*/
/* 									<th class="left" width="20%">Open</th>*/
/* 									</tr>*/
/* 								</thead>*/
/*                                 <tbody>*/
/* 									{% for i in 1..10 %}*/
/*                                     <tr>*/
/* 									<td>Link {{ i }}:</td>*/
/* 									<td>*/
/*     */
/*                                     <select name="t1o_menu_cm_link[{{ i }}][status]" class="form-control">*/
/* 									    <option value="0"{% if t1o_menu_cm_link[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                         <option value="1"{% if t1o_menu_cm_link[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    	</select>*/
/* */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     {% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_menu_cm_link[{{ i }}][{{ language.language_id }}][title]" id="t1o_menu_cm_link_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_menu_cm_link[i][language.language_id].title ? t1o_menu_cm_link[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     <input type="text" name="t1o_menu_cm_link[{{ i }}][url]" value="{{ t1o_menu_cm_link[i].url }}" class="form-control" />*/
/*                                     */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     <select name="t1o_menu_cm_link[{{ i }}][target]" class="form-control">*/
/* 										<option value="_self"{% if t1o_menu_cm_link[i].target == '_self' %}{{ "selected=\"selected\"" }}{% endif %}>in the same frame</option> */
/*                                     	<option value="_blank"{% if t1o_menu_cm_link[i].target == '_blank' %}{{ "selected=\"selected\"" }}{% endif %}>in a new tab</option>*/
/* 									</select>*/
/* */
/*                                     </td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/*                                 </tbody>*/
/* 							</table>*/
/*                         */
/*                         </div>*/
/*                         */
/*                         <legend>Custom Dropdown Menu 2</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Menu 2:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_cm_2_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_cm_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_menu_cm_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 						    <label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 									<div class="input-group">*/
/* 										<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                         <input type="text" name="t1o_menu_cm_2_title[{{ language.language_id }}]" id="t1o_menu_cm_2_title_{{ language.language_id }}" value="{{ t1o_menu_cm_2_title[language.language_id] ? t1o_menu_cm_2_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />  */
/* 									</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 								<thead>*/
/* 									<tr>*/
/*                                     <th class="left" width="10%">Link</th>*/
/* 									<th class="left" width="10%">Show</th>*/
/* 									<th class="left" width="30%">Title</th>*/
/* 									<th class="left" width="30%">URL</th>*/
/* 									<th class="left" width="20%">Open</th>*/
/* 									</tr>*/
/* 								</thead>*/
/*                                 <tbody>*/
/* 									{% for i in 1..10 %}*/
/*                                     <tr>*/
/* 									<td>Link {{ i }}:</td>*/
/* 									<td>*/
/*     */
/*                                     <select name="t1o_menu_cm_2_link[{{ i }}][status]" class="form-control">*/
/* 									    <option value="0"{% if t1o_menu_cm_2_link[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                         <option value="1"{% if t1o_menu_cm_2_link[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    	</select>*/
/* */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     {% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_menu_cm_2_link[{{ i }}][{{ language.language_id }}][title]" id="t1o_menu_cm_2_link_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_menu_cm_2_link[i][language.language_id].title ? t1o_menu_cm_2_link[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     <input type="text" name="t1o_menu_cm_2_link[{{ i }}][url]" value="{{ t1o_menu_cm_2_link[i].url }}" class="form-control" />*/
/*                                     */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     <select name="t1o_menu_cm_2_link[{{ i }}][target]" class="form-control">*/
/* 										<option value="_self"{% if t1o_menu_cm_2_link[i].target == '_self' %}{{ "selected=\"selected\"" }}{% endif %}>in the same frame</option> */
/*                                     	<option value="_blank"{% if t1o_menu_cm_2_link[i].target == '_blank' %}{{ "selected=\"selected\"" }}{% endif %}>in a new tab</option>*/
/* 									</select>*/
/*                                     */
/*                                     </td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/*                                 </tbody>*/
/* 							</table>*/
/*                         */
/*                         </div>*/
/*                         */
/*                         <legend>Custom Dropdown Menu 3</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Menu 3:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_menu_cm_3_status" class="form-control">*/
/* 									<option value="0"{% if t1o_menu_cm_3_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_menu_cm_3_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 						    <label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 									<div class="input-group">*/
/* 										<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                         <input type="text" name="t1o_menu_cm_3_title[{{ language.language_id }}]" id="t1o_menu_cm_3_title_{{ language.language_id }}" value="{{ t1o_menu_cm_3_title[language.language_id] ? t1o_menu_cm_3_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />  */
/* 									</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 								<thead>*/
/* 									<tr>*/
/*                                     <th class="left" width="10%">Link</th>*/
/* 									<th class="left" width="10%">Show</th>*/
/* 									<th class="left" width="30%">Title</th>*/
/* 									<th class="left" width="30%">URL</th>*/
/* 									<th class="left" width="20%">Open</th>*/
/* 									</tr>*/
/* 								</thead>*/
/*                                 <tbody>*/
/* 									{% for i in 1..10 %}*/
/*                                     <tr>*/
/* 									<td>Link {{ i }}:</td>*/
/* 									<td>*/
/*     */
/*                                     <select name="t1o_menu_cm_3_link[{{ i }}][status]" class="form-control">*/
/* 									    <option value="0"{% if t1o_menu_cm_3_link[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                         <option value="1"{% if t1o_menu_cm_3_link[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    	</select>*/
/* */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     {% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_menu_cm_3_link[{{ i }}][{{ language.language_id }}][title]" id="t1o_menu_cm_3_link_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_menu_cm_3_link[i][language.language_id].title ? t1o_menu_cm_3_link[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 									{% endfor %}*/
/*                                     */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     <input type="text" name="t1o_menu_cm_3_link[{{ i }}][url]" value="{{ t1o_menu_cm_3_link[i].url }}" class="form-control" />*/
/*                                     */
/*                                     </td>*/
/*                                     <td>*/
/*                                     */
/*                                     <select name="t1o_menu_cm_3_link[{{ i }}][target]" class="form-control">*/
/* 										<option value="_self"{% if t1o_menu_cm_3_link[i].target == '_self' %}{{ "selected=\"selected\"" }}{% endif %}>in the same frame</option> */
/*                                     	<option value="_blank"{% if t1o_menu_cm_3_link[i].target == '_blank' %}{{ "selected=\"selected\"" }}{% endif %}>in a new tab</option>*/
/* 									</select>*/
/*                                     */
/*                                     </td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/*                                 </tbody>*/
/* 							</table>*/
/*                         */
/*                         </div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/*         */
/*         <div id="tab-menu-custom-links" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Custom Links <a href="view/image/theme_img/help_oxy_theme/go_11.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 								<thead>*/
/* 									<tr>*/
/*                                     <th class="left" width="10%">Link</th>*/
/* 									<th class="left" width="10%">Show</th>*/
/* 									<th class="left" width="30%">Title</th>*/
/* 									<th class="left" width="30%">URL</th>*/
/* 									<th class="left" width="20%">Open</th>*/
/* 									</tr>*/
/* 								</thead>*/
/*                                 <tbody>*/
/* 									{% for i in 1..10 %}*/
/*                                     <tr>*/
/* 									<td>Link {{ i }}:</td>*/
/* 									<td>*/
/*                                     <select name="t1o_menu_link[{{ i }}][status]" class="form-control">*/
/* 									    <option value="0"{% if t1o_menu_link[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                         <option value="1"{% if t1o_menu_link[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    	</select>*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     {% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_menu_link[{{ i }}][{{ language.language_id }}][title]" id="t1o_menu_link_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_menu_link[i][language.language_id].title ? t1o_menu_link[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" /> */
/* 										</div>*/
/* 									{% endfor %}*/
/*                                     </td>*/
/*                                     <td>*/
/*                                     <input type="text" name="t1o_menu_link[{{ i }}][url]" value="{{ t1o_menu_link[i].url }}" class="form-control" />*/
/*                                     </td>*/
/*                                     <td>*/
/* */
/*                                     <select name="t1o_menu_link[{{ i }}][target]" class="form-control">*/
/* 										<option value="_self"{% if t1o_menu_link[i].target == '_self' %}{{ "selected=\"selected\"" }}{% endif %}>in the same frame</option> */
/*                                     	<option value="_blank"{% if t1o_menu_link[i].target == '_blank' %}{{ "selected=\"selected\"" }}{% endif %}>in a new tab</option>*/
/* 									</select>*/
/*                                     */
/*                                     </td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/*                                 </tbody>*/
/* 							</table>*/
/*                         */
/*                         </div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-menu-labels" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Menu Labels <a href="view/image/theme_img/help_oxy_theme/go_14.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="table-responsive">*/
/*                         */
/* 							<table class="table table-hover">*/
/* 									{% for i in 1..15 %}*/
/*                                     <tr>*/
/* 									<td width="15%" style="text-align:right;">Menu Label {{ i }}:</td>*/
/* 									<td width="35%">*/
/*                                     {% for language in languages %}	*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_menu_labels[{{ i }}][{{ language.language_id }}][title]" id="t1o_menu_labels_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_menu_labels[i][language.language_id].title ? t1o_menu_labels[i][language.language_id].title : '' }}" placeholder="Label" class="form-control" />*/
/* 										</div>*/
/* 									{% endfor %}*/
/*                                     </td>*/
/*                                     <td width="15%" style="text-align:right;">Background color:</td>*/
/*                                     <td width="35%">*/
/*                                     <input type="text" name="t1o_menu_labels_color[{{ i }}]" id="t1o_menu_labels_color_[{{ i }}]" value="{{ t1o_menu_labels_color[i] ? t1o_menu_labels_color[i] : '#16B778' }}" class="color {required:false,hash:true}" size="8" />*/
/*                                     </td>*/
/*                                     </tr>*/
/*                                     {% endfor %}*/
/* 							</table>*/
/*                         */
/*                         </div>*/
/*                         */
/*                     </fieldset>*/
/*                     */
/*         </div>*/
/* */
/*         <div id="tab-menu-custom-bar" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Custom Bar below Main Menu <a href="view/image/theme_img/help_oxy_theme/go_15.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Bar:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_bar_below_menu_status" class="form-control">*/
/* 									<option value="0"{% if t1o_custom_bar_below_menu_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_bar_below_menu_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_bar_below_menu_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_custom_bar_below_menu_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_bar_below_menu_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_bar_below_menu_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_bar_below_menu_content-{{ language.language_id }}">{{ t1o_custom_bar_below_menu_content[language.language_id] ? t1o_custom_bar_below_menu_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Background Image:</label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1o_custom_bar_below_menu_bg_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_custom_bar_below_menu_bg_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_custom_bar_below_menu_bg" value="{{ t1o_custom_bar_below_menu_bg }}" id="t1o_custom_bar_below_menu_bg" />*/
/* 					        </div>*/
/* 				        </div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background Image Animation:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_bar_below_menu_bg_animation" class="form-control">*/
/* 									<option value="0"{% if t1o_custom_bar_below_menu_bg_animation == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_bar_below_menu_bg_animation == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_custom_bar_below_menu_bg_color" id="t1o_custom_bar_below_menu_bg_color" value="{{ t1o_custom_bar_below_menu_bg_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Text color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_custom_bar_below_menu_text_color" id="t1o_custom_bar_below_menu_text_color" value="{{ t1o_custom_bar_below_menu_text_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset> */
/*      */
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/* */
/* */
/* */
/* */
/*         <div class="tab-pane" id="tab-midsection"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="midsection_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-midsection-category" data-toggle="tab">Category Page</a></li>*/
/*              <li><a href="#tab-midsection-product" data-toggle="tab">Product Page</a></li>*/
/*              <li><a href="#tab-midsection-contact" data-toggle="tab">Contact Page</a></li>*/
/*              <li><a href="#tab-midsection-lf" data-toggle="tab">Left/Right Column</a></li>                                      */
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-midsection-category" class="tab-pane fade in active">  */
/*         */
/*                     <fieldset>*/
/*                     */
/*                         <legend>Category Title <a href="view/image/theme_img/help_oxy_theme/go_16.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Category Title position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_title_position" class="form-control">*/
/* 									<option value="0"{% if t1o_category_title_position == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Above Content Column</option>*/
/*                                     <option value="1"{% if t1o_category_title_position == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_title_position == '' %}{{ "selected=\"selected\"" }}{% endif %}>Content Column</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Category Title color:<br /><span class="k_help">for "Above Content Column" position</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_category_title_above_color" id="t1o_category_title_above_color" value="{{ t1o_category_title_above_color }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Category Info <a href="view/image/theme_img/help_oxy_theme/go_17.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show category description:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_desc_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_desc_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_desc_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_desc_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>  */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show category image:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_img_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_img_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_img_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/*                             <label class="col-sm-2 control-label">Parallax scrolling effect:<br /><span class="k_help">for "Above Content Column" category title position</span></label>*/
/*                             <div class="col-sm-10">*/
/* 								<select name="t1o_category_img_parallax" class="form-control">*/
/* 									<option value="0"{% if t1o_category_img_parallax == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_img_parallax == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_18.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 				        </div>*/
/*                         */
/*                         <legend>Subcategories <a href="view/image/theme_img/help_oxy_theme/go_19.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show subcategories:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_subcategories_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_subcategories_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_subcategories_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_subcategories_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subcategories style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_subcategories_style" class="form-control">*/
/* 									<option value="0"{% if t1o_category_subcategories_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>OXY Theme</option>*/
/*                                     <option value="1"{% if t1o_category_subcategories_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}>OpenCart</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subcategories per row:<br /><span class="k_help">for "OXY Theme" style</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_subcategories_per_row" class="form-control">*/
/*                            			<option value="2"{% if t1o_category_subcategories_per_row == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_category_subcategories_per_row == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_category_subcategories_per_row == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_category_subcategories_per_row == '5' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_subcategories_per_row == '' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_category_subcategories_per_row == '6' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/*                                     <option value="7"{% if t1o_category_subcategories_per_row == '7' %}{{ "selected=\"selected\"" }}{% endif %}>7</option>*/
/*                                     <option value="8"{% if t1o_category_subcategories_per_row == '8' %}{{ "selected=\"selected\"" }}{% endif %}>8</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">AutoPlay:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_subcategories_autoplay" class="form-control">*/
/* 									<option value="4000"{% if t1o_category_subcategories_autoplay == '4000' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/*                                     <option value="false"{% if t1o_category_subcategories_autoplay == 'false' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<legend>Product Box <a href="view/image/theme_img/help_oxy_theme/go_20.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Product Box style:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_box_style" class="form-control">*/
/*                                     <option value="product-box-style-1"{% if t1o_category_prod_box_style == 'product-box-style-1' %}{{ "selected=\"selected\"" }}{% endif %}>Style 1</option>*/
/* 									<option value="product-box-style-2"{% if t1o_category_prod_box_style == 'product-box-style-2' %}{{ "selected=\"selected\"" }}{% endif %}>Style 2</option>*/
/*                                     <option value="product-box-style-3"{% if t1o_category_prod_box_style == 'product-box-style-3' %}{{ "selected=\"selected\"" }}{% endif %}>Style 3</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_01.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show sale badge:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_sale_badge_status" class="form-control">*/
/* 									<option value="0"{% if t1o_sale_badge_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_sale_badge_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_sale_badge_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/*                                     */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Sale badge type:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_sale_badge_type" class="form-control">*/
/* 									<option value="0"{% if t1o_sale_badge_type == '0' %}{{ "selected=\"selected\"" }}{% endif %}>SALE text</option>*/
/*                                     <option value="1"{% if t1o_sale_badge_type == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_sale_badge_type == '' %}{{ "selected=\"selected\"" }}{% endif %}>Percent</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                                        */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show new product badge:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_new_badge_status" class="form-control">*/
/* 									<option value="0"{% if t1o_new_badge_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_new_badge_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_new_badge_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show "Out of Stock" badge:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_out_of_stock_badge_status" class="form-control">*/
/* 									<option value="0"{% if t1o_out_of_stock_badge_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_out_of_stock_badge_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_out_of_stock_badge_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show product name:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_name_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_name_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_name_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_name_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show product brand:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_brand_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_brand_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_brand_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_brand_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show product price:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_price_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_price_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_price_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_price_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option> */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show "Quick View" button:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_quickview_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_quickview_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_quickview_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_quickview_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show "Add to Cart" button:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_cart_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_cart_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_cart_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_cart_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show rating stars:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_ratings_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_ratings_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_ratings_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_ratings_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show "Add to Wishlist", "Add to Compare" buttons:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_wis_com_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_wis_com_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_wis_com_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_wis_com_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show zoom image effect:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_zoom_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_zoom_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_zoom_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show swap image effect:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_swap_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_swap_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_swap_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_swap_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Box hover shadow:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_shadow_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_shadow_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_shadow_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_shadow_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Box hover lift up:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_lift_up_status" class="form-control">*/
/* 									<option value="0"{% if t1o_category_prod_lift_up_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_category_prod_lift_up_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Align items to:<br /><span class="k_help">for Grid view</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_category_prod_align" class="form-control">*/
/*                                     <option value="0"{% if t1o_category_prod_align == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Center</option>*/
/*                                     <option value="1"{% if t1o_category_prod_align == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_category_prod_align == '' %}{{ "selected=\"selected\"" }}{% endif %}>Left</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-midsection-product" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend class="bn"></legend> */
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Prev/Next products:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_prev_next_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_prev_next_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_prev_next_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_prev_next_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>  */
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_21.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Product Page Layout</legend> */
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Product Page Layout:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_layout_product_page" class="form-control">*/
/* 									<option value="1"{% if t1o_layout_product_page == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 1</option>*/
/*                            			<option value="2"{% if t1o_layout_product_page == '2' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 2</option>*/
/*                            			<option value="3"{% if t1o_layout_product_page == '3' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 3</option>*/
/*                            			<option value="4"{% if t1o_layout_product_page == '4' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 4</option> */
/*                            			<option value="5"{% if t1o_layout_product_page == '5' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 5</option>*/
/*                            			<option value="6"{% if t1o_layout_product_page == '6' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 6</option>*/
/*                            			<option value="7"{% if t1o_layout_product_page == '7' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 7</option>    */
/*                            			<option value="8"{% if t1o_layout_product_page == '8' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 8</option>*/
/*                            			<option value="9"{% if t1o_layout_product_page == '9' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 9</option>*/
/*                            			<option value="10"{% if t1o_layout_product_page == '10' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_layout_product_page == '' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 10</option>*/
/*                                     <option value="11"{% if t1o_layout_product_page == '11' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 11</option>*/
/*                                     <option value="12"{% if t1o_layout_product_page == '12' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 12</option>*/
/*                                     <option value="13"{% if t1o_layout_product_page == '13' %}{{ "selected=\"selected\"" }}{% endif %}>Layout 13</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/*                             <div class="col-sm-12">*/
/*                                 {% for ppl in 1..4 %} */
/*                                 <div class="prod_l"><img src="view/image/theme_img/pl_{{ ppl }}.png"><span class="t1o_help prod_l_nr">Layout {{ ppl }}</span></div> */
/* 						        {% endfor %}	*/
/* 						    </div>   */
/*                             <div class="col-sm-12">*/
/*                                 {% for ppl in 5..8 %}*/
/*                                 <div class="prod_l"><img src="view/image/theme_img/pl_{{ ppl }}.png"><span class="t1o_help prod_l_nr">Layout {{ ppl }}</span></div> */
/* 						        {% endfor %}	*/
/* 						    </div>*/
/*                             <div class="col-sm-12">*/
/*                                 {% for ppl in 9..12 %}*/
/*                                 <div class="prod_l"><img src="view/image/theme_img/pl_{{ ppl }}.png"><span class="t1o_help prod_l_nr">Layout {{ ppl }}</span></div> */
/* 						        {% endfor %}	*/
/* 						    </div>*/
/*                             <div class="col-sm-12">*/
/*                                 {% for ppl in 13..13 %}*/
/*                                 <div class="prod_l"><img src="view/image/theme_img/pl_{{ ppl }}.png"><span class="t1o_help prod_l_nr">Layout {{ ppl }}</span></div> */
/* 						        {% endfor %}*/
/* 						    </div>*/
/*                         </div>*/
/* */
/* 						<legend>Product Name <a href="view/image/theme_img/help_oxy_theme/go_22.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_name_position" class="form-control">*/
/*                                     <option value="0"{% if t1o_product_name_position == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Buy Column</option>*/
/* 									<option value="1"{% if t1o_product_name_position == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_name_position == '' %}{{ "selected=\"selected\"" }}{% endif %}>Content Column</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Product Images</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Thumbnails in a row:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_additional_images" class="form-control">*/
/* 									<option value="1"{% if t1o_additional_images == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                            			<option value="2"{% if t1o_additional_images == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                            			<option value="3"{% if t1o_additional_images == '3' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_additional_images == '' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/*                            			<option value="4"{% if t1o_additional_images == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4</option> */
/*                            			<option value="5"{% if t1o_additional_images == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5</option>*/
/*                            			<option value="6"{% if t1o_additional_images == '6' %}{{ "selected=\"selected\"" }}{% endif %}>6</option>*/
/*                            			<option value="7"{% if t1o_additional_images == '7' %}{{ "selected=\"selected\"" }}{% endif %}>7</option>    */
/*                            			<option value="8"{% if t1o_additional_images == '8' %}{{ "selected=\"selected\"" }}{% endif %}>8</option>*/
/*                            			<option value="9"{% if t1o_additional_images == '9' %}{{ "selected=\"selected\"" }}{% endif %}>9</option>*/
/*                            			<option value="10"{% if t1o_additional_images == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10</option>*/
/* 								</select>  */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Buy Section</legend>*/
/* 		                    */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Align items to:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_align" class="form-control">*/
/*                                     <option value="0"{% if t1o_product_align == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Center</option>*/
/*                                     <option value="1"{% if t1o_product_align == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_align == '' %}{{ "selected=\"selected\"" }}{% endif %}>Left</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_23.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show manufacturer logo:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_manufacturer_logo_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_manufacturer_logo_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_manufacturer_logo_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_manufacturer_logo_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_24.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Save Percent:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_save_percent_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_save_percent_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_save_percent_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_save_percent_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_25.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Tax:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_tax_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_tax_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_tax_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_tax_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_26.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show product viewed:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_viewed_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_viewed_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_viewed_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_viewed_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_27.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Increment/Decrement a Quantity:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_i_c_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_i_c_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_i_c_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_i_c_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_28.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Related Products</legend>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show related products:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_related_status" class="form-control">*/
/* 									<option value="0"{% if t1o_product_related_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_product_related_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_related_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Related products position:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_related_position" class="form-control">*/
/* 									<option value="0"{% if t1o_product_related_position == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Right</option>*/
/*                                     <option value="1"{% if t1o_product_related_position == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_related_position == '' %}{{ "selected=\"selected\"" }}{% endif %}>Bottom</option>*/
/* 								</select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_29.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Related products style:<br /><span class="k_help">only bottom position</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_related_style" class="form-control">*/
/* 									<option value="0"{% if t1o_product_related_style == '0' %}{{ "selected=\"selected\"" }}{% endif %}>Grid</option>*/
/*                                     <option value="1"{% if t1o_product_related_style == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_product_related_style == '' %}{{ "selected=\"selected\"" }}{% endif %}>Slider</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Feature Box 1<a href="view/image/theme_img/help_oxy_theme/go_30.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Custom Icon:<br /><span class="k_help">Recommended dimensions<br>46 x 46px</span></label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1o_product_fb1_icon_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_product_fb1_icon_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_product_fb1_icon" value="{{ t1o_product_fb1_icon }}" id="t1o_product_fb1_icon" />*/
/* 					        </div>*/
/* 				        </div> */
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1o_product_fb1_awesome" value="{{ t1o_product_fb1_awesome }}" class="form-control" />*/
/*                                 <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>*/
/*                                 <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">    */
/*                                             <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_fb1_title[{{ language.language_id }}]" id="t1o_product_fb1_title_{{ language.language_id }}" value="{{ t1o_product_fb1_title[language.language_id] ? t1o_product_fb1_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subtitle:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_fb1_subtitle[{{ language.language_id }}]" id="t1o_product_fb1_subtitle_{{ language.language_id }}" value="{{ t1o_product_fb1_subtitle[language.language_id] ? t1o_product_fb1_subtitle[language.language_id] : '' }}" placeholder="Subtitle" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_fb1_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_fb1_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_fb1_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_product_fb1_content[{{ language.language_id }}][htmlcontent]" id="t1o_product_fb1_content-{{ language.language_id }}">{{ t1o_product_fb1_content[language.language_id] ? t1o_product_fb1_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Feature Box 2</legend>*/
/*  */
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Custom Icon:<br /><span class="k_help">Recommended dimensions<br>46 x 46px</span></label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1o_product_fb2_icon_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_product_fb2_icon_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_product_fb2_icon" value="{{ t1o_product_fb2_icon }}" id="t1o_product_fb2_icon" />*/
/* 					        </div>*/
/* 				        </div> */
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1o_product_fb2_awesome" value="{{ t1o_product_fb2_awesome }}" class="form-control" />*/
/*                                 <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>*/
/*                                 <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">    */
/*                                             <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_fb2_title[{{ language.language_id }}]" id="t1o_product_fb2_title_{{ language.language_id }}" value="{{ t1o_product_fb2_title[language.language_id] ? t1o_product_fb2_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subtitle:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_fb2_subtitle[{{ language.language_id }}]" id="t1o_product_fb2_subtitle_{{ language.language_id }}" value="{{ t1o_product_fb2_subtitle[language.language_id] ? t1o_product_fb2_subtitle[language.language_id] : '' }}" placeholder="Subtitle" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_fb2_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_fb2_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_fb2_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_product_fb2_content[{{ language.language_id }}][htmlcontent]" id="t1o_product_fb2_content-{{ language.language_id }}">{{ t1o_product_fb2_content[language.language_id] ? t1o_product_fb2_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Feature Box 3</legend>*/
/* */
/*                         <div class="form-group">*/
/* 					        <label class="col-sm-2 control-label">Custom Icon:<br /><span class="k_help">Recommended dimensions<br>46 x 46px</span></label>*/
/* 					        <div class="col-sm-10">                                */
/*                                 <a href="" id="t1o_product_fb3_icon_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_product_fb3_icon_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_product_fb3_icon" value="{{ t1o_product_fb3_icon }}" id="t1o_product_fb3_icon" />*/
/* 					        </div>*/
/* 				        </div> */
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Font Awesome Icon:</label>*/
/*                             <div class="col-sm-10">*/
/* 								<input type="text" name="t1o_product_fb3_awesome" value="{{ t1o_product_fb3_awesome }}" class="form-control" />*/
/*                                 <span class="k_help">Enter the name of an icon, for example: <b>car</b></span>*/
/*                                 <a href="http://fortawesome.github.io/Font-Awesome/icons/" target="_blank" class="link" style="margin-left:5px">Font Awesome Icon Collection &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">    */
/*                                             <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_fb3_title[{{ language.language_id }}]" id="t1o_product_fb3_title_{{ language.language_id }}" value="{{ t1o_product_fb3_title[language.language_id] ? t1o_product_fb3_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subtitle:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_fb3_subtitle[{{ language.language_id }}]" id="t1o_product_fb3_subtitle_{{ language.language_id }}" value="{{ t1o_product_fb3_subtitle[language.language_id] ? t1o_product_fb3_subtitle[language.language_id] : '' }}" placeholder="Subtitle" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_fb3_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_fb3_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_fb3_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_product_fb3_content[{{ language.language_id }}][htmlcontent]" id="t1o_product_fb3_content-{{ language.language_id }}">{{ t1o_product_fb3_content[language.language_id] ? t1o_product_fb3_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Custom Block - Under Main Image<a href="view/image/theme_img/help_oxy_theme/go_31.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_custom_block_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_product_custom_block_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_product_custom_block_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">    */
/*                                             <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_custom_block_1_title[{{ language.language_id }}]" id="t1o_product_custom_block_1_title_{{ language.language_id }}" value="{{ t1o_product_custom_block_1_title[language.language_id] ? t1o_product_custom_block_1_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_custom_block_1_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_custom_block_1_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_custom_block_1_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_product_custom_block_1_content[{{ language.language_id }}][htmlcontent]" id="t1o_product_custom_block_1_content-{{ language.language_id }}">{{ t1o_product_custom_block_1_content[language.language_id] ? t1o_product_custom_block_1_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Custom Block - Buy Section<a href="view/image/theme_img/help_oxy_theme/go_32.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_custom_block_2_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_product_custom_block_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_product_custom_block_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">    */
/*                                             <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_custom_block_2_title[{{ language.language_id }}]" id="t1o_product_custom_block_2_title_{{ language.language_id }}" value="{{ t1o_product_custom_block_2_title[language.language_id] ? t1o_product_custom_block_2_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_custom_block_2_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_custom_block_2_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_custom_block_2_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_product_custom_block_2_content[{{ language.language_id }}][htmlcontent]" id="t1o_product_custom_block_2_content-{{ language.language_id }}">{{ t1o_product_custom_block_2_content[language.language_id] ? t1o_product_custom_block_2_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*           */
/* 							</div>*/
/* 						</div>  */
/*                         */
/*                         <legend>Custom Block - Right Sidebar<a href="view/image/theme_img/help_oxy_theme/go_33.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_custom_block_3_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_product_custom_block_3_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_product_custom_block_3_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">    */
/*                                             <span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_custom_block_3_title[{{ language.language_id }}]" id="t1o_product_custom_block_3_title_{{ language.language_id }}" value="{{ t1o_product_custom_block_3_title[language.language_id] ? t1o_product_custom_block_3_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_custom_block_3_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_custom_block_3_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_custom_block_3_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_product_custom_block_3_content[{{ language.language_id }}][htmlcontent]" id="t1o_product_custom_block_3_content-{{ language.language_id }}">{{ t1o_product_custom_block_3_content[language.language_id] ? t1o_product_custom_block_3_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         {% for i in 1..3 %}*/
/*                         */
/*                         <legend>Custom Tab {{ i }}<a href="view/image/theme_img/help_oxy_theme/go_34.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Tab {{ i }}:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_product_custom_tab[{{ i }}][status]" class="form-control">*/
/* 								    <option value="0"{% if t1o_product_custom_tab[i].status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_product_custom_tab[i].status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_product_custom_tab[{{ i }}][{{ language.language_id }}][title]" id="t1o_product_custom_tab_{{ i }}_{{ language.language_id }}_title" value="{{ t1o_product_custom_tab[i][language.language_id].title ? t1o_product_custom_tab[i][language.language_id].title : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_product_custom_tab_{{ i }}" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_product_custom_tab_{{ i }}_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_product_custom_tab_{{ i }}_{{ language.language_id }}" class="tab-pane">*/
/*                                             <textarea name="t1o_product_custom_tab[{{ i }}][{{ language.language_id }}][htmlcontent]" id="t1o_product_custom_tab_{{ i }}_{{ language.language_id }}_content">{{ t1o_product_custom_tab[i][language.language_id] ? t1o_product_custom_tab[i][language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>				*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         {% endfor %}*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-midsection-contact" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/*                         */
/*                         <legend>Google Map <a href="view/image/theme_img/help_oxy_theme/go_35.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Google Map:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_contact_map_status" class="form-control">*/
/* 									<option value="0"{% if t1o_contact_map_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_contact_map_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Latitude, Longitude:<br /><span class="k_help">For example:<br />51.5224954,-0.1720996</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_contact_map_ll" value="{{ t1o_contact_map_ll }}" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 <a href="http://itouchmap.com/latlong.html" target="_blank" class="link">How to find Latitude and Longitude?</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Your Google Maps API key:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_contact_map_api" value="{{ t1o_contact_map_api }}" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 <a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank" class="link">How to get your Google Maps API key?</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Map type:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_contact_map_type" class="form-control">*/
/* 									<option value="ROADMAP"{% if t1o_contact_map_type == 'ROADMAP' %}{{ "selected=\"selected\"" }}{% endif %}>ROADMAP</option>*/
/*                             		<option value="SATELLITE"{% if t1o_contact_map_type == 'SATELLITE' %}{{ "selected=\"selected\"" }}{% endif %}>SATELLITE</option>*/
/*                             		<option value="HYBRID"{% if t1o_contact_map_type == 'HYBRID' %}{{ "selected=\"selected\"" }}{% endif %}>HYBRID</option>*/
/*                             		<option value="TERRAIN"{% if t1o_contact_map_type == 'TERRAIN' %}{{ "selected=\"selected\"" }}{% endif %}>TERRAIN</option> */
/* 								</select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-midsection-lf" class="tab-pane">  */
/*         */
/*                     <fieldset> */
/*         */
/*                         <legend>Categories</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Categories display type:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_left_right_column_categories_type" class="form-control">*/
/* 								    <option value="0"{% if t1o_left_right_column_categories_type == '0' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_opencart }}</option> */
/*                                     <option value="1"{% if t1o_left_right_column_categories_type == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_left_right_column_categories_type == '' %}{{ "selected=\"selected\"" }}{% endif %}>{{ text_accordion }}</option>  */
/*                                     <option value="2"{% if t1o_left_right_column_categories_type == '2' %}{{ "selected=\"selected\"" }}{% endif %}>Dropdown</option> */
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_36.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*         */
/*                     </fieldset>      */
/*         */
/*         </div>*/
/*  */
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-footer"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="footer_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-footer-top-custom-1" data-toggle="tab">Top Custom Block</a></li>*/
/*              <li><a href="#tab-footer-custom-column-1" data-toggle="tab">Custom Column 1</a></li>*/
/*              <li><a href="#tab-footer-information" data-toggle="tab">Information Block</a></li>*/
/*              <li><a href="#tab-footer-custom-column-2" data-toggle="tab">Custom Column 2 / Newsletter</a></li>*/
/*              <li><a href="#tab-footer-payment" data-toggle="tab">Payment Images</a></li>*/
/*              <li><a href="#tab-footer-powered" data-toggle="tab">Powered by</a></li>*/
/*              <li><a href="#tab-footer-follow" data-toggle="tab">Follow us</a></li>*/
/*              <li><a href="#tab-footer-bottom-custom-1" data-toggle="tab">Bottom Custom Block</a></li>*/
/*              <li><a href="#tab-footer-bottom-custom-2" data-toggle="tab">Sliding Bottom Custom Block</a></li>                                  */
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-footer-top-custom-1" class="tab-pane fade in active">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Top Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_top_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_custom_top_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_top_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_top_1_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_custom_top_1_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_top_1_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_top_1_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_top_1_content-{{ language.language_id }}">{{ t1o_custom_top_1_content[language.language_id] ? t1o_custom_top_1_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                     */
/*                     </fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-custom-column-1" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Column 1:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_custom_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Custom Column 1 width:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_1_column_width" class="form-control">*/
/* 								    <option value="1"{% if t1o_custom_1_column_width == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1/12</option>*/
/*                                     <option value="2"{% if t1o_custom_1_column_width == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2/12</option>*/
/*                                     <option value="3"{% if t1o_custom_1_column_width == '3' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_custom_1_column_width == '' %}{{ "selected=\"selected\"" }}{% endif %}>3/12</option>*/
/*                                     <option value="4"{% if t1o_custom_1_column_width == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4/12</option>*/
/*                                     <option value="5"{% if t1o_custom_1_column_width == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5/12</option>*/
/*                                     <option value="6"{% if t1o_custom_1_column_width == '6' %}{{ "selected=\"selected\"" }}{% endif %}>6/12</option>*/
/*                                     <option value="7"{% if t1o_custom_1_column_width == '7' %}{{ "selected=\"selected\"" }}{% endif %}>7/12</option>*/
/*                                     <option value="8"{% if t1o_custom_1_column_width == '8' %}{{ "selected=\"selected\"" }}{% endif %}>8/12</option>*/
/*                                     <option value="9"{% if t1o_custom_1_column_width == '9' %}{{ "selected=\"selected\"" }}{% endif %}>9/12</option>*/
/*                                     <option value="10"{% if t1o_custom_1_column_width == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10/12</option>*/
/*                                     <option value="11"{% if t1o_custom_1_column_width == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11/12</option>*/
/*                                     <option value="12"{% if t1o_custom_1_column_width == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12/12</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_custom_1_title[{{ language.language_id }}]" id="t1o_custom_1_title_{{ language.language_id }}" value="{{ t1o_custom_1_title[language.language_id] ? t1o_custom_1_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_1_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}*/
/* 										<li><a href="#t1o_custom_1_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_1_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_1_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_1_content-{{ language.language_id }}">{{ t1o_custom_1_content[language.language_id] ? t1o_custom_1_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-information" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/*                     */
/*                         <legend>Information Block<a href="view/image/theme_img/help_oxy_theme/go_38.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Information Block width:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_information_block_width" class="form-control">*/
/* 								    <option value="1"{% if t1o_information_block_width == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1/12</option>*/
/*                                     <option value="2"{% if t1o_information_block_width == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2/12</option>*/
/*                                     <option value="3"{% if t1o_information_block_width == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3/12</option>*/
/*                                     <option value="4"{% if t1o_information_block_width == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4/12</option>*/
/*                                     <option value="5"{% if t1o_information_block_width == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5/12</option>*/
/*                                     <option value="6"{% if t1o_information_block_width == '6' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_information_block_width == '' %}{{ "selected=\"selected\"" }}{% endif %}>6/12</option>*/
/*                                     <option value="7"{% if t1o_information_block_width == '7' %}{{ "selected=\"selected\"" }}{% endif %}>7/12</option>*/
/*                                     <option value="8"{% if t1o_information_block_width == '8' %}{{ "selected=\"selected\"" }}{% endif %}>8/12</option>*/
/*                                     <option value="9"{% if t1o_information_block_width == '9' %}{{ "selected=\"selected\"" }}{% endif %}>9/12</option>*/
/*                                     <option value="10"{% if t1o_information_block_width == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10/12</option>*/
/*                                     <option value="11"{% if t1o_information_block_width == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11/12</option>*/
/*                                     <option value="12"{% if t1o_information_block_width == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12/12</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 						<legend>Information Column<a href="view/image/theme_img/help_oxy_theme/go_38.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Information Column:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_information_column_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_information_column_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_information_column_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_information_column_1_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <span class="k_help">To disable the default OpenCart links, like <b>About Us</b>, <b>Delivery Information</b> or <b>Privacy Policy</b>, go to OpenCart Admin > Catalog > Information.</span>*/
/*                         */
/*                         */
/*                         <legend>Customer Service Column<a href="view/image/theme_img/help_oxy_theme/go_39.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Customer Service Column:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_information_column_2_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_information_column_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_information_column_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_information_column_2_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Contact Us link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_2_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_2_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_2_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_2_1_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">My Account link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_2_2_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_2_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_2_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_2_2_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Returns link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_2_3_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_2_3_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_2_3_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_2_3_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Order History link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_2_4_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_2_4_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_2_4_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_2_4_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Wish List link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_2_5_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_2_5_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_2_5_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_2_5_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <legend>Extras Column<a href="view/image/theme_img/help_oxy_theme/go_40.jpg" target="_blank"><span class="k_help_tip">?</span></a></legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Extras Column:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_information_column_3_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_information_column_3_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_information_column_3_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_information_column_3_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Brands link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_3_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_3_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_3_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_3_1_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Gift Vouchers link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_3_2_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_3_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_3_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_3_2_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Affiliates link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_3_3_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_3_3_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_3_3_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_3_3_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Specials link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_3_4_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_3_4_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_3_4_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_3_4_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Newsletter link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_3_5_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_3_5_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_3_5_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_3_5_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Site Map link:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_i_c_3_6_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_i_c_3_6_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_i_c_3_6_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_i_c_3_6_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-custom-column-2" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Column 2 / Newsletter:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_2_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_custom_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Custom Column 2 / Newsletter width:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_2_column_width" class="form-control">*/
/* 								    <option value="1"{% if t1o_custom_2_column_width == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1/12</option>*/
/*                                     <option value="2"{% if t1o_custom_2_column_width == '2' %}{{ "selected=\"selected\"" }}{% endif %}>2/12</option>*/
/*                                     <option value="3"{% if t1o_custom_2_column_width == '3' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_custom_2_column_width == '' %}{{ "selected=\"selected\"" }}{% endif %}>3/12</option>*/
/*                                     <option value="4"{% if t1o_custom_2_column_width == '4' %}{{ "selected=\"selected\"" }}{% endif %}>4/12</option>*/
/*                                     <option value="5"{% if t1o_custom_2_column_width == '5' %}{{ "selected=\"selected\"" }}{% endif %}>5/12</option>*/
/*                                     <option value="6"{% if t1o_custom_2_column_width == '6' %}{{ "selected=\"selected\"" }}{% endif %}>6/12</option>*/
/*                                     <option value="7"{% if t1o_custom_2_column_width == '7' %}{{ "selected=\"selected\"" }}{% endif %}>7/12</option>*/
/*                                     <option value="8"{% if t1o_custom_2_column_width == '8' %}{{ "selected=\"selected\"" }}{% endif %}>8/12</option>*/
/*                                     <option value="9"{% if t1o_custom_2_column_width == '9' %}{{ "selected=\"selected\"" }}{% endif %}>9/12</option>*/
/*                                     <option value="10"{% if t1o_custom_2_column_width == '10' %}{{ "selected=\"selected\"" }}{% endif %}>10/12</option>*/
/*                                     <option value="11"{% if t1o_custom_2_column_width == '11' %}{{ "selected=\"selected\"" }}{% endif %}>11/12</option>*/
/*                                     <option value="12"{% if t1o_custom_2_column_width == '12' %}{{ "selected=\"selected\"" }}{% endif %}>12/12</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_custom_2_title[{{ language.language_id }}]" id="t1o_custom_2_title_{{ language.language_id }}" value="{{ t1o_custom_2_title[language.language_id] ? t1o_custom_2_title[language.language_id] : '' }}" placeholder="Title" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Newsletter Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_newsletter_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_newsletter_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_newsletter_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_41.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Your MailChimp Campaign URL:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 <input type="text" name="t1o_newsletter_campaign_url" value="{{ t1o_newsletter_campaign_url }}" class="form-control" style="width: 100%;" />*/
/*                                 <br /><span class="k_help"><a href="https://mailchimp.com/" target="_blank" class="link">MailChimp Website &raquo;</a></span>*/
/*                                 <span class="k_help"><a href="http://kb.mailchimp.com/" target="_blank" class="link">MailChimp Knowledge Base &raquo;</a></span>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Newsletter Promo text:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_newsletter_promo_text[{{ language.language_id }}]" id="t1o_newsletter_promo_text_{{ language.language_id }}" value="{{ t1o_newsletter_promo_text[language.language_id] ? t1o_newsletter_promo_text[language.language_id] : '' }}" placeholder="Newsletter Promo text" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">"Your email address" text:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_newsletter_email[{{ language.language_id }}]" id="t1o_newsletter_email_{{ language.language_id }}" value="{{ t1o_newsletter_email[language.language_id] ? t1o_newsletter_email[language.language_id] : '' }}" placeholder="Your email address" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">"Subscribe" text:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_newsletter_subscribe[{{ language.language_id }}]" id="t1o_newsletter_subscribe_{{ language.language_id }}" value="{{ t1o_newsletter_subscribe[language.language_id] ? t1o_newsletter_subscribe[language.language_id] : '' }}" placeholder="Subscribe" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Custom Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_2_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}*/
/* 										<li><a href="#t1o_custom_2_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_2_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_2_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_2_content-{{ language.language_id }}">{{ t1o_custom_2_content[language.language_id] ? t1o_custom_2_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-payment" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<legend class="bn"></legend>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show payment images:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_payment_block_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_block_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_payment_block_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_45.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                         <legend>Custom payment image</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_payment_block_custom_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_block_custom_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_block_custom_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Upload your payment image:<br /><span class="k_help">Recommended height: 36px</span></label>*/
/* 							<div class="col-sm-10">*/
/* 								<a href="" id="t1o_payment_block_custom_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_payment_block_custom_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_payment_block_custom" value="{{ t1o_payment_block_custom }}" id="t1o_payment_block_custom" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">url:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 <input type="text" name="t1o_payment_block_custom_url" value="{{ t1o_payment_block_custom_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>  */
/*                         */
/*                         <legend>OXY Theme payment images:</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">PayPal:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_paypal" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_paypal == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_paypal == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_paypal-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_paypal_url" value="{{ t1o_payment_paypal_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Visa:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_visa" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_visa == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_visa == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_visa-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_visa_url" value="{{ t1o_payment_visa_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">MasterCard:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_mastercard" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_mastercard == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_mastercard == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_mastercard-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_mastercard_url" value="{{ t1o_payment_mastercard_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Maestro:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_maestro" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_maestro == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_maestro == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_maestro-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_maestro_url" value="{{ t1o_payment_maestro_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Discover:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_discover" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_discover == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_discover == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_discover-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_discover_url" value="{{ t1o_payment_discover_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Skrill:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_skrill" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_skrill == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_skrill == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_skrill-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_skrill_url" value="{{ t1o_payment_skrill_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">American Express:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_american_express" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_american_express == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_american_express == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_american_express-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_american_express_url" value="{{ t1o_payment_american_express_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Cirrus:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_cirrus" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_cirrus == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_cirrus == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_cirrus-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_cirrus_url" value="{{ t1o_payment_cirrus_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Delta:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_delta" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_delta == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/* */
/*                                     <option value="1"{% if t1o_payment_delta == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_delta-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_delta_url" value="{{ t1o_payment_delta_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Google:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_google" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_google == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_google == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_google-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_google_url" value="{{ t1o_payment_google_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">2CheckOut:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_2co" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_2co == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_2co == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_2co-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_2co_url" value="{{ t1o_payment_2co_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Sage:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_sage" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_sage == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_sage == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_sage-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_sage_url" value="{{ t1o_payment_sage_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Solo:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_solo" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_solo == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_solo == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_solo-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_solo_url" value="{{ t1o_payment_solo_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Amazon Payments:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_amazon" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_amazon == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_amazon == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_amazon-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_amazon_url" value="{{ t1o_payment_amazon_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Western Union:</label>*/
/* 							<div class="col-sm-2">*/
/* 								<select name="t1o_payment_western_union" class="form-control">*/
/* 								    <option value="0"{% if t1o_payment_western_union == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option> */
/*                                     <option value="1"{% if t1o_payment_western_union == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>      */
/* 							    </select>*/
/* 							</div>*/
/*                             <div class="col-sm-1"><img src="../catalog/view/theme/oxy/image/payment/payment_image_western_union-1.png"></div>*/
/*                             <div class="col-sm-1 control-label">url:</div>*/
/*                             <div class="col-sm-6">*/
/* 								<input type="text" name="t1o_payment_western_union_url" value="{{ t1o_payment_western_union_url }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-powered" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show powered by:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_powered_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_powered_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_powered_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_42.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_powered_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_powered_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_powered_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_powered_content[{{ language.language_id }}][htmlcontent]" id="t1o_powered_content-{{ language.language_id }}">{{ t1o_powered_content[language.language_id] ? t1o_powered_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>		*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-follow" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<legend>Follow Us</legend>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Follow Us Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_follow_us_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_follow_us_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_follow_us_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_44.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Facebook:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_facebook" value="{{ t1o_facebook }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Twitter:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_twitter" value="{{ t1o_twitter }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Google+:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_googleplus" value="{{ t1o_googleplus }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">RSS:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_rss" value="{{ t1o_rss }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Pinterest:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_pinterest" value="{{ t1o_pinterest }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Vimeo:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_vimeo" value="{{ t1o_vimeo }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Flickr:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_flickr" value="{{ t1o_flickr }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">LinkedIn:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_linkedin" value="{{ t1o_linkedin }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">YouTube:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_youtube" value="{{ t1o_youtube }}" class="form-control" />*/
/* */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Dribbble:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_dribbble" value="{{ t1o_dribbble }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Instagram:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_instagram" value="{{ t1o_instagram }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Behance:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_behance" value="{{ t1o_behance }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Skype username:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_skype" value="{{ t1o_skype }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Tumblr:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_tumblr" value="{{ t1o_tumblr }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Reddit:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_reddit" value="{{ t1o_reddit }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">VK:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_vk" value="{{ t1o_vk }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-bottom-custom-1" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Bottom Custom Block:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_bottom_1_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_custom_bottom_1_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_bottom_1_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_bottom_1_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_custom_bottom_1_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_bottom_1_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_bottom_1_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_bottom_1_content-{{ language.language_id }}">{{ t1o_custom_bottom_1_content[language.language_id] ? t1o_custom_bottom_1_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                     */
/*                     </fieldset>        */
/*         */
/*         </div>*/
/*         */
/*         <div id="tab-footer-bottom-custom-2" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Sliding Bottom Custom Block:*/
/*                             <br /><span class="k_help">for "Full Width" layout style</span>*/
/*                             </label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_bottom_2_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_custom_bottom_2_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_bottom_2_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_bottom_2_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_custom_bottom_2_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_bottom_2_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_bottom_2_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_bottom_2_content-{{ language.language_id }}">{{ t1o_custom_bottom_2_content[language.language_id] ? t1o_custom_bottom_2_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 <span class="k_help">To add background image or pattern, go to <b>OXY Theme Settings - Design > Background Images > Footer > Sliding Bottom Custom Block</b>.</span>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Height:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_bottom_2_footer_margin" class="form-control">*/
/* 								    <option value="50"{% if t1o_custom_bottom_2_footer_margin == '50' %}{{ "selected=\"selected\"" }}{% endif %}>50px</option>*/
/*                                     <option value="100"{% if t1o_custom_bottom_2_footer_margin == '100' %}{{ "selected=\"selected\"" }}{% endif %}>100px</option>*/
/*                                     <option value="150"{% if t1o_custom_bottom_2_footer_margin == '150' %}{{ "selected=\"selected\"" }}{% endif %}>150px</option>*/
/*                                     <option value="200"{% if t1o_custom_bottom_2_footer_margin == '200' %}{{ "selected=\"selected\"" }}{% endif %}>200px</option>*/
/*                                     <option value="250"{% if t1o_custom_bottom_2_footer_margin == '250' %}{{ "selected=\"selected\"" }}{% endif %}>250px</option>*/
/*                                     <option value="300"{% if t1o_custom_bottom_2_footer_margin == '300' %}{{ "selected=\"selected\"" }}{% endif %}>300px</option>*/
/*                                     <option value="350"{% if t1o_custom_bottom_2_footer_margin == '350' %}{{ "selected=\"selected\"" }}{% endif %}>350px</option>*/
/*                                     <option value="400"{% if t1o_custom_bottom_2_footer_margin == '400' %}{{ "selected=\"selected\"" }}{% endif %}>400px</option>*/
/*                                     <option value="450"{% if t1o_custom_bottom_2_footer_margin == '450' %}{{ "selected=\"selected\"" }}{% endif %}>450px</option>*/
/*                                     <option value="500"{% if t1o_custom_bottom_2_footer_margin == '500' %}{{ "selected=\"selected\"" }}{% endif %}>500px</option>*/
/*                                     <option value="550"{% if t1o_custom_bottom_2_footer_margin == '550' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_custom_bottom_2_footer_margin == '' %}{{ "selected=\"selected\"" }}{% endif %}>550px</option>*/
/*                                     <option value="600"{% if t1o_custom_bottom_2_footer_margin == '600' %}{{ "selected=\"selected\"" }}{% endif %}>600px</option>*/
/*                                     <option value="650"{% if t1o_custom_bottom_2_footer_margin == '650' %}{{ "selected=\"selected\"" }}{% endif %}>650px</option>*/
/*                                     <option value="700"{% if t1o_custom_bottom_2_footer_margin == '700' %}{{ "selected=\"selected\"" }}{% endif %}>700px</option>*/
/* 							    </select>*/
/*                                 <a href="view/image/theme_img/help_oxy_theme/go_46.jpg" target="_blank"><span class="k_help_tip">?</span></a>*/
/* 							</div>*/
/* 						</div>*/
/*                     */
/*                     </fieldset> */
/*         */
/*         </div>*/
/*  */
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-widgets"> */
/*         <div class="row form-horizontal">  */
/*         */
/*         <div class="col-sm-2">    */
/*         <ul id="widgets_tabs" class="nav nav-pills nav-stacked">*/
/*              <li class="active"><a href="#tab-widgets-facebook" data-toggle="tab">Facebook Widget</a></li>*/
/*              <li><a href="#tab-widgets-twitter" data-toggle="tab">Twitter Widget</a></li>*/
/*              <li><a href="#tab-widgets-googleplus" data-toggle="tab">Google+ Widget</a></li>*/
/*              <li><a href="#tab-widgets-pinterest" data-toggle="tab">Pinterest Widget</a></li>*/
/*              <li><a href="#tab-widgets-snapchat" data-toggle="tab">Snapchat Widget</a></li>*/
/*              <li><a href="#tab-widgets-video-box" data-toggle="tab">Video Box</a></li>*/
/*              <li><a href="#tab-widgets-custom-box" data-toggle="tab">Custom Content Box</a></li>*/
/*              <li><a href="#tab-widgets-cookie" data-toggle="tab">EU Cookie Message Widget</a></li>*/
/*         </ul> */
/*         </div>*/
/*         */
/*         <div class="col-sm-10">*/
/*         <div class="tab-content">*/
/*         */
/*         <div id="tab-widgets-facebook" class="tab-pane fade in active">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Facebook Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_facebook_likebox_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_facebook_likebox_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_facebook_likebox_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Facebook FanPage ID:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_facebook_likebox_id" value="{{ t1o_facebook_likebox_id }}" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 <a href="http://findmyfacebookid.com/" target="_blank" class="link">Find your Facebook ID &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>          */
/* */
/*         </div>*/
/*  */
/*         <div id="tab-widgets-twitter" class="tab-pane"> */
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Twitter Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_twitter_block_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_twitter_block_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_twitter_block_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Twitter username:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_twitter_block_user" value="{{ t1o_twitter_block_user }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Widget ID:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_twitter_block_widget_id" value="{{ t1o_twitter_block_widget_id }}" class="form-control" />&nbsp;&nbsp;&nbsp;&nbsp;*/
/*                                 <a href="http://321cart.com/oxy/documentation/assets/images/screen_14.png" target="_blank" class="link">Find your Widget ID &raquo;</a>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Tweet limit:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_twitter_block_tweets" class="form-control">*/
/* 								    <option value="1"{% if t1o_twitter_block_tweets == '1' %}{{ "selected=\"selected\"" }}{% endif %}>1</option>*/
/*                                     <option value="2"{% if t1o_twitter_block_tweets == '2' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_twitter_block_tweets == '' %}{{ "selected=\"selected\"" }}{% endif %}>2</option>*/
/*                                     <option value="3"{% if t1o_twitter_block_tweets == '3' %}{{ "selected=\"selected\"" }}{% endif %}>3</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>         */
/* */
/*         </div>*/
/*         */
/*         <div id="tab-widgets-googleplus" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Google+ Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_googleplus_box_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_googleplus_box_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_googleplus_box_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Google+ User:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_googleplus_box_user" value="{{ t1o_googleplus_box_user }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>          */
/* */
/*         </div>*/
/*         */
/*         <div id="tab-widgets-pinterest" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Pinterest Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_pinterest_box_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_pinterest_box_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_pinterest_box_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Pinterest User:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_pinterest_box_user" value="{{ t1o_pinterest_box_user }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>          */
/* */
/*         </div>*/
/*         */
/*         <div id="tab-widgets-snapchat" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Snapchat Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_snapchat_box_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_snapchat_box_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_snapchat_box_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Upload your Snapchat Code image:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<a href="" id="t1o_snapchat_box_code_custom_thumb" data-toggle="image" class="img-thumbnail"><img src="{{ t1o_snapchat_box_code_custom_thumb }}" alt="" title="" data-placeholder="{{ placeholder }}" /></a>*/
/*                                 <input type="hidden" name="t1o_snapchat_box_code_custom" value="{{ t1o_snapchat_box_code_custom }}" id="t1o_snapchat_box_code_custom" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Title:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_snapchat_box_title" value="{{ t1o_snapchat_box_title }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Subtitle:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_snapchat_box_subtitle" value="{{ t1o_snapchat_box_subtitle }}" class="form-control" />*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_snapchat_box_bg" id="t1o_snapchat_box_bg" value="{{ t1o_snapchat_box_bg }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset> */
/*         */
/*         </div>*/
/*   */
/*         <div id="tab-widgets-video-box" class="tab-pane">*/
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Video Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_video_box_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_video_box_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_video_box_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_video_box_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_video_box_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_video_box_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_video_box_content[{{ language.language_id }}][htmlcontent]" id="t1o_video_box_content-{{ language.language_id }}">{{ t1o_video_box_content[language.language_id] ? t1o_video_box_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_video_box_bg" id="t1o_video_box_bg" value="{{ t1o_video_box_bg }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>          */
/* */
/*         </div>*/
/*         */
/*         <div id="tab-widgets-custom-box" class="tab-pane"> */
/*         */
/*                     <fieldset>*/
/* */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Custom Content Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_custom_box_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_custom_box_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_custom_box_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Content:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_custom_box_content" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_custom_box_content_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_custom_box_content_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_custom_box_content[{{ language.language_id }}][htmlcontent]" id="t1o_custom_box_content-{{ language.language_id }}">{{ t1o_custom_box_content[language.language_id] ? t1o_custom_box_content[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Background color:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<input type="text" name="t1o_custom_box_bg" id="t1o_custom_box_bg" value="{{ t1o_custom_box_bg }}" class="color {required:false,hash:true}" size="8" />*/
/* 							</div>*/
/* 						</div>*/
/* */
/*                     </fieldset>              */
/* */
/*         </div>*/
/*         */
/*         */
/*         <div id="tab-widgets-cookie" class="tab-pane">  */
/*         */
/*                     <fieldset>*/
/* */
/* 						<div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show EU Cookie Message Widget:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_eu_cookie_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_eu_cookie_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_eu_cookie_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Show Cookie Icon:</label>*/
/* 							<div class="col-sm-10">*/
/* 								<select name="t1o_eu_cookie_icon_status" class="form-control">*/
/* 								    <option value="0"{% if t1o_eu_cookie_icon_status == '0' %}{{ "selected=\"selected\"" }}{% endif %}>No</option>*/
/*                                     <option value="1"{% if t1o_eu_cookie_icon_status == '1' %}{{ "selected=\"selected\"" }}{% endif %}{% if t1o_eu_cookie_icon_status == '' %}{{ "selected=\"selected\"" }}{% endif %}>Yes</option>*/
/* 							    </select>*/
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">EU Cookie Message:</label>*/
/* 							<div class="col-sm-10">*/
/* 								*/
/*                                 <ul id="t1o_eu_cookie_message" class="nav nav-tabs">*/
/* 									{% for language in languages %}	*/
/* 										<li><a href="#t1o_eu_cookie_message_{{ language.language_id }}" data-toggle="tab"><img src="language/{{ language.code }}/{{ language.code }}.png" alt="{{ language.name }}" width="16px" height="11px" /> {{ language.name }}</a></li>*/
/* 									{% endfor %}*/
/* 								</ul>*/
/* */
/* 								<div class="tab-content">					*/
/* 									{% for language in languages %}				*/
/* 										<div id="t1o_eu_cookie_message_{{ language.language_id }}" class="tab-pane">*/
/* 											<textarea name="t1o_eu_cookie_message[{{ language.language_id }}][htmlcontent]" id="t1o_eu_cookie_message-{{ language.language_id }}">{{ t1o_eu_cookie_message[language.language_id] ? t1o_eu_cookie_message[language.language_id].htmlcontent : '' }}</textarea>*/
/* 										</div>			*/
/* 									{% endfor %}*/
/* 								</div>*/
/*                                 */
/* 							</div>*/
/* 						</div>*/
/*                         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">"Close" text:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_eu_cookie_close[{{ language.language_id }}]" id="t1o_eu_cookie_close_{{ language.language_id }}" value="{{ t1o_eu_cookie_close[language.language_id] ? t1o_eu_cookie_close[language.language_id] : '' }}" placeholder="Close" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/* */
/* 					</fieldset>        */
/*         */
/*         </div>*/
/*  */
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*        */
/* */
/*  */
/*  */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-css"> */
/*         <div class="row form-horizontal">  */
/* */
/*         */
/*         <div class="col-sm-12">*/
/*         <div class="tab-content">*/
/*         */
/*                     <fieldset>         */
/*         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Custom CSS:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 <textarea name="t1o_custom_css" rows="10" class="form-control" />{{ t1o_custom_css }}</textarea>*/
/* 							</div>*/
/* 						</div> */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Custom JavaScript:</label>*/
/* 							<div class="col-sm-10">*/
/*                                 <textarea name="t1o_custom_js" rows="10" class="form-control" />{{ t1o_custom_js }}</textarea>*/
/* 							</div>*/
/* 						</div> */
/*  */
/*                     </fieldset>                          */
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/*         */
/*         */
/*         */
/*         */
/*         */
/*         <div class="tab-pane" id="tab-translate"> */
/*         <div class="row form-horizontal">  */
/* */
/*         */
/*         <div class="col-sm-12">*/
/*         <div class="tab-content">*/
/*         */
/*                     <fieldset>         */
/*         */
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Sale</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_sale[{{ language.language_id }}]" id="t1o_text_sale_{{ language.language_id }}" value="{{ t1o_text_sale[language.language_id] ? t1o_text_sale[language.language_id] : 'Sale' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">New</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_new_prod[{{ language.language_id }}]" id="t1o_text_new_prod_{{ language.language_id }}" value="{{ t1o_text_new_prod[language.language_id] ? t1o_text_new_prod[language.language_id] : 'New' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Quick View</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_quickview[{{ language.language_id }}]" id="t1o_text_quickview_{{ language.language_id }}" value="{{ t1o_text_quickview[language.language_id] ? t1o_text_quickview[language.language_id] : 'Quick View' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Shop Now</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_shop_now[{{ language.language_id }}]" id="t1o_text_shop_now_{{ language.language_id }}" value="{{ t1o_text_shop_now[language.language_id] ? t1o_text_shop_now[language.language_id] : 'Shop Now' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">View Now</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_view[{{ language.language_id }}]" id="t1o_text_view_{{ language.language_id }}" value="{{ t1o_text_view[language.language_id] ? t1o_text_view[language.language_id] : 'View Now' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Next</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_next_product[{{ language.language_id }}]" id="t1o_text_next_product_{{ language.language_id }}" value="{{ t1o_text_next_product[language.language_id] ? t1o_text_next_product[language.language_id] : 'Next' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Previous</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_previous_product[{{ language.language_id }}]" id="t1o_text_previous_product_{{ language.language_id }}" value="{{ t1o_text_previous_product[language.language_id] ? t1o_text_previous_product[language.language_id] : 'Previous' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Product viewed:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_product_viewed[{{ language.language_id }}]" id="t1o_text_product_viewed_{{ language.language_id }}" value="{{ t1o_text_product_viewed[language.language_id] ? t1o_text_product_viewed[language.language_id] : 'Product viewed:' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Special price:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_special_price[{{ language.language_id }}]" id="t1o_text_special_price_{{ language.language_id }}" value="{{ t1o_text_special_price[language.language_id] ? t1o_text_special_price[language.language_id] : 'Special price:' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Old price:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_old_price[{{ language.language_id }}]" id="t1o_text_old_price_{{ language.language_id }}" value="{{ t1o_text_old_price[language.language_id] ? t1o_text_old_price[language.language_id] : 'Old price:' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">You save:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_percent_saved[{{ language.language_id }}]" id="t1o_text_percent_saved_{{ language.language_id }}" value="{{ t1o_text_percent_saved[language.language_id] ? t1o_text_percent_saved[language.language_id] : 'You save:' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Send to a friend</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_product_friend[{{ language.language_id }}]" id="t1o_text_product_friend_{{ language.language_id }}" value="{{ t1o_text_product_friend[language.language_id] ? t1o_text_product_friend[language.language_id] : 'Send to a friend' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Shop by Category</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_categories[{{ language.language_id }}]" id="t1o_text_menu_categories_{{ language.language_id }}" value="{{ t1o_text_menu_categories[language.language_id] ? t1o_text_menu_categories[language.language_id] : 'Shop by Category' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Brands</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_brands[{{ language.language_id }}]" id="t1o_text_menu_brands_{{ language.language_id }}" value="{{ t1o_text_menu_brands[language.language_id] ? t1o_text_menu_brands[language.language_id] : 'Brands' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Contact us</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_contact_us[{{ language.language_id }}]" id="t1o_text_contact_us_{{ language.language_id }}" value="{{ t1o_text_contact_us[language.language_id] ? t1o_text_contact_us[language.language_id] : 'Contact us' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Address</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_contact_address[{{ language.language_id }}]" id="t1o_text_menu_contact_address_{{ language.language_id }}" value="{{ t1o_text_menu_contact_address[language.language_id] ? t1o_text_menu_contact_address[language.language_id] : 'Address' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">E-mail</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_contact_email[{{ language.language_id }}]" id="t1o_text_menu_contact_email_{{ language.language_id }}" value="{{ t1o_text_menu_contact_email[language.language_id] ? t1o_text_menu_contact_email[language.language_id] : 'E-mail' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Telephone</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_contact_tel[{{ language.language_id }}]" id="t1o_text_menu_contact_tel_{{ language.language_id }}" value="{{ t1o_text_menu_contact_tel[language.language_id] ? t1o_text_menu_contact_tel[language.language_id] : 'Telephone' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Fax</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_contact_fax[{{ language.language_id }}]" id="t1o_text_menu_contact_fax_{{ language.language_id }}" value="{{ t1o_text_menu_contact_fax[language.language_id] ? t1o_text_menu_contact_fax[language.language_id] : 'Fax' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Opening Times</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_contact_hours[{{ language.language_id }}]" id="t1o_text_menu_contact_hours_{{ language.language_id }}" value="{{ t1o_text_menu_contact_hours[language.language_id] ? t1o_text_menu_contact_hours[language.language_id] : 'Opening Times' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Contact Form</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_contact_form[{{ language.language_id }}]" id="t1o_text_menu_contact_form_{{ language.language_id }}" value="{{ t1o_text_menu_contact_form[language.language_id] ? t1o_text_menu_contact_form[language.language_id] : 'Contact Form' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Menu</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_menu_menu[{{ language.language_id }}]" id="t1o_text_menu_menu_{{ language.language_id }}" value="{{ t1o_text_menu_menu[language.language_id] ? t1o_text_menu_menu[language.language_id] : 'Menu' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">See all products by</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_see_all_products_by[{{ language.language_id }}]" id="t1o_text_see_all_products_by_{{ language.language_id }}" value="{{ t1o_text_see_all_products_by[language.language_id] ? t1o_text_see_all_products_by[language.language_id] : 'See all products by' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Bestseller</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_bestseller[{{ language.language_id }}]" id="t1o_text_bestseller_{{ language.language_id }}" value="{{ t1o_text_bestseller[language.language_id] ? t1o_text_bestseller[language.language_id] : 'Bestseller' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Featured</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_featured[{{ language.language_id }}]" id="t1o_text_featured_{{ language.language_id }}" value="{{ t1o_text_featured[language.language_id] ? t1o_text_featured[language.language_id] : 'Featured' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Latest</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_latest[{{ language.language_id }}]" id="t1o_text_latest_{{ language.language_id }}" value="{{ t1o_text_latest[language.language_id] ? t1o_text_latest[language.language_id] : 'Latest' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Specials</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_special[{{ language.language_id }}]" id="t1o_text_special_{{ language.language_id }}" value="{{ t1o_text_special[language.language_id] ? t1o_text_special[language.language_id] : 'Specials' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Most Viewed</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_most_viewed[{{ language.language_id }}]" id="t1o_text_most_viewed_{{ language.language_id }}" value="{{ t1o_text_most_viewed[language.language_id] ? t1o_text_most_viewed[language.language_id] : 'Most Viewed' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">News</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_news[{{ language.language_id }}]" id="t1o_text_news_{{ language.language_id }}" value="{{ t1o_text_news[language.language_id] ? t1o_text_news[language.language_id] : 'News' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Gallery</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_gallery[{{ language.language_id }}]" id="t1o_text_gallery_{{ language.language_id }}" value="{{ t1o_text_gallery[language.language_id] ? t1o_text_gallery[language.language_id] : 'Gallery' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Small List</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_small_list[{{ language.language_id }}]" id="t1o_text_small_list_{{ language.language_id }}" value="{{ t1o_text_small_list[language.language_id] ? t1o_text_small_list[language.language_id] : 'Small List' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Your Cart:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_your_cart[{{ language.language_id }}]" id="t1o_text_your_cart_{{ language.language_id }}" value="{{ t1o_text_your_cart[language.language_id] ? t1o_text_your_cart[language.language_id] : 'Your Cart:' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Popular Search:</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_popular_search[{{ language.language_id }}]" id="t1o_text_popular_search_{{ language.language_id }}" value="{{ t1o_text_popular_search[language.language_id] ? t1o_text_popular_search[language.language_id] : 'Popular Search:' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*                         <div class="form-group">*/
/* 							<label class="col-sm-2 control-label">Advanced Search</label>*/
/* 							<div class="col-sm-10">*/
/* 								{% for language in languages %}*/
/* 										<div class="input-group">*/
/* 											<span class="input-group-addon"><img src="language/{{ language.code }}/{{ language.code }}.png" title="{{ language.name }}" /></span>*/
/*                                             <input type="text" name="t1o_text_advanced_search[{{ language.language_id }}]" id="t1o_text_advanced_search_{{ language.language_id }}" value="{{ t1o_text_advanced_search[language.language_id] ? t1o_text_advanced_search[language.language_id] : 'Advanced Search' }}" placeholder="" class="form-control" />*/
/* 										</div>*/
/* 								{% endfor %}*/
/* 							</div>*/
/* 						</div>*/
/*  */
/*                     </fieldset>                          */
/*      */
/*         </div>*/
/*         </div>*/
/*         */
/*         </div>*/
/*         </div>*/
/* */
/* */
/*         <!-- -->         */
/*         </div>  */
/*         */
/*         */
/*     </form>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* */
/* <script type="text/javascript" src="view/javascript/jscolor/jscolor.js"></script>*/
/* <script type="text/javascript" src="view/javascript/poshytip/jquery.poshytip.js"></script>*/
/* <link rel="stylesheet" type="text/css" href="view/javascript/poshytip/tip-twitter/tip-twitter.css" />*/
/* */
/* <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>*/
/* <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />*/
/* <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>  */
/* */
/* <script type="text/javascript"><!--					*/
/* {% for language in languages %}	*/
/* $('#t1o_top_custom_block_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_header_custom_block_1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_bar_below_menu_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_menu_categories_custom_block_left_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_menu_categories_custom_block_right_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_menu_categories_custom_block_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_menu_custom_block_1_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_menu_custom_block_2_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_menu_custom_block_3_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_menu_custom_block_4_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_menu_custom_block_5_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_product_fb1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_product_fb2_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_product_fb3_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_product_custom_block_1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_product_custom_block_2_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_product_custom_block_3_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_product_custom_tab_1_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_product_custom_tab_2_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_product_custom_tab_3_{{ language.language_id }}_content').summernote({height: 300});*/
/* $('#t1o_contact_custom_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_fp_fb1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_fp_fb2_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_fp_fb3_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_fp_fb4_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_top_1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_2_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_bottom_1_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_bottom_2_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_powered_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_video_box_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_custom_box_content-{{ language.language_id }}').summernote({height: 300});*/
/* $('#t1o_eu_cookie_message-{{ language.language_id }}').summernote({height: 300});*/
/* {% endfor %}*/
/* //--></script>*/
/* <script type="text/javascript"><!--*/
/* $('#t1o_top_custom_block_content li:first-child a').tab('show');*/
/* $('#t1o_header_custom_block_1_content li:first-child a').tab('show');*/
/* $('#t1o_custom_bar_below_menu_content li:first-child a').tab('show');*/
/* $('#t1o_menu_categories_custom_block_left_content li:first-child a').tab('show');*/
/* $('#t1o_menu_categories_custom_block_right_content li:first-child a').tab('show');*/
/* $('#t1o_menu_categories_custom_block_content li:first-child a').tab('show');*/
/* $('#t1o_menu_custom_block_1 li:first-child a').tab('show');*/
/* $('#t1o_menu_custom_block_2 li:first-child a').tab('show');*/
/* $('#t1o_menu_custom_block_3 li:first-child a').tab('show');*/
/* $('#t1o_menu_custom_block_4 li:first-child a').tab('show');*/
/* $('#t1o_menu_custom_block_5 li:first-child a').tab('show');*/
/* $('#t1o_product_fb1_content li:first-child a').tab('show');*/
/* $('#t1o_product_fb2_content li:first-child a').tab('show');*/
/* $('#t1o_product_fb3_content li:first-child a').tab('show');*/
/* $('#t1o_product_custom_block_1_content li:first-child a').tab('show');*/
/* $('#t1o_product_custom_block_2_content li:first-child a').tab('show');*/
/* $('#t1o_product_custom_block_3_content li:first-child a').tab('show');*/
/* $('#t1o_product_custom_tab_1 li:first-child a').tab('show');*/
/* $('#t1o_product_custom_tab_2 li:first-child a').tab('show');*/
/* $('#t1o_product_custom_tab_3 li:first-child a').tab('show');*/
/* $('#t1o_contact_custom_content li:first-child a').tab('show');*/
/* $('#t1o_fp_fb1_content li:first-child a').tab('show');*/
/* $('#t1o_fp_fb2_content li:first-child a').tab('show');*/
/* $('#t1o_fp_fb3_content li:first-child a').tab('show');*/
/* $('#t1o_fp_fb4_content li:first-child a').tab('show');*/
/* $('#t1o_custom_top_1_content li:first-child a').tab('show');*/
/* $('#t1o_custom_1_content li:first-child a').tab('show');*/
/* $('#t1o_custom_2_content li:first-child a').tab('show');*/
/* $('#t1o_custom_bottom_1_content li:first-child a').tab('show');*/
/* $('#t1o_custom_bottom_2_content li:first-child a').tab('show');*/
/* $('#t1o_powered_content li:first-child a').tab('show');*/
/* $('#t1o_video_box_content li:first-child a').tab('show');*/
/* $('#t1o_custom_box_content li:first-child a').tab('show');*/
/* $('#t1o_eu_cookie_message li:first-child a').tab('show');*/
/* //--></script>*/
/* </div>*/
/* */
/* {{ footer }}*/
/* */
