<?php

/* oxy/template/common/cart.twig */
class __TwigTemplate_8c3269148f6526b5416df35261306f34c0392be387eeda0149956a10fcd170fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"cart\" class=\"btn-group btn-block buttons-header\" data-toggle=\"tooltip\" title=\"";
        echo (isset($context["text_cart"]) ? $context["text_cart"] : null);
        echo "\">
  <a data-toggle=\"dropdown\" data-loading-text=\"";
        // line 2
        echo (isset($context["text_loading"]) ? $context["text_loading"] : null);
        echo "\" class=\"btn dropdown-toggle\">
  <div id=\"cart-tt\"><i class=\"fa fa-shopping-bag\"></i><div id=\"cart-total\" class=\"button-i\">";
        // line 3
        echo (isset($context["text_items"]) ? $context["text_items"] : null);
        echo "</div></div>
  </a>
  <ul class=\"dropdown-menu pull-right\">
    ";
        // line 6
        if (((isset($context["products"]) ? $context["products"] : null) || (isset($context["vouchers"]) ? $context["vouchers"] : null))) {
            // line 7
            echo "    <li>
      <div class=\"cart-title\"><i class=\"fa fa-shopping-bag\"></i><div class=\"cart-total\">";
            // line 8
            echo $this->getAttribute((isset($context["t1o_text_your_cart"]) ? $context["t1o_text_your_cart"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo " <span>";
            echo (isset($context["text_items"]) ? $context["text_items"] : null);
            echo "</span></div></div>
    </li>
    <li>
      <table class=\"table table-striped\">
        ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 13
                echo "        <tr>
          <td class=\"text-center\">";
                // line 14
                if ($this->getAttribute($context["product"], "thumb", array())) {
                    // line 15
                    echo "            <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-thumbnail\" /></a>
            ";
                }
                // line 16
                echo "</td>
          <td class=\"text-left\"><span class=\"name\"><a href=\"";
                // line 17
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></span>
            ";
                // line 18
                if ($this->getAttribute($context["product"], "option", array())) {
                    // line 19
                    echo "            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["product"], "option", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["option"]) {
                        // line 20
                        echo "            <br />
            <small>";
                        // line 21
                        echo $this->getAttribute($context["option"], "name", array());
                        echo ": ";
                        echo $this->getAttribute($context["option"], "value", array());
                        echo "</small>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['option'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 23
                    echo "            ";
                }
                // line 24
                echo "            ";
                if ($this->getAttribute($context["product"], "recurring", array())) {
                    // line 25
                    echo "            <br />
            <small>";
                    // line 26
                    echo (isset($context["text_recurring"]) ? $context["text_recurring"] : null);
                    echo ": ";
                    echo $this->getAttribute($context["product"], "recurring", array());
                    echo "</small>
            ";
                }
                // line 28
                echo "            <br />
            <span class=\"price\">";
                // line 29
                echo $this->getAttribute($context["product"], "total", array());
                echo "</span></td>
          <td class=\"text-right\"><span class=\"quantity\">x";
                // line 30
                echo $this->getAttribute($context["product"], "quantity", array());
                echo "</span></td>
          <td class=\"text-center\"><button type=\"button\" onclick=\"cart.remove('";
                // line 31
                echo $this->getAttribute($context["product"], "cart_id", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-xs item-remove\"><i class=\"fa fa-times\"></i></button></td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["vouchers"]) ? $context["vouchers"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["voucher"]) {
                // line 35
                echo "        <tr>
          <td class=\"text-center\"></td>
          <td class=\"text-left\">";
                // line 37
                echo $this->getAttribute($context["voucher"], "description", array());
                echo "</td>
          <td class=\"text-right\">x&nbsp;1</td>
          <td class=\"text-right\">";
                // line 39
                echo $this->getAttribute($context["voucher"], "amount", array());
                echo "</td>
          <td class=\"text-center text-danger\"><button type=\"button\" onclick=\"voucher.remove('";
                // line 40
                echo $this->getAttribute($context["voucher"], "key", array());
                echo "');\" title=\"";
                echo (isset($context["button_remove"]) ? $context["button_remove"] : null);
                echo "\" class=\"btn btn-danger btn-xs\"><i class=\"fa fa-times\"></i></button></td>
        </tr>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voucher'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 43
            echo "      </table>
    </li>
    <li>
      <div>
        <table class=\"table cart-total\">
          ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["totals"]) ? $context["totals"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["total"]) {
                // line 49
                echo "          <tr>
            <td class=\"text-right\">";
                // line 50
                echo $this->getAttribute($context["total"], "title", array());
                echo "</td>
            <td class=\"text-right\"><span>";
                // line 51
                echo $this->getAttribute($context["total"], "text", array());
                echo "</span></td>
          </tr>
          ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['total'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "        </table>
        <div class=\"checkout\"><a href=\"";
            // line 55
            echo (isset($context["cart"]) ? $context["cart"] : null);
            echo "\" class=\"btn btn-xs btn-default\">";
            echo (isset($context["text_cart"]) ? $context["text_cart"] : null);
            echo "</a>&nbsp;&nbsp;&nbsp;<a href=\"";
            echo (isset($context["checkout"]) ? $context["checkout"] : null);
            echo "\" class=\"btn btn-xs btn-primary\">";
            echo (isset($context["text_checkout"]) ? $context["text_checkout"] : null);
            echo "</a></div>
      </div>
    </li>
    ";
        } else {
            // line 59
            echo "    <li>
      <p class=\"text-center\"><i class=\"fa fa-shopping-bag\"></i><br />";
            // line 60
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
    </li>
    ";
        }
        // line 63
        echo "  </ul>
</div>
";
    }

    public function getTemplateName()
    {
        return "oxy/template/common/cart.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 63,  211 => 60,  208 => 59,  195 => 55,  192 => 54,  183 => 51,  179 => 50,  176 => 49,  172 => 48,  165 => 43,  154 => 40,  150 => 39,  145 => 37,  141 => 35,  136 => 34,  125 => 31,  121 => 30,  117 => 29,  114 => 28,  107 => 26,  104 => 25,  101 => 24,  98 => 23,  88 => 21,  85 => 20,  80 => 19,  78 => 18,  72 => 17,  69 => 16,  57 => 15,  55 => 14,  52 => 13,  48 => 12,  39 => 8,  36 => 7,  34 => 6,  28 => 3,  24 => 2,  19 => 1,);
    }
}
/* <div id="cart" class="btn-group btn-block buttons-header" data-toggle="tooltip" title="{{ text_cart }}">*/
/*   <a data-toggle="dropdown" data-loading-text="{{ text_loading }}" class="btn dropdown-toggle">*/
/*   <div id="cart-tt"><i class="fa fa-shopping-bag"></i><div id="cart-total" class="button-i">{{ text_items }}</div></div>*/
/*   </a>*/
/*   <ul class="dropdown-menu pull-right">*/
/*     {% if products or vouchers %}*/
/*     <li>*/
/*       <div class="cart-title"><i class="fa fa-shopping-bag"></i><div class="cart-total">{{ t1o_text_your_cart[lang_id] }} <span>{{ text_items }}</span></div></div>*/
/*     </li>*/
/*     <li>*/
/*       <table class="table table-striped">*/
/*         {% for product in products %}*/
/*         <tr>*/
/*           <td class="text-center">{% if product.thumb %}*/
/*             <a href="{{ product.href }}"><img src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-thumbnail" /></a>*/
/*             {% endif %}</td>*/
/*           <td class="text-left"><span class="name"><a href="{{ product.href }}">{{ product.name }}</a></span>*/
/*             {% if product.option %}*/
/*             {% for option in product.option %}*/
/*             <br />*/
/*             <small>{{ option.name }}: {{ option.value }}</small>*/
/*             {% endfor %}*/
/*             {% endif %}*/
/*             {% if product.recurring %}*/
/*             <br />*/
/*             <small>{{ text_recurring }}: {{ product.recurring }}</small>*/
/*             {% endif %}*/
/*             <br />*/
/*             <span class="price">{{ product.total }}</span></td>*/
/*           <td class="text-right"><span class="quantity">x{{ product.quantity }}</span></td>*/
/*           <td class="text-center"><button type="button" onclick="cart.remove('{{ product.cart_id }}');" title="{{ button_remove }}" class="btn btn-xs item-remove"><i class="fa fa-times"></i></button></td>*/
/*         </tr>*/
/*         {% endfor %}*/
/*         {% for voucher in vouchers %}*/
/*         <tr>*/
/*           <td class="text-center"></td>*/
/*           <td class="text-left">{{ voucher.description }}</td>*/
/*           <td class="text-right">x&nbsp;1</td>*/
/*           <td class="text-right">{{ voucher.amount }}</td>*/
/*           <td class="text-center text-danger"><button type="button" onclick="voucher.remove('{{ voucher.key }}');" title="{{ button_remove }}" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>*/
/*         </tr>*/
/*         {% endfor %}*/
/*       </table>*/
/*     </li>*/
/*     <li>*/
/*       <div>*/
/*         <table class="table cart-total">*/
/*           {% for total in totals %}*/
/*           <tr>*/
/*             <td class="text-right">{{ total.title }}</td>*/
/*             <td class="text-right"><span>{{ total.text }}</span></td>*/
/*           </tr>*/
/*           {% endfor %}*/
/*         </table>*/
/*         <div class="checkout"><a href="{{ cart }}" class="btn btn-xs btn-default">{{ text_cart }}</a>&nbsp;&nbsp;&nbsp;<a href="{{ checkout }}" class="btn btn-xs btn-primary">{{ text_checkout }}</a></div>*/
/*       </div>*/
/*     </li>*/
/*     {% else %}*/
/*     <li>*/
/*       <p class="text-center"><i class="fa fa-shopping-bag"></i><br />{{ text_empty }}</p>*/
/*     </li>*/
/*     {% endif %}*/
/*   </ul>*/
/* </div>*/
/* */
