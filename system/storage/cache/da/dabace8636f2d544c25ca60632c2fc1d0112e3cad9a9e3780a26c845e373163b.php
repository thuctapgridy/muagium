<?php

/* oxy/template/common/search.twig */
class __TwigTemplate_3a64ee8fd48304010ffebd503599e108abd67299b95ede275dacda03ce406636 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-4") || ((isset($context["t1o_header_style"]) ? $context["t1o_header_style"] : null) == "header-style-5"))) {
            echo "  

<div id=\"search\" class=\"header-search-bar\">
  <input type=\"text\" name=\"search\" value=\"";
            // line 4
            echo (isset($context["search"]) ? $context["search"] : null);
            echo "\" placeholder=\"";
            echo (isset($context["text_search"]) ? $context["text_search"] : null);
            echo "\" class=\"form-control input-lg\" />
  <button type=\"button\" class=\"btn btn-default btn-lg\"><div class=\"button-i\"><i class=\"fa fa-search\"></i></div></button>
</div>

";
        } else {
            // line 9
            echo "
<a href=\"#\" data-toggle=\"modal\" data-target=\"#modal-search\" class=\"btn search-block\">
<div id=\"search-block\" class=\"buttons-header theme-modal\" data-toggle=\"tooltip\" title=\"";
            // line 11
            echo (isset($context["text_search"]) ? $context["text_search"] : null);
            echo "\">
<div class=\"button-i\"><i class=\"fa fa-search\"></i></div>
</div>
</a>
<div class=\"modal fade theme-modal\" id=\"modal-search\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"modal-search\" aria-hidden=\"true\">
  <div class=\"modal-dialog\">
    <div class=\"modal-content\">
      <div class=\"modal-body\">
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
      
      <div id=\"search\" class=\"input-group\">
        <input type=\"text\" name=\"search\" value=\"";
            // line 22
            echo (isset($context["search"]) ? $context["search"] : null);
            echo "\" placeholder=\"";
            echo (isset($context["text_search"]) ? $context["text_search"] : null);
            echo "\" class=\"form-control input-lg\" />
        <span class=\"input-group-addon\">
          <button type=\"button\" class=\"btn btn-default btn-lg\"><i class=\"fa fa-search\"></i></button>
        </span>
      </div>
      
";
            // line 28
            if (((isset($context["t1o_header_popular_search_status"]) ? $context["t1o_header_popular_search_status"] : null) == 1)) {
                echo "  
<div id=\"popular-search\">
    ";
                // line 30
                echo $this->getAttribute((isset($context["t1o_text_popular_search"]) ? $context["t1o_text_popular_search"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo "<br /><br />
";
                // line 31
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 20));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 32
                    if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_header_popular_search"]) ? $context["t1o_header_popular_search"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "word", array())) {
                        echo "                      
    <a href=\"index.php?route=product/search&search=";
                        // line 33
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_header_popular_search"]) ? $context["t1o_header_popular_search"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "word", array());
                        echo "\" class=\"btn btn-default popular-search-word\">
    ";
                        // line 34
                        echo $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["t1o_header_popular_search"]) ? $context["t1o_header_popular_search"] : null), $context["i"], array(), "array"), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array"), "word", array());
                        echo " 
    </a>
";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo "</div>
";
            }
            // line 40
            echo "      <a href=\"index.php?route=product/search\" class=\"btn btn-primary advanced-search\">";
            echo $this->getAttribute((isset($context["t1o_text_advanced_search"]) ? $context["t1o_text_advanced_search"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "</a>
      </div>
    </div>
  </div>
</div>

";
        }
    }

    public function getTemplateName()
    {
        return "oxy/template/common/search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 40,  95 => 38,  85 => 34,  81 => 33,  77 => 32,  73 => 31,  69 => 30,  64 => 28,  53 => 22,  39 => 11,  35 => 9,  25 => 4,  19 => 1,);
    }
}
/* {% if t1o_header_style == 'header-style-4' or t1o_header_style == 'header-style-5' %}  */
/* */
/* <div id="search" class="header-search-bar">*/
/*   <input type="text" name="search" value="{{ search }}" placeholder="{{ text_search }}" class="form-control input-lg" />*/
/*   <button type="button" class="btn btn-default btn-lg"><div class="button-i"><i class="fa fa-search"></i></div></button>*/
/* </div>*/
/* */
/* {% else %}*/
/* */
/* <a href="#" data-toggle="modal" data-target="#modal-search" class="btn search-block">*/
/* <div id="search-block" class="buttons-header theme-modal" data-toggle="tooltip" title="{{ text_search }}">*/
/* <div class="button-i"><i class="fa fa-search"></i></div>*/
/* </div>*/
/* </a>*/
/* <div class="modal fade theme-modal" id="modal-search" tabindex="-1" role="dialog" aria-labelledby="modal-search" aria-hidden="true">*/
/*   <div class="modal-dialog">*/
/*     <div class="modal-content">*/
/*       <div class="modal-body">*/
/*       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>*/
/*       */
/*       <div id="search" class="input-group">*/
/*         <input type="text" name="search" value="{{ search }}" placeholder="{{ text_search }}" class="form-control input-lg" />*/
/*         <span class="input-group-addon">*/
/*           <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>*/
/*         </span>*/
/*       </div>*/
/*       */
/* {% if t1o_header_popular_search_status == 1 %}  */
/* <div id="popular-search">*/
/*     {{ t1o_text_popular_search[lang_id] }}<br /><br />*/
/* {% for i in 1..20 %}*/
/* {% if t1o_header_popular_search[i][lang_id].word %}                      */
/*     <a href="index.php?route=product/search&search={{ t1o_header_popular_search[i][lang_id].word }}" class="btn btn-default popular-search-word">*/
/*     {{ t1o_header_popular_search[i][lang_id].word }} */
/*     </a>*/
/* {% endif %}*/
/* {% endfor %}*/
/* </div>*/
/* {% endif %}*/
/*       <a href="index.php?route=product/search" class="btn btn-primary advanced-search">{{ t1o_text_advanced_search[lang_id] }}</a>*/
/*       </div>*/
/*     </div>*/
/*   </div>*/
/* </div>*/
/* */
/* {% endif %}*/
/* */
