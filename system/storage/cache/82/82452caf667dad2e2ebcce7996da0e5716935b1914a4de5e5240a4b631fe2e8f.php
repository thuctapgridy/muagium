<?php

/* oxy/template/product/manufacturer_info.twig */
class __TwigTemplate_2d51e3c8416a3297771fc42eec63a828af5345f4d5f982165c8ae5cddd1d01ed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["header"]) ? $context["header"] : null);
        echo "
<div id=\"product-manufacturer\" class=\"container full-width-container\">
  <ul class=\"breadcrumb\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["breadcrumbs"]) ? $context["breadcrumbs"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
            // line 5
            echo "    <li><a href=\"";
            echo $this->getAttribute($context["breadcrumb"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["breadcrumb"], "text", array());
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "  </ul>
  <div class=\"row\">";
        // line 8
        echo (isset($context["column_left"]) ? $context["column_left"] : null);
        echo "
    ";
        // line 9
        if (((isset($context["column_left"]) ? $context["column_left"] : null) && (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 10
            echo "    ";
            $context["class"] = "col-sm-6";
            // line 11
            echo "    ";
        } elseif (((isset($context["column_left"]) ? $context["column_left"] : null) || (isset($context["column_right"]) ? $context["column_right"] : null))) {
            // line 12
            echo "    ";
            $context["class"] = "col-sm-9";
            // line 13
            echo "    ";
        } else {
            // line 14
            echo "    ";
            $context["class"] = "col-sm-12";
            // line 15
            echo "    ";
        }
        // line 16
        echo "    <div id=\"content\" class=\"";
        echo (isset($context["class"]) ? $context["class"] : null);
        echo "\">";
        echo (isset($context["content_top"]) ? $context["content_top"] : null);
        echo "
      <h2>";
        // line 17
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h2>
      ";
        // line 18
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 19
            echo "      <div class=\"row content-padd padd-t-b-25 product-filter\">
        <div class=\"col-md-4\">
          <div class=\"btn-group hidden-xs\">
            <button type=\"button\" id=\"list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 22
            echo (isset($context["button_list"]) ? $context["button_list"] : null);
            echo "\"><i class=\"fa fa-th-list\"></i></button>
            <button type=\"button\" id=\"small-list-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 23
            echo $this->getAttribute((isset($context["t1o_text_small_list"]) ? $context["t1o_text_small_list"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "\"><i class=\"fa fa-list\"></i></button>
            <button type=\"button\" id=\"grid-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 24
            echo (isset($context["button_grid"]) ? $context["button_grid"] : null);
            echo "\"><i class=\"fa fa-th\"></i></button>
            <button type=\"button\" id=\"gallery-view\" class=\"btn btn-default\" data-toggle=\"tooltip\" title=\"";
            // line 25
            echo $this->getAttribute((isset($context["t1o_text_gallery"]) ? $context["t1o_text_gallery"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "\"><i class=\"fa fa-th-large\"></i></button>
          </div>
        </div>
        <div class=\"col-md-4\">
          <a href=\"";
            // line 29
            echo (isset($context["compare"]) ? $context["compare"] : null);
            echo "\" id=\"compare-total\" class=\"btn btn-default\">";
            echo (isset($context["text_compare"]) ? $context["text_compare"] : null);
            echo "</a>
        </div>
        <div class=\"col-md-2 text-right\">
          <select id=\"input-sort\" class=\"form-control\" onchange=\"location = this.value;\">
              
              ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["sorts"]);
            foreach ($context['_seq'] as $context["_key"] => $context["sorts"]) {
                // line 35
                echo "              ";
                if (($this->getAttribute($context["sorts"], "value", array()) == sprintf("%s-%s", (isset($context["sort"]) ? $context["sort"] : null), (isset($context["order"]) ? $context["order"] : null)))) {
                    // line 36
                    echo "              
              <option value=\"";
                    // line 37
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
              
              ";
                } else {
                    // line 40
                    echo "              
              <option value=\"";
                    // line 41
                    echo $this->getAttribute($context["sorts"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["sorts"], "text", array());
                    echo "</option>
              
              ";
                }
                // line 44
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sorts'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "            
          </select>
        </div>
        <div class=\"col-md-2 text-right\">
          <select id=\"input-limit\" class=\"form-control\" onchange=\"location = this.value;\">
              
              ";
            // line 51
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["limits"]);
            foreach ($context['_seq'] as $context["_key"] => $context["limits"]) {
                // line 52
                echo "              ";
                if (($this->getAttribute($context["limits"], "value", array()) == (isset($context["limit"]) ? $context["limit"] : null))) {
                    // line 53
                    echo "              
              <option value=\"";
                    // line 54
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\" selected=\"selected\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>
              
              ";
                } else {
                    // line 57
                    echo "              
              <option value=\"";
                    // line 58
                    echo $this->getAttribute($context["limits"], "href", array());
                    echo "\">";
                    echo $this->getAttribute($context["limits"], "text", array());
                    echo "</option>
              
              ";
                }
                // line 61
                echo "              ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['limits'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 62
            echo "            
          </select>
        </div>
      </div>
      
      <div class=\"row product-items category-product-items\">      
        ";
            // line 68
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 69
                echo "        <div class=\"product-layout product-list col-xs-12\">
          <div class=\"product-thumb\">
          
            <div class=\"image\">
            
            ";
                // line 74
                if ((($this->getAttribute($context["product"], "out_of_stock_quantity", array()) <= 0) && ((isset($context["t1o_out_of_stock_badge_status"]) ? $context["t1o_out_of_stock_badge_status"] : null) == 1))) {
                    echo " 
            <span class=\"badge out-of-stock\"><span>";
                    // line 75
                    echo $this->getAttribute($context["product"], "out_of_stock_badge", array());
                    echo "</span></span>
            ";
                }
                // line 76
                echo "  
            
            <span class=\"badge-wrapper\">
            
            ";
                // line 80
                if (((isset($context["t1o_sale_badge_status"]) ? $context["t1o_sale_badge_status"] : null) == 1)) {
                    echo "\t
            ";
                    // line 81
                    if (($this->getAttribute($context["product"], "special", array()) && ((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == 0))) {
                        // line 82
                        echo "            <span class=\"badge sale\">";
                        echo $this->getAttribute((isset($context["t1o_text_sale"]) ? $context["t1o_text_sale"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</span>
            ";
                    }
                    // line 83
                    echo " 
            ";
                    // line 84
                    if (($this->getAttribute($context["product"], "special", array()) && ((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == 1))) {
                        echo "            
            ";
                        // line 85
                        $context["res"] = (($this->getAttribute($context["product"], "val1", array()) / $this->getAttribute($context["product"], "val2", array())) * 100);
                        // line 86
                        echo "            ";
                        $context["res"] = (100 - (isset($context["res"]) ? $context["res"] : null));
                        // line 87
                        echo "            <span class=\"badge sale\">-";
                        echo twig_round((isset($context["res"]) ? $context["res"] : null), 0, "common");
                        echo "%</span>
            ";
                    }
                    // line 89
                    echo "            ";
                }
                // line 90
                echo "      
            ";
                // line 91
                if (((isset($context["t1o_new_badge_status"]) ? $context["t1o_new_badge_status"] : null) == 1)) {
                    echo "\t
            ";
                    // line 92
                    $context["days"] = ((twig_round($this->getAttribute($context["product"], "endDate2", array()), 0, "common") / 86400) - (twig_round($this->getAttribute($context["product"], "startDate1", array()), 0, "common") / 86400));
                    // line 93
                    echo "            ";
                    $context["newproductdays"] = 30;
                    // line 94
                    echo "            ";
                    if (((isset($context["days"]) ? $context["days"] : null) < (isset($context["newproductdays"]) ? $context["newproductdays"] : null))) {
                        // line 95
                        echo "            <span class=\"badge new\">";
                        echo $this->getAttribute((isset($context["t1o_text_new_prod"]) ? $context["t1o_text_new_prod"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                        echo "</span>
            ";
                    }
                    // line 97
                    echo "            ";
                }
                // line 98
                echo "            
            </span>
            
            ";
                // line 101
                if (((isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null) == "product-box-style-3")) {
                    // line 102
                    echo "            <div class=\"flybar-top\">  
            <div class=\"flybar-top-items\">
            <p class=\"description\">";
                    // line 104
                    echo $this->getAttribute($context["product"], "description", array());
                    echo "</p>
            <div class=\"rating\">
              ";
                    // line 106
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(range(1, 5));
                    foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                        // line 107
                        echo "              ";
                        if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                            // line 108
                            echo "              <span class=\"fa fa-stack fa-g\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
              ";
                        } else {
                            // line 110
                            echo "              <span class=\"fa fa-stack fa-y\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
              ";
                        }
                        // line 112
                        echo "              ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 113
                    echo "            </div>
            </div>   
            </div>
            ";
                }
                // line 117
                echo "      
            <div class=\"flybar\">  
            <div class=\"flybar-items\">
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 120
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "\" onclick=\"cart.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array());
                echo "');\" class=\"btn btn-default\"><i class=\"fa fa-shopping-bag\"></i></button>
            <a class=\"btn btn-default quickview\" href=\"";
                // line 121
                echo $this->getAttribute($context["product"], "quickview", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo $this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo "\"><i class=\"fa fa-search\"></i></a>
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 122
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"wishlist\"><i class=\"fa fa-heart\"></i></button>
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 123
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"compare\"><i class=\"fa fa-retweet\"></i></button>
            </div>   
            </div>
            
            ";
                // line 127
                if ($this->getAttribute($context["product"], "thumb_swap", array())) {
                    // line 128
                    echo "            <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\">
            <img src=\"";
                    // line 129
                    echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                    echo "\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive img-";
                    echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                    echo " lazyload\" />
            <img src=\"";
                    // line 130
                    echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                    echo "\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb_swap", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive thumb_swap img-";
                    echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                    echo " lazyload\" />
            </a>
            ";
                } else {
                    // line 133
                    echo "            <a href=\"";
                    echo $this->getAttribute($context["product"], "href", array());
                    echo "\"><img src=\"";
                    echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                    echo "\" data-src=\"";
                    echo $this->getAttribute($context["product"], "thumb", array());
                    echo "\" alt=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" title=\"";
                    echo $this->getAttribute($context["product"], "name", array());
                    echo "\" class=\"img-responsive img-";
                    echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                    echo " lazyload\" /></a>
            ";
                }
                // line 135
                echo "            </div>
            
            <div>
            
              <div class=\"caption\">
                <div class=\"name\"><h4><a href=\"";
                // line 140
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "name", array());
                echo "</a></h4></div>
                <div class=\"product_box_brand\">";
                // line 141
                if ($this->getAttribute($context["product"], "brand", array())) {
                    echo "<a href=\"";
                    echo $this->getAttribute($context["product"], "brand_url", array());
                    echo "\">";
                    echo $this->getAttribute($context["product"], "brand", array());
                    echo "</a>";
                }
                echo "</div>
                <p class=\"description\">";
                // line 142
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>

                <div class=\"rating\">
                  ";
                // line 145
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 146
                    echo "                  ";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        // line 147
                        echo "                  <span class=\"fa fa-stack fa-g\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
                  ";
                    } else {
                        // line 149
                        echo "                  <span class=\"fa fa-stack fa-y\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
                  ";
                    }
                    // line 151
                    echo "                  ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 152
                echo "                </div>
                
                ";
                // line 154
                if ($this->getAttribute($context["product"], "price", array())) {
                    // line 155
                    echo "                <p class=\"price\">
                  ";
                    // line 156
                    if ( !$this->getAttribute($context["product"], "special", array())) {
                        // line 157
                        echo "                  ";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "
                  ";
                    } else {
                        // line 159
                        echo "                  <span class=\"price-new\">";
                        echo $this->getAttribute($context["product"], "special", array());
                        echo "</span> <span class=\"price-old\">";
                        echo $this->getAttribute($context["product"], "price", array());
                        echo "</span>
                  ";
                    }
                    // line 161
                    echo "                </p>
                ";
                }
                // line 163
                echo "                
                <div class=\"product-list-buttons\">
                  <button type=\"button\" onclick=\"cart.add('";
                // line 165
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "', '";
                echo $this->getAttribute($context["product"], "minimum", array());
                echo "');\" class=\"btn btn-default cart\"><i class=\"fa fa-shopping-bag\"></i> <span>";
                echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
                echo "</span></button>
                  <a class=\"btn btn-default quickview list-quickview\" href=\"";
                // line 166
                echo $this->getAttribute($context["product"], "quickview", array());
                echo "\" data-toggle=\"tooltip\" title=\"";
                echo $this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                echo "\"><i class=\"fa fa-search\"></i></a>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 167
                echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
                echo "\" onclick=\"wishlist.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"btn btn-default list-wishlist\"><i class=\"fa fa-heart\"></i></button>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
                // line 168
                echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
                echo "\" onclick=\"compare.add('";
                echo $this->getAttribute($context["product"], "product_id", array());
                echo "');\" class=\"btn btn-default list-compare\"><i class=\"fa fa-retweet\"></i></button>
                </div>
                
              </div>
              
            </div>
          </div>
          
        </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 178
            echo "      </div>
      <div class=\"row pagination-box content-padd padd-t-b-30\">
        <div class=\"col-sm-6 text-left\">";
            // line 180
            echo (isset($context["pagination"]) ? $context["pagination"] : null);
            echo "</div>
        <div class=\"col-sm-6 text-right\">";
            // line 181
            echo (isset($context["results"]) ? $context["results"] : null);
            echo "</div>
      </div>
      ";
        }
        // line 184
        echo "      ";
        if (( !(isset($context["categories"]) ? $context["categories"] : null) &&  !(isset($context["products"]) ? $context["products"] : null))) {
            // line 185
            echo "      <p>";
            echo (isset($context["text_empty"]) ? $context["text_empty"] : null);
            echo "</p>
      <div class=\"buttons\">
        <div class=\"pull-right\"><a href=\"";
            // line 187
            echo (isset($context["continue"]) ? $context["continue"] : null);
            echo "\" class=\"btn btn-primary\">";
            echo (isset($context["button_continue"]) ? $context["button_continue"] : null);
            echo "</a></div>
      </div>
      ";
        }
        // line 190
        echo "      ";
        echo (isset($context["content_bottom"]) ? $context["content_bottom"] : null);
        echo "</div>
    ";
        // line 191
        echo (isset($context["column_right"]) ? $context["column_right"] : null);
        echo "</div>
</div>
";
        // line 193
        echo (isset($context["footer"]) ? $context["footer"] : null);
        echo " ";
    }

    public function getTemplateName()
    {
        return "oxy/template/product/manufacturer_info.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  571 => 193,  566 => 191,  561 => 190,  553 => 187,  547 => 185,  544 => 184,  538 => 181,  534 => 180,  530 => 178,  512 => 168,  506 => 167,  500 => 166,  492 => 165,  488 => 163,  484 => 161,  476 => 159,  470 => 157,  468 => 156,  465 => 155,  463 => 154,  459 => 152,  453 => 151,  449 => 149,  445 => 147,  442 => 146,  438 => 145,  432 => 142,  422 => 141,  416 => 140,  409 => 135,  393 => 133,  379 => 130,  367 => 129,  362 => 128,  360 => 127,  351 => 123,  345 => 122,  339 => 121,  331 => 120,  326 => 117,  320 => 113,  314 => 112,  310 => 110,  306 => 108,  303 => 107,  299 => 106,  294 => 104,  290 => 102,  288 => 101,  283 => 98,  280 => 97,  274 => 95,  271 => 94,  268 => 93,  266 => 92,  262 => 91,  259 => 90,  256 => 89,  250 => 87,  247 => 86,  245 => 85,  241 => 84,  238 => 83,  232 => 82,  230 => 81,  226 => 80,  220 => 76,  215 => 75,  211 => 74,  204 => 69,  200 => 68,  192 => 62,  186 => 61,  178 => 58,  175 => 57,  167 => 54,  164 => 53,  161 => 52,  157 => 51,  149 => 45,  143 => 44,  135 => 41,  132 => 40,  124 => 37,  121 => 36,  118 => 35,  114 => 34,  104 => 29,  97 => 25,  93 => 24,  89 => 23,  85 => 22,  80 => 19,  78 => 18,  74 => 17,  67 => 16,  64 => 15,  61 => 14,  58 => 13,  55 => 12,  52 => 11,  49 => 10,  47 => 9,  43 => 8,  40 => 7,  29 => 5,  25 => 4,  19 => 1,);
    }
}
/* {{ header }}*/
/* <div id="product-manufacturer" class="container full-width-container">*/
/*   <ul class="breadcrumb">*/
/*     {% for breadcrumb in breadcrumbs %}*/
/*     <li><a href="{{ breadcrumb.href }}">{{ breadcrumb.text }}</a></li>*/
/*     {% endfor %}*/
/*   </ul>*/
/*   <div class="row">{{ column_left }}*/
/*     {% if column_left and column_right %}*/
/*     {% set class = 'col-sm-6' %}*/
/*     {% elseif column_left or column_right %}*/
/*     {% set class = 'col-sm-9' %}*/
/*     {% else %}*/
/*     {% set class = 'col-sm-12' %}*/
/*     {% endif %}*/
/*     <div id="content" class="{{ class }}">{{ content_top }}*/
/*       <h2>{{ heading_title }}</h2>*/
/*       {% if products %}*/
/*       <div class="row content-padd padd-t-b-25 product-filter">*/
/*         <div class="col-md-4">*/
/*           <div class="btn-group hidden-xs">*/
/*             <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="{{ button_list }}"><i class="fa fa-th-list"></i></button>*/
/*             <button type="button" id="small-list-view" class="btn btn-default" data-toggle="tooltip" title="{{ t1o_text_small_list[lang_id] }}"><i class="fa fa-list"></i></button>*/
/*             <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="{{ button_grid }}"><i class="fa fa-th"></i></button>*/
/*             <button type="button" id="gallery-view" class="btn btn-default" data-toggle="tooltip" title="{{ t1o_text_gallery[lang_id] }}"><i class="fa fa-th-large"></i></button>*/
/*           </div>*/
/*         </div>*/
/*         <div class="col-md-4">*/
/*           <a href="{{ compare }}" id="compare-total" class="btn btn-default">{{ text_compare }}</a>*/
/*         </div>*/
/*         <div class="col-md-2 text-right">*/
/*           <select id="input-sort" class="form-control" onchange="location = this.value;">*/
/*               */
/*               {% for sorts in sorts %}*/
/*               {% if sorts.value == '%s-%s'|format(sort, order) %}*/
/*               */
/*               <option value="{{ sorts.href }}" selected="selected">{{ sorts.text }}</option>*/
/*               */
/*               {% else %}*/
/*               */
/*               <option value="{{ sorts.href }}">{{ sorts.text }}</option>*/
/*               */
/*               {% endif %}*/
/*               {% endfor %}*/
/*             */
/*           </select>*/
/*         </div>*/
/*         <div class="col-md-2 text-right">*/
/*           <select id="input-limit" class="form-control" onchange="location = this.value;">*/
/*               */
/*               {% for limits in limits %}*/
/*               {% if limits.value == limit %}*/
/*               */
/*               <option value="{{ limits.href }}" selected="selected">{{ limits.text }}</option>*/
/*               */
/*               {% else %}*/
/*               */
/*               <option value="{{ limits.href }}">{{ limits.text }}</option>*/
/*               */
/*               {% endif %}*/
/*               {% endfor %}*/
/*             */
/*           </select>*/
/*         </div>*/
/*       </div>*/
/*       */
/*       <div class="row product-items category-product-items">      */
/*         {% for product in products %}*/
/*         <div class="product-layout product-list col-xs-12">*/
/*           <div class="product-thumb">*/
/*           */
/*             <div class="image">*/
/*             */
/*             {% if product.out_of_stock_quantity <= 0 and t1o_out_of_stock_badge_status == 1 %} */
/*             <span class="badge out-of-stock"><span>{{ product.out_of_stock_badge }}</span></span>*/
/*             {% endif %}  */
/*             */
/*             <span class="badge-wrapper">*/
/*             */
/*             {% if t1o_sale_badge_status == 1 %}	*/
/*             {% if product.special and t1o_sale_badge_type == 0 %}*/
/*             <span class="badge sale">{{ t1o_text_sale[lang_id] }}</span>*/
/*             {% endif %} */
/*             {% if product.special and t1o_sale_badge_type == 1 %}            */
/*             {% set res = (product.val1 / product.val2) * 100 %}*/
/*             {% set res = 100 - res %}*/
/*             <span class="badge sale">-{{ res|round(0, 'common') }}%</span>*/
/*             {% endif %}*/
/*             {% endif %}*/
/*       */
/*             {% if t1o_new_badge_status == 1 %}	*/
/*             {% set days = (product.endDate2|round(0, 'common') / 86400) - (product.startDate1|round(0, 'common') /86400) %}*/
/*             {% set newproductdays = 30 %}*/
/*             {% if days < newproductdays %}*/
/*             <span class="badge new">{{ t1o_text_new_prod[lang_id] }}</span>*/
/*             {% endif %}*/
/*             {% endif %}*/
/*             */
/*             </span>*/
/*             */
/*             {% if t1o_category_prod_box_style == 'product-box-style-3' %}*/
/*             <div class="flybar-top">  */
/*             <div class="flybar-top-items">*/
/*             <p class="description">{{ product.description }}</p>*/
/*             <div class="rating">*/
/*               {% for i in 1..5 %}*/
/*               {% if product.rating < i %}*/
/*               <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*               {% else %}*/
/*               <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*               {% endif %}*/
/*               {% endfor %}*/
/*             </div>*/
/*             </div>   */
/*             </div>*/
/*             {% endif %}*/
/*       */
/*             <div class="flybar">  */
/*             <div class="flybar-items">*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}', '{{ product.minimum }}');" class="btn btn-default"><i class="fa fa-shopping-bag"></i></button>*/
/*             <a class="btn btn-default quickview" href="{{ product.quickview }}" data-toggle="tooltip" title="{{ t1o_text_quickview[lang_id] }}"><i class="fa fa-search"></i></a>*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');" class="wishlist"><i class="fa fa-heart"></i></button>*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');" class="compare"><i class="fa fa-retweet"></i></button>*/
/*             </div>   */
/*             </div>*/
/*             */
/*             {% if product.thumb_swap %}*/
/*             <a href="{{ product.href }}">*/
/*             <img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive img-{{ t1d_img_style }} lazyload" />*/
/*             <img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb_swap }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive thumb_swap img-{{ t1d_img_style }} lazyload" />*/
/*             </a>*/
/*             {% else %}*/
/*             <a href="{{ product.href }}"><img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive img-{{ t1d_img_style }} lazyload" /></a>*/
/*             {% endif %}*/
/*             </div>*/
/*             */
/*             <div>*/
/*             */
/*               <div class="caption">*/
/*                 <div class="name"><h4><a href="{{ product.href }}">{{ product.name }}</a></h4></div>*/
/*                 <div class="product_box_brand">{% if product.brand %}<a href="{{ product.brand_url }}">{{ product.brand }}</a>{% endif %}</div>*/
/*                 <p class="description">{{ product.description }}</p>*/
/* */
/*                 <div class="rating">*/
/*                   {% for i in 1..5 %}*/
/*                   {% if product.rating < i %}*/
/*                   <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*                   {% else %}*/
/*                   <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*                   {% endif %}*/
/*                   {% endfor %}*/
/*                 </div>*/
/*                 */
/*                 {% if product.price %}*/
/*                 <p class="price">*/
/*                   {% if not product.special %}*/
/*                   {{ product.price }}*/
/*                   {% else %}*/
/*                   <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/*                   {% endif %}*/
/*                 </p>*/
/*                 {% endif %}*/
/*                 */
/*                 <div class="product-list-buttons">*/
/*                   <button type="button" onclick="cart.add('{{ product.product_id }}', '{{ product.minimum }}');" class="btn btn-default cart"><i class="fa fa-shopping-bag"></i> <span>{{ button_cart }}</span></button>*/
/*                   <a class="btn btn-default quickview list-quickview" href="{{ product.quickview }}" data-toggle="tooltip" title="{{ t1o_text_quickview[lang_id] }}"><i class="fa fa-search"></i></a>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>*/
/*                 </div>*/
/*                 */
/*               </div>*/
/*               */
/*             </div>*/
/*           </div>*/
/*           */
/*         </div>*/
/*         {% endfor %}*/
/*       </div>*/
/*       <div class="row pagination-box content-padd padd-t-b-30">*/
/*         <div class="col-sm-6 text-left">{{ pagination }}</div>*/
/*         <div class="col-sm-6 text-right">{{ results }}</div>*/
/*       </div>*/
/*       {% endif %}*/
/*       {% if not categories and not products %}*/
/*       <p>{{ text_empty }}</p>*/
/*       <div class="buttons">*/
/*         <div class="pull-right"><a href="{{ continue }}" class="btn btn-primary">{{ button_continue }}</a></div>*/
/*       </div>*/
/*       {% endif %}*/
/*       {{ content_bottom }}</div>*/
/*     {{ column_right }}</div>*/
/* </div>*/
/* {{ footer }} */
