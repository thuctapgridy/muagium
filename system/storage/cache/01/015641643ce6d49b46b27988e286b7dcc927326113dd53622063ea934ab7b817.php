<?php

/* oxy/template/common/currency.twig */
class __TwigTemplate_eb5eb43704a5d96730e64b74dcf7a8cb4011c97a9eb90cdc9900009cd0260de7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, (isset($context["currencies"]) ? $context["currencies"] : null)) > 1)) {
            // line 2
            echo "<div class=\"pull-right\">
<form action=\"";
            // line 3
            echo (isset($context["action"]) ? $context["action"] : null);
            echo "\" method=\"post\" enctype=\"multipart/form-data\" id=\"form-currency\">
  <div class=\"btn-group\">
    <button class=\"btn btn-link dropdown-toggle\" data-toggle=\"dropdown\">
    ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 7
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "    ";
            echo (isset($context["code"]) ? $context["code"] : null);
            echo " <i class=\"fa fa-angle-down\"></i></button>
    <ul class=\"dropdown-menu dropdown-menu-right\">
      ";
            // line 10
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["currencies"]) ? $context["currencies"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["currency"]) {
                // line 11
                echo "      ";
                if ($this->getAttribute($context["currency"], "symbol_left", array())) {
                    // line 12
                    echo "      <li><button class=\"currency-select btn btn-link btn-block\" type=\"button\" name=\"";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "\">";
                    echo $this->getAttribute($context["currency"], "title", array());
                    echo "</button></li>
      ";
                } else {
                    // line 14
                    echo "      <li><button class=\"currency-select btn btn-link btn-block\" type=\"button\" name=\"";
                    echo $this->getAttribute($context["currency"], "code", array());
                    echo "\">";
                    echo $this->getAttribute($context["currency"], "title", array());
                    echo "</button></li>
      ";
                }
                // line 16
                echo "      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['currency'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 17
            echo "    </ul>
  </div>
  <input type=\"hidden\" name=\"code\" value=\"\" />
  <input type=\"hidden\" name=\"redirect\" value=\"";
            // line 20
            echo (isset($context["redirect"]) ? $context["redirect"] : null);
            echo "\" />
</form>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "oxy/template/common/currency.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 20,  75 => 17,  69 => 16,  61 => 14,  53 => 12,  50 => 11,  46 => 10,  40 => 8,  34 => 7,  30 => 6,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if currencies|length > 1 %}*/
/* <div class="pull-right">*/
/* <form action="{{ action }}" method="post" enctype="multipart/form-data" id="form-currency">*/
/*   <div class="btn-group">*/
/*     <button class="btn btn-link dropdown-toggle" data-toggle="dropdown">*/
/*     {% for currency in currencies %}*/
/*     {% endfor %}*/
/*     {{ code }} <i class="fa fa-angle-down"></i></button>*/
/*     <ul class="dropdown-menu dropdown-menu-right">*/
/*       {% for currency in currencies %}*/
/*       {% if currency.symbol_left %}*/
/*       <li><button class="currency-select btn btn-link btn-block" type="button" name="{{ currency.code }}">{{ currency.title }}</button></li>*/
/*       {% else %}*/
/*       <li><button class="currency-select btn btn-link btn-block" type="button" name="{{ currency.code }}">{{ currency.title }}</button></li>*/
/*       {% endif %}*/
/*       {% endfor %}*/
/*     </ul>*/
/*   </div>*/
/*   <input type="hidden" name="code" value="" />*/
/*   <input type="hidden" name="redirect" value="{{ redirect }}" />*/
/* </form>*/
/* </div>*/
/* {% endif %}*/
/* */
