<?php

/* oxy/template/extension/module/category.twig */
class __TwigTemplate_16389b1ce4f9020f0cf2181558766763dbe86ed54743cb2bac4f2f2d8550ce78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == 0)) {
            // line 2
            echo "<div class=\"panel panel-default panel-category\">
  <div class=\"panel-heading\"><h2>";
            // line 3
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h2></div>
<div class=\"list-group\">
  ";
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 6
                echo "  ";
                if (($this->getAttribute($context["category"], "category_id", array()) == (isset($context["category_id"]) ? $context["category_id"] : null))) {
                    echo " 
  <a href=\"";
                    // line 7
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item active\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
  ";
                    // line 8
                    if ($this->getAttribute($context["category"], "children", array())) {
                        // line 9
                        echo "  ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                            // line 10
                            echo "  ";
                            if (($this->getAttribute($context["child"], "category_id", array()) == (isset($context["child_id"]) ? $context["child_id"] : null))) {
                                // line 11
                                echo "  <div class=\"cat-mod-child\">
  <a href=\"";
                                // line 12
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\" class=\"list-group-item active\">&nbsp;&nbsp;&nbsp;&nbsp; ";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "</a>
  </div>
  ";
                            } else {
                                // line 15
                                echo "  <div class=\"cat-mod-child\">
  <a href=\"";
                                // line 16
                                echo $this->getAttribute($context["child"], "href", array());
                                echo "\" class=\"list-group-item\">&nbsp;&nbsp;&nbsp;&nbsp; ";
                                echo $this->getAttribute($context["child"], "name", array());
                                echo "</a>
  </div>
  ";
                            }
                            // line 19
                            echo "  ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 20
                        echo "  ";
                    }
                    // line 21
                    echo "  ";
                } else {
                    // line 22
                    echo "  <a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
  ";
                }
                // line 24
                echo "  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "</div>
</div>
";
        }
        // line 28
        echo "
";
        // line 29
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == 1)) {
            // line 30
            echo "<script type=\"text/javascript\">
\$(document).ready(function() {
\t\$('#accordion-1').dcAccordion({
\t\tdisableLink: false,\t
\t\tmenuClose: false,
\t\tautoClose: true,
\t\tautoExpand: true,\t\t
\t\tsaveState: false
\t});
});
</script>
<div class=\"panel panel-default panel-category\">
  <div class=\"panel-heading\"><h2>";
            // line 42
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h2></div>
    <div class=\"list-group\">
      <ul class=\"accordion\" id=\"accordion-1\">
        ";
            // line 45
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 46
                echo "        <li>
          ";
                // line 47
                if (($this->getAttribute($context["category"], "category_id", array()) == (isset($context["category_id"]) ? $context["category_id"] : null))) {
                    echo " 
          <a href=\"";
                    // line 48
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item active\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
          ";
                } elseif ($this->getAttribute(                // line 49
$context["category"], "children", array())) {
                    // line 50
                    echo "          <a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a><div class=\"dcjq-icon\"><i class=\"fa fa-plus\"></i></div>
          ";
                } else {
                    // line 52
                    echo "          <a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
          ";
                }
                // line 54
                echo "          ";
                if ($this->getAttribute($context["category"], "children", array())) {
                    // line 55
                    echo "          <ul>
            ";
                    // line 56
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 57
                        echo "            <li>
              ";
                        // line 58
                        if (($this->getAttribute($context["child"], "category_id", array()) == (isset($context["child_id"]) ? $context["child_id"] : null))) {
                            // line 59
                            echo "              <a href=\"";
                            echo $this->getAttribute($context["child"], "href", array());
                            echo "\" class=\"list-group-item active\">";
                            echo $this->getAttribute($context["child"], "name", array());
                            echo "</a>
              ";
                        } else {
                            // line 61
                            echo "              <a href=\"";
                            echo $this->getAttribute($context["child"], "href", array());
                            echo "\" class=\"list-group-item\">";
                            echo $this->getAttribute($context["child"], "name", array());
                            echo "</a>
              ";
                        }
                        // line 63
                        echo "            </li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 65
                    echo "          </ul>
          ";
                }
                // line 67
                echo "        </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "      </ul>
  </div>
</div>
";
        }
        // line 73
        echo "
";
        // line 74
        if (((isset($context["t1o_left_right_column_categories_type"]) ? $context["t1o_left_right_column_categories_type"] : null) == 2)) {
            // line 75
            echo "<div class=\"panel panel-default panel-category panel-category-dropdown\">
  <div class=\"panel-heading\"><h2>";
            // line 76
            echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
            echo "</h2></div>
    <div class=\"list-group\">
      <ul>
        ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 80
                echo "        <li>
          ";
                // line 81
                if (($this->getAttribute($context["category"], "category_id", array()) == (isset($context["category_id"]) ? $context["category_id"] : null))) {
                    echo " 
          <a href=\"";
                    // line 82
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item active\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "  <i class=\"fa fa-caret-right \"></i></a>
          ";
                } elseif ($this->getAttribute(                // line 83
$context["category"], "children", array())) {
                    // line 84
                    echo "          <a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo " <i class=\"fa fa-caret-right \"></i></a>
          ";
                } else {
                    // line 86
                    echo "          <a href=\"";
                    echo $this->getAttribute($context["category"], "href", array());
                    echo "\" class=\"list-group-item\">";
                    echo $this->getAttribute($context["category"], "name", array());
                    echo "</a>
          ";
                }
                // line 88
                echo "          ";
                if ($this->getAttribute($context["category"], "children", array())) {
                    // line 89
                    echo "          <div class=\"dropdown-menus\">  
          <ul>
            ";
                    // line 91
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["category"], "children", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                        // line 92
                        echo "            <li>
              ";
                        // line 93
                        if (($this->getAttribute($context["child"], "category_id", array()) == (isset($context["child_id"]) ? $context["child_id"] : null))) {
                            // line 94
                            echo "              <a href=\"";
                            echo $this->getAttribute($context["child"], "href", array());
                            echo "\" class=\"list-group-item active\">";
                            echo $this->getAttribute($context["child"], "name", array());
                            echo "</a>
              ";
                        } else {
                            // line 96
                            echo "              <a href=\"";
                            echo $this->getAttribute($context["child"], "href", array());
                            echo "\" class=\"list-group-item\">";
                            echo $this->getAttribute($context["child"], "name", array());
                            echo "</a>
              ";
                        }
                        // line 98
                        echo "            </li>
            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 100
                    echo "          </ul>
          </div>
          ";
                }
                // line 103
                echo "        </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 105
            echo "      </ul>
  </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "oxy/template/extension/module/category.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  322 => 105,  315 => 103,  310 => 100,  303 => 98,  295 => 96,  287 => 94,  285 => 93,  282 => 92,  278 => 91,  274 => 89,  271 => 88,  263 => 86,  255 => 84,  253 => 83,  247 => 82,  243 => 81,  240 => 80,  236 => 79,  230 => 76,  227 => 75,  225 => 74,  222 => 73,  216 => 69,  209 => 67,  205 => 65,  198 => 63,  190 => 61,  182 => 59,  180 => 58,  177 => 57,  173 => 56,  170 => 55,  167 => 54,  159 => 52,  151 => 50,  149 => 49,  143 => 48,  139 => 47,  136 => 46,  132 => 45,  126 => 42,  112 => 30,  110 => 29,  107 => 28,  102 => 25,  96 => 24,  88 => 22,  85 => 21,  82 => 20,  76 => 19,  68 => 16,  65 => 15,  57 => 12,  54 => 11,  51 => 10,  46 => 9,  44 => 8,  38 => 7,  33 => 6,  29 => 5,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% if t1o_left_right_column_categories_type == 0 %}*/
/* <div class="panel panel-default panel-category">*/
/*   <div class="panel-heading"><h2>{{ heading_title }}</h2></div>*/
/* <div class="list-group">*/
/*   {% for category in categories %}*/
/*   {% if category.category_id == category_id %} */
/*   <a href="{{ category.href }}" class="list-group-item active">{{ category.name }}</a>*/
/*   {% if category.children %}*/
/*   {% for child in category.children %}*/
/*   {% if child.category_id == child_id %}*/
/*   <div class="cat-mod-child">*/
/*   <a href="{{ child.href }}" class="list-group-item active">&nbsp;&nbsp;&nbsp;&nbsp; {{ child.name }}</a>*/
/*   </div>*/
/*   {% else %}*/
/*   <div class="cat-mod-child">*/
/*   <a href="{{ child.href }}" class="list-group-item">&nbsp;&nbsp;&nbsp;&nbsp; {{ child.name }}</a>*/
/*   </div>*/
/*   {% endif %}*/
/*   {% endfor %}*/
/*   {% endif %}*/
/*   {% else %}*/
/*   <a href="{{ category.href }}" class="list-group-item">{{ category.name }}</a>*/
/*   {% endif %}*/
/*   {% endfor %}*/
/* </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_left_right_column_categories_type == 1 %}*/
/* <script type="text/javascript">*/
/* $(document).ready(function() {*/
/* 	$('#accordion-1').dcAccordion({*/
/* 		disableLink: false,	*/
/* 		menuClose: false,*/
/* 		autoClose: true,*/
/* 		autoExpand: true,		*/
/* 		saveState: false*/
/* 	});*/
/* });*/
/* </script>*/
/* <div class="panel panel-default panel-category">*/
/*   <div class="panel-heading"><h2>{{ heading_title }}</h2></div>*/
/*     <div class="list-group">*/
/*       <ul class="accordion" id="accordion-1">*/
/*         {% for category in categories %}*/
/*         <li>*/
/*           {% if category.category_id == category_id %} */
/*           <a href="{{ category.href }}" class="list-group-item active">{{ category.name }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*           {% elseif  category.children %}*/
/*           <a href="{{ category.href }}" class="list-group-item">{{ category.name }}</a><div class="dcjq-icon"><i class="fa fa-plus"></i></div>*/
/*           {% else %}*/
/*           <a href="{{ category.href }}" class="list-group-item">{{ category.name }}</a>*/
/*           {% endif %}*/
/*           {% if category.children %}*/
/*           <ul>*/
/*             {% for child in category.children %}*/
/*             <li>*/
/*               {% if child.category_id == child_id %}*/
/*               <a href="{{ child.href }}" class="list-group-item active">{{ child.name }}</a>*/
/*               {% else %}*/
/*               <a href="{{ child.href }}" class="list-group-item">{{ child.name }}</a>*/
/*               {% endif %}*/
/*             </li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*           {% endif %}*/
/*         </li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*   </div>*/
/* </div>*/
/* {% endif %}*/
/* */
/* {% if t1o_left_right_column_categories_type == 2 %}*/
/* <div class="panel panel-default panel-category panel-category-dropdown">*/
/*   <div class="panel-heading"><h2>{{ heading_title }}</h2></div>*/
/*     <div class="list-group">*/
/*       <ul>*/
/*         {% for category in categories %}*/
/*         <li>*/
/*           {% if category.category_id == category_id %} */
/*           <a href="{{ category.href }}" class="list-group-item active">{{ category.name }}  <i class="fa fa-caret-right "></i></a>*/
/*           {% elseif  category.children %}*/
/*           <a href="{{ category.href }}" class="list-group-item">{{ category.name }} <i class="fa fa-caret-right "></i></a>*/
/*           {% else %}*/
/*           <a href="{{ category.href }}" class="list-group-item">{{ category.name }}</a>*/
/*           {% endif %}*/
/*           {% if category.children %}*/
/*           <div class="dropdown-menus">  */
/*           <ul>*/
/*             {% for child in category.children %}*/
/*             <li>*/
/*               {% if child.category_id == child_id %}*/
/*               <a href="{{ child.href }}" class="list-group-item active">{{ child.name }}</a>*/
/*               {% else %}*/
/*               <a href="{{ child.href }}" class="list-group-item">{{ child.name }}</a>*/
/*               {% endif %}*/
/*             </li>*/
/*             {% endfor %}*/
/*           </ul>*/
/*           </div>*/
/*           {% endif %}*/
/*         </li>*/
/*         {% endfor %}*/
/*       </ul>*/
/*   </div>*/
/* </div>*/
/* {% endif %}*/
/* */
