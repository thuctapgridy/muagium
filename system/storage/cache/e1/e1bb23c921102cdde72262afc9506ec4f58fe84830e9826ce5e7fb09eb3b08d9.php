<?php

/* oxy/template/extension/module/featured.twig */
class __TwigTemplate_8d0f0c2d8753f5404a4397f255075e5ba087006354889922312e2eb3a578c63a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=\"panel panel-default product-module full-width-container\">
  <div class=\"panel-heading module-heading\"><h2>";
        // line 3
        echo (isset($context["heading_title"]) ? $context["heading_title"] : null);
        echo "</h2></div>
  <div class=\"row product-items-wrapper\">
  <div class=\"product-items product-items-featured product-items-";
        // line 5
        echo (isset($context["t1o_featured_style"]) ? $context["t1o_featured_style"] : null);
        echo "\">
  ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 7
            echo "  ";
            if (((isset($context["t1o_featured_style"]) ? $context["t1o_featured_style"] : null) == 1)) {
                // line 8
                echo "  <div class=\"product-layout-slider product-grid col-xs-12\">
  ";
            } else {
                // line 10
                echo "  <div class=\"product-layout-grid product-grid col-xs-12\">
  ";
            }
            // line 12
            echo "    <div class=\"product-thumb transition\">
      <div class=\"image\"> 
          
            ";
            // line 15
            if ((($this->getAttribute($context["product"], "out_of_stock_quantity", array()) <= 0) && ((isset($context["t1o_out_of_stock_badge_status"]) ? $context["t1o_out_of_stock_badge_status"] : null) == 1))) {
                // line 16
                echo "            <span class=\"badge out-of-stock\"><span>";
                echo $this->getAttribute($context["product"], "out_of_stock_badge", array());
                echo "</span></span>
            ";
            }
            // line 18
            echo "            
            <span class=\"badge-wrapper\">
            
            ";
            // line 21
            if (((isset($context["t1o_sale_badge_status"]) ? $context["t1o_sale_badge_status"] : null) == 1)) {
                echo "\t
            ";
                // line 22
                if (($this->getAttribute($context["product"], "special", array()) && ((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == 0))) {
                    // line 23
                    echo "            <span class=\"badge sale\">";
                    echo $this->getAttribute((isset($context["t1o_text_sale"]) ? $context["t1o_text_sale"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "</span>
            ";
                }
                // line 24
                echo " 
            ";
                // line 25
                if (($this->getAttribute($context["product"], "special", array()) && ((isset($context["t1o_sale_badge_type"]) ? $context["t1o_sale_badge_type"] : null) == 1))) {
                    echo "            
            ";
                    // line 26
                    $context["res"] = (($this->getAttribute($context["product"], "val1", array()) / $this->getAttribute($context["product"], "val2", array())) * 100);
                    // line 27
                    echo "            ";
                    $context["res"] = (100 - (isset($context["res"]) ? $context["res"] : null));
                    // line 28
                    echo "            <span class=\"badge sale\">-";
                    echo twig_round((isset($context["res"]) ? $context["res"] : null), 0, "common");
                    echo "%</span>
            ";
                }
                // line 30
                echo "            ";
            }
            // line 31
            echo "      
            ";
            // line 32
            if (((isset($context["t1o_new_badge_status"]) ? $context["t1o_new_badge_status"] : null) == 1)) {
                echo "\t
            ";
                // line 33
                $context["days"] = ((twig_round($this->getAttribute($context["product"], "endDate2", array()), 0, "common") / 86400) - (twig_round($this->getAttribute($context["product"], "startDate1", array()), 0, "common") / 86400));
                // line 34
                echo "            ";
                $context["newproductdays"] = 30;
                // line 35
                echo "            ";
                if (((isset($context["days"]) ? $context["days"] : null) < (isset($context["newproductdays"]) ? $context["newproductdays"] : null))) {
                    // line 36
                    echo "            <span class=\"badge new\">";
                    echo $this->getAttribute((isset($context["t1o_text_new_prod"]) ? $context["t1o_text_new_prod"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
                    echo "</span>
            ";
                }
                // line 38
                echo "            ";
            }
            // line 39
            echo "            
            </span>
            
            ";
            // line 42
            if (((isset($context["t1o_category_prod_box_style"]) ? $context["t1o_category_prod_box_style"] : null) == "product-box-style-3")) {
                // line 43
                echo "            <div class=\"flybar-top\">  
            <div class=\"flybar-top-items\">
            <p class=\"description\">";
                // line 45
                echo $this->getAttribute($context["product"], "description", array());
                echo "</p>
            <div class=\"rating\">
              ";
                // line 47
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(range(1, 5));
                foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                    // line 48
                    echo "              ";
                    if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                        // line 49
                        echo "              <span class=\"fa fa-stack fa-g\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
              ";
                    } else {
                        // line 51
                        echo "              <span class=\"fa fa-stack fa-y\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
              ";
                    }
                    // line 53
                    echo "              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 54
                echo "            </div>
            </div>   
            </div>
            ";
            }
            // line 58
            echo "            
            <div class=\"flybar\">  
            <div class=\"flybar-items\">
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 61
            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
            echo "\" onclick=\"cart.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\" class=\"btn btn-default\"><i class=\"fa fa-shopping-bag\"></i></button>
            <a class=\"btn btn-default quickview\" href=\"";
            // line 62
            echo $this->getAttribute($context["product"], "quickview", array());
            echo "\" data-toggle=\"tooltip\" title=\"";
            echo $this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "\"><i class=\"fa fa-search\"></i></a>
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 63
            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
            echo "\" onclick=\"wishlist.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\" class=\"wishlist\"><i class=\"fa fa-heart\"></i></button>
            <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 64
            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
            echo "\" onclick=\"compare.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\" class=\"compare\"><i class=\"fa fa-retweet\"></i></button>
            </div>   
            </div>
            
            ";
            // line 68
            if ($this->getAttribute($context["product"], "thumb_swap", array())) {
                // line 69
                echo "            <a href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\">
            <img src=\"";
                // line 70
                echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                echo "\" data-src=\"";
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-responsive img-";
                echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                echo " lazyload\" />
            <img src=\"";
                // line 71
                echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                echo "\" data-src=\"";
                echo $this->getAttribute($context["product"], "thumb_swap", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-responsive thumb_swap img-";
                echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                echo " lazyload\" />
            </a>
            ";
            } else {
                // line 74
                echo "            <a href=\"";
                echo $this->getAttribute($context["product"], "href", array());
                echo "\"><img src=\"";
                echo (isset($context["lazy_load_placeholder"]) ? $context["lazy_load_placeholder"] : null);
                echo "\" data-src=\"";
                echo $this->getAttribute($context["product"], "thumb", array());
                echo "\" alt=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" title=\"";
                echo $this->getAttribute($context["product"], "name", array());
                echo "\" class=\"img-responsive img-";
                echo (isset($context["t1d_img_style"]) ? $context["t1d_img_style"] : null);
                echo " lazyload\" /></a>
            ";
            }
            // line 75
            echo " 
      </div>
            
      <div class=\"caption\">
              
                <div class=\"name\"><h4><a href=\"";
            // line 80
            echo $this->getAttribute($context["product"], "href", array());
            echo "\">";
            echo $this->getAttribute($context["product"], "name", array());
            echo "</a></h4></div>
                <div class=\"product_box_brand\">";
            // line 81
            if ($this->getAttribute($context["product"], "brand", array())) {
                echo "<a href=\"";
                echo $this->getAttribute($context["product"], "brand_url", array());
                echo "\">";
                echo $this->getAttribute($context["product"], "brand", array());
                echo "</a>";
            }
            echo "</div>
                <p class=\"description\">";
            // line 82
            echo $this->getAttribute($context["product"], "description", array());
            echo "</p>

                <div class=\"rating\">
                  ";
            // line 85
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, 5));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 86
                echo "                  ";
                if (($this->getAttribute($context["product"], "rating", array()) < $context["i"])) {
                    // line 87
                    echo "                  <span class=\"fa fa-stack fa-g\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
                  ";
                } else {
                    // line 89
                    echo "                  <span class=\"fa fa-stack fa-y\"><i class=\"fa fa-star fa-stack-2x\"></i></span>
                  ";
                }
                // line 91
                echo "                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 92
            echo "                </div>
                
                ";
            // line 94
            if ($this->getAttribute($context["product"], "price", array())) {
                // line 95
                echo "                <p class=\"price\">
                  ";
                // line 96
                if ( !$this->getAttribute($context["product"], "special", array())) {
                    echo " 
                  ";
                    // line 97
                    echo $this->getAttribute($context["product"], "price", array());
                    echo "
                  ";
                } else {
                    // line 99
                    echo "                  <span class=\"price-new\">";
                    echo $this->getAttribute($context["product"], "special", array());
                    echo "</span> <span class=\"price-old\">";
                    echo $this->getAttribute($context["product"], "price", array());
                    echo "</span>
                  ";
                }
                // line 101
                echo "                </p>
                ";
            }
            // line 103
            echo "                
                <div class=\"product-list-buttons\">
                  <button type=\"button\" onclick=\"cart.add('";
            // line 105
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\" class=\"btn btn-default cart\"><i class=\"fa fa-shopping-bag\"></i> <span>";
            echo (isset($context["button_cart"]) ? $context["button_cart"] : null);
            echo "</span></button>
                  <a class=\"btn btn-default quickview list-quickview\" href=\"";
            // line 106
            echo $this->getAttribute($context["product"], "quickview", array());
            echo "\" data-toggle=\"tooltip\" title=\"";
            echo $this->getAttribute((isset($context["t1o_text_quickview"]) ? $context["t1o_text_quickview"] : null), (isset($context["lang_id"]) ? $context["lang_id"] : null), array(), "array");
            echo "\"><i class=\"fa fa-search\"></i></a>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 107
            echo (isset($context["button_wishlist"]) ? $context["button_wishlist"] : null);
            echo "\" onclick=\"wishlist.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\" class=\"btn btn-default list-wishlist\"><i class=\"fa fa-heart\"></i></button>
                  <button type=\"button\" data-toggle=\"tooltip\" title=\"";
            // line 108
            echo (isset($context["button_compare"]) ? $context["button_compare"] : null);
            echo "\" onclick=\"compare.add('";
            echo $this->getAttribute($context["product"], "product_id", array());
            echo "');\" class=\"btn btn-default list-compare\"><i class=\"fa fa-retweet\"></i></button>
                </div>
                
      </div>
    </div>
  </div>
  ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 115
        echo "</div>
";
        // line 116
        if (((isset($context["t1o_featured_style"]) ? $context["t1o_featured_style"] : null) == 1)) {
            // line 117
            echo "<script type=\"text/javascript\"><!--
\$('.product-items-featured').owlCarousel({
\titems: '";
            // line 119
            echo (isset($context["t1o_featured_per_row"]) ? $context["t1o_featured_per_row"] : null);
            echo "',
\titemsMobile : [479, 2],
\tsingleItem: false,
\tscrollPerPage: true,
\tpagination: false,
\tnavigation: true,
\tnavigationText: ['<i class=\"fa fa-chevron-left fa-5x\"></i>', '<i class=\"fa fa-chevron-right fa-5x\"></i>']
});
--></script>
";
        }
        // line 129
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "oxy/template/extension/module/featured.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 129,  372 => 119,  368 => 117,  366 => 116,  363 => 115,  348 => 108,  342 => 107,  336 => 106,  330 => 105,  326 => 103,  322 => 101,  314 => 99,  309 => 97,  305 => 96,  302 => 95,  300 => 94,  296 => 92,  290 => 91,  286 => 89,  282 => 87,  279 => 86,  275 => 85,  269 => 82,  259 => 81,  253 => 80,  246 => 75,  230 => 74,  216 => 71,  204 => 70,  199 => 69,  197 => 68,  188 => 64,  182 => 63,  176 => 62,  170 => 61,  165 => 58,  159 => 54,  153 => 53,  149 => 51,  145 => 49,  142 => 48,  138 => 47,  133 => 45,  129 => 43,  127 => 42,  122 => 39,  119 => 38,  113 => 36,  110 => 35,  107 => 34,  105 => 33,  101 => 32,  98 => 31,  95 => 30,  89 => 28,  86 => 27,  84 => 26,  80 => 25,  77 => 24,  71 => 23,  69 => 22,  65 => 21,  60 => 18,  54 => 16,  52 => 15,  47 => 12,  43 => 10,  39 => 8,  36 => 7,  32 => 6,  28 => 5,  23 => 3,  19 => 1,);
    }
}
/* */
/* <div class="panel panel-default product-module full-width-container">*/
/*   <div class="panel-heading module-heading"><h2>{{ heading_title }}</h2></div>*/
/*   <div class="row product-items-wrapper">*/
/*   <div class="product-items product-items-featured product-items-{{ t1o_featured_style }}">*/
/*   {% for product in products %}*/
/*   {% if t1o_featured_style == 1 %}*/
/*   <div class="product-layout-slider product-grid col-xs-12">*/
/*   {% else %}*/
/*   <div class="product-layout-grid product-grid col-xs-12">*/
/*   {% endif %}*/
/*     <div class="product-thumb transition">*/
/*       <div class="image"> */
/*           */
/*             {% if product.out_of_stock_quantity <= 0 and t1o_out_of_stock_badge_status == 1 %}*/
/*             <span class="badge out-of-stock"><span>{{ product.out_of_stock_badge }}</span></span>*/
/*             {% endif %}*/
/*             */
/*             <span class="badge-wrapper">*/
/*             */
/*             {% if t1o_sale_badge_status == 1 %}	*/
/*             {% if product.special and t1o_sale_badge_type == 0 %}*/
/*             <span class="badge sale">{{ t1o_text_sale[lang_id] }}</span>*/
/*             {% endif %} */
/*             {% if product.special and t1o_sale_badge_type == 1 %}            */
/*             {% set res = (product.val1 / product.val2) * 100 %}*/
/*             {% set res = 100 - res %}*/
/*             <span class="badge sale">-{{ res|round(0, 'common') }}%</span>*/
/*             {% endif %}*/
/*             {% endif %}*/
/*       */
/*             {% if t1o_new_badge_status == 1 %}	*/
/*             {% set days = (product.endDate2|round(0, 'common') / 86400) - (product.startDate1|round(0, 'common') /86400) %}*/
/*             {% set newproductdays = 30 %}*/
/*             {% if days < newproductdays %}*/
/*             <span class="badge new">{{ t1o_text_new_prod[lang_id] }}</span>*/
/*             {% endif %}*/
/*             {% endif %}*/
/*             */
/*             </span>*/
/*             */
/*             {% if t1o_category_prod_box_style == 'product-box-style-3' %}*/
/*             <div class="flybar-top">  */
/*             <div class="flybar-top-items">*/
/*             <p class="description">{{ product.description }}</p>*/
/*             <div class="rating">*/
/*               {% for i in 1..5 %}*/
/*               {% if product.rating < i %}*/
/*               <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*               {% else %}*/
/*               <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*               {% endif %}*/
/*               {% endfor %}*/
/*             </div>*/
/*             </div>   */
/*             </div>*/
/*             {% endif %}*/
/*             */
/*             <div class="flybar">  */
/*             <div class="flybar-items">*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_cart }}" onclick="cart.add('{{ product.product_id }}');" class="btn btn-default"><i class="fa fa-shopping-bag"></i></button>*/
/*             <a class="btn btn-default quickview" href="{{ product.quickview }}" data-toggle="tooltip" title="{{ t1o_text_quickview[lang_id] }}"><i class="fa fa-search"></i></a>*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');" class="wishlist"><i class="fa fa-heart"></i></button>*/
/*             <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');" class="compare"><i class="fa fa-retweet"></i></button>*/
/*             </div>   */
/*             </div>*/
/*             */
/*             {% if product.thumb_swap %}*/
/*             <a href="{{ product.href }}">*/
/*             <img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive img-{{ t1d_img_style }} lazyload" />*/
/*             <img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb_swap }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive thumb_swap img-{{ t1d_img_style }} lazyload" />*/
/*             </a>*/
/*             {% else %}*/
/*             <a href="{{ product.href }}"><img src="{{ lazy_load_placeholder }}" data-src="{{ product.thumb }}" alt="{{ product.name }}" title="{{ product.name }}" class="img-responsive img-{{ t1d_img_style }} lazyload" /></a>*/
/*             {% endif %} */
/*       </div>*/
/*             */
/*       <div class="caption">*/
/*               */
/*                 <div class="name"><h4><a href="{{ product.href }}">{{ product.name }}</a></h4></div>*/
/*                 <div class="product_box_brand">{% if product.brand %}<a href="{{ product.brand_url }}">{{ product.brand }}</a>{% endif %}</div>*/
/*                 <p class="description">{{ product.description }}</p>*/
/* */
/*                 <div class="rating">*/
/*                   {% for i in 1..5 %}*/
/*                   {% if product.rating < i %}*/
/*                   <span class="fa fa-stack fa-g"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*                   {% else %}*/
/*                   <span class="fa fa-stack fa-y"><i class="fa fa-star fa-stack-2x"></i></span>*/
/*                   {% endif %}*/
/*                   {% endfor %}*/
/*                 </div>*/
/*                 */
/*                 {% if product.price %}*/
/*                 <p class="price">*/
/*                   {% if not product.special %} */
/*                   {{ product.price }}*/
/*                   {% else %}*/
/*                   <span class="price-new">{{ product.special }}</span> <span class="price-old">{{ product.price }}</span>*/
/*                   {% endif %}*/
/*                 </p>*/
/*                 {% endif %}*/
/*                 */
/*                 <div class="product-list-buttons">*/
/*                   <button type="button" onclick="cart.add('{{ product.product_id }}');" class="btn btn-default cart"><i class="fa fa-shopping-bag"></i> <span>{{ button_cart }}</span></button>*/
/*                   <a class="btn btn-default quickview list-quickview" href="{{ product.quickview }}" data-toggle="tooltip" title="{{ t1o_text_quickview[lang_id] }}"><i class="fa fa-search"></i></a>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_wishlist }}" onclick="wishlist.add('{{ product.product_id }}');" class="btn btn-default list-wishlist"><i class="fa fa-heart"></i></button>*/
/*                   <button type="button" data-toggle="tooltip" title="{{ button_compare }}" onclick="compare.add('{{ product.product_id }}');" class="btn btn-default list-compare"><i class="fa fa-retweet"></i></button>*/
/*                 </div>*/
/*                 */
/*       </div>*/
/*     </div>*/
/*   </div>*/
/*   {% endfor %}*/
/* </div>*/
/* {% if t1o_featured_style == 1 %}*/
/* <script type="text/javascript"><!--*/
/* $('.product-items-featured').owlCarousel({*/
/* 	items: '{{ t1o_featured_per_row }}',*/
/* 	itemsMobile : [479, 2],*/
/* 	singleItem: false,*/
/* 	scrollPerPage: true,*/
/* 	pagination: false,*/
/* 	navigation: true,*/
/* 	navigationText: ['<i class="fa fa-chevron-left fa-5x"></i>', '<i class="fa fa-chevron-right fa-5x"></i>']*/
/* });*/
/* --></script>*/
/* {% endif %}*/
/* </div>*/
/* </div>*/
/* */
