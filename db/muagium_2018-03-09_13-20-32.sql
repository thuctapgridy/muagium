-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2018 at 03:09 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `muagium`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_address`
--

CREATE TABLE `oc_address` (
  `address_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `company` varchar(40) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL DEFAULT '0',
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_address`
--

INSERT INTO `oc_address` (`address_id`, `customer_id`, `firstname`, `lastname`, `company`, `address_1`, `address_2`, `city`, `postcode`, `country_id`, `zone_id`, `custom_field`) VALUES
(1, 1, 'John', 'Rodman', '', '135 South Park Avenue', '', 'Los Angeles', 'CA 90024', 223, 3624, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api`
--

CREATE TABLE `oc_api` (
  `api_id` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `key` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_api`
--

INSERT INTO `oc_api` (`api_id`, `username`, `key`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Default', 'AdRpjSJurTqlF15J31UPZ8VsCD07P6PuLDwK5Q4OeFMsB7HfwjmKK5FGR04nI974rqlGl2WS56cS3JZM3sYaFmr4LNFB1wJwBUiKna9L4s2hGS26rO7JRckyHJMY3DjkhzUByFqTsUf0uTZP0E3UalyvzeqOXiqvkmS5Rg2u9THr5SPNOB3OZAyEUrtst3m3Eu8fKO1EslCOELP7RRwZFaokDn7B5pg7brb5mXG34pls89phRxaQgqDuCeMwvREN', 1, '2017-07-12 11:49:53', '2017-07-12 11:49:53');

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_ip`
--

CREATE TABLE `oc_api_ip` (
  `api_ip_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_api_session`
--

CREATE TABLE `oc_api_session` (
  `api_session_id` int(11) NOT NULL,
  `api_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute`
--

CREATE TABLE `oc_attribute` (
  `attribute_id` int(11) NOT NULL,
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute`
--

INSERT INTO `oc_attribute` (`attribute_id`, `attribute_group_id`, `sort_order`) VALUES
(1, 6, 1),
(2, 6, 5),
(3, 6, 3),
(25, 11, 1),
(24, 10, 1),
(23, 9, 2),
(22, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_description`
--

CREATE TABLE `oc_attribute_description` (
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_description`
--

INSERT INTO `oc_attribute_description` (`attribute_id`, `language_id`, `name`) VALUES
(1, 1, 'Description'),
(2, 1, 'No. of Cores'),
(24, 2, 'Washing'),
(25, 2, 'Style'),
(2, 2, 'No. of Cores'),
(1, 2, 'Description'),
(25, 1, 'Style'),
(24, 1, 'Washing'),
(3, 1, 'Clockspeed'),
(23, 2, 'Polyester'),
(3, 2, 'Clockspeed'),
(23, 1, 'Polyester'),
(22, 1, 'Cotton'),
(22, 2, 'Cotton');

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group`
--

CREATE TABLE `oc_attribute_group` (
  `attribute_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group`
--

INSERT INTO `oc_attribute_group` (`attribute_group_id`, `sort_order`) VALUES
(3, 2),
(4, 1),
(5, 3),
(6, 4),
(10, 2),
(9, 1),
(11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_attribute_group_description`
--

CREATE TABLE `oc_attribute_group_description` (
  `attribute_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_attribute_group_description`
--

INSERT INTO `oc_attribute_group_description` (`attribute_group_id`, `language_id`, `name`) VALUES
(3, 1, 'Memory'),
(4, 1, 'Technical'),
(5, 1, 'Motherboard'),
(6, 1, 'Processor'),
(9, 1, 'Material'),
(10, 1, 'Cleaning'),
(11, 1, 'Other'),
(3, 2, 'Memory'),
(4, 2, 'Technical'),
(5, 2, 'Motherboard'),
(6, 2, 'Processor'),
(9, 2, 'Material'),
(10, 2, 'Cleaning'),
(11, 2, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner`
--

CREATE TABLE `oc_banner` (
  `banner_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner`
--

INSERT INTO `oc_banner` (`banner_id`, `name`, `status`) VALUES
(6, 'HP Products', 1),
(7, 'Home Page Slideshow', 1),
(8, 'Manufacturers', 1),
(9, 'Video Reviews', 1),
(10, 'Gallery', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_banner_image`
--

CREATE TABLE `oc_banner_image` (
  `banner_image_id` int(11) NOT NULL,
  `banner_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_banner_image`
--

INSERT INTO `oc_banner_image` (`banner_image_id`, `banner_id`, `language_id`, `title`, `link`, `image`, `sort_order`) VALUES
(100, 7, 1, 'MacBookAir', '', 'catalog/banner/slide_2_bg.jpg', 2),
(87, 6, 1, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(94, 8, 1, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(95, 8, 1, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(96, 8, 1, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(91, 8, 1, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(92, 8, 1, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(93, 8, 1, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(88, 8, 1, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(89, 8, 1, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(90, 8, 1, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(99, 7, 1, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/banner/slide_1_bg.jpg', 1),
(97, 8, 1, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(98, 8, 1, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0),
(101, 7, 2, 'MacBookAir', '', 'catalog/banner/slide_2_bg.jpg', 2),
(102, 6, 2, 'HP Banner', 'index.php?route=product/manufacturer/info&amp;manufacturer_id=7', 'catalog/demo/compaq_presario.jpg', 0),
(103, 8, 2, 'NFL', '', 'catalog/demo/manufacturer/nfl.png', 0),
(104, 8, 2, 'RedBull', '', 'catalog/demo/manufacturer/redbull.png', 0),
(105, 8, 2, 'Sony', '', 'catalog/demo/manufacturer/sony.png', 0),
(106, 8, 2, 'Coca Cola', '', 'catalog/demo/manufacturer/cocacola.png', 0),
(107, 8, 2, 'Burger King', '', 'catalog/demo/manufacturer/burgerking.png', 0),
(108, 8, 2, 'Canon', '', 'catalog/demo/manufacturer/canon.png', 0),
(109, 8, 2, 'Harley Davidson', '', 'catalog/demo/manufacturer/harley.png', 0),
(110, 8, 2, 'Dell', '', 'catalog/demo/manufacturer/dell.png', 0),
(111, 8, 2, 'Disney', '', 'catalog/demo/manufacturer/disney.png', 0),
(112, 7, 2, 'iPhone 6', 'index.php?route=product/product&amp;path=57&amp;product_id=49', 'catalog/banner/slide_1_bg.jpg', 1),
(113, 8, 2, 'Starbucks', '', 'catalog/demo/manufacturer/starbucks.png', 0),
(114, 8, 2, 'Nintendo', '', 'catalog/demo/manufacturer/nintendo.png', 0),
(168, 9, 2, 'Canon T7i 800D Digital Camera', 'https://www.youtube.com/watch?v=FflMjrzBS9E', 'catalog/video_reviews/product_review_1.jpg', 1),
(167, 9, 1, 'Sony MDR-XB950N1 Headphone', 'https://www.youtube.com/watch?v=Hp8o70M6JgE', 'catalog/video_reviews/product_review_3.jpg', 7),
(166, 9, 1, 'Logitech MX Anywhere 2S', 'https://www.youtube.com/watch?v=FLiXztzokRw', 'catalog/video_reviews/product_review_5.jpg', 6),
(165, 9, 1, 'Angelcare AC401 Baby Monitor', 'https://www.youtube.com/watch?v=qKRQqsGKUHA', 'catalog/video_reviews/product_review_6.jpg', 5),
(164, 9, 1, 'Emmaljunga Stroller Review', 'https://www.youtube.com/watch?v=yd_kyknTrE4', 'catalog/video_reviews/product_review_4.jpg', 4),
(163, 9, 1, 'Wacom Intuos Pen &amp; Touch', 'https://www.youtube.com/watch?v=w2DLTEn830I', 'catalog/video_reviews/product_review_7.jpg', 3),
(162, 9, 1, 'Maxi Cosi Tobi Review', 'https://www.youtube.com/watch?v=UPImJrGR6u0', 'catalog/video_reviews/product_review_2.jpg', 2),
(161, 9, 1, 'Canon T7i 800D Digital Camera', 'https://www.youtube.com/watch?v=FflMjrzBS9E', 'catalog/video_reviews/product_review_1.jpg', 1),
(169, 9, 2, 'Canon T7i 800D Digital Camera', 'https://www.youtube.com/watch?v=FflMjrzBS9E', 'catalog/video_reviews/product_review_1.jpg', 2),
(211, 10, 1, 'Suspendisse vitae neque vel nisl mollis.', '#', 'catalog/gallery/gallery_6.jpg', 6),
(212, 10, 1, 'Nam non tincidunt urna.', '#', 'catalog/gallery/gallery_7.jpg', 7),
(213, 10, 1, 'Fusce sem pretium hendrerit et at dolor.', '#', 'catalog/gallery/gallery_9.jpg', 8),
(207, 10, 1, 'Donec non ante justo at ultrices quam.', '#', 'catalog/gallery/gallery_2.jpg', 2),
(208, 10, 1, 'Integer sit amet nisi ante.', '#', 'catalog/gallery/gallery_3.jpg', 3),
(209, 10, 1, 'Vivamus fringilla iaculis libero at lobortis.', '#', 'catalog/gallery/gallery_4.jpg', 4),
(210, 10, 1, 'Ut a consectetur metus.', '#', 'catalog/gallery/gallery_5.jpg', 5),
(206, 10, 1, ' Nunc ornare adipiscing orci eu consectetur.', '#', 'catalog/gallery/gallery_1.jpg', 1),
(214, 10, 1, 'Aenean egestas congue est.', '#', 'catalog/gallery/gallery_8.jpg', 9);

-- --------------------------------------------------------

--
-- Table structure for table `oc_cart`
--

CREATE TABLE `oc_cart` (
  `cart_id` int(11) UNSIGNED NOT NULL,
  `api_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `session_id` varchar(32) NOT NULL,
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `option` text NOT NULL,
  `quantity` int(5) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE `oc_category` (
  `category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(72, 'catalog/category/category_electronics_1.jpg', 60, 1, 1, 2, 1, '2017-07-12 17:56:03', '2017-08-04 16:43:42'),
(70, 'catalog/category/category_home_1.jpg', 59, 1, 1, 5, 1, '2017-07-12 17:54:11', '2017-08-04 15:21:50'),
(68, 'catalog/category/category_man_1.jpg', 59, 1, 1, 3, 1, '2017-07-12 17:52:42', '2017-08-04 15:21:10'),
(71, 'catalog/category/category_man_1.jpg', 60, 1, 1, 4, 1, '2017-07-12 17:54:57', '2017-08-04 16:44:24'),
(74, 'catalog/category/category_electronics_1.jpg', 60, 1, 1, 5, 1, '2017-07-12 17:57:06', '2017-08-04 16:44:39'),
(73, 'catalog/category/category_home_1.jpg', 60, 1, 1, 3, 1, '2017-07-12 17:56:36', '2017-08-04 16:43:56'),
(59, 'catalog/category/category_woman_1.jpg', 0, 1, 1, 1, 1, '2017-07-12 17:13:25', '2017-09-18 14:23:48'),
(60, 'catalog/category/category_man_1.jpg', 0, 1, 1, 2, 1, '2017-07-12 17:44:58', '2017-09-18 14:24:23'),
(61, 'catalog/category/category_kids_1.jpg', 0, 1, 1, 3, 1, '2017-07-12 17:46:34', '2017-09-18 14:25:16'),
(62, 'catalog/category/category_electronics_1.jpg', 0, 1, 1, 4, 1, '2017-07-12 17:48:21', '2017-09-18 14:26:08'),
(63, 'catalog/category/category_home_1.jpg', 0, 1, 1, 5, 1, '2017-07-12 17:49:57', '2017-08-04 16:52:21'),
(64, 'catalog/category/category_health_beauty_1.jpg', 59, 1, 1, 1, 1, '2017-07-12 17:51:02', '2017-08-04 15:18:54'),
(67, 'catalog/category/category_woman_1.jpg', 59, 1, 1, 2, 1, '2017-07-12 17:52:20', '2017-08-04 15:19:14'),
(69, 'catalog/category/category_kids_1.jpg', 59, 1, 1, 4, 1, '2017-07-12 17:53:19', '2017-08-04 15:21:26'),
(66, 'catalog/category/category_man_1.jpg', 62, 1, 1, 1, 1, '2017-07-12 17:51:56', '2017-08-04 16:50:44'),
(65, 'catalog/category/category_man_1.jpg', 60, 1, 1, 1, 1, '2017-07-12 17:51:27', '2017-08-04 16:43:13'),
(79, 'catalog/category/category_kids_1.jpg', 61, 1, 1, 4, 1, '2017-07-12 18:06:43', '2017-08-04 16:48:22'),
(78, 'catalog/category/category_woman_1.jpg', 61, 1, 1, 3, 1, '2017-07-12 18:06:22', '2017-08-04 16:47:44'),
(77, 'catalog/category/category_home_1.jpg', 61, 1, 1, 2, 1, '2017-07-12 18:06:02', '2017-08-04 16:47:18'),
(76, 'catalog/category/category_kids_1.jpg', 61, 1, 1, 1, 1, '2017-07-12 18:05:45', '2017-08-04 16:46:58'),
(75, 'catalog/category/category_health_beauty_1.jpg', 0, 1, 1, 6, 1, '2017-07-12 18:04:59', '2017-08-04 16:55:01'),
(80, 'catalog/category/category_home_1.jpg', 61, 1, 1, 5, 1, '2017-07-12 18:07:06', '2017-08-04 16:48:36'),
(81, 'catalog/category/category_electronics_1.jpg', 62, 1, 1, 2, 1, '2017-07-12 18:07:53', '2017-08-04 16:50:08'),
(82, 'catalog/category/category_man_1.jpg', 62, 1, 1, 3, 1, '2017-07-12 18:08:57', '2017-08-04 16:50:29'),
(83, 'catalog/category/category_electronics_1.jpg', 62, 1, 1, 4, 1, '2017-07-12 18:09:15', '2017-08-04 16:51:11'),
(84, 'catalog/category/category_man_1.jpg', 62, 1, 1, 5, 1, '2017-07-12 18:09:36', '2017-08-04 16:51:26'),
(85, 'catalog/category/category_home_1.jpg', 63, 1, 1, 1, 1, '2017-07-12 18:10:21', '2017-08-04 16:53:05'),
(86, 'catalog/category/category_electronics_1.jpg', 63, 1, 1, 2, 1, '2017-07-12 18:10:41', '2017-08-04 16:53:30'),
(87, 'catalog/category/category_home_1.jpg', 63, 1, 1, 3, 1, '2017-07-12 18:11:06', '2017-08-04 16:53:43'),
(88, 'catalog/category/category_electronics_1.jpg', 63, 1, 1, 4, 1, '2017-07-12 18:11:42', '2017-08-04 16:53:59'),
(89, 'catalog/category/category_home_1.jpg', 63, 1, 1, 5, 1, '2017-07-12 18:12:02', '2017-08-04 16:54:10'),
(90, 'catalog/category/category_woman_1.jpg', 75, 1, 1, 1, 1, '2017-07-12 18:12:56', '2017-08-04 16:55:26'),
(91, 'catalog/category/category_health_beauty_1.jpg', 75, 1, 1, 2, 1, '2017-07-12 18:13:40', '2017-08-04 16:55:40'),
(92, 'catalog/category/category_home_1.jpg', 75, 1, 1, 3, 1, '2017-07-12 18:14:04', '2017-08-04 16:56:54'),
(93, 'catalog/category/category_woman_1.jpg', 75, 1, 1, 4, 1, '2017-07-12 18:14:38', '2017-08-04 16:57:15'),
(94, 'catalog/category/category_health_beauty_1.jpg', 75, 1, 1, 5, 1, '2017-07-12 18:15:02', '2017-08-04 16:57:35'),
(95, 'catalog/category/category_electronics_1.jpg', 59, 1, 1, 6, 1, '2017-08-04 15:27:49', '2017-08-04 15:27:49'),
(96, 'catalog/category/category_home_1.jpg', 60, 1, 1, 6, 1, '2017-08-04 15:30:46', '2017-08-04 16:44:51'),
(97, 'catalog/category/category_electronics_1.jpg', 61, 1, 1, 6, 1, '2017-08-04 15:31:56', '2017-08-04 16:48:52'),
(98, 'catalog/category/category_electronics_1.jpg', 62, 1, 1, 6, 1, '2017-08-04 15:32:41', '2017-08-04 16:51:40'),
(99, 'catalog/category/category_electronics_1.jpg', 63, 1, 1, 6, 1, '2017-08-04 15:33:32', '2017-08-04 16:54:26'),
(100, 'catalog/category/category_home_1.jpg', 75, 1, 1, 6, 1, '2017-08-04 15:34:25', '2017-08-04 16:57:53'),
(103, '', 64, 1, 1, 1, 1, '2017-08-21 17:15:14', '2017-08-21 17:17:10'),
(104, '', 64, 1, 1, 2, 1, '2017-08-21 17:17:43', '2017-08-21 17:17:43'),
(105, '', 64, 1, 1, 3, 1, '2017-08-21 17:18:17', '2017-08-21 17:18:17'),
(106, '', 64, 1, 1, 4, 1, '2017-08-21 17:19:08', '2017-08-21 17:19:08'),
(107, '', 64, 1, 1, 5, 1, '2017-08-21 17:19:32', '2017-08-21 17:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(62, 2, 'Fresh Drinks', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Electronics', '', ''),
(61, 1, 'Fresh Food', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Kids', '', ''),
(59, 1, 'Fresh Vegetables', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Woman', '', 'woman, fashion'),
(60, 2, 'Fresh Fruits', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Man', '', ''),
(60, 1, 'Fresh Fruits', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Man', '', ''),
(61, 2, 'Fresh Food', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Kids', '', ''),
(95, 1, 'Watches', '', 'Watches', '', ''),
(62, 1, 'Fresh Drinks', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Electronics', '', ''),
(63, 1, 'Home &amp; Garden', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Home &amp; Garden', '', ''),
(63, 2, 'Home &amp; Garden', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Home &amp; Garden', '', ''),
(64, 2, 'Accessories', '', 'Accessories', '', ''),
(65, 2, 'Accessories', '', 'Accessories', '', ''),
(66, 1, 'Accessories', '', 'Accessories', '', ''),
(67, 2, 'Bags', '', 'Bags', '', ''),
(68, 2, 'Jeans', '', 'Jeans', '', ''),
(69, 2, 'Shoes', '', 'Shoes', '', ''),
(70, 2, 'Tops', '', 'Tops', '', ''),
(71, 2, 'Shoes', '', 'Shoes', '', ''),
(72, 2, 'Jackets', '', 'Jackets', '', ''),
(73, 2, 'Jeans', '', 'Jeans', '', ''),
(74, 2, 'T-Shirts', '', 'T-Shirts', '', ''),
(75, 1, 'Health &amp; Beauty', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Health &amp; Beauty', '', ''),
(75, 2, 'Health &amp; Beauty', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Health &amp; Beauty', '', ''),
(76, 2, 'Games', '', 'Games', '', ''),
(77, 2, 'Health &amp; Safety', '', 'Health &amp; Safety', '', ''),
(78, 2, 'Puzzles', '', 'Puzzles', '', ''),
(79, 1, 'Strollers', '', 'Strollers', '', ''),
(80, 2, 'Toys', '', 'Toys', '', ''),
(81, 2, 'Cameras', '', 'Cameras', '', ''),
(82, 2, 'Computers', '', 'Computers', '', ''),
(83, 2, 'Smartphones', '', 'Smartphones', '', ''),
(84, 2, 'TV &amp; Home Audio', '', 'TV &amp; Home Audio', '', ''),
(85, 2, 'Bedding', '', 'Bedding', '', ''),
(86, 2, 'Food', '', 'Food', '', ''),
(87, 2, 'Furniture', '', 'Furniture', '', ''),
(88, 2, 'Lighting', '', 'Lighting', '', ''),
(89, 2, 'Tools', '', 'Tools', '', ''),
(90, 2, 'Perfumes', '', 'Perfumes', '', ''),
(91, 2, 'Makeup', '', 'Makeup', '', ''),
(92, 1, 'Skin Care', '', 'Skin Care', '', ''),
(93, 1, 'Hair Care', '', 'Hair Care', '', ''),
(94, 2, 'Sun Care', '', 'Sun Care', '', ''),
(59, 2, 'Fresh Vegetables', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat aliquam. Mauris risus felis, adipiscing eu consequat. Aenean egestas congue est, nec convallis nunc viverra ac.\r\n&lt;/p&gt;', 'Woman', '', 'woman, fashion'),
(106, 1, 'Beauty', '', 'Beauty', '', ''),
(106, 2, 'Beauty', '', 'Beauty', '', ''),
(107, 1, 'Wedding', '', 'Wedding', '', ''),
(107, 2, 'Wedding', '', 'Wedding', '', ''),
(64, 1, 'Accessories', '', 'Accessories', '', ''),
(65, 1, 'Accessories', '', 'Accessories', '', ''),
(66, 2, 'Accessories', '', 'Accessories', '', ''),
(67, 1, 'Bags', '', 'Bags', '', ''),
(68, 1, 'Jeans', '', 'Jeans', '', ''),
(69, 1, 'Shoes', '', 'Shoes', '', ''),
(70, 1, 'Tops', '', 'Tops', '', ''),
(71, 1, 'Shoes', '', 'Shoes', '', ''),
(72, 1, 'Jackets', '', 'Jackets', '', ''),
(73, 1, 'Jeans', '', 'Jeans', '', ''),
(74, 1, 'T-Shirts', '', 'T-Shirts', '', ''),
(105, 2, 'Artwork', '', 'Artwork', '', ''),
(105, 1, 'Artwork', '', 'Artwork', '', ''),
(104, 2, 'Handmade', '', 'Handmade', '', ''),
(103, 1, 'Jewelry', '', 'Jewelry', '', ''),
(103, 2, 'Jewelry', '', 'Jewelry', '', ''),
(104, 1, 'Handmade', '', 'Handmade', '', ''),
(76, 1, 'Games', '', 'Games', '', ''),
(77, 1, 'Health &amp; Safety', '', 'Health &amp; Safety', '', ''),
(78, 1, 'Puzzles', '', 'Puzzles', '', ''),
(79, 2, 'Strollers', '', 'Strollers', '', ''),
(80, 1, 'Toys', '', 'Toys', '', ''),
(81, 1, 'Cameras', '', 'Cameras', '', ''),
(82, 1, 'Computers', '', 'Computers', '', ''),
(83, 1, 'Smartphones', '', 'Smartphones', '', ''),
(84, 1, 'TV &amp; Home Audio', '', 'TV &amp; Home Audio', '', ''),
(85, 1, 'Bedding', '', 'Bedding', '', ''),
(86, 1, 'Food', '', 'Food', '', ''),
(87, 1, 'Furniture', '', 'Furniture', '', ''),
(88, 1, 'Lighting', '', 'Lighting', '', ''),
(89, 1, 'Tools', '', 'Tools', '', ''),
(90, 1, 'Perfumes', '', 'Perfumes', '', ''),
(91, 1, 'Makeup', '', 'Makeup', '', ''),
(92, 2, 'Skin Care', '', 'Skin Care', '', ''),
(93, 2, 'Hair Care', '', 'Hair Care', '', ''),
(94, 1, 'Sun Care', '', 'Sun Care', '', ''),
(95, 2, 'Watches', '', 'Watches', '', ''),
(96, 2, 'Watches', '', 'Watches', '', ''),
(96, 1, 'Watches', '', 'Watches', '', ''),
(97, 2, 'Hobbies', '', 'Hobbies', '', ''),
(97, 1, 'Hobbies', '', 'Hobbies', '', ''),
(98, 2, 'Headphones', '', 'Headphones', '', ''),
(98, 1, 'Headphones', '', 'Headphones', '', ''),
(99, 2, 'Home Décor', '', 'Home Décor', '', ''),
(99, 1, 'Home Décor', '', 'Home Décor', '', ''),
(100, 2, 'Jewelry', '', 'Jewelry', '', ''),
(100, 1, 'Jewelry', '', 'Jewelry', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_filter`
--

CREATE TABLE `oc_category_filter` (
  `category_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_filter`
--

INSERT INTO `oc_category_filter` (`category_id`, `filter_id`) VALUES
(59, 1),
(59, 2),
(59, 3),
(59, 4),
(59, 5),
(59, 6),
(59, 7),
(59, 8),
(59, 9),
(59, 10),
(59, 11),
(59, 12),
(59, 13),
(59, 14),
(59, 15),
(60, 1),
(60, 2),
(60, 3),
(60, 4),
(60, 5),
(60, 6),
(60, 7),
(60, 8),
(60, 9),
(60, 10),
(60, 11),
(60, 12),
(60, 13),
(60, 14),
(60, 15),
(61, 1),
(61, 2),
(61, 3),
(61, 4),
(61, 5),
(61, 6),
(61, 7),
(61, 8),
(61, 9),
(61, 10),
(61, 11),
(61, 12),
(61, 13),
(61, 14),
(61, 15),
(62, 1),
(62, 2),
(62, 3),
(62, 4),
(62, 5),
(62, 6),
(62, 7),
(62, 8),
(62, 9),
(62, 10),
(62, 11),
(62, 12),
(62, 13),
(62, 14),
(62, 15),
(63, 1),
(63, 2),
(63, 3),
(63, 4),
(63, 5),
(63, 6),
(63, 7),
(63, 8),
(63, 9),
(63, 10),
(63, 11),
(63, 12),
(63, 13),
(63, 14),
(63, 15),
(75, 1),
(75, 2),
(75, 3),
(75, 4),
(75, 5),
(75, 6),
(75, 7),
(75, 8),
(75, 9),
(75, 10),
(75, 11),
(75, 12),
(75, 13),
(75, 14),
(75, 15);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(79, 79, 1),
(79, 61, 0),
(78, 78, 1),
(78, 61, 0),
(77, 77, 1),
(77, 61, 0),
(76, 76, 1),
(76, 61, 0),
(75, 75, 0),
(74, 74, 1),
(74, 60, 0),
(73, 73, 1),
(73, 60, 0),
(72, 72, 1),
(72, 60, 0),
(71, 71, 1),
(71, 60, 0),
(70, 70, 1),
(70, 59, 0),
(69, 69, 1),
(69, 59, 0),
(68, 68, 1),
(81, 81, 1),
(68, 59, 0),
(67, 67, 1),
(67, 59, 0),
(66, 66, 1),
(66, 62, 0),
(81, 62, 0),
(80, 61, 0),
(65, 65, 1),
(86, 86, 1),
(86, 63, 0),
(87, 87, 1),
(87, 63, 0),
(85, 85, 1),
(85, 63, 0),
(84, 84, 1),
(84, 62, 0),
(83, 83, 1),
(83, 62, 0),
(82, 82, 1),
(82, 62, 0),
(65, 60, 0),
(64, 64, 1),
(64, 59, 0),
(63, 63, 0),
(62, 62, 0),
(61, 61, 0),
(60, 60, 0),
(59, 59, 0),
(97, 61, 0),
(96, 96, 1),
(96, 60, 0),
(95, 95, 1),
(95, 59, 0),
(94, 94, 1),
(94, 75, 0),
(93, 93, 1),
(93, 75, 0),
(92, 92, 1),
(92, 75, 0),
(91, 91, 1),
(91, 75, 0),
(90, 90, 1),
(90, 75, 0),
(89, 89, 1),
(89, 63, 0),
(88, 88, 1),
(88, 63, 0),
(80, 80, 1),
(97, 97, 1),
(98, 62, 0),
(98, 98, 1),
(99, 63, 0),
(99, 99, 1),
(100, 75, 0),
(100, 100, 1),
(103, 64, 1),
(103, 59, 0),
(103, 103, 2),
(104, 59, 0),
(104, 64, 1),
(104, 104, 2),
(105, 59, 0),
(105, 64, 1),
(105, 105, 2),
(106, 59, 0),
(106, 64, 1),
(106, 106, 2),
(107, 59, 0),
(107, 64, 1),
(107, 107, 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_layout`
--

INSERT INTO `oc_category_to_layout` (`category_id`, `store_id`, `layout_id`) VALUES
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(66, 0, 0),
(67, 0, 0),
(68, 0, 0),
(69, 0, 0),
(70, 0, 0),
(71, 0, 0),
(72, 0, 0),
(73, 0, 0),
(74, 0, 0),
(75, 0, 0),
(76, 0, 0),
(77, 0, 0),
(78, 0, 0),
(79, 0, 0),
(80, 0, 0),
(81, 0, 0),
(82, 0, 0),
(83, 0, 0),
(84, 0, 0),
(85, 0, 0),
(86, 0, 0),
(87, 0, 0),
(88, 0, 0),
(89, 0, 0),
(90, 0, 0),
(91, 0, 0),
(92, 0, 0),
(93, 0, 0),
(94, 0, 0),
(95, 0, 0),
(96, 0, 0),
(97, 0, 0),
(98, 0, 0),
(99, 0, 0),
(100, 0, 0),
(103, 0, 0),
(104, 0, 0),
(105, 0, 0),
(106, 0, 0),
(107, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_store`
--

CREATE TABLE `oc_category_to_store` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_to_store`
--

INSERT INTO `oc_category_to_store` (`category_id`, `store_id`) VALUES
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0),
(90, 0),
(91, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 0),
(100, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_country`
--

CREATE TABLE `oc_country` (
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `address_format` text NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_country`
--

INSERT INTO `oc_country` (`country_id`, `name`, `iso_code_2`, `iso_code_3`, `address_format`, `postcode_required`, `status`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', '', 0, 1),
(2, 'Albania', 'AL', 'ALB', '', 0, 1),
(3, 'Algeria', 'DZ', 'DZA', '', 0, 1),
(4, 'American Samoa', 'AS', 'ASM', '', 0, 1),
(5, 'Andorra', 'AD', 'AND', '', 0, 1),
(6, 'Angola', 'AO', 'AGO', '', 0, 1),
(7, 'Anguilla', 'AI', 'AIA', '', 0, 1),
(8, 'Antarctica', 'AQ', 'ATA', '', 0, 1),
(9, 'Antigua and Barbuda', 'AG', 'ATG', '', 0, 1),
(10, 'Argentina', 'AR', 'ARG', '', 0, 1),
(11, 'Armenia', 'AM', 'ARM', '', 0, 1),
(12, 'Aruba', 'AW', 'ABW', '', 0, 1),
(13, 'Australia', 'AU', 'AUS', '', 0, 1),
(14, 'Austria', 'AT', 'AUT', '', 0, 1),
(15, 'Azerbaijan', 'AZ', 'AZE', '', 0, 1),
(16, 'Bahamas', 'BS', 'BHS', '', 0, 1),
(17, 'Bahrain', 'BH', 'BHR', '', 0, 1),
(18, 'Bangladesh', 'BD', 'BGD', '', 0, 1),
(19, 'Barbados', 'BB', 'BRB', '', 0, 1),
(20, 'Belarus', 'BY', 'BLR', '', 0, 1),
(21, 'Belgium', 'BE', 'BEL', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 0, 1),
(22, 'Belize', 'BZ', 'BLZ', '', 0, 1),
(23, 'Benin', 'BJ', 'BEN', '', 0, 1),
(24, 'Bermuda', 'BM', 'BMU', '', 0, 1),
(25, 'Bhutan', 'BT', 'BTN', '', 0, 1),
(26, 'Bolivia', 'BO', 'BOL', '', 0, 1),
(27, 'Bosnia and Herzegovina', 'BA', 'BIH', '', 0, 1),
(28, 'Botswana', 'BW', 'BWA', '', 0, 1),
(29, 'Bouvet Island', 'BV', 'BVT', '', 0, 1),
(30, 'Brazil', 'BR', 'BRA', '', 0, 1),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', '', 0, 1),
(32, 'Brunei Darussalam', 'BN', 'BRN', '', 0, 1),
(33, 'Bulgaria', 'BG', 'BGR', '', 0, 1),
(34, 'Burkina Faso', 'BF', 'BFA', '', 0, 1),
(35, 'Burundi', 'BI', 'BDI', '', 0, 1),
(36, 'Cambodia', 'KH', 'KHM', '', 0, 1),
(37, 'Cameroon', 'CM', 'CMR', '', 0, 1),
(38, 'Canada', 'CA', 'CAN', '', 0, 1),
(39, 'Cape Verde', 'CV', 'CPV', '', 0, 1),
(40, 'Cayman Islands', 'KY', 'CYM', '', 0, 1),
(41, 'Central African Republic', 'CF', 'CAF', '', 0, 1),
(42, 'Chad', 'TD', 'TCD', '', 0, 1),
(43, 'Chile', 'CL', 'CHL', '', 0, 1),
(44, 'China', 'CN', 'CHN', '', 0, 1),
(45, 'Christmas Island', 'CX', 'CXR', '', 0, 1),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', '', 0, 1),
(47, 'Colombia', 'CO', 'COL', '', 0, 1),
(48, 'Comoros', 'KM', 'COM', '', 0, 1),
(49, 'Congo', 'CG', 'COG', '', 0, 1),
(50, 'Cook Islands', 'CK', 'COK', '', 0, 1),
(51, 'Costa Rica', 'CR', 'CRI', '', 0, 1),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', '', 0, 1),
(53, 'Croatia', 'HR', 'HRV', '', 0, 1),
(54, 'Cuba', 'CU', 'CUB', '', 0, 1),
(55, 'Cyprus', 'CY', 'CYP', '', 0, 1),
(56, 'Czech Republic', 'CZ', 'CZE', '', 0, 1),
(57, 'Denmark', 'DK', 'DNK', '', 0, 1),
(58, 'Djibouti', 'DJ', 'DJI', '', 0, 1),
(59, 'Dominica', 'DM', 'DMA', '', 0, 1),
(60, 'Dominican Republic', 'DO', 'DOM', '', 0, 1),
(61, 'East Timor', 'TL', 'TLS', '', 0, 1),
(62, 'Ecuador', 'EC', 'ECU', '', 0, 1),
(63, 'Egypt', 'EG', 'EGY', '', 0, 1),
(64, 'El Salvador', 'SV', 'SLV', '', 0, 1),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', '', 0, 1),
(66, 'Eritrea', 'ER', 'ERI', '', 0, 1),
(67, 'Estonia', 'EE', 'EST', '', 0, 1),
(68, 'Ethiopia', 'ET', 'ETH', '', 0, 1),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', '', 0, 1),
(70, 'Faroe Islands', 'FO', 'FRO', '', 0, 1),
(71, 'Fiji', 'FJ', 'FJI', '', 0, 1),
(72, 'Finland', 'FI', 'FIN', '', 0, 1),
(74, 'France, Metropolitan', 'FR', 'FRA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(75, 'French Guiana', 'GF', 'GUF', '', 0, 1),
(76, 'French Polynesia', 'PF', 'PYF', '', 0, 1),
(77, 'French Southern Territories', 'TF', 'ATF', '', 0, 1),
(78, 'Gabon', 'GA', 'GAB', '', 0, 1),
(79, 'Gambia', 'GM', 'GMB', '', 0, 1),
(80, 'Georgia', 'GE', 'GEO', '', 0, 1),
(81, 'Germany', 'DE', 'DEU', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(82, 'Ghana', 'GH', 'GHA', '', 0, 1),
(83, 'Gibraltar', 'GI', 'GIB', '', 0, 1),
(84, 'Greece', 'GR', 'GRC', '', 0, 1),
(85, 'Greenland', 'GL', 'GRL', '', 0, 1),
(86, 'Grenada', 'GD', 'GRD', '', 0, 1),
(87, 'Guadeloupe', 'GP', 'GLP', '', 0, 1),
(88, 'Guam', 'GU', 'GUM', '', 0, 1),
(89, 'Guatemala', 'GT', 'GTM', '', 0, 1),
(90, 'Guinea', 'GN', 'GIN', '', 0, 1),
(91, 'Guinea-Bissau', 'GW', 'GNB', '', 0, 1),
(92, 'Guyana', 'GY', 'GUY', '', 0, 1),
(93, 'Haiti', 'HT', 'HTI', '', 0, 1),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', '', 0, 1),
(95, 'Honduras', 'HN', 'HND', '', 0, 1),
(96, 'Hong Kong', 'HK', 'HKG', '', 0, 1),
(97, 'Hungary', 'HU', 'HUN', '', 0, 1),
(98, 'Iceland', 'IS', 'ISL', '', 0, 1),
(99, 'India', 'IN', 'IND', '', 0, 1),
(100, 'Indonesia', 'ID', 'IDN', '', 0, 1),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', '', 0, 1),
(102, 'Iraq', 'IQ', 'IRQ', '', 0, 1),
(103, 'Ireland', 'IE', 'IRL', '', 0, 1),
(104, 'Israel', 'IL', 'ISR', '', 0, 1),
(105, 'Italy', 'IT', 'ITA', '', 0, 1),
(106, 'Jamaica', 'JM', 'JAM', '', 0, 1),
(107, 'Japan', 'JP', 'JPN', '', 0, 1),
(108, 'Jordan', 'JO', 'JOR', '', 0, 1),
(109, 'Kazakhstan', 'KZ', 'KAZ', '', 0, 1),
(110, 'Kenya', 'KE', 'KEN', '', 0, 1),
(111, 'Kiribati', 'KI', 'KIR', '', 0, 1),
(112, 'North Korea', 'KP', 'PRK', '', 0, 1),
(113, 'South Korea', 'KR', 'KOR', '', 0, 1),
(114, 'Kuwait', 'KW', 'KWT', '', 0, 1),
(115, 'Kyrgyzstan', 'KG', 'KGZ', '', 0, 1),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', '', 0, 1),
(117, 'Latvia', 'LV', 'LVA', '', 0, 1),
(118, 'Lebanon', 'LB', 'LBN', '', 0, 1),
(119, 'Lesotho', 'LS', 'LSO', '', 0, 1),
(120, 'Liberia', 'LR', 'LBR', '', 0, 1),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', '', 0, 1),
(122, 'Liechtenstein', 'LI', 'LIE', '', 0, 1),
(123, 'Lithuania', 'LT', 'LTU', '', 0, 1),
(124, 'Luxembourg', 'LU', 'LUX', '', 0, 1),
(125, 'Macau', 'MO', 'MAC', '', 0, 1),
(126, 'FYROM', 'MK', 'MKD', '', 0, 1),
(127, 'Madagascar', 'MG', 'MDG', '', 0, 1),
(128, 'Malawi', 'MW', 'MWI', '', 0, 1),
(129, 'Malaysia', 'MY', 'MYS', '', 0, 1),
(130, 'Maldives', 'MV', 'MDV', '', 0, 1),
(131, 'Mali', 'ML', 'MLI', '', 0, 1),
(132, 'Malta', 'MT', 'MLT', '', 0, 1),
(133, 'Marshall Islands', 'MH', 'MHL', '', 0, 1),
(134, 'Martinique', 'MQ', 'MTQ', '', 0, 1),
(135, 'Mauritania', 'MR', 'MRT', '', 0, 1),
(136, 'Mauritius', 'MU', 'MUS', '', 0, 1),
(137, 'Mayotte', 'YT', 'MYT', '', 0, 1),
(138, 'Mexico', 'MX', 'MEX', '', 0, 1),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', '', 0, 1),
(140, 'Moldova, Republic of', 'MD', 'MDA', '', 0, 1),
(141, 'Monaco', 'MC', 'MCO', '', 0, 1),
(142, 'Mongolia', 'MN', 'MNG', '', 0, 1),
(143, 'Montserrat', 'MS', 'MSR', '', 0, 1),
(144, 'Morocco', 'MA', 'MAR', '', 0, 1),
(145, 'Mozambique', 'MZ', 'MOZ', '', 0, 1),
(146, 'Myanmar', 'MM', 'MMR', '', 0, 1),
(147, 'Namibia', 'NA', 'NAM', '', 0, 1),
(148, 'Nauru', 'NR', 'NRU', '', 0, 1),
(149, 'Nepal', 'NP', 'NPL', '', 0, 1),
(150, 'Netherlands', 'NL', 'NLD', '', 0, 1),
(151, 'Netherlands Antilles', 'AN', 'ANT', '', 0, 1),
(152, 'New Caledonia', 'NC', 'NCL', '', 0, 1),
(153, 'New Zealand', 'NZ', 'NZL', '', 0, 1),
(154, 'Nicaragua', 'NI', 'NIC', '', 0, 1),
(155, 'Niger', 'NE', 'NER', '', 0, 1),
(156, 'Nigeria', 'NG', 'NGA', '', 0, 1),
(157, 'Niue', 'NU', 'NIU', '', 0, 1),
(158, 'Norfolk Island', 'NF', 'NFK', '', 0, 1),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 0, 1),
(160, 'Norway', 'NO', 'NOR', '', 0, 1),
(161, 'Oman', 'OM', 'OMN', '', 0, 1),
(162, 'Pakistan', 'PK', 'PAK', '', 0, 1),
(163, 'Palau', 'PW', 'PLW', '', 0, 1),
(164, 'Panama', 'PA', 'PAN', '', 0, 1),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 0, 1),
(166, 'Paraguay', 'PY', 'PRY', '', 0, 1),
(167, 'Peru', 'PE', 'PER', '', 0, 1),
(168, 'Philippines', 'PH', 'PHL', '', 0, 1),
(169, 'Pitcairn', 'PN', 'PCN', '', 0, 1),
(170, 'Poland', 'PL', 'POL', '', 0, 1),
(171, 'Portugal', 'PT', 'PRT', '', 0, 1),
(172, 'Puerto Rico', 'PR', 'PRI', '', 0, 1),
(173, 'Qatar', 'QA', 'QAT', '', 0, 1),
(174, 'Reunion', 'RE', 'REU', '', 0, 1),
(175, 'Romania', 'RO', 'ROM', '', 0, 1),
(176, 'Russian Federation', 'RU', 'RUS', '', 0, 1),
(177, 'Rwanda', 'RW', 'RWA', '', 0, 1),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 0, 1),
(179, 'Saint Lucia', 'LC', 'LCA', '', 0, 1),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 0, 1),
(181, 'Samoa', 'WS', 'WSM', '', 0, 1),
(182, 'San Marino', 'SM', 'SMR', '', 0, 1),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 0, 1),
(184, 'Saudi Arabia', 'SA', 'SAU', '', 0, 1),
(185, 'Senegal', 'SN', 'SEN', '', 0, 1),
(186, 'Seychelles', 'SC', 'SYC', '', 0, 1),
(187, 'Sierra Leone', 'SL', 'SLE', '', 0, 1),
(188, 'Singapore', 'SG', 'SGP', '', 0, 1),
(189, 'Slovak Republic', 'SK', 'SVK', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city} {postcode}\r\n{zone}\r\n{country}', 0, 1),
(190, 'Slovenia', 'SI', 'SVN', '', 0, 1),
(191, 'Solomon Islands', 'SB', 'SLB', '', 0, 1),
(192, 'Somalia', 'SO', 'SOM', '', 0, 1),
(193, 'South Africa', 'ZA', 'ZAF', '', 0, 1),
(194, 'South Georgia &amp; South Sandwich Islands', 'GS', 'SGS', '', 0, 1),
(195, 'Spain', 'ES', 'ESP', '', 0, 1),
(196, 'Sri Lanka', 'LK', 'LKA', '', 0, 1),
(197, 'St. Helena', 'SH', 'SHN', '', 0, 1),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 0, 1),
(199, 'Sudan', 'SD', 'SDN', '', 0, 1),
(200, 'Suriname', 'SR', 'SUR', '', 0, 1),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 0, 1),
(202, 'Swaziland', 'SZ', 'SWZ', '', 0, 1),
(203, 'Sweden', 'SE', 'SWE', '{company}\r\n{firstname} {lastname}\r\n{address_1}\r\n{address_2}\r\n{postcode} {city}\r\n{country}', 1, 1),
(204, 'Switzerland', 'CH', 'CHE', '', 0, 1),
(205, 'Syrian Arab Republic', 'SY', 'SYR', '', 0, 1),
(206, 'Taiwan', 'TW', 'TWN', '', 0, 1),
(207, 'Tajikistan', 'TJ', 'TJK', '', 0, 1),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', '', 0, 1),
(209, 'Thailand', 'TH', 'THA', '', 0, 1),
(210, 'Togo', 'TG', 'TGO', '', 0, 1),
(211, 'Tokelau', 'TK', 'TKL', '', 0, 1),
(212, 'Tonga', 'TO', 'TON', '', 0, 1),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 0, 1),
(214, 'Tunisia', 'TN', 'TUN', '', 0, 1),
(215, 'Turkey', 'TR', 'TUR', '', 0, 1),
(216, 'Turkmenistan', 'TM', 'TKM', '', 0, 1),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 0, 1),
(218, 'Tuvalu', 'TV', 'TUV', '', 0, 1),
(219, 'Uganda', 'UG', 'UGA', '', 0, 1),
(220, 'Ukraine', 'UA', 'UKR', '', 0, 1),
(221, 'United Arab Emirates', 'AE', 'ARE', '', 0, 1),
(222, 'United Kingdom', 'GB', 'GBR', '', 1, 1),
(223, 'United States', 'US', 'USA', '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', 0, 1),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 0, 1),
(225, 'Uruguay', 'UY', 'URY', '', 0, 1),
(226, 'Uzbekistan', 'UZ', 'UZB', '', 0, 1),
(227, 'Vanuatu', 'VU', 'VUT', '', 0, 1),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 0, 1),
(229, 'Venezuela', 'VE', 'VEN', '', 0, 1),
(230, 'Viet Nam', 'VN', 'VNM', '', 0, 1),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 0, 1),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 0, 1),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 0, 1),
(234, 'Western Sahara', 'EH', 'ESH', '', 0, 1),
(235, 'Yemen', 'YE', 'YEM', '', 0, 1),
(237, 'Democratic Republic of Congo', 'CD', 'COD', '', 0, 1),
(238, 'Zambia', 'ZM', 'ZMB', '', 0, 1),
(239, 'Zimbabwe', 'ZW', 'ZWE', '', 0, 1),
(242, 'Montenegro', 'ME', 'MNE', '', 0, 1),
(243, 'Serbia', 'RS', 'SRB', '', 0, 1),
(244, 'Aaland Islands', 'AX', 'ALA', '', 0, 1),
(245, 'Bonaire, Sint Eustatius and Saba', 'BQ', 'BES', '', 0, 1),
(246, 'Curacao', 'CW', 'CUW', '', 0, 1),
(247, 'Palestinian Territory, Occupied', 'PS', 'PSE', '', 0, 1),
(248, 'South Sudan', 'SS', 'SSD', '', 0, 1),
(249, 'St. Barthelemy', 'BL', 'BLM', '', 0, 1),
(250, 'St. Martin (French part)', 'MF', 'MAF', '', 0, 1),
(251, 'Canary Islands', 'IC', 'ICA', '', 0, 1),
(252, 'Ascension Island (British)', 'AC', 'ASC', '', 0, 1),
(253, 'Kosovo, Republic of', 'XK', 'UNK', '', 0, 1),
(254, 'Isle of Man', 'IM', 'IMN', '', 0, 1),
(255, 'Tristan da Cunha', 'TA', 'SHN', '', 0, 1),
(256, 'Guernsey', 'GG', 'GGY', '', 0, 1),
(257, 'Jersey', 'JE', 'JEY', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon`
--

CREATE TABLE `oc_coupon` (
  `coupon_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(20) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) NOT NULL,
  `logged` tinyint(1) NOT NULL,
  `shipping` tinyint(1) NOT NULL,
  `total` decimal(15,4) NOT NULL,
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00',
  `uses_total` int(11) NOT NULL,
  `uses_customer` varchar(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_coupon`
--

INSERT INTO `oc_coupon` (`coupon_id`, `name`, `code`, `type`, `discount`, `logged`, `shipping`, `total`, `date_start`, `date_end`, `uses_total`, `uses_customer`, `status`, `date_added`) VALUES
(4, '-10% Discount', '2222', 'P', '10.0000', 0, 0, '0.0000', '2014-01-01', '2020-01-01', 10, '10', 0, '2009-01-27 13:55:03'),
(5, 'Free Shipping', '3333', 'P', '0.0000', 0, 1, '100.0000', '2014-01-01', '2014-02-01', 10, '10', 0, '2009-03-14 21:13:53'),
(6, '-10.00 Discount', '1111', 'F', '10.0000', 0, 0, '10.0000', '2014-01-01', '2020-01-01', 100000, '10000', 0, '2009-03-14 21:15:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_category`
--

CREATE TABLE `oc_coupon_category` (
  `coupon_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_history`
--

CREATE TABLE `oc_coupon_history` (
  `coupon_history_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_coupon_product`
--

CREATE TABLE `oc_coupon_product` (
  `coupon_product_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_currency`
--

CREATE TABLE `oc_currency` (
  `currency_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `code` varchar(3) NOT NULL,
  `symbol_left` varchar(12) NOT NULL,
  `symbol_right` varchar(12) NOT NULL,
  `decimal_place` char(1) NOT NULL,
  `value` double(15,8) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_currency`
--

INSERT INTO `oc_currency` (`currency_id`, `title`, `code`, `symbol_left`, `symbol_right`, `decimal_place`, `value`, `status`, `date_modified`) VALUES
(4, 'VND', 'VND', '', 'VND', '', 1.00000000, 1, '2018-03-09 13:25:27'),
(2, 'USD', 'USD', '$', '', '', 1.00000000, 1, '2018-03-08 14:57:12');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer`
--

CREATE TABLE `oc_customer` (
  `customer_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `language_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `cart` text,
  `wishlist` text,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `address_id` int(11) NOT NULL DEFAULT '0',
  `custom_field` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `safe` tinyint(1) NOT NULL,
  `token` text NOT NULL,
  `code` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer`
--

INSERT INTO `oc_customer` (`customer_id`, `customer_group_id`, `store_id`, `language_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `password`, `salt`, `cart`, `wishlist`, `newsletter`, `address_id`, `custom_field`, `ip`, `status`, `safe`, `token`, `code`, `date_added`) VALUES
(1, 1, 0, 1, 'John', 'Rodman', 'fasdfasfasf@sfaisdgfuiysavgf.com', '41234323414', '', '7c6c13dc2f744c9d4db021a13b9da0e7a8b28c1e', 'zbYq7xNNl', '', '', 1, 1, '[]', '127.0.0.1', 1, 1, '', '', '2017-08-02 11:52:43');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_activity`
--

CREATE TABLE `oc_customer_activity` (
  `customer_activity_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `key` varchar(64) NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_affiliate`
--

CREATE TABLE `oc_customer_affiliate` (
  `customer_id` int(11) NOT NULL,
  `company` varchar(40) NOT NULL,
  `website` varchar(255) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `commission` decimal(4,2) NOT NULL DEFAULT '0.00',
  `tax` varchar(64) NOT NULL,
  `payment` varchar(6) NOT NULL,
  `cheque` varchar(100) NOT NULL,
  `paypal` varchar(64) NOT NULL,
  `bank_name` varchar(64) NOT NULL,
  `bank_branch_number` varchar(64) NOT NULL,
  `bank_swift_code` varchar(64) NOT NULL,
  `bank_account_name` varchar(64) NOT NULL,
  `bank_account_number` varchar(64) NOT NULL,
  `custom_field` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_approval`
--

CREATE TABLE `oc_customer_approval` (
  `customer_approval_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `type` varchar(9) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group`
--

CREATE TABLE `oc_customer_group` (
  `customer_group_id` int(11) NOT NULL,
  `approval` int(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group`
--

INSERT INTO `oc_customer_group` (`customer_group_id`, `approval`, `sort_order`) VALUES
(1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_group_description`
--

CREATE TABLE `oc_customer_group_description` (
  `customer_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_group_description`
--

INSERT INTO `oc_customer_group_description` (`customer_group_id`, `language_id`, `name`, `description`) VALUES
(1, 1, 'Default', 'test'),
(1, 2, 'Default', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_history`
--

CREATE TABLE `oc_customer_history` (
  `customer_history_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_ip`
--

CREATE TABLE `oc_customer_ip` (
  `customer_ip_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_login`
--

CREATE TABLE `oc_customer_login` (
  `customer_login_id` int(11) NOT NULL,
  `email` varchar(96) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `total` int(4) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_customer_login`
--

INSERT INTO `oc_customer_login` (`customer_login_id`, `email`, `ip`, `total`, `date_added`, `date_modified`) VALUES
(1, 'admin', '127.0.0.1', 1, '2017-08-04 13:57:30', '2017-08-04 13:57:30');

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_online`
--

CREATE TABLE `oc_customer_online` (
  `ip` varchar(40) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `referer` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_reward`
--

CREATE TABLE `oc_customer_reward` (
  `customer_reward_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `points` int(8) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_search`
--

CREATE TABLE `oc_customer_search` (
  `customer_search_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `keyword` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category` tinyint(1) NOT NULL,
  `description` tinyint(1) NOT NULL,
  `products` int(11) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_transaction`
--

CREATE TABLE `oc_customer_transaction` (
  `customer_transaction_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_customer_wishlist`
--

CREATE TABLE `oc_customer_wishlist` (
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field`
--

CREATE TABLE `oc_custom_field` (
  `custom_field_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `validation` varchar(255) NOT NULL,
  `location` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_customer_group`
--

CREATE TABLE `oc_custom_field_customer_group` (
  `custom_field_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_description`
--

CREATE TABLE `oc_custom_field_description` (
  `custom_field_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value`
--

CREATE TABLE `oc_custom_field_value` (
  `custom_field_value_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_custom_field_value_description`
--

CREATE TABLE `oc_custom_field_value_description` (
  `custom_field_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `custom_field_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download`
--

CREATE TABLE `oc_download` (
  `download_id` int(11) NOT NULL,
  `filename` varchar(160) NOT NULL,
  `mask` varchar(128) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_download_description`
--

CREATE TABLE `oc_download_description` (
  `download_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_event`
--

CREATE TABLE `oc_event` (
  `event_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `trigger` text NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_event`
--

INSERT INTO `oc_event` (`event_id`, `code`, `trigger`, `action`, `status`, `sort_order`) VALUES
(1, 'activity_customer_add', 'catalog/model/account/customer/addCustomer/after', 'event/activity/addCustomer', 1, 0),
(2, 'activity_customer_edit', 'catalog/model/account/customer/editCustomer/after', 'event/activity/editCustomer', 1, 0),
(3, 'activity_customer_password', 'catalog/model/account/customer/editPassword/after', 'event/activity/editPassword', 1, 0),
(4, 'activity_customer_forgotten', 'catalog/model/account/customer/editCode/after', 'event/activity/forgotten', 1, 0),
(5, 'activity_transaction', 'catalog/model/account/customer/addTransaction/after', 'event/activity/addTransaction', 1, 0),
(6, 'activity_customer_login', 'catalog/model/account/customer/deleteLoginAttempts/after', 'event/activity/login', 1, 0),
(7, 'activity_address_add', 'catalog/model/account/address/addAddress/after', 'event/activity/addAddress', 1, 0),
(8, 'activity_address_edit', 'catalog/model/account/address/editAddress/after', 'event/activity/editAddress', 1, 0),
(9, 'activity_address_delete', 'catalog/model/account/address/deleteAddress/after', 'event/activity/deleteAddress', 1, 0),
(10, 'activity_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'event/activity/addAffiliate', 1, 0),
(11, 'activity_affiliate_edit', 'catalog/model/account/customer/editAffiliate/after', 'event/activity/editAffiliate', 1, 0),
(12, 'activity_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'event/activity/addOrderHistory', 1, 0),
(13, 'activity_return_add', 'catalog/model/account/return/addReturn/after', 'event/activity/addReturn', 1, 0),
(14, 'mail_transaction', 'catalog/model/account/customer/addTransaction/after', 'mail/transaction', 1, 0),
(15, 'mail_forgotten', 'catalog/model/account/customer/editCode/after', 'mail/forgotten', 1, 0),
(16, 'mail_customer_add', 'catalog/model/account/customer/addCustomer/after', 'mail/register', 1, 0),
(17, 'mail_customer_alert', 'catalog/model/account/customer/addCustomer/after', 'mail/register/alert', 1, 0),
(18, 'mail_affiliate_add', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate', 1, 0),
(19, 'mail_affiliate_alert', 'catalog/model/account/customer/addAffiliate/after', 'mail/affiliate/alert', 1, 0),
(20, 'mail_voucher', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/total/voucher/send', 1, 0),
(21, 'mail_order_add', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order', 1, 0),
(22, 'mail_order_alert', 'catalog/model/checkout/order/addOrderHistory/before', 'mail/order/alert', 1, 0),
(23, 'statistics_review_add', 'catalog/model/catalog/review/addReview/after', 'event/statistics/addReview', 1, 0),
(24, 'statistics_return_add', 'catalog/model/account/return/addReturn/after', 'event/statistics/addReturn', 1, 0),
(25, 'statistics_order_history', 'catalog/model/checkout/order/addOrderHistory/after', 'event/statistics/addOrderHistory', 1, 0),
(26, 'admin_mail_affiliate_approve', 'admin/model/customer/customer_approval/approveAffiliate/after', 'mail/affiliate/approve', 1, 0),
(27, 'admin_mail_affiliate_deny', 'admin/model/customer/customer_approval/denyAffiliate/after', 'mail/affiliate/deny', 1, 0),
(28, 'admin_mail_customer_approve', 'admin/model/customer/customer_approval/approveCustomer/after', 'mail/customer/approve', 1, 0),
(29, 'admin_mail_customer_deny', 'admin/model/customer/customer_approval/denyCustomer/after', 'mail/customer/deny', 1, 0),
(30, 'admin_mail_reward', 'admin/model/customer/customer/addReward/after', 'mail/reward', 1, 0),
(31, 'admin_mail_transaction', 'admin/model/customer/customer/addTransaction/after', 'mail/transaction', 1, 0),
(32, 'admin_mail_return', 'admin/model/sale/return/addReturn/after', 'mail/return', 1, 0),
(33, 'admin_mail_forgotten', 'admin/model/user/user/editCode/after', 'mail/forgotten', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension`
--

CREATE TABLE `oc_extension` (
  `extension_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `code` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_extension`
--

INSERT INTO `oc_extension` (`extension_id`, `type`, `code`) VALUES
(1, 'payment', 'cod'),
(2, 'total', 'shipping'),
(3, 'total', 'sub_total'),
(4, 'total', 'tax'),
(5, 'total', 'total'),
(6, 'module', 'banner'),
(7, 'module', 'carousel'),
(8, 'total', 'credit'),
(9, 'shipping', 'flat'),
(10, 'total', 'handling'),
(11, 'total', 'low_order_fee'),
(12, 'total', 'coupon'),
(13, 'module', 'category'),
(14, 'module', 'account'),
(15, 'total', 'reward'),
(16, 'total', 'voucher'),
(17, 'payment', 'free_checkout'),
(18, 'module', 'featured'),
(19, 'module', 'slideshow'),
(20, 'theme', 'default'),
(21, 'dashboard', 'activity'),
(22, 'dashboard', 'sale'),
(23, 'dashboard', 'recent'),
(24, 'dashboard', 'order'),
(25, 'dashboard', 'online'),
(26, 'dashboard', 'map'),
(27, 'dashboard', 'customer'),
(28, 'dashboard', 'chart'),
(29, 'report', 'sale_coupon'),
(31, 'report', 'customer_search'),
(32, 'report', 'customer_transaction'),
(33, 'report', 'product_purchased'),
(34, 'report', 'product_viewed'),
(35, 'report', 'sale_return'),
(36, 'report', 'sale_order'),
(37, 'report', 'sale_shipping'),
(38, 'report', 'sale_tax'),
(39, 'report', 'customer_activity'),
(40, 'report', 'customer_order'),
(41, 'report', 'customer_reward'),
(42, 'theme', 'oxy'),
(46, 'module', 'oxy_theme_options'),
(45, 'module', 'oxy_theme_design'),
(47, 'module', 'theme_product_tabs'),
(48, 'module', 'theme_banner_pro'),
(49, 'module', 'html'),
(50, 'module', 'filter'),
(51, 'module', 'special'),
(52, 'module', 'theme_features_blocks'),
(53, 'module', 'theme_faq'),
(54, 'module', 'information'),
(55, 'module', 'theme_highly_recommended'),
(56, 'module', 'theme_testimonial'),
(57, 'module', 'theme_category_slider'),
(58, 'module', 'theme_brand_slider'),
(59, 'module', 'theme_custom_products'),
(60, 'module', 'theme_gallery'),
(61, 'module', 'theme_lookbook'),
(62, 'module', 'theme_store_tv'),
(63, 'module', 'theme_featured'),
(64, 'module', 'theme_slideshow'),
(65, 'module', 'theme_banner'),
(66, 'module', 'theme_most_viewed'),
(67, 'module', 'bestseller'),
(68, 'module', 'theme_product_slider');

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_install`
--

CREATE TABLE `oc_extension_install` (
  `extension_install_id` int(11) NOT NULL,
  `extension_download_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_extension_path`
--

CREATE TABLE `oc_extension_path` (
  `extension_path_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter`
--

CREATE TABLE `oc_filter` (
  `filter_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter`
--

INSERT INTO `oc_filter` (`filter_id`, `filter_group_id`, `sort_order`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 2, 1),
(7, 2, 2),
(8, 2, 3),
(9, 2, 4),
(10, 2, 5),
(11, 3, 1),
(12, 3, 2),
(13, 3, 3),
(14, 3, 4),
(15, 3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_description`
--

CREATE TABLE `oc_filter_description` (
  `filter_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `filter_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_description`
--

INSERT INTO `oc_filter_description` (`filter_id`, `language_id`, `filter_group_id`, `name`) VALUES
(1, 1, 1, 'White'),
(2, 1, 1, 'Black'),
(3, 1, 1, 'Red'),
(4, 1, 1, 'Orange'),
(5, 1, 1, 'Blue'),
(6, 1, 2, '$0 - $20'),
(7, 1, 2, '$21 - $50'),
(8, 1, 2, '$51 - $100'),
(9, 1, 2, '$101 - $200'),
(10, 1, 2, 'More than $200'),
(11, 1, 3, 'S'),
(12, 1, 3, 'M'),
(13, 1, 3, 'L'),
(14, 1, 3, 'XL'),
(15, 1, 3, 'XXL'),
(1, 2, 1, 'White'),
(2, 2, 1, 'Black'),
(3, 2, 1, 'Red'),
(4, 2, 1, 'Orange'),
(5, 2, 1, 'Blue'),
(6, 2, 2, '$0 - $20'),
(7, 2, 2, '$21 - $50'),
(8, 2, 2, '$51 - $100'),
(9, 2, 2, '$101 - $200'),
(10, 2, 2, 'More than $200'),
(11, 2, 3, 'S'),
(12, 2, 3, 'M'),
(13, 2, 3, 'L'),
(14, 2, 3, 'XL'),
(15, 2, 3, 'XXL');

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group`
--

CREATE TABLE `oc_filter_group` (
  `filter_group_id` int(11) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group`
--

INSERT INTO `oc_filter_group` (`filter_group_id`, `sort_order`) VALUES
(1, 2),
(2, 1),
(3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `oc_filter_group_description`
--

CREATE TABLE `oc_filter_group_description` (
  `filter_group_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_filter_group_description`
--

INSERT INTO `oc_filter_group_description` (`filter_group_id`, `language_id`, `name`) VALUES
(1, 1, 'Shop by Color'),
(2, 1, 'Shop by Price'),
(3, 1, 'Shop by Size'),
(1, 2, 'Shop by Color'),
(2, 2, 'Shop by Price'),
(3, 2, 'Shop by Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_geo_zone`
--

CREATE TABLE `oc_geo_zone` (
  `geo_zone_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_geo_zone`
--

INSERT INTO `oc_geo_zone` (`geo_zone_id`, `name`, `description`, `date_added`, `date_modified`) VALUES
(3, 'UK VAT Zone', 'UK VAT', '2009-01-06 23:26:25', '2010-02-26 22:33:24'),
(4, 'UK Shipping', 'UK Shipping Zones', '2009-06-23 01:14:53', '2010-12-15 15:18:13');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information`
--

CREATE TABLE `oc_information` (
  `information_id` int(11) NOT NULL,
  `bottom` int(1) NOT NULL DEFAULT '0',
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information`
--

INSERT INTO `oc_information` (`information_id`, `bottom`, `sort_order`, `status`) VALUES
(3, 1, 3, 1),
(4, 1, 1, 1),
(5, 1, 4, 1),
(6, 1, 2, 1),
(7, 1, 5, 1),
(8, 1, 6, 1),
(9, 1, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_description`
--

CREATE TABLE `oc_information_description` (
  `information_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` mediumtext NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_description`
--

INSERT INTO `oc_information_description` (`information_id`, `language_id`, `title`, `description`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(4, 1, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n', 'About Us', '', ''),
(5, 1, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 1, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(6, 1, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', 'Delivery Information', '', ''),
(7, 1, 'Payment Security', 'Payment Security', 'Payment Security', '', ''),
(8, 1, 'What\'s new', '&lt;p&gt;What\'s new&lt;br&gt;&lt;/p&gt;', 'What\'s new', '', ''),
(9, 1, 'FAQ', '&lt;p&gt;FAQ&lt;br&gt;&lt;/p&gt;', 'FAQ', '', ''),
(4, 2, 'About Us', '&lt;p&gt;\r\n	About Us&lt;/p&gt;\r\n', 'About Us', '', ''),
(5, 2, 'Terms &amp; Conditions', '&lt;p&gt;\r\n	Terms &amp;amp; Conditions&lt;/p&gt;\r\n', 'Terms &amp; Conditions', '', ''),
(3, 2, 'Privacy Policy', '&lt;p&gt;\r\n	Privacy Policy&lt;/p&gt;\r\n', 'Privacy Policy', '', ''),
(6, 2, 'Delivery Information', '&lt;p&gt;\r\n	Delivery Information&lt;/p&gt;\r\n', 'Delivery Information', '', ''),
(7, 2, 'Payment Security', 'Payment Security', 'Payment Security', '', ''),
(8, 2, 'What\'s new', '&lt;p&gt;What\'s new&lt;br&gt;&lt;/p&gt;', 'What\'s new', '', ''),
(9, 2, 'FAQ', '&lt;p&gt;FAQ&lt;br&gt;&lt;/p&gt;', 'FAQ', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_layout`
--

CREATE TABLE `oc_information_to_layout` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_layout`
--

INSERT INTO `oc_information_to_layout` (`information_id`, `store_id`, `layout_id`) VALUES
(7, 0, 0),
(8, 0, 0),
(9, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_information_to_store`
--

CREATE TABLE `oc_information_to_store` (
  `information_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_information_to_store`
--

INSERT INTO `oc_information_to_store` (`information_id`, `store_id`) VALUES
(3, 0),
(4, 0),
(5, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_language`
--

CREATE TABLE `oc_language` (
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `code` varchar(5) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `image` varchar(64) NOT NULL,
  `directory` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_language`
--

INSERT INTO `oc_language` (`language_id`, `name`, `code`, `locale`, `image`, `directory`, `sort_order`, `status`) VALUES
(1, 'English', 'en-gb', 'en-US,en_US.UTF-8,en_US,en-gb,english', 'gb.png', 'english', 1, 1),
(2, 'Tiếng Việt', 'vi-vn', 'vi-vn.UTF-8,vi-vn,vi-vn,vietnamese', '', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout`
--

CREATE TABLE `oc_layout` (
  `layout_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout`
--

INSERT INTO `oc_layout` (`layout_id`, `name`) VALUES
(1, 'Home'),
(2, 'Product'),
(3, 'Category'),
(4, 'Default'),
(5, 'Manufacturer'),
(6, 'Account'),
(7, 'Checkout'),
(8, 'Contact'),
(9, 'Sitemap'),
(10, 'Affiliate'),
(11, 'Information'),
(12, 'Compare'),
(13, 'Search');

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_module`
--

CREATE TABLE `oc_layout_module` (
  `layout_module_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `position` varchar(14) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_module`
--

INSERT INTO `oc_layout_module` (`layout_module_id`, `layout_id`, `code`, `position`, `sort_order`) VALUES
(2, 4, '0', 'content_top', 0),
(3, 4, '0', 'content_top', 1),
(20, 5, '0', 'column_left', 2),
(69, 10, 'account', 'column_right', 1),
(68, 6, 'account', 'column_right', 1),
(764, 1, 'html.34', 'content_top', 8),
(112, 2, 'theme_faq.38', 'content_bottom', 0),
(706, 3, 'special.35', 'content_bottom', 1),
(705, 3, 'theme_banner_pro.33', 'content_bottom', 0),
(763, 1, 'theme_gallery.44', 'content_top', 6),
(762, 1, 'theme_testimonial.40', 'content_top', 5),
(761, 1, 'theme_most_viewed.48', 'content_top', 4),
(760, 1, 'theme_features_blocks.37', 'content_top', 3),
(759, 1, 'theme_product_tabs.32', 'content_top', 2),
(758, 1, 'theme_banner_pro.33', 'content_top', 1),
(757, 1, 'theme_slideshow.49', 'content_top', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_layout_route`
--

CREATE TABLE `oc_layout_route` (
  `layout_route_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_layout_route`
--

INSERT INTO `oc_layout_route` (`layout_route_id`, `layout_id`, `store_id`, `route`) VALUES
(38, 6, 0, 'account/%'),
(17, 10, 0, 'affiliate/%'),
(121, 3, 0, 'product/category'),
(128, 1, 0, 'common/home'),
(66, 2, 0, 'product/product'),
(24, 11, 0, 'information/information'),
(23, 7, 0, 'checkout/%'),
(31, 8, 0, 'information/contact'),
(32, 9, 0, 'information/sitemap'),
(34, 4, 0, ''),
(45, 5, 0, 'product/manufacturer'),
(52, 12, 0, 'product/compare'),
(53, 13, 0, 'product/search');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class`
--

CREATE TABLE `oc_length_class` (
  `length_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class`
--

INSERT INTO `oc_length_class` (`length_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '10.00000000'),
(3, '0.39370000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_length_class_description`
--

CREATE TABLE `oc_length_class_description` (
  `length_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_length_class_description`
--

INSERT INTO `oc_length_class_description` (`length_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Centimeter', 'cm'),
(2, 1, 'Millimeter', 'mm'),
(3, 1, 'Inch', 'in'),
(1, 2, 'Centimeter', 'cm'),
(2, 2, 'Millimeter', 'mm'),
(3, 2, 'Inch', 'in');

-- --------------------------------------------------------

--
-- Table structure for table `oc_location`
--

CREATE TABLE `oc_location` (
  `location_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `geocode` varchar(32) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `open` text NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer`
--

CREATE TABLE `oc_manufacturer` (
  `manufacturer_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer`
--

INSERT INTO `oc_manufacturer` (`manufacturer_id`, `name`, `image`, `sort_order`) VALUES
(12, 'Sample Brand', 'catalog/brand/logo_brand_2.png', 2),
(13, 'Sample Brand', 'catalog/brand/logo_brand_3.png', 3),
(14, 'Sample Brand', 'catalog/brand/logo_brand_4.png', 4),
(11, 'Sample Brand', 'catalog/brand/logo_brand_1.png', 1),
(15, 'Sample Brand', 'catalog/brand/logo_brand_5.png', 5),
(16, 'Sample Brand', 'catalog/brand/logo_brand_1.png', 6),
(17, 'Sample Brand', 'catalog/brand/logo_brand_2.png', 7),
(18, 'Sample Brand', 'catalog/brand/logo_brand_3.png', 8);

-- --------------------------------------------------------

--
-- Table structure for table `oc_manufacturer_to_store`
--

CREATE TABLE `oc_manufacturer_to_store` (
  `manufacturer_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_manufacturer_to_store`
--

INSERT INTO `oc_manufacturer_to_store` (`manufacturer_id`, `store_id`) VALUES
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_marketing`
--

CREATE TABLE `oc_marketing` (
  `marketing_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `code` varchar(64) NOT NULL,
  `clicks` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_modification`
--

CREATE TABLE `oc_modification` (
  `modification_id` int(11) NOT NULL,
  `extension_install_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(64) NOT NULL,
  `author` varchar(64) NOT NULL,
  `version` varchar(32) NOT NULL,
  `link` varchar(255) NOT NULL,
  `xml` mediumtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_module`
--

CREATE TABLE `oc_module` (
  `module_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `code` varchar(32) NOT NULL,
  `setting` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_module`
--

INSERT INTO `oc_module` (`module_id`, `name`, `code`, `setting`) VALUES
(30, 'Category', 'banner', '{\"name\":\"Category\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(29, 'Home Page', 'carousel', '{\"name\":\"Home Page\",\"banner_id\":\"8\",\"width\":\"130\",\"height\":\"100\",\"status\":\"1\"}'),
(28, 'Featured - Home Page', 'featured', '{\"name\":\"Featured - Home Page\",\"product_name\":\"\",\"product\":[\"55\",\"56\",\"52\",\"51\",\"54\",\"53\",\"50\",\"58\",\"62\"],\"limit\":\"9\",\"width\":\"300\",\"height\":\"400\",\"status\":\"1\"}'),
(27, 'Home Page', 'slideshow', '{\"name\":\"Home Page\",\"banner_id\":\"7\",\"width\":\"1170\",\"height\":\"450\",\"status\":\"1\"}'),
(31, 'Banner 1', 'banner', '{\"name\":\"Banner 1\",\"banner_id\":\"6\",\"width\":\"182\",\"height\":\"182\",\"status\":\"1\"}'),
(32, 'Product Tabs - Home Page', 'theme_product_tabs', '{\"name\":\"Product Tabs - Home Page\",\"limit\":\"10\",\"width\":\"300\",\"height\":\"300\",\"status\":\"1\",\"product\":[\"75\",\"84\",\"72\",\"76\",\"67\",\"87\",\"64\"]}'),
(33, 'Banner Pro - Home Page', 'theme_banner_pro', '{\"name\":\"Banner Pro - Home Page\",\"banner_pro_image_custom\":\"\",\"banner_pro_title_shadow\":\"0\",\"banner_pro_hover_bg_color_status\":\"1\",\"banner_pro_hover_bg_color\":\"#000000\",\"banner_pro_hover_bg_color_opacity\":\"0\",\"banner_pro_padding\":\"0\",\"status\":\"1\",\"banner_pro_item\":{\"status_1\":\"1\",\"width_1\":\"4\",\"1\":{\"url_1\":\"#\",\"label_1\":\"\",\"title_1\":\"Sample Banner\",\"subtitle_1\":\"Donec non ante justo at set duo&lt;br&gt;ultrices quam magna porta\",\"button_1\":\"Shop Now\",\"url_2\":\"#                     \",\"label_2\":\"\",\"title_2\":\"Sample Banner\",\"subtitle_2\":\"Donec non ante justo at set duo&lt;br&gt;ultrices quam magna porta\",\"button_2\":\"Shop Now\",\"url_3\":\"#                     \",\"label_3\":\"\",\"title_3\":\"Sample Banner\",\"subtitle_3\":\"Donec non ante justo at set duo&lt;br&gt;ultrices quam magna porta\",\"button_3\":\"Shop Now\",\"url_4\":\"                     \",\"label_4\":\"\",\"title_4\":\"\",\"subtitle_4\":\"\",\"button_4\":\"\",\"url_5\":\"                     \",\"label_5\":\"\",\"title_5\":\"\",\"subtitle_5\":\"\",\"button_5\":\"\",\"url_6\":\"                     \",\"label_6\":\"\",\"title_6\":\"\",\"subtitle_6\":\"\",\"button_6\":\"\"},\"2\":{\"url_1\":\"#\",\"label_1\":\"\",\"title_1\":\"Sample Banner\",\"subtitle_1\":\"Donec non ante justo at set quam elit&lt;br&gt;ultrices libero condimentum\",\"button_1\":\"Shop Now\",\"url_2\":\"#                     \",\"label_2\":\"\",\"title_2\":\"Sample Banner\",\"subtitle_2\":\"Donec non ante justo at set duo&lt;br&gt;ultrices quam magna porta\",\"button_2\":\"Shop Now\",\"url_3\":\"#                     \",\"label_3\":\"\",\"title_3\":\"Sample Banner\",\"subtitle_3\":\"Donec non ante justo at set ultrices quam\",\"button_3\":\"Shop Now\",\"url_4\":\"                     \",\"label_4\":\"\",\"title_4\":\"\",\"subtitle_4\":\"\",\"button_4\":\"\",\"url_5\":\"                     \",\"label_5\":\"\",\"title_5\":\"\",\"subtitle_5\":\"\",\"button_5\":\"\",\"url_6\":\"                     \",\"label_6\":\"\",\"title_6\":\"\",\"subtitle_6\":\"\",\"button_6\":\"\"},\"button_style_1\":\"primary\",\"content_position_1\":\"content-right\",\"hover_effect_1\":\"2\",\"status_2\":\"1\",\"width_2\":\"4\",\"button_style_2\":\"primary\",\"content_position_2\":\"content-left\",\"hover_effect_2\":\"4\",\"status_3\":\"1\",\"width_3\":\"4\",\"button_style_3\":\"primary\",\"content_position_3\":\"content-left\",\"hover_effect_3\":\"2\",\"status_4\":\"0\",\"width_4\":\"4\",\"button_style_4\":\"primary\",\"content_position_4\":\"content-bottom-center\",\"hover_effect_4\":\"1\",\"status_5\":\"0\",\"width_5\":\"4\",\"button_style_5\":\"primary\",\"content_position_5\":\"content-bottom-center\",\"hover_effect_5\":\"1\",\"status_6\":\"0\",\"width_6\":\"4\",\"button_style_6\":\"primary\",\"content_position_6\":\"content-bottom-center\",\"hover_effect_6\":\"1\"},\"banner_pro_item_image_custom_1\":\"catalog\\/banner\\/banner3.jpg\",\"banner_pro_label_color_1\":\"#95DB7E\",\"banner_pro_title_color_1\":\"#FFFFFF\",\"banner_pro_subtitle_color_1\":\"#F7F6F2\",\"banner_pro_item_image_custom_2\":\"catalog\\/banner\\/banner2.jpg\",\"banner_pro_label_color_2\":\"#95DB7E\",\"banner_pro_title_color_2\":\"#FFFFFF\",\"banner_pro_subtitle_color_2\":\"#F7F6F2\",\"banner_pro_item_image_custom_3\":\"catalog\\/banner\\/banner4.jpg\",\"banner_pro_label_color_3\":\"#95DB7E\",\"banner_pro_title_color_3\":\"#FFFFFF\",\"banner_pro_subtitle_color_3\":\"#F7F6F2\",\"banner_pro_item_image_custom_4\":\"\",\"banner_pro_label_color_4\":\"#F1494B\",\"banner_pro_title_color_4\":\"#424242\",\"banner_pro_subtitle_color_4\":\"#B6B6B6\",\"banner_pro_item_image_custom_5\":\"\",\"banner_pro_label_color_5\":\"#F1494B\",\"banner_pro_title_color_5\":\"#424242\",\"banner_pro_subtitle_color_5\":\"#B6B6B6\",\"banner_pro_item_image_custom_6\":\"\",\"banner_pro_label_color_6\":\"#F1494B\",\"banner_pro_title_color_6\":\"#424242\",\"banner_pro_subtitle_color_6\":\"#B6B6B6\"}'),
(34, 'HTML - Home Page - 1', 'html', '{\"name\":\"HTML - Home Page - 1\",\"module_description\":{\"1\":{\"title\":\"\",\"description\":\"&lt;p style=&quot;text-align: center; font-size: 19px; line-height: 35px;  padding-top: 10px; padding-bottom: 30px; margin: 0; color: #222222;&quot;&gt;&lt;strong&gt;OXY&lt;\\/strong&gt; IS A EXTREME POWERFUL &amp;amp; FLUID RESPONSIVE OPENCART THEME WITH HUNDREDS OPTIONS!&lt;br&gt;\\r\\n&lt;span style=&quot;text-align: center; font-size: 17px; color: #b6b6b6;&quot;&gt;BUILT BY PROFESSIONALS FOR PROFESSIONALS!&lt;\\/span&gt; &lt;a href=&quot;http:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720&quot; style=&quot;font-size: 17px; color:#82CE69;&quot;&gt;BUY THIS THEME!&lt;\\/a&gt;&lt;\\/p&gt;\"},\"2\":{\"title\":\"\",\"description\":\"&lt;p style=&quot;text-align: center; font-size: 19px; line-height: 35px;  padding-top: 10px; padding-bottom: 30px; margin: 0; color: #222222;&quot;&gt;&lt;strong&gt;OXY&lt;\\/strong&gt; IS A EXTREME POWERFUL &amp;amp; FLUID RESPONSIVE OPENCART THEME WITH HUNDREDS OPTIONS!&lt;br&gt;\\r\\n&lt;span style=&quot;text-align: center; font-size: 17px; color: #b6b6b6;&quot;&gt;BUILT BY PROFESSIONALS FOR PROFESSIONALS!&lt;\\/span&gt; &lt;a href=&quot;http:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720&quot; style=&quot;font-size: 17px; color:#82CE69;&quot;&gt;BUY THIS THEME!&lt;\\/a&gt;&lt;\\/p&gt;\"}},\"status\":\"0\"}'),
(35, 'Specials - Category Page', 'special', '{\"name\":\"Specials - Category Page\",\"limit\":\"6\",\"width\":\"300\",\"height\":\"400\",\"status\":\"1\"}'),
(36, 'HTML - Category Page - 1', 'html', '{\"name\":\"HTML - Category Page - 1\",\"module_description\":{\"1\":{\"title\":\"Custom Block\",\"description\":\"&lt;p style=&quot;margin-bottom:0px;&quot;&gt;This is a CMS block edited from Admin Panel. You can insert any content (text, images, HTML) here. Create an unlimited number of CSM blocks from the Admin Panel. Place the custom content in the left, right or main column.&lt;\\/p&gt;\"},\"2\":{\"title\":\"\",\"description\":\"\"}},\"status\":\"0\"}'),
(37, 'Features Blocks - Home Page', 'theme_features_blocks', '{\"name\":\"Features Blocks - Home Page\",\"bpr_id\":\"3\",\"features_blocks_style\":\"style-2\",\"module_bg_status\":\"1\",\"module_bg_color\":\"#F7F6F2\",\"module_image_custom\":\"\",\"module_title_color\":\"#222222\",\"module_subtitle_color\":\"#B6B6B6\",\"status\":\"1\",\"features_block\":{\"1\":{\"awesome_font_1\":\"cube\",\"title_1\":\"100% Fluid Responsive\",\"subtitle_1\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\",\"awesome_font_2\":\"bullseye\",\"title_2\":\"5 Ready-Made Skins\",\"subtitle_2\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\",\"awesome_font_3\":\"newspaper-o\",\"title_3\":\"13 Product Layouts\",\"subtitle_3\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\",\"awesome_font_4\":\"cube\",\"title_4\":\"Max-Width 1440px\",\"subtitle_4\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\"},\"2\":{\"awesome_font_1\":\"cube\",\"title_1\":\"100% Fluid Responsive\",\"subtitle_1\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\",\"awesome_font_2\":\"bullseye\",\"title_2\":\"5 Ready-Made Skins\",\"subtitle_2\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\",\"awesome_font_3\":\"newspaper-o\",\"title_3\":\"13 Product Layouts\",\"subtitle_3\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\",\"awesome_font_4\":\"cube\",\"title_4\":\"Max-Width 1440px\",\"subtitle_4\":\"Consectetur adipiscing egestas congue est nec convallis nunc viverra ac duis sit amet mattis\"}},\"module_icon_1_bg_color\":\"#7B8D63\",\"module_icon_1_color\":\"#FFFFFF\",\"module_description_1\":{\"1\":{\"description_1\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_1.jpg&quot; alt=&quot;&quot; style=&quot;float: left; margin-right: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"description_1\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_1.jpg&quot; alt=&quot;&quot; style=&quot;float: left; margin-right: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"}},\"module_icon_2_bg_color\":\"#A4496C\",\"module_icon_2_color\":\"#FFFFFF\",\"module_description_2\":{\"1\":{\"description_2\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_2.jpg&quot; alt=&quot;&quot; style=&quot;float: right; margin-left: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"description_2\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_2.jpg&quot; alt=&quot;&quot; style=&quot;float: right; margin-left: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"}},\"module_icon_3_bg_color\":\"#C8AD45\",\"module_icon_3_color\":\"#FFFFFF\",\"module_description_3\":{\"1\":{\"description_3\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;br&gt;&lt;br&gt;\\r\\n\\r\\nPhasellus ipsum tortor, venenatis eu ullamcorper et, imperdiet quis nisi. Sed ultrices velit ut lacus viverra ornare. Nam adipiscing ullamcorper arcu, in tincidunt leo tempor non. Maecenas adipiscing nulla eget est malesuada molestie. Proin et lorem lectus, ac eleifend nunc. Duis commodo imperdiet viverra. Aenean nec interdum magna. Fusce ullamcorper ipsum at nibh eleifend at pulvinar sapien egestas. Proin nec augue id turpis consequat iaculis vitae quis dui. Maecenas fermentum est vel felis aliquet vitae pulvinar augue iaculis. Duis felis lacus, lacinia at luctus non, elementum fermentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam condimentum metus a urna tempus vel sollicitudin lacus pharetra. Vestibulum tincidunt, magna vel porta hendrerit, mi purus rhoncus odio, sit amet venenatis orci urna a ligula.\"},\"2\":{\"description_3\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;br&gt;&lt;br&gt;\\r\\n\\r\\nPhasellus ipsum tortor, venenatis eu ullamcorper et, imperdiet quis nisi. Sed ultrices velit ut lacus viverra ornare. Nam adipiscing ullamcorper arcu, in tincidunt leo tempor non. Maecenas adipiscing nulla eget est malesuada molestie. Proin et lorem lectus, ac eleifend nunc. Duis commodo imperdiet viverra. Aenean nec interdum magna. Fusce ullamcorper ipsum at nibh eleifend at pulvinar sapien egestas. Proin nec augue id turpis consequat iaculis vitae quis dui. Maecenas fermentum est vel felis aliquet vitae pulvinar augue iaculis. Duis felis lacus, lacinia at luctus non, elementum fermentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam condimentum metus a urna tempus vel sollicitudin lacus pharetra. Vestibulum tincidunt, magna vel porta hendrerit, mi purus rhoncus odio, sit amet venenatis orci urna a ligula.\"}},\"module_icon_4_bg_color\":\"#5A495B\",\"module_icon_4_color\":\"#FFFFFF\",\"module_description_4\":{\"1\":{\"description_4\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;br&gt;&lt;br&gt;\\r\\n\\r\\nPhasellus ipsum tortor, venenatis eu ullamcorper et, imperdiet quis nisi. Sed ultrices velit ut lacus viverra ornare. Nam adipiscing ullamcorper arcu, in tincidunt leo tempor non. Maecenas adipiscing nulla eget est malesuada molestie. Proin et lorem lectus, ac eleifend nunc. Duis commodo imperdiet viverra. Aenean nec interdum magna. Fusce ullamcorper ipsum at nibh eleifend at pulvinar sapien egestas. Proin nec augue id turpis consequat iaculis vitae quis dui. Maecenas fermentum est vel felis aliquet vitae pulvinar augue iaculis. Duis felis lacus, lacinia at luctus non, elementum fermentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam condimentum metus a urna tempus vel sollicitudin lacus pharetra. Vestibulum tincidunt, magna vel porta hendrerit, mi purus rhoncus odio, sit amet venenatis orci urna a ligula.\"},\"2\":{\"description_4\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;br&gt;&lt;br&gt;\\r\\n\\r\\nPhasellus ipsum tortor, venenatis eu ullamcorper et, imperdiet quis nisi. Sed ultrices velit ut lacus viverra ornare. Nam adipiscing ullamcorper arcu, in tincidunt leo tempor non. Maecenas adipiscing nulla eget est malesuada molestie. Proin et lorem lectus, ac eleifend nunc. Duis commodo imperdiet viverra. Aenean nec interdum magna. Fusce ullamcorper ipsum at nibh eleifend at pulvinar sapien egestas. Proin nec augue id turpis consequat iaculis vitae quis dui. Maecenas fermentum est vel felis aliquet vitae pulvinar augue iaculis. Duis felis lacus, lacinia at luctus non, elementum fermentum velit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam condimentum metus a urna tempus vel sollicitudin lacus pharetra. Vestibulum tincidunt, magna vel porta hendrerit, mi purus rhoncus odio, sit amet venenatis orci urna a ligula.\"}}}'),
(38, 'FAQ - Product Page', 'theme_faq', '{\"name\":\"FAQ - Product Page\",\"faq\":{\"1\":{\"title\":\"FAQ - Frequently Asked Questions\"},\"2\":{\"title\":\"FAQ - Frequently Asked Questions\"}},\"module_question_bg_color\":\"#F7F6F2\",\"module_question_bg_color_hover\":\"#82CE69\",\"module_question_color\":\"#222222\",\"module_question_color_hover\":\"#FFFFFF\",\"module_answer_bg_color\":\"#FFFFFF\",\"module_answer_color\":\"#222222\",\"status\":\"1\",\"sections\":{\"1\":{\"faq_question\":{\"1\":\"Q: I need to change something on my order. How can I do that?\",\"2\":\"Q: I need to change something on my order. How can I do that?\"},\"faq_answer\":{\"1\":\"Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus &lt;a href=&quot;#&quot;&gt;mauris sed risus&lt;\\/a&gt;. Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim. Proin vitae tristique lacus. Phasellus vitae metus vitae sapien pharetra. \",\"2\":\"Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus &lt;a href=&quot;#&quot;&gt;mauris sed risus&lt;\\/a&gt;. Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim. Proin vitae tristique lacus. Phasellus vitae metus vitae sapien pharetra. \"}},\"2\":{\"faq_question\":{\"1\":\"Q: Where is my order confirmation?\",\"2\":\"Q: Where is my order confirmation?\"},\"faq_answer\":{\"1\":\"Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim. Proin vitae tristique lacus. Phasellus vitae metus vitae sapien pharetra. \",\"2\":\"Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim. Proin vitae tristique lacus. Phasellus vitae metus vitae sapien pharetra. \"}},\"3\":{\"faq_question\":{\"1\":\"Q: When will my order ship?\",\"2\":\"Q: When will my order ship?\"},\"faq_answer\":{\"1\":\"Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. \",\"2\":\"Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. \"}},\"4\":{\"faq_question\":{\"1\":\"Q: What is your return policy?\",\"2\":\"Q: What is your return policy?\"},\"faq_answer\":{\"1\":\"Nunc at neque nisl. Praesent ac laoreet nibh. Aliquam enim lectus, sagittis in ullamcorper sit amet, posuere eu arcu. Cras sit amet purus lorem. Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus. Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero. \",\"2\":\"Nunc at neque nisl. Praesent ac laoreet nibh. Aliquam enim lectus, sagittis in ullamcorper sit amet, posuere eu arcu. Cras sit amet purus lorem. Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus. Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero. \"}},\"5\":{\"faq_question\":{\"1\":\"Q: I don\'t see the answer to my question here. Who do I contact?\",\"2\":\"Q: I don\'t see the answer to my question here. Who do I contact?\"},\"faq_answer\":{\"1\":\"Integer sit amet nisi ante. Duis commodo, dolor a ullamcorper tincidunt, mi velit dignissim tellus, eget mattis risus nibh eget enim. Quisque quis ligula ac justo dictum posuere at sit amet ipsum. Phasellus nisl sapien, tincidunt quis consectetur. \",\"2\":\"Integer sit amet nisi ante. Duis commodo, dolor a ullamcorper tincidunt, mi velit dignissim tellus, eget mattis risus nibh eget enim. Quisque quis ligula ac justo dictum posuere at sit amet ipsum. Phasellus nisl sapien, tincidunt quis consectetur. \"}}}}'),
(39, 'Highly Recommended', 'theme_highly_recommended', '{\"name\":\"Highly Recommended\",\"highlyrecommended\":{\"1\":{\"title\":\"Recommended Products\"},\"2\":{\"title\":\"Recommended Products\"}},\"product\":[\"89\",\"85\",\"84\",\"86\",\"82\",\"77\",\"78\"],\"limit\":\"9\",\"width\":\"300\",\"height\":\"400\",\"module_bottom_bar_bg_color\":\"#F1494B\",\"status\":\"1\"}'),
(40, 'Testimonial - Home Page', 'theme_testimonial', '{\"name\":\"Testimonial - Home Page\",\"testimonial\":{\"1\":{\"title\":\"What People Are Saying\"},\"2\":{\"title\":\"What People Are Saying\"}},\"pr_id\":\"3\",\"module_bg_color\":\"#F7F6F2\",\"module_image_custom\":\"\",\"module_title_color\":\"#222222\",\"module_title_border_color\":\"#F7F6F2\",\"module_testimonial_color\":\"#222222\",\"module_testimonial_bg_color\":\"#FFFFFF\",\"module_name_color\":\"#B6B6B6\",\"status\":\"0\",\"sections\":{\"1\":{\"thumb_image\":\"catalog\\/testimonial_img_2.jpg\",\"testimonial_block\":{\"1\":\"Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit, et leo magna mattis . Senectus et netus et malesuada egestas.\",\"2\":\"Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit, et leo magna mattis . Senectus et netus et malesuada egestas.\"},\"reviewer_name\":{\"1\":\"Kerrie\",\"2\":\"Kerrie\"}},\"2\":{\"thumb_image\":\"catalog\\/testimonial_img_1.jpg\",\"testimonial_block\":{\"1\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam sed magna mattis consectetur. Aenean nulla felis hendrerit.\",\"2\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam sed magna mattis consectetur. Aenean nulla felis hendrerit.\"},\"reviewer_name\":{\"1\":\"Alex\",\"2\":\"Alex\"}},\"3\":{\"thumb_image\":\"catalog\\/testimonial_img_3.jpg\",\"testimonial_block\":{\"1\":\"Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Etiam dapibus mauris placerat nulla sagittis suscipit vitae. Arcu placerat est semper congue rutrum.\",\"2\":\"Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Etiam dapibus mauris placerat nulla sagittis suscipit vitae. Arcu placerat est semper congue rutrum.\"},\"reviewer_name\":{\"1\":\"Joan\",\"2\":\"Joan\"}},\"4\":{\"thumb_image\":\"catalog\\/testimonial_img_4.jpg\",\"testimonial_block\":{\"1\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam sed magna mattis consectetur. Vestibulum quis ornare mauris.\",\"2\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam sed magna mattis consectetur. Vestibulum quis ornare mauris.\"},\"reviewer_name\":{\"1\":\"Roland\",\"2\":\"Roland\"}}}}'),
(41, 'Category Slider - Home Page', 'theme_category_slider', '{\"name\":\"Category Slider - Home Page\",\"category_slider\":{\"1\":{\"title\":\"Shop by Category\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"},\"2\":{\"title\":\"Shop by Category\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"}},\"ca_id\":\"6\",\"module_bg_color\":\"#FFFFFF\",\"module_title_color\":\"#424242\",\"module_title_border_color\":\"#F3F3F3\",\"module_subtitle_color\":\"#B6B6B6\",\"status\":\"1\"}'),
(42, 'Brand Slider - Home Page', 'theme_brand_slider', '{\"name\":\"Brand Slider - Home Page\",\"brand_slider\":{\"1\":{\"title\":\"Shop by Brand\",\"subtitle\":\"\"},\"2\":{\"title\":\"Shop by Brand\",\"subtitle\":\"\"}},\"ca_id\":\"6\",\"brands_display_style\":\"2\",\"module_bg_color\":\"#FFFFFF\",\"module_title_color\":\"#424242\",\"module_title_border_color\":\"#F3F3F3\",\"module_subtitle_color\":\"#B6B6B6\",\"status\":\"1\"}'),
(43, 'Custom Products - Home Page', 'theme_custom_products', '{\"name\":\"Custom Products - Home Page\",\"custom_products\":{\"1\":{\"title\":\"Custom Products\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui neque a arcu. Fusce erat dolor, varius non pretium eget, vestibulum elementum lectus volutpat congue. \",\"button\":\"More Products\",\"button_url\":\"#\"},\"2\":{\"title\":\"Custom Products\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui neque a arcu. Fusce erat dolor, varius non pretium eget, vestibulum elementum lectus volutpat congue. \",\"button\":\"More Products\",\"button_url\":\"#\"}},\"product\":[\"74\",\"65\",\"82\",\"84\",\"76\"],\"limit\":\"9\",\"width\":\"300\",\"height\":\"400\",\"pr_id\":\"4\",\"module_title_position\":\"left\",\"module_title_width\":\"3\",\"module_items_width\":\"9\",\"module_bg_color\":\"#2F2F2F\",\"module_image_custom\":\"\",\"module_title_color\":\"#FFFFFF\",\"module_title_border_color\":\"#373737\",\"module_subtitle_color\":\"#6E6E6E\",\"status\":\"1\"}'),
(44, 'Gallery - Home Page', 'theme_gallery', '{\"name\":\"Gallery - Home Page\",\"gallery\":{\"1\":{\"title\":\"Gallery\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"},\"2\":{\"title\":\"Gallery\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"}},\"banner_id\":\"10\",\"width\":\"650\",\"height\":\"650\",\"pr_id\":\"8\",\"module_bg_color\":\"#FFFFFF\",\"module_title_color\":\"#222222\",\"module_title_border_color\":\"#F7F6F2\",\"module_subtitle_color\":\"#B6B6B6\",\"status\":\"0\"}'),
(47, 'Theme Featured - Home Page', 'theme_featured', '{\"name\":\"Theme Featured - Home Page\",\"featured\":{\"1\":{\"title\":\"Theme Featured\"},\"2\":{\"title\":\"Theme Featured\"}},\"product\":[\"70\",\"74\",\"65\",\"82\"],\"limit\":\"6\",\"width\":\"900\",\"height\":\"1200\",\"products_style\":\"featured-product-view-2\",\"status\":\"1\"}'),
(45, 'LookBook - Home Page', 'theme_lookbook', '{\"name\":\"LookBook - Home Page\",\"lookbook\":{\"1\":{\"title\":\"LookBook\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"},\"2\":{\"title\":\"LookBook\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"}},\"banner_id\":\"8\",\"width\":\"300\",\"height\":\"400\",\"pr_id\":\"5\",\"module_bg_color\":\"#FFFFFF\",\"module_title_color\":\"#424242\",\"module_title_border_color\":\"#F3F3F3\",\"module_subtitle_color\":\"#B6B6B6\",\"status\":\"1\"}'),
(46, 'Store TV - Home Page', 'theme_store_tv', '{\"name\":\"Store TV - Home Page\",\"store_tv\":{\"1\":{\"title\":\"Video Reviews\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"},\"2\":{\"title\":\"Video Reviews\",\"subtitle\":\"Mauris risus felis adipiscing eu consequat in tincidunt leo tempor est adipiscing nulla eget est malesuada consequat iaculis vitae quis dui\"}},\"banner_id\":\"9\",\"width\":\"480\",\"height\":\"270\",\"pr_id\":\"5\",\"module_bg_color\":\"#FFFFFF\",\"module_title_color\":\"#424242\",\"module_title_border_color\":\"#F3F3F3\",\"module_subtitle_color\":\"#B6B6B6\",\"status\":\"1\"}'),
(48, 'Most Viewed - Home Page', 'theme_most_viewed', '{\"name\":\"Most Viewed - Home Page\",\"limit\":\"5\",\"width\":\"300\",\"height\":\"300\",\"status\":\"1\"}'),
(49, 'Theme Slideshow - Home Page', 'theme_slideshow', '{\"name\":\"Theme Slideshow - Home Page\",\"slideshow_hover_bg_color_status\":\"0\",\"slideshow_hover_bg_color\":\"#000000\",\"slideshow_hover_bg_color_opacity\":\"0.1\",\"slideshow_pagination\":\"1\",\"slideshow_time\":\"7000\",\"status\":\"1\",\"slideshow_item\":{\"status_1\":\"1\",\"1\":{\"url_1\":\"https:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720\",\"label_1\":\"\",\"title_1\":\"Superb\\r\\n&lt;br&gt;\\r\\nExtreme Powerful\\r\\n&lt;br&gt;\\r\\nAdmin Panel\",\"subtitle_1\":\"OXY can save you hundreds of hours of work&lt;br&gt;and can really speed up your store!\",\"button_1\":\"Buy This Theme!\",\"url_2\":\"https:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720\",\"label_2\":\"\",\"title_2\":\"Super Flexible\\r\\n&lt;br&gt;\\r\\nResponsive Layout\",\"subtitle_2\":\"OXY has a solid responsive layout&lt;br&gt;that scales from 320px to Full Width!\",\"button_2\":\"Buy This Theme!\",\"url_3\":\"\",\"label_3\":\"\",\"title_3\":\"\",\"subtitle_3\":\"\",\"button_3\":\"\",\"url_4\":\"\",\"label_4\":\"\",\"title_4\":\"\",\"subtitle_4\":\"\",\"button_4\":\"\",\"url_5\":\"\",\"label_5\":\"\",\"title_5\":\"\",\"subtitle_5\":\"\",\"button_5\":\"\",\"url_6\":\"\",\"label_6\":\"\",\"title_6\":\"\",\"subtitle_6\":\"\",\"button_6\":\"\"},\"2\":{\"url_1\":\"https:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720\",\"label_1\":\"Extreme Powerful\",\"title_1\":\"Admin Panel\",\"subtitle_1\":\"OXY can save you hundreds of hours of work&lt;br&gt;and can really speed up your store!\",\"button_1\":\"Buy This Theme!\",\"url_2\":\"https:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720\",\"label_2\":\"Super Flexible\",\"title_2\":\"Layout\",\"subtitle_2\":\"OXY has a solid responsive layout&lt;br&gt;that scales from 320px to Full Width!\",\"button_2\":\"Buy This Theme!\",\"url_3\":\"\",\"label_3\":\"\",\"title_3\":\"\",\"subtitle_3\":\"\",\"button_3\":\"\",\"url_4\":\"\",\"label_4\":\"\",\"title_4\":\"\",\"subtitle_4\":\"\",\"button_4\":\"\",\"url_5\":\"\",\"label_5\":\"\",\"title_5\":\"\",\"subtitle_5\":\"\",\"button_5\":\"\",\"url_6\":\"\",\"label_6\":\"\",\"title_6\":\"\",\"subtitle_6\":\"\",\"button_6\":\"\"},\"button_style_1\":\"primary\",\"content_position_1\":\"content-center-left\",\"main_image_animation_1\":\"zoomIn\",\"title_animation_1\":\"slideInRight\",\"subtitle_animation_1\":\"slideInLeft\",\"button_animation_1\":\"slideInRight\",\"hover_effect_1\":\"2\",\"status_2\":\"1\",\"button_style_2\":\"primary\",\"content_position_2\":\"content-center\",\"main_image_animation_2\":\"slideInUp\",\"title_animation_2\":\"slideInUp\",\"subtitle_animation_2\":\"slideInUp\",\"button_animation_2\":\"slideInUp\",\"hover_effect_2\":\"3\",\"status_3\":\"0\",\"button_style_3\":\"primary\",\"content_position_3\":\"content-bottom-center\",\"main_image_animation_3\":\"fadeIn\",\"title_animation_3\":\"fadeIn\",\"subtitle_animation_3\":\"fadeIn\",\"button_animation_3\":\"fadeIn\",\"hover_effect_3\":\"1\",\"status_4\":\"0\",\"button_style_4\":\"primary\",\"content_position_4\":\"content-bottom-center\",\"main_image_animation_4\":\"fadeIn\",\"title_animation_4\":\"fadeIn\",\"subtitle_animation_4\":\"fadeIn\",\"button_animation_4\":\"fadeIn\",\"hover_effect_4\":\"1\",\"status_5\":\"0\",\"button_style_5\":\"primary\",\"content_position_5\":\"content-bottom-center\",\"main_image_animation_5\":\"fadeIn\",\"title_animation_5\":\"fadeIn\",\"subtitle_animation_5\":\"fadeIn\",\"button_animation_5\":\"fadeIn\",\"hover_effect_5\":\"1\",\"status_6\":\"0\",\"button_style_6\":\"primary\",\"content_position_6\":\"content-bottom-center\",\"main_image_animation_6\":\"fadeIn\",\"title_animation_6\":\"fadeIn\",\"subtitle_animation_6\":\"fadeIn\",\"button_animation_6\":\"fadeIn\",\"hover_effect_6\":\"1\"},\"slideshow_item_image_custom_1\":\"catalog\\/banner\\/banner1.jpg\",\"slideshow_item_main_image_custom_1\":\"\",\"slideshow_label_color_1\":\"#F1494B\",\"slideshow_title_color_1\":\"#FFFFFF\",\"slideshow_subtitle_color_1\":\"#F7F6F2\",\"slideshow_item_image_custom_2\":\"catalog\\/banner\\/banner4.jpg\",\"slideshow_item_main_image_custom_2\":\"\",\"slideshow_label_color_2\":\"#F1494B\",\"slideshow_title_color_2\":\"#FFFFFF\",\"slideshow_subtitle_color_2\":\"#F7F6F2\",\"slideshow_item_image_custom_3\":\"\",\"slideshow_item_main_image_custom_3\":\"\",\"slideshow_label_color_3\":\"#F1494B\",\"slideshow_title_color_3\":\"#424242\",\"slideshow_subtitle_color_3\":\"#666666\",\"slideshow_item_image_custom_4\":\"\",\"slideshow_item_main_image_custom_4\":\"\",\"slideshow_label_color_4\":\"#F1494B\",\"slideshow_title_color_4\":\"#424242\",\"slideshow_subtitle_color_4\":\"#666666\",\"slideshow_item_image_custom_5\":\"\",\"slideshow_item_main_image_custom_5\":\"\",\"slideshow_label_color_5\":\"#F1494B\",\"slideshow_title_color_5\":\"#424242\",\"slideshow_subtitle_color_5\":\"#666666\",\"slideshow_item_image_custom_6\":\"\",\"slideshow_item_main_image_custom_6\":\"\",\"slideshow_label_color_6\":\"#F1494B\",\"slideshow_title_color_6\":\"#424242\",\"slideshow_subtitle_color_6\":\"#666666\"}'),
(50, 'Banner Pro - Category Page', 'theme_banner_pro', '{\"name\":\"Banner Pro - Category Page\",\"banner_pro_image_custom\":\"\",\"banner_pro_title_shadow\":\"0\",\"banner_pro_hover_bg_color_status\":\"1\",\"banner_pro_hover_bg_color\":\"#000000\",\"banner_pro_hover_bg_color_opacity\":\"0\",\"banner_pro_padding\":\"30\",\"status\":\"1\",\"banner_pro_item\":{\"status_1\":\"1\",\"width_1\":\"6\",\"1\":{\"url_1\":\"\",\"label_1\":\"\",\"title_1\":\"Sample Banner\",\"subtitle_1\":\"Donec non ante justo at set quam elit&lt;br&gt;ultrices libero condimentum est&lt;br&gt;quis ornare mauris arcu ac\",\"button_1\":\"Shop Now\",\"url_2\":\"       \",\"label_2\":\"New\",\"title_2\":\"Sample Banner\",\"subtitle_2\":\"Donec non ante justo at set ultrices quam&lt;br&gt;placerat libero nec semper laoreet quis\",\"button_2\":\"\",\"url_3\":\"       \",\"label_3\":\"\",\"title_3\":\"\",\"subtitle_3\":\"\",\"button_3\":\"\",\"url_4\":\"       \",\"label_4\":\"\",\"title_4\":\"\",\"subtitle_4\":\"\",\"button_4\":\"\",\"url_5\":\"       \",\"label_5\":\"\",\"title_5\":\"\",\"subtitle_5\":\"\",\"button_5\":\"\",\"url_6\":\"       \",\"label_6\":\"\",\"title_6\":\"\",\"subtitle_6\":\"\",\"button_6\":\"\"},\"2\":{\"url_1\":\"\",\"label_1\":\"\",\"title_1\":\"Sample Banner\",\"subtitle_1\":\"Donec non ante justo at set quam elit&lt;br&gt;ultrices libero condimentum est&lt;br&gt;quis ornare mauris arcu ac\",\"button_1\":\"Shop Now\",\"url_2\":\"       \",\"label_2\":\"New\",\"title_2\":\"Sample Banner\",\"subtitle_2\":\"Donec non ante justo at set ultrices quam&lt;br&gt;placerat libero nec semper laoreet quis\",\"button_2\":\"\",\"url_3\":\"       \",\"label_3\":\"\",\"title_3\":\"\",\"subtitle_3\":\"\",\"button_3\":\"\",\"url_4\":\"       \",\"label_4\":\"\",\"title_4\":\"\",\"subtitle_4\":\"\",\"button_4\":\"\",\"url_5\":\"       \",\"label_5\":\"\",\"title_5\":\"\",\"subtitle_5\":\"\",\"button_5\":\"\",\"url_6\":\"       \",\"label_6\":\"\",\"title_6\":\"\",\"subtitle_6\":\"\",\"button_6\":\"\"},\"button_style_1\":\"primary\",\"content_position_1\":\"content-top-left\",\"hover_effect_1\":\"4\",\"status_2\":\"1\",\"width_2\":\"6\",\"button_style_2\":\"primary\",\"content_position_2\":\"content-bottom-left\",\"hover_effect_2\":\"2\",\"status_3\":\"0\",\"width_3\":\"1\",\"button_style_3\":\"primary\",\"content_position_3\":\"content-bottom-center\",\"hover_effect_3\":\"1\",\"status_4\":\"0\",\"width_4\":\"1\",\"button_style_4\":\"primary\",\"content_position_4\":\"content-bottom-center\",\"hover_effect_4\":\"1\",\"status_5\":\"0\",\"width_5\":\"1\",\"button_style_5\":\"primary\",\"content_position_5\":\"content-bottom-center\",\"hover_effect_5\":\"1\",\"status_6\":\"0\",\"width_6\":\"1\",\"button_style_6\":\"primary\",\"content_position_6\":\"content-bottom-center\",\"hover_effect_6\":\"1\"},\"banner_pro_item_image_custom_1\":\"catalog\\/banner\\/small_banner_1.jpg\",\"banner_pro_label_color_1\":\"#F1494B\",\"banner_pro_title_color_1\":\"#424242\",\"banner_pro_subtitle_color_1\":\"#B6B6B6\",\"banner_pro_item_image_custom_2\":\"catalog\\/banner\\/small_banner_2.jpg\",\"banner_pro_label_color_2\":\"#F1494B\",\"banner_pro_title_color_2\":\"#424242\",\"banner_pro_subtitle_color_2\":\"#B6B6B6\",\"banner_pro_item_image_custom_3\":\"\",\"banner_pro_label_color_3\":\"#F1494B\",\"banner_pro_title_color_3\":\"#424242\",\"banner_pro_subtitle_color_3\":\"#B6B6B6\",\"banner_pro_item_image_custom_4\":\"\",\"banner_pro_label_color_4\":\"#F1494B\",\"banner_pro_title_color_4\":\"#424242\",\"banner_pro_subtitle_color_4\":\"#B6B6B6\",\"banner_pro_item_image_custom_5\":\"\",\"banner_pro_label_color_5\":\"#F1494B\",\"banner_pro_title_color_5\":\"#424242\",\"banner_pro_subtitle_color_5\":\"#B6B6B6\",\"banner_pro_item_image_custom_6\":\"\",\"banner_pro_label_color_6\":\"#F1494B\",\"banner_pro_title_color_6\":\"#424242\",\"banner_pro_subtitle_color_6\":\"#B6B6B6\"}'),
(52, 'Product Slider - Home Page', 'theme_product_slider', '{\"name\":\"Product Slider - Home Page\",\"product\":[\"55\",\"56\",\"52\",\"51\",\"54\",\"53\",\"58\",\"62\"],\"limit\":\"10\",\"width\":\"375\",\"height\":\"500\",\"module_bg_color\":\"#FFFFFF\",\"module_product_name_color\":\"#424242\",\"module_product_description_color\":\"#B6B6B6\",\"module_product_price_color\":\"#424242\",\"module_product_active_border_color\":\"#F3F3F3\",\"status\":\"1\"}'),
(51, 'Bestsellers - Category Page', 'bestseller', '{\"name\":\"Bestsellers - Category Page\",\"limit\":\"6\",\"width\":\"52\",\"height\":\"70\",\"status\":\"1\"}'),
(53, 'HTML - Home Page - Parallax 1', 'html', '{\"name\":\"HTML - Home Page - Parallax 1\",\"module_description\":{\"1\":{\"title\":\"\",\"description\":\"&lt;div class=&quot;widget-wrapper widget-parallax widget-parallax-001&quot; data-stellar-background-ratio=&quot;0.5&quot; style=&quot;background-color:#F7F6F2; background-image: url(image\\/catalog\\/banner\\/main_banner_bg_organic_3.jpg); text-align:center; padding: 90px 70px;&quot;&gt;\r\n\r\n&lt;div class=&quot;widget-title come-item&quot;&gt;\r\n&lt;h1 style=&quot;color:#FFFFFF; font-size:58px; font-weight:900; margin-bottom:15px; text-align:center;&quot;&gt;Final Clothing Sale&lt;\\/h1&gt;\r\n&lt;\\/div&gt;\r\n\r\n&lt;div class=&quot;widget-subtitle subtitle come-item&quot; style=&quot;color:#F7F6F2; font-size:16px; margin-bottom:30px;&quot;&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor&lt;br&gt;gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam.&lt;br&gt;Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna.&lt;\\/div&gt;\r\n\r\n&lt;div class=&quot;widget-buttons widget-buttons-center come-item&quot; style=&quot;margin-bottom:35px;&quot;&gt;\r\n&lt;a href=&quot;#&quot; class=&quot;btn btn-default&quot;&gt;View Details&lt;\\/a&gt;\r\n&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot;&gt;Shop Now!&lt;\\/a&gt;\r\n&lt;\\/div&gt;\r\n\r\n&lt;\\/div&gt;\r\n\"},\"2\":{\"title\":\"\",\"description\":\"\"}},\"status\":\"1\"}');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option`
--

CREATE TABLE `oc_option` (
  `option_id` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option`
--

INSERT INTO `oc_option` (`option_id`, `type`, `sort_order`) VALUES
(1, 'radio', 1),
(2, 'checkbox', 2),
(4, 'text', 3),
(5, 'select', 4),
(6, 'textarea', 5),
(7, 'file', 6),
(8, 'date', 7),
(9, 'time', 8),
(10, 'datetime', 9),
(11, 'select', 10),
(12, 'date', 11),
(13, 'select', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_description`
--

CREATE TABLE `oc_option_description` (
  `option_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_description`
--

INSERT INTO `oc_option_description` (`option_id`, `language_id`, `name`) VALUES
(1, 1, 'Radio'),
(2, 1, 'Checkbox'),
(4, 1, 'Text'),
(6, 1, 'Textarea'),
(8, 1, 'Date'),
(7, 1, 'File'),
(5, 1, 'Select'),
(9, 1, 'Time'),
(10, 1, 'Date &amp; Time'),
(12, 1, 'Delivery Date'),
(11, 1, 'Size'),
(13, 1, 'Color'),
(1, 2, 'Radio'),
(2, 2, 'Checkbox'),
(4, 2, 'Text'),
(6, 2, 'Textarea'),
(8, 2, 'Date'),
(7, 2, 'File'),
(5, 2, 'Select'),
(9, 2, 'Time'),
(10, 2, 'Date &amp; Time'),
(12, 2, 'Delivery Date'),
(11, 2, 'Size'),
(13, 2, 'Color');

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value`
--

CREATE TABLE `oc_option_value` (
  `option_value_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value`
--

INSERT INTO `oc_option_value` (`option_value_id`, `option_id`, `image`, `sort_order`) VALUES
(43, 1, '', 3),
(32, 1, '', 1),
(45, 2, '', 4),
(44, 2, '', 3),
(42, 5, '', 4),
(41, 5, '', 3),
(39, 5, '', 1),
(40, 5, '', 2),
(31, 1, '', 2),
(23, 2, '', 1),
(24, 2, '', 2),
(50, 11, '', 5),
(49, 11, '', 4),
(48, 11, '', 3),
(47, 11, '', 2),
(46, 11, '', 1),
(54, 13, '', 4),
(55, 13, '', 5),
(53, 13, '', 3),
(52, 13, '', 2),
(51, 13, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_option_value_description`
--

CREATE TABLE `oc_option_value_description` (
  `option_value_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_option_value_description`
--

INSERT INTO `oc_option_value_description` (`option_value_id`, `language_id`, `option_id`, `name`) VALUES
(43, 1, 1, 'Large'),
(32, 1, 1, 'Small'),
(45, 1, 2, 'Checkbox 4'),
(44, 1, 2, 'Checkbox 3'),
(31, 1, 1, 'Medium'),
(42, 1, 5, 'Yellow'),
(41, 1, 5, 'Green'),
(39, 1, 5, 'Red'),
(40, 1, 5, 'Blue'),
(23, 1, 2, 'Checkbox 1'),
(24, 1, 2, 'Checkbox 2'),
(50, 1, 11, 'XXL'),
(49, 1, 11, 'XL'),
(48, 1, 11, 'L'),
(47, 1, 11, 'M'),
(46, 1, 11, 'S'),
(55, 1, 13, 'Blue'),
(54, 1, 13, 'Orange'),
(53, 1, 13, 'Red'),
(52, 1, 13, 'Black'),
(51, 1, 13, 'White'),
(43, 2, 1, 'Large'),
(32, 2, 1, 'Small'),
(45, 2, 2, 'Checkbox 4'),
(44, 2, 2, 'Checkbox 3'),
(31, 2, 1, 'Medium'),
(42, 2, 5, 'Yellow'),
(41, 2, 5, 'Green'),
(39, 2, 5, 'Red'),
(40, 2, 5, 'Blue'),
(23, 2, 2, 'Checkbox 1'),
(24, 2, 2, 'Checkbox 2'),
(50, 2, 11, 'XXL'),
(49, 2, 11, 'XL'),
(48, 2, 11, 'L'),
(47, 2, 11, 'M'),
(46, 2, 11, 'S'),
(55, 2, 13, 'Blue'),
(54, 2, 13, 'Orange'),
(53, 2, 13, 'Red'),
(52, 2, 13, 'Black'),
(51, 2, 13, 'White');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order`
--

CREATE TABLE `oc_order` (
  `order_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL DEFAULT '0',
  `invoice_prefix` varchar(26) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `store_name` varchar(64) NOT NULL,
  `store_url` varchar(255) NOT NULL,
  `customer_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `custom_field` text NOT NULL,
  `payment_firstname` varchar(32) NOT NULL,
  `payment_lastname` varchar(32) NOT NULL,
  `payment_company` varchar(60) NOT NULL,
  `payment_address_1` varchar(128) NOT NULL,
  `payment_address_2` varchar(128) NOT NULL,
  `payment_city` varchar(128) NOT NULL,
  `payment_postcode` varchar(10) NOT NULL,
  `payment_country` varchar(128) NOT NULL,
  `payment_country_id` int(11) NOT NULL,
  `payment_zone` varchar(128) NOT NULL,
  `payment_zone_id` int(11) NOT NULL,
  `payment_address_format` text NOT NULL,
  `payment_custom_field` text NOT NULL,
  `payment_method` varchar(128) NOT NULL,
  `payment_code` varchar(128) NOT NULL,
  `shipping_firstname` varchar(32) NOT NULL,
  `shipping_lastname` varchar(32) NOT NULL,
  `shipping_company` varchar(40) NOT NULL,
  `shipping_address_1` varchar(128) NOT NULL,
  `shipping_address_2` varchar(128) NOT NULL,
  `shipping_city` varchar(128) NOT NULL,
  `shipping_postcode` varchar(10) NOT NULL,
  `shipping_country` varchar(128) NOT NULL,
  `shipping_country_id` int(11) NOT NULL,
  `shipping_zone` varchar(128) NOT NULL,
  `shipping_zone_id` int(11) NOT NULL,
  `shipping_address_format` text NOT NULL,
  `shipping_custom_field` text NOT NULL,
  `shipping_method` varchar(128) NOT NULL,
  `shipping_code` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `order_status_id` int(11) NOT NULL DEFAULT '0',
  `affiliate_id` int(11) NOT NULL,
  `commission` decimal(15,4) NOT NULL,
  `marketing_id` int(11) NOT NULL,
  `tracking` varchar(64) NOT NULL,
  `language_id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_value` decimal(15,8) NOT NULL DEFAULT '1.00000000',
  `ip` varchar(40) NOT NULL,
  `forwarded_ip` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `accept_language` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order`
--

INSERT INTO `oc_order` (`order_id`, `invoice_no`, `invoice_prefix`, `store_id`, `store_name`, `store_url`, `customer_id`, `customer_group_id`, `firstname`, `lastname`, `email`, `telephone`, `fax`, `custom_field`, `payment_firstname`, `payment_lastname`, `payment_company`, `payment_address_1`, `payment_address_2`, `payment_city`, `payment_postcode`, `payment_country`, `payment_country_id`, `payment_zone`, `payment_zone_id`, `payment_address_format`, `payment_custom_field`, `payment_method`, `payment_code`, `shipping_firstname`, `shipping_lastname`, `shipping_company`, `shipping_address_1`, `shipping_address_2`, `shipping_city`, `shipping_postcode`, `shipping_country`, `shipping_country_id`, `shipping_zone`, `shipping_zone_id`, `shipping_address_format`, `shipping_custom_field`, `shipping_method`, `shipping_code`, `comment`, `total`, `order_status_id`, `affiliate_id`, `commission`, `marketing_id`, `tracking`, `language_id`, `currency_id`, `currency_code`, `currency_value`, `ip`, `forwarded_ip`, `user_agent`, `accept_language`, `date_added`, `date_modified`) VALUES
(1, 0, 'INV-2017-00', 0, 'Your Store', 'http://localhost/opencart_30_oxy/', 1, 1, 'John', 'Rodman', 'fasdfasfasf@sfaisdgfuiysavgf.com', '41234323414', '', '', 'John', 'Rodman', '', '135 South Park Avenue', '', 'Los Angeles', 'CA 90024', 'United States', 223, 'California', 3624, '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', '[]', 'Cash On Delivery', 'cod', 'John', 'Rodman', '', '135 South Park Avenue', '', 'Los Angeles', 'CA 90024', 'United States', 223, 'California', 3624, '{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}', '[]', 'Flat Shipping Rate', 'flat.flat', '', '21362.0000', 1, 0, '0.0000', 0, '', 1, 2, 'USD', '1.00000000', '127.0.0.1', '', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:54.0) Gecko/20100101 Firefox/54.0', 'pl,en-US;q=0.7,en;q=0.3', '2017-08-02 11:53:12', '2017-08-02 11:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_history`
--

CREATE TABLE `oc_order_history` (
  `order_history_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_history`
--

INSERT INTO `oc_order_history` (`order_history_id`, `order_id`, `order_status_id`, `notify`, `comment`, `date_added`) VALUES
(1, 1, 1, 0, '', '2017-08-02 11:53:16'),
(2, 1, 1, 0, '', '2017-08-02 11:53:21');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_option`
--

CREATE TABLE `oc_order_option` (
  `order_option_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `order_product_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_option_value_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_product`
--

CREATE TABLE `oc_order_product` (
  `order_product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `tax` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `reward` int(8) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_product`
--

INSERT INTO `oc_order_product` (`order_product_id`, `order_id`, `product_id`, `name`, `model`, `quantity`, `price`, `total`, `tax`, `reward`) VALUES
(1, 1, 86, 'Sample Top', 'RES.343', 200, '17.9000', '3580.0000', '0.0000', 0),
(2, 1, 84, 'Basic T-Shirt', 'RES.340', 180, '18.9000', '3402.0000', '0.0000', 0),
(3, 1, 82, 'Basic T-Shirt', 'RES.336', 170, '16.9000', '2873.0000', '0.0000', 0),
(4, 1, 80, 'Printed T-Shirt', 'RES.332', 160, '18.9000', '3024.0000', '0.0000', 0),
(5, 1, 79, 'Printed T-Shirt', 'RES.329', 150, '19.9000', '2985.0000', '0.0000', 0),
(6, 1, 87, 'Printed Top', 'RES.345', 140, '18.9000', '2646.0000', '0.0000', 0),
(7, 1, 78, 'Sample T-Shirt', 'RES.327', 130, '21.9000', '2847.0000', '0.0000', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring`
--

CREATE TABLE `oc_order_recurring` (
  `order_recurring_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `recurring_name` varchar(255) NOT NULL,
  `recurring_description` varchar(255) NOT NULL,
  `recurring_frequency` varchar(25) NOT NULL,
  `recurring_cycle` smallint(6) NOT NULL,
  `recurring_duration` smallint(6) NOT NULL,
  `recurring_price` decimal(10,4) NOT NULL,
  `trial` tinyint(1) NOT NULL,
  `trial_frequency` varchar(25) NOT NULL,
  `trial_cycle` smallint(6) NOT NULL,
  `trial_duration` smallint(6) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_recurring_transaction`
--

CREATE TABLE `oc_order_recurring_transaction` (
  `order_recurring_transaction_id` int(11) NOT NULL,
  `order_recurring_id` int(11) NOT NULL,
  `reference` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `amount` decimal(10,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_shipment`
--

CREATE TABLE `oc_order_shipment` (
  `order_shipment_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `shipping_courier_id` varchar(255) NOT NULL DEFAULT '',
  `tracking_number` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_status`
--

CREATE TABLE `oc_order_status` (
  `order_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_status`
--

INSERT INTO `oc_order_status` (`order_status_id`, `language_id`, `name`) VALUES
(2, 1, 'Processing'),
(3, 1, 'Shipped'),
(7, 1, 'Canceled'),
(5, 1, 'Complete'),
(8, 1, 'Denied'),
(9, 1, 'Canceled Reversal'),
(10, 1, 'Failed'),
(11, 1, 'Refunded'),
(12, 1, 'Reversed'),
(13, 1, 'Chargeback'),
(1, 1, 'Pending'),
(16, 1, 'Voided'),
(15, 1, 'Processed'),
(14, 1, 'Expired'),
(2, 2, 'Processing'),
(3, 2, 'Shipped'),
(7, 2, 'Canceled'),
(5, 2, 'Complete'),
(8, 2, 'Denied'),
(9, 2, 'Canceled Reversal'),
(10, 2, 'Failed'),
(11, 2, 'Refunded'),
(12, 2, 'Reversed'),
(13, 2, 'Chargeback'),
(1, 2, 'Pending'),
(16, 2, 'Voided'),
(15, 2, 'Processed'),
(14, 2, 'Expired');

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_total`
--

CREATE TABLE `oc_order_total` (
  `order_total_id` int(10) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `sort_order` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_order_total`
--

INSERT INTO `oc_order_total` (`order_total_id`, `order_id`, `code`, `title`, `value`, `sort_order`) VALUES
(1, 1, 'sub_total', 'Sub-Total', '21357.0000', 1),
(2, 1, 'shipping', 'Flat Shipping Rate', '5.0000', 3),
(3, 1, 'total', 'Total', '21362.0000', 9);

-- --------------------------------------------------------

--
-- Table structure for table `oc_order_voucher`
--

CREATE TABLE `oc_order_voucher` (
  `order_voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product`
--

CREATE TABLE `oc_product` (
  `product_id` int(11) NOT NULL,
  `model` varchar(64) NOT NULL,
  `sku` varchar(64) NOT NULL,
  `upc` varchar(12) NOT NULL,
  `ean` varchar(14) NOT NULL,
  `jan` varchar(13) NOT NULL,
  `isbn` varchar(17) NOT NULL,
  `mpn` varchar(64) NOT NULL,
  `location` varchar(128) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `stock_status_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `shipping` tinyint(1) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `points` int(8) NOT NULL DEFAULT '0',
  `tax_class_id` int(11) NOT NULL,
  `date_available` date NOT NULL DEFAULT '0000-00-00',
  `weight` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `weight_class_id` int(11) NOT NULL DEFAULT '0',
  `length` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `width` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `height` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `length_class_id` int(11) NOT NULL DEFAULT '0',
  `subtract` tinyint(1) NOT NULL DEFAULT '1',
  `minimum` int(11) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `viewed` int(5) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product`
--

INSERT INTO `oc_product` (`product_id`, `model`, `sku`, `upc`, `ean`, `jan`, `isbn`, `mpn`, `location`, `quantity`, `stock_status_id`, `image`, `manufacturer_id`, `shipping`, `price`, `points`, `tax_class_id`, `date_available`, `weight`, `weight_class_id`, `length`, `width`, `height`, `length_class_id`, `subtract`, `minimum`, `sort_order`, `status`, `viewed`, `date_added`, `date_modified`) VALUES
(66, 'RES.801', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_5.jpg', 13, 1, '28.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 10, 1, 2, '2017-07-13 15:18:01', '2017-09-18 13:31:24'),
(63, 'PLM.257', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/aurora_plush_mimi_1.jpg', 12, 1, '43.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 2, '2017-07-13 15:01:01', '2017-07-13 15:03:42'),
(64, 'RES.115', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_3.jpg', 11, 1, '37.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 12, 1, 3, '2017-07-13 15:11:29', '2017-09-18 13:28:04'),
(65, 'RES.963', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_4.jpg', 12, 1, '22.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 11, 1, 63, '2017-07-13 15:16:18', '2017-09-18 13:30:19'),
(54, 'APM.357', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_electronics/c7c1a6e43af3.jpg', 15, 1, '1599.0000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 2, 1, 3, '2017-07-13 14:23:48', '2017-07-13 14:35:53'),
(55, 'PQE.153', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_1.jpg', 14, 1, '799.0000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 17, '2017-07-13 14:26:37', '2017-09-19 15:40:34'),
(56, 'MAG.389', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/emmaljunga_2.jpg', 11, 1, '1099.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 8, 1, 3, '2017-07-13 14:33:05', '2017-07-13 14:35:24'),
(57, 'VME.305', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/fisher_1.jpg', 12, 1, '59.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 7, 1, 4, '2017-07-13 14:37:01', '2017-07-13 14:41:12'),
(58, 'OUI.410', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/angelcare_1.jpg', 13, 1, '89.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 6, 1, 3, '2017-07-13 14:39:37', '2017-07-13 17:55:34'),
(59, 'FDE.783', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/happy_trails_lamb_plush_1.jpg', 14, 1, '159.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 5, 1, 2, '2017-07-13 14:41:31', '2017-07-13 14:43:00'),
(60, 'DCA.178', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/lamb_baby_hat_1.jpg', 15, 1, '34.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 4, 1, 2, '2017-07-13 14:44:25', '2017-07-13 14:45:49'),
(61, 'ADL.547', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/chicco_plush_1.jpg', 14, 1, '27.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 3, 1, 3, '2017-07-13 14:46:10', '2017-07-13 14:47:33'),
(62, 'MAG.389', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_kids/emmaljunga_1.jpg', 11, 1, '1099.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 2, 1, 3, '2017-07-13 14:48:56', '2017-07-13 14:50:36'),
(50, 'APM.353', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_electronics/iphone-5.jpg', 11, 1, '1599.0000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 6, 1, 13, '2017-07-12 18:38:11', '2017-07-13 13:33:44'),
(51, 'HPS.190', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_6.jpg', 12, 1, '439.0000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 5, 1, 24, '2017-07-13 13:21:59', '2017-10-04 14:30:12'),
(52, 'MPL.457', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_electronics/apple_ipod_touch_1d.jpg', 13, 1, '199.0000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 4, 1, 4, '2017-07-13 13:39:39', '2017-07-13 14:20:08'),
(53, 'AQM.978', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_electronics/apple_imac_1.jpg', 13, 1, '1359.0000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 3, 1, 5, '2017-07-13 14:20:27', '2017-07-13 14:23:37'),
(67, 'RES.443', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_6.jpg', 14, 1, '32.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 9, 1, 2, '2017-07-13 15:19:39', '2017-09-18 13:32:20'),
(68, 'RES.168', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_1.jpg', 15, 1, '19.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 8, 1, 2, '2017-07-13 15:23:29', '2017-09-18 13:34:19'),
(69, 'RES.167', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_2.jpg', 11, 1, '21.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 7, 1, 3, '2017-07-13 15:25:11', '2017-09-18 13:40:08'),
(70, 'RES.896', '', '', '', '', '', '', '', 0, 5, 'catalog/products_organic/organic_prod_3.jpg', 12, 1, '23.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 6, 1, 89, '2017-07-13 15:27:35', '2017-09-18 13:41:56'),
(71, 'RES.921', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_4.jpg', 14, 1, '18.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 5, 1, 3, '2017-07-13 15:30:11', '2017-09-18 13:43:19'),
(72, 'RES.745', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_5.jpg', 15, 1, '19.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 4, 1, 2, '2017-07-13 15:33:42', '2017-09-18 13:45:55'),
(73, 'RES.193', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_6.jpg', 11, 1, '21.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 3, 1, 2, '2017-07-13 15:35:57', '2017-09-18 13:47:53'),
(74, 'RES.268', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_1.jpg', 12, 1, '17.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 2, 1, 110, '2017-07-13 15:38:19', '2017-09-18 13:51:36'),
(75, 'RES.175', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_2.jpg', 12, 1, '18.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 4, '2017-07-13 15:40:28', '2017-09-18 13:53:20'),
(76, 'RES.322', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_2.jpg', 11, 1, '17.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 1, 1, 279, '2017-07-13 15:59:19', '2017-09-18 13:21:10'),
(77, 'RES.325', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_1.jpg', 12, 1, '17.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 2, 1, 3, '2017-07-13 17:07:13', '2017-09-18 13:18:13'),
(78, 'RES.327', '', '', '', '', '', '', '', 2147483516, 5, 'catalog/products_organic/organic_prod_6.jpg', 13, 1, '21.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 3, 1, 8, '2017-07-13 17:09:59', '2017-09-18 13:16:53'),
(79, 'RES.329', '', '', '', '', '', '', '', 2147483496, 5, 'catalog/products_organic/organic_prod_5.jpg', 14, 1, '19.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 4, 1, 11, '2017-07-13 17:12:10', '2017-09-18 13:07:23'),
(80, 'RES.332', '', '', '', '', '', '', '', 2147483486, 5, 'catalog/products_organic/organic_prod_4.jpg', 15, 1, '18.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 5, 1, 5, '2017-07-13 17:14:05', '2017-09-18 13:06:07'),
(81, 'RES.334', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_3.jpg', 11, 1, '23.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 6, 1, 22, '2017-07-13 17:16:47', '2017-09-18 13:00:16'),
(82, 'RES.336', '', '', '', '', '', '', '', 2147483476, 5, 'catalog/products_organic/organic_prod_2.jpg', 12, 1, '18.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 7, 1, 95, '2017-07-13 17:19:03', '2017-09-18 12:58:49'),
(83, 'RES.338', '', '', '', '', '', '', '', 0, 5, 'catalog/products_organic/organic_prod_1.jpg', 13, 1, '19.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 8, 1, 3, '2017-07-13 17:20:49', '2017-09-18 12:57:18'),
(84, 'RES.340', '', '', '', '', '', '', '', 2147483466, 5, 'catalog/products_organic/organic_prod_6.jpg', 14, 1, '18.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 9, 1, 77, '2017-07-13 17:25:53', '2017-09-15 18:56:45'),
(85, 'RES.342', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_5.jpg', 15, 1, '21.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 10, 1, 56, '2017-07-13 17:27:40', '2017-09-15 18:48:19'),
(86, 'RES.343', '', '', '', '', '', '', '', 2147483446, 5, 'catalog/products_organic/organic_prod_4.jpg', 11, 1, '17.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 11, 1, 10, '2017-07-13 17:29:54', '2017-09-15 18:27:24'),
(87, 'RES.345', '', '', '', '', '', '', '', 2147483506, 5, 'catalog/products_organic/organic_prod_3.jpg', 12, 1, '18.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 12, 1, 36, '2017-07-13 17:31:39', '2017-09-15 18:16:37'),
(88, 'RES.117', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_2.jpg', 12, 1, '21.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 13, 1, 10, '2017-07-13 17:34:11', '2017-09-15 17:51:51'),
(89, 'RES.347', '', '', '', '', '', '', '', 2147483646, 5, 'catalog/products_organic/organic_prod_1.jpg', 13, 1, '23.9000', 0, 0, '2017-07-12', '0.00000000', 1, '0.00000000', '0.00000000', '0.00000000', 1, 1, 1, 13, 1, 14, '2017-07-13 17:36:02', '2017-09-15 17:52:18');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_attribute`
--

CREATE TABLE `oc_product_attribute` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_attribute`
--

INSERT INTO `oc_product_attribute` (`product_id`, `attribute_id`, `language_id`, `text`) VALUES
(50, 25, 1, 'Regular fit - True to Size'),
(50, 24, 1, 'Machine Wash'),
(50, 23, 1, '20%'),
(50, 22, 1, '80%'),
(51, 23, 2, '20%'),
(51, 24, 1, 'Machine Wash'),
(51, 24, 2, 'Machine Wash'),
(52, 25, 1, 'Regular fit - True to Size'),
(52, 24, 1, 'Machine Wash'),
(52, 22, 1, '80%'),
(52, 23, 1, '20%'),
(53, 23, 1, '20%'),
(53, 24, 1, 'Machine Wash'),
(53, 25, 1, 'Regular fit - True to Size'),
(53, 22, 1, '80%'),
(54, 23, 1, '20%'),
(54, 24, 1, 'Machine Wash'),
(54, 25, 1, 'Regular fit - True to Size'),
(54, 22, 1, '80%'),
(55, 23, 2, '20%'),
(55, 24, 1, 'Machine Wash'),
(55, 24, 2, 'Machine Wash'),
(56, 24, 1, 'Machine Wash'),
(56, 23, 1, '20%'),
(56, 22, 1, '80%'),
(56, 25, 1, 'Regular fit - True to Size'),
(57, 23, 1, '20%'),
(57, 24, 1, 'Machine Wash'),
(57, 25, 1, 'Regular fit - True to Size'),
(57, 22, 1, '80%'),
(58, 23, 1, '20%'),
(58, 24, 1, 'Machine Wash'),
(58, 25, 1, 'Regular fit - True to Size'),
(58, 22, 1, '80%'),
(59, 24, 1, 'Machine Wash'),
(59, 23, 1, '20%'),
(59, 22, 1, '80%'),
(59, 25, 1, 'Regular fit - True to Size'),
(60, 24, 1, 'Machine Wash'),
(60, 23, 1, '20%'),
(60, 22, 1, '80%'),
(60, 25, 1, 'Regular fit - True to Size'),
(61, 24, 1, 'Machine Wash'),
(61, 23, 1, '20%'),
(61, 22, 1, '80%'),
(61, 25, 1, 'Regular fit - True to Size'),
(62, 24, 1, 'Machine Wash'),
(62, 23, 1, '20%'),
(62, 22, 1, '80%'),
(62, 25, 1, 'Regular fit - True to Size'),
(63, 24, 1, 'Machine Wash'),
(63, 23, 1, '20%'),
(63, 22, 1, '80%'),
(63, 25, 1, 'Regular fit - True to Size'),
(64, 24, 1, 'Machine Wash'),
(64, 24, 2, 'Machine Wash'),
(64, 22, 2, '80%'),
(65, 24, 1, 'Machine Wash'),
(65, 24, 2, 'Machine Wash'),
(65, 22, 2, '80%'),
(66, 24, 2, 'Machine Wash'),
(66, 23, 1, '20%'),
(66, 23, 2, '20%'),
(67, 24, 1, 'Machine Wash'),
(67, 24, 2, 'Machine Wash'),
(67, 22, 2, '80%'),
(68, 24, 1, 'Machine Wash'),
(68, 24, 2, 'Machine Wash'),
(68, 22, 2, '80%'),
(69, 24, 1, 'Machine Wash'),
(69, 23, 1, '20%'),
(69, 23, 2, '20%'),
(70, 24, 1, 'Machine Wash'),
(70, 23, 2, '20%'),
(70, 22, 2, '80%'),
(70, 23, 1, '20%'),
(71, 23, 2, '20%'),
(71, 24, 1, 'Machine Wash'),
(71, 24, 2, 'Machine Wash'),
(72, 24, 1, 'Machine Wash'),
(72, 24, 2, 'Machine Wash'),
(72, 22, 2, '80%'),
(73, 24, 1, 'Machine Wash'),
(73, 24, 2, 'Machine Wash'),
(73, 22, 2, '80%'),
(74, 24, 2, 'Machine Wash'),
(74, 23, 1, '20%'),
(74, 23, 2, '20%'),
(75, 24, 2, 'Machine Wash'),
(75, 23, 1, '20%'),
(75, 23, 2, '20%'),
(76, 23, 2, '20%'),
(76, 24, 1, 'Machine Wash'),
(76, 24, 2, 'Machine Wash'),
(77, 24, 1, 'Machine Wash'),
(77, 24, 2, 'Machine Wash'),
(77, 22, 2, '80%'),
(78, 24, 1, 'Machine Wash'),
(78, 23, 2, '20%'),
(78, 23, 1, '20%'),
(79, 24, 1, 'Machine Wash'),
(79, 24, 2, 'Machine Wash'),
(79, 22, 2, '80%'),
(80, 23, 2, '20%'),
(80, 22, 2, '80%'),
(81, 24, 2, 'Machine Wash'),
(81, 23, 1, '20%'),
(81, 23, 2, '20%'),
(82, 24, 1, 'Machine Wash'),
(82, 24, 2, 'Machine Wash'),
(82, 22, 2, '80%'),
(83, 23, 2, '20%'),
(83, 24, 1, 'Machine Wash'),
(83, 24, 2, 'Machine Wash'),
(84, 24, 1, 'Machine Wash'),
(84, 23, 2, '20%'),
(84, 23, 1, '20%'),
(85, 24, 1, 'Machine Wash'),
(85, 23, 1, '20%'),
(85, 23, 2, '20%'),
(86, 24, 1, 'Machine Wash'),
(86, 23, 1, '20%'),
(86, 23, 2, '20%'),
(87, 24, 1, 'Machine Wash'),
(87, 23, 1, '20%'),
(87, 23, 2, '20%'),
(88, 24, 1, 'Machine Wash'),
(88, 23, 1, '20%'),
(88, 23, 2, '20%'),
(89, 24, 1, 'Machine Wash'),
(89, 24, 2, 'Machine Wash'),
(89, 25, 1, 'Regular fit - True to Size'),
(50, 25, 2, 'Regular fit - True to Size'),
(50, 24, 2, 'Machine Wash'),
(50, 23, 2, '20%'),
(50, 22, 2, '80%'),
(51, 22, 1, '80%'),
(51, 22, 2, '80%'),
(51, 23, 1, '20%'),
(52, 25, 2, 'Regular fit - True to Size'),
(52, 24, 2, 'Machine Wash'),
(52, 22, 2, '80%'),
(52, 23, 2, '20%'),
(53, 23, 2, '20%'),
(53, 24, 2, 'Machine Wash'),
(53, 25, 2, 'Regular fit - True to Size'),
(53, 22, 2, '80%'),
(54, 23, 2, '20%'),
(54, 24, 2, 'Machine Wash'),
(54, 25, 2, 'Regular fit - True to Size'),
(54, 22, 2, '80%'),
(55, 22, 1, '80%'),
(55, 22, 2, '80%'),
(55, 23, 1, '20%'),
(56, 24, 2, 'Machine Wash'),
(56, 23, 2, '20%'),
(56, 22, 2, '80%'),
(56, 25, 2, 'Regular fit - True to Size'),
(57, 23, 2, '20%'),
(57, 24, 2, 'Machine Wash'),
(57, 25, 2, 'Regular fit - True to Size'),
(57, 22, 2, '80%'),
(58, 23, 2, '20%'),
(58, 24, 2, 'Machine Wash'),
(58, 25, 2, 'Regular fit - True to Size'),
(58, 22, 2, '80%'),
(59, 24, 2, 'Machine Wash'),
(59, 23, 2, '20%'),
(59, 22, 2, '80%'),
(59, 25, 2, 'Regular fit - True to Size'),
(60, 24, 2, 'Machine Wash'),
(60, 23, 2, '20%'),
(60, 22, 2, '80%'),
(60, 25, 2, 'Regular fit - True to Size'),
(61, 24, 2, 'Machine Wash'),
(61, 23, 2, '20%'),
(61, 22, 2, '80%'),
(61, 25, 2, 'Regular fit - True to Size'),
(62, 24, 2, 'Machine Wash'),
(62, 23, 2, '20%'),
(62, 22, 2, '80%'),
(62, 25, 2, 'Regular fit - True to Size'),
(63, 24, 2, 'Machine Wash'),
(63, 23, 2, '20%'),
(63, 22, 2, '80%'),
(63, 25, 2, 'Regular fit - True to Size'),
(64, 23, 1, '20%'),
(64, 23, 2, '20%'),
(64, 22, 1, '80%'),
(65, 23, 1, '20%'),
(65, 23, 2, '20%'),
(65, 22, 1, '80%'),
(66, 25, 1, 'Regular fit - True to Size'),
(66, 24, 1, 'Machine Wash'),
(66, 22, 1, '80%'),
(66, 22, 2, '80%'),
(67, 23, 1, '20%'),
(67, 23, 2, '20%'),
(67, 22, 1, '80%'),
(68, 23, 1, '20%'),
(68, 23, 2, '20%'),
(68, 22, 1, '80%'),
(69, 25, 2, 'Regular fit - True to Size'),
(69, 24, 2, 'Machine Wash'),
(69, 25, 1, 'Regular fit - True to Size'),
(70, 25, 1, 'Regular fit - True to Size'),
(70, 24, 2, 'Machine Wash'),
(71, 22, 1, '80%'),
(71, 22, 2, '80%'),
(71, 23, 1, '20%'),
(72, 23, 1, '20%'),
(72, 23, 2, '20%'),
(72, 22, 1, '80%'),
(73, 23, 1, '20%'),
(73, 23, 2, '20%'),
(73, 22, 1, '80%'),
(74, 25, 1, 'Regular fit - True to Size'),
(74, 24, 1, 'Machine Wash'),
(74, 22, 1, '80%'),
(74, 22, 2, '80%'),
(75, 25, 1, 'Regular fit - True to Size'),
(75, 24, 1, 'Machine Wash'),
(75, 22, 1, '80%'),
(75, 22, 2, '80%'),
(76, 22, 1, '80%'),
(76, 22, 2, '80%'),
(76, 23, 1, '20%'),
(77, 23, 1, '20%'),
(77, 23, 2, '20%'),
(77, 22, 1, '80%'),
(78, 25, 1, 'Regular fit - True to Size'),
(78, 24, 2, 'Machine Wash'),
(79, 23, 1, '20%'),
(79, 23, 2, '20%'),
(79, 22, 1, '80%'),
(80, 24, 1, 'Machine Wash'),
(80, 23, 1, '20%'),
(80, 22, 1, '80%'),
(81, 25, 1, 'Regular fit - True to Size'),
(81, 24, 1, 'Machine Wash'),
(81, 22, 1, '80%'),
(81, 22, 2, '80%'),
(80, 24, 2, 'Machine Wash'),
(82, 23, 1, '20%'),
(82, 23, 2, '20%'),
(82, 22, 1, '80%'),
(83, 22, 1, '80%'),
(83, 22, 2, '80%'),
(83, 23, 1, '20%'),
(84, 25, 1, 'Regular fit - True to Size'),
(84, 24, 2, 'Machine Wash'),
(85, 25, 2, 'Regular fit - True to Size'),
(85, 24, 2, 'Machine Wash'),
(85, 25, 1, 'Regular fit - True to Size'),
(86, 25, 2, 'Regular fit - True to Size'),
(86, 24, 2, 'Machine Wash'),
(86, 25, 1, 'Regular fit - True to Size'),
(87, 25, 2, 'Regular fit - True to Size'),
(87, 24, 2, 'Machine Wash'),
(87, 25, 1, 'Regular fit - True to Size'),
(88, 25, 2, 'Regular fit - True to Size'),
(88, 24, 2, 'Machine Wash'),
(88, 25, 1, 'Regular fit - True to Size'),
(89, 22, 2, '80%'),
(89, 23, 1, '20%'),
(89, 23, 2, '20%'),
(70, 22, 1, '80%'),
(89, 25, 2, 'Regular fit - True to Size'),
(89, 22, 1, '80%'),
(88, 22, 2, '80%'),
(88, 22, 1, '80%'),
(87, 22, 2, '80%'),
(87, 22, 1, '80%'),
(86, 22, 2, '80%'),
(86, 22, 1, '80%'),
(85, 22, 2, '80%'),
(85, 22, 1, '80%'),
(84, 22, 2, '80%'),
(84, 22, 1, '80%'),
(84, 25, 2, 'Regular fit - True to Size'),
(83, 25, 1, 'Regular fit - True to Size'),
(83, 25, 2, 'Regular fit - True to Size'),
(82, 25, 1, 'Regular fit - True to Size'),
(82, 25, 2, 'Regular fit - True to Size'),
(81, 25, 2, 'Regular fit - True to Size'),
(80, 25, 1, 'Regular fit - True to Size'),
(80, 25, 2, 'Regular fit - True to Size'),
(79, 25, 1, 'Regular fit - True to Size'),
(79, 25, 2, 'Regular fit - True to Size'),
(78, 22, 2, '80%'),
(78, 22, 1, '80%'),
(78, 25, 2, 'Regular fit - True to Size'),
(77, 25, 1, 'Regular fit - True to Size'),
(77, 25, 2, 'Regular fit - True to Size'),
(76, 25, 1, 'Regular fit - True to Size'),
(76, 25, 2, 'Regular fit - True to Size'),
(64, 25, 1, 'Regular fit - True to Size'),
(64, 25, 2, 'Regular fit - True to Size'),
(65, 25, 1, 'Regular fit - True to Size'),
(65, 25, 2, 'Regular fit - True to Size'),
(66, 25, 2, 'Regular fit - True to Size'),
(67, 25, 1, 'Regular fit - True to Size'),
(67, 25, 2, 'Regular fit - True to Size'),
(68, 25, 1, 'Regular fit - True to Size'),
(68, 25, 2, 'Regular fit - True to Size'),
(69, 22, 2, '80%'),
(69, 22, 1, '80%'),
(70, 25, 2, 'Regular fit - True to Size'),
(71, 25, 1, 'Regular fit - True to Size'),
(71, 25, 2, 'Regular fit - True to Size'),
(72, 25, 1, 'Regular fit - True to Size'),
(72, 25, 2, 'Regular fit - True to Size'),
(73, 25, 1, 'Regular fit - True to Size'),
(73, 25, 2, 'Regular fit - True to Size'),
(74, 25, 2, 'Regular fit - True to Size'),
(75, 25, 2, 'Regular fit - True to Size'),
(55, 25, 1, 'Regular fit - True to Size'),
(55, 25, 2, 'Regular fit - True to Size'),
(51, 25, 1, 'Regular fit - True to Size'),
(51, 25, 2, 'Regular fit - True to Size');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_description`
--

CREATE TABLE `oc_product_description` (
  `product_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `tag` text NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_description`
--

INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(70, 1, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(71, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(72, 2, 'Sample T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample T-Shirt', '', ''),
(73, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(74, 2, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(75, 2, 'Sample T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample T-Shirt', '', ''),
(76, 2, 'Ladies Singlet', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Ladies Singlet', '', ''),
(77, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(78, 1, 'Sample T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample T-Shirt', '', ''),
(67, 2, 'Jogging Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Jogging Top', '', ''),
(68, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(50, 1, 'Sample Smartphone', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Smartphone', '', ''),
(51, 2, 'Sample Headphones', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Headphones', '', ''),
(52, 1, 'Sample Player', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Player', '', ''),
(53, 1, 'Sample Computer', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Computer', '', ''),
(54, 1, 'Sample Smartphone', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Smartphone', '', ''),
(55, 2, 'Sample Camera', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Camera', '', ''),
(56, 1, 'Sample Stroller', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Stroller', '', ''),
(57, 1, 'Sample Toy', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Toy', '', ''),
(58, 1, 'Sample Monitor', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Monitor', '', ''),
(59, 1, 'Rocking Animal', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Rocking Animal', '', ''),
(60, 1, 'Sample Baby Hat', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Baby Hat', '', ''),
(61, 1, 'Sample Plush', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Plush', '', ''),
(62, 1, 'Sample Stroller', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Stroller', '', ''),
(63, 1, 'Sample Plush', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Plush', '', ''),
(64, 2, 'Basic Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic Top', '', ''),
(65, 2, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(66, 2, 'Hooded Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Hooded Top', '', ''),
(69, 1, 'Sample Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Top', '', ''),
(79, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(81, 2, 'Sample Singlet', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Singlet', '', ''),
(80, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(82, 2, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(83, 2, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(84, 1, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(85, 1, 'Basic Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic Top', '', ''),
(86, 1, 'Sample Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Top', '', ''),
(87, 1, 'Printed Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed Top', '', ''),
(88, 1, 'Sample Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Top', '', ''),
(89, 2, 'Sample Singlet', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Singlet', '', ''),
(70, 2, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(71, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(72, 1, 'Sample T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample T-Shirt', '', ''),
(73, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(74, 1, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(75, 1, 'Sample T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample T-Shirt', '', ''),
(76, 1, 'Ladies Singlet', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Ladies Singlet', '', ''),
(77, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(78, 2, 'Sample T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample T-Shirt', '', ''),
(67, 1, 'Jogging Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Jogging Top', '', '');
INSERT INTO `oc_product_description` (`product_id`, `language_id`, `name`, `description`, `tag`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(68, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(50, 2, 'Sample Smartphone', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Smartphone', '', ''),
(51, 1, 'Sample Headphones', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Headphones', '', ''),
(52, 2, 'Sample Player', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Player', '', ''),
(53, 2, 'Sample Computer', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Computer', '', ''),
(54, 2, 'Sample Smartphone', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Smartphone', '', ''),
(55, 1, 'Sample Camera', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Camera', '', ''),
(56, 2, 'Sample Stroller', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Stroller', '', ''),
(57, 2, 'Sample Toy', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Toy', '', ''),
(58, 2, 'Sample Monitor', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Monitor', '', ''),
(59, 2, 'Rocking Animal', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Rocking Animal', '', ''),
(60, 2, 'Sample Baby Hat', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Baby Hat', '', ''),
(61, 2, 'Sample Plush', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Plush', '', ''),
(62, 2, 'Sample Stroller', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Stroller', '', ''),
(63, 2, 'Sample Plush', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Plush', '', ''),
(64, 1, 'Basic Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic Top', '', ''),
(65, 1, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(66, 1, 'Hooded Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Hooded Top', '', ''),
(69, 2, 'Sample Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Top', '', ''),
(79, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(81, 1, 'Sample Singlet', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Singlet', '', ''),
(80, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(82, 1, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(83, 1, 'Printed T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed T-Shirt', '', ''),
(84, 2, 'Basic T-Shirt', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic T-Shirt', '', ''),
(85, 2, 'Basic Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Basic Top', '', ''),
(86, 2, 'Sample Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Top', '', ''),
(87, 2, 'Printed Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Printed Top', '', ''),
(88, 2, 'Sample Top', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Top', '', ''),
(89, 1, 'Sample Singlet', '&lt;p&gt;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit. Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla.&lt;/p&gt;', 'lorem,ipsum,dolor,sit,amet,consectetur,adipiscing,elit', 'Sample Singlet', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_discount`
--

CREATE TABLE `oc_product_discount` (
  `product_discount_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `quantity` int(4) NOT NULL DEFAULT '0',
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_filter`
--

CREATE TABLE `oc_product_filter` (
  `product_id` int(11) NOT NULL,
  `filter_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_filter`
--

INSERT INTO `oc_product_filter` (`product_id`, `filter_id`) VALUES
(50, 1),
(50, 2),
(50, 3),
(50, 4),
(50, 5),
(50, 10),
(51, 1),
(51, 2),
(51, 3),
(51, 4),
(51, 5),
(51, 10),
(51, 11),
(51, 12),
(51, 13),
(52, 1),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(52, 9),
(52, 11),
(52, 12),
(52, 13),
(52, 14),
(53, 1),
(53, 2),
(53, 3),
(53, 4),
(53, 5),
(53, 10),
(53, 11),
(53, 12),
(53, 13),
(53, 14),
(54, 1),
(54, 2),
(54, 3),
(54, 4),
(54, 5),
(54, 10),
(54, 11),
(54, 12),
(54, 13),
(54, 14),
(55, 1),
(55, 2),
(55, 3),
(55, 4),
(55, 5),
(55, 10),
(55, 11),
(55, 12),
(55, 13),
(55, 14),
(56, 1),
(56, 2),
(56, 3),
(56, 4),
(56, 5),
(56, 10),
(56, 11),
(56, 12),
(56, 13),
(56, 14),
(56, 15),
(57, 1),
(57, 2),
(57, 3),
(57, 4),
(57, 5),
(57, 8),
(57, 11),
(57, 12),
(57, 13),
(57, 14),
(57, 15),
(58, 1),
(58, 2),
(58, 3),
(58, 4),
(58, 5),
(58, 8),
(58, 11),
(58, 12),
(58, 13),
(58, 14),
(58, 15),
(59, 1),
(59, 2),
(59, 3),
(59, 4),
(59, 5),
(59, 9),
(59, 11),
(59, 12),
(59, 13),
(59, 14),
(59, 15),
(60, 1),
(60, 2),
(60, 3),
(60, 4),
(60, 5),
(60, 7),
(60, 11),
(60, 12),
(60, 13),
(60, 14),
(60, 15),
(61, 1),
(61, 2),
(61, 3),
(61, 4),
(61, 5),
(61, 7),
(61, 11),
(61, 12),
(61, 13),
(61, 14),
(61, 15),
(62, 1),
(62, 2),
(62, 3),
(62, 4),
(62, 5),
(62, 10),
(62, 11),
(62, 12),
(62, 13),
(62, 14),
(62, 15),
(63, 1),
(63, 2),
(63, 3),
(63, 4),
(63, 5),
(63, 7),
(63, 11),
(63, 12),
(63, 13),
(63, 14),
(63, 15),
(64, 1),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(64, 7),
(64, 11),
(64, 12),
(64, 13),
(64, 14),
(64, 15),
(65, 1),
(65, 2),
(65, 3),
(65, 4),
(65, 5),
(65, 7),
(65, 11),
(65, 12),
(65, 13),
(65, 14),
(65, 15),
(66, 1),
(66, 2),
(66, 3),
(66, 4),
(66, 5),
(66, 7),
(66, 11),
(66, 12),
(66, 13),
(66, 14),
(66, 15),
(67, 1),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(67, 7),
(67, 11),
(67, 12),
(67, 13),
(67, 14),
(67, 15),
(68, 1),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(68, 6),
(68, 11),
(68, 12),
(68, 13),
(68, 14),
(68, 15),
(69, 1),
(69, 2),
(69, 3),
(69, 4),
(69, 5),
(69, 6),
(69, 11),
(69, 12),
(69, 13),
(69, 14),
(69, 15),
(70, 1),
(70, 2),
(70, 3),
(70, 4),
(70, 5),
(70, 7),
(70, 11),
(70, 12),
(70, 13),
(70, 14),
(70, 15),
(71, 1),
(71, 2),
(71, 3),
(71, 4),
(71, 5),
(71, 6),
(71, 11),
(71, 12),
(71, 13),
(71, 14),
(71, 15),
(72, 1),
(72, 2),
(72, 3),
(72, 4),
(72, 5),
(72, 6),
(72, 11),
(72, 12),
(72, 13),
(72, 14),
(72, 15),
(73, 1),
(73, 2),
(73, 3),
(73, 4),
(73, 5),
(73, 7),
(73, 11),
(73, 12),
(73, 13),
(73, 14),
(73, 15),
(74, 1),
(74, 2),
(74, 3),
(74, 4),
(74, 5),
(74, 6),
(74, 11),
(74, 12),
(74, 13),
(74, 14),
(74, 15),
(75, 1),
(75, 2),
(75, 3),
(75, 4),
(75, 5),
(75, 6),
(75, 11),
(75, 12),
(75, 13),
(75, 14),
(75, 15),
(76, 1),
(76, 2),
(76, 3),
(76, 4),
(76, 5),
(76, 6),
(76, 11),
(76, 12),
(76, 13),
(76, 14),
(76, 15),
(77, 1),
(77, 2),
(77, 3),
(77, 4),
(77, 5),
(77, 6),
(77, 11),
(77, 12),
(77, 13),
(77, 14),
(77, 15),
(78, 1),
(78, 2),
(78, 3),
(78, 4),
(78, 5),
(78, 7),
(78, 11),
(78, 12),
(78, 13),
(78, 14),
(78, 15),
(79, 1),
(79, 2),
(79, 3),
(79, 4),
(79, 5),
(79, 6),
(79, 11),
(79, 12),
(79, 13),
(79, 14),
(79, 15),
(80, 1),
(80, 2),
(80, 3),
(80, 4),
(80, 5),
(80, 6),
(80, 11),
(80, 12),
(80, 13),
(80, 14),
(80, 15),
(81, 1),
(81, 2),
(81, 3),
(81, 4),
(81, 5),
(81, 6),
(81, 11),
(81, 12),
(81, 13),
(81, 14),
(81, 15),
(82, 1),
(82, 2),
(82, 3),
(82, 4),
(82, 5),
(82, 7),
(82, 11),
(82, 12),
(82, 13),
(82, 14),
(82, 15),
(83, 1),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(83, 6),
(83, 11),
(83, 12),
(83, 13),
(83, 14),
(83, 15),
(84, 1),
(84, 2),
(84, 3),
(84, 4),
(84, 5),
(84, 6),
(84, 11),
(84, 12),
(84, 13),
(84, 14),
(84, 15),
(85, 1),
(85, 2),
(85, 3),
(85, 4),
(85, 5),
(85, 6),
(85, 11),
(85, 12),
(85, 13),
(85, 14),
(85, 15),
(86, 1),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(86, 6),
(86, 11),
(86, 12),
(86, 13),
(86, 14),
(86, 15),
(87, 1),
(87, 2),
(87, 3),
(87, 4),
(87, 5),
(87, 6),
(87, 11),
(87, 12),
(87, 13),
(87, 14),
(87, 15),
(88, 1),
(88, 2),
(88, 3),
(88, 4),
(88, 5),
(88, 7),
(88, 11),
(88, 12),
(88, 13),
(88, 14),
(88, 15),
(89, 1),
(89, 2),
(89, 3),
(89, 4),
(89, 5),
(89, 7),
(89, 11),
(89, 12),
(89, 13),
(89, 14),
(89, 15);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_image`
--

CREATE TABLE `oc_product_image` (
  `product_image_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_image`
--

INSERT INTO `oc_product_image` (`product_image_id`, `product_id`, `image`, `sort_order`) VALUES
(2445, 60, 'catalog/products_kids/lamb_baby_hat_2c.jpg', 4),
(2444, 60, 'catalog/products_kids/lamb_baby_hat_1.jpg', 3),
(2443, 60, 'catalog/products_kids/lamb_baby_hat_2c.jpg', 2),
(2439, 59, 'catalog/products_kids/happy_trails_lamb_plush_2.jpg', 4),
(2438, 59, 'catalog/products_kids/happy_trails_lamb_plush_1.jpg', 3),
(2437, 59, 'catalog/products_kids/happy_trails_lamb_plush_2.jpg', 2),
(2661, 58, 'catalog/products_kids/angelcare_2.jpg', 3),
(2660, 58, 'catalog/products_kids/angelcare_3.jpg', 2),
(2434, 57, 'catalog/products_kids/fisher_4.jpg', 4),
(2433, 57, 'catalog/products_kids/fisher_2.jpg', 3),
(2418, 56, 'catalog/products_kids/emmaljunga_1.jpg', 3),
(2417, 56, 'catalog/products_kids/emmaljunga_3.jpg', 2),
(2781, 55, 'catalog/products_organic/organic_prod_1b.jpg', 4),
(2384, 52, 'catalog/products_electronics/apple_ipod_touch_4.jpg', 2),
(2385, 52, 'catalog/products_electronics/apple_ipod_touch_2.jpg', 3),
(2386, 52, 'catalog/products_electronics/apple_ipod_touch_3.jpg', 4),
(2421, 54, 'catalog/products_electronics/c7c1a6e43af3.jpg', 4),
(2420, 54, 'catalog/products_electronics/apple-iphone-5_1348748219.jpg', 3),
(2419, 54, 'catalog/products_electronics/iphone-5.jpg', 2),
(2397, 53, 'catalog/products_electronics/apple_imac_3.jpg', 5),
(2396, 53, 'catalog/products_electronics/apple_imac_5.jpg', 4),
(2394, 53, 'catalog/products_electronics/apple_imac_2.jpg', 2),
(2395, 53, 'catalog/products_electronics/apple_imac_3.jpg', 3),
(2432, 57, 'catalog/products_kids/fisher_3.jpg', 2),
(2780, 55, 'catalog/products_organic/organic_prod_1.jpg', 3),
(2783, 51, 'catalog/products_organic/organic_prod_6.jpg', 3),
(2374, 50, 'catalog/products_electronics/apple-iphone-5_1348748219.jpg', 3),
(2373, 50, 'catalog/products_electronics/c7c1a6e43af3.jpg', 2),
(2779, 55, 'catalog/products_organic/organic_prod_1b.jpg', 2),
(2782, 51, 'catalog/products_organic/organic_prod_6b.jpg', 2),
(2451, 61, 'catalog/products_kids/chicco_plush_2.jpg', 4),
(2450, 61, 'catalog/products_kids/chicco_plush_1.jpg', 3),
(2449, 61, 'catalog/products_kids/chicco_plush_2.jpg', 2),
(2456, 62, 'catalog/products_kids/emmaljunga_3.jpg', 3),
(2455, 62, 'catalog/products_kids/emmaljunga_2.jpg', 2),
(2459, 63, 'catalog/products_kids/aurora_plush_mimi_2.jpg', 2),
(2460, 63, 'catalog/products_kids/aurora_plush_mimi_1.jpg', 3),
(2461, 63, 'catalog/products_kids/aurora_plush_mimi_2.jpg', 4),
(2741, 64, 'catalog/products_organic/organic_prod_3.jpg', 3),
(2740, 64, 'catalog/products_organic/organic_prod_3b.jpg', 2),
(2744, 65, 'catalog/products_organic/organic_prod_4.jpg', 3),
(2743, 65, 'catalog/products_organic/organic_prod_4b.jpg', 2),
(2747, 66, 'catalog/products_organic/organic_prod_5.jpg', 3),
(2746, 66, 'catalog/products_organic/organic_prod_5b.jpg', 2),
(2750, 67, 'catalog/products_organic/organic_prod_6.jpg', 3),
(2749, 67, 'catalog/products_organic/organic_prod_6b.jpg', 2),
(2753, 68, 'catalog/products_organic/organic_prod_1.jpg', 3),
(2752, 68, 'catalog/products_organic/organic_prod_1b.jpg', 2),
(2760, 69, 'catalog/products_organic/organic_prod_2b.jpg', 4),
(2759, 69, 'catalog/products_organic/organic_prod_2.jpg', 3),
(2762, 70, 'catalog/products_organic/organic_prod_3.jpg', 3),
(2761, 70, 'catalog/products_organic/organic_prod_3b.jpg', 2),
(2765, 71, 'catalog/products_organic/organic_prod_4.jpg', 3),
(2764, 71, 'catalog/products_organic/organic_prod_4b.jpg', 2),
(2768, 72, 'catalog/products_organic/organic_prod_5.jpg', 3),
(2767, 72, 'catalog/products_organic/organic_prod_5b.jpg', 2),
(2771, 73, 'catalog/products_organic/organic_prod_6.jpg', 3),
(2770, 73, 'catalog/products_organic/organic_prod_6b.jpg', 2),
(2774, 74, 'catalog/products_organic/organic_prod_1.jpg', 3),
(2773, 74, 'catalog/products_organic/organic_prod_1b.jpg', 2),
(2777, 75, 'catalog/products_organic/organic_prod_2.jpg', 3),
(2776, 75, 'catalog/products_organic/organic_prod_2b.jpg', 2),
(2738, 76, 'catalog/products_organic/organic_prod_2.jpg', 3),
(2737, 76, 'catalog/products_organic/organic_prod_2b.jpg', 2),
(2735, 77, 'catalog/products_organic/organic_prod_1.jpg', 3),
(2734, 77, 'catalog/products_organic/organic_prod_1b.jpg', 2),
(2733, 78, 'catalog/products_organic/organic_prod_6b.jpg', 4),
(2732, 78, 'catalog/products_organic/organic_prod_6.jpg', 3),
(2726, 79, 'catalog/products_organic/organic_prod_5.jpg', 3),
(2725, 79, 'catalog/products_organic/organic_prod_5b.jpg', 2),
(2723, 80, 'catalog/products_organic/organic_prod_4.jpg', 3),
(2722, 80, 'catalog/products_organic/organic_prod_4b.jpg', 2),
(2720, 81, 'catalog/products_organic/organic_prod_3.jpg', 3),
(2719, 81, 'catalog/products_organic/organic_prod_3b.jpg', 2),
(2717, 82, 'catalog/products_organic/organic_prod_2.jpg', 3),
(2716, 82, 'catalog/products_organic/organic_prod_2b.jpg', 2),
(2714, 83, 'catalog/products_organic/organic_prod_1.jpg', 3),
(2713, 83, 'catalog/products_organic/organic_prod_1b.jpg', 2),
(2711, 84, 'catalog/products_organic/organic_prod_6.jpg', 3),
(2710, 84, 'catalog/products_organic/organic_prod_6b.jpg', 2),
(2705, 85, 'catalog/products_organic/organic_prod_5.jpg', 3),
(2704, 85, 'catalog/products_organic/organic_prod_5b.jpg', 2),
(2699, 86, 'catalog/products_organic/organic_prod_4.jpg', 3),
(2698, 86, 'catalog/products_organic/organic_prod_4b.jpg', 2),
(2693, 87, 'catalog/products_organic/organic_prod_3.jpg', 3),
(2692, 87, 'catalog/products_organic/organic_prod_3b.jpg', 2),
(2684, 88, 'catalog/products_organic/organic_prod_2.jpg', 3),
(2683, 88, 'catalog/products_organic/organic_prod_2b.jpg', 2),
(2687, 89, 'catalog/products_organic/organic_prod_1.jpg', 3),
(2688, 89, 'catalog/products_organic/organic_prod_1b.jpg', 4),
(2686, 89, 'catalog/products_organic/organic_prod_1b.jpg', 2),
(2685, 88, 'catalog/products_organic/organic_prod_2b.jpg', 4),
(2694, 87, 'catalog/products_organic/organic_prod_3b.jpg', 4),
(2700, 86, 'catalog/products_organic/organic_prod_4b.jpg', 4),
(2706, 85, 'catalog/products_organic/organic_prod_5b.jpg', 4),
(2712, 84, 'catalog/products_organic/organic_prod_6b.jpg', 4),
(2715, 83, 'catalog/products_organic/organic_prod_1b.jpg', 4),
(2718, 82, 'catalog/products_organic/organic_prod_2b.jpg', 4),
(2721, 81, 'catalog/products_organic/organic_prod_3b.jpg', 4),
(2724, 80, 'catalog/products_organic/organic_prod_4b.jpg', 4),
(2727, 79, 'catalog/products_organic/organic_prod_5b.jpg', 4),
(2731, 78, 'catalog/products_organic/organic_prod_6b.jpg', 2),
(2736, 77, 'catalog/products_organic/organic_prod_1b.jpg', 4),
(2739, 76, 'catalog/products_organic/organic_prod_2b.jpg', 4),
(2742, 64, 'catalog/products_organic/organic_prod_3b.jpg', 4),
(2745, 65, 'catalog/products_organic/organic_prod_4b.jpg', 4),
(2748, 66, 'catalog/products_organic/organic_prod_5b.jpg', 4),
(2751, 67, 'catalog/products_organic/organic_prod_6b.jpg', 4),
(2754, 68, 'catalog/products_organic/organic_prod_1b.jpg', 4),
(2758, 69, 'catalog/products_organic/organic_prod_2b.jpg', 2),
(2763, 70, 'catalog/products_organic/organic_prod_3b.jpg', 4),
(2766, 71, 'catalog/products_organic/organic_prod_4b.jpg', 4),
(2769, 72, 'catalog/products_organic/organic_prod_5b.jpg', 4),
(2772, 73, 'catalog/products_organic/organic_prod_6b.jpg', 4),
(2775, 74, 'catalog/products_organic/organic_prod_1b.jpg', 4),
(2778, 75, 'catalog/products_organic/organic_prod_2b.jpg', 4),
(2784, 51, 'catalog/products_organic/organic_prod_6b.jpg', 4);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option`
--

CREATE TABLE `oc_product_option` (
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `required` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_option`
--

INSERT INTO `oc_product_option` (`product_option_id`, `product_id`, `option_id`, `value`, `required`) VALUES
(235, 54, 11, '', 0),
(233, 53, 11, '', 0),
(227, 50, 13, '', 0),
(228, 50, 11, '', 0),
(232, 52, 11, '', 0),
(231, 52, 13, '', 0),
(230, 51, 11, '', 0),
(229, 51, 13, '', 0),
(234, 53, 13, '', 0),
(236, 54, 13, '', 0),
(237, 55, 13, '', 0),
(238, 55, 11, '', 0),
(240, 56, 11, '', 0),
(239, 56, 13, '', 0),
(241, 57, 11, '', 0),
(242, 57, 13, '', 0),
(243, 58, 13, '', 0),
(244, 58, 11, '', 0),
(246, 59, 13, '', 0),
(245, 59, 11, '', 0),
(248, 60, 11, '', 0),
(247, 60, 13, '', 0),
(250, 61, 13, '', 0),
(249, 61, 11, '', 0),
(252, 62, 11, '', 0),
(251, 62, 13, '', 0),
(254, 63, 13, '', 0),
(253, 63, 11, '', 0),
(255, 64, 13, '', 0),
(256, 64, 11, '', 0),
(257, 65, 11, '', 0),
(258, 65, 13, '', 0),
(260, 66, 11, '', 0),
(259, 66, 13, '', 0),
(261, 67, 11, '', 0),
(262, 67, 13, '', 0),
(263, 68, 13, '', 0),
(264, 68, 11, '', 0),
(266, 69, 13, '', 0),
(265, 69, 11, '', 0),
(268, 70, 11, '', 0),
(267, 70, 13, '', 0),
(270, 71, 13, '', 0),
(269, 71, 11, '', 0),
(271, 72, 11, '', 0),
(272, 72, 13, '', 0),
(273, 73, 13, '', 0),
(274, 73, 11, '', 0),
(276, 74, 13, '', 0),
(275, 74, 11, '', 0),
(278, 75, 13, '', 0),
(277, 75, 11, '', 0),
(279, 76, 11, '', 0),
(280, 76, 13, '', 0),
(281, 77, 13, '', 0),
(282, 77, 11, '', 0),
(284, 78, 13, '', 0),
(283, 78, 11, '', 0),
(285, 79, 13, '', 0),
(286, 79, 11, '', 0),
(288, 80, 13, '', 0),
(287, 80, 11, '', 0),
(290, 81, 13, '', 0),
(289, 81, 11, '', 0),
(291, 82, 11, '', 0),
(292, 82, 13, '', 0),
(293, 83, 13, '', 0),
(294, 83, 11, '', 0),
(296, 84, 13, '', 0),
(295, 84, 11, '', 0),
(298, 85, 13, '', 0),
(297, 85, 11, '', 0),
(300, 86, 11, '', 0),
(299, 86, 13, '', 0),
(302, 87, 13, '', 0),
(301, 87, 11, '', 0),
(304, 88, 13, '', 0),
(303, 88, 11, '', 0),
(305, 89, 13, '', 0),
(306, 89, 11, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_option_value`
--

CREATE TABLE `oc_product_option_value` (
  `product_option_value_id` int(11) NOT NULL,
  `product_option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `quantity` int(3) NOT NULL,
  `subtract` tinyint(1) NOT NULL,
  `price` decimal(15,4) NOT NULL,
  `price_prefix` varchar(1) NOT NULL,
  `points` int(8) NOT NULL,
  `points_prefix` varchar(1) NOT NULL,
  `weight` decimal(15,8) NOT NULL,
  `weight_prefix` varchar(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_option_value`
--

INSERT INTO `oc_product_option_value` (`product_option_value_id`, `product_option_id`, `product_id`, `option_id`, `option_value_id`, `quantity`, `subtract`, `price`, `price_prefix`, `points`, `points_prefix`, `weight`, `weight_prefix`) VALUES
(36, 230, 51, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(35, 230, 51, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(34, 230, 51, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(33, 230, 51, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(26, 228, 50, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(25, 228, 50, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(24, 228, 50, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(23, 228, 50, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(22, 228, 50, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(21, 227, 50, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(20, 227, 50, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(19, 227, 50, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(18, 227, 50, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(17, 227, 50, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(32, 230, 51, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(31, 229, 51, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(30, 229, 51, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(29, 229, 51, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(28, 229, 51, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(27, 229, 51, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(46, 232, 52, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(45, 232, 52, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(44, 232, 52, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(43, 232, 52, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(42, 232, 52, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(41, 231, 52, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(40, 231, 52, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(39, 231, 52, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(38, 231, 52, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(37, 231, 52, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(51, 233, 53, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(50, 233, 53, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(49, 233, 53, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(48, 233, 53, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(47, 233, 53, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(56, 234, 53, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(55, 234, 53, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(54, 234, 53, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(53, 234, 53, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(52, 234, 53, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(61, 235, 54, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(60, 235, 54, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(59, 235, 54, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(58, 235, 54, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(57, 235, 54, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(66, 236, 54, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(65, 236, 54, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(64, 236, 54, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(63, 236, 54, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(62, 236, 54, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(71, 237, 55, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(70, 237, 55, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(69, 237, 55, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(68, 237, 55, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(67, 237, 55, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(76, 238, 55, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(75, 238, 55, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(74, 238, 55, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(73, 238, 55, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(72, 238, 55, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(86, 240, 56, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(85, 240, 56, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(84, 240, 56, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(83, 240, 56, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(82, 240, 56, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(81, 239, 56, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(80, 239, 56, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(79, 239, 56, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(78, 239, 56, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(77, 239, 56, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(91, 241, 57, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(90, 241, 57, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(89, 241, 57, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(88, 241, 57, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(87, 241, 57, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(96, 242, 57, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(95, 242, 57, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(94, 242, 57, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(93, 242, 57, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(92, 242, 57, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(101, 243, 58, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(100, 243, 58, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(99, 243, 58, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(98, 243, 58, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(97, 243, 58, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(106, 244, 58, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(105, 244, 58, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(104, 244, 58, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(103, 244, 58, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(102, 244, 58, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(116, 246, 59, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(115, 246, 59, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(114, 246, 59, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(113, 246, 59, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(112, 246, 59, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(111, 245, 59, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(110, 245, 59, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(109, 245, 59, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(108, 245, 59, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(107, 245, 59, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(126, 248, 60, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(125, 248, 60, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(124, 248, 60, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(123, 248, 60, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(122, 248, 60, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(121, 247, 60, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(120, 247, 60, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(119, 247, 60, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(118, 247, 60, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(117, 247, 60, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(136, 250, 61, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(135, 250, 61, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(134, 250, 61, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(133, 250, 61, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(132, 250, 61, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(131, 249, 61, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(130, 249, 61, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(129, 249, 61, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(128, 249, 61, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(127, 249, 61, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(146, 252, 62, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(145, 252, 62, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(144, 252, 62, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(143, 252, 62, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(142, 252, 62, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(141, 251, 62, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(140, 251, 62, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(139, 251, 62, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(138, 251, 62, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(137, 251, 62, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(156, 254, 63, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(155, 254, 63, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(154, 254, 63, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(153, 254, 63, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(152, 254, 63, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(151, 253, 63, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(150, 253, 63, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(149, 253, 63, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(148, 253, 63, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(147, 253, 63, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(161, 255, 64, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(160, 255, 64, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(159, 255, 64, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(158, 255, 64, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(157, 255, 64, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(166, 256, 64, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(165, 256, 64, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(164, 256, 64, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(163, 256, 64, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(162, 256, 64, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(171, 257, 65, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(170, 257, 65, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(169, 257, 65, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(168, 257, 65, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(167, 257, 65, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(176, 258, 65, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(175, 258, 65, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(174, 258, 65, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(173, 258, 65, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(172, 258, 65, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(186, 260, 66, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(185, 260, 66, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(184, 260, 66, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(183, 260, 66, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(182, 260, 66, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(181, 259, 66, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(180, 259, 66, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(179, 259, 66, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(178, 259, 66, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(177, 259, 66, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(191, 261, 67, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(190, 261, 67, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(189, 261, 67, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(188, 261, 67, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(187, 261, 67, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(196, 262, 67, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(195, 262, 67, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(194, 262, 67, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(193, 262, 67, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(192, 262, 67, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(201, 263, 68, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(200, 263, 68, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(199, 263, 68, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(198, 263, 68, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(197, 263, 68, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(206, 264, 68, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(205, 264, 68, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(204, 264, 68, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(203, 264, 68, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(202, 264, 68, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(216, 266, 69, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(215, 266, 69, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(214, 266, 69, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(213, 266, 69, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(212, 266, 69, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(211, 265, 69, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(210, 265, 69, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(209, 265, 69, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(208, 265, 69, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(207, 265, 69, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(226, 268, 70, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(225, 268, 70, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(224, 268, 70, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(223, 268, 70, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(222, 268, 70, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(221, 267, 70, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(220, 267, 70, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(219, 267, 70, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(218, 267, 70, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(217, 267, 70, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(236, 270, 71, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(235, 270, 71, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(234, 270, 71, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(233, 270, 71, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(232, 270, 71, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(231, 269, 71, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(230, 269, 71, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(229, 269, 71, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(228, 269, 71, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(227, 269, 71, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(241, 271, 72, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(240, 271, 72, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(239, 271, 72, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(238, 271, 72, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(237, 271, 72, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(246, 272, 72, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(245, 272, 72, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(244, 272, 72, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(243, 272, 72, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(242, 272, 72, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(251, 273, 73, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(250, 273, 73, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(249, 273, 73, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(248, 273, 73, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(247, 273, 73, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(256, 274, 73, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(255, 274, 73, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(254, 274, 73, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(253, 274, 73, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(252, 274, 73, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(266, 276, 74, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(265, 276, 74, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(264, 276, 74, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(263, 276, 74, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(262, 276, 74, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(261, 275, 74, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(260, 275, 74, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(259, 275, 74, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(258, 275, 74, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(257, 275, 74, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(276, 278, 75, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(275, 278, 75, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(274, 278, 75, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(273, 278, 75, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(272, 278, 75, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(271, 277, 75, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(270, 277, 75, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(269, 277, 75, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(268, 277, 75, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(267, 277, 75, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(281, 279, 76, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(280, 279, 76, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(279, 279, 76, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(278, 279, 76, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(277, 279, 76, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(286, 280, 76, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(285, 280, 76, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(284, 280, 76, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(283, 280, 76, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(282, 280, 76, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(291, 281, 77, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(290, 281, 77, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(289, 281, 77, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(288, 281, 77, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(287, 281, 77, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(296, 282, 77, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(295, 282, 77, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(294, 282, 77, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(293, 282, 77, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(292, 282, 77, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(306, 284, 78, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(305, 284, 78, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(304, 284, 78, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(303, 284, 78, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(302, 284, 78, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(301, 283, 78, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(300, 283, 78, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(299, 283, 78, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(298, 283, 78, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(297, 283, 78, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(311, 285, 79, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(310, 285, 79, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(309, 285, 79, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(308, 285, 79, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(307, 285, 79, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(316, 286, 79, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(315, 286, 79, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(314, 286, 79, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(313, 286, 79, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(312, 286, 79, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(326, 288, 80, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(325, 288, 80, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(324, 288, 80, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(323, 288, 80, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(322, 288, 80, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(321, 287, 80, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(320, 287, 80, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(319, 287, 80, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(318, 287, 80, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(317, 287, 80, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(336, 290, 81, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(335, 290, 81, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(334, 290, 81, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(333, 290, 81, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(332, 290, 81, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(331, 289, 81, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(330, 289, 81, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(329, 289, 81, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(328, 289, 81, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(327, 289, 81, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(341, 291, 82, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(340, 291, 82, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(339, 291, 82, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(338, 291, 82, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(337, 291, 82, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(346, 292, 82, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(345, 292, 82, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(344, 292, 82, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(343, 292, 82, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(342, 292, 82, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(351, 293, 83, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(350, 293, 83, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(349, 293, 83, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(348, 293, 83, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(347, 293, 83, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(356, 294, 83, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(355, 294, 83, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(354, 294, 83, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(353, 294, 83, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(352, 294, 83, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(366, 296, 84, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(365, 296, 84, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(364, 296, 84, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(363, 296, 84, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(362, 296, 84, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(361, 295, 84, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(360, 295, 84, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(359, 295, 84, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(358, 295, 84, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(357, 295, 84, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(376, 298, 85, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(375, 298, 85, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(374, 298, 85, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(373, 298, 85, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(372, 298, 85, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(371, 297, 85, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(370, 297, 85, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(369, 297, 85, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(368, 297, 85, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(367, 297, 85, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(386, 300, 86, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(385, 300, 86, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(384, 300, 86, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(383, 300, 86, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(382, 300, 86, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(381, 299, 86, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(380, 299, 86, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(379, 299, 86, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(378, 299, 86, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(377, 299, 86, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(396, 302, 87, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(395, 302, 87, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(394, 302, 87, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(393, 302, 87, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(392, 302, 87, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(391, 301, 87, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(390, 301, 87, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(389, 301, 87, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(388, 301, 87, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(387, 301, 87, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(406, 304, 88, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(405, 304, 88, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(404, 304, 88, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(403, 304, 88, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(402, 304, 88, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(401, 303, 88, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(400, 303, 88, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(399, 303, 88, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(398, 303, 88, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(397, 303, 88, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(411, 305, 89, 13, 55, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(410, 305, 89, 13, 54, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(409, 305, 89, 13, 53, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(408, 305, 89, 13, 52, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(407, 305, 89, 13, 51, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(416, 306, 89, 11, 50, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(415, 306, 89, 11, 49, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(414, 306, 89, 11, 48, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(413, 306, 89, 11, 47, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+'),
(412, 306, 89, 11, 46, 483646, 1, '0.0000', '+', 0, '+', '0.00000000', '+');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_recurring`
--

CREATE TABLE `oc_product_recurring` (
  `product_id` int(11) NOT NULL,
  `recurring_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_related`
--

CREATE TABLE `oc_product_related` (
  `product_id` int(11) NOT NULL,
  `related_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_related`
--

INSERT INTO `oc_product_related` (`product_id`, `related_id`) VALUES
(50, 51),
(50, 52),
(50, 53),
(50, 54),
(50, 55),
(51, 50),
(51, 52),
(51, 53),
(51, 54),
(51, 55),
(52, 50),
(52, 51),
(52, 53),
(52, 54),
(52, 55),
(53, 50),
(53, 51),
(53, 52),
(53, 54),
(53, 55),
(54, 50),
(54, 51),
(54, 52),
(54, 53),
(54, 55),
(55, 50),
(55, 51),
(55, 52),
(55, 53),
(55, 54),
(56, 57),
(56, 58),
(56, 59),
(56, 60),
(56, 61),
(56, 62),
(56, 63),
(57, 56),
(57, 58),
(57, 59),
(57, 60),
(57, 61),
(57, 62),
(57, 63),
(58, 56),
(58, 57),
(58, 59),
(58, 60),
(58, 61),
(58, 62),
(58, 63),
(59, 56),
(59, 57),
(59, 58),
(59, 60),
(59, 61),
(59, 62),
(59, 63),
(60, 56),
(60, 57),
(60, 58),
(60, 59),
(60, 61),
(60, 62),
(60, 63),
(61, 56),
(61, 57),
(61, 58),
(61, 59),
(61, 60),
(61, 62),
(61, 63),
(62, 56),
(62, 57),
(62, 58),
(62, 59),
(62, 60),
(62, 61),
(62, 63),
(63, 56),
(63, 57),
(63, 58),
(63, 59),
(63, 60),
(63, 61),
(63, 62),
(64, 65),
(64, 66),
(64, 67),
(64, 68),
(64, 69),
(64, 70),
(64, 71),
(64, 72),
(64, 73),
(64, 74),
(64, 75),
(64, 88),
(65, 64),
(65, 66),
(65, 67),
(65, 68),
(65, 69),
(65, 70),
(65, 71),
(65, 72),
(65, 73),
(65, 74),
(65, 75),
(65, 88),
(66, 64),
(66, 65),
(66, 66),
(66, 67),
(66, 68),
(66, 69),
(66, 70),
(66, 71),
(66, 72),
(66, 73),
(66, 74),
(66, 75),
(66, 88),
(67, 64),
(67, 65),
(67, 66),
(67, 68),
(67, 69),
(67, 70),
(67, 71),
(67, 72),
(67, 73),
(67, 74),
(67, 75),
(67, 88),
(68, 64),
(68, 65),
(68, 66),
(68, 67),
(68, 69),
(68, 70),
(68, 71),
(68, 72),
(68, 73),
(68, 74),
(68, 75),
(68, 88),
(69, 64),
(69, 65),
(69, 66),
(69, 67),
(69, 68),
(69, 71),
(69, 72),
(69, 73),
(69, 74),
(69, 75),
(69, 88),
(70, 64),
(70, 65),
(70, 66),
(70, 67),
(70, 68),
(70, 70),
(70, 71),
(70, 72),
(70, 73),
(70, 74),
(70, 75),
(70, 88),
(71, 64),
(71, 65),
(71, 66),
(71, 67),
(71, 68),
(71, 69),
(71, 70),
(71, 72),
(71, 73),
(71, 74),
(71, 75),
(71, 88),
(72, 64),
(72, 65),
(72, 66),
(72, 67),
(72, 68),
(72, 69),
(72, 70),
(72, 71),
(72, 73),
(72, 74),
(72, 75),
(72, 88),
(73, 64),
(73, 65),
(73, 66),
(73, 67),
(73, 68),
(73, 69),
(73, 70),
(73, 71),
(73, 72),
(73, 74),
(73, 75),
(73, 88),
(74, 64),
(74, 65),
(74, 66),
(74, 67),
(74, 68),
(74, 69),
(74, 70),
(74, 71),
(74, 72),
(74, 73),
(74, 75),
(74, 88),
(75, 64),
(75, 65),
(75, 66),
(75, 67),
(75, 68),
(75, 69),
(75, 70),
(75, 71),
(75, 72),
(75, 73),
(75, 74),
(75, 88),
(76, 77),
(76, 78),
(76, 79),
(76, 80),
(76, 81),
(76, 82),
(76, 83),
(76, 84),
(76, 85),
(76, 86),
(76, 87),
(76, 89),
(77, 76),
(77, 78),
(77, 79),
(77, 80),
(77, 81),
(77, 82),
(77, 83),
(77, 84),
(77, 85),
(77, 86),
(77, 87),
(77, 89),
(78, 76),
(78, 77),
(78, 79),
(78, 80),
(78, 81),
(78, 82),
(78, 83),
(78, 84),
(78, 85),
(78, 86),
(78, 87),
(78, 89),
(79, 76),
(79, 77),
(79, 78),
(79, 80),
(79, 81),
(79, 82),
(79, 83),
(79, 84),
(79, 85),
(79, 86),
(79, 87),
(79, 89),
(80, 76),
(80, 77),
(80, 78),
(80, 79),
(80, 81),
(80, 82),
(80, 83),
(80, 84),
(80, 85),
(80, 86),
(80, 87),
(80, 89),
(81, 76),
(81, 77),
(81, 78),
(81, 79),
(81, 80),
(81, 82),
(81, 83),
(81, 84),
(81, 85),
(81, 86),
(81, 87),
(81, 89),
(82, 76),
(82, 77),
(82, 78),
(82, 79),
(82, 80),
(82, 81),
(82, 83),
(82, 84),
(82, 85),
(82, 86),
(82, 87),
(82, 89),
(83, 76),
(83, 77),
(83, 78),
(83, 79),
(83, 80),
(83, 81),
(83, 82),
(83, 84),
(83, 85),
(83, 86),
(83, 87),
(83, 89),
(84, 76),
(84, 77),
(84, 78),
(84, 79),
(84, 80),
(84, 81),
(84, 82),
(84, 83),
(84, 85),
(84, 86),
(84, 87),
(84, 89),
(85, 76),
(85, 77),
(85, 78),
(85, 79),
(85, 80),
(85, 81),
(85, 82),
(85, 83),
(85, 84),
(85, 86),
(85, 87),
(85, 89),
(86, 76),
(86, 77),
(86, 78),
(86, 79),
(86, 80),
(86, 81),
(86, 82),
(86, 83),
(86, 84),
(86, 85),
(86, 87),
(86, 89),
(87, 76),
(87, 77),
(87, 78),
(87, 79),
(87, 80),
(87, 81),
(87, 82),
(87, 83),
(87, 84),
(87, 85),
(87, 86),
(87, 89),
(88, 64),
(88, 65),
(88, 66),
(88, 67),
(88, 68),
(88, 69),
(88, 70),
(88, 71),
(88, 72),
(88, 73),
(88, 74),
(88, 75),
(89, 76),
(89, 77),
(89, 78),
(89, 79),
(89, 80),
(89, 81),
(89, 82),
(89, 83),
(89, 84),
(89, 85),
(89, 86),
(89, 87);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_reward`
--

CREATE TABLE `oc_product_reward` (
  `product_reward_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL DEFAULT '0',
  `customer_group_id` int(11) NOT NULL DEFAULT '0',
  `points` int(8) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_reward`
--

INSERT INTO `oc_product_reward` (`product_reward_id`, `product_id`, `customer_group_id`, `points`) VALUES
(547, 76, 1, 150);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_special`
--

CREATE TABLE `oc_product_special` (
  `product_special_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1',
  `price` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `date_start` date NOT NULL DEFAULT '0000-00-00',
  `date_end` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_special`
--

INSERT INTO `oc_product_special` (`product_special_id`, `product_id`, `customer_group_id`, `priority`, `price`, `date_start`, `date_end`) VALUES
(466, 69, 1, 0, '18.9000', '0000-00-00', '0000-00-00'),
(464, 66, 1, 0, '25.9000', '0000-00-00', '0000-00-00'),
(440, 63, 1, 0, '32.9000', '0000-00-00', '0000-00-00'),
(467, 75, 1, 0, '16.9000', '0000-00-00', '0000-00-00'),
(463, 76, 1, 0, '15.9000', '0000-00-00', '0000-00-00'),
(462, 82, 1, 0, '16.9000', '0000-00-00', '0000-00-00'),
(461, 85, 1, 0, '19.9000', '0000-00-00', '0000-00-00'),
(468, 55, 1, 0, '739.0000', '0000-00-00', '0000-00-00'),
(457, 58, 1, 0, '83.9000', '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_category`
--

CREATE TABLE `oc_product_to_category` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_category`
--

INSERT INTO `oc_product_to_category` (`product_id`, `category_id`) VALUES
(50, 62),
(50, 83),
(51, 62),
(51, 66),
(52, 62),
(52, 66),
(52, 84),
(53, 62),
(53, 82),
(54, 62),
(54, 66),
(54, 83),
(55, 62),
(55, 66),
(55, 81),
(56, 61),
(56, 77),
(56, 79),
(57, 61),
(57, 77),
(57, 80),
(58, 61),
(58, 77),
(59, 61),
(59, 77),
(59, 80),
(60, 61),
(60, 77),
(60, 80),
(61, 61),
(61, 77),
(61, 80),
(62, 61),
(62, 77),
(62, 79),
(63, 61),
(63, 76),
(63, 77),
(63, 78),
(63, 80),
(64, 60),
(64, 65),
(64, 72),
(65, 60),
(65, 65),
(65, 74),
(66, 60),
(66, 65),
(66, 72),
(66, 74),
(67, 60),
(67, 65),
(67, 72),
(67, 74),
(68, 60),
(68, 65),
(68, 72),
(68, 74),
(69, 60),
(69, 65),
(69, 72),
(70, 60),
(70, 72),
(70, 74),
(71, 60),
(71, 65),
(71, 74),
(72, 60),
(72, 65),
(72, 74),
(73, 60),
(73, 65),
(73, 74),
(74, 60),
(74, 65),
(74, 74),
(75, 60),
(75, 65),
(75, 74),
(76, 59),
(76, 64),
(76, 70),
(77, 59),
(77, 64),
(77, 70),
(78, 59),
(78, 64),
(78, 70),
(79, 59),
(79, 64),
(79, 70),
(80, 59),
(80, 64),
(80, 70),
(81, 59),
(81, 64),
(81, 70),
(82, 59),
(82, 64),
(82, 70),
(83, 59),
(83, 64),
(83, 70),
(84, 59),
(84, 64),
(84, 70),
(85, 59),
(85, 64),
(85, 70),
(86, 59),
(86, 64),
(86, 70),
(87, 59),
(87, 64),
(87, 70),
(88, 60),
(88, 65),
(88, 72),
(89, 59),
(89, 64),
(89, 70);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_download`
--

CREATE TABLE `oc_product_to_download` (
  `product_id` int(11) NOT NULL,
  `download_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_layout`
--

CREATE TABLE `oc_product_to_layout` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_layout`
--

INSERT INTO `oc_product_to_layout` (`product_id`, `store_id`, `layout_id`) VALUES
(50, 0, 0),
(51, 0, 0),
(52, 0, 0),
(53, 0, 0),
(54, 0, 0),
(55, 0, 0),
(56, 0, 0),
(57, 0, 0),
(58, 0, 0),
(59, 0, 0),
(60, 0, 0),
(61, 0, 0),
(62, 0, 0),
(63, 0, 0),
(64, 0, 0),
(65, 0, 0),
(66, 0, 0),
(67, 0, 0),
(68, 0, 0),
(69, 0, 0),
(70, 0, 0),
(71, 0, 0),
(72, 0, 0),
(73, 0, 0),
(74, 0, 0),
(75, 0, 0),
(76, 0, 0),
(77, 0, 0),
(78, 0, 0),
(79, 0, 0),
(80, 0, 0),
(81, 0, 0),
(82, 0, 0),
(83, 0, 0),
(84, 0, 0),
(85, 0, 0),
(86, 0, 0),
(87, 0, 0),
(88, 0, 0),
(89, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_product_to_store`
--

CREATE TABLE `oc_product_to_store` (
  `product_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_product_to_store`
--

INSERT INTO `oc_product_to_store` (`product_id`, `store_id`) VALUES
(50, 0),
(51, 0),
(52, 0),
(53, 0),
(54, 0),
(55, 0),
(56, 0),
(57, 0),
(58, 0),
(59, 0),
(60, 0),
(61, 0),
(62, 0),
(63, 0),
(64, 0),
(65, 0),
(66, 0),
(67, 0),
(68, 0),
(69, 0),
(70, 0),
(71, 0),
(72, 0),
(73, 0),
(74, 0),
(75, 0),
(76, 0),
(77, 0),
(78, 0),
(79, 0),
(80, 0),
(81, 0),
(82, 0),
(83, 0),
(84, 0),
(85, 0),
(86, 0),
(87, 0),
(88, 0),
(89, 0);

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring`
--

CREATE TABLE `oc_recurring` (
  `recurring_id` int(11) NOT NULL,
  `price` decimal(10,4) NOT NULL,
  `frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `cycle` int(10) UNSIGNED NOT NULL,
  `trial_status` tinyint(4) NOT NULL,
  `trial_price` decimal(10,4) NOT NULL,
  `trial_frequency` enum('day','week','semi_month','month','year') NOT NULL,
  `trial_duration` int(10) UNSIGNED NOT NULL,
  `trial_cycle` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_recurring_description`
--

CREATE TABLE `oc_recurring_description` (
  `recurring_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return`
--

CREATE TABLE `oc_return` (
  `return_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `product` varchar(255) NOT NULL,
  `model` varchar(64) NOT NULL,
  `quantity` int(4) NOT NULL,
  `opened` tinyint(1) NOT NULL,
  `return_reason_id` int(11) NOT NULL,
  `return_action_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `comment` text,
  `date_ordered` date NOT NULL DEFAULT '0000-00-00',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_action`
--

CREATE TABLE `oc_return_action` (
  `return_action_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_action`
--

INSERT INTO `oc_return_action` (`return_action_id`, `language_id`, `name`) VALUES
(1, 1, 'Refunded'),
(2, 1, 'Credit Issued'),
(3, 1, 'Replacement Sent'),
(1, 2, 'Refunded'),
(2, 2, 'Credit Issued'),
(3, 2, 'Replacement Sent');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_history`
--

CREATE TABLE `oc_return_history` (
  `return_history_id` int(11) NOT NULL,
  `return_id` int(11) NOT NULL,
  `return_status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_reason`
--

CREATE TABLE `oc_return_reason` (
  `return_reason_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_reason`
--

INSERT INTO `oc_return_reason` (`return_reason_id`, `language_id`, `name`) VALUES
(1, 1, 'Dead On Arrival'),
(2, 1, 'Received Wrong Item'),
(3, 1, 'Order Error'),
(4, 1, 'Faulty, please supply details'),
(5, 1, 'Other, please supply details'),
(1, 2, 'Dead On Arrival'),
(2, 2, 'Received Wrong Item'),
(3, 2, 'Order Error'),
(4, 2, 'Faulty, please supply details'),
(5, 2, 'Other, please supply details');

-- --------------------------------------------------------

--
-- Table structure for table `oc_return_status`
--

CREATE TABLE `oc_return_status` (
  `return_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_return_status`
--

INSERT INTO `oc_return_status` (`return_status_id`, `language_id`, `name`) VALUES
(1, 1, 'Pending'),
(3, 1, 'Complete'),
(2, 1, 'Awaiting Products'),
(1, 2, 'Pending'),
(3, 2, 'Complete'),
(2, 2, 'Awaiting Products');

-- --------------------------------------------------------

--
-- Table structure for table `oc_review`
--

CREATE TABLE `oc_review` (
  `review_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `author` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `rating` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_review`
--

INSERT INTO `oc_review` (`review_id`, `product_id`, `customer_id`, `author`, `text`, `rating`, `status`, `date_added`, `date_modified`) VALUES
(1, 76, 0, 'Henry', 'Vestibulum hendrerit, augue at aliquam placerat, magna justo feugiat eros, sit amet laoreet est urna id eros. Aliquam ac mauris et sem vehicula molestie.', 5, 1, '2017-07-14 17:37:02', '2017-07-14 17:38:23'),
(2, 76, 0, 'Roland', 'Fusce blandit felis in tortor tristique sed posuere lectus elementum. Fusce non dui nec sem pretium hendrerit et at dolor. Suspendisse tempus dapibus lorem, ut tristique diam fringilla vel.', 5, 1, '2017-07-14 17:37:28', '2017-07-14 17:38:16'),
(3, 76, 0, 'Jennifer', 'Quisque adipiscing tortor sit amet urna tempor et fermentum nisi egestas. Donec congue lobortis luctus. Phasellus at sem condimentum velit adipiscing varius. Suspendisse dignissim, urna vel lacinia fermentum, lectus nulla gravida orci, a rutrum lorem neque a arcu.', 5, 1, '2017-07-14 17:37:46', '2017-07-14 17:38:10'),
(4, 89, 0, 'Jennifer', 'Quisque adipiscing tortor sit amet urna tempor et fermentum nisi egestas. Donec congue lobortis luctus. Phasellus at sem condimentum velit adipiscing varius. Suspendisse dignissim, urna vel lacinia fermentum, lectus nulla gravida orci, a rutrum lorem neque a arcu.', 5, 1, '2017-07-14 17:39:49', '2017-07-14 17:41:28'),
(5, 89, 0, 'Roland', 'Fusce blandit felis in tortor tristique sed posuere lectus elementum. Fusce non dui nec sem pretium hendrerit et at dolor. Suspendisse tempus dapibus lorem, ut tristique diam fringilla vel.', 5, 1, '2017-07-14 17:40:02', '2017-07-14 17:41:22'),
(6, 89, 0, 'Henry', 'Vestibulum hendrerit, augue at aliquam placerat, magna justo feugiat eros, sit amet laoreet est urna id eros. Aliquam ac mauris et sem vehicula molestie.', 5, 1, '2017-07-14 17:40:11', '2017-07-14 17:41:16'),
(7, 88, 0, 'Alex', 'Aenean ac mi ipsum. Nam nec sem libero. Fusce erat orci, convallis sed egestas vel, aliquam non augue. Integer eu magna eu tellus accumsan tristique a non quam. Morbi posuere arcu nec libero interdum dictum. ', 5, 1, '2017-08-14 13:54:58', '2017-08-14 13:55:41'),
(8, 88, 0, 'Kelly', 'Nunc at neque nisl. Praesent ac laoreet nibh. Aliquam enim lectus, sagittis in ullamcorper sit amet, posuere eu arcu. Cras sit amet purus lorem.', 5, 1, '2017-08-14 13:55:11', '2017-08-14 13:55:50'),
(9, 88, 0, 'Kerry', 'Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus.', 5, 1, '2017-08-14 13:55:21', '2017-08-14 13:55:55'),
(10, 87, 0, 'John', 'Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem. Donec volutpat vehicula est, quis rhoncus velit varius ac. Duis sed lorem ut elit iaculis consequat semper non nunc. Duis vel nisi nunc.', 4, 1, '2017-08-14 13:56:39', '2017-08-14 13:58:42'),
(11, 86, 0, 'Roland', 'Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem. Donec volutpat vehicula est, quis rhoncus velit varius ac. Duis sed lorem ut elit iaculis consequat semper non nunc. Duis vel nisi nunc.', 5, 1, '2017-08-14 13:57:00', '2017-08-14 13:58:48'),
(12, 86, 0, 'Kelly', 'Donec porta enim sit amet augue iaculis eu sollicitudin ligula pharetra. Ut nec dui nec nisi consectetur sodales. Cras magna felis, porttitor volutpat rhoncus a, gravida id ante.', 5, 1, '2017-08-14 13:57:12', '2017-08-14 14:00:04'),
(13, 85, 0, 'Caroline', 'Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.', 5, 1, '2017-08-14 13:57:34', '2017-08-14 14:00:11'),
(14, 85, 0, 'Jennifer', 'Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue.', 5, 1, '2017-08-14 13:57:48', '2017-08-14 14:00:16'),
(15, 84, 0, 'Kelly', 'Duis eget tortor tortor. Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 5, 1, '2017-08-14 13:58:13', '2017-08-14 14:00:22'),
(16, 84, 0, 'Jack', 'Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim.', 5, 1, '2017-08-14 13:58:23', '2017-08-14 14:00:27'),
(17, 83, 0, 'Kelly', 'Duis eget tortor tortor. Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 4, 1, '2017-08-14 14:02:13', '2017-08-14 14:06:09'),
(18, 82, 0, 'Alex', 'Aenean ac mi ipsum. Nam nec sem libero. Fusce erat orci, convallis sed egestas vel, aliquam non augue. Integer eu magna eu tellus accumsan tristique a non quam. Morbi posuere arcu nec libero interdum dictum. ', 5, 1, '2017-08-14 14:02:52', '2017-08-14 14:06:15'),
(19, 82, 0, 'Kelly', 'Nunc at neque nisl. Praesent ac laoreet nibh. Aliquam enim lectus, sagittis in ullamcorper sit amet, posuere eu arcu. Cras sit amet purus lorem.', 4, 1, '2017-08-14 14:03:07', '2017-08-14 14:06:23'),
(20, 82, 0, 'Kerry', 'Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus.', 5, 1, '2017-08-14 14:03:16', '2017-08-14 14:06:37'),
(21, 81, 0, 'Bob', 'Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue.', 5, 1, '2017-08-14 14:03:39', '2017-08-14 14:06:43'),
(22, 81, 0, 'Jennifer', 'Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.', 5, 1, '2017-08-14 14:03:48', '2017-08-14 14:06:50'),
(23, 80, 0, 'John', 'Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue.', 5, 1, '2017-08-14 14:04:06', '2017-08-14 14:06:56'),
(24, 79, 0, 'Barbara', 'Duis eget tortor tortor. Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 5, 1, '2017-08-14 14:04:32', '2017-08-14 14:07:01'),
(25, 79, 0, 'Jack', 'Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim.', 5, 1, '2017-08-14 14:04:41', '2017-08-14 14:07:08'),
(26, 78, 0, 'Steve', 'Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 5, 1, '2017-08-14 14:05:11', '2017-08-14 14:07:15'),
(27, 77, 0, 'Jack', 'Suspendisse vitae neque vel nisl mollis faucibus in sit amet nisl. Mauris ut feugiat leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean quis quam orci. Quisque dictum suscipit quam.', 4, 1, '2017-08-14 14:14:59', '2017-08-14 14:15:22'),
(28, 77, 0, 'Victor', 'Curabitur id lacus id felis auctor dictum. Nulla nisl felis, cursus quis accumsan eget, consectetur sed tortor. Proin imperdiet ultricies lorem non laoreet. Phasellus sed pretium mi. Sed fermentum placerat libero nec semper. ', 4, 1, '2017-08-14 14:15:12', '2017-08-14 14:15:29'),
(29, 75, 0, 'Tom', 'Nullam tristique interdum tincidunt. Nulla et orci est. Nam non tincidunt urna. Cras dapibus iaculis faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 1, '2017-08-14 14:16:23', '2017-08-14 14:18:17'),
(30, 75, 0, 'Jack', 'Suspendisse vitae neque vel nisl mollis faucibus in sit amet nisl. Mauris ut feugiat leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean quis quam orci. Quisque dictum suscipit quam.', 5, 1, '2017-08-14 14:16:32', '2017-08-14 14:22:15'),
(31, 74, 0, 'Roland', 'Mauris ut nisl ipsum, id laoreet augue. Vivamus scelerisque rutrum bibendum. Vestibulum hendrerit, augue at aliquam placerat, magna justo feugiat eros, sit amet laoreet est urna id eros. Aliquam ac mauris et sem vehicula molestie.', 5, 1, '2017-08-14 14:16:49', '2017-08-14 14:22:22'),
(32, 74, 0, 'Kevin', 'Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit.', 5, 1, '2017-08-14 14:17:02', '2017-08-14 14:22:28'),
(33, 74, 0, 'Roy', 'Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla. ', 5, 1, '2017-08-14 14:17:12', '2017-08-14 14:22:34'),
(34, 73, 0, 'Kelly', 'Duis eget tortor tortor. Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 4, 1, '2017-08-14 14:17:39', '2017-08-14 14:22:40'),
(35, 73, 0, 'Jack', 'Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim.', 5, 1, '2017-08-14 14:18:00', '2017-08-14 14:22:46'),
(36, 72, 0, 'Alex', 'Aenean ac mi ipsum. Nam nec sem libero. Fusce erat orci, convallis sed egestas vel, aliquam non augue. Integer eu magna eu tellus accumsan tristique a non quam. Morbi posuere arcu nec libero interdum dictum. ', 5, 1, '2017-08-14 14:23:30', '2017-08-14 14:26:08'),
(37, 72, 0, 'Kelly', 'Nunc at neque nisl. Praesent ac laoreet nibh. Aliquam enim lectus, sagittis in ullamcorper sit amet, posuere eu arcu. Cras sit amet purus lorem.', 4, 1, '2017-08-14 14:23:42', '2017-08-14 14:26:14'),
(38, 71, 0, 'Kerry', 'Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus.', 5, 1, '2017-08-14 14:24:03', '2017-08-14 14:26:22'),
(39, 71, 0, 'Tom', 'Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.', 5, 1, '2017-08-14 14:24:16', '2017-08-14 14:26:28'),
(40, 70, 0, 'Jennifer', 'Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue.', 5, 1, '2017-08-14 14:24:33', '2017-08-14 14:26:34'),
(41, 70, 0, 'Caroline', 'Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.', 5, 1, '2017-08-14 14:24:43', '2017-08-14 14:26:42'),
(42, 69, 0, 'Victor', 'Curabitur id lacus id felis auctor dictum. Nulla nisl felis, cursus quis accumsan eget, consectetur sed tortor. Proin imperdiet ultricies lorem non laoreet. Phasellus sed pretium mi. Sed fermentum placerat libero nec semper. ', 5, 1, '2017-08-14 14:25:14', '2017-08-14 14:26:50'),
(43, 69, 0, 'Jack', 'Suspendisse vitae neque vel nisl mollis faucibus in sit amet nisl. Mauris ut feugiat leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean quis quam orci. Quisque dictum suscipit quam.', 5, 1, '2017-08-14 14:25:29', '2017-08-14 14:26:55'),
(44, 68, 0, 'Tom', 'Nullam tristique interdum tincidunt. Nulla et orci est. Nam non tincidunt urna. Cras dapibus iaculis faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 1, '2017-08-14 14:25:46', '2017-08-14 14:27:03'),
(45, 68, 0, 'Steve', 'Curabitur id lacus id felis auctor dictum. Nulla nisl felis, cursus quis accumsan eget, consectetur sed tortor. Proin imperdiet ultricies lorem non laoreet. Phasellus sed pretium mi. Sed fermentum placerat libero nec semper. ', 5, 1, '2017-08-14 14:25:57', '2017-08-14 14:27:10'),
(46, 67, 0, 'Roy', 'Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla. ', 4, 1, '2017-08-14 14:27:53', '2017-08-14 14:29:33'),
(47, 66, 0, 'Kevin', 'Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit.', 5, 1, '2017-08-14 14:28:13', '2017-08-14 14:29:39'),
(48, 66, 0, 'Roland', 'Mauris ut nisl ipsum, id laoreet augue. Vivamus scelerisque rutrum bibendum. Vestibulum hendrerit, augue at aliquam placerat, magna justo feugiat eros, sit amet laoreet est urna id eros. Aliquam ac mauris et sem vehicula molestie.', 5, 1, '2017-08-14 14:28:27', '2017-08-14 14:29:45'),
(49, 65, 0, 'Jack', 'Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim.', 5, 1, '2017-08-14 14:28:47', '2017-08-14 14:29:51'),
(50, 65, 0, 'John', 'Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 5, 1, '2017-08-14 14:28:57', '2017-08-14 14:29:57'),
(51, 64, 0, 'Kelly', 'Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 5, 1, '2017-08-14 14:29:13', '2017-08-14 14:30:04'),
(52, 63, 0, 'Barbara', 'Donec porta enim sit amet augue iaculis eu sollicitudin ligula pharetra. Ut nec dui nec nisi consectetur sodales. Cras magna felis, porttitor volutpat rhoncus a, gravida id ante.', 5, 1, '2017-08-14 15:23:58', '2017-08-14 15:27:51'),
(53, 63, 0, 'Chris', 'Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem. Donec volutpat vehicula est, quis rhoncus velit varius ac. Duis sed lorem ut elit iaculis consequat semper non nunc. Duis vel nisi nunc.', 5, 1, '2017-08-14 15:24:18', '2017-08-14 15:27:58'),
(54, 62, 0, 'Tom', 'Nullam tristique interdum tincidunt. Nulla et orci est. Nam non tincidunt urna. Cras dapibus iaculis faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 1, '2017-08-14 15:25:00', '2017-08-14 15:28:05'),
(55, 62, 0, 'Jerry', 'Curabitur id lacus id felis auctor dictum. Nulla nisl felis, cursus quis accumsan eget, consectetur sed tortor. Proin imperdiet ultricies lorem non laoreet. Phasellus sed pretium mi. Sed fermentum placerat libero nec semper. ', 4, 1, '2017-08-14 15:25:13', '2017-08-14 15:28:12'),
(56, 62, 0, 'Jack', 'Suspendisse vitae neque vel nisl mollis faucibus in sit amet nisl. Mauris ut feugiat leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean quis quam orci. Quisque dictum suscipit quam.', 5, 1, '2017-08-14 15:25:24', '2017-08-14 15:28:18'),
(57, 61, 0, 'Victor', 'Nulla nisl felis, cursus quis accumsan eget, consectetur sed tortor. Proin imperdiet ultricies lorem non laoreet. Phasellus sed pretium mi. Sed fermentum placerat libero nec semper. ', 4, 1, '2017-08-14 15:25:44', '2017-08-14 15:28:25'),
(58, 60, 0, 'Roy', 'Nullam ac eros eros, et ullamcorper leo. Proin placerat nunc sed magna mattis dapibus. Donec vitae risus id ante fringilla eleifend vel eu sem. Maecenas eros tellus, iaculis a fermentum a, ultrices dictum purus. Aliquam erat volutpat. Pellentesque viverra arcu vel est sodales ac lobortis velit fringilla. ', 5, 1, '2017-08-14 15:26:01', '2017-08-14 15:28:31'),
(59, 59, 0, 'Kevin', 'Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor. Pellentesque sem augue, elementum sit amet commodo quis, auctor in erat. Etiam at elit orci. Etiam dapibus mauris placerat nulla sagittis suscipit.', 5, 1, '2017-08-14 15:26:17', '2017-08-14 15:28:37'),
(60, 58, 0, 'Roland', 'Mauris ut nisl ipsum, id laoreet augue. Vivamus scelerisque rutrum bibendum. Vestibulum hendrerit, augue at aliquam placerat, magna justo feugiat eros, sit amet laoreet est urna id eros. Aliquam ac mauris et sem vehicula molestie.', 5, 1, '2017-08-14 15:26:36', '2017-08-14 15:28:43'),
(61, 57, 0, 'Jack', 'Nunc ornare adipiscing orci eu consectetur. Ut justo libero, porttitor ac elementum luctus, blandit at dui. Donec sit amet nunc id libero sagittis mattis. Morbi at mauris eu mauris facilisis sollicitudin. Phasellus consectetur laoreet fringilla. Nulla molestie pulvinar aliquet. Donec non metus enim.', 4, 1, '2017-08-14 15:26:55', '2017-08-14 15:28:49'),
(62, 56, 0, 'Kelly', 'Mauris ornare lorem at sapien placerat non sollicitudin libero posuere. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque nec tellus sed nisl cursus iaculis vel vel ante. Nam in nisl justo, ut rhoncus libero.', 5, 1, '2017-08-14 15:27:16', '2017-08-14 15:28:54'),
(63, 56, 0, 'Kerry', 'Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus.', 5, 1, '2017-08-14 15:27:27', '2017-08-14 15:29:01'),
(64, 56, 0, 'Alex', 'Aenean ac mi ipsum. Nam nec sem libero. Fusce erat orci, convallis sed egestas vel, aliquam non augue. Integer eu magna eu tellus accumsan tristique a non quam. Morbi posuere arcu nec libero interdum dictum. ', 5, 1, '2017-08-14 15:27:39', '2017-08-14 15:29:06'),
(65, 55, 0, 'Alex', 'Aenean ac mi ipsum. Nam nec sem libero. Fusce erat orci, convallis sed egestas vel, aliquam non augue. Integer eu magna eu tellus accumsan tristique a non quam. Morbi posuere arcu nec libero interdum dictum. ', 5, 1, '2017-08-14 15:31:39', '2017-08-14 15:34:19'),
(66, 55, 0, 'Kelly', 'Nunc at neque nisl. Praesent ac laoreet nibh. Aliquam enim lectus, sagittis in ullamcorper sit amet, posuere eu arcu. Cras sit amet purus lorem.', 5, 1, '2017-08-14 15:31:50', '2017-08-14 15:34:25'),
(67, 55, 0, 'Kerry', 'Fusce pulvinar, sem in hendrerit varius, tortor ligula hendrerit elit, eu consectetur eros turpis a nulla. Nunc tincidunt enim non nunc rutrum viverra facilisis magna imperdiet. Ut ultricies ultricies diam id sagittis. Nam vestibulum massa congue diam lacinia tempus.', 5, 1, '2017-08-14 15:32:00', '2017-08-14 15:34:31'),
(68, 54, 0, 'Kerry', 'Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem. Donec volutpat vehicula est, quis rhoncus velit varius ac. Duis sed lorem ut elit iaculis consequat semper non nunc. Duis vel nisi nunc.', 5, 1, '2017-08-14 15:32:30', '2017-08-14 15:34:37'),
(69, 53, 0, 'Kelly', 'Donec porta enim sit amet augue iaculis eu sollicitudin ligula pharetra. Ut nec dui nec nisi consectetur sodales. Cras magna felis, porttitor volutpat rhoncus a, gravida id ante.', 5, 1, '2017-08-14 15:32:45', '2017-08-14 15:34:44'),
(70, 53, 0, 'John', 'Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus purus ut lorem. Donec volutpat vehicula est, quis rhoncus velit varius ac. Duis sed lorem ut elit iaculis consequat semper non nunc. Duis vel nisi nunc.', 5, 1, '2017-08-14 15:32:54', '2017-08-14 15:35:47'),
(71, 52, 0, 'Caroline', 'Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.', 5, 1, '2017-08-14 15:33:11', '2017-08-14 15:35:54'),
(72, 51, 0, 'Jennifer', 'Donec non ante justo, at ultrices quam. Pellentesque bibendum, ipsum sed mollis sagittis, nisi libero condimentum lacus, sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue.', 4, 1, '2017-08-14 15:33:29', '2017-08-14 15:36:04'),
(73, 50, 0, 'Victor', 'Curabitur id lacus id felis auctor dictum. Nulla nisl felis, cursus quis accumsan eget, consectetur sed tortor. Proin imperdiet ultricies lorem non laoreet. Phasellus sed pretium mi. Sed fermentum placerat libero nec semper. ', 5, 1, '2017-08-14 15:33:49', '2017-08-14 15:36:10'),
(74, 50, 0, 'Jack', 'Suspendisse vitae neque vel nisl mollis faucibus in sit amet nisl. Mauris ut feugiat leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aenean quis quam orci. Quisque dictum suscipit quam.', 5, 1, '2017-08-14 15:33:59', '2017-08-14 15:36:15'),
(75, 50, 0, 'Tom', 'Nullam tristique interdum tincidunt. Nulla et orci est. Nam non tincidunt urna. Cras dapibus iaculis faucibus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 5, 1, '2017-08-14 15:34:09', '2017-08-14 15:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `oc_seo_url`
--

CREATE TABLE `oc_seo_url` (
  `seo_url_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `query` varchar(255) NOT NULL,
  `keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_seo_url`
--

INSERT INTO `oc_seo_url` (`seo_url_id`, `store_id`, `language_id`, `query`, `keyword`) VALUES
(772, 0, 1, 'information_id=4', 'about_us'),
(847, 0, 1, 'manufacturer_id=14', 'sample-brand-4'),
(851, 0, 1, 'manufacturer_id=18', 'sample-brand-8'),
(870, 0, 1, 'category_id=60', 'man'),
(871, 0, 1, 'category_id=61', 'kids'),
(872, 0, 1, 'category_id=62', 'electronics'),
(866, 0, 1, 'category_id=63', 'home-garden'),
(867, 0, 1, 'category_id=75', 'health-beauty'),
(850, 0, 1, 'manufacturer_id=17', 'sample-brand-7'),
(849, 0, 1, 'manufacturer_id=16', 'sample-brand-6'),
(848, 0, 1, 'manufacturer_id=15', 'sample-brand-5'),
(846, 0, 1, 'manufacturer_id=13', 'sample-brand-3'),
(869, 0, 1, 'manufacturer_id=12', 'sample-brand-2'),
(844, 0, 1, 'manufacturer_id=11', 'sample-brand-1'),
(841, 0, 1, 'information_id=6', 'delivery'),
(842, 0, 1, 'information_id=3', 'privacy'),
(843, 0, 1, 'information_id=5', 'terms');

-- --------------------------------------------------------

--
-- Table structure for table `oc_session`
--

CREATE TABLE `oc_session` (
  `session_id` varchar(32) NOT NULL,
  `data` text NOT NULL,
  `expire` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_session`
--

INSERT INTO `oc_session` (`session_id`, `data`, `expire`) VALUES
('08cc3cc57e43976cedb31e181b', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"99PCpG7TMvmZGMPg8kA6VyDsV7rtTjHC\"}', '2017-08-14 18:17:39'),
('0bfef7c23b6d6eb6cce5e4456e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 16:30:57'),
('0fcca164bc5dde0a7a523c1bc9', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 16:31:04'),
('12b8b3b8cd8ce5dc65ec94a982', '{\"user_id\":\"1\",\"user_token\":\"Do5aufxTYXnmKPwjPb2Sw2SCMOHy93V1\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-14 17:18:47'),
('1855277cd067c3073a1d7dfebe', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 16:33:09'),
('1d7f3f58e1458b115e3caee8aa', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"Rml03k0rhJkUFoKHg63ZPL0Hv1oOoZEr\"}', '2017-07-21 17:55:51'),
('1f66baadc8f1772fd3ad45a410', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-15 12:16:41'),
('219072b5fe5ddd0ec9b145a248', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 16:32:40'),
('2930aa3ed9582ce1d1b8a1c96f', '{\"language\":\"vi-vn\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"RyUBTXhW3XgwILMwduyGg05z5WnTa6Yf\"}', '2018-03-09 14:33:19'),
('2aaa5176b8598f97f7f9166e09', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-19 14:23:07'),
('2b80786cd45c646e79d898946d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-05 13:48:29'),
('2c123b73c35de31b4a1454d6eb', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-03 17:19:50'),
('2d2c0eba3bb3fc8de201aa859f', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-09 21:20:23'),
('3265faff7991f3042c0f23662b', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-13 11:03:27'),
('3bd9d0c4f4ee7d4582ddbdc1b6', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-15 15:04:54'),
('3c08060314203b9056cffbd2c5', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"DJeIb3rNypmx1xY21pv7BTQmI8oymNgI\"}', '2017-08-04 17:55:08'),
('3df7ac851c507c1eec0f4cafb0', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"mLM8oVGUKGn7iP1eHIi2LTEBQDfl2Y3L\"}', '2017-07-20 23:31:59'),
('419f476e99bc67469729650611', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"ti3z3ywVHW6X9LZszJjosfTjGz7iXMO1\"}', '2017-08-03 23:30:57'),
('41ad82ea92b755338eb4e795fc', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"IN1g7KrwKLsyoyh15a0TUlXZtaJ8d8gD\"}', '2017-08-07 18:30:56'),
('44170459d1955d0ffdf0a6dd02', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-04 11:44:53'),
('4a241e275faa33fb7695ec69a6', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"qa3Vx6Yu39v966XHXMOFY6ji9LGr2uYR\"}', '2017-07-28 17:47:11'),
('58939d5a2f33582a468b6be7ec', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-22 11:15:53'),
('5b25493c72cb7cf4af12004d45', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-14 15:50:05'),
('5f2c324a393b570b0b614232fc', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-15 10:47:33'),
('5f51468db1a4b6527c54417684', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"AmLXvJ99f8WCScTVxqMN10SfVkQxFX0G\"}', '2017-07-12 20:06:20'),
('60c84639f791d1c6c230ec53a4', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"Ajt7Sz0NRm0wLkWy1hE3a0R8deRTYYDU\"}', '2017-07-24 19:12:18'),
('65fdcccc906da61ca937fbce80', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"oAgr86lcm9EdZpAjbN0TLfabucv1MFF3\"}', '2017-08-15 18:34:13'),
('6922a720e242905a4c63b452e3', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-08 14:50:04'),
('7186b894dd551ac89e28d9e52e', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-17 20:38:38'),
('74640f69a63a88538f581e1c6b', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"2NpxTcviwxMpeKdyisJ8rV9TeHMbc8Tg\"}', '2017-08-10 23:09:06'),
('7506ac498fcb13c208ed472443', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-27 21:24:05'),
('787b509a791b5de8275eb6e891', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"rFgfnvwtJoNTcTiTQJxP4K6hnjZPvz3T\"}', '2017-08-01 18:08:20'),
('8a6679f6423990be0ac50dcc5a', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 14:13:18'),
('8e520e7e1956dec14b0b176461', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"dt1GVres5mRL8Meo94zj50T0SHTPVsma\"}', '2017-10-05 14:29:03'),
('a407230d81668ca4ec433b8026', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"lzIVQZWOhb7a3vJfHypBuyFawZqsmI76\",\"account\":\"register\",\"customer_id\":\"1\",\"payment_address\":{\"address_id\":\"1\",\"firstname\":\"John\",\"lastname\":\"Rodman\",\"company\":\"\",\"address_1\":\"135 South Park Avenue\",\"address_2\":\"\",\"postcode\":\"CA 90024\",\"city\":\"Los Angeles\",\"zone_id\":\"3624\",\"zone\":\"California\",\"zone_code\":\"CA\",\"country_id\":\"223\",\"country\":\"United States\",\"iso_code_2\":\"US\",\"iso_code_3\":\"USA\",\"address_format\":\"{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}\",\"custom_field\":null},\"shipping_address\":{\"address_id\":\"1\",\"firstname\":\"John\",\"lastname\":\"Rodman\",\"company\":\"\",\"address_1\":\"135 South Park Avenue\",\"address_2\":\"\",\"postcode\":\"CA 90024\",\"city\":\"Los Angeles\",\"zone_id\":\"3624\",\"zone\":\"California\",\"zone_code\":\"CA\",\"country_id\":\"223\",\"country\":\"United States\",\"iso_code_2\":\"US\",\"iso_code_3\":\"USA\",\"address_format\":\"{firstname} {lastname}\r\n{company}\r\n{address_1}\r\n{address_2}\r\n{city}, {zone} {postcode}\r\n{country}\",\"custom_field\":null}}', '2017-08-02 21:25:04'),
('a4198a0c245390d95c4eb5f48d', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 16:22:48'),
('a52e79809c4389d41ef23578b0', '{\"language\":\"en-gb\",\"currency\":\"VND\",\"user_id\":\"1\",\"user_token\":\"w2hEEwS81tklH1Drnvz9QOAP9h4ZpWrK\"}', '2018-03-08 15:30:42'),
('a9feda42fe9f1893b20a3c9324', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-21 11:03:20'),
('ab03716f1d45ad4bb780f0a60b', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-15 16:24:32'),
('abee76b6f29f6caf5db077cd7d', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"MmFJetOMyDFv2jJycu2WOTsZOQYCNmnv\"}', '2017-07-26 21:27:16'),
('ad44559c054f02e23e4b8e8d67', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-15 15:33:22'),
('aee8c597e31683fb73e13f7182', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-02 20:52:41'),
('bf61275e9fb2b1c5ded92f89fe', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-10 17:26:27'),
('bffe38cd4eb4b6c1138cdc1a09', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"9G0pC9ikmKVtmNk7h1GtTPGs8KYjuoLY\"}', '2017-07-28 00:41:27'),
('c048ea0f1d8ada3d19833daad9', 'false', '2017-07-26 13:15:32'),
('c41e8ef12bbb8a39584f7c5fec', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"OiILSmPbtx7BHMzIu0Pr8RRTN4XYQ14m\",\"compare\":[\"82\",\"77\"],\"wishlist\":[\"82\",\"76\"]}', '2017-07-18 21:53:05'),
('c456c331411a96e94e3248fd21', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"HonDlzpwGG9SMQU8yX61M2VAW2QppSYT\"}', '2017-08-22 12:56:01'),
('cafac428a7516f548d470913a7', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 16:32:11'),
('cb90fd5e479df6580accaaf255', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-20 20:09:13'),
('cbdf9ade1f9026c43e5ca7cefd', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-09-12 12:28:57'),
('d5746c3b817fe22abf702e6f49', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"qg4AzXNGu6PBkwzoF61BXhrn3FUQT0mf\",\"compare\":[\"77\",\"78\"]}', '2017-07-31 18:00:50'),
('de2280625270bf869cd7a91699', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"wogLnpc5j9G3VZ2XgnEeC01iPREzQGIQ\"}', '2017-07-17 17:50:56'),
('e11ae541f7ef8a747260b14ec1', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"HW7phVJGEK0xaqW8C3jFttD1kYsvPQUx\",\"compare\":[\"51\"]}', '2017-07-19 19:20:53'),
('e318ab539a9cc57ec8fcc31360', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-04 17:11:47'),
('e5fb851396a1204e634ffe8def', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"10R0BfRSz6VosslIMNdxxrjFIKHigcFH\"}', '2017-07-25 20:51:04'),
('e796575d90f5c5226bb13b5dd9', '{\"language\":\"en-gb\",\"currency\":\"USD\",\"user_id\":\"1\",\"user_token\":\"RMcXizV5Sc6GIEPPRIoql639xuTXFyGD\"}', '2017-07-15 11:59:42'),
('e7c8965d93bbda865baad11431', '{\"user_id\":\"1\",\"user_token\":\"de0HH4hULBnDeWjtGgbfZQ6PxDHMKwJq\",\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-13 19:16:47'),
('f3ec761d1df8caccdd33e66144', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-12 16:23:00'),
('f64dc7f0cd23ccc3620b216a04', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-08-04 14:45:41'),
('f9de3b8ea137fc13c6cb1b9f9b', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-10-04 14:12:43'),
('fd239c561bf8035e50b97dd129', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-30 08:42:57'),
('fe55b3fe828464acfb74303f43', '{\"language\":\"en-gb\",\"currency\":\"USD\"}', '2017-07-17 17:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `oc_setting`
--

CREATE TABLE `oc_setting` (
  `setting_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL DEFAULT '0',
  `code` varchar(128) NOT NULL,
  `key` varchar(128) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_setting`
--

INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(214762, 0, 'config', 'config_error_log', '1', 0),
(214763, 0, 'config', 'config_error_filename', 'error.log', 0),
(214947, 0, 't1d', 't1d_button_text_color', '#222222', 0),
(214758, 0, 'config', 'config_file_max_size', '300000', 0),
(214759, 0, 'config', 'config_file_ext_allowed', 'zip\r\ntxt\r\npng\r\njpe\r\njpeg\r\njpg\r\ngif\r\nbmp\r\nico\r\ntiff\r\ntif\r\nsvg\r\nsvgz\r\nzip\r\nrar\r\nmsi\r\ncab\r\nmp3\r\nqt\r\nmov\r\npdf\r\npsd\r\nai\r\neps\r\nps\r\ndoc', 0),
(214760, 0, 'config', 'config_file_mime_allowed', 'text/plain\r\nimage/png\r\nimage/jpeg\r\nimage/gif\r\nimage/bmp\r\nimage/tiff\r\nimage/svg+xml\r\napplication/zip\r\n&quot;application/zip&quot;\r\napplication/x-zip\r\n&quot;application/x-zip&quot;\r\napplication/x-zip-compressed\r\n&quot;application/x-zip-compressed&quot;\r\napplication/rar\r\n&quot;application/rar&quot;\r\napplication/x-rar\r\n&quot;application/x-rar&quot;\r\napplication/x-rar-compressed\r\n&quot;application/x-rar-compressed&quot;\r\napplication/octet-stream\r\n&quot;application/octet-stream&quot;\r\naudio/mpeg\r\nvideo/quicktime\r\napplication/pdf', 0),
(4, 0, 'voucher', 'total_voucher_sort_order', '8', 0),
(5, 0, 'voucher', 'total_voucher_status', '1', 0),
(214753, 0, 'config', 'config_compression', '0', 0),
(214754, 0, 'config', 'config_secure', '0', 0),
(214755, 0, 'config', 'config_password', '1', 0),
(214756, 0, 'config', 'config_shared', '0', 0),
(214757, 0, 'config', 'config_encryption', 'fYPFdc2L8LgrmVrBw7HIAI64jPf2xQJQjwP6moGXRkmKldtEPnX2OS4Cyy0MXHimDy1Em6K2f4c2YozgDejht3AGcC2ZLYWDY0IjTczTZx6fKS8uYKLnTTJKnKNBSQENjmvbJjxHN2kt3uzVALWTWBEbxHF8B4WerZX1VFVeT9im8IZjfWR6lWsVlFscknrZiySAJ0ul7gpQbCeQAJtjYEVfL8efZF1cfpkYDj2WnEhzOIFXlTFA7C0FT3D5RZt9f1acMcVw5BgCSrHjAumsIm5zSjMjMchEVTH0wdVGRF0XdQT3vRwPx4NBMHcXmuBrfguZHnrYO9MPYQFrG9qzSNlxUxDdqJuUGC8eEznD4qnnrhgcWBZ0eA06h8U6w1mRL3z1Huy9oO7YhlPFUmW28UQD8Si2TOFOrE8UW1uwmoP4aEIn0lqmKKNNUWXL63UuUPm80h7jCYXvoFuSq6MD4Zq43j5xoWwt8ZJrG7AcW0FDCyQNaSmMhrmXuSLo5dpoe8SGlXvVKk9dqlE2dGzBP1tbLSBVqoHRIeUggkdOE12xfuGTS2jddJo5Cven7VM78hSZrALVYP4H2vcvHOPw4gJmWkXSOgpnColQyXknAYoj6HrpbZQiYE9LwKH6HGOvGGCylJ7k2HZxQcOI5uQWe2gT2wPd8oonwKubk1Yl1hWeLXG8lJ6Q5ib4poketdOEi0xbuG0LjzagtzhfSh3SMGpdrjue5nyQZbBusX3Knsu6R3IquA367u1y0QmQk6byU1CW8jsV8UahxY0U3462npkOW6vfsx7DrZGmgJszyFd6WvbYnyUSN2ApLdtj3h6ETTjeqFoxMoO66k7Sr2brIgpZSnncQRSTm0aOaX6PuVcxjJInN7nkuRX0rVVr7LVbVnH7Znwx29lgt20MK17RriF7PUMUs3fIs2WGkUeO7WlEQAPBLVsM1I4Vwzs5OVndhzttCNKOLMQIclud1RNxDSw3X2pk5nps7zyhkZoQYVRBFQWlaDnNOQb2cURtsz7h', 0),
(214752, 0, 'config', 'config_robots', 'abot\r\ndbot\r\nebot\r\nhbot\r\nkbot\r\nlbot\r\nmbot\r\nnbot\r\nobot\r\npbot\r\nrbot\r\nsbot\r\ntbot\r\nvbot\r\nybot\r\nzbot\r\nbot.\r\nbot/\r\n_bot\r\n.bot\r\n/bot\r\n-bot\r\n:bot\r\n(bot\r\ncrawl\r\nslurp\r\nspider\r\nseek\r\naccoona\r\nacoon\r\nadressendeutschland\r\nah-ha.com\r\nahoy\r\naltavista\r\nananzi\r\nanthill\r\nappie\r\narachnophilia\r\narale\r\naraneo\r\naranha\r\narchitext\r\naretha\r\narks\r\nasterias\r\natlocal\r\natn\r\natomz\r\naugurfind\r\nbackrub\r\nbannana_bot\r\nbaypup\r\nbdfetch\r\nbig brother\r\nbiglotron\r\nbjaaland\r\nblackwidow\r\nblaiz\r\nblog\r\nblo.\r\nbloodhound\r\nboitho\r\nbooch\r\nbradley\r\nbutterfly\r\ncalif\r\ncassandra\r\nccubee\r\ncfetch\r\ncharlotte\r\nchurl\r\ncienciaficcion\r\ncmc\r\ncollective\r\ncomagent\r\ncombine\r\ncomputingsite\r\ncsci\r\ncurl\r\ncusco\r\ndaumoa\r\ndeepindex\r\ndelorie\r\ndepspid\r\ndeweb\r\ndie blinde kuh\r\ndigger\r\nditto\r\ndmoz\r\ndocomo\r\ndownload express\r\ndtaagent\r\ndwcp\r\nebiness\r\nebingbong\r\ne-collector\r\nejupiter\r\nemacs-w3 search engine\r\nesther\r\nevliya celebi\r\nezresult\r\nfalcon\r\nfelix ide\r\nferret\r\nfetchrover\r\nfido\r\nfindlinks\r\nfireball\r\nfish search\r\nfouineur\r\nfunnelweb\r\ngazz\r\ngcreep\r\ngenieknows\r\ngetterroboplus\r\ngeturl\r\nglx\r\ngoforit\r\ngolem\r\ngrabber\r\ngrapnel\r\ngralon\r\ngriffon\r\ngromit\r\ngrub\r\ngulliver\r\nhamahakki\r\nharvest\r\nhavindex\r\nhelix\r\nheritrix\r\nhku www octopus\r\nhomerweb\r\nhtdig\r\nhtml index\r\nhtml_analyzer\r\nhtmlgobble\r\nhubater\r\nhyper-decontextualizer\r\nia_archiver\r\nibm_planetwide\r\nichiro\r\niconsurf\r\niltrovatore\r\nimage.kapsi.net\r\nimagelock\r\nincywincy\r\nindexer\r\ninfobee\r\ninformant\r\ningrid\r\ninktomisearch.com\r\ninspector web\r\nintelliagent\r\ninternet shinchakubin\r\nip3000\r\niron33\r\nisraeli-search\r\nivia\r\njack\r\njakarta\r\njavabee\r\njetbot\r\njumpstation\r\nkatipo\r\nkdd-explorer\r\nkilroy\r\nknowledge\r\nkototoi\r\nkretrieve\r\nlabelgrabber\r\nlachesis\r\nlarbin\r\nlegs\r\nlibwww\r\nlinkalarm\r\nlink validator\r\nlinkscan\r\nlockon\r\nlwp\r\nlycos\r\nmagpie\r\nmantraagent\r\nmapoftheinternet\r\nmarvin/\r\nmattie\r\nmediafox\r\nmediapartners\r\nmercator\r\nmerzscope\r\nmicrosoft url control\r\nminirank\r\nmiva\r\nmj12\r\nmnogosearch\r\nmoget\r\nmonster\r\nmoose\r\nmotor\r\nmultitext\r\nmuncher\r\nmuscatferret\r\nmwd.search\r\nmyweb\r\nnajdi\r\nnameprotect\r\nnationaldirectory\r\nnazilla\r\nncsa beta\r\nnec-meshexplorer\r\nnederland.zoek\r\nnetcarta webmap engine\r\nnetmechanic\r\nnetresearchserver\r\nnetscoop\r\nnewscan-online\r\nnhse\r\nnokia6682/\r\nnomad\r\nnoyona\r\nnutch\r\nnzexplorer\r\nobjectssearch\r\noccam\r\nomni\r\nopen text\r\nopenfind\r\nopenintelligencedata\r\norb search\r\nosis-project\r\npack rat\r\npageboy\r\npagebull\r\npage_verifier\r\npanscient\r\nparasite\r\npartnersite\r\npatric\r\npear.\r\npegasus\r\nperegrinator\r\npgp key agent\r\nphantom\r\nphpdig\r\npicosearch\r\npiltdownman\r\npimptrain\r\npinpoint\r\npioneer\r\npiranha\r\nplumtreewebaccessor\r\npogodak\r\npoirot\r\npompos\r\npoppelsdorf\r\npoppi\r\npopular iconoclast\r\npsycheclone\r\npublisher\r\npython\r\nrambler\r\nraven search\r\nroach\r\nroad runner\r\nroadhouse\r\nrobbie\r\nrobofox\r\nrobozilla\r\nrules\r\nsalty\r\nsbider\r\nscooter\r\nscoutjet\r\nscrubby\r\nsearch.\r\nsearchprocess\r\nsemanticdiscovery\r\nsenrigan\r\nsg-scout\r\nshai\'hulud\r\nshark\r\nshopwiki\r\nsidewinder\r\nsift\r\nsilk\r\nsimmany\r\nsite searcher\r\nsite valet\r\nsitetech-rover\r\nskymob.com\r\nsleek\r\nsmartwit\r\nsna-\r\nsnappy\r\nsnooper\r\nsohu\r\nspeedfind\r\nsphere\r\nsphider\r\nspinner\r\nspyder\r\nsteeler/\r\nsuke\r\nsuntek\r\nsupersnooper\r\nsurfnomore\r\nsven\r\nsygol\r\nszukacz\r\ntach black widow\r\ntarantula\r\ntempleton\r\n/teoma\r\nt-h-u-n-d-e-r-s-t-o-n-e\r\ntheophrastus\r\ntitan\r\ntitin\r\ntkwww\r\ntoutatis\r\nt-rex\r\ntutorgig\r\ntwiceler\r\ntwisted\r\nucsd\r\nudmsearch\r\nurl check\r\nupdated\r\nvagabondo\r\nvalkyrie\r\nverticrawl\r\nvictoria\r\nvision-search\r\nvolcano\r\nvoyager/\r\nvoyager-hc\r\nw3c_validator\r\nw3m2\r\nw3mir\r\nwalker\r\nwallpaper\r\nwanderer\r\nwauuu\r\nwavefire\r\nweb core\r\nweb hopper\r\nweb wombat\r\nwebbandit\r\nwebcatcher\r\nwebcopy\r\nwebfoot\r\nweblayers\r\nweblinker\r\nweblog monitor\r\nwebmirror\r\nwebmonkey\r\nwebquest\r\nwebreaper\r\nwebsitepulse\r\nwebsnarf\r\nwebstolperer\r\nwebvac\r\nwebwalk\r\nwebwatch\r\nwebwombat\r\nwebzinger\r\nwhizbang\r\nwhowhere\r\nwild ferret\r\nworldlight\r\nwwwc\r\nwwwster\r\nxenu\r\nxget\r\nxift\r\nxirq\r\nyandex\r\nyanga\r\nyeti\r\nyodao\r\nzao\r\nzippp\r\nzyborg', 0),
(214751, 0, 'config', 'config_seo_url', '1', 0),
(214750, 0, 'config', 'config_maintenance', '0', 0),
(214749, 0, 'config', 'config_mail_alert_email', '', 0),
(214748, 0, 'config', 'config_mail_alert', '[\"order\"]', 1),
(95, 0, 'payment_free_checkout', 'payment_free_checkout_status', '1', 0),
(96, 0, 'payment_free_checkout', 'free_checkout_order_status_id', '1', 0),
(97, 0, 'payment_free_checkout', 'payment_free_checkout_sort_order', '1', 0),
(98, 0, 'payment_cod', 'payment_cod_sort_order', '5', 0),
(99, 0, 'payment_cod', 'payment_cod_total', '0.01', 0),
(100, 0, 'payment_cod', 'payment_cod_order_status_id', '1', 0),
(101, 0, 'payment_cod', 'payment_cod_geo_zone_id', '0', 0),
(102, 0, 'payment_cod', 'payment_cod_status', '1', 0),
(103, 0, 'shipping_flat', 'shipping_flat_sort_order', '1', 0),
(104, 0, 'shipping_flat', 'shipping_flat_status', '1', 0),
(105, 0, 'shipping_flat', 'shipping_flat_geo_zone_id', '0', 0),
(106, 0, 'shipping_flat', 'shipping_flat_tax_class_id', '9', 0),
(107, 0, 'shipping_flat', 'shipping_flat_cost', '5.00', 0),
(108, 0, 'total_shipping', 'total_shipping_sort_order', '3', 0),
(109, 0, 'total_sub_total', 'sub_total_sort_order', '1', 0),
(110, 0, 'total_sub_total', 'total_sub_total_status', '1', 0),
(111, 0, 'total_tax', 'total_tax_status', '1', 0),
(112, 0, 'total_total', 'total_total_sort_order', '9', 0),
(113, 0, 'total_total', 'total_total_status', '1', 0),
(114, 0, 'total_tax', 'total_tax_sort_order', '5', 0),
(115, 0, 'total_credit', 'total_credit_sort_order', '7', 0),
(116, 0, 'total_credit', 'total_credit_status', '1', 0),
(117, 0, 'total_reward', 'total_reward_sort_order', '2', 0),
(118, 0, 'total_reward', 'total_reward_status', '1', 0),
(119, 0, 'total_shipping', 'total_shipping_status', '1', 0),
(120, 0, 'total_shipping', 'total_shipping_estimator', '1', 0),
(121, 0, 'total_coupon', 'total_coupon_sort_order', '4', 0),
(122, 0, 'total_coupon', 'total_coupon_status', '1', 0),
(123, 0, 'module_category', 'module_category_status', '1', 0),
(124, 0, 'module_account', 'module_account_status', '1', 0),
(125, 0, 'theme_default', 'theme_default_product_limit', '15', 0),
(126, 0, 'theme_default', 'theme_default_product_description_length', '100', 0),
(127, 0, 'theme_default', 'theme_default_image_thumb_width', '228', 0),
(128, 0, 'theme_default', 'theme_default_image_thumb_height', '228', 0),
(129, 0, 'theme_default', 'theme_default_image_popup_width', '500', 0),
(130, 0, 'theme_default', 'theme_default_image_popup_height', '500', 0),
(131, 0, 'theme_default', 'theme_default_image_category_width', '80', 0),
(132, 0, 'theme_default', 'theme_default_image_category_height', '80', 0),
(133, 0, 'theme_default', 'theme_default_image_product_width', '228', 0),
(134, 0, 'theme_default', 'theme_default_image_product_height', '228', 0),
(135, 0, 'theme_default', 'theme_default_image_additional_width', '74', 0),
(136, 0, 'theme_default', 'theme_default_image_additional_height', '74', 0),
(137, 0, 'theme_default', 'theme_default_image_related_width', '200', 0),
(138, 0, 'theme_default', 'theme_default_image_related_height', '200', 0),
(139, 0, 'theme_default', 'theme_default_image_compare_width', '90', 0),
(140, 0, 'theme_default', 'theme_default_image_compare_height', '90', 0),
(141, 0, 'theme_default', 'theme_default_image_wishlist_width', '47', 0),
(142, 0, 'theme_default', 'theme_default_image_wishlist_height', '47', 0),
(143, 0, 'theme_default', 'theme_default_image_cart_height', '47', 0),
(144, 0, 'theme_default', 'theme_default_image_cart_width', '47', 0),
(145, 0, 'theme_default', 'theme_default_image_location_height', '50', 0),
(146, 0, 'theme_default', 'theme_default_image_location_width', '268', 0),
(147, 0, 'theme_default', 'theme_default_directory', 'default', 0),
(148, 0, 'theme_default', 'theme_default_status', '1', 0),
(149, 0, 'dashboard_activity', 'dashboard_activity_status', '1', 0),
(150, 0, 'dashboard_activity', 'dashboard_activity_sort_order', '7', 0),
(151, 0, 'dashboard_sale', 'dashboard_sale_status', '1', 0),
(152, 0, 'dashboard_sale', 'dashboard_sale_width', '3', 0),
(153, 0, 'dashboard_chart', 'dashboard_chart_status', '1', 0),
(154, 0, 'dashboard_chart', 'dashboard_chart_width', '6', 0),
(155, 0, 'dashboard_customer', 'dashboard_customer_status', '1', 0),
(156, 0, 'dashboard_customer', 'dashboard_customer_width', '3', 0),
(157, 0, 'dashboard_map', 'dashboard_map_status', '1', 0),
(158, 0, 'dashboard_map', 'dashboard_map_width', '6', 0),
(159, 0, 'dashboard_online', 'dashboard_online_status', '1', 0),
(160, 0, 'dashboard_online', 'dashboard_online_width', '3', 0),
(161, 0, 'dashboard_order', 'dashboard_order_sort_order', '1', 0),
(162, 0, 'dashboard_order', 'dashboard_order_status', '1', 0),
(163, 0, 'dashboard_order', 'dashboard_order_width', '3', 0),
(164, 0, 'dashboard_sale', 'dashboard_sale_sort_order', '2', 0),
(165, 0, 'dashboard_customer', 'dashboard_customer_sort_order', '3', 0),
(166, 0, 'dashboard_online', 'dashboard_online_sort_order', '4', 0),
(167, 0, 'dashboard_map', 'dashboard_map_sort_order', '5', 0),
(168, 0, 'dashboard_chart', 'dashboard_chart_sort_order', '6', 0),
(169, 0, 'dashboard_recent', 'dashboard_recent_status', '1', 0),
(170, 0, 'dashboard_recent', 'dashboard_recent_sort_order', '8', 0),
(171, 0, 'dashboard_activity', 'dashboard_activity_width', '4', 0),
(172, 0, 'dashboard_recent', 'dashboard_recent_width', '8', 0),
(173, 0, 'report_customer_activity', 'report_customer_activity_status', '1', 0),
(174, 0, 'report_customer_activity', 'report_customer_activity_sort_order', '1', 0),
(175, 0, 'report_customer_order', 'report_customer_order_status', '1', 0),
(176, 0, 'report_customer_order', 'report_customer_order_sort_order', '2', 0),
(177, 0, 'report_customer_reward', 'report_customer_reward_status', '1', 0),
(178, 0, 'report_customer_reward', 'report_customer_reward_sort_order', '3', 0),
(179, 0, 'report_customer_search', 'report_customer_search_sort_order', '3', 0),
(180, 0, 'report_customer_search', 'report_customer_search_status', '1', 0),
(181, 0, 'report_customer_transaction', 'report_customer_transaction_status', '1', 0),
(182, 0, 'report_customer_transaction', 'report_customer_transaction_status_sort_order', '4', 0),
(183, 0, 'report_sale_tax', 'report_sale_tax_status', '1', 0),
(184, 0, 'report_sale_tax', 'report_sale_tax_sort_order', '5', 0),
(185, 0, 'report_sale_shipping', 'report_sale_shipping_status', '1', 0),
(186, 0, 'report_sale_shipping', 'report_sale_shipping_sort_order', '6', 0),
(187, 0, 'report_sale_return', 'report_sale_return_status', '1', 0),
(188, 0, 'report_sale_return', 'report_sale_return_sort_order', '7', 0),
(189, 0, 'report_sale_order', 'report_sale_order_status', '1', 0),
(190, 0, 'report_sale_order', 'report_sale_order_sort_order', '8', 0),
(191, 0, 'report_sale_coupon', 'report_sale_coupon_status', '1', 0),
(192, 0, 'report_sale_coupon', 'report_sale_coupon_sort_order', '9', 0),
(193, 0, 'report_product_viewed', 'report_product_viewed_status', '1', 0),
(194, 0, 'report_product_viewed', 'report_product_viewed_sort_order', '10', 0),
(195, 0, 'report_product_purchased', 'report_product_purchased_status', '1', 0),
(196, 0, 'report_product_purchased', 'report_product_purchased_sort_order', '11', 0),
(197, 0, 'report_marketing', 'report_marketing_status', '1', 0),
(198, 0, 'report_marketing', 'report_marketing_sort_order', '12', 0),
(162313, 0, 'developer', 'developer_sass', '0', 0),
(214747, 0, 'config', 'config_mail_smtp_timeout', '5', 0),
(214746, 0, 'config', 'config_mail_smtp_port', '25', 0),
(214745, 0, 'config', 'config_mail_smtp_password', '', 0),
(214744, 0, 'config', 'config_mail_smtp_username', '', 0),
(214743, 0, 'config', 'config_mail_smtp_hostname', '', 0),
(214742, 0, 'config', 'config_mail_parameter', '', 0),
(214741, 0, 'config', 'config_mail_engine', 'mail', 0),
(214740, 0, 'config', 'config_icon', 'catalog/Logo/logomg.png', 0),
(214739, 0, 'config', 'config_logo', 'catalog/Logo/logomg.png', 0),
(214737, 0, 'config', 'config_captcha', '', 0),
(214738, 0, 'config', 'config_captcha_page', '[\"review\",\"return\",\"contact\"]', 1),
(214736, 0, 'config', 'config_return_status_id', '2', 0),
(214735, 0, 'config', 'config_return_id', '0', 0),
(214734, 0, 'config', 'config_affiliate_id', '4', 0),
(214733, 0, 'config', 'config_affiliate_commission', '5', 0),
(214732, 0, 'config', 'config_affiliate_auto', '0', 0),
(214731, 0, 'config', 'config_affiliate_approval', '0', 0),
(214730, 0, 'config', 'config_affiliate_group_id', '1', 0),
(214729, 0, 'config', 'config_stock_checkout', '0', 0),
(214728, 0, 'config', 'config_stock_warning', '0', 0),
(214726, 0, 'config', 'config_api_id', '1', 0),
(214727, 0, 'config', 'config_stock_display', '0', 0),
(214725, 0, 'config', 'config_fraud_status_id', '7', 0),
(214724, 0, 'config', 'config_complete_status', '[\"5\",\"3\"]', 1),
(214723, 0, 'config', 'config_processing_status', '[\"5\",\"1\",\"2\",\"12\",\"3\"]', 1),
(214722, 0, 'config', 'config_order_status_id', '1', 0),
(214721, 0, 'config', 'config_checkout_id', '5', 0),
(214718, 0, 'config', 'config_invoice_prefix', 'INV-2017-00', 0),
(214719, 0, 'config', 'config_cart_weight', '1', 0),
(214720, 0, 'config', 'config_checkout_guest', '1', 0),
(214764, 0, 't1d_store_id', 't1d_store_id', '0', 0),
(199707, 0, 'theme_oxy', 'theme_oxy_image_location_height', '50', 0),
(212930, 0, 'module_information', 'module_information_status', '1', 0),
(199704, 0, 'theme_oxy', 'theme_oxy_image_cart_width', '52', 0),
(199705, 0, 'theme_oxy', 'theme_oxy_image_cart_height', '52', 0),
(199706, 0, 'theme_oxy', 'theme_oxy_image_location_width', '268', 0),
(199703, 0, 'theme_oxy', 'theme_oxy_image_wishlist_height', '52', 0),
(199702, 0, 'theme_oxy', 'theme_oxy_image_wishlist_width', '52', 0),
(199701, 0, 'theme_oxy', 'theme_oxy_image_compare_height', '90', 0),
(199700, 0, 'theme_oxy', 'theme_oxy_image_compare_width', '90', 0),
(199698, 0, 'theme_oxy', 'theme_oxy_image_related_width', '300', 0),
(199699, 0, 'theme_oxy', 'theme_oxy_image_related_height', '300', 0),
(199697, 0, 'theme_oxy', 'theme_oxy_image_additional_height', '600', 0),
(199696, 0, 'theme_oxy', 'theme_oxy_image_additional_width', '600', 0),
(199695, 0, 'theme_oxy', 'theme_oxy_image_product_height', '300', 0),
(199694, 0, 'theme_oxy', 'theme_oxy_image_product_width', '300', 0),
(199693, 0, 'theme_oxy', 'theme_oxy_image_popup_height', '600', 0),
(199692, 0, 'theme_oxy', 'theme_oxy_image_popup_width', '600', 0),
(199691, 0, 'theme_oxy', 'theme_oxy_image_thumb_height', '525', 0),
(199690, 0, 'theme_oxy', 'theme_oxy_image_thumb_width', '525', 0),
(199689, 0, 'theme_oxy', 'theme_oxy_image_category_height', '140', 0),
(199684, 0, 'theme_oxy', 'theme_oxy_directory', 'oxy', 0),
(199685, 0, 'theme_oxy', 'theme_oxy_status', '1', 0),
(199686, 0, 'theme_oxy', 'theme_oxy_product_limit', '10', 0),
(199687, 0, 'theme_oxy', 'theme_oxy_product_description_length', '160', 0),
(199688, 0, 'theme_oxy', 'theme_oxy_image_category_width', '200', 0),
(214717, 0, 'config', 'config_account_id', '3', 0),
(214716, 0, 'config', 'config_login_attempts', '5', 0),
(214715, 0, 'config', 'config_customer_price', '0', 0),
(214713, 0, 'config', 'config_customer_group_id', '1', 0),
(214714, 0, 'config', 'config_customer_group_display', '[\"1\"]', 1),
(214712, 0, 'config', 'config_customer_search', '0', 0),
(214711, 0, 'config', 'config_customer_activity', '0', 0),
(214710, 0, 'config', 'config_customer_online', '0', 0),
(214709, 0, 'config', 'config_tax_customer', 'shipping', 0),
(214946, 0, 't1d', 't1d_button_bg_hover_color', '#FC0012', 0),
(214945, 0, 't1d', 't1d_button_bg_color', '#F7F6F2', 0),
(214944, 0, 't1d', 't1d_button_bg_status', '1', 0),
(214943, 0, 't1d', 't1d_button_shadow_status', '1', 0),
(214942, 0, 't1d', 't1d_button_border_radius', '4', 0),
(214941, 0, 't1d', 't1d_price_new_color', '#FC0012', 0),
(214940, 0, 't1d', 't1d_price_old_color', '#B6B6B6', 0),
(214939, 0, 't1d', 't1d_price_color', '#222222', 0),
(214938, 0, 't1d', 't1d_f5_border_top_color', '#FFFFFF', 0),
(214937, 0, 't1d', 't1d_f5_border_top_size', '1', 0),
(214936, 0, 't1d', 't1d_f5_border_top_status', '0', 0),
(214935, 0, 't1d', 't1d_f5_link_hover_color', '#FC0012', 0),
(214934, 0, 't1d', 't1d_f5_link_color', '#222222', 0),
(214933, 0, 't1d', 't1d_f5_text_color', '#222222', 0),
(214932, 0, 't1d', 't1d_f5_bg_color', '#F7F6F2', 0),
(214931, 0, 't1d', 't1d_f5_bg_color_status', '1', 0),
(214930, 0, 't1d', 't1d_f4_border_top_color', '#FFFFFF', 0),
(214929, 0, 't1d', 't1d_f4_border_top_size', '1', 0),
(214928, 0, 't1d', 't1d_f4_border_top_status', '1', 0),
(214927, 0, 't1d', 't1d_f4_link_hover_color', '#FC0012', 0),
(214926, 0, 't1d', 't1d_f4_link_color', '#222222', 0),
(214925, 0, 't1d', 't1d_f4_text_color', '#B6B6B6', 0),
(214924, 0, 't1d', 't1d_f4_bg_color', '#FFFFFF', 0),
(214923, 0, 't1d', 't1d_f4_bg_color_status', '1', 0),
(214922, 0, 't1d', 't1d_f3_border_top_color', '#FFFFFF', 0),
(214921, 0, 't1d', 't1d_f3_border_top_size', '1', 0),
(214920, 0, 't1d', 't1d_f3_border_top_status', '0', 0),
(214919, 0, 't1d', 't1d_f3_payment_images_style', '1', 0),
(214918, 0, 't1d', 't1d_f3_icons_social_style', '1', 0),
(214917, 0, 't1d', 't1d_f3_icons_bg_color', '#333333', 0),
(214916, 0, 't1d', 't1d_f3_link_hover_color', '#FC0012', 0),
(214915, 0, 't1d', 't1d_f3_link_color', '#222222', 0),
(214914, 0, 't1d', 't1d_f3_text_color', '#222222', 0),
(214913, 0, 't1d', 't1d_f3_bg_color', '#FFFFFF', 0),
(214912, 0, 't1d', 't1d_f3_bg_color_status', '1', 0),
(214911, 0, 't1d', 't1d_f2_border_top_color', '#FFFFFF', 0),
(214910, 0, 't1d', 't1d_f2_border_top_size', '1', 0),
(214909, 0, 't1d', 't1d_f2_border_top_status', '1', 0),
(214908, 0, 't1d', 't1d_f2_link_hover_color', '#FC0012', 0),
(214907, 0, 't1d', 't1d_f2_link_color', '#222222', 0),
(214906, 0, 't1d', 't1d_f2_text_color', '#222222', 0),
(214905, 0, 't1d', 't1d_f2_titles_border_color', '#FFFFFF', 0),
(214904, 0, 't1d', 't1d_f2_titles_border_size', '1', 0),
(214903, 0, 't1d', 't1d_f2_titles_border_status', '0', 0),
(214902, 0, 't1d', 't1d_f2_titles_color', '#FC0012', 0),
(214901, 0, 't1d', 't1d_f2_bg_color', '#F7F6F2', 0),
(214900, 0, 't1d', 't1d_f2_bg_color_status', '1', 0),
(214899, 0, 't1d', 't1d_f6_border_top_color', '#F7F6F2', 0),
(214898, 0, 't1d', 't1d_f6_border_top_size', '1', 0),
(214897, 0, 't1d', 't1d_f6_border_top_status', '1', 0),
(214896, 0, 't1d', 't1d_f6_link_hover_color', '#FC0012', 0),
(214895, 0, 't1d', 't1d_f6_link_color', '#222222', 0),
(214894, 0, 't1d', 't1d_f6_text_color', '#222222', 0),
(214893, 0, 't1d', 't1d_f6_bg_color', '#FFFFFF', 0),
(214892, 0, 't1d', 't1d_f6_bg_color_status', '0', 0),
(214891, 0, 't1d', 't1d_mid_prod_stars_color', '#FC0012', 0),
(214890, 0, 't1d', 't1d_mid_prod_box_out_of_stock_icon_color', '#FFFFFF', 0),
(214889, 0, 't1d', 't1d_mid_prod_box_new_icon_color', '#7B8D63', 0),
(214888, 0, 't1d', 't1d_mid_prod_box_sale_icon_color', '#A4496C', 0),
(214887, 0, 't1d', 't1d_mm_sub_box_shadow', '1', 0),
(214886, 0, 't1d', 't1d_mm_sub_border_status', '0', 0),
(214885, 0, 't1d', 't1d_mm_sub_subcategory_border_status', '1', 0),
(214884, 0, 't1d', 't1d_mm_sub_subcategory_color', '#B6B6B6', 0),
(214883, 0, 't1d', 't1d_mm_sub_main_category_border_status', '1', 0),
(214882, 0, 't1d', 't1d_mm_sub_link_hover_color', '#FC0012', 0),
(214881, 0, 't1d', 't1d_mm_sub_link_color', '#222222', 0),
(214880, 0, 't1d', 't1d_mm_sub_text_color', '#222222', 0),
(214879, 0, 't1d', 't1d_mm_sub_titles_bg_color', '#F7F6F2', 0),
(214878, 0, 't1d', 't1d_mm_sub_bg_color', '#FFFFFF', 0),
(214877, 0, 't1d', 't1d_mm4_link_hover_color', '#FC0012', 0),
(214876, 0, 't1d', 't1d_mm4_link_color', '#222222', 0),
(214875, 0, 't1d', 't1d_mm4_bg_hover_color', '#F7F6F2', 0),
(214874, 0, 't1d', 't1d_mm4_bg_color', '#F7F6F2', 0),
(214873, 0, 't1d', 't1d_mm4_bg_color_status', '0', 0),
(214872, 0, 't1d', 't1d_mm5_link_hover_color', '#FC0012', 0),
(214871, 0, 't1d', 't1d_mm5_link_color', '#222222', 0),
(214870, 0, 't1d', 't1d_mm5_bg_hover_color', '#F7F6F2', 0),
(214869, 0, 't1d', 't1d_mm5_bg_color', '#F7F6F2', 0),
(214868, 0, 't1d', 't1d_mm5_bg_color_status', '0', 0),
(214867, 0, 't1d', 't1d_mm6_link_hover_color', '#FC0012', 0),
(214866, 0, 't1d', 't1d_mm6_link_color', '#222222', 0),
(214865, 0, 't1d', 't1d_mm6_bg_hover_color', '#F7F6F2', 0),
(214864, 0, 't1d', 't1d_mm6_bg_color', '#F7F6F2', 0),
(214863, 0, 't1d', 't1d_mm6_bg_color_status', '0', 0),
(214862, 0, 't1d', 't1d_mm3_link_hover_color', '#FC0012', 0),
(214861, 0, 't1d', 't1d_mm3_link_color', '#222222', 0),
(214860, 0, 't1d', 't1d_mm3_bg_hover_color', '#F7F6F2', 0),
(214859, 0, 't1d', 't1d_mm3_bg_color', '#F7F6F2', 0),
(214858, 0, 't1d', 't1d_mm3_bg_color_status', '0', 0),
(215027, 0, 't1o_store_id', 't1o_store_id', '0', 0),
(214857, 0, 't1d', 't1d_mm2_link_hover_color', '#FFFFFF', 0),
(7735, 0, 'module_filter', 'module_filter_status', '1', 0),
(214856, 0, 't1d', 't1d_mm2_link_color', '#FFFFFF', 0),
(214855, 0, 't1d', 't1d_mm2_bg_hover_color', '#A4496C', 0),
(214854, 0, 't1d', 't1d_mm2_bg_color', '#A4496C', 0),
(214853, 0, 't1d', 't1d_mm2_bg_color_status', '1', 0),
(214852, 0, 't1d', 't1d_mm1_link_hover_color', '#FFFFFF', 0),
(214851, 0, 't1d', 't1d_mm1_link_color', '#FFFFFF', 0),
(214850, 0, 't1d', 't1d_mm1_bg_hover_color', '#FC0012', 0),
(214849, 0, 't1d', 't1d_mm1_bg_color', '#FC0012', 0),
(214848, 0, 't1d', 't1d_mm1_bg_color_status', '1', 0),
(214847, 0, 't1d', 't1d_mm_sort_down_icon_status', '1', 0),
(214846, 0, 't1d', 't1d_mm_shadow_status', '1', 0),
(214845, 0, 't1d', 't1d_mm_border_bottom_color', '#555555', 0),
(214844, 0, 't1d', 't1d_mm_border_bottom_size', '1', 0),
(214843, 0, 't1d', 't1d_mm_border_bottom_status', '0', 0),
(214697, 0, 'config', 'config_currency', 'VND', 0),
(214698, 0, 'config', 'config_currency_auto', '1', 0),
(214699, 0, 'config', 'config_length_class_id', '1', 0),
(214700, 0, 'config', 'config_weight_class_id', '1', 0),
(214701, 0, 'config', 'config_product_count', '1', 0),
(214702, 0, 'config', 'config_limit_admin', '200', 0),
(214703, 0, 'config', 'config_review_status', '1', 0),
(214842, 0, 't1d', 't1d_mm_border_top_color', '#555555', 0),
(214841, 0, 't1d', 't1d_mm_border_top_size', '1', 0),
(214840, 0, 't1d', 't1d_mm_border_top_status', '0', 0),
(214839, 0, 't1d', 't1d_mm_separator_color', '#555555', 0),
(214838, 0, 't1d', 't1d_mm_separator_size', '1', 0),
(214837, 0, 't1d', 't1d_mm_separator_status', '0', 0),
(214836, 0, 't1d', 't1d_mm_bg_color', '#F7F6F2', 0),
(214835, 0, 't1d', 't1d_mm_bg_color_status', '0', 0),
(214834, 0, 't1d', 't1d_top_area_tb_icons_color', '#FC0012', 0),
(214833, 0, 't1d', 't1d_top_area_tb_link_color_hover', '#FC0012', 0),
(214832, 0, 't1d', 't1d_top_area_tb_link_color', '#FFFFFF', 0),
(214704, 0, 'config', 'config_review_guest', '1', 0),
(214705, 0, 'config', 'config_voucher_min', '1', 0),
(214706, 0, 'config', 'config_voucher_max', '1000', 0),
(215337, 0, 't1o', 't1o_text_gallery', '{\"1\":\"Gallery\",\"2\":\"B\\u1ed9 s\\u01b0u t\\u1eadp\"}', 1),
(215338, 0, 't1o', 't1o_text_small_list', '{\"1\":\"Small List\",\"2\":\"Danh s\\u00e1ch thu nh\\u1ecf\"}', 1),
(214761, 0, 'config', 'config_error_display', '1', 0),
(214707, 0, 'config', 'config_tax', '1', 0),
(214708, 0, 'config', 'config_tax_default', 'shipping', 0),
(215339, 0, 't1o', 't1o_text_your_cart', '{\"1\":\"Your Cart:\",\"2\":\"Gi\\u1ecf h\\u00e0ng:\"}', 1),
(215336, 0, 't1o', 't1o_text_news', '{\"1\":\"News\",\"2\":\"Tin t\\u1ee9c\"}', 1),
(215335, 0, 't1o', 't1o_text_most_viewed', '{\"1\":\"Most Viewed\",\"2\":\"Xem nhi\\u1ec1u nh\\u1ea5t\"}', 1),
(215334, 0, 't1o', 't1o_text_special', '{\"1\":\"Specials\",\"2\":\"\\u0110\\u1eb7c bi\\u1eb9t\"}', 1),
(215333, 0, 't1o', 't1o_text_latest', '{\"1\":\"Latest\",\"2\":\"M\\u1edbi nh\\u1ea5t\"}', 1),
(215331, 0, 't1o', 't1o_text_bestseller', '{\"1\":\"Bestseller\",\"2\":\"B\\u00e1n nhi\\u1ec1u nh\\u1ea5t\"}', 1),
(215332, 0, 't1o', 't1o_text_featured', '{\"1\":\"Featured\",\"2\":\"S\\u1ea3n ph\\u1ea9m n\\u1ed5i b\\u1eadt\"}', 1),
(215330, 0, 't1o', 't1o_text_see_all_products_by', '{\"1\":\"See all products by\",\"2\":\"Xem t\\u1ea5t c\\u1ea3 s\\u1ea3n ph\\u1ea9m theo\"}', 1),
(215329, 0, 't1o', 't1o_text_menu_menu', '{\"1\":\"Menu\",\"2\":\"Menu\"}', 1),
(215326, 0, 't1o', 't1o_text_menu_contact_fax', '{\"1\":\"Fax\",\"2\":\"Fax\"}', 1),
(215327, 0, 't1o', 't1o_text_menu_contact_hours', '{\"1\":\"Opening Times\",\"2\":\"Gi\\u1edd m\\u1edf c\\u1eeda\"}', 1),
(215328, 0, 't1o', 't1o_text_menu_contact_form', '{\"1\":\"Contact Form\",\"2\":\"Form m\\u1eabu li\\u00ean h\\u1ec7\"}', 1),
(215325, 0, 't1o', 't1o_text_menu_contact_tel', '{\"1\":\"Telephone\",\"2\":\"\\u0110i\\u1ec7n tho\\u1ea1i\"}', 1),
(215324, 0, 't1o', 't1o_text_menu_contact_email', '{\"1\":\"E-mail\",\"2\":\"E-mail\"}', 1),
(215323, 0, 't1o', 't1o_text_menu_contact_address', '{\"1\":\"Address\",\"2\":\"\\u0110\\u1ecba ch\\u1ec9\"}', 1),
(215322, 0, 't1o', 't1o_text_contact_us', '{\"1\":\"Contact us\",\"2\":\"Li\\u00ean h\\u1ec7 v\\u1edbi ch\\u00fang t\\u00f4i\"}', 1),
(215321, 0, 't1o', 't1o_text_menu_brands', '{\"1\":\"Brands\",\"2\":\"Th\\u01b0\\u01a1ng hi\\u1ec7u\"}', 1),
(215320, 0, 't1o', 't1o_text_menu_categories', '{\"1\":\"Shop by Category\",\"2\":\"Mua theo danh m\\u1ee5c\"}', 1),
(215319, 0, 't1o', 't1o_text_product_friend', '{\"1\":\"Send to a friend\",\"2\":\"G\\u1eedi \\u0111\\u1ebfn b\\u1ea1n b\\u00e8\"}', 1),
(215318, 0, 't1o', 't1o_text_percent_saved', '{\"1\":\"You save:\",\"2\":\"B\\u1ea1n ti\\u1ebft ki\\u1ec7m:\"}', 1),
(215317, 0, 't1o', 't1o_text_old_price', '{\"1\":\"Old price:\",\"2\":\"Gi\\u00e1 c\\u0169:\"}', 1),
(215316, 0, 't1o', 't1o_text_special_price', '{\"1\":\"Special price:\",\"2\":\"Gi\\u00e1 \\u0111\\u1eb7c bi\\u1ec7t:\"}', 1),
(215315, 0, 't1o', 't1o_text_product_viewed', '{\"1\":\"Product viewed:\",\"2\":\"S\\u1ea3n ph\\u1ea9m \\u0111\\u00e3 xem:\"}', 1),
(215314, 0, 't1o', 't1o_text_previous_product', '{\"1\":\"Previous\",\"2\":\"Quay l\\u1ea1i\"}', 1),
(215313, 0, 't1o', 't1o_text_next_product', '{\"1\":\"Next\",\"2\":\"Ti\\u1ebfp theo\"}', 1),
(215311, 0, 't1o', 't1o_text_shop_now', '{\"1\":\"Shop Now\",\"2\":\"Mua ngay\"}', 1),
(215312, 0, 't1o', 't1o_text_view', '{\"1\":\"View Now\",\"2\":\"Xem ngay\"}', 1),
(215310, 0, 't1o', 't1o_text_quickview', '{\"1\":\"Quick View\",\"2\":\"Xem nhanh\"}', 1),
(215307, 0, 't1o', 't1o_custom_js', '', 0),
(215308, 0, 't1o', 't1o_text_sale', '{\"1\":\"Sale\",\"2\":\"B\\u00e1n h\\u00e0ng\"}', 1),
(215309, 0, 't1o', 't1o_text_new_prod', '{\"1\":\"New\",\"2\":\"M\\u1edbi\"}', 1),
(215282, 0, 't1o', 't1o_facebook_likebox_id', '20531316728', 0),
(215306, 0, 't1o', 't1o_custom_css', '', 0),
(215305, 0, 't1o', 't1o_eu_cookie_close', '{\"1\":\"Don\'t show me again\",\"2\":\"\"}', 1),
(215304, 0, 't1o', 't1o_eu_cookie_message', '{\"1\":{\"htmlcontent\":\"\"},\"2\":{\"htmlcontent\":\"\"}}', 1),
(215303, 0, 't1o', 't1o_eu_cookie_icon_status', '1', 0),
(215302, 0, 't1o', 't1o_eu_cookie_status', '0', 0),
(215301, 0, 't1o', 't1o_custom_box_bg', '#424242', 0),
(215300, 0, 't1o', 't1o_custom_box_content', '{\"1\":{\"htmlcontent\":\"&lt;p&gt;Custom Content Widget&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"\"}}', 1),
(215299, 0, 't1o', 't1o_custom_box_status', '0', 0),
(215298, 0, 't1o', 't1o_video_box_bg', '#E22C29', 0),
(215295, 0, 't1o', 't1o_snapchat_box_bg', '#000000', 0),
(215296, 0, 't1o', 't1o_video_box_status', '0', 0),
(215297, 0, 't1o', 't1o_video_box_content', '{\"1\":{\"htmlcontent\":\"&lt;p&gt;&lt;iframe allowfullscreen=&quot;&quot; src=&quot;\\/\\/www.youtube.com\\/embed\\/HVCGEyjqRXc&quot; width=&quot;560&quot; height=&quot;315&quot; frameborder=&quot;0&quot;&gt;&lt;\\/iframe&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"\"}}', 1),
(215294, 0, 't1o', 't1o_snapchat_box_subtitle', 'Scan Snapchat Code', 0),
(215292, 0, 't1o', 't1o_snapchat_box_code_custom', 'catalog/snapcode.png', 0),
(215293, 0, 't1o', 't1o_snapchat_box_title', 'OXY Theme', 0),
(215291, 0, 't1o', 't1o_snapchat_box_status', '0', 0),
(215290, 0, 't1o', 't1o_pinterest_box_user', 'pinterest', 0),
(215289, 0, 't1o', 't1o_pinterest_box_status', '0', 0),
(215288, 0, 't1o', 't1o_googleplus_box_user', '+envato', 0),
(215287, 0, 't1o', 't1o_googleplus_box_status', '0', 0),
(215286, 0, 't1o', 't1o_twitter_block_tweets', '1', 0),
(215285, 0, 't1o', 't1o_twitter_block_widget_id', '705034984197918720', 0),
(215284, 0, 't1o', 't1o_twitter_block_user', '@envato', 0),
(215283, 0, 't1o', 't1o_twitter_block_status', '0', 0),
(215225, 0, 't1o', 't1o_payment_block_custom', '', 0),
(215226, 0, 't1o', 't1o_payment_block_custom_url', '', 0),
(215227, 0, 't1o', 't1o_payment_paypal', '1', 0),
(215228, 0, 't1o', 't1o_payment_paypal_url', '', 0),
(215229, 0, 't1o', 't1o_payment_visa', '1', 0),
(215230, 0, 't1o', 't1o_payment_visa_url', '', 0),
(215231, 0, 't1o', 't1o_payment_mastercard', '0', 0),
(215232, 0, 't1o', 't1o_payment_mastercard_url', '', 0),
(215233, 0, 't1o', 't1o_payment_maestro', '0', 0),
(215234, 0, 't1o', 't1o_payment_maestro_url', '', 0),
(215235, 0, 't1o', 't1o_payment_discover', '0', 0),
(215236, 0, 't1o', 't1o_payment_discover_url', '', 0),
(215237, 0, 't1o', 't1o_payment_skrill', '1', 0),
(215238, 0, 't1o', 't1o_payment_skrill_url', '', 0),
(215239, 0, 't1o', 't1o_payment_american_express', '0', 0),
(215240, 0, 't1o', 't1o_payment_american_express_url', '', 0),
(215241, 0, 't1o', 't1o_payment_cirrus', '0', 0),
(215242, 0, 't1o', 't1o_payment_cirrus_url', '', 0),
(215243, 0, 't1o', 't1o_payment_delta', '0', 0),
(215244, 0, 't1o', 't1o_payment_delta_url', '', 0),
(215245, 0, 't1o', 't1o_payment_google', '1', 0),
(215246, 0, 't1o', 't1o_payment_google_url', '', 0),
(215247, 0, 't1o', 't1o_payment_2co', '0', 0),
(215248, 0, 't1o', 't1o_payment_2co_url', '', 0),
(215249, 0, 't1o', 't1o_payment_sage', '0', 0),
(215250, 0, 't1o', 't1o_payment_sage_url', '', 0),
(215251, 0, 't1o', 't1o_payment_solo', '0', 0),
(215252, 0, 't1o', 't1o_payment_solo_url', '', 0),
(215253, 0, 't1o', 't1o_payment_amazon', '0', 0),
(215254, 0, 't1o', 't1o_payment_amazon_url', '', 0),
(215255, 0, 't1o', 't1o_payment_western_union', '1', 0),
(215256, 0, 't1o', 't1o_payment_western_union_url', '', 0),
(215195, 0, 't1o', 't1o_custom_1_status', '0', 0),
(215196, 0, 't1o', 't1o_custom_1_column_width', '3', 0),
(215197, 0, 't1o', 't1o_custom_1_title', '{\"1\":\"Who we are\",\"2\":\"Who we are\"}', 1),
(215198, 0, 't1o', 't1o_custom_1_content', '{\"1\":{\"htmlcontent\":\"OXY is a Multi-Purpose, Fully Responsive OpenCart Theme, for any type of store, designed with great attention to details and usability in mind.&lt;p&gt;&lt;\\/p&gt;\\r\\n\\r\\n&lt;br&gt;&lt;p&gt;Got Questions? Call us 24\\/7: &lt;span&gt;+1 800 321 321&lt;\\/span&gt;\\r\\n&lt;br&gt;Address: &lt;span&gt;135 Park Avenue, Los Angeles CA 90024&lt;\\/span&gt;\\r\\n&lt;br&gt;Email: &lt;a href=&quot;mailto:contact@yourstore.com&quot;&gt;contact@yourstore.com&lt;\\/a&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"OXY is a Multi-Purpose, Fully Responsive OpenCart Theme, for any type of store, designed with great attention to details and usability in mind.&lt;p&gt;&lt;\\/p&gt;\\r\\n\\r\\n&lt;br&gt;&lt;p&gt;Got Questions? Call us 24\\/7: &lt;span&gt;+1 800 321 321&lt;\\/span&gt;\\r\\n&lt;br&gt;Address: &lt;span&gt;135 Park Avenue, Los Angeles CA 90024&lt;\\/span&gt;\\r\\n&lt;br&gt;Email: &lt;a href=&quot;mailto:contact@yourstore.com&quot;&gt;contact@yourstore.com&lt;\\/a&gt;&lt;\\/p&gt;\\r\\n\\r\\n&lt;p class=&quot;mobile-apps&quot;&gt;\\r\\n&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;image\\/catalog\\/mobile_app_google_play.png&quot; alt=&quot;&quot;&gt;&lt;\\/a&gt;\\r\\n&lt;a href=&quot;#&quot;&gt;&lt;img src=&quot;image\\/catalog\\/mobile_app_app_store.png&quot; alt=&quot;&quot;&gt;&lt;\\/a&gt;\\r\\n&lt;\\/p&gt;\"}}', 1),
(215280, 0, 't1o', 't1o_custom_bottom_2_footer_margin', '600', 0),
(215281, 0, 't1o', 't1o_facebook_likebox_status', '0', 0),
(215278, 0, 't1o', 't1o_custom_bottom_2_status', '0', 0),
(215279, 0, 't1o', 't1o_custom_bottom_2_content', '{\"1\":{\"htmlcontent\":\"&lt;br&gt;&lt;br&gt;&lt;br&gt;\\r\\n&lt;div style=&quot;width:65%;text-align:center;float:right;&quot;&gt;\\r\\n&lt;h2 style=&quot;font-size:58px;font-weight: 900;color:#ffffff;text-align:center;&quot;&gt;Organic Breads&lt;br&gt;&lt;span style=&quot;color:#82ce69;&quot;&gt;Up To 30%&lt;\\/span&gt;&lt;\\/h2&gt;\\r\\n\\r\\n&lt;span class=&quot;subtitle&quot; style=&quot;font-size:16px;color:#fffbea;&quot;&gt;\\r\\nAenean egestas congue est nec convallis nunc viverra duis sit amet quam&lt;br&gt;imperdiet tincidunt lorem ornare nulla pellentesque velit volutpat&lt;br&gt;ornare justo sem blandit ante et eleifend lacus mauris&lt;\\/span&gt;\\r\\n&lt;br&gt;&lt;br&gt;&lt;br&gt;\\r\\n\\r\\n&lt;a href=&quot;#&quot; class=&quot;btn btn-default&quot; target=&quot;_blank&quot;&gt;See All Products&lt;\\/a&gt;\\r\\n&lt;div&gt;&lt;\\/div&gt;&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;br&gt;&lt;br&gt;&lt;br&gt;\\r\\n&lt;div style=&quot;width:65%;text-align:center;float:right;&quot;&gt;\\r\\n&lt;h2 style=&quot;font-size:58px;font-weight: 900;color:#ffffff;text-align:center;&quot;&gt;Organic Breads&lt;br&gt;&lt;span style=&quot;color:#82ce69;&quot;&gt;Up To 30%&lt;\\/span&gt;&lt;\\/h2&gt;\\r\\n\\r\\n&lt;span class=&quot;subtitle&quot; style=&quot;font-size:16px;color:#fffbea;&quot;&gt;\\r\\nAenean egestas congue est nec convallis nunc viverra duis sit amet quam&lt;br&gt;imperdiet tincidunt lorem ornare nulla pellentesque velit volutpat&lt;br&gt;ornare justo sem blandit ante et eleifend lacus mauris&lt;\\/span&gt;\\r\\n&lt;br&gt;&lt;br&gt;&lt;br&gt;\\r\\n\\r\\n&lt;a href=&quot;#&quot; class=&quot;btn btn-default&quot; target=&quot;_blank&quot;&gt;See All Products&lt;\\/a&gt;\\r\\n&lt;div&gt;&lt;\\/div&gt;&lt;\\/div&gt;\"}}', 1),
(215277, 0, 't1o', 't1o_custom_bottom_1_content', '{\"1\":{\"htmlcontent\":\"&lt;div align=&quot;center&quot;&gt;This is a CMS block edited from theme admin panel. You can insert any content (text, images, HTML) here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor.&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;div align=&quot;center&quot;&gt;This is a CMS block edited from theme admin panel. You can insert any content (text, images, HTML) here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non dui at sapien tempor gravida ut vel arcu. Maecenas odio arcu, feugiat vel congue feugiat, laoreet vitae diam. In hac habitasse platea dictumst. Morbi consectetur nunc porta ligula tempor et varius nibh sollicitudin. Mauris risus felis, adipiscing eu consequat ac, aliquam vel urna. Duis bibendum sapien nec mi egestas tempor.&lt;\\/div&gt;\"}}', 1),
(215276, 0, 't1o', 't1o_custom_bottom_1_status', '0', 0),
(215275, 0, 't1o', 't1o_vk', 'oxy', 0),
(215274, 0, 't1o', 't1o_reddit', '', 0),
(215273, 0, 't1o', 't1o_tumblr', 'oxy', 0),
(215272, 0, 't1o', 't1o_skype', '', 0),
(215271, 0, 't1o', 't1o_behance', '', 0),
(215270, 0, 't1o', 't1o_instagram', 'oxy', 0),
(215269, 0, 't1o', 't1o_dribbble', '', 0),
(215268, 0, 't1o', 't1o_youtube', 'oxy', 0),
(215267, 0, 't1o', 't1o_linkedin', '', 0),
(215266, 0, 't1o', 't1o_flickr', '', 0),
(215261, 0, 't1o', 't1o_twitter', 'oxy', 0),
(215262, 0, 't1o', 't1o_googleplus', 'oxy', 0),
(215263, 0, 't1o', 't1o_rss', '', 0),
(215264, 0, 't1o', 't1o_pinterest', 'oxy', 0),
(215265, 0, 't1o', 't1o_vimeo', 'oxy', 0),
(215257, 0, 't1o', 't1o_powered_status', '1', 0),
(215258, 0, 't1o', 't1o_powered_content', '{\"1\":{\"htmlcontent\":\"&lt;p style=&quot;text-align: center;padding-bottom: 0;&quot;&gt;Design by Mua Gium Company&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"&lt;p style=&quot;text-align: center;padding-bottom: 0;&quot;&gt;\\u0110\\u01b0\\u1ee3c thi\\u1ebft k\\u1ebf b\\u1edfi c\\u00f4ng ty Mua Gi\\u00f9m&lt;br&gt;&lt;\\/p&gt;\"}}', 1),
(215260, 0, 't1o', 't1o_facebook', 'oxy', 0),
(215259, 0, 't1o', 't1o_follow_us_status', '0', 0),
(215223, 0, 't1o', 't1o_payment_block_status', '0', 0),
(215224, 0, 't1o', 't1o_payment_block_custom_status', '0', 0),
(215221, 0, 't1o', 't1o_newsletter_subscribe', '{\"1\":\"Subscribe\",\"2\":\"Subscribe\"}', 1),
(215222, 0, 't1o', 't1o_custom_2_content', '{\"1\":{\"htmlcontent\":\"*Don\'t worry, we respect your privacy. Your email address WILL NOT be shared.\"},\"2\":{\"htmlcontent\":\"*Don\'t worry, we respect your privacy. Your email address WILL NOT be shared.\"}}', 1),
(215220, 0, 't1o', 't1o_newsletter_email', '{\"1\":\"Your email address\",\"2\":\"Your email address\"}', 1),
(215184, 0, 't1o', 't1o_product_custom_block_3_status', '0', 0),
(215185, 0, 't1o', 't1o_product_custom_block_3_title', '{\"1\":\"\",\"2\":\"\"}', 1),
(215219, 0, 't1o', 't1o_newsletter_promo_text', '{\"1\":\"Subscribe to our newsletter to get special offers and receive the latest news, sales and updates!\",\"2\":\"Subscribe to our newsletter to get special offers and receive the latest news, sales and updates!\"}', 1),
(215218, 0, 't1o', 't1o_newsletter_campaign_url', '//321cart.us14.list-manage.com/subscribe/post?u=619755c76ad5fa60a96e52bec&amp;amp;id=c5fb795f8e', 0),
(215217, 0, 't1o', 't1o_newsletter_status', '1', 0),
(215216, 0, 't1o', 't1o_custom_2_title', '{\"1\":\"Join Our Newsletter\",\"2\":\"Join Our Newsletter\"}', 1),
(215215, 0, 't1o', 't1o_custom_2_column_width', '3', 0),
(215214, 0, 't1o', 't1o_custom_2_status', '0', 0),
(215213, 0, 't1o', 't1o_i_c_3_6_status', '1', 0),
(215211, 0, 't1o', 't1o_i_c_3_4_status', '1', 0),
(215212, 0, 't1o', 't1o_i_c_3_5_status', '1', 0),
(215210, 0, 't1o', 't1o_i_c_3_3_status', '1', 0),
(215182, 0, 't1o', 't1o_product_custom_block_2_title', '{\"1\":\"\",\"2\":\"\"}', 1),
(215183, 0, 't1o', 't1o_product_custom_block_2_content', '{\"1\":{\"htmlcontent\":\"&lt;div style=&quot;padding:15px 30px;background-color:#F7F6F2;color:#222222;text-align:center;&quot;&gt;This is a CMS block edited from theme admin panel.&lt;br&gt;You can insert any content here.&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;div style=&quot;padding:15px 30px;background-color:#F7F6F2;color:#222222;text-align:center;&quot;&gt;This is a CMS block edited from theme admin panel.&lt;br&gt;You can insert any content here.&lt;\\/div&gt;\"}}', 1),
(215200, 0, 't1o', 't1o_information_column_1_status', '0', 0),
(215201, 0, 't1o', 't1o_information_column_2_status', '1', 0),
(215202, 0, 't1o', 't1o_i_c_2_1_status', '1', 0),
(215203, 0, 't1o', 't1o_i_c_2_2_status', '1', 0),
(215204, 0, 't1o', 't1o_i_c_2_3_status', '1', 0),
(215205, 0, 't1o', 't1o_i_c_2_4_status', '1', 0),
(215206, 0, 't1o', 't1o_i_c_2_5_status', '1', 0),
(215207, 0, 't1o', 't1o_information_column_3_status', '1', 0),
(215208, 0, 't1o', 't1o_i_c_3_1_status', '1', 0),
(215209, 0, 't1o', 't1o_i_c_3_2_status', '1', 0),
(215199, 0, 't1o', 't1o_information_block_width', '6', 0),
(214831, 0, 't1d', 't1d_top_area_tb_text_color', '#FFFFFF', 0),
(214830, 0, 't1d', 't1d_top_area_tb_bottom_border_color', '#222222', 0),
(214829, 0, 't1d', 't1d_top_area_tb_bottom_border_status', '0', 0),
(214828, 0, 't1d', 't1d_top_area_tb_bg_color', '#7B8D63', 0),
(214827, 0, 't1d', 't1d_top_area_tb_bg_status', '1', 0),
(214694, 0, 'config', 'config_zone_id', '', 0),
(214692, 0, 'config', 'config_comment', 'Our hotline is open 24 hours a day, every day.', 0),
(214826, 0, 't1d', 't1d_top_area_search_icon_color', '#222222', 0),
(214825, 0, 't1d', 't1d_top_area_search_color_active', '#222222', 0),
(215186, 0, 't1o', 't1o_product_custom_block_3_content', '{\"1\":{\"htmlcontent\":\"&lt;div style=&quot;padding:15px 30px;background-color:#F7F6F2;color:#222222;text-align:center;&quot;&gt;This is a CMS block&lt;br&gt;edited from theme admin panel.&lt;br&gt;You can insert any content here.&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;div style=&quot;padding:15px 30px;background-color:#F7F6F2;color:#222222;text-align:center;&quot;&gt;This is a CMS block&lt;br&gt;edited from theme admin panel.&lt;br&gt;You can insert any content here.&lt;\\/div&gt;\"}}', 1),
(215187, 0, 't1o', 't1o_product_custom_tab', '{\"1\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}},\"2\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}},\"3\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}}}', 1),
(215188, 0, 't1o', 't1o_contact_map_status', '0', 0),
(215189, 0, 't1o', 't1o_contact_map_ll', '', 0),
(215190, 0, 't1o', 't1o_contact_map_api', '', 0),
(215191, 0, 't1o', 't1o_contact_map_type', 'ROADMAP', 0),
(215192, 0, 't1o', 't1o_left_right_column_categories_type', '1', 0),
(215193, 0, 't1o', 't1o_custom_top_1_status', '0', 0),
(215194, 0, 't1o', 't1o_custom_top_1_content', '{\"1\":{\"htmlcontent\":\"&lt;div class=&quot;widget-wrapper widget-footer-004&quot; style=&quot;padding: 0;&quot;&gt;&lt;div class=&quot;row&quot;&gt;&lt;div class=&quot;col-sm-4&quot;&gt;&lt;div class=&quot;widget-footer-chat-icon-1 icon-comments&quot; style=&quot;color:#82CE69;&quot;&gt;&lt;i class=&quot;fa fa-comments-o&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;&lt;div class=&quot;widget-footer-chat-title&quot; style=&quot;color:#B6B6B6; margin:0; font-size:12px; font-weight:normal; text-transform:uppercase;&quot;&gt;Need Help?&lt;\\/div&gt;&lt;div class=&quot;widget-footer-chat-subtitle&quot; style=&quot;margin:0;&quot;&gt;&lt;h3 style=&quot;color:#222222;&quot;&gt;Have a Question?&lt;\\/h3&gt;\\r\\n&lt;\\/div&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-sm-4 text-center&quot;&gt;&lt;span class=&quot;subtitle&quot; style=&quot;color:#222222; margin:0;&quot;&gt;Our Customer Service Team are online now&lt;br&gt;and waiting to chat to you.&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-sm-4 widget-buttons widget-buttons-right text-right&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot;&gt;Start Live Chat&lt;\\/a&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;div class=&quot;widget-wrapper widget-footer-004&quot; style=&quot;padding: 0;&quot;&gt;&lt;div class=&quot;row&quot;&gt;&lt;div class=&quot;col-sm-4&quot;&gt;&lt;div class=&quot;widget-footer-chat-icon-1 icon-comments&quot; style=&quot;color:#82CE69;&quot;&gt;&lt;i class=&quot;fa fa-comments-o&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;&lt;div class=&quot;widget-footer-chat-title&quot; style=&quot;color:#B6B6B6; margin:0; font-size:12px; font-weight:normal; text-transform:uppercase;&quot;&gt;Need Help?&lt;\\/div&gt;&lt;div class=&quot;widget-footer-chat-subtitle&quot; style=&quot;margin:0;&quot;&gt;&lt;h3 style=&quot;color:#222222;&quot;&gt;Have a Question?&lt;\\/h3&gt;\\r\\n&lt;\\/div&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-sm-4 text-center&quot;&gt;&lt;span class=&quot;subtitle&quot; style=&quot;color:#222222; margin:0;&quot;&gt;Our Customer Service Team are online now&lt;br&gt;and waiting to chat to you.&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;col-sm-4 widget-buttons widget-buttons-right text-right&quot;&gt;&lt;a href=&quot;#&quot; class=&quot;btn btn-primary&quot;&gt;Start Live Chat&lt;\\/a&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\"}}', 1),
(214824, 0, 't1d', 't1d_top_area_search_color', '#B6B6B6', 0),
(215177, 0, 't1o', 't1o_product_fb3_content', '{\"1\":{\"htmlcontent\":\"&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci. Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus lorem.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci. Phasellus sed erat leo. Donec luctus, justo eget ultricies tristique, enim mauris bibendum orci, a sodales lectus lorem.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"}}', 1),
(215181, 0, 't1o', 't1o_product_custom_block_2_status', '1', 0),
(215180, 0, 't1o', 't1o_product_custom_block_1_content', '{\"1\":{\"htmlcontent\":\"&lt;div style=&quot;padding:15px 30px;background-color:#F7F6F2;color:#222222;text-align:center;&quot;&gt;This is a CMS block edited from theme admin panel.&lt;br&gt;You can insert any content here.&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;div style=&quot;padding:15px 30px;background-color:#F7F6F2;color:#222222;text-align:center;&quot;&gt;This is a CMS block edited from theme admin panel.&lt;br&gt;You can insert any content here.&lt;\\/div&gt;\"}}', 1),
(215179, 0, 't1o', 't1o_product_custom_block_1_title', '{\"1\":\"\",\"2\":\"\"}', 1),
(215178, 0, 't1o', 't1o_product_custom_block_1_status', '1', 0),
(215173, 0, 't1o', 't1o_product_fb3_icon', '', 0),
(215174, 0, 't1o', 't1o_product_fb3_awesome', 'thumbs-up', 0),
(215175, 0, 't1o', 't1o_product_fb3_title', '{\"1\":\"30-Day Money-Back\",\"2\":\"30-Day Money-Back\"}', 1),
(215176, 0, 't1o', 't1o_product_fb3_subtitle', '{\"1\":\"100% Guarantee\",\"2\":\"100% Guarantee\"}', 1),
(215135, 0, 't1o', 't1o_sale_badge_type', '1', 0),
(215136, 0, 't1o', 't1o_new_badge_status', '1', 0),
(215137, 0, 't1o', 't1o_out_of_stock_badge_status', '1', 0),
(215138, 0, 't1o', 't1o_category_prod_name_status', '1', 0),
(215139, 0, 't1o', 't1o_category_prod_brand_status', '1', 0),
(215140, 0, 't1o', 't1o_category_prod_price_status', '1', 0),
(215141, 0, 't1o', 't1o_category_prod_quickview_status', '1', 0),
(215142, 0, 't1o', 't1o_category_prod_cart_status', '1', 0);
INSERT INTO `oc_setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES
(215143, 0, 't1o', 't1o_category_prod_ratings_status', '1', 0),
(215144, 0, 't1o', 't1o_category_prod_wis_com_status', '1', 0),
(215145, 0, 't1o', 't1o_category_prod_zoom_status', '0', 0),
(215146, 0, 't1o', 't1o_category_prod_swap_status', '1', 0),
(215147, 0, 't1o', 't1o_category_prod_shadow_status', '0', 0),
(215148, 0, 't1o', 't1o_category_prod_lift_up_status', '0', 0),
(215149, 0, 't1o', 't1o_category_prod_align', '1', 0),
(215150, 0, 't1o', 't1o_product_prev_next_status', '0', 0),
(215151, 0, 't1o', 't1o_layout_product_page', '7', 0),
(215152, 0, 't1o', 't1o_product_name_position', '0', 0),
(215153, 0, 't1o', 't1o_additional_images', '3', 0),
(215154, 0, 't1o', 't1o_product_align', '1', 0),
(215155, 0, 't1o', 't1o_product_manufacturer_logo_status', '1', 0),
(215156, 0, 't1o', 't1o_product_save_percent_status', '1', 0),
(215157, 0, 't1o', 't1o_product_tax_status', '0', 0),
(215158, 0, 't1o', 't1o_product_viewed_status', '1', 0),
(215159, 0, 't1o', 't1o_product_i_c_status', '1', 0),
(215160, 0, 't1o', 't1o_product_related_status', '1', 0),
(215161, 0, 't1o', 't1o_product_related_position', '0', 0),
(215162, 0, 't1o', 't1o_product_related_style', '1', 0),
(215163, 0, 't1o', 't1o_product_fb1_icon', '', 0),
(215164, 0, 't1o', 't1o_product_fb1_awesome', 'cube', 0),
(215165, 0, 't1o', 't1o_product_fb1_title', '{\"1\":\"Free Ground Shipping\",\"2\":\"Free Ground Shipping\"}', 1),
(215166, 0, 't1o', 't1o_product_fb1_subtitle', '{\"1\":\"on orders over $50\",\"2\":\"on orders over $50\"}', 1),
(215167, 0, 't1o', 't1o_product_fb1_content', '{\"1\":{\"htmlcontent\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_1.jpg&quot; alt=&quot;&quot; style=&quot;float: left; margin-right: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_1.jpg&quot; alt=&quot;&quot; style=&quot;float: left; margin-right: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"}}', 1),
(215168, 0, 't1o', 't1o_product_fb2_icon', '', 0),
(215169, 0, 't1o', 't1o_product_fb2_awesome', 'gift', 0),
(215170, 0, 't1o', 't1o_product_fb2_title', '{\"1\":\"Free Bonus Gifts\",\"2\":\"Free Bonus Gifts\"}', 1),
(215171, 0, 't1o', 't1o_product_fb2_subtitle', '{\"1\":\"with every order\",\"2\":\"with every order\"}', 1),
(215172, 0, 't1o', 't1o_product_fb2_content', '{\"1\":{\"htmlcontent\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_2.jpg&quot; alt=&quot;&quot; style=&quot;float: right; margin-left: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"&lt;img src=&quot;image\\/catalog\\/banner\\/content_features_2.jpg&quot; alt=&quot;&quot; style=&quot;float: right; margin-left: 45px;&quot;&gt;&lt;p&gt;Donec non ante justo at set ultrices quam ipsum sed mollis sagittise, nisi libero condimentum lacus sit amet sagittis quam tellus sed sapien. Aenean mi mauris, tempor id accumsan in, tincidunt non mi. Fusce risus nisl, bibendum sit amet lacinia vitae, sollicitudin sit amet augue. Phasellus arcu quam, hendrerit in dictum sed, eleifend non sapien. Sed velit quam, auctor id semper a, hendrerit eget justo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis vel arcu pulvinar dolor tempus feugiat id in orci.&lt;br&gt;\\r\\n&lt;\\/p&gt;\\r\\n&lt;ul&gt;\\r\\n&lt;li&gt;Pellentesque non dui at sapien tempor gravida.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Morbi consectetur nunc porta ligula tempor et varius.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;li&gt;Proin placerat nunc sed magna.&lt;br&gt;&lt;\\/li&gt;\\r\\n&lt;\\/ul&gt;\\r\\n&lt;p&gt;Aenean egestas congue est, nec convallis nunc viverra ac. Duis sit amet mattis quam. Nulla imperdiet tincidunt lorem ac ornare. Nulla pellentesque, velit eu volutpat ornare, justo sem blandit ante, et eleifend lacus mauris sed risus. Phasellus nec libero purus, a dignissim neque. Duis vel nisi nunc.&lt;br&gt;&lt;\\/p&gt;\"}}', 1),
(215116, 0, 't1o', 't1o_menu_labels', '{\"1\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"2\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"3\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"4\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"5\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"6\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"7\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"8\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"9\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"10\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"11\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"12\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"13\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"14\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}},\"15\":{\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"}}}', 1),
(215124, 0, 't1o', 't1o_category_title_position', '1', 0),
(215125, 0, 't1o', 't1o_category_title_above_color', '#424242', 0),
(215126, 0, 't1o', 't1o_category_desc_status', '1', 0),
(215127, 0, 't1o', 't1o_category_img_status', '0', 0),
(215128, 0, 't1o', 't1o_category_img_parallax', '0', 0),
(215129, 0, 't1o', 't1o_category_subcategories_status', '1', 0),
(215130, 0, 't1o', 't1o_category_subcategories_style', '0', 0),
(215131, 0, 't1o', 't1o_category_subcategories_per_row', '5', 0),
(215132, 0, 't1o', 't1o_category_subcategories_autoplay', '4000', 0),
(215133, 0, 't1o', 't1o_category_prod_box_style', 'product-box-style-1', 0),
(215134, 0, 't1o', 't1o_sale_badge_status', '1', 0),
(215123, 0, 't1o', 't1o_custom_bar_below_menu_text_color', '#FFFFFF', 0),
(215122, 0, 't1o', 't1o_custom_bar_below_menu_bg_color', '#373737', 0),
(215121, 0, 't1o', 't1o_custom_bar_below_menu_bg_animation', '0', 0),
(215120, 0, 't1o', 't1o_custom_bar_below_menu_bg', '', 0),
(215119, 0, 't1o', 't1o_custom_bar_below_menu_content', '{\"1\":{\"htmlcontent\":\"&lt;i class=&quot;fa fa-star-o&quot; style=&quot;color:#F1494B;font-size:13px;margin-right:7px;&quot;&gt;&lt;\\/i&gt; &lt;b&gt;LIMITED TIME! FREE BONUS GIFTS WITH EVERY ORDER!&lt;\\/b&gt; &lt;i class=&quot;fa fa-star-o&quot; style=&quot;color:#F1494B;font-size:13px;margin-left:7px;&quot;&gt;&lt;\\/i&gt;\"},\"2\":{\"htmlcontent\":\"&lt;i class=&quot;fa fa-star-o&quot; style=&quot;color:#F1494B;font-size:13px;margin-right:7px;&quot;&gt;&lt;\\/i&gt; &lt;b&gt;LIMITED TIME! FREE BONUS GIFTS WITH EVERY ORDER!&lt;\\/b&gt; &lt;i class=&quot;fa fa-star-o&quot; style=&quot;color:#F1494B;font-size:13px;margin-left:7px;&quot;&gt;&lt;\\/i&gt;\"}}', 1),
(215118, 0, 't1o', 't1o_custom_bar_below_menu_status', '0', 0),
(215091, 0, 't1o', 't1o_menu_main_category_padding_right', '200', 0),
(215092, 0, 't1o', 't1o_menu_main_category_padding_bottom', '110', 0),
(215093, 0, 't1o', 't1o_menu_categories_per_row', 'col-sm-2', 0),
(215094, 0, 't1o', 't1o_menu_categories_3_level', '0', 0),
(215095, 0, 't1o', 't1o_menu_categories_home_visibility', '1', 0),
(215096, 0, 't1o', 't1o_menu_categories_custom_block_left_status', '0', 0),
(215097, 0, 't1o', 't1o_menu_categories_custom_block_left_content', '{\"1\":{\"htmlcontent\":\"\"},\"2\":{\"htmlcontent\":\"\"}}', 1),
(215098, 0, 't1o', 't1o_menu_categories_custom_block_right_status', '0', 0),
(215099, 0, 't1o', 't1o_menu_categories_custom_block_right_content', '{\"1\":{\"htmlcontent\":\"&lt;p&gt;&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"\"}}', 1),
(215100, 0, 't1o', 't1o_menu_categories_custom_block_status', '0', 0),
(215101, 0, 't1o', 't1o_menu_categories_custom_block_content', '{\"1\":{\"htmlcontent\":\"\"},\"2\":{\"htmlcontent\":\"\"}}', 1),
(215102, 0, 't1o', 't1o_menu_brands_status', '1', 0),
(215103, 0, 't1o', 't1o_menu_brands_style', 'logoname', 0),
(215104, 0, 't1o', 't1o_menu_brands_per_row', '8-pr', 0),
(215105, 0, 't1o', 't1o_menu_custom_block', '{\"1\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}},\"2\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}},\"3\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}},\"4\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}},\"5\":{\"status\":\"0\",\"1\":{\"title\":\"\",\"htmlcontent\":\"\"},\"2\":{\"title\":\"\",\"htmlcontent\":\"\"}}}', 1),
(215106, 0, 't1o', 't1o_menu_cm_status', '1', 0),
(215107, 0, 't1o', 't1o_menu_cm_title', '{\"1\":\"Custom Dropdowns\",\"2\":\"Custom Dropdowns\"}', 1),
(215080, 0, 't1o', 't1o_news_hover_color', '#FFFFFF', 0),
(215081, 0, 't1o', 't1o_header_custom_block_1_status', '1', 0),
(215082, 0, 't1o', 't1o_header_custom_block_1_content', '{\"1\":{\"htmlcontent\":\"&lt;div class=&quot;header-custom-box&quot;&gt;    &lt;div class=&quot;buttons-header&quot;&gt;&lt;div class=&quot;button-i&quot;&gt;&lt;i class=&quot;fa fa-cube&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;span class=&quot;header-custom-title&quot; style=&quot;color:#2a2a2a;&quot;&gt;Free Shipping&lt;\\/span&gt;&lt;span class=&quot;header-custom-subtitle&quot; style=&quot;color:#b6b6b6;&quot;&gt;on all orders&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;header-custom-box&quot;&gt;    &lt;div class=&quot;buttons-header&quot;&gt;&lt;div class=&quot;button-i&quot;&gt;&lt;i class=&quot;fa fa-gift&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;span class=&quot;header-custom-title&quot; style=&quot;color:#2a2a2a;&quot;&gt;Free Bonus Gifts&lt;\\/span&gt;&lt;span class=&quot;header-custom-subtitle&quot; style=&quot;color:#b6b6b6;&quot;&gt;with every order&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;header-custom-box&quot;&gt;    &lt;div class=&quot;buttons-header&quot;&gt;&lt;div class=&quot;button-i&quot;&gt;&lt;i class=&quot;fa fa-thumbs-up&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;span class=&quot;header-custom-title&quot; style=&quot;color:#2a2a2a;&quot;&gt;Free Returns&lt;\\/span&gt;&lt;span class=&quot;header-custom-subtitle&quot; style=&quot;color:#b6b6b6;&quot;&gt;100% Guarantee&lt;\\/span&gt;&lt;\\/div&gt;\"},\"2\":{\"htmlcontent\":\"&lt;div class=&quot;header-custom-box&quot;&gt;    &lt;div class=&quot;buttons-header&quot;&gt;&lt;div class=&quot;button-i&quot;&gt;&lt;i class=&quot;fa fa-cube&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;span class=&quot;header-custom-title&quot; style=&quot;color:#2a2a2a;&quot;&gt;Free Shipping&lt;\\/span&gt;&lt;span class=&quot;header-custom-subtitle&quot; style=&quot;color:#b6b6b6;&quot;&gt;on all orders&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;header-custom-box&quot;&gt;    &lt;div class=&quot;buttons-header&quot;&gt;&lt;div class=&quot;button-i&quot;&gt;&lt;i class=&quot;fa fa-gift&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;span class=&quot;header-custom-title&quot; style=&quot;color:#2a2a2a;&quot;&gt;Free Bonus Gifts&lt;\\/span&gt;&lt;span class=&quot;header-custom-subtitle&quot; style=&quot;color:#b6b6b6;&quot;&gt;with every order&lt;\\/span&gt;&lt;\\/div&gt;\\r\\n&lt;div class=&quot;header-custom-box&quot;&gt;    &lt;div class=&quot;buttons-header&quot;&gt;&lt;div class=&quot;button-i&quot;&gt;&lt;i class=&quot;fa fa-thumbs-up&quot;&gt;&lt;\\/i&gt;&lt;\\/div&gt;\\r\\n&lt;\\/div&gt;\\r\\n&lt;span class=&quot;header-custom-title&quot; style=&quot;color:#2a2a2a;&quot;&gt;Free Returns&lt;\\/span&gt;&lt;span class=&quot;header-custom-subtitle&quot; style=&quot;color:#b6b6b6;&quot;&gt;100% Guarantee&lt;\\/span&gt;&lt;\\/div&gt;\"}}', 1),
(215115, 0, 't1o', 't1o_menu_link', '{\"1\":{\"status\":\"1\",\"1\":{\"title\":\"Custom Links\"},\"2\":{\"title\":\"Custom Links\"},\"url\":\"#\",\"target\":\"_self\"},\"2\":{\"status\":\"0\",\"1\":{\"title\":\"Blog\"},\"2\":{\"title\":\"Blog\"},\"url\":\"#\",\"target\":\"_blank\"},\"3\":{\"status\":\"1\",\"1\":{\"title\":\"What\'s new\"},\"2\":{\"title\":\"What\'s new\"},\"url\":\"#\",\"target\":\"_self\"},\"4\":{\"status\":\"0\",\"1\":{\"title\":\"Trends\"},\"2\":{\"title\":\"Trends\"},\"url\":\"#\",\"target\":\"_self\"},\"5\":{\"status\":\"0\",\"1\":{\"title\":\"Buy Theme\"},\"2\":{\"title\":\"Buy Theme\"},\"url\":\"https:\\/\\/themeforest.net\\/item\\/oxy-multipurpose-responsive-opencart-theme\\/6351720\",\"target\":\"_self\"},\"6\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"7\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"8\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"9\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"10\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"}}', 1),
(215113, 0, 't1o', 't1o_menu_cm_3_title', '{\"1\":\"\",\"2\":\"\"}', 1),
(215114, 0, 't1o', 't1o_menu_cm_3_link', '{\"1\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"2\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"3\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"4\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"5\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"6\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"7\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"8\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"9\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"10\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"}}', 1),
(215112, 0, 't1o', 't1o_menu_cm_3_status', '0', 0),
(215109, 0, 't1o', 't1o_menu_cm_2_status', '0', 0),
(215110, 0, 't1o', 't1o_menu_cm_2_title', '{\"1\":\"\",\"2\":\"\"}', 1),
(215111, 0, 't1o', 't1o_menu_cm_2_link', '{\"1\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"2\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"3\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"4\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"5\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"6\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"7\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"8\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"9\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"10\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"}}', 1),
(162312, 0, 'developer', 'developer_theme', '0', 0),
(214823, 0, 't1d', 't1d_top_area_search_border_color', '#F7F6F2', 0),
(214822, 0, 't1d', 't1d_top_area_search_bg_color', '#FFFFFF', 0),
(214821, 0, 't1d', 't1d_top_area_icons_separator_color', '#F7F6F2', 0),
(214820, 0, 't1d', 't1d_top_area_icons_separator_status', '0', 0),
(214819, 0, 't1d', 't1d_top_area_icons_color_hover', '#FC0012', 0),
(214818, 0, 't1d', 't1d_top_area_icons_color', '#3E4534', 0),
(214817, 0, 't1d', 't1d_top_area_mini_bg_color', '#3E4534', 0),
(214816, 0, 't1d', 't1d_top_area_bg_color', '#F7F6F2', 0),
(214815, 0, 't1d', 't1d_top_area_status', '1', 0),
(214814, 0, 't1d', 't1d_filter_box_box_separator_color', '#FFFFFF', 0),
(214813, 0, 't1d', 't1d_filter_box_box_filter_name_color_hover', '#FC0012', 0),
(214812, 0, 't1d', 't1d_filter_box_box_filter_name_color', '#222222', 0),
(214811, 0, 't1d', 't1d_filter_box_box_filter_title_color', '#222222', 0),
(214810, 0, 't1d', 't1d_filter_box_box_bg_color', '#F7F6F2', 0),
(214809, 0, 't1d', 't1d_filter_box_head_title_color', '#FFFFFF', 0),
(214808, 0, 't1d', 't1d_filter_box_head_title_bg_color', '#3E4534', 0),
(214807, 0, 't1d', 't1d_category_box_box_separator_color', '#FFFFFF', 0),
(214806, 0, 't1d', 't1d_category_box_box_subcat_color', '#222222', 0),
(214805, 0, 't1d', 't1d_category_box_box_links_color_hover', '#FC0012', 0),
(214804, 0, 't1d', 't1d_category_box_box_links_color', '#222222', 0),
(214803, 0, 't1d', 't1d_category_box_box_bg_color', '#F7F6F2', 0),
(214802, 0, 't1d', 't1d_category_box_head_title_color', '#FFFFFF', 0),
(214801, 0, 't1d', 't1d_category_box_head_title_bg_color', '#A4496C', 0),
(214800, 0, 't1d', 't1d_right_column_box_separator_color', '#FFFFFF', 0),
(214799, 0, 't1d', 't1d_right_column_box_links_color_hover', '#FC0012', 0),
(214798, 0, 't1d', 't1d_right_column_box_links_color', '#222222', 0),
(214797, 0, 't1d', 't1d_right_column_box_text_color', '#222222', 0),
(214796, 0, 't1d', 't1d_right_column_box_bg_color', '#F7F6F2', 0),
(214795, 0, 't1d', 't1d_right_column_head_title_color', '#FFFFFF', 0),
(214794, 0, 't1d', 't1d_right_column_head_title_bg_color', '#FC0012', 0),
(214793, 0, 't1d', 't1d_left_column_box_separator_color', '#FFFFFF', 0),
(214792, 0, 't1d', 't1d_left_column_box_links_color_hover', '#FC0012', 0),
(214791, 0, 't1d', 't1d_left_column_box_links_color', '#222222', 0),
(214790, 0, 't1d', 't1d_left_column_box_text_color', '#222222', 0),
(214789, 0, 't1d', 't1d_left_column_box_bg_color', '#F7F6F2', 0),
(214788, 0, 't1d', 't1d_left_column_head_title_color', '#FFFFFF', 0),
(214787, 0, 't1d', 't1d_left_column_head_title_bg_color', '#FC0012', 0),
(214786, 0, 't1d', 't1d_sidebar_margin_bottom', '0', 0),
(214785, 0, 't1d', 't1d_sidebar_separator_status', '0', 0),
(214784, 0, 't1d', 't1d_sidebar_bg_color_status', '1', 0),
(214783, 0, 't1d', 't1d_content_column_hli_bg_color', '#F7F6F2', 0),
(214782, 0, 't1d', 't1d_content_column_hli_buy_column', '0', 0),
(214781, 0, 't1d', 't1d_content_column_separator_color', '#F7F6F2', 0),
(214780, 0, 't1d', 't1d_content_column_bg_color', '#FFFFFF', 0),
(214779, 0, 't1d', 't1d_boxes_shadow', '1', 0),
(214778, 0, 't1d', 't1d_wrapper_shadow', '1', 0),
(214777, 0, 't1d', 't1d_wrapper_frame_bg_color', '#FFFFFF', 0),
(214776, 0, 't1d', 't1d_wrapper_frame_bg_color_status', '1', 0),
(214775, 0, 't1d', 't1d_img_style', 'normal', 0),
(214774, 0, 't1d', 't1d_icons_hover_color', '#FC0012', 0),
(214773, 0, 't1d', 't1d_icons_color', '#5F6853', 0),
(214772, 0, 't1d', 't1d_links_hover_color', '#FC0012', 0),
(214771, 0, 't1d', 't1d_other_links_color', '#222222', 0),
(214770, 0, 't1d', 't1d_light_text_color', '#B6B6B6', 0),
(214769, 0, 't1d', 't1d_body_text_color', '#222222', 0),
(215117, 0, 't1o', 't1o_menu_labels_color', '{\"1\":\"#555555\",\"2\":\"#555555\",\"3\":\"#555555\",\"4\":\"#555555\",\"5\":\"#555555\",\"6\":\"#555555\",\"7\":\"#555555\",\"8\":\"#555555\",\"9\":\"#555555\",\"10\":\"#555555\",\"11\":\"#555555\",\"12\":\"#555555\",\"13\":\"#555555\",\"14\":\"#555555\",\"15\":\"#555555\"}', 1),
(214696, 0, 'config', 'config_admin_language', 'vi-vn', 0),
(214695, 0, 'config', 'config_language', 'vi-vn', 0),
(214768, 0, 't1d', 't1d_headings_border_color', '#F7F6F2', 0),
(214767, 0, 't1d', 't1d_headings_border_status', '0', 0),
(214766, 0, 't1d', 't1d_headings_color', '#222222', 0),
(214765, 0, 't1d', 't1d_body_bg_color', '#F7F6F2', 0),
(215108, 0, 't1o', 't1o_menu_cm_link', '{\"1\":{\"status\":\"1\",\"1\":{\"title\":\"What\'s new\"},\"2\":{\"title\":\"What\'s new\"},\"url\":\"#\",\"target\":\"_self\"},\"2\":{\"status\":\"1\",\"1\":{\"title\":\"About Us\"},\"2\":{\"title\":\"About Us\"},\"url\":\"#\",\"target\":\"_self\"},\"3\":{\"status\":\"1\",\"1\":{\"title\":\"Contact Us\"},\"2\":{\"title\":\"Contact Us\"},\"url\":\"#\",\"target\":\"_self\"},\"4\":{\"status\":\"1\",\"1\":{\"title\":\"LookBook\"},\"2\":{\"title\":\"LookBook\"},\"url\":\"#\",\"target\":\"_self\"},\"5\":{\"status\":\"1\",\"1\":{\"title\":\"Trends\"},\"2\":{\"title\":\"Trends\"},\"url\":\"#\",\"target\":\"_self\"},\"6\":{\"status\":\"1\",\"1\":{\"title\":\"FAQs\"},\"2\":{\"title\":\"FAQs\"},\"url\":\"#\",\"target\":\"_self\"},\"7\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"8\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"9\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"},\"10\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\",\"target\":\"_self\"}}', 1),
(215090, 0, 't1o', 't1o_menu_main_category_icon_status', '1', 0),
(215088, 0, 't1o', 't1o_menu_categories_status', '1', 0),
(215089, 0, 't1o', 't1o_menu_categories_style', '3', 0),
(214693, 0, 'config', 'config_country_id', '230', 0),
(214689, 0, 'config', 'config_fax', '', 0),
(214690, 0, 'config', 'config_image', 'catalog/Logo/logomg.png', 0),
(214691, 0, 'config', 'config_open', 'From Monday to Friday\r\n9:00 a.m. to 5:00 p.m.', 0),
(214679, 0, 'config', 'config_meta_description', 'My Store', 0),
(214680, 0, 'config', 'config_meta_keyword', '', 0),
(214681, 0, 'config', 'config_theme', 'oxy', 0),
(214682, 0, 'config', 'config_layout_id', '4', 0),
(214683, 0, 'config', 'config_name', 'Your Store', 0),
(214684, 0, 'config', 'config_owner', 'John', 0),
(214685, 0, 'config', 'config_address', '135 South Park Avenue\r\nLos Angeles, CA 90024. USA', 0),
(214686, 0, 'config', 'config_geocode', '51.5224954,-0.1720996', 0),
(214687, 0, 'config', 'config_email', 'contact@yourstore.com', 0),
(214688, 0, 'config', 'config_telephone', '24/7 Hotline: +1 800 321 321', 0),
(215083, 0, 't1o', 't1o_header_auto_suggest_status', '1', 0),
(215084, 0, 't1o', 't1o_header_popular_search_status', '1', 0),
(215085, 0, 't1o', 't1o_header_popular_search', '{\"1\":{\"1\":{\"word\":\"Jeans\"},\"2\":{\"word\":\"Jeans\"}},\"2\":{\"1\":{\"word\":\"Toys\"},\"2\":{\"word\":\"Toys\"}},\"3\":{\"1\":{\"word\":\"Sony\"},\"2\":{\"word\":\"Sony\"}},\"4\":{\"1\":{\"word\":\"Tablet\"},\"2\":{\"word\":\"Tablet\"}},\"5\":{\"1\":{\"word\":\"Jacket\"},\"2\":{\"word\":\"Jacket\"}},\"6\":{\"1\":{\"word\":\"Computer\"},\"2\":{\"word\":\"Computer\"}},\"7\":{\"1\":{\"word\":\"Top\"},\"2\":{\"word\":\"Top\"}},\"8\":{\"1\":{\"word\":\"Samsung\"},\"2\":{\"word\":\"Samsung\"}},\"9\":{\"1\":{\"word\":\"Watch\"},\"2\":{\"word\":\"Watch\"}},\"10\":{\"1\":{\"word\":\"White\"},\"2\":{\"word\":\"White\"}},\"11\":{\"1\":{\"word\":\"XL\"},\"2\":{\"word\":\"XL\"}},\"12\":{\"1\":{\"word\":\"Bicycle\"},\"2\":{\"word\":\"Bicycle\"}},\"13\":{\"1\":{\"word\":\"T-Shirt\"},\"2\":{\"word\":\"T-Shirt\"}},\"14\":{\"1\":{\"word\":\"Red\"},\"2\":{\"word\":\"Red\"}},\"15\":{\"1\":{\"word\":\"Camera\"},\"2\":{\"word\":\"Camera\"}},\"16\":{\"1\":{\"word\":\"1TB\"},\"2\":{\"word\":\"1TB\"}},\"17\":{\"1\":{\"word\":\"Dress\"},\"2\":{\"word\":\"Dress\"}},\"18\":{\"1\":{\"word\":\"Brown\"},\"2\":{\"word\":\"Brown\"}},\"19\":{\"1\":{\"word\":\"Nikon\"},\"2\":{\"word\":\"Nikon\"}},\"20\":{\"1\":{\"word\":\"Shoes\"},\"2\":{\"word\":\"Shoes\"}}}', 1),
(215075, 0, 't1o', 't1o_news', '{\"1\":{\"status\":\"1\",\"1\":{\"title\":\"Summer Sale - All Printed T-Shirts on Sale\"},\"2\":{\"title\":\"Summer Sale - All Printed T-Shirts on Sale\"},\"url\":\"#\"},\"2\":{\"status\":\"1\",\"1\":{\"title\":\"Final Clothing Sale - Up To 50%\"},\"2\":{\"title\":\"Final Clothing Sale - Up To 50%\"},\"url\":\"#\"},\"3\":{\"status\":\"1\",\"1\":{\"title\":\"New Denim Collection 2017 - See All Products\"},\"2\":{\"title\":\"New Denim Collection 2017 - See All Products\"},\"url\":\"#\"},\"4\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"},\"5\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"},\"6\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"},\"7\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"},\"8\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"},\"9\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"},\"10\":{\"status\":\"0\",\"1\":{\"title\":\"\"},\"2\":{\"title\":\"\"},\"url\":\"\"}}', 1),
(215087, 0, 't1o', 't1o_menu_homepage', 'text', 0),
(215086, 0, 't1o', 't1o_menu_align', 'left', 0),
(215079, 0, 't1o', 't1o_news_color', '#FFFFFF', 0),
(215076, 0, 't1o', 't1o_news_bg_color', '#545B49', 0),
(215077, 0, 't1o', 't1o_news_icons_color', '#FFFFFF', 0),
(215078, 0, 't1o', 't1o_news_word_color', '#FFFFFF', 0),
(215073, 0, 't1o', 't1o_top_custom_block_text_color', '#FFFFFF', 0),
(215074, 0, 't1o', 't1o_news_status', '1', 0),
(215071, 0, 't1o', 't1o_top_custom_block_bg', '', 0),
(215072, 0, 't1o', 't1o_top_custom_block_bg_color', '#424242', 0),
(215069, 0, 't1o', 't1o_top_custom_block_title_color', '#FFFFFF', 0),
(215070, 0, 't1o', 't1o_top_custom_block_content', '{\"1\":{\"htmlcontent\":\"&lt;p&gt;New &amp;amp; Interesting Finds on OXY Theme&lt;br&gt;&lt;\\/p&gt;\"},\"2\":{\"htmlcontent\":\"&lt;p&gt;New &amp;amp; Interesting Finds on OXY Theme&lt;br&gt;&lt;\\/p&gt;\"}}', 1),
(215068, 0, 't1o', 't1o_top_custom_block_title', '{\"1\":\"New &amp; Interesting Finds on OXY Theme\",\"2\":\"New &amp; Interesting Finds on OXY Theme\"}', 1),
(215067, 0, 't1o', 't1o_top_custom_block_awesome_color', '#B2945F', 0),
(215066, 0, 't1o', 't1o_top_custom_block_awesome', 'diamond', 0),
(215065, 0, 't1o', 't1o_top_custom_block_awesome_status', '1', 0),
(215064, 0, 't1o', 't1o_top_custom_block_bar_bg', '', 0),
(215062, 0, 't1o', 't1o_top_custom_block_status', '0', 0),
(215063, 0, 't1o', 't1o_top_custom_block_title_bar_bg_color', '#EDD05F', 0),
(215061, 0, 't1o', 't1o_top_bar_welcome', '{\"1\":\"Welcome\",\"2\":\"Xin ch\\u00e0o\"}', 1),
(215060, 0, 't1o', 't1o_top_bar_welcome_awesome', 'flag', 0),
(215059, 0, 't1o', 't1o_top_bar_welcome_status', '1', 0),
(215058, 0, 't1o', 't1o_top_bar_cart_link_status', '0', 0),
(215057, 0, 't1o', 't1o_top_bar_status', '1', 0),
(215056, 0, 't1o', 't1o_header_fixed_header_status', '1', 0),
(215055, 0, 't1o', 't1o_text_logo_awesome_color', '#F1494B', 0),
(215054, 0, 't1o', 't1o_text_logo_awesome', 'shopping-bag', 0),
(215053, 0, 't1o', 't1o_text_logo_awesome_status', '1', 0),
(215052, 0, 't1o', 't1o_text_logo_color', '#424242', 0),
(215050, 0, 't1o', 't1o_header_style', 'header-style-3', 0),
(215051, 0, 't1o', 't1o_text_logo', '{\"1\":\"OXY Theme\",\"2\":\"OXY Theme\"}', 1),
(215049, 0, 't1o', 't1o_others_totop', '1', 0),
(215048, 0, 't1o', 't1o_carousel_items_per_row', '6', 0),
(215047, 0, 't1o', 't1o_related_per_row', '6', 0),
(215046, 0, 't1o', 't1o_product_tabs_per_row', '6', 0),
(215045, 0, 't1o', 't1o_most_viewed_per_row', '5', 0),
(215044, 0, 't1o', 't1o_specials_per_row', '5', 0),
(215043, 0, 't1o', 't1o_latest_per_row', '6', 0),
(215042, 0, 't1o', 't1o_featured_per_row', '6', 0),
(215041, 0, 't1o', 't1o_bestseller_per_row', '1', 0),
(215040, 0, 't1o', 't1o_product_grid_per_row', '15', 0),
(215039, 0, 't1o', 't1o_product_tabs_style', '0', 0),
(215038, 0, 't1o', 't1o_most_viewed_style', '1', 0),
(215037, 0, 't1o', 't1o_specials_style', '1', 0),
(215035, 0, 't1o', 't1o_featured_style', '1', 0),
(215036, 0, 't1o', 't1o_latest_style', '1', 0),
(215032, 0, 't1o', 't1o_layout_h_align', '0', 0),
(215033, 0, 't1o', 't1o_layout_catalog_mode', '0', 0),
(215034, 0, 't1o', 't1o_bestseller_style', '0', 0),
(215031, 0, 't1o', 't1o_layout_full_width_max', '0', 0),
(215030, 0, 't1o', 't1o_layout_framed_align', '0', 0),
(215029, 0, 't1o', 't1o_layout_l', '1', 0),
(215028, 0, 't1o', 't1o_layout_style', 'full-width', 0),
(214678, 0, 'config', 'config_meta_title', 'OXY - EXTREME POWERFUL &amp; FLUID RESPONSIVE OPENCART THEME WITH HUNDREDS OPTIONS', 0),
(214948, 0, 't1d', 't1d_button_text_hover_color', '#FFFFFF', 0),
(214949, 0, 't1d', 't1d_button_exclusive_bg_status', '1', 0),
(214950, 0, 't1d', 't1d_button_exclusive_bg_color', '#FC0012', 0),
(214951, 0, 't1d', 't1d_button_exclusive_bg_hover_color', '#FC0012', 0),
(214952, 0, 't1d', 't1d_button_exclusive_text_color', '#FFFFFF', 0),
(214953, 0, 't1d', 't1d_button_exclusive_text_hover_color', '#FFFFFF', 0),
(214954, 0, 't1d', 't1d_dd_bg_color', '#FFFFFF', 0),
(214955, 0, 't1d', 't1d_dd_headings_color', '#222222', 0),
(214956, 0, 't1d', 't1d_dd_text_color', '#222222', 0),
(214957, 0, 't1d', 't1d_dd_light_text_color', '#B6B6B6', 0),
(214958, 0, 't1d', 't1d_dd_links_color', '#222222', 0),
(214959, 0, 't1d', 't1d_dd_links_hover_color', '#FC0012', 0),
(214960, 0, 't1d', 't1d_dd_icons_color', '#DBD9D1', 0),
(214961, 0, 't1d', 't1d_dd_icons_hover_color', '#FC0012', 0),
(214962, 0, 't1d', 't1d_dd_hli_bg_color', '#F7F6F2', 0),
(214963, 0, 't1d', 't1d_dd_separator_color', '#F7F6F2', 0),
(214964, 0, 't1d', 't1d_dd_shadow', '1', 0),
(214965, 0, 't1d', 't1d_pattern_body', 'none', 0),
(214966, 0, 't1d', 't1d_bg_image_custom', '', 0),
(214967, 0, 't1d', 't1d_bg_image_position', 'top center', 0),
(214968, 0, 't1d', 't1d_bg_image_repeat', 'repeat', 0),
(214969, 0, 't1d', 't1d_bg_image_attachment', 'fixed', 0),
(214970, 0, 't1d', 't1d_pattern_k_ta', 'none', 0),
(214971, 0, 't1d', 't1d_bg_image_ta_custom', '', 0),
(214972, 0, 't1d', 't1d_bg_image_ta_position', 'top center', 0),
(214973, 0, 't1d', 't1d_bg_image_ta_repeat', 'no-repeat', 0),
(214974, 0, 't1d', 't1d_bg_image_ta_attachment', 'scroll', 0),
(214975, 0, 't1d', 't1d_pattern_k_mm', 'none', 0),
(214976, 0, 't1d', 't1d_bg_image_mm_custom', '', 0),
(214977, 0, 't1d', 't1d_bg_image_mm_repeat', 'repeat', 0),
(214978, 0, 't1d', 't1d_pattern_k_f1', 'none', 0),
(214979, 0, 't1d', 't1d_bg_image_f1_custom', '', 0),
(214980, 0, 't1d', 't1d_bg_image_f1_parallax', '0', 0),
(214981, 0, 't1d', 't1d_bg_image_f1_position', 'top center', 0),
(214982, 0, 't1d', 't1d_bg_image_f1_repeat', 'repeat', 0),
(214983, 0, 't1d', 't1d_pattern_k_f6', 'none', 0),
(214984, 0, 't1d', 't1d_bg_image_f6_custom', '', 0),
(214985, 0, 't1d', 't1d_bg_image_f6_position', 'top center', 0),
(214986, 0, 't1d', 't1d_bg_image_f6_repeat', 'repeat', 0),
(214987, 0, 't1d', 't1d_pattern_k_f2', 'none', 0),
(214988, 0, 't1d', 't1d_bg_image_f2_custom', '', 0),
(214989, 0, 't1d', 't1d_bg_image_f2_position', 'top center', 0),
(214990, 0, 't1d', 't1d_bg_image_f2_repeat', 'repeat', 0),
(214991, 0, 't1d', 't1d_pattern_k_f3', 'none', 0),
(214992, 0, 't1d', 't1d_bg_image_f3_custom', '', 0),
(214993, 0, 't1d', 't1d_bg_image_f3_position', 'top center', 0),
(214994, 0, 't1d', 't1d_bg_image_f3_repeat', 'repeat', 0),
(214995, 0, 't1d', 't1d_pattern_k_f4', 'none', 0),
(214996, 0, 't1d', 't1d_bg_image_f4_custom', '', 0),
(214997, 0, 't1d', 't1d_bg_image_f4_position', 'top center', 0),
(214998, 0, 't1d', 't1d_bg_image_f4_repeat', 'repeat', 0),
(214999, 0, 't1d', 't1d_pattern_k_f5', 'none', 0),
(215000, 0, 't1d', 't1d_bg_image_f5_custom', 'catalog/banner/footer_banner_bg_organic_1.jpg', 0),
(215001, 0, 't1d', 't1d_bg_image_f5_position', 'top left', 0),
(215002, 0, 't1d', 't1d_bg_image_f5_repeat', 'repeat', 0),
(215003, 0, 't1d', 't1d_body_font', 'Roboto Slab', 0),
(215004, 0, 't1d', 't1d_body_font_size', '13', 0),
(215005, 0, 't1d', 't1d_body_font_uppercase', '0', 0),
(215006, 0, 't1d', 't1d_small_font_size', '12', 0),
(215007, 0, 't1d', 't1d_title_font', 'Bitter', 0),
(215008, 0, 't1d', 't1d_title_font_weight', 'normal', 0),
(215009, 0, 't1d', 't1d_title_font_uppercase', '1', 0),
(215010, 0, 't1d', 't1d_subtitle_font', 'Roboto Slab', 0),
(215011, 0, 't1d', 't1d_subtitle_font_weight', 'normal', 0),
(215012, 0, 't1d', 't1d_subtitle_font_size', '12', 0),
(215013, 0, 't1d', 't1d_subtitle_font_uppercase', '0', 0),
(215014, 0, 't1d', 't1d_subtitle_font_style', 'normal', 0),
(215015, 0, 't1d', 't1d_price_font_weight', 'normal', 0),
(215016, 0, 't1d', 't1d_button_font_weight', 'normal', 0),
(215017, 0, 't1d', 't1d_button_font_size', '10', 0),
(215018, 0, 't1d', 't1d_button_font_uppercase', '1', 0),
(215019, 0, 't1d', 't1d_mm_font_weight', 'normal', 0),
(215020, 0, 't1d', 't1d_mm_font_size', '16', 0),
(215021, 0, 't1d', 't1d_mm_font_uppercase', '1', 0),
(215022, 0, 't1d', 't1d_mm_sub_main_cat_font_weight', 'normal', 0),
(215023, 0, 't1d', 't1d_mm_sub_main_cat_font_size', '14', 0),
(215024, 0, 't1d', 't1d_mm_sub_font_weight', 'normal', 0),
(215025, 0, 't1d', 't1d_mm_sub_font_size', '12', 0),
(215026, 0, 't1d', 't1d_skin', 'skin1-default', 0),
(215340, 0, 't1o', 't1o_text_popular_search', '{\"1\":\"Popular Search:\",\"2\":\"T\\u00ecm ki\\u1ebfm nhi\\u1ec1u nh\\u1ea5t:\"}', 1),
(215341, 0, 't1o', 't1o_text_advanced_search', '{\"1\":\"Advanced Search\",\"2\":\"T\\u00ecm ki\\u1ebfm n\\u00e2ng cao\"}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_shipping_courier`
--

CREATE TABLE `oc_shipping_courier` (
  `shipping_courier_id` int(11) NOT NULL,
  `shipping_courier_code` varchar(255) NOT NULL DEFAULT '',
  `shipping_courier_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_shipping_courier`
--

INSERT INTO `oc_shipping_courier` (`shipping_courier_id`, `shipping_courier_code`, `shipping_courier_name`) VALUES
(1, 'dhl', 'DHL'),
(2, 'fedex', 'Fedex'),
(3, 'ups', 'UPS'),
(4, 'royal-mail', 'Royal Mail'),
(5, 'usps', 'United States Postal Service'),
(6, 'auspost', 'Australia Post'),
(7, 'citylink', 'Citylink');

-- --------------------------------------------------------

--
-- Table structure for table `oc_statistics`
--

CREATE TABLE `oc_statistics` (
  `statistics_id` int(11) NOT NULL,
  `code` varchar(64) NOT NULL,
  `value` decimal(15,4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_statistics`
--

INSERT INTO `oc_statistics` (`statistics_id`, `code`, `value`) VALUES
(1, 'order_sale', '42724.0000'),
(2, 'order_processing', '0.0000'),
(3, 'order_complete', '0.0000'),
(4, 'order_other', '0.0000'),
(5, 'returns', '0.0000'),
(6, 'product', '0.0000'),
(7, 'review', '75.0000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_stock_status`
--

CREATE TABLE `oc_stock_status` (
  `stock_status_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_stock_status`
--

INSERT INTO `oc_stock_status` (`stock_status_id`, `language_id`, `name`) VALUES
(7, 1, 'In Stock'),
(8, 1, 'Pre-Order'),
(5, 1, 'Out Of Stock'),
(6, 1, '2-3 Days'),
(7, 2, 'In Stock'),
(8, 2, 'Pre-Order'),
(5, 2, 'Out Of Stock'),
(6, 2, '2-3 Days');

-- --------------------------------------------------------

--
-- Table structure for table `oc_store`
--

CREATE TABLE `oc_store` (
  `store_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `url` varchar(255) NOT NULL,
  `ssl` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_class`
--

CREATE TABLE `oc_tax_class` (
  `tax_class_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_class`
--

INSERT INTO `oc_tax_class` (`tax_class_id`, `title`, `description`, `date_added`, `date_modified`) VALUES
(9, 'Taxable Goods', 'Taxed goods', '2009-01-06 23:21:53', '2011-09-23 14:07:50'),
(10, 'Downloadable Products', 'Downloadable', '2011-09-21 22:19:39', '2011-09-22 10:27:36');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate`
--

CREATE TABLE `oc_tax_rate` (
  `tax_rate_id` int(11) NOT NULL,
  `geo_zone_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) NOT NULL,
  `rate` decimal(15,4) NOT NULL DEFAULT '0.0000',
  `type` char(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate`
--

INSERT INTO `oc_tax_rate` (`tax_rate_id`, `geo_zone_id`, `name`, `rate`, `type`, `date_added`, `date_modified`) VALUES
(86, 3, 'VAT (20%)', '20.0000', 'P', '2011-03-09 21:17:10', '2011-09-22 22:24:29'),
(87, 3, 'Eco Tax (-2.00)', '2.0000', 'F', '2011-09-21 21:49:23', '2011-09-23 00:40:19');

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rate_to_customer_group`
--

CREATE TABLE `oc_tax_rate_to_customer_group` (
  `tax_rate_id` int(11) NOT NULL,
  `customer_group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rate_to_customer_group`
--

INSERT INTO `oc_tax_rate_to_customer_group` (`tax_rate_id`, `customer_group_id`) VALUES
(86, 1),
(87, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_tax_rule`
--

CREATE TABLE `oc_tax_rule` (
  `tax_rule_id` int(11) NOT NULL,
  `tax_class_id` int(11) NOT NULL,
  `tax_rate_id` int(11) NOT NULL,
  `based` varchar(10) NOT NULL,
  `priority` int(5) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_tax_rule`
--

INSERT INTO `oc_tax_rule` (`tax_rule_id`, `tax_class_id`, `tax_rate_id`, `based`, `priority`) VALUES
(121, 10, 86, 'payment', 1),
(120, 10, 87, 'store', 0),
(128, 9, 86, 'shipping', 1),
(127, 9, 87, 'shipping', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oc_theme`
--

CREATE TABLE `oc_theme` (
  `theme_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `theme` varchar(64) NOT NULL,
  `route` varchar(64) NOT NULL,
  `code` mediumtext NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_translation`
--

CREATE TABLE `oc_translation` (
  `translation_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `route` varchar(64) NOT NULL,
  `key` varchar(64) NOT NULL,
  `value` text NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_upload`
--

CREATE TABLE `oc_upload` (
  `upload_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_user`
--

CREATE TABLE `oc_user` (
  `user_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `image` varchar(255) NOT NULL,
  `code` varchar(40) NOT NULL,
  `ip` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user`
--

INSERT INTO `oc_user` (`user_id`, `user_group_id`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `image`, `code`, `ip`, `status`, `date_added`) VALUES
(1, 1, 'admin', 'd9410192ccd8b0052e8fee271a85cecf75d00b82', 'BPVMuVU7T', 'John', 'Doe', 'huynhkynhatcuong@gmail.com', '', '', '127.0.0.1', 1, '2018-03-08 20:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `oc_user_group`
--

CREATE TABLE `oc_user_group` (
  `user_group_id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `permission` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_user_group`
--

INSERT INTO `oc_user_group` (`user_group_id`, `name`, `permission`) VALUES
(1, 'Administrator', '{\"access\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/oxy_theme_design\",\"extension\\/module\\/oxy_theme_options\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_banner\",\"extension\\/module\\/theme_banner_pro\",\"extension\\/module\\/theme_brand_slider\",\"extension\\/module\\/theme_category_slider\",\"extension\\/module\\/theme_custom_products\",\"extension\\/module\\/theme_faq\",\"extension\\/module\\/theme_featured\",\"extension\\/module\\/theme_features_blocks\",\"extension\\/module\\/theme_gallery\",\"extension\\/module\\/theme_highly_recommended\",\"extension\\/module\\/theme_lookbook\",\"extension\\/module\\/theme_most_viewed\",\"extension\\/module\\/theme_product_slider\",\"extension\\/module\\/theme_product_tabs\",\"extension\\/module\\/theme_slideshow\",\"extension\\/module\\/theme_store_tv\",\"extension\\/module\\/theme_testimonial\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/oxy\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"],\"modify\":[\"catalog\\/attribute\",\"catalog\\/attribute_group\",\"catalog\\/category\",\"catalog\\/download\",\"catalog\\/filter\",\"catalog\\/information\",\"catalog\\/manufacturer\",\"catalog\\/option\",\"catalog\\/product\",\"catalog\\/recurring\",\"catalog\\/review\",\"common\\/column_left\",\"common\\/developer\",\"common\\/filemanager\",\"common\\/profile\",\"common\\/security\",\"customer\\/custom_field\",\"customer\\/customer\",\"customer\\/customer_approval\",\"customer\\/customer_group\",\"design\\/banner\",\"design\\/layout\",\"design\\/seo_url\",\"design\\/theme\",\"design\\/translation\",\"event\\/language\",\"event\\/statistics\",\"event\\/theme\",\"extension\\/analytics\\/google\",\"extension\\/captcha\\/basic\",\"extension\\/captcha\\/google\",\"extension\\/dashboard\\/activity\",\"extension\\/dashboard\\/chart\",\"extension\\/dashboard\\/customer\",\"extension\\/dashboard\\/map\",\"extension\\/dashboard\\/online\",\"extension\\/dashboard\\/order\",\"extension\\/dashboard\\/recent\",\"extension\\/dashboard\\/sale\",\"extension\\/extension\\/analytics\",\"extension\\/extension\\/captcha\",\"extension\\/extension\\/dashboard\",\"extension\\/extension\\/feed\",\"extension\\/extension\\/fraud\",\"extension\\/extension\\/menu\",\"extension\\/extension\\/module\",\"extension\\/extension\\/payment\",\"extension\\/extension\\/report\",\"extension\\/extension\\/shipping\",\"extension\\/extension\\/theme\",\"extension\\/extension\\/total\",\"extension\\/feed\\/google_base\",\"extension\\/feed\\/google_sitemap\",\"extension\\/feed\\/openbaypro\",\"extension\\/fraud\\/fraudlabspro\",\"extension\\/fraud\\/ip\",\"extension\\/fraud\\/maxmind\",\"extension\\/module\\/account\",\"extension\\/module\\/amazon_login\",\"extension\\/module\\/amazon_pay\",\"extension\\/module\\/banner\",\"extension\\/module\\/bestseller\",\"extension\\/module\\/carousel\",\"extension\\/module\\/category\",\"extension\\/module\\/divido_calculator\",\"extension\\/module\\/ebay_listing\",\"extension\\/module\\/featured\",\"extension\\/module\\/filter\",\"extension\\/module\\/google_hangouts\",\"extension\\/module\\/html\",\"extension\\/module\\/information\",\"extension\\/module\\/klarna_checkout_module\",\"extension\\/module\\/latest\",\"extension\\/module\\/laybuy_layout\",\"extension\\/module\\/oxy_theme_design\",\"extension\\/module\\/oxy_theme_options\",\"extension\\/module\\/pilibaba_button\",\"extension\\/module\\/pp_braintree_button\",\"extension\\/module\\/pp_button\",\"extension\\/module\\/pp_login\",\"extension\\/module\\/sagepay_direct_cards\",\"extension\\/module\\/sagepay_server_cards\",\"extension\\/module\\/slideshow\",\"extension\\/module\\/special\",\"extension\\/module\\/store\",\"extension\\/module\\/theme_banner\",\"extension\\/module\\/theme_banner_pro\",\"extension\\/module\\/theme_brand_slider\",\"extension\\/module\\/theme_category_slider\",\"extension\\/module\\/theme_custom_products\",\"extension\\/module\\/theme_faq\",\"extension\\/module\\/theme_featured\",\"extension\\/module\\/theme_features_blocks\",\"extension\\/module\\/theme_gallery\",\"extension\\/module\\/theme_highly_recommended\",\"extension\\/module\\/theme_lookbook\",\"extension\\/module\\/theme_most_viewed\",\"extension\\/module\\/theme_product_slider\",\"extension\\/module\\/theme_product_tabs\",\"extension\\/module\\/theme_slideshow\",\"extension\\/module\\/theme_store_tv\",\"extension\\/module\\/theme_testimonial\",\"extension\\/openbay\\/amazon\",\"extension\\/openbay\\/amazon_listing\",\"extension\\/openbay\\/amazon_product\",\"extension\\/openbay\\/amazonus\",\"extension\\/openbay\\/amazonus_listing\",\"extension\\/openbay\\/amazonus_product\",\"extension\\/openbay\\/ebay\",\"extension\\/openbay\\/ebay_profile\",\"extension\\/openbay\\/ebay_template\",\"extension\\/openbay\\/etsy\",\"extension\\/openbay\\/etsy_product\",\"extension\\/openbay\\/etsy_shipping\",\"extension\\/openbay\\/etsy_shop\",\"extension\\/openbay\\/fba\",\"extension\\/payment\\/alipay\",\"extension\\/payment\\/alipay_cross\",\"extension\\/payment\\/amazon_login_pay\",\"extension\\/payment\\/authorizenet_aim\",\"extension\\/payment\\/authorizenet_sim\",\"extension\\/payment\\/bank_transfer\",\"extension\\/payment\\/bluepay_hosted\",\"extension\\/payment\\/bluepay_redirect\",\"extension\\/payment\\/cardconnect\",\"extension\\/payment\\/cardinity\",\"extension\\/payment\\/cheque\",\"extension\\/payment\\/cod\",\"extension\\/payment\\/divido\",\"extension\\/payment\\/eway\",\"extension\\/payment\\/firstdata\",\"extension\\/payment\\/firstdata_remote\",\"extension\\/payment\\/free_checkout\",\"extension\\/payment\\/g2apay\",\"extension\\/payment\\/globalpay\",\"extension\\/payment\\/globalpay_remote\",\"extension\\/payment\\/klarna_account\",\"extension\\/payment\\/klarna_checkout\",\"extension\\/payment\\/klarna_invoice\",\"extension\\/payment\\/laybuy\",\"extension\\/payment\\/liqpay\",\"extension\\/payment\\/nochex\",\"extension\\/payment\\/paymate\",\"extension\\/payment\\/paypoint\",\"extension\\/payment\\/payza\",\"extension\\/payment\\/perpetual_payments\",\"extension\\/payment\\/pilibaba\",\"extension\\/payment\\/pp_braintree\",\"extension\\/payment\\/pp_express\",\"extension\\/payment\\/pp_payflow\",\"extension\\/payment\\/pp_payflow_iframe\",\"extension\\/payment\\/pp_pro\",\"extension\\/payment\\/pp_pro_iframe\",\"extension\\/payment\\/pp_standard\",\"extension\\/payment\\/realex\",\"extension\\/payment\\/realex_remote\",\"extension\\/payment\\/sagepay_direct\",\"extension\\/payment\\/sagepay_server\",\"extension\\/payment\\/sagepay_us\",\"extension\\/payment\\/securetrading_pp\",\"extension\\/payment\\/securetrading_ws\",\"extension\\/payment\\/skrill\",\"extension\\/payment\\/squareup\",\"extension\\/payment\\/twocheckout\",\"extension\\/payment\\/web_payment_software\",\"extension\\/payment\\/wechat_pay\",\"extension\\/payment\\/worldpay\",\"extension\\/report\\/customer_activity\",\"extension\\/report\\/customer_order\",\"extension\\/report\\/customer_reward\",\"extension\\/report\\/customer_search\",\"extension\\/report\\/customer_transaction\",\"extension\\/report\\/marketing\",\"extension\\/report\\/product_purchased\",\"extension\\/report\\/product_viewed\",\"extension\\/report\\/sale_coupon\",\"extension\\/report\\/sale_order\",\"extension\\/report\\/sale_return\",\"extension\\/report\\/sale_shipping\",\"extension\\/report\\/sale_tax\",\"extension\\/shipping\\/auspost\",\"extension\\/shipping\\/citylink\",\"extension\\/shipping\\/ec_ship\",\"extension\\/shipping\\/fedex\",\"extension\\/shipping\\/flat\",\"extension\\/shipping\\/free\",\"extension\\/shipping\\/item\",\"extension\\/shipping\\/parcelforce_48\",\"extension\\/shipping\\/pickup\",\"extension\\/shipping\\/royal_mail\",\"extension\\/shipping\\/ups\",\"extension\\/shipping\\/usps\",\"extension\\/shipping\\/weight\",\"extension\\/theme\\/default\",\"extension\\/theme\\/oxy\",\"extension\\/total\\/coupon\",\"extension\\/total\\/credit\",\"extension\\/total\\/handling\",\"extension\\/total\\/klarna_fee\",\"extension\\/total\\/low_order_fee\",\"extension\\/total\\/reward\",\"extension\\/total\\/shipping\",\"extension\\/total\\/sub_total\",\"extension\\/total\\/tax\",\"extension\\/total\\/total\",\"extension\\/total\\/voucher\",\"localisation\\/country\",\"localisation\\/currency\",\"localisation\\/geo_zone\",\"localisation\\/language\",\"localisation\\/length_class\",\"localisation\\/location\",\"localisation\\/order_status\",\"localisation\\/return_action\",\"localisation\\/return_reason\",\"localisation\\/return_status\",\"localisation\\/stock_status\",\"localisation\\/tax_class\",\"localisation\\/tax_rate\",\"localisation\\/weight_class\",\"localisation\\/zone\",\"mail\\/affiliate\",\"mail\\/customer\",\"mail\\/forgotten\",\"mail\\/return\",\"mail\\/reward\",\"mail\\/transaction\",\"marketing\\/contact\",\"marketing\\/coupon\",\"marketing\\/marketing\",\"marketplace\\/api\",\"marketplace\\/event\",\"marketplace\\/extension\",\"marketplace\\/install\",\"marketplace\\/installer\",\"marketplace\\/marketplace\",\"marketplace\\/modification\",\"marketplace\\/openbay\",\"report\\/online\",\"report\\/report\",\"report\\/statistics\",\"sale\\/order\",\"sale\\/recurring\",\"sale\\/return\",\"sale\\/voucher\",\"sale\\/voucher_theme\",\"setting\\/setting\",\"setting\\/store\",\"startup\\/error\",\"startup\\/event\",\"startup\\/login\",\"startup\\/permission\",\"startup\\/router\",\"startup\\/sass\",\"startup\\/startup\",\"tool\\/backup\",\"tool\\/log\",\"tool\\/upload\",\"user\\/api\",\"user\\/user\",\"user\\/user_permission\"]}'),
(10, 'Demonstration', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher`
--

CREATE TABLE `oc_voucher` (
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `from_name` varchar(64) NOT NULL,
  `from_email` varchar(96) NOT NULL,
  `to_name` varchar(64) NOT NULL,
  `to_email` varchar(96) NOT NULL,
  `voucher_theme_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_history`
--

CREATE TABLE `oc_voucher_history` (
  `voucher_history_id` int(11) NOT NULL,
  `voucher_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme`
--

CREATE TABLE `oc_voucher_theme` (
  `voucher_theme_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme`
--

INSERT INTO `oc_voucher_theme` (`voucher_theme_id`, `image`) VALUES
(8, 'catalog/demo/canon_eos_5d_2.jpg'),
(7, 'catalog/demo/gift-voucher-birthday.jpg'),
(6, 'catalog/demo/apple_logo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `oc_voucher_theme_description`
--

CREATE TABLE `oc_voucher_theme_description` (
  `voucher_theme_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_voucher_theme_description`
--

INSERT INTO `oc_voucher_theme_description` (`voucher_theme_id`, `language_id`, `name`) VALUES
(6, 1, 'Christmas'),
(7, 1, 'Birthday'),
(8, 1, 'General'),
(6, 2, 'Christmas'),
(7, 2, 'Birthday'),
(8, 2, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class`
--

CREATE TABLE `oc_weight_class` (
  `weight_class_id` int(11) NOT NULL,
  `value` decimal(15,8) NOT NULL DEFAULT '0.00000000'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class`
--

INSERT INTO `oc_weight_class` (`weight_class_id`, `value`) VALUES
(1, '1.00000000'),
(2, '1000.00000000'),
(5, '2.20460000'),
(6, '35.27400000');

-- --------------------------------------------------------

--
-- Table structure for table `oc_weight_class_description`
--

CREATE TABLE `oc_weight_class_description` (
  `weight_class_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `unit` varchar(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_weight_class_description`
--

INSERT INTO `oc_weight_class_description` (`weight_class_id`, `language_id`, `title`, `unit`) VALUES
(1, 1, 'Kilogram', 'kg'),
(2, 1, 'Gram', 'g'),
(5, 1, 'Pound ', 'lb'),
(6, 1, 'Ounce', 'oz'),
(1, 2, 'Kilogram', 'kg'),
(2, 2, 'Gram', 'g'),
(5, 2, 'Pound ', 'lb'),
(6, 2, 'Ounce', 'oz');

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone`
--

CREATE TABLE `oc_zone` (
  `zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `code` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone`
--

INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1, 1, 'Badakhshan', 'BDS', 1),
(2, 1, 'Badghis', 'BDG', 1),
(3, 1, 'Baghlan', 'BGL', 1),
(4, 1, 'Balkh', 'BAL', 1),
(5, 1, 'Bamian', 'BAM', 1),
(6, 1, 'Farah', 'FRA', 1),
(7, 1, 'Faryab', 'FYB', 1),
(8, 1, 'Ghazni', 'GHA', 1),
(9, 1, 'Ghowr', 'GHO', 1),
(10, 1, 'Helmand', 'HEL', 1),
(11, 1, 'Herat', 'HER', 1),
(12, 1, 'Jowzjan', 'JOW', 1),
(13, 1, 'Kabul', 'KAB', 1),
(14, 1, 'Kandahar', 'KAN', 1),
(15, 1, 'Kapisa', 'KAP', 1),
(16, 1, 'Khost', 'KHO', 1),
(17, 1, 'Konar', 'KNR', 1),
(18, 1, 'Kondoz', 'KDZ', 1),
(19, 1, 'Laghman', 'LAG', 1),
(20, 1, 'Lowgar', 'LOW', 1),
(21, 1, 'Nangrahar', 'NAN', 1),
(22, 1, 'Nimruz', 'NIM', 1),
(23, 1, 'Nurestan', 'NUR', 1),
(24, 1, 'Oruzgan', 'ORU', 1),
(25, 1, 'Paktia', 'PIA', 1),
(26, 1, 'Paktika', 'PKA', 1),
(27, 1, 'Parwan', 'PAR', 1),
(28, 1, 'Samangan', 'SAM', 1),
(29, 1, 'Sar-e Pol', 'SAR', 1),
(30, 1, 'Takhar', 'TAK', 1),
(31, 1, 'Wardak', 'WAR', 1),
(32, 1, 'Zabol', 'ZAB', 1),
(33, 2, 'Berat', 'BR', 1),
(34, 2, 'Bulqize', 'BU', 1),
(35, 2, 'Delvine', 'DL', 1),
(36, 2, 'Devoll', 'DV', 1),
(37, 2, 'Diber', 'DI', 1),
(38, 2, 'Durres', 'DR', 1),
(39, 2, 'Elbasan', 'EL', 1),
(40, 2, 'Kolonje', 'ER', 1),
(41, 2, 'Fier', 'FR', 1),
(42, 2, 'Gjirokaster', 'GJ', 1),
(43, 2, 'Gramsh', 'GR', 1),
(44, 2, 'Has', 'HA', 1),
(45, 2, 'Kavaje', 'KA', 1),
(46, 2, 'Kurbin', 'KB', 1),
(47, 2, 'Kucove', 'KC', 1),
(48, 2, 'Korce', 'KO', 1),
(49, 2, 'Kruje', 'KR', 1),
(50, 2, 'Kukes', 'KU', 1),
(51, 2, 'Librazhd', 'LB', 1),
(52, 2, 'Lezhe', 'LE', 1),
(53, 2, 'Lushnje', 'LU', 1),
(54, 2, 'Malesi e Madhe', 'MM', 1),
(55, 2, 'Mallakaster', 'MK', 1),
(56, 2, 'Mat', 'MT', 1),
(57, 2, 'Mirdite', 'MR', 1),
(58, 2, 'Peqin', 'PQ', 1),
(59, 2, 'Permet', 'PR', 1),
(60, 2, 'Pogradec', 'PG', 1),
(61, 2, 'Puke', 'PU', 1),
(62, 2, 'Shkoder', 'SH', 1),
(63, 2, 'Skrapar', 'SK', 1),
(64, 2, 'Sarande', 'SR', 1),
(65, 2, 'Tepelene', 'TE', 1),
(66, 2, 'Tropoje', 'TP', 1),
(67, 2, 'Tirane', 'TR', 1),
(68, 2, 'Vlore', 'VL', 1),
(69, 3, 'Adrar', 'ADR', 1),
(70, 3, 'Ain Defla', 'ADE', 1),
(71, 3, 'Ain Temouchent', 'ATE', 1),
(72, 3, 'Alger', 'ALG', 1),
(73, 3, 'Annaba', 'ANN', 1),
(74, 3, 'Batna', 'BAT', 1),
(75, 3, 'Bechar', 'BEC', 1),
(76, 3, 'Bejaia', 'BEJ', 1),
(77, 3, 'Biskra', 'BIS', 1),
(78, 3, 'Blida', 'BLI', 1),
(79, 3, 'Bordj Bou Arreridj', 'BBA', 1),
(80, 3, 'Bouira', 'BOA', 1),
(81, 3, 'Boumerdes', 'BMD', 1),
(82, 3, 'Chlef', 'CHL', 1),
(83, 3, 'Constantine', 'CON', 1),
(84, 3, 'Djelfa', 'DJE', 1),
(85, 3, 'El Bayadh', 'EBA', 1),
(86, 3, 'El Oued', 'EOU', 1),
(87, 3, 'El Tarf', 'ETA', 1),
(88, 3, 'Ghardaia', 'GHA', 1),
(89, 3, 'Guelma', 'GUE', 1),
(90, 3, 'Illizi', 'ILL', 1),
(91, 3, 'Jijel', 'JIJ', 1),
(92, 3, 'Khenchela', 'KHE', 1),
(93, 3, 'Laghouat', 'LAG', 1),
(94, 3, 'Muaskar', 'MUA', 1),
(95, 3, 'Medea', 'MED', 1),
(96, 3, 'Mila', 'MIL', 1),
(97, 3, 'Mostaganem', 'MOS', 1),
(98, 3, 'M\'Sila', 'MSI', 1),
(99, 3, 'Naama', 'NAA', 1),
(100, 3, 'Oran', 'ORA', 1),
(101, 3, 'Ouargla', 'OUA', 1),
(102, 3, 'Oum el-Bouaghi', 'OEB', 1),
(103, 3, 'Relizane', 'REL', 1),
(104, 3, 'Saida', 'SAI', 1),
(105, 3, 'Setif', 'SET', 1),
(106, 3, 'Sidi Bel Abbes', 'SBA', 1),
(107, 3, 'Skikda', 'SKI', 1),
(108, 3, 'Souk Ahras', 'SAH', 1),
(109, 3, 'Tamanghasset', 'TAM', 1),
(110, 3, 'Tebessa', 'TEB', 1),
(111, 3, 'Tiaret', 'TIA', 1),
(112, 3, 'Tindouf', 'TIN', 1),
(113, 3, 'Tipaza', 'TIP', 1),
(114, 3, 'Tissemsilt', 'TIS', 1),
(115, 3, 'Tizi Ouzou', 'TOU', 1),
(116, 3, 'Tlemcen', 'TLE', 1),
(117, 4, 'Eastern', 'E', 1),
(118, 4, 'Manu\'a', 'M', 1),
(119, 4, 'Rose Island', 'R', 1),
(120, 4, 'Swains Island', 'S', 1),
(121, 4, 'Western', 'W', 1),
(122, 5, 'Andorra la Vella', 'ALV', 1),
(123, 5, 'Canillo', 'CAN', 1),
(124, 5, 'Encamp', 'ENC', 1),
(125, 5, 'Escaldes-Engordany', 'ESE', 1),
(126, 5, 'La Massana', 'LMA', 1),
(127, 5, 'Ordino', 'ORD', 1),
(128, 5, 'Sant Julia de Loria', 'SJL', 1),
(129, 6, 'Bengo', 'BGO', 1),
(130, 6, 'Benguela', 'BGU', 1),
(131, 6, 'Bie', 'BIE', 1),
(132, 6, 'Cabinda', 'CAB', 1),
(133, 6, 'Cuando-Cubango', 'CCU', 1),
(134, 6, 'Cuanza Norte', 'CNO', 1),
(135, 6, 'Cuanza Sul', 'CUS', 1),
(136, 6, 'Cunene', 'CNN', 1),
(137, 6, 'Huambo', 'HUA', 1),
(138, 6, 'Huila', 'HUI', 1),
(139, 6, 'Luanda', 'LUA', 1),
(140, 6, 'Lunda Norte', 'LNO', 1),
(141, 6, 'Lunda Sul', 'LSU', 1),
(142, 6, 'Malange', 'MAL', 1),
(143, 6, 'Moxico', 'MOX', 1),
(144, 6, 'Namibe', 'NAM', 1),
(145, 6, 'Uige', 'UIG', 1),
(146, 6, 'Zaire', 'ZAI', 1),
(147, 9, 'Saint George', 'ASG', 1),
(148, 9, 'Saint John', 'ASJ', 1),
(149, 9, 'Saint Mary', 'ASM', 1),
(150, 9, 'Saint Paul', 'ASL', 1),
(151, 9, 'Saint Peter', 'ASR', 1),
(152, 9, 'Saint Philip', 'ASH', 1),
(153, 9, 'Barbuda', 'BAR', 1),
(154, 9, 'Redonda', 'RED', 1),
(155, 10, 'Antartida e Islas del Atlantico', 'AN', 1),
(156, 10, 'Buenos Aires', 'BA', 1),
(157, 10, 'Catamarca', 'CA', 1),
(158, 10, 'Chaco', 'CH', 1),
(159, 10, 'Chubut', 'CU', 1),
(160, 10, 'Cordoba', 'CO', 1),
(161, 10, 'Corrientes', 'CR', 1),
(162, 10, 'Distrito Federal', 'DF', 1),
(163, 10, 'Entre Rios', 'ER', 1),
(164, 10, 'Formosa', 'FO', 1),
(165, 10, 'Jujuy', 'JU', 1),
(166, 10, 'La Pampa', 'LP', 1),
(167, 10, 'La Rioja', 'LR', 1),
(168, 10, 'Mendoza', 'ME', 1),
(169, 10, 'Misiones', 'MI', 1),
(170, 10, 'Neuquen', 'NE', 1),
(171, 10, 'Rio Negro', 'RN', 1),
(172, 10, 'Salta', 'SA', 1),
(173, 10, 'San Juan', 'SJ', 1),
(174, 10, 'San Luis', 'SL', 1),
(175, 10, 'Santa Cruz', 'SC', 1),
(176, 10, 'Santa Fe', 'SF', 1),
(177, 10, 'Santiago del Estero', 'SD', 1),
(178, 10, 'Tierra del Fuego', 'TF', 1),
(179, 10, 'Tucuman', 'TU', 1),
(180, 11, 'Aragatsotn', 'AGT', 1),
(181, 11, 'Ararat', 'ARR', 1),
(182, 11, 'Armavir', 'ARM', 1),
(183, 11, 'Geghark\'unik\'', 'GEG', 1),
(184, 11, 'Kotayk\'', 'KOT', 1),
(185, 11, 'Lorri', 'LOR', 1),
(186, 11, 'Shirak', 'SHI', 1),
(187, 11, 'Syunik\'', 'SYU', 1),
(188, 11, 'Tavush', 'TAV', 1),
(189, 11, 'Vayots\' Dzor', 'VAY', 1),
(190, 11, 'Yerevan', 'YER', 1),
(191, 13, 'Australian Capital Territory', 'ACT', 1),
(192, 13, 'New South Wales', 'NSW', 1),
(193, 13, 'Northern Territory', 'NT', 1),
(194, 13, 'Queensland', 'QLD', 1),
(195, 13, 'South Australia', 'SA', 1),
(196, 13, 'Tasmania', 'TAS', 1),
(197, 13, 'Victoria', 'VIC', 1),
(198, 13, 'Western Australia', 'WA', 1),
(199, 14, 'Burgenland', 'BUR', 1),
(200, 14, 'Kärnten', 'KAR', 1),
(201, 14, 'Nieder&ouml;sterreich', 'NOS', 1),
(202, 14, 'Ober&ouml;sterreich', 'OOS', 1),
(203, 14, 'Salzburg', 'SAL', 1),
(204, 14, 'Steiermark', 'STE', 1),
(205, 14, 'Tirol', 'TIR', 1),
(206, 14, 'Vorarlberg', 'VOR', 1),
(207, 14, 'Wien', 'WIE', 1),
(208, 15, 'Ali Bayramli', 'AB', 1),
(209, 15, 'Abseron', 'ABS', 1),
(210, 15, 'AgcabAdi', 'AGC', 1),
(211, 15, 'Agdam', 'AGM', 1),
(212, 15, 'Agdas', 'AGS', 1),
(213, 15, 'Agstafa', 'AGA', 1),
(214, 15, 'Agsu', 'AGU', 1),
(215, 15, 'Astara', 'AST', 1),
(216, 15, 'Baki', 'BA', 1),
(217, 15, 'BabAk', 'BAB', 1),
(218, 15, 'BalakAn', 'BAL', 1),
(219, 15, 'BArdA', 'BAR', 1),
(220, 15, 'Beylaqan', 'BEY', 1),
(221, 15, 'Bilasuvar', 'BIL', 1),
(222, 15, 'Cabrayil', 'CAB', 1),
(223, 15, 'Calilabab', 'CAL', 1),
(224, 15, 'Culfa', 'CUL', 1),
(225, 15, 'Daskasan', 'DAS', 1),
(226, 15, 'Davaci', 'DAV', 1),
(227, 15, 'Fuzuli', 'FUZ', 1),
(228, 15, 'Ganca', 'GA', 1),
(229, 15, 'Gadabay', 'GAD', 1),
(230, 15, 'Goranboy', 'GOR', 1),
(231, 15, 'Goycay', 'GOY', 1),
(232, 15, 'Haciqabul', 'HAC', 1),
(233, 15, 'Imisli', 'IMI', 1),
(234, 15, 'Ismayilli', 'ISM', 1),
(235, 15, 'Kalbacar', 'KAL', 1),
(236, 15, 'Kurdamir', 'KUR', 1),
(237, 15, 'Lankaran', 'LA', 1),
(238, 15, 'Lacin', 'LAC', 1),
(239, 15, 'Lankaran', 'LAN', 1),
(240, 15, 'Lerik', 'LER', 1),
(241, 15, 'Masalli', 'MAS', 1),
(242, 15, 'Mingacevir', 'MI', 1),
(243, 15, 'Naftalan', 'NA', 1),
(244, 15, 'Neftcala', 'NEF', 1),
(245, 15, 'Oguz', 'OGU', 1),
(246, 15, 'Ordubad', 'ORD', 1),
(247, 15, 'Qabala', 'QAB', 1),
(248, 15, 'Qax', 'QAX', 1),
(249, 15, 'Qazax', 'QAZ', 1),
(250, 15, 'Qobustan', 'QOB', 1),
(251, 15, 'Quba', 'QBA', 1),
(252, 15, 'Qubadli', 'QBI', 1),
(253, 15, 'Qusar', 'QUS', 1),
(254, 15, 'Saki', 'SA', 1),
(255, 15, 'Saatli', 'SAT', 1),
(256, 15, 'Sabirabad', 'SAB', 1),
(257, 15, 'Sadarak', 'SAD', 1),
(258, 15, 'Sahbuz', 'SAH', 1),
(259, 15, 'Saki', 'SAK', 1),
(260, 15, 'Salyan', 'SAL', 1),
(261, 15, 'Sumqayit', 'SM', 1),
(262, 15, 'Samaxi', 'SMI', 1),
(263, 15, 'Samkir', 'SKR', 1),
(264, 15, 'Samux', 'SMX', 1),
(265, 15, 'Sarur', 'SAR', 1),
(266, 15, 'Siyazan', 'SIY', 1),
(267, 15, 'Susa', 'SS', 1),
(268, 15, 'Susa', 'SUS', 1),
(269, 15, 'Tartar', 'TAR', 1),
(270, 15, 'Tovuz', 'TOV', 1),
(271, 15, 'Ucar', 'UCA', 1),
(272, 15, 'Xankandi', 'XA', 1),
(273, 15, 'Xacmaz', 'XAC', 1),
(274, 15, 'Xanlar', 'XAN', 1),
(275, 15, 'Xizi', 'XIZ', 1),
(276, 15, 'Xocali', 'XCI', 1),
(277, 15, 'Xocavand', 'XVD', 1),
(278, 15, 'Yardimli', 'YAR', 1),
(279, 15, 'Yevlax', 'YEV', 1),
(280, 15, 'Zangilan', 'ZAN', 1),
(281, 15, 'Zaqatala', 'ZAQ', 1),
(282, 15, 'Zardab', 'ZAR', 1),
(283, 15, 'Naxcivan', 'NX', 1),
(284, 16, 'Acklins', 'ACK', 1),
(285, 16, 'Berry Islands', 'BER', 1),
(286, 16, 'Bimini', 'BIM', 1),
(287, 16, 'Black Point', 'BLK', 1),
(288, 16, 'Cat Island', 'CAT', 1),
(289, 16, 'Central Abaco', 'CAB', 1),
(290, 16, 'Central Andros', 'CAN', 1),
(291, 16, 'Central Eleuthera', 'CEL', 1),
(292, 16, 'City of Freeport', 'FRE', 1),
(293, 16, 'Crooked Island', 'CRO', 1),
(294, 16, 'East Grand Bahama', 'EGB', 1),
(295, 16, 'Exuma', 'EXU', 1),
(296, 16, 'Grand Cay', 'GRD', 1),
(297, 16, 'Harbour Island', 'HAR', 1),
(298, 16, 'Hope Town', 'HOP', 1),
(299, 16, 'Inagua', 'INA', 1),
(300, 16, 'Long Island', 'LNG', 1),
(301, 16, 'Mangrove Cay', 'MAN', 1),
(302, 16, 'Mayaguana', 'MAY', 1),
(303, 16, 'Moore\'s Island', 'MOO', 1),
(304, 16, 'North Abaco', 'NAB', 1),
(305, 16, 'North Andros', 'NAN', 1),
(306, 16, 'North Eleuthera', 'NEL', 1),
(307, 16, 'Ragged Island', 'RAG', 1),
(308, 16, 'Rum Cay', 'RUM', 1),
(309, 16, 'San Salvador', 'SAL', 1),
(310, 16, 'South Abaco', 'SAB', 1),
(311, 16, 'South Andros', 'SAN', 1),
(312, 16, 'South Eleuthera', 'SEL', 1),
(313, 16, 'Spanish Wells', 'SWE', 1),
(314, 16, 'West Grand Bahama', 'WGB', 1),
(315, 17, 'Capital', 'CAP', 1),
(316, 17, 'Central', 'CEN', 1),
(317, 17, 'Muharraq', 'MUH', 1),
(318, 17, 'Northern', 'NOR', 1),
(319, 17, 'Southern', 'SOU', 1),
(320, 18, 'Barisal', 'BAR', 1),
(321, 18, 'Chittagong', 'CHI', 1),
(322, 18, 'Dhaka', 'DHA', 1),
(323, 18, 'Khulna', 'KHU', 1),
(324, 18, 'Rajshahi', 'RAJ', 1),
(325, 18, 'Sylhet', 'SYL', 1),
(326, 19, 'Christ Church', 'CC', 1),
(327, 19, 'Saint Andrew', 'AND', 1),
(328, 19, 'Saint George', 'GEO', 1),
(329, 19, 'Saint James', 'JAM', 1),
(330, 19, 'Saint John', 'JOH', 1),
(331, 19, 'Saint Joseph', 'JOS', 1),
(332, 19, 'Saint Lucy', 'LUC', 1),
(333, 19, 'Saint Michael', 'MIC', 1),
(334, 19, 'Saint Peter', 'PET', 1),
(335, 19, 'Saint Philip', 'PHI', 1),
(336, 19, 'Saint Thomas', 'THO', 1),
(337, 20, 'Brestskaya (Brest)', 'BR', 1),
(338, 20, 'Homyel\'skaya (Homyel\')', 'HO', 1),
(339, 20, 'Horad Minsk', 'HM', 1),
(340, 20, 'Hrodzyenskaya (Hrodna)', 'HR', 1),
(341, 20, 'Mahilyowskaya (Mahilyow)', 'MA', 1),
(342, 20, 'Minskaya', 'MI', 1),
(343, 20, 'Vitsyebskaya (Vitsyebsk)', 'VI', 1),
(344, 21, 'Antwerpen', 'VAN', 1),
(345, 21, 'Brabant Wallon', 'WBR', 1),
(346, 21, 'Hainaut', 'WHT', 1),
(347, 21, 'Liège', 'WLG', 1),
(348, 21, 'Limburg', 'VLI', 1),
(349, 21, 'Luxembourg', 'WLX', 1),
(350, 21, 'Namur', 'WNA', 1),
(351, 21, 'Oost-Vlaanderen', 'VOV', 1),
(352, 21, 'Vlaams Brabant', 'VBR', 1),
(353, 21, 'West-Vlaanderen', 'VWV', 1),
(354, 22, 'Belize', 'BZ', 1),
(355, 22, 'Cayo', 'CY', 1),
(356, 22, 'Corozal', 'CR', 1),
(357, 22, 'Orange Walk', 'OW', 1),
(358, 22, 'Stann Creek', 'SC', 1),
(359, 22, 'Toledo', 'TO', 1),
(360, 23, 'Alibori', 'AL', 1),
(361, 23, 'Atakora', 'AK', 1),
(362, 23, 'Atlantique', 'AQ', 1),
(363, 23, 'Borgou', 'BO', 1),
(364, 23, 'Collines', 'CO', 1),
(365, 23, 'Donga', 'DO', 1),
(366, 23, 'Kouffo', 'KO', 1),
(367, 23, 'Littoral', 'LI', 1),
(368, 23, 'Mono', 'MO', 1),
(369, 23, 'Oueme', 'OU', 1),
(370, 23, 'Plateau', 'PL', 1),
(371, 23, 'Zou', 'ZO', 1),
(372, 24, 'Devonshire', 'DS', 1),
(373, 24, 'Hamilton City', 'HC', 1),
(374, 24, 'Hamilton', 'HA', 1),
(375, 24, 'Paget', 'PG', 1),
(376, 24, 'Pembroke', 'PB', 1),
(377, 24, 'Saint George City', 'GC', 1),
(378, 24, 'Saint George\'s', 'SG', 1),
(379, 24, 'Sandys', 'SA', 1),
(380, 24, 'Smith\'s', 'SM', 1),
(381, 24, 'Southampton', 'SH', 1),
(382, 24, 'Warwick', 'WA', 1),
(383, 25, 'Bumthang', 'BUM', 1),
(384, 25, 'Chukha', 'CHU', 1),
(385, 25, 'Dagana', 'DAG', 1),
(386, 25, 'Gasa', 'GAS', 1),
(387, 25, 'Haa', 'HAA', 1),
(388, 25, 'Lhuntse', 'LHU', 1),
(389, 25, 'Mongar', 'MON', 1),
(390, 25, 'Paro', 'PAR', 1),
(391, 25, 'Pemagatshel', 'PEM', 1),
(392, 25, 'Punakha', 'PUN', 1),
(393, 25, 'Samdrup Jongkhar', 'SJO', 1),
(394, 25, 'Samtse', 'SAT', 1),
(395, 25, 'Sarpang', 'SAR', 1),
(396, 25, 'Thimphu', 'THI', 1),
(397, 25, 'Trashigang', 'TRG', 1),
(398, 25, 'Trashiyangste', 'TRY', 1),
(399, 25, 'Trongsa', 'TRO', 1),
(400, 25, 'Tsirang', 'TSI', 1),
(401, 25, 'Wangdue Phodrang', 'WPH', 1),
(402, 25, 'Zhemgang', 'ZHE', 1),
(403, 26, 'Beni', 'BEN', 1),
(404, 26, 'Chuquisaca', 'CHU', 1),
(405, 26, 'Cochabamba', 'COC', 1),
(406, 26, 'La Paz', 'LPZ', 1),
(407, 26, 'Oruro', 'ORU', 1),
(408, 26, 'Pando', 'PAN', 1),
(409, 26, 'Potosi', 'POT', 1),
(410, 26, 'Santa Cruz', 'SCZ', 1),
(411, 26, 'Tarija', 'TAR', 1),
(412, 27, 'Brcko district', 'BRO', 1),
(413, 27, 'Unsko-Sanski Kanton', 'FUS', 1),
(414, 27, 'Posavski Kanton', 'FPO', 1),
(415, 27, 'Tuzlanski Kanton', 'FTU', 1),
(416, 27, 'Zenicko-Dobojski Kanton', 'FZE', 1),
(417, 27, 'Bosanskopodrinjski Kanton', 'FBP', 1),
(418, 27, 'Srednjebosanski Kanton', 'FSB', 1),
(419, 27, 'Hercegovacko-neretvanski Kanton', 'FHN', 1),
(420, 27, 'Zapadnohercegovacka Zupanija', 'FZH', 1),
(421, 27, 'Kanton Sarajevo', 'FSA', 1),
(422, 27, 'Zapadnobosanska', 'FZA', 1),
(423, 27, 'Banja Luka', 'SBL', 1),
(424, 27, 'Doboj', 'SDO', 1),
(425, 27, 'Bijeljina', 'SBI', 1),
(426, 27, 'Vlasenica', 'SVL', 1),
(427, 27, 'Sarajevo-Romanija or Sokolac', 'SSR', 1),
(428, 27, 'Foca', 'SFO', 1),
(429, 27, 'Trebinje', 'STR', 1),
(430, 28, 'Central', 'CE', 1),
(431, 28, 'Ghanzi', 'GH', 1),
(432, 28, 'Kgalagadi', 'KD', 1),
(433, 28, 'Kgatleng', 'KT', 1),
(434, 28, 'Kweneng', 'KW', 1),
(435, 28, 'Ngamiland', 'NG', 1),
(436, 28, 'North East', 'NE', 1),
(437, 28, 'North West', 'NW', 1),
(438, 28, 'South East', 'SE', 1),
(439, 28, 'Southern', 'SO', 1),
(440, 30, 'Acre', 'AC', 1),
(441, 30, 'Alagoas', 'AL', 1),
(442, 30, 'Amapá', 'AP', 1),
(443, 30, 'Amazonas', 'AM', 1),
(444, 30, 'Bahia', 'BA', 1),
(445, 30, 'Ceará', 'CE', 1),
(446, 30, 'Distrito Federal', 'DF', 1),
(447, 30, 'Espírito Santo', 'ES', 1),
(448, 30, 'Goiás', 'GO', 1),
(449, 30, 'Maranhão', 'MA', 1),
(450, 30, 'Mato Grosso', 'MT', 1),
(451, 30, 'Mato Grosso do Sul', 'MS', 1),
(452, 30, 'Minas Gerais', 'MG', 1),
(453, 30, 'Pará', 'PA', 1),
(454, 30, 'Paraíba', 'PB', 1),
(455, 30, 'Paraná', 'PR', 1),
(456, 30, 'Pernambuco', 'PE', 1),
(457, 30, 'Piauí', 'PI', 1),
(458, 30, 'Rio de Janeiro', 'RJ', 1),
(459, 30, 'Rio Grande do Norte', 'RN', 1),
(460, 30, 'Rio Grande do Sul', 'RS', 1),
(461, 30, 'Rondônia', 'RO', 1),
(462, 30, 'Roraima', 'RR', 1),
(463, 30, 'Santa Catarina', 'SC', 1),
(464, 30, 'São Paulo', 'SP', 1),
(465, 30, 'Sergipe', 'SE', 1),
(466, 30, 'Tocantins', 'TO', 1),
(467, 31, 'Peros Banhos', 'PB', 1),
(468, 31, 'Salomon Islands', 'SI', 1),
(469, 31, 'Nelsons Island', 'NI', 1),
(470, 31, 'Three Brothers', 'TB', 1),
(471, 31, 'Eagle Islands', 'EA', 1),
(472, 31, 'Danger Island', 'DI', 1),
(473, 31, 'Egmont Islands', 'EG', 1),
(474, 31, 'Diego Garcia', 'DG', 1),
(475, 32, 'Belait', 'BEL', 1),
(476, 32, 'Brunei and Muara', 'BRM', 1),
(477, 32, 'Temburong', 'TEM', 1),
(478, 32, 'Tutong', 'TUT', 1),
(479, 33, 'Blagoevgrad', '', 1),
(480, 33, 'Burgas', '', 1),
(481, 33, 'Dobrich', '', 1),
(482, 33, 'Gabrovo', '', 1),
(483, 33, 'Haskovo', '', 1),
(484, 33, 'Kardjali', '', 1),
(485, 33, 'Kyustendil', '', 1),
(486, 33, 'Lovech', '', 1),
(487, 33, 'Montana', '', 1),
(488, 33, 'Pazardjik', '', 1),
(489, 33, 'Pernik', '', 1),
(490, 33, 'Pleven', '', 1),
(491, 33, 'Plovdiv', '', 1),
(492, 33, 'Razgrad', '', 1),
(493, 33, 'Shumen', '', 1),
(494, 33, 'Silistra', '', 1),
(495, 33, 'Sliven', '', 1),
(496, 33, 'Smolyan', '', 1),
(497, 33, 'Sofia', '', 1),
(498, 33, 'Sofia - town', '', 1),
(499, 33, 'Stara Zagora', '', 1),
(500, 33, 'Targovishte', '', 1),
(501, 33, 'Varna', '', 1),
(502, 33, 'Veliko Tarnovo', '', 1),
(503, 33, 'Vidin', '', 1),
(504, 33, 'Vratza', '', 1),
(505, 33, 'Yambol', '', 1),
(506, 34, 'Bale', 'BAL', 1),
(507, 34, 'Bam', 'BAM', 1),
(508, 34, 'Banwa', 'BAN', 1),
(509, 34, 'Bazega', 'BAZ', 1),
(510, 34, 'Bougouriba', 'BOR', 1),
(511, 34, 'Boulgou', 'BLG', 1),
(512, 34, 'Boulkiemde', 'BOK', 1),
(513, 34, 'Comoe', 'COM', 1),
(514, 34, 'Ganzourgou', 'GAN', 1),
(515, 34, 'Gnagna', 'GNA', 1),
(516, 34, 'Gourma', 'GOU', 1),
(517, 34, 'Houet', 'HOU', 1),
(518, 34, 'Ioba', 'IOA', 1),
(519, 34, 'Kadiogo', 'KAD', 1),
(520, 34, 'Kenedougou', 'KEN', 1),
(521, 34, 'Komondjari', 'KOD', 1),
(522, 34, 'Kompienga', 'KOP', 1),
(523, 34, 'Kossi', 'KOS', 1),
(524, 34, 'Koulpelogo', 'KOL', 1),
(525, 34, 'Kouritenga', 'KOT', 1),
(526, 34, 'Kourweogo', 'KOW', 1),
(527, 34, 'Leraba', 'LER', 1),
(528, 34, 'Loroum', 'LOR', 1),
(529, 34, 'Mouhoun', 'MOU', 1),
(530, 34, 'Nahouri', 'NAH', 1),
(531, 34, 'Namentenga', 'NAM', 1),
(532, 34, 'Nayala', 'NAY', 1),
(533, 34, 'Noumbiel', 'NOU', 1),
(534, 34, 'Oubritenga', 'OUB', 1),
(535, 34, 'Oudalan', 'OUD', 1),
(536, 34, 'Passore', 'PAS', 1),
(537, 34, 'Poni', 'PON', 1),
(538, 34, 'Sanguie', 'SAG', 1),
(539, 34, 'Sanmatenga', 'SAM', 1),
(540, 34, 'Seno', 'SEN', 1),
(541, 34, 'Sissili', 'SIS', 1),
(542, 34, 'Soum', 'SOM', 1),
(543, 34, 'Sourou', 'SOR', 1),
(544, 34, 'Tapoa', 'TAP', 1),
(545, 34, 'Tuy', 'TUY', 1),
(546, 34, 'Yagha', 'YAG', 1),
(547, 34, 'Yatenga', 'YAT', 1),
(548, 34, 'Ziro', 'ZIR', 1),
(549, 34, 'Zondoma', 'ZOD', 1),
(550, 34, 'Zoundweogo', 'ZOW', 1),
(551, 35, 'Bubanza', 'BB', 1),
(552, 35, 'Bujumbura', 'BJ', 1),
(553, 35, 'Bururi', 'BR', 1),
(554, 35, 'Cankuzo', 'CA', 1),
(555, 35, 'Cibitoke', 'CI', 1),
(556, 35, 'Gitega', 'GI', 1),
(557, 35, 'Karuzi', 'KR', 1),
(558, 35, 'Kayanza', 'KY', 1),
(559, 35, 'Kirundo', 'KI', 1),
(560, 35, 'Makamba', 'MA', 1),
(561, 35, 'Muramvya', 'MU', 1),
(562, 35, 'Muyinga', 'MY', 1),
(563, 35, 'Mwaro', 'MW', 1),
(564, 35, 'Ngozi', 'NG', 1),
(565, 35, 'Rutana', 'RT', 1),
(566, 35, 'Ruyigi', 'RY', 1),
(567, 36, 'Phnom Penh', 'PP', 1),
(568, 36, 'Preah Seihanu (Kompong Som or Sihanoukville)', 'PS', 1),
(569, 36, 'Pailin', 'PA', 1),
(570, 36, 'Keb', 'KB', 1),
(571, 36, 'Banteay Meanchey', 'BM', 1),
(572, 36, 'Battambang', 'BA', 1),
(573, 36, 'Kampong Cham', 'KM', 1),
(574, 36, 'Kampong Chhnang', 'KN', 1),
(575, 36, 'Kampong Speu', 'KU', 1),
(576, 36, 'Kampong Som', 'KO', 1),
(577, 36, 'Kampong Thom', 'KT', 1),
(578, 36, 'Kampot', 'KP', 1),
(579, 36, 'Kandal', 'KL', 1),
(580, 36, 'Kaoh Kong', 'KK', 1),
(581, 36, 'Kratie', 'KR', 1),
(582, 36, 'Mondul Kiri', 'MK', 1),
(583, 36, 'Oddar Meancheay', 'OM', 1),
(584, 36, 'Pursat', 'PU', 1),
(585, 36, 'Preah Vihear', 'PR', 1),
(586, 36, 'Prey Veng', 'PG', 1),
(587, 36, 'Ratanak Kiri', 'RK', 1),
(588, 36, 'Siemreap', 'SI', 1),
(589, 36, 'Stung Treng', 'ST', 1),
(590, 36, 'Svay Rieng', 'SR', 1),
(591, 36, 'Takeo', 'TK', 1),
(592, 37, 'Adamawa (Adamaoua)', 'ADA', 1),
(593, 37, 'Centre', 'CEN', 1),
(594, 37, 'East (Est)', 'EST', 1),
(595, 37, 'Extreme North (Extreme-Nord)', 'EXN', 1),
(596, 37, 'Littoral', 'LIT', 1),
(597, 37, 'North (Nord)', 'NOR', 1),
(598, 37, 'Northwest (Nord-Ouest)', 'NOT', 1),
(599, 37, 'West (Ouest)', 'OUE', 1),
(600, 37, 'South (Sud)', 'SUD', 1),
(601, 37, 'Southwest (Sud-Ouest).', 'SOU', 1),
(602, 38, 'Alberta', 'AB', 1),
(603, 38, 'British Columbia', 'BC', 1),
(604, 38, 'Manitoba', 'MB', 1),
(605, 38, 'New Brunswick', 'NB', 1),
(606, 38, 'Newfoundland and Labrador', 'NL', 1),
(607, 38, 'Northwest Territories', 'NT', 1),
(608, 38, 'Nova Scotia', 'NS', 1),
(609, 38, 'Nunavut', 'NU', 1),
(610, 38, 'Ontario', 'ON', 1),
(611, 38, 'Prince Edward Island', 'PE', 1),
(612, 38, 'Qu&eacute;bec', 'QC', 1),
(613, 38, 'Saskatchewan', 'SK', 1),
(614, 38, 'Yukon Territory', 'YT', 1),
(615, 39, 'Boa Vista', 'BV', 1),
(616, 39, 'Brava', 'BR', 1),
(617, 39, 'Calheta de Sao Miguel', 'CS', 1),
(618, 39, 'Maio', 'MA', 1),
(619, 39, 'Mosteiros', 'MO', 1),
(620, 39, 'Paul', 'PA', 1),
(621, 39, 'Porto Novo', 'PN', 1),
(622, 39, 'Praia', 'PR', 1),
(623, 39, 'Ribeira Grande', 'RG', 1),
(624, 39, 'Sal', 'SL', 1),
(625, 39, 'Santa Catarina', 'CA', 1),
(626, 39, 'Santa Cruz', 'CR', 1),
(627, 39, 'Sao Domingos', 'SD', 1),
(628, 39, 'Sao Filipe', 'SF', 1),
(629, 39, 'Sao Nicolau', 'SN', 1),
(630, 39, 'Sao Vicente', 'SV', 1),
(631, 39, 'Tarrafal', 'TA', 1),
(632, 40, 'Creek', 'CR', 1),
(633, 40, 'Eastern', 'EA', 1),
(634, 40, 'Midland', 'ML', 1),
(635, 40, 'South Town', 'ST', 1),
(636, 40, 'Spot Bay', 'SP', 1),
(637, 40, 'Stake Bay', 'SK', 1),
(638, 40, 'West End', 'WD', 1),
(639, 40, 'Western', 'WN', 1),
(640, 41, 'Bamingui-Bangoran', 'BBA', 1),
(641, 41, 'Basse-Kotto', 'BKO', 1),
(642, 41, 'Haute-Kotto', 'HKO', 1),
(643, 41, 'Haut-Mbomou', 'HMB', 1),
(644, 41, 'Kemo', 'KEM', 1),
(645, 41, 'Lobaye', 'LOB', 1),
(646, 41, 'Mambere-KadeÔ', 'MKD', 1),
(647, 41, 'Mbomou', 'MBO', 1),
(648, 41, 'Nana-Mambere', 'NMM', 1),
(649, 41, 'Ombella-M\'Poko', 'OMP', 1),
(650, 41, 'Ouaka', 'OUK', 1),
(651, 41, 'Ouham', 'OUH', 1),
(652, 41, 'Ouham-Pende', 'OPE', 1),
(653, 41, 'Vakaga', 'VAK', 1),
(654, 41, 'Nana-Grebizi', 'NGR', 1),
(655, 41, 'Sangha-Mbaere', 'SMB', 1),
(656, 41, 'Bangui', 'BAN', 1),
(657, 42, 'Batha', 'BA', 1),
(658, 42, 'Biltine', 'BI', 1),
(659, 42, 'Borkou-Ennedi-Tibesti', 'BE', 1),
(660, 42, 'Chari-Baguirmi', 'CB', 1),
(661, 42, 'Guera', 'GU', 1),
(662, 42, 'Kanem', 'KA', 1),
(663, 42, 'Lac', 'LA', 1),
(664, 42, 'Logone Occidental', 'LC', 1),
(665, 42, 'Logone Oriental', 'LR', 1),
(666, 42, 'Mayo-Kebbi', 'MK', 1),
(667, 42, 'Moyen-Chari', 'MC', 1),
(668, 42, 'Ouaddai', 'OU', 1),
(669, 42, 'Salamat', 'SA', 1),
(670, 42, 'Tandjile', 'TA', 1),
(671, 43, 'Aisen del General Carlos Ibanez', 'AI', 1),
(672, 43, 'Antofagasta', 'AN', 1),
(673, 43, 'Araucania', 'AR', 1),
(674, 43, 'Atacama', 'AT', 1),
(675, 43, 'Bio-Bio', 'BI', 1),
(676, 43, 'Coquimbo', 'CO', 1),
(677, 43, 'Libertador General Bernardo O\'Higgins', 'LI', 1),
(678, 43, 'Los Lagos', 'LL', 1),
(679, 43, 'Magallanes y de la Antartica Chilena', 'MA', 1),
(680, 43, 'Maule', 'ML', 1),
(681, 43, 'Region Metropolitana', 'RM', 1),
(682, 43, 'Tarapaca', 'TA', 1),
(683, 43, 'Valparaiso', 'VS', 1),
(684, 44, 'Anhui', 'AN', 1),
(685, 44, 'Beijing', 'BE', 1),
(686, 44, 'Chongqing', 'CH', 1),
(687, 44, 'Fujian', 'FU', 1),
(688, 44, 'Gansu', 'GA', 1),
(689, 44, 'Guangdong', 'GU', 1),
(690, 44, 'Guangxi', 'GX', 1),
(691, 44, 'Guizhou', 'GZ', 1),
(692, 44, 'Hainan', 'HA', 1),
(693, 44, 'Hebei', 'HB', 1),
(694, 44, 'Heilongjiang', 'HL', 1),
(695, 44, 'Henan', 'HE', 1),
(696, 44, 'Hong Kong', 'HK', 1),
(697, 44, 'Hubei', 'HU', 1),
(698, 44, 'Hunan', 'HN', 1),
(699, 44, 'Inner Mongolia', 'IM', 1),
(700, 44, 'Jiangsu', 'JI', 1),
(701, 44, 'Jiangxi', 'JX', 1),
(702, 44, 'Jilin', 'JL', 1),
(703, 44, 'Liaoning', 'LI', 1),
(704, 44, 'Macau', 'MA', 1),
(705, 44, 'Ningxia', 'NI', 1),
(706, 44, 'Shaanxi', 'SH', 1),
(707, 44, 'Shandong', 'SA', 1),
(708, 44, 'Shanghai', 'SG', 1),
(709, 44, 'Shanxi', 'SX', 1),
(710, 44, 'Sichuan', 'SI', 1),
(711, 44, 'Tianjin', 'TI', 1),
(712, 44, 'Xinjiang', 'XI', 1),
(713, 44, 'Yunnan', 'YU', 1),
(714, 44, 'Zhejiang', 'ZH', 1),
(715, 46, 'Direction Island', 'D', 1),
(716, 46, 'Home Island', 'H', 1),
(717, 46, 'Horsburgh Island', 'O', 1),
(718, 46, 'South Island', 'S', 1),
(719, 46, 'West Island', 'W', 1),
(720, 47, 'Amazonas', 'AMZ', 1),
(721, 47, 'Antioquia', 'ANT', 1),
(722, 47, 'Arauca', 'ARA', 1),
(723, 47, 'Atlantico', 'ATL', 1),
(724, 47, 'Bogota D.C.', 'BDC', 1),
(725, 47, 'Bolivar', 'BOL', 1),
(726, 47, 'Boyaca', 'BOY', 1),
(727, 47, 'Caldas', 'CAL', 1),
(728, 47, 'Caqueta', 'CAQ', 1),
(729, 47, 'Casanare', 'CAS', 1),
(730, 47, 'Cauca', 'CAU', 1),
(731, 47, 'Cesar', 'CES', 1),
(732, 47, 'Choco', 'CHO', 1),
(733, 47, 'Cordoba', 'COR', 1),
(734, 47, 'Cundinamarca', 'CAM', 1),
(735, 47, 'Guainia', 'GNA', 1),
(736, 47, 'Guajira', 'GJR', 1),
(737, 47, 'Guaviare', 'GVR', 1),
(738, 47, 'Huila', 'HUI', 1),
(739, 47, 'Magdalena', 'MAG', 1),
(740, 47, 'Meta', 'MET', 1),
(741, 47, 'Narino', 'NAR', 1),
(742, 47, 'Norte de Santander', 'NDS', 1),
(743, 47, 'Putumayo', 'PUT', 1),
(744, 47, 'Quindio', 'QUI', 1),
(745, 47, 'Risaralda', 'RIS', 1),
(746, 47, 'San Andres y Providencia', 'SAP', 1),
(747, 47, 'Santander', 'SAN', 1),
(748, 47, 'Sucre', 'SUC', 1),
(749, 47, 'Tolima', 'TOL', 1),
(750, 47, 'Valle del Cauca', 'VDC', 1),
(751, 47, 'Vaupes', 'VAU', 1),
(752, 47, 'Vichada', 'VIC', 1),
(753, 48, 'Grande Comore', 'G', 1),
(754, 48, 'Anjouan', 'A', 1),
(755, 48, 'Moheli', 'M', 1),
(756, 49, 'Bouenza', 'BO', 1),
(757, 49, 'Brazzaville', 'BR', 1),
(758, 49, 'Cuvette', 'CU', 1),
(759, 49, 'Cuvette-Ouest', 'CO', 1),
(760, 49, 'Kouilou', 'KO', 1),
(761, 49, 'Lekoumou', 'LE', 1),
(762, 49, 'Likouala', 'LI', 1),
(763, 49, 'Niari', 'NI', 1),
(764, 49, 'Plateaux', 'PL', 1),
(765, 49, 'Pool', 'PO', 1),
(766, 49, 'Sangha', 'SA', 1),
(767, 50, 'Pukapuka', 'PU', 1),
(768, 50, 'Rakahanga', 'RK', 1),
(769, 50, 'Manihiki', 'MK', 1),
(770, 50, 'Penrhyn', 'PE', 1),
(771, 50, 'Nassau Island', 'NI', 1),
(772, 50, 'Surwarrow', 'SU', 1),
(773, 50, 'Palmerston', 'PA', 1),
(774, 50, 'Aitutaki', 'AI', 1),
(775, 50, 'Manuae', 'MA', 1),
(776, 50, 'Takutea', 'TA', 1),
(777, 50, 'Mitiaro', 'MT', 1),
(778, 50, 'Atiu', 'AT', 1),
(779, 50, 'Mauke', 'MU', 1),
(780, 50, 'Rarotonga', 'RR', 1),
(781, 50, 'Mangaia', 'MG', 1),
(782, 51, 'Alajuela', 'AL', 1),
(783, 51, 'Cartago', 'CA', 1),
(784, 51, 'Guanacaste', 'GU', 1),
(785, 51, 'Heredia', 'HE', 1),
(786, 51, 'Limon', 'LI', 1),
(787, 51, 'Puntarenas', 'PU', 1),
(788, 51, 'San Jose', 'SJ', 1),
(789, 52, 'Abengourou', 'ABE', 1),
(790, 52, 'Abidjan', 'ABI', 1),
(791, 52, 'Aboisso', 'ABO', 1),
(792, 52, 'Adiake', 'ADI', 1),
(793, 52, 'Adzope', 'ADZ', 1),
(794, 52, 'Agboville', 'AGB', 1),
(795, 52, 'Agnibilekrou', 'AGN', 1),
(796, 52, 'Alepe', 'ALE', 1),
(797, 52, 'Bocanda', 'BOC', 1),
(798, 52, 'Bangolo', 'BAN', 1),
(799, 52, 'Beoumi', 'BEO', 1),
(800, 52, 'Biankouma', 'BIA', 1),
(801, 52, 'Bondoukou', 'BDK', 1),
(802, 52, 'Bongouanou', 'BGN', 1),
(803, 52, 'Bouafle', 'BFL', 1),
(804, 52, 'Bouake', 'BKE', 1),
(805, 52, 'Bouna', 'BNA', 1),
(806, 52, 'Boundiali', 'BDL', 1),
(807, 52, 'Dabakala', 'DKL', 1),
(808, 52, 'Dabou', 'DBU', 1),
(809, 52, 'Daloa', 'DAL', 1),
(810, 52, 'Danane', 'DAN', 1),
(811, 52, 'Daoukro', 'DAO', 1),
(812, 52, 'Dimbokro', 'DIM', 1),
(813, 52, 'Divo', 'DIV', 1),
(814, 52, 'Duekoue', 'DUE', 1),
(815, 52, 'Ferkessedougou', 'FER', 1),
(816, 52, 'Gagnoa', 'GAG', 1),
(817, 52, 'Grand-Bassam', 'GBA', 1),
(818, 52, 'Grand-Lahou', 'GLA', 1),
(819, 52, 'Guiglo', 'GUI', 1),
(820, 52, 'Issia', 'ISS', 1),
(821, 52, 'Jacqueville', 'JAC', 1),
(822, 52, 'Katiola', 'KAT', 1),
(823, 52, 'Korhogo', 'KOR', 1),
(824, 52, 'Lakota', 'LAK', 1),
(825, 52, 'Man', 'MAN', 1),
(826, 52, 'Mankono', 'MKN', 1),
(827, 52, 'Mbahiakro', 'MBA', 1),
(828, 52, 'Odienne', 'ODI', 1),
(829, 52, 'Oume', 'OUM', 1),
(830, 52, 'Sakassou', 'SAK', 1),
(831, 52, 'San-Pedro', 'SPE', 1),
(832, 52, 'Sassandra', 'SAS', 1),
(833, 52, 'Seguela', 'SEG', 1),
(834, 52, 'Sinfra', 'SIN', 1),
(835, 52, 'Soubre', 'SOU', 1),
(836, 52, 'Tabou', 'TAB', 1),
(837, 52, 'Tanda', 'TAN', 1),
(838, 52, 'Tiebissou', 'TIE', 1),
(839, 52, 'Tingrela', 'TIN', 1),
(840, 52, 'Tiassale', 'TIA', 1),
(841, 52, 'Touba', 'TBA', 1),
(842, 52, 'Toulepleu', 'TLP', 1),
(843, 52, 'Toumodi', 'TMD', 1),
(844, 52, 'Vavoua', 'VAV', 1),
(845, 52, 'Yamoussoukro', 'YAM', 1),
(846, 52, 'Zuenoula', 'ZUE', 1),
(847, 53, 'Bjelovarsko-bilogorska', 'BB', 1),
(848, 53, 'Grad Zagreb', 'GZ', 1),
(849, 53, 'Dubrovačko-neretvanska', 'DN', 1),
(850, 53, 'Istarska', 'IS', 1),
(851, 53, 'Karlovačka', 'KA', 1),
(852, 53, 'Koprivničko-križevačka', 'KK', 1),
(853, 53, 'Krapinsko-zagorska', 'KZ', 1),
(854, 53, 'Ličko-senjska', 'LS', 1),
(855, 53, 'Međimurska', 'ME', 1),
(856, 53, 'Osječko-baranjska', 'OB', 1),
(857, 53, 'Požeško-slavonska', 'PS', 1),
(858, 53, 'Primorsko-goranska', 'PG', 1),
(859, 53, 'Šibensko-kninska', 'SK', 1),
(860, 53, 'Sisačko-moslavačka', 'SM', 1),
(861, 53, 'Brodsko-posavska', 'BP', 1),
(862, 53, 'Splitsko-dalmatinska', 'SD', 1),
(863, 53, 'Varaždinska', 'VA', 1),
(864, 53, 'Virovitičko-podravska', 'VP', 1),
(865, 53, 'Vukovarsko-srijemska', 'VS', 1),
(866, 53, 'Zadarska', 'ZA', 1),
(867, 53, 'Zagrebačka', 'ZG', 1),
(868, 54, 'Camaguey', 'CA', 1),
(869, 54, 'Ciego de Avila', 'CD', 1),
(870, 54, 'Cienfuegos', 'CI', 1),
(871, 54, 'Ciudad de La Habana', 'CH', 1),
(872, 54, 'Granma', 'GR', 1),
(873, 54, 'Guantanamo', 'GU', 1),
(874, 54, 'Holguin', 'HO', 1),
(875, 54, 'Isla de la Juventud', 'IJ', 1),
(876, 54, 'La Habana', 'LH', 1),
(877, 54, 'Las Tunas', 'LT', 1),
(878, 54, 'Matanzas', 'MA', 1),
(879, 54, 'Pinar del Rio', 'PR', 1),
(880, 54, 'Sancti Spiritus', 'SS', 1),
(881, 54, 'Santiago de Cuba', 'SC', 1),
(882, 54, 'Villa Clara', 'VC', 1),
(883, 55, 'Famagusta', 'F', 1),
(884, 55, 'Kyrenia', 'K', 1),
(885, 55, 'Larnaca', 'A', 1),
(886, 55, 'Limassol', 'I', 1),
(887, 55, 'Nicosia', 'N', 1),
(888, 55, 'Paphos', 'P', 1),
(889, 56, 'Ústecký', 'U', 1),
(890, 56, 'Jihočeský', 'C', 1),
(891, 56, 'Jihomoravský', 'B', 1),
(892, 56, 'Karlovarský', 'K', 1),
(893, 56, 'Královehradecký', 'H', 1),
(894, 56, 'Liberecký', 'L', 1),
(895, 56, 'Moravskoslezský', 'T', 1),
(896, 56, 'Olomoucký', 'M', 1),
(897, 56, 'Pardubický', 'E', 1),
(898, 56, 'Plzeňský', 'P', 1),
(899, 56, 'Praha', 'A', 1),
(900, 56, 'Středočeský', 'S', 1),
(901, 56, 'Vysočina', 'J', 1),
(902, 56, 'Zlínský', 'Z', 1),
(903, 57, 'Arhus', 'AR', 1),
(904, 57, 'Bornholm', 'BH', 1),
(905, 57, 'Copenhagen', 'CO', 1),
(906, 57, 'Faroe Islands', 'FO', 1),
(907, 57, 'Frederiksborg', 'FR', 1),
(908, 57, 'Fyn', 'FY', 1),
(909, 57, 'Kobenhavn', 'KO', 1),
(910, 57, 'Nordjylland', 'NO', 1),
(911, 57, 'Ribe', 'RI', 1),
(912, 57, 'Ringkobing', 'RK', 1),
(913, 57, 'Roskilde', 'RO', 1),
(914, 57, 'Sonderjylland', 'SO', 1),
(915, 57, 'Storstrom', 'ST', 1),
(916, 57, 'Vejle', 'VK', 1),
(917, 57, 'Vestj&aelig;lland', 'VJ', 1),
(918, 57, 'Viborg', 'VB', 1),
(919, 58, '\'Ali Sabih', 'S', 1),
(920, 58, 'Dikhil', 'K', 1),
(921, 58, 'Djibouti', 'J', 1),
(922, 58, 'Obock', 'O', 1),
(923, 58, 'Tadjoura', 'T', 1),
(924, 59, 'Saint Andrew Parish', 'AND', 1),
(925, 59, 'Saint David Parish', 'DAV', 1),
(926, 59, 'Saint George Parish', 'GEO', 1),
(927, 59, 'Saint John Parish', 'JOH', 1),
(928, 59, 'Saint Joseph Parish', 'JOS', 1),
(929, 59, 'Saint Luke Parish', 'LUK', 1),
(930, 59, 'Saint Mark Parish', 'MAR', 1),
(931, 59, 'Saint Patrick Parish', 'PAT', 1),
(932, 59, 'Saint Paul Parish', 'PAU', 1),
(933, 59, 'Saint Peter Parish', 'PET', 1),
(934, 60, 'Distrito Nacional', 'DN', 1),
(935, 60, 'Azua', 'AZ', 1),
(936, 60, 'Baoruco', 'BC', 1),
(937, 60, 'Barahona', 'BH', 1),
(938, 60, 'Dajabon', 'DJ', 1),
(939, 60, 'Duarte', 'DU', 1),
(940, 60, 'Elias Pina', 'EL', 1),
(941, 60, 'El Seybo', 'SY', 1),
(942, 60, 'Espaillat', 'ET', 1),
(943, 60, 'Hato Mayor', 'HM', 1),
(944, 60, 'Independencia', 'IN', 1),
(945, 60, 'La Altagracia', 'AL', 1),
(946, 60, 'La Romana', 'RO', 1),
(947, 60, 'La Vega', 'VE', 1),
(948, 60, 'Maria Trinidad Sanchez', 'MT', 1),
(949, 60, 'Monsenor Nouel', 'MN', 1),
(950, 60, 'Monte Cristi', 'MC', 1),
(951, 60, 'Monte Plata', 'MP', 1),
(952, 60, 'Pedernales', 'PD', 1),
(953, 60, 'Peravia (Bani)', 'PR', 1),
(954, 60, 'Puerto Plata', 'PP', 1),
(955, 60, 'Salcedo', 'SL', 1),
(956, 60, 'Samana', 'SM', 1),
(957, 60, 'Sanchez Ramirez', 'SH', 1),
(958, 60, 'San Cristobal', 'SC', 1),
(959, 60, 'San Jose de Ocoa', 'JO', 1),
(960, 60, 'San Juan', 'SJ', 1),
(961, 60, 'San Pedro de Macoris', 'PM', 1),
(962, 60, 'Santiago', 'SA', 1),
(963, 60, 'Santiago Rodriguez', 'ST', 1),
(964, 60, 'Santo Domingo', 'SD', 1),
(965, 60, 'Valverde', 'VA', 1),
(966, 61, 'Aileu', 'AL', 1),
(967, 61, 'Ainaro', 'AN', 1),
(968, 61, 'Baucau', 'BA', 1),
(969, 61, 'Bobonaro', 'BO', 1),
(970, 61, 'Cova Lima', 'CO', 1),
(971, 61, 'Dili', 'DI', 1),
(972, 61, 'Ermera', 'ER', 1),
(973, 61, 'Lautem', 'LA', 1),
(974, 61, 'Liquica', 'LI', 1),
(975, 61, 'Manatuto', 'MT', 1),
(976, 61, 'Manufahi', 'MF', 1),
(977, 61, 'Oecussi', 'OE', 1),
(978, 61, 'Viqueque', 'VI', 1),
(979, 62, 'Azuay', 'AZU', 1),
(980, 62, 'Bolivar', 'BOL', 1),
(981, 62, 'Ca&ntilde;ar', 'CAN', 1),
(982, 62, 'Carchi', 'CAR', 1),
(983, 62, 'Chimborazo', 'CHI', 1),
(984, 62, 'Cotopaxi', 'COT', 1),
(985, 62, 'El Oro', 'EOR', 1),
(986, 62, 'Esmeraldas', 'ESM', 1),
(987, 62, 'Gal&aacute;pagos', 'GPS', 1),
(988, 62, 'Guayas', 'GUA', 1),
(989, 62, 'Imbabura', 'IMB', 1),
(990, 62, 'Loja', 'LOJ', 1),
(991, 62, 'Los Rios', 'LRO', 1),
(992, 62, 'Manab&iacute;', 'MAN', 1),
(993, 62, 'Morona Santiago', 'MSA', 1),
(994, 62, 'Napo', 'NAP', 1),
(995, 62, 'Orellana', 'ORE', 1),
(996, 62, 'Pastaza', 'PAS', 1),
(997, 62, 'Pichincha', 'PIC', 1),
(998, 62, 'Sucumb&iacute;os', 'SUC', 1),
(999, 62, 'Tungurahua', 'TUN', 1),
(1000, 62, 'Zamora Chinchipe', 'ZCH', 1),
(1001, 63, 'Ad Daqahliyah', 'DHY', 1),
(1002, 63, 'Al Bahr al Ahmar', 'BAM', 1),
(1003, 63, 'Al Buhayrah', 'BHY', 1),
(1004, 63, 'Al Fayyum', 'FYM', 1),
(1005, 63, 'Al Gharbiyah', 'GBY', 1),
(1006, 63, 'Al Iskandariyah', 'IDR', 1),
(1007, 63, 'Al Isma\'iliyah', 'IML', 1),
(1008, 63, 'Al Jizah', 'JZH', 1),
(1009, 63, 'Al Minufiyah', 'MFY', 1),
(1010, 63, 'Al Minya', 'MNY', 1),
(1011, 63, 'Al Qahirah', 'QHR', 1),
(1012, 63, 'Al Qalyubiyah', 'QLY', 1),
(1013, 63, 'Al Wadi al Jadid', 'WJD', 1),
(1014, 63, 'Ash Sharqiyah', 'SHQ', 1),
(1015, 63, 'As Suways', 'SWY', 1),
(1016, 63, 'Aswan', 'ASW', 1),
(1017, 63, 'Asyut', 'ASY', 1),
(1018, 63, 'Bani Suwayf', 'BSW', 1),
(1019, 63, 'Bur Sa\'id', 'BSD', 1),
(1020, 63, 'Dumyat', 'DMY', 1),
(1021, 63, 'Janub Sina\'', 'JNS', 1),
(1022, 63, 'Kafr ash Shaykh', 'KSH', 1),
(1023, 63, 'Matruh', 'MAT', 1),
(1024, 63, 'Qina', 'QIN', 1),
(1025, 63, 'Shamal Sina\'', 'SHS', 1),
(1026, 63, 'Suhaj', 'SUH', 1),
(1027, 64, 'Ahuachapan', 'AH', 1),
(1028, 64, 'Cabanas', 'CA', 1),
(1029, 64, 'Chalatenango', 'CH', 1),
(1030, 64, 'Cuscatlan', 'CU', 1),
(1031, 64, 'La Libertad', 'LB', 1),
(1032, 64, 'La Paz', 'PZ', 1),
(1033, 64, 'La Union', 'UN', 1),
(1034, 64, 'Morazan', 'MO', 1),
(1035, 64, 'San Miguel', 'SM', 1),
(1036, 64, 'San Salvador', 'SS', 1),
(1037, 64, 'San Vicente', 'SV', 1),
(1038, 64, 'Santa Ana', 'SA', 1),
(1039, 64, 'Sonsonate', 'SO', 1),
(1040, 64, 'Usulutan', 'US', 1),
(1041, 65, 'Provincia Annobon', 'AN', 1),
(1042, 65, 'Provincia Bioko Norte', 'BN', 1),
(1043, 65, 'Provincia Bioko Sur', 'BS', 1),
(1044, 65, 'Provincia Centro Sur', 'CS', 1),
(1045, 65, 'Provincia Kie-Ntem', 'KN', 1),
(1046, 65, 'Provincia Litoral', 'LI', 1),
(1047, 65, 'Provincia Wele-Nzas', 'WN', 1),
(1048, 66, 'Central (Maekel)', 'MA', 1),
(1049, 66, 'Anseba (Keren)', 'KE', 1),
(1050, 66, 'Southern Red Sea (Debub-Keih-Bahri)', 'DK', 1),
(1051, 66, 'Northern Red Sea (Semien-Keih-Bahri)', 'SK', 1),
(1052, 66, 'Southern (Debub)', 'DE', 1),
(1053, 66, 'Gash-Barka (Barentu)', 'BR', 1),
(1054, 67, 'Harjumaa (Tallinn)', 'HA', 1),
(1055, 67, 'Hiiumaa (Kardla)', 'HI', 1),
(1056, 67, 'Ida-Virumaa (Johvi)', 'IV', 1),
(1057, 67, 'Jarvamaa (Paide)', 'JA', 1),
(1058, 67, 'Jogevamaa (Jogeva)', 'JO', 1),
(1059, 67, 'Laane-Virumaa (Rakvere)', 'LV', 1),
(1060, 67, 'Laanemaa (Haapsalu)', 'LA', 1),
(1061, 67, 'Parnumaa (Parnu)', 'PA', 1),
(1062, 67, 'Polvamaa (Polva)', 'PO', 1),
(1063, 67, 'Raplamaa (Rapla)', 'RA', 1),
(1064, 67, 'Saaremaa (Kuessaare)', 'SA', 1),
(1065, 67, 'Tartumaa (Tartu)', 'TA', 1),
(1066, 67, 'Valgamaa (Valga)', 'VA', 1),
(1067, 67, 'Viljandimaa (Viljandi)', 'VI', 1),
(1068, 67, 'Vorumaa (Voru)', 'VO', 1),
(1069, 68, 'Afar', 'AF', 1),
(1070, 68, 'Amhara', 'AH', 1),
(1071, 68, 'Benishangul-Gumaz', 'BG', 1),
(1072, 68, 'Gambela', 'GB', 1),
(1073, 68, 'Hariai', 'HR', 1),
(1074, 68, 'Oromia', 'OR', 1),
(1075, 68, 'Somali', 'SM', 1),
(1076, 68, 'Southern Nations - Nationalities and Peoples Region', 'SN', 1),
(1077, 68, 'Tigray', 'TG', 1),
(1078, 68, 'Addis Ababa', 'AA', 1),
(1079, 68, 'Dire Dawa', 'DD', 1),
(1080, 71, 'Central Division', 'C', 1),
(1081, 71, 'Northern Division', 'N', 1),
(1082, 71, 'Eastern Division', 'E', 1),
(1083, 71, 'Western Division', 'W', 1),
(1084, 71, 'Rotuma', 'R', 1),
(1085, 72, 'Ahvenanmaan lääni', 'AL', 1),
(1086, 72, 'Etelä-Suomen lääni', 'ES', 1),
(1087, 72, 'Itä-Suomen lääni', 'IS', 1),
(1088, 72, 'Länsi-Suomen lääni', 'LS', 1),
(1089, 72, 'Lapin lääni', 'LA', 1),
(1090, 72, 'Oulun lääni', 'OU', 1),
(1114, 74, 'Ain', '01', 1),
(1115, 74, 'Aisne', '02', 1),
(1116, 74, 'Allier', '03', 1),
(1117, 74, 'Alpes de Haute Provence', '04', 1),
(1118, 74, 'Hautes-Alpes', '05', 1),
(1119, 74, 'Alpes Maritimes', '06', 1),
(1120, 74, 'Ard&egrave;che', '07', 1),
(1121, 74, 'Ardennes', '08', 1),
(1122, 74, 'Ari&egrave;ge', '09', 1),
(1123, 74, 'Aube', '10', 1),
(1124, 74, 'Aude', '11', 1),
(1125, 74, 'Aveyron', '12', 1),
(1126, 74, 'Bouches du Rh&ocirc;ne', '13', 1),
(1127, 74, 'Calvados', '14', 1),
(1128, 74, 'Cantal', '15', 1),
(1129, 74, 'Charente', '16', 1),
(1130, 74, 'Charente Maritime', '17', 1),
(1131, 74, 'Cher', '18', 1),
(1132, 74, 'Corr&egrave;ze', '19', 1),
(1133, 74, 'Corse du Sud', '2A', 1),
(1134, 74, 'Haute Corse', '2B', 1),
(1135, 74, 'C&ocirc;te d&#039;or', '21', 1),
(1136, 74, 'C&ocirc;tes d&#039;Armor', '22', 1),
(1137, 74, 'Creuse', '23', 1),
(1138, 74, 'Dordogne', '24', 1),
(1139, 74, 'Doubs', '25', 1),
(1140, 74, 'Dr&ocirc;me', '26', 1),
(1141, 74, 'Eure', '27', 1),
(1142, 74, 'Eure et Loir', '28', 1),
(1143, 74, 'Finist&egrave;re', '29', 1),
(1144, 74, 'Gard', '30', 1),
(1145, 74, 'Haute Garonne', '31', 1),
(1146, 74, 'Gers', '32', 1),
(1147, 74, 'Gironde', '33', 1),
(1148, 74, 'H&eacute;rault', '34', 1),
(1149, 74, 'Ille et Vilaine', '35', 1),
(1150, 74, 'Indre', '36', 1),
(1151, 74, 'Indre et Loire', '37', 1),
(1152, 74, 'Is&eacute;re', '38', 1),
(1153, 74, 'Jura', '39', 1),
(1154, 74, 'Landes', '40', 1),
(1155, 74, 'Loir et Cher', '41', 1),
(1156, 74, 'Loire', '42', 1),
(1157, 74, 'Haute Loire', '43', 1),
(1158, 74, 'Loire Atlantique', '44', 1),
(1159, 74, 'Loiret', '45', 1),
(1160, 74, 'Lot', '46', 1),
(1161, 74, 'Lot et Garonne', '47', 1),
(1162, 74, 'Loz&egrave;re', '48', 1),
(1163, 74, 'Maine et Loire', '49', 1),
(1164, 74, 'Manche', '50', 1),
(1165, 74, 'Marne', '51', 1),
(1166, 74, 'Haute Marne', '52', 1),
(1167, 74, 'Mayenne', '53', 1),
(1168, 74, 'Meurthe et Moselle', '54', 1),
(1169, 74, 'Meuse', '55', 1),
(1170, 74, 'Morbihan', '56', 1),
(1171, 74, 'Moselle', '57', 1),
(1172, 74, 'Ni&egrave;vre', '58', 1),
(1173, 74, 'Nord', '59', 1),
(1174, 74, 'Oise', '60', 1),
(1175, 74, 'Orne', '61', 1),
(1176, 74, 'Pas de Calais', '62', 1),
(1177, 74, 'Puy de D&ocirc;me', '63', 1),
(1178, 74, 'Pyr&eacute;n&eacute;es Atlantiques', '64', 1),
(1179, 74, 'Hautes Pyr&eacute;n&eacute;es', '65', 1),
(1180, 74, 'Pyr&eacute;n&eacute;es Orientales', '66', 1),
(1181, 74, 'Bas Rhin', '67', 1),
(1182, 74, 'Haut Rhin', '68', 1),
(1183, 74, 'Rh&ocirc;ne', '69', 1),
(1184, 74, 'Haute Sa&ocirc;ne', '70', 1),
(1185, 74, 'Sa&ocirc;ne et Loire', '71', 1),
(1186, 74, 'Sarthe', '72', 1),
(1187, 74, 'Savoie', '73', 1),
(1188, 74, 'Haute Savoie', '74', 1),
(1189, 74, 'Paris', '75', 1),
(1190, 74, 'Seine Maritime', '76', 1),
(1191, 74, 'Seine et Marne', '77', 1),
(1192, 74, 'Yvelines', '78', 1),
(1193, 74, 'Deux S&egrave;vres', '79', 1),
(1194, 74, 'Somme', '80', 1),
(1195, 74, 'Tarn', '81', 1),
(1196, 74, 'Tarn et Garonne', '82', 1),
(1197, 74, 'Var', '83', 1),
(1198, 74, 'Vaucluse', '84', 1),
(1199, 74, 'Vend&eacute;e', '85', 1),
(1200, 74, 'Vienne', '86', 1),
(1201, 74, 'Haute Vienne', '87', 1),
(1202, 74, 'Vosges', '88', 1),
(1203, 74, 'Yonne', '89', 1),
(1204, 74, 'Territoire de Belfort', '90', 1),
(1205, 74, 'Essonne', '91', 1),
(1206, 74, 'Hauts de Seine', '92', 1),
(1207, 74, 'Seine St-Denis', '93', 1),
(1208, 74, 'Val de Marne', '94', 1),
(1209, 74, 'Val d\'Oise', '95', 1),
(1210, 76, 'Archipel des Marquises', 'M', 1),
(1211, 76, 'Archipel des Tuamotu', 'T', 1),
(1212, 76, 'Archipel des Tubuai', 'I', 1),
(1213, 76, 'Iles du Vent', 'V', 1),
(1214, 76, 'Iles Sous-le-Vent', 'S', 1),
(1215, 77, 'Iles Crozet', 'C', 1),
(1216, 77, 'Iles Kerguelen', 'K', 1),
(1217, 77, 'Ile Amsterdam', 'A', 1),
(1218, 77, 'Ile Saint-Paul', 'P', 1),
(1219, 77, 'Adelie Land', 'D', 1),
(1220, 78, 'Estuaire', 'ES', 1),
(1221, 78, 'Haut-Ogooue', 'HO', 1),
(1222, 78, 'Moyen-Ogooue', 'MO', 1),
(1223, 78, 'Ngounie', 'NG', 1),
(1224, 78, 'Nyanga', 'NY', 1),
(1225, 78, 'Ogooue-Ivindo', 'OI', 1),
(1226, 78, 'Ogooue-Lolo', 'OL', 1),
(1227, 78, 'Ogooue-Maritime', 'OM', 1),
(1228, 78, 'Woleu-Ntem', 'WN', 1),
(1229, 79, 'Banjul', 'BJ', 1),
(1230, 79, 'Basse', 'BS', 1),
(1231, 79, 'Brikama', 'BR', 1),
(1232, 79, 'Janjangbure', 'JA', 1),
(1233, 79, 'Kanifeng', 'KA', 1),
(1234, 79, 'Kerewan', 'KE', 1),
(1235, 79, 'Kuntaur', 'KU', 1),
(1236, 79, 'Mansakonko', 'MA', 1),
(1237, 79, 'Lower River', 'LR', 1),
(1238, 79, 'Central River', 'CR', 1),
(1239, 79, 'North Bank', 'NB', 1),
(1240, 79, 'Upper River', 'UR', 1),
(1241, 79, 'Western', 'WE', 1),
(1242, 80, 'Abkhazia', 'AB', 1),
(1243, 80, 'Ajaria', 'AJ', 1),
(1244, 80, 'Tbilisi', 'TB', 1),
(1245, 80, 'Guria', 'GU', 1),
(1246, 80, 'Imereti', 'IM', 1),
(1247, 80, 'Kakheti', 'KA', 1),
(1248, 80, 'Kvemo Kartli', 'KK', 1),
(1249, 80, 'Mtskheta-Mtianeti', 'MM', 1),
(1250, 80, 'Racha Lechkhumi and Kvemo Svanet', 'RL', 1),
(1251, 80, 'Samegrelo-Zemo Svaneti', 'SZ', 1),
(1252, 80, 'Samtskhe-Javakheti', 'SJ', 1),
(1253, 80, 'Shida Kartli', 'SK', 1),
(1254, 81, 'Baden-W&uuml;rttemberg', 'BAW', 1),
(1255, 81, 'Bayern', 'BAY', 1),
(1256, 81, 'Berlin', 'BER', 1),
(1257, 81, 'Brandenburg', 'BRG', 1),
(1258, 81, 'Bremen', 'BRE', 1),
(1259, 81, 'Hamburg', 'HAM', 1),
(1260, 81, 'Hessen', 'HES', 1),
(1261, 81, 'Mecklenburg-Vorpommern', 'MEC', 1),
(1262, 81, 'Niedersachsen', 'NDS', 1),
(1263, 81, 'Nordrhein-Westfalen', 'NRW', 1),
(1264, 81, 'Rheinland-Pfalz', 'RHE', 1),
(1265, 81, 'Saarland', 'SAR', 1),
(1266, 81, 'Sachsen', 'SAS', 1),
(1267, 81, 'Sachsen-Anhalt', 'SAC', 1),
(1268, 81, 'Schleswig-Holstein', 'SCN', 1),
(1269, 81, 'Th&uuml;ringen', 'THE', 1),
(1270, 82, 'Ashanti Region', 'AS', 1),
(1271, 82, 'Brong-Ahafo Region', 'BA', 1),
(1272, 82, 'Central Region', 'CE', 1),
(1273, 82, 'Eastern Region', 'EA', 1),
(1274, 82, 'Greater Accra Region', 'GA', 1),
(1275, 82, 'Northern Region', 'NO', 1),
(1276, 82, 'Upper East Region', 'UE', 1),
(1277, 82, 'Upper West Region', 'UW', 1),
(1278, 82, 'Volta Region', 'VO', 1),
(1279, 82, 'Western Region', 'WE', 1),
(1280, 84, 'Attica', 'AT', 1),
(1281, 84, 'Central Greece', 'CN', 1),
(1282, 84, 'Central Macedonia', 'CM', 1),
(1283, 84, 'Crete', 'CR', 1),
(1284, 84, 'East Macedonia and Thrace', 'EM', 1),
(1285, 84, 'Epirus', 'EP', 1),
(1286, 84, 'Ionian Islands', 'II', 1),
(1287, 84, 'North Aegean', 'NA', 1),
(1288, 84, 'Peloponnesos', 'PP', 1),
(1289, 84, 'South Aegean', 'SA', 1),
(1290, 84, 'Thessaly', 'TH', 1),
(1291, 84, 'West Greece', 'WG', 1),
(1292, 84, 'West Macedonia', 'WM', 1),
(1293, 85, 'Avannaa', 'A', 1),
(1294, 85, 'Tunu', 'T', 1),
(1295, 85, 'Kitaa', 'K', 1),
(1296, 86, 'Saint Andrew', 'A', 1),
(1297, 86, 'Saint David', 'D', 1),
(1298, 86, 'Saint George', 'G', 1),
(1299, 86, 'Saint John', 'J', 1),
(1300, 86, 'Saint Mark', 'M', 1),
(1301, 86, 'Saint Patrick', 'P', 1),
(1302, 86, 'Carriacou', 'C', 1),
(1303, 86, 'Petit Martinique', 'Q', 1),
(1304, 89, 'Alta Verapaz', 'AV', 1),
(1305, 89, 'Baja Verapaz', 'BV', 1),
(1306, 89, 'Chimaltenango', 'CM', 1),
(1307, 89, 'Chiquimula', 'CQ', 1),
(1308, 89, 'El Peten', 'PE', 1),
(1309, 89, 'El Progreso', 'PR', 1),
(1310, 89, 'El Quiche', 'QC', 1),
(1311, 89, 'Escuintla', 'ES', 1),
(1312, 89, 'Guatemala', 'GU', 1),
(1313, 89, 'Huehuetenango', 'HU', 1),
(1314, 89, 'Izabal', 'IZ', 1),
(1315, 89, 'Jalapa', 'JA', 1),
(1316, 89, 'Jutiapa', 'JU', 1),
(1317, 89, 'Quetzaltenango', 'QZ', 1),
(1318, 89, 'Retalhuleu', 'RE', 1),
(1319, 89, 'Sacatepequez', 'ST', 1),
(1320, 89, 'San Marcos', 'SM', 1),
(1321, 89, 'Santa Rosa', 'SR', 1),
(1322, 89, 'Solola', 'SO', 1),
(1323, 89, 'Suchitepequez', 'SU', 1),
(1324, 89, 'Totonicapan', 'TO', 1),
(1325, 89, 'Zacapa', 'ZA', 1),
(1326, 90, 'Conakry', 'CNK', 1),
(1327, 90, 'Beyla', 'BYL', 1),
(1328, 90, 'Boffa', 'BFA', 1),
(1329, 90, 'Boke', 'BOK', 1),
(1330, 90, 'Coyah', 'COY', 1),
(1331, 90, 'Dabola', 'DBL', 1),
(1332, 90, 'Dalaba', 'DLB', 1),
(1333, 90, 'Dinguiraye', 'DGR', 1),
(1334, 90, 'Dubreka', 'DBR', 1),
(1335, 90, 'Faranah', 'FRN', 1),
(1336, 90, 'Forecariah', 'FRC', 1),
(1337, 90, 'Fria', 'FRI', 1),
(1338, 90, 'Gaoual', 'GAO', 1),
(1339, 90, 'Gueckedou', 'GCD', 1),
(1340, 90, 'Kankan', 'KNK', 1),
(1341, 90, 'Kerouane', 'KRN', 1),
(1342, 90, 'Kindia', 'KND', 1),
(1343, 90, 'Kissidougou', 'KSD', 1),
(1344, 90, 'Koubia', 'KBA', 1),
(1345, 90, 'Koundara', 'KDA', 1),
(1346, 90, 'Kouroussa', 'KRA', 1),
(1347, 90, 'Labe', 'LAB', 1),
(1348, 90, 'Lelouma', 'LLM', 1),
(1349, 90, 'Lola', 'LOL', 1),
(1350, 90, 'Macenta', 'MCT', 1),
(1351, 90, 'Mali', 'MAL', 1),
(1352, 90, 'Mamou', 'MAM', 1),
(1353, 90, 'Mandiana', 'MAN', 1),
(1354, 90, 'Nzerekore', 'NZR', 1),
(1355, 90, 'Pita', 'PIT', 1),
(1356, 90, 'Siguiri', 'SIG', 1),
(1357, 90, 'Telimele', 'TLM', 1),
(1358, 90, 'Tougue', 'TOG', 1),
(1359, 90, 'Yomou', 'YOM', 1),
(1360, 91, 'Bafata Region', 'BF', 1),
(1361, 91, 'Biombo Region', 'BB', 1),
(1362, 91, 'Bissau Region', 'BS', 1),
(1363, 91, 'Bolama Region', 'BL', 1),
(1364, 91, 'Cacheu Region', 'CA', 1),
(1365, 91, 'Gabu Region', 'GA', 1),
(1366, 91, 'Oio Region', 'OI', 1),
(1367, 91, 'Quinara Region', 'QU', 1),
(1368, 91, 'Tombali Region', 'TO', 1),
(1369, 92, 'Barima-Waini', 'BW', 1),
(1370, 92, 'Cuyuni-Mazaruni', 'CM', 1),
(1371, 92, 'Demerara-Mahaica', 'DM', 1),
(1372, 92, 'East Berbice-Corentyne', 'EC', 1),
(1373, 92, 'Essequibo Islands-West Demerara', 'EW', 1),
(1374, 92, 'Mahaica-Berbice', 'MB', 1),
(1375, 92, 'Pomeroon-Supenaam', 'PM', 1),
(1376, 92, 'Potaro-Siparuni', 'PI', 1),
(1377, 92, 'Upper Demerara-Berbice', 'UD', 1),
(1378, 92, 'Upper Takutu-Upper Essequibo', 'UT', 1),
(1379, 93, 'Artibonite', 'AR', 1),
(1380, 93, 'Centre', 'CE', 1),
(1381, 93, 'Grand\'Anse', 'GA', 1),
(1382, 93, 'Nord', 'ND', 1),
(1383, 93, 'Nord-Est', 'NE', 1),
(1384, 93, 'Nord-Ouest', 'NO', 1),
(1385, 93, 'Ouest', 'OU', 1),
(1386, 93, 'Sud', 'SD', 1),
(1387, 93, 'Sud-Est', 'SE', 1),
(1388, 94, 'Flat Island', 'F', 1),
(1389, 94, 'McDonald Island', 'M', 1),
(1390, 94, 'Shag Island', 'S', 1),
(1391, 94, 'Heard Island', 'H', 1),
(1392, 95, 'Atlantida', 'AT', 1),
(1393, 95, 'Choluteca', 'CH', 1),
(1394, 95, 'Colon', 'CL', 1),
(1395, 95, 'Comayagua', 'CM', 1),
(1396, 95, 'Copan', 'CP', 1),
(1397, 95, 'Cortes', 'CR', 1),
(1398, 95, 'El Paraiso', 'PA', 1),
(1399, 95, 'Francisco Morazan', 'FM', 1),
(1400, 95, 'Gracias a Dios', 'GD', 1),
(1401, 95, 'Intibuca', 'IN', 1),
(1402, 95, 'Islas de la Bahia (Bay Islands)', 'IB', 1),
(1403, 95, 'La Paz', 'PZ', 1),
(1404, 95, 'Lempira', 'LE', 1),
(1405, 95, 'Ocotepeque', 'OC', 1),
(1406, 95, 'Olancho', 'OL', 1),
(1407, 95, 'Santa Barbara', 'SB', 1),
(1408, 95, 'Valle', 'VA', 1),
(1409, 95, 'Yoro', 'YO', 1),
(1410, 96, 'Central and Western Hong Kong Island', 'HCW', 1),
(1411, 96, 'Eastern Hong Kong Island', 'HEA', 1),
(1412, 96, 'Southern Hong Kong Island', 'HSO', 1),
(1413, 96, 'Wan Chai Hong Kong Island', 'HWC', 1),
(1414, 96, 'Kowloon City Kowloon', 'KKC', 1),
(1415, 96, 'Kwun Tong Kowloon', 'KKT', 1),
(1416, 96, 'Sham Shui Po Kowloon', 'KSS', 1),
(1417, 96, 'Wong Tai Sin Kowloon', 'KWT', 1),
(1418, 96, 'Yau Tsim Mong Kowloon', 'KYT', 1),
(1419, 96, 'Islands New Territories', 'NIS', 1),
(1420, 96, 'Kwai Tsing New Territories', 'NKT', 1),
(1421, 96, 'North New Territories', 'NNO', 1),
(1422, 96, 'Sai Kung New Territories', 'NSK', 1),
(1423, 96, 'Sha Tin New Territories', 'NST', 1),
(1424, 96, 'Tai Po New Territories', 'NTP', 1),
(1425, 96, 'Tsuen Wan New Territories', 'NTW', 1),
(1426, 96, 'Tuen Mun New Territories', 'NTM', 1),
(1427, 96, 'Yuen Long New Territories', 'NYL', 1),
(1467, 98, 'Austurland', 'AL', 1),
(1468, 98, 'Hofuoborgarsvaeoi', 'HF', 1),
(1469, 98, 'Norourland eystra', 'NE', 1),
(1470, 98, 'Norourland vestra', 'NV', 1),
(1471, 98, 'Suourland', 'SL', 1),
(1472, 98, 'Suournes', 'SN', 1),
(1473, 98, 'Vestfiroir', 'VF', 1),
(1474, 98, 'Vesturland', 'VL', 1),
(1475, 99, 'Andaman and Nicobar Islands', 'AN', 1),
(1476, 99, 'Andhra Pradesh', 'AP', 1),
(1477, 99, 'Arunachal Pradesh', 'AR', 1),
(1478, 99, 'Assam', 'AS', 1),
(1479, 99, 'Bihar', 'BI', 1),
(1480, 99, 'Chandigarh', 'CH', 1),
(1481, 99, 'Dadra and Nagar Haveli', 'DA', 1),
(1482, 99, 'Daman and Diu', 'DM', 1),
(1483, 99, 'Delhi', 'DE', 1),
(1484, 99, 'Goa', 'GO', 1),
(1485, 99, 'Gujarat', 'GU', 1),
(1486, 99, 'Haryana', 'HA', 1),
(1487, 99, 'Himachal Pradesh', 'HP', 1),
(1488, 99, 'Jammu and Kashmir', 'JA', 1),
(1489, 99, 'Karnataka', 'KA', 1),
(1490, 99, 'Kerala', 'KE', 1),
(1491, 99, 'Lakshadweep Islands', 'LI', 1),
(1492, 99, 'Madhya Pradesh', 'MP', 1),
(1493, 99, 'Maharashtra', 'MA', 1),
(1494, 99, 'Manipur', 'MN', 1),
(1495, 99, 'Meghalaya', 'ME', 1),
(1496, 99, 'Mizoram', 'MI', 1),
(1497, 99, 'Nagaland', 'NA', 1),
(1498, 99, 'Orissa', 'OR', 1),
(1499, 99, 'Puducherry', 'PO', 1),
(1500, 99, 'Punjab', 'PU', 1),
(1501, 99, 'Rajasthan', 'RA', 1),
(1502, 99, 'Sikkim', 'SI', 1),
(1503, 99, 'Tamil Nadu', 'TN', 1),
(1504, 99, 'Tripura', 'TR', 1),
(1505, 99, 'Uttar Pradesh', 'UP', 1),
(1506, 99, 'West Bengal', 'WB', 1),
(1507, 100, 'Aceh', 'AC', 1),
(1508, 100, 'Bali', 'BA', 1),
(1509, 100, 'Banten', 'BT', 1),
(1510, 100, 'Bengkulu', 'BE', 1),
(1511, 100, 'BoDeTaBek', 'BD', 1),
(1512, 100, 'Gorontalo', 'GO', 1),
(1513, 100, 'Jakarta Raya', 'JK', 1),
(1514, 100, 'Jambi', 'JA', 1),
(1515, 100, 'Jawa Barat', 'JB', 1),
(1516, 100, 'Jawa Tengah', 'JT', 1),
(1517, 100, 'Jawa Timur', 'JI', 1),
(1518, 100, 'Kalimantan Barat', 'KB', 1),
(1519, 100, 'Kalimantan Selatan', 'KS', 1),
(1520, 100, 'Kalimantan Tengah', 'KT', 1),
(1521, 100, 'Kalimantan Timur', 'KI', 1),
(1522, 100, 'Kepulauan Bangka Belitung', 'BB', 1),
(1523, 100, 'Lampung', 'LA', 1),
(1524, 100, 'Maluku', 'MA', 1),
(1525, 100, 'Maluku Utara', 'MU', 1),
(1526, 100, 'Nusa Tenggara Barat', 'NB', 1),
(1527, 100, 'Nusa Tenggara Timur', 'NT', 1),
(1528, 100, 'Papua', 'PA', 1),
(1529, 100, 'Riau', 'RI', 1),
(1530, 100, 'Sulawesi Selatan', 'SN', 1),
(1531, 100, 'Sulawesi Tengah', 'ST', 1),
(1532, 100, 'Sulawesi Tenggara', 'SG', 1),
(1533, 100, 'Sulawesi Utara', 'SA', 1),
(1534, 100, 'Sumatera Barat', 'SB', 1),
(1535, 100, 'Sumatera Selatan', 'SS', 1),
(1536, 100, 'Sumatera Utara', 'SU', 1),
(1537, 100, 'Yogyakarta', 'YO', 1),
(1538, 101, 'Tehran', 'TEH', 1),
(1539, 101, 'Qom', 'QOM', 1),
(1540, 101, 'Markazi', 'MKZ', 1),
(1541, 101, 'Qazvin', 'QAZ', 1),
(1542, 101, 'Gilan', 'GIL', 1),
(1543, 101, 'Ardabil', 'ARD', 1),
(1544, 101, 'Zanjan', 'ZAN', 1),
(1545, 101, 'East Azarbaijan', 'EAZ', 1),
(1546, 101, 'West Azarbaijan', 'WEZ', 1),
(1547, 101, 'Kurdistan', 'KRD', 1),
(1548, 101, 'Hamadan', 'HMD', 1),
(1549, 101, 'Kermanshah', 'KRM', 1),
(1550, 101, 'Ilam', 'ILM', 1),
(1551, 101, 'Lorestan', 'LRS', 1),
(1552, 101, 'Khuzestan', 'KZT', 1),
(1553, 101, 'Chahar Mahaal and Bakhtiari', 'CMB', 1),
(1554, 101, 'Kohkiluyeh and Buyer Ahmad', 'KBA', 1),
(1555, 101, 'Bushehr', 'BSH', 1),
(1556, 101, 'Fars', 'FAR', 1),
(1557, 101, 'Hormozgan', 'HRM', 1),
(1558, 101, 'Sistan and Baluchistan', 'SBL', 1),
(1559, 101, 'Kerman', 'KRB', 1),
(1560, 101, 'Yazd', 'YZD', 1),
(1561, 101, 'Esfahan', 'EFH', 1),
(1562, 101, 'Semnan', 'SMN', 1),
(1563, 101, 'Mazandaran', 'MZD', 1),
(1564, 101, 'Golestan', 'GLS', 1),
(1565, 101, 'North Khorasan', 'NKH', 1),
(1566, 101, 'Razavi Khorasan', 'RKH', 1),
(1567, 101, 'South Khorasan', 'SKH', 1),
(1568, 102, 'Baghdad', 'BD', 1),
(1569, 102, 'Salah ad Din', 'SD', 1),
(1570, 102, 'Diyala', 'DY', 1),
(1571, 102, 'Wasit', 'WS', 1),
(1572, 102, 'Maysan', 'MY', 1),
(1573, 102, 'Al Basrah', 'BA', 1),
(1574, 102, 'Dhi Qar', 'DQ', 1),
(1575, 102, 'Al Muthanna', 'MU', 1),
(1576, 102, 'Al Qadisyah', 'QA', 1),
(1577, 102, 'Babil', 'BB', 1),
(1578, 102, 'Al Karbala', 'KB', 1),
(1579, 102, 'An Najaf', 'NJ', 1),
(1580, 102, 'Al Anbar', 'AB', 1),
(1581, 102, 'Ninawa', 'NN', 1),
(1582, 102, 'Dahuk', 'DH', 1),
(1583, 102, 'Arbil', 'AL', 1),
(1584, 102, 'At Ta\'mim', 'TM', 1),
(1585, 102, 'As Sulaymaniyah', 'SL', 1),
(1586, 103, 'Carlow', 'CA', 1),
(1587, 103, 'Cavan', 'CV', 1),
(1588, 103, 'Clare', 'CL', 1),
(1589, 103, 'Cork', 'CO', 1),
(1590, 103, 'Donegal', 'DO', 1),
(1591, 103, 'Dublin', 'DU', 1),
(1592, 103, 'Galway', 'GA', 1),
(1593, 103, 'Kerry', 'KE', 1),
(1594, 103, 'Kildare', 'KI', 1),
(1595, 103, 'Kilkenny', 'KL', 1),
(1596, 103, 'Laois', 'LA', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(1597, 103, 'Leitrim', 'LE', 1),
(1598, 103, 'Limerick', 'LI', 1),
(1599, 103, 'Longford', 'LO', 1),
(1600, 103, 'Louth', 'LU', 1),
(1601, 103, 'Mayo', 'MA', 1),
(1602, 103, 'Meath', 'ME', 1),
(1603, 103, 'Monaghan', 'MO', 1),
(1604, 103, 'Offaly', 'OF', 1),
(1605, 103, 'Roscommon', 'RO', 1),
(1606, 103, 'Sligo', 'SL', 1),
(1607, 103, 'Tipperary', 'TI', 1),
(1608, 103, 'Waterford', 'WA', 1),
(1609, 103, 'Westmeath', 'WE', 1),
(1610, 103, 'Wexford', 'WX', 1),
(1611, 103, 'Wicklow', 'WI', 1),
(1612, 104, 'Be\'er Sheva', 'BS', 1),
(1613, 104, 'Bika\'at Hayarden', 'BH', 1),
(1614, 104, 'Eilat and Arava', 'EA', 1),
(1615, 104, 'Galil', 'GA', 1),
(1616, 104, 'Haifa', 'HA', 1),
(1617, 104, 'Jehuda Mountains', 'JM', 1),
(1618, 104, 'Jerusalem', 'JE', 1),
(1619, 104, 'Negev', 'NE', 1),
(1620, 104, 'Semaria', 'SE', 1),
(1621, 104, 'Sharon', 'SH', 1),
(1622, 104, 'Tel Aviv (Gosh Dan)', 'TA', 1),
(3860, 105, 'Caltanissetta', 'CL', 1),
(3842, 105, 'Agrigento', 'AG', 1),
(3843, 105, 'Alessandria', 'AL', 1),
(3844, 105, 'Ancona', 'AN', 1),
(3845, 105, 'Aosta', 'AO', 1),
(3846, 105, 'Arezzo', 'AR', 1),
(3847, 105, 'Ascoli Piceno', 'AP', 1),
(3848, 105, 'Asti', 'AT', 1),
(3849, 105, 'Avellino', 'AV', 1),
(3850, 105, 'Bari', 'BA', 1),
(3851, 105, 'Belluno', 'BL', 1),
(3852, 105, 'Benevento', 'BN', 1),
(3853, 105, 'Bergamo', 'BG', 1),
(3854, 105, 'Biella', 'BI', 1),
(3855, 105, 'Bologna', 'BO', 1),
(3856, 105, 'Bolzano', 'BZ', 1),
(3857, 105, 'Brescia', 'BS', 1),
(3858, 105, 'Brindisi', 'BR', 1),
(3859, 105, 'Cagliari', 'CA', 1),
(1643, 106, 'Clarendon Parish', 'CLA', 1),
(1644, 106, 'Hanover Parish', 'HAN', 1),
(1645, 106, 'Kingston Parish', 'KIN', 1),
(1646, 106, 'Manchester Parish', 'MAN', 1),
(1647, 106, 'Portland Parish', 'POR', 1),
(1648, 106, 'Saint Andrew Parish', 'AND', 1),
(1649, 106, 'Saint Ann Parish', 'ANN', 1),
(1650, 106, 'Saint Catherine Parish', 'CAT', 1),
(1651, 106, 'Saint Elizabeth Parish', 'ELI', 1),
(1652, 106, 'Saint James Parish', 'JAM', 1),
(1653, 106, 'Saint Mary Parish', 'MAR', 1),
(1654, 106, 'Saint Thomas Parish', 'THO', 1),
(1655, 106, 'Trelawny Parish', 'TRL', 1),
(1656, 106, 'Westmoreland Parish', 'WML', 1),
(1657, 107, 'Aichi', 'AI', 1),
(1658, 107, 'Akita', 'AK', 1),
(1659, 107, 'Aomori', 'AO', 1),
(1660, 107, 'Chiba', 'CH', 1),
(1661, 107, 'Ehime', 'EH', 1),
(1662, 107, 'Fukui', 'FK', 1),
(1663, 107, 'Fukuoka', 'FU', 1),
(1664, 107, 'Fukushima', 'FS', 1),
(1665, 107, 'Gifu', 'GI', 1),
(1666, 107, 'Gumma', 'GU', 1),
(1667, 107, 'Hiroshima', 'HI', 1),
(1668, 107, 'Hokkaido', 'HO', 1),
(1669, 107, 'Hyogo', 'HY', 1),
(1670, 107, 'Ibaraki', 'IB', 1),
(1671, 107, 'Ishikawa', 'IS', 1),
(1672, 107, 'Iwate', 'IW', 1),
(1673, 107, 'Kagawa', 'KA', 1),
(1674, 107, 'Kagoshima', 'KG', 1),
(1675, 107, 'Kanagawa', 'KN', 1),
(1676, 107, 'Kochi', 'KO', 1),
(1677, 107, 'Kumamoto', 'KU', 1),
(1678, 107, 'Kyoto', 'KY', 1),
(1679, 107, 'Mie', 'MI', 1),
(1680, 107, 'Miyagi', 'MY', 1),
(1681, 107, 'Miyazaki', 'MZ', 1),
(1682, 107, 'Nagano', 'NA', 1),
(1683, 107, 'Nagasaki', 'NG', 1),
(1684, 107, 'Nara', 'NR', 1),
(1685, 107, 'Niigata', 'NI', 1),
(1686, 107, 'Oita', 'OI', 1),
(1687, 107, 'Okayama', 'OK', 1),
(1688, 107, 'Okinawa', 'ON', 1),
(1689, 107, 'Osaka', 'OS', 1),
(1690, 107, 'Saga', 'SA', 1),
(1691, 107, 'Saitama', 'SI', 1),
(1692, 107, 'Shiga', 'SH', 1),
(1693, 107, 'Shimane', 'SM', 1),
(1694, 107, 'Shizuoka', 'SZ', 1),
(1695, 107, 'Tochigi', 'TO', 1),
(1696, 107, 'Tokushima', 'TS', 1),
(1697, 107, 'Tokyo', 'TK', 1),
(1698, 107, 'Tottori', 'TT', 1),
(1699, 107, 'Toyama', 'TY', 1),
(1700, 107, 'Wakayama', 'WA', 1),
(1701, 107, 'Yamagata', 'YA', 1),
(1702, 107, 'Yamaguchi', 'YM', 1),
(1703, 107, 'Yamanashi', 'YN', 1),
(1704, 108, '\'Amman', 'AM', 1),
(1705, 108, 'Ajlun', 'AJ', 1),
(1706, 108, 'Al \'Aqabah', 'AA', 1),
(1707, 108, 'Al Balqa\'', 'AB', 1),
(1708, 108, 'Al Karak', 'AK', 1),
(1709, 108, 'Al Mafraq', 'AL', 1),
(1710, 108, 'At Tafilah', 'AT', 1),
(1711, 108, 'Az Zarqa\'', 'AZ', 1),
(1712, 108, 'Irbid', 'IR', 1),
(1713, 108, 'Jarash', 'JA', 1),
(1714, 108, 'Ma\'an', 'MA', 1),
(1715, 108, 'Madaba', 'MD', 1),
(1716, 109, 'Almaty', 'AL', 1),
(1717, 109, 'Almaty City', 'AC', 1),
(1718, 109, 'Aqmola', 'AM', 1),
(1719, 109, 'Aqtobe', 'AQ', 1),
(1720, 109, 'Astana City', 'AS', 1),
(1721, 109, 'Atyrau', 'AT', 1),
(1722, 109, 'Batys Qazaqstan', 'BA', 1),
(1723, 109, 'Bayqongyr City', 'BY', 1),
(1724, 109, 'Mangghystau', 'MA', 1),
(1725, 109, 'Ongtustik Qazaqstan', 'ON', 1),
(1726, 109, 'Pavlodar', 'PA', 1),
(1727, 109, 'Qaraghandy', 'QA', 1),
(1728, 109, 'Qostanay', 'QO', 1),
(1729, 109, 'Qyzylorda', 'QY', 1),
(1730, 109, 'Shyghys Qazaqstan', 'SH', 1),
(1731, 109, 'Soltustik Qazaqstan', 'SO', 1),
(1732, 109, 'Zhambyl', 'ZH', 1),
(1733, 110, 'Central', 'CE', 1),
(1734, 110, 'Coast', 'CO', 1),
(1735, 110, 'Eastern', 'EA', 1),
(1736, 110, 'Nairobi Area', 'NA', 1),
(1737, 110, 'North Eastern', 'NE', 1),
(1738, 110, 'Nyanza', 'NY', 1),
(1739, 110, 'Rift Valley', 'RV', 1),
(1740, 110, 'Western', 'WE', 1),
(1741, 111, 'Abaiang', 'AG', 1),
(1742, 111, 'Abemama', 'AM', 1),
(1743, 111, 'Aranuka', 'AK', 1),
(1744, 111, 'Arorae', 'AO', 1),
(1745, 111, 'Banaba', 'BA', 1),
(1746, 111, 'Beru', 'BE', 1),
(1747, 111, 'Butaritari', 'bT', 1),
(1748, 111, 'Kanton', 'KA', 1),
(1749, 111, 'Kiritimati', 'KR', 1),
(1750, 111, 'Kuria', 'KU', 1),
(1751, 111, 'Maiana', 'MI', 1),
(1752, 111, 'Makin', 'MN', 1),
(1753, 111, 'Marakei', 'ME', 1),
(1754, 111, 'Nikunau', 'NI', 1),
(1755, 111, 'Nonouti', 'NO', 1),
(1756, 111, 'Onotoa', 'ON', 1),
(1757, 111, 'Tabiteuea', 'TT', 1),
(1758, 111, 'Tabuaeran', 'TR', 1),
(1759, 111, 'Tamana', 'TM', 1),
(1760, 111, 'Tarawa', 'TW', 1),
(1761, 111, 'Teraina', 'TE', 1),
(1762, 112, 'Chagang-do', 'CHA', 1),
(1763, 112, 'Hamgyong-bukto', 'HAB', 1),
(1764, 112, 'Hamgyong-namdo', 'HAN', 1),
(1765, 112, 'Hwanghae-bukto', 'HWB', 1),
(1766, 112, 'Hwanghae-namdo', 'HWN', 1),
(1767, 112, 'Kangwon-do', 'KAN', 1),
(1768, 112, 'P\'yongan-bukto', 'PYB', 1),
(1769, 112, 'P\'yongan-namdo', 'PYN', 1),
(1770, 112, 'Ryanggang-do (Yanggang-do)', 'YAN', 1),
(1771, 112, 'Rason Directly Governed City', 'NAJ', 1),
(1772, 112, 'P\'yongyang Special City', 'PYO', 1),
(1773, 113, 'Ch\'ungch\'ong-bukto', 'CO', 1),
(1774, 113, 'Ch\'ungch\'ong-namdo', 'CH', 1),
(1775, 113, 'Cheju-do', 'CD', 1),
(1776, 113, 'Cholla-bukto', 'CB', 1),
(1777, 113, 'Cholla-namdo', 'CN', 1),
(1778, 113, 'Inch\'on-gwangyoksi', 'IG', 1),
(1779, 113, 'Kangwon-do', 'KA', 1),
(1780, 113, 'Kwangju-gwangyoksi', 'KG', 1),
(1781, 113, 'Kyonggi-do', 'KD', 1),
(1782, 113, 'Kyongsang-bukto', 'KB', 1),
(1783, 113, 'Kyongsang-namdo', 'KN', 1),
(1784, 113, 'Pusan-gwangyoksi', 'PG', 1),
(1785, 113, 'Soul-t\'ukpyolsi', 'SO', 1),
(1786, 113, 'Taegu-gwangyoksi', 'TA', 1),
(1787, 113, 'Taejon-gwangyoksi', 'TG', 1),
(1788, 114, 'Al \'Asimah', 'AL', 1),
(1789, 114, 'Al Ahmadi', 'AA', 1),
(1790, 114, 'Al Farwaniyah', 'AF', 1),
(1791, 114, 'Al Jahra\'', 'AJ', 1),
(1792, 114, 'Hawalli', 'HA', 1),
(1793, 115, 'Bishkek', 'GB', 1),
(1794, 115, 'Batken', 'B', 1),
(1795, 115, 'Chu', 'C', 1),
(1796, 115, 'Jalal-Abad', 'J', 1),
(1797, 115, 'Naryn', 'N', 1),
(1798, 115, 'Osh', 'O', 1),
(1799, 115, 'Talas', 'T', 1),
(1800, 115, 'Ysyk-Kol', 'Y', 1),
(1801, 116, 'Vientiane', 'VT', 1),
(1802, 116, 'Attapu', 'AT', 1),
(1803, 116, 'Bokeo', 'BK', 1),
(1804, 116, 'Bolikhamxai', 'BL', 1),
(1805, 116, 'Champasak', 'CH', 1),
(1806, 116, 'Houaphan', 'HO', 1),
(1807, 116, 'Khammouan', 'KH', 1),
(1808, 116, 'Louang Namtha', 'LM', 1),
(1809, 116, 'Louangphabang', 'LP', 1),
(1810, 116, 'Oudomxai', 'OU', 1),
(1811, 116, 'Phongsali', 'PH', 1),
(1812, 116, 'Salavan', 'SL', 1),
(1813, 116, 'Savannakhet', 'SV', 1),
(1814, 116, 'Vientiane', 'VI', 1),
(1815, 116, 'Xaignabouli', 'XA', 1),
(1816, 116, 'Xekong', 'XE', 1),
(1817, 116, 'Xiangkhoang', 'XI', 1),
(1818, 116, 'Xaisomboun', 'XN', 1),
(1852, 119, 'Berea', 'BE', 1),
(1853, 119, 'Butha-Buthe', 'BB', 1),
(1854, 119, 'Leribe', 'LE', 1),
(1855, 119, 'Mafeteng', 'MF', 1),
(1856, 119, 'Maseru', 'MS', 1),
(1857, 119, 'Mohale\'s Hoek', 'MH', 1),
(1858, 119, 'Mokhotlong', 'MK', 1),
(1859, 119, 'Qacha\'s Nek', 'QN', 1),
(1860, 119, 'Quthing', 'QT', 1),
(1861, 119, 'Thaba-Tseka', 'TT', 1),
(1862, 120, 'Bomi', 'BI', 1),
(1863, 120, 'Bong', 'BG', 1),
(1864, 120, 'Grand Bassa', 'GB', 1),
(1865, 120, 'Grand Cape Mount', 'CM', 1),
(1866, 120, 'Grand Gedeh', 'GG', 1),
(1867, 120, 'Grand Kru', 'GK', 1),
(1868, 120, 'Lofa', 'LO', 1),
(1869, 120, 'Margibi', 'MG', 1),
(1870, 120, 'Maryland', 'ML', 1),
(1871, 120, 'Montserrado', 'MS', 1),
(1872, 120, 'Nimba', 'NB', 1),
(1873, 120, 'River Cess', 'RC', 1),
(1874, 120, 'Sinoe', 'SN', 1),
(1875, 121, 'Ajdabiya', 'AJ', 1),
(1876, 121, 'Al \'Aziziyah', 'AZ', 1),
(1877, 121, 'Al Fatih', 'FA', 1),
(1878, 121, 'Al Jabal al Akhdar', 'JA', 1),
(1879, 121, 'Al Jufrah', 'JU', 1),
(1880, 121, 'Al Khums', 'KH', 1),
(1881, 121, 'Al Kufrah', 'KU', 1),
(1882, 121, 'An Nuqat al Khams', 'NK', 1),
(1883, 121, 'Ash Shati\'', 'AS', 1),
(1884, 121, 'Awbari', 'AW', 1),
(1885, 121, 'Az Zawiyah', 'ZA', 1),
(1886, 121, 'Banghazi', 'BA', 1),
(1887, 121, 'Darnah', 'DA', 1),
(1888, 121, 'Ghadamis', 'GD', 1),
(1889, 121, 'Gharyan', 'GY', 1),
(1890, 121, 'Misratah', 'MI', 1),
(1891, 121, 'Murzuq', 'MZ', 1),
(1892, 121, 'Sabha', 'SB', 1),
(1893, 121, 'Sawfajjin', 'SW', 1),
(1894, 121, 'Surt', 'SU', 1),
(1895, 121, 'Tarabulus (Tripoli)', 'TL', 1),
(1896, 121, 'Tarhunah', 'TH', 1),
(1897, 121, 'Tubruq', 'TU', 1),
(1898, 121, 'Yafran', 'YA', 1),
(1899, 121, 'Zlitan', 'ZL', 1),
(1900, 122, 'Vaduz', 'V', 1),
(1901, 122, 'Schaan', 'A', 1),
(1902, 122, 'Balzers', 'B', 1),
(1903, 122, 'Triesen', 'N', 1),
(1904, 122, 'Eschen', 'E', 1),
(1905, 122, 'Mauren', 'M', 1),
(1906, 122, 'Triesenberg', 'T', 1),
(1907, 122, 'Ruggell', 'R', 1),
(1908, 122, 'Gamprin', 'G', 1),
(1909, 122, 'Schellenberg', 'L', 1),
(1910, 122, 'Planken', 'P', 1),
(1911, 123, 'Alytus', 'AL', 1),
(1912, 123, 'Kaunas', 'KA', 1),
(1913, 123, 'Klaipeda', 'KL', 1),
(1914, 123, 'Marijampole', 'MA', 1),
(1915, 123, 'Panevezys', 'PA', 1),
(1916, 123, 'Siauliai', 'SI', 1),
(1917, 123, 'Taurage', 'TA', 1),
(1918, 123, 'Telsiai', 'TE', 1),
(1919, 123, 'Utena', 'UT', 1),
(1920, 123, 'Vilnius', 'VI', 1),
(1921, 124, 'Diekirch', 'DD', 1),
(1922, 124, 'Clervaux', 'DC', 1),
(1923, 124, 'Redange', 'DR', 1),
(1924, 124, 'Vianden', 'DV', 1),
(1925, 124, 'Wiltz', 'DW', 1),
(1926, 124, 'Grevenmacher', 'GG', 1),
(1927, 124, 'Echternach', 'GE', 1),
(1928, 124, 'Remich', 'GR', 1),
(1929, 124, 'Luxembourg', 'LL', 1),
(1930, 124, 'Capellen', 'LC', 1),
(1931, 124, 'Esch-sur-Alzette', 'LE', 1),
(1932, 124, 'Mersch', 'LM', 1),
(1933, 125, 'Our Lady Fatima Parish', 'OLF', 1),
(1934, 125, 'St. Anthony Parish', 'ANT', 1),
(1935, 125, 'St. Lazarus Parish', 'LAZ', 1),
(1936, 125, 'Cathedral Parish', 'CAT', 1),
(1937, 125, 'St. Lawrence Parish', 'LAW', 1),
(1938, 127, 'Antananarivo', 'AN', 1),
(1939, 127, 'Antsiranana', 'AS', 1),
(1940, 127, 'Fianarantsoa', 'FN', 1),
(1941, 127, 'Mahajanga', 'MJ', 1),
(1942, 127, 'Toamasina', 'TM', 1),
(1943, 127, 'Toliara', 'TL', 1),
(1944, 128, 'Balaka', 'BLK', 1),
(1945, 128, 'Blantyre', 'BLT', 1),
(1946, 128, 'Chikwawa', 'CKW', 1),
(1947, 128, 'Chiradzulu', 'CRD', 1),
(1948, 128, 'Chitipa', 'CTP', 1),
(1949, 128, 'Dedza', 'DDZ', 1),
(1950, 128, 'Dowa', 'DWA', 1),
(1951, 128, 'Karonga', 'KRG', 1),
(1952, 128, 'Kasungu', 'KSG', 1),
(1953, 128, 'Likoma', 'LKM', 1),
(1954, 128, 'Lilongwe', 'LLG', 1),
(1955, 128, 'Machinga', 'MCG', 1),
(1956, 128, 'Mangochi', 'MGC', 1),
(1957, 128, 'Mchinji', 'MCH', 1),
(1958, 128, 'Mulanje', 'MLJ', 1),
(1959, 128, 'Mwanza', 'MWZ', 1),
(1960, 128, 'Mzimba', 'MZM', 1),
(1961, 128, 'Ntcheu', 'NTU', 1),
(1962, 128, 'Nkhata Bay', 'NKB', 1),
(1963, 128, 'Nkhotakota', 'NKH', 1),
(1964, 128, 'Nsanje', 'NSJ', 1),
(1965, 128, 'Ntchisi', 'NTI', 1),
(1966, 128, 'Phalombe', 'PHL', 1),
(1967, 128, 'Rumphi', 'RMP', 1),
(1968, 128, 'Salima', 'SLM', 1),
(1969, 128, 'Thyolo', 'THY', 1),
(1970, 128, 'Zomba', 'ZBA', 1),
(1971, 129, 'Johor', 'MY-01', 1),
(1972, 129, 'Kedah', 'MY-02', 1),
(1973, 129, 'Kelantan', 'MY-03', 1),
(1974, 129, 'Labuan', 'MY-15', 1),
(1975, 129, 'Melaka', 'MY-04', 1),
(1976, 129, 'Negeri Sembilan', 'MY-05', 1),
(1977, 129, 'Pahang', 'MY-06', 1),
(1978, 129, 'Perak', 'MY-08', 1),
(1979, 129, 'Perlis', 'MY-09', 1),
(1980, 129, 'Pulau Pinang', 'MY-07', 1),
(1981, 129, 'Sabah', 'MY-12', 1),
(1982, 129, 'Sarawak', 'MY-13', 1),
(1983, 129, 'Selangor', 'MY-10', 1),
(1984, 129, 'Terengganu', 'MY-11', 1),
(1985, 129, 'Kuala Lumpur', 'MY-14', 1),
(4035, 129, 'Putrajaya', 'MY-16', 1),
(1986, 130, 'Thiladhunmathi Uthuru', 'THU', 1),
(1987, 130, 'Thiladhunmathi Dhekunu', 'THD', 1),
(1988, 130, 'Miladhunmadulu Uthuru', 'MLU', 1),
(1989, 130, 'Miladhunmadulu Dhekunu', 'MLD', 1),
(1990, 130, 'Maalhosmadulu Uthuru', 'MAU', 1),
(1991, 130, 'Maalhosmadulu Dhekunu', 'MAD', 1),
(1992, 130, 'Faadhippolhu', 'FAA', 1),
(1993, 130, 'Male Atoll', 'MAA', 1),
(1994, 130, 'Ari Atoll Uthuru', 'AAU', 1),
(1995, 130, 'Ari Atoll Dheknu', 'AAD', 1),
(1996, 130, 'Felidhe Atoll', 'FEA', 1),
(1997, 130, 'Mulaku Atoll', 'MUA', 1),
(1998, 130, 'Nilandhe Atoll Uthuru', 'NAU', 1),
(1999, 130, 'Nilandhe Atoll Dhekunu', 'NAD', 1),
(2000, 130, 'Kolhumadulu', 'KLH', 1),
(2001, 130, 'Hadhdhunmathi', 'HDH', 1),
(2002, 130, 'Huvadhu Atoll Uthuru', 'HAU', 1),
(2003, 130, 'Huvadhu Atoll Dhekunu', 'HAD', 1),
(2004, 130, 'Fua Mulaku', 'FMU', 1),
(2005, 130, 'Addu', 'ADD', 1),
(2006, 131, 'Gao', 'GA', 1),
(2007, 131, 'Kayes', 'KY', 1),
(2008, 131, 'Kidal', 'KD', 1),
(2009, 131, 'Koulikoro', 'KL', 1),
(2010, 131, 'Mopti', 'MP', 1),
(2011, 131, 'Segou', 'SG', 1),
(2012, 131, 'Sikasso', 'SK', 1),
(2013, 131, 'Tombouctou', 'TB', 1),
(2014, 131, 'Bamako Capital District', 'CD', 1),
(2015, 132, 'Attard', 'ATT', 1),
(2016, 132, 'Balzan', 'BAL', 1),
(2017, 132, 'Birgu', 'BGU', 1),
(2018, 132, 'Birkirkara', 'BKK', 1),
(2019, 132, 'Birzebbuga', 'BRZ', 1),
(2020, 132, 'Bormla', 'BOR', 1),
(2021, 132, 'Dingli', 'DIN', 1),
(2022, 132, 'Fgura', 'FGU', 1),
(2023, 132, 'Floriana', 'FLO', 1),
(2024, 132, 'Gudja', 'GDJ', 1),
(2025, 132, 'Gzira', 'GZR', 1),
(2026, 132, 'Gargur', 'GRG', 1),
(2027, 132, 'Gaxaq', 'GXQ', 1),
(2028, 132, 'Hamrun', 'HMR', 1),
(2029, 132, 'Iklin', 'IKL', 1),
(2030, 132, 'Isla', 'ISL', 1),
(2031, 132, 'Kalkara', 'KLK', 1),
(2032, 132, 'Kirkop', 'KRK', 1),
(2033, 132, 'Lija', 'LIJ', 1),
(2034, 132, 'Luqa', 'LUQ', 1),
(2035, 132, 'Marsa', 'MRS', 1),
(2036, 132, 'Marsaskala', 'MKL', 1),
(2037, 132, 'Marsaxlokk', 'MXL', 1),
(2038, 132, 'Mdina', 'MDN', 1),
(2039, 132, 'Melliea', 'MEL', 1),
(2040, 132, 'Mgarr', 'MGR', 1),
(2041, 132, 'Mosta', 'MST', 1),
(2042, 132, 'Mqabba', 'MQA', 1),
(2043, 132, 'Msida', 'MSI', 1),
(2044, 132, 'Mtarfa', 'MTF', 1),
(2045, 132, 'Naxxar', 'NAX', 1),
(2046, 132, 'Paola', 'PAO', 1),
(2047, 132, 'Pembroke', 'PEM', 1),
(2048, 132, 'Pieta', 'PIE', 1),
(2049, 132, 'Qormi', 'QOR', 1),
(2050, 132, 'Qrendi', 'QRE', 1),
(2051, 132, 'Rabat', 'RAB', 1),
(2052, 132, 'Safi', 'SAF', 1),
(2053, 132, 'San Giljan', 'SGI', 1),
(2054, 132, 'Santa Lucija', 'SLU', 1),
(2055, 132, 'San Pawl il-Bahar', 'SPB', 1),
(2056, 132, 'San Gwann', 'SGW', 1),
(2057, 132, 'Santa Venera', 'SVE', 1),
(2058, 132, 'Siggiewi', 'SIG', 1),
(2059, 132, 'Sliema', 'SLM', 1),
(2060, 132, 'Swieqi', 'SWQ', 1),
(2061, 132, 'Ta Xbiex', 'TXB', 1),
(2062, 132, 'Tarxien', 'TRX', 1),
(2063, 132, 'Valletta', 'VLT', 1),
(2064, 132, 'Xgajra', 'XGJ', 1),
(2065, 132, 'Zabbar', 'ZBR', 1),
(2066, 132, 'Zebbug', 'ZBG', 1),
(2067, 132, 'Zejtun', 'ZJT', 1),
(2068, 132, 'Zurrieq', 'ZRQ', 1),
(2069, 132, 'Fontana', 'FNT', 1),
(2070, 132, 'Ghajnsielem', 'GHJ', 1),
(2071, 132, 'Gharb', 'GHR', 1),
(2072, 132, 'Ghasri', 'GHS', 1),
(2073, 132, 'Kercem', 'KRC', 1),
(2074, 132, 'Munxar', 'MUN', 1),
(2075, 132, 'Nadur', 'NAD', 1),
(2076, 132, 'Qala', 'QAL', 1),
(2077, 132, 'Victoria', 'VIC', 1),
(2078, 132, 'San Lawrenz', 'SLA', 1),
(2079, 132, 'Sannat', 'SNT', 1),
(2080, 132, 'Xagra', 'ZAG', 1),
(2081, 132, 'Xewkija', 'XEW', 1),
(2082, 132, 'Zebbug', 'ZEB', 1),
(2083, 133, 'Ailinginae', 'ALG', 1),
(2084, 133, 'Ailinglaplap', 'ALL', 1),
(2085, 133, 'Ailuk', 'ALK', 1),
(2086, 133, 'Arno', 'ARN', 1),
(2087, 133, 'Aur', 'AUR', 1),
(2088, 133, 'Bikar', 'BKR', 1),
(2089, 133, 'Bikini', 'BKN', 1),
(2090, 133, 'Bokak', 'BKK', 1),
(2091, 133, 'Ebon', 'EBN', 1),
(2092, 133, 'Enewetak', 'ENT', 1),
(2093, 133, 'Erikub', 'EKB', 1),
(2094, 133, 'Jabat', 'JBT', 1),
(2095, 133, 'Jaluit', 'JLT', 1),
(2096, 133, 'Jemo', 'JEM', 1),
(2097, 133, 'Kili', 'KIL', 1),
(2098, 133, 'Kwajalein', 'KWJ', 1),
(2099, 133, 'Lae', 'LAE', 1),
(2100, 133, 'Lib', 'LIB', 1),
(2101, 133, 'Likiep', 'LKP', 1),
(2102, 133, 'Majuro', 'MJR', 1),
(2103, 133, 'Maloelap', 'MLP', 1),
(2104, 133, 'Mejit', 'MJT', 1),
(2105, 133, 'Mili', 'MIL', 1),
(2106, 133, 'Namorik', 'NMK', 1),
(2107, 133, 'Namu', 'NAM', 1),
(2108, 133, 'Rongelap', 'RGL', 1),
(2109, 133, 'Rongrik', 'RGK', 1),
(2110, 133, 'Toke', 'TOK', 1),
(2111, 133, 'Ujae', 'UJA', 1),
(2112, 133, 'Ujelang', 'UJL', 1),
(2113, 133, 'Utirik', 'UTK', 1),
(2114, 133, 'Wotho', 'WTH', 1),
(2115, 133, 'Wotje', 'WTJ', 1),
(2116, 135, 'Adrar', 'AD', 1),
(2117, 135, 'Assaba', 'AS', 1),
(2118, 135, 'Brakna', 'BR', 1),
(2119, 135, 'Dakhlet Nouadhibou', 'DN', 1),
(2120, 135, 'Gorgol', 'GO', 1),
(2121, 135, 'Guidimaka', 'GM', 1),
(2122, 135, 'Hodh Ech Chargui', 'HC', 1),
(2123, 135, 'Hodh El Gharbi', 'HG', 1),
(2124, 135, 'Inchiri', 'IN', 1),
(2125, 135, 'Tagant', 'TA', 1),
(2126, 135, 'Tiris Zemmour', 'TZ', 1),
(2127, 135, 'Trarza', 'TR', 1),
(2128, 135, 'Nouakchott', 'NO', 1),
(2129, 136, 'Beau Bassin-Rose Hill', 'BR', 1),
(2130, 136, 'Curepipe', 'CU', 1),
(2131, 136, 'Port Louis', 'PU', 1),
(2132, 136, 'Quatre Bornes', 'QB', 1),
(2133, 136, 'Vacoas-Phoenix', 'VP', 1),
(2134, 136, 'Agalega Islands', 'AG', 1),
(2135, 136, 'Cargados Carajos Shoals (Saint Brandon Islands)', 'CC', 1),
(2136, 136, 'Rodrigues', 'RO', 1),
(2137, 136, 'Black River', 'BL', 1),
(2138, 136, 'Flacq', 'FL', 1),
(2139, 136, 'Grand Port', 'GP', 1),
(2140, 136, 'Moka', 'MO', 1),
(2141, 136, 'Pamplemousses', 'PA', 1),
(2142, 136, 'Plaines Wilhems', 'PW', 1),
(2143, 136, 'Port Louis', 'PL', 1),
(2144, 136, 'Riviere du Rempart', 'RR', 1),
(2145, 136, 'Savanne', 'SA', 1),
(2146, 138, 'Baja California Norte', 'BN', 1),
(2147, 138, 'Baja California Sur', 'BS', 1),
(2148, 138, 'Campeche', 'CA', 1),
(2149, 138, 'Chiapas', 'CI', 1),
(2150, 138, 'Chihuahua', 'CH', 1),
(2151, 138, 'Coahuila de Zaragoza', 'CZ', 1),
(2152, 138, 'Colima', 'CL', 1),
(2153, 138, 'Distrito Federal', 'DF', 1),
(2154, 138, 'Durango', 'DU', 1),
(2155, 138, 'Guanajuato', 'GA', 1),
(2156, 138, 'Guerrero', 'GE', 1),
(2157, 138, 'Hidalgo', 'HI', 1),
(2158, 138, 'Jalisco', 'JA', 1),
(2159, 138, 'Mexico', 'ME', 1),
(2160, 138, 'Michoacan de Ocampo', 'MI', 1),
(2161, 138, 'Morelos', 'MO', 1),
(2162, 138, 'Nayarit', 'NA', 1),
(2163, 138, 'Nuevo Leon', 'NL', 1),
(2164, 138, 'Oaxaca', 'OA', 1),
(2165, 138, 'Puebla', 'PU', 1),
(2166, 138, 'Queretaro de Arteaga', 'QA', 1),
(2167, 138, 'Quintana Roo', 'QR', 1),
(2168, 138, 'San Luis Potosi', 'SA', 1),
(2169, 138, 'Sinaloa', 'SI', 1),
(2170, 138, 'Sonora', 'SO', 1),
(2171, 138, 'Tabasco', 'TB', 1),
(2172, 138, 'Tamaulipas', 'TM', 1),
(2173, 138, 'Tlaxcala', 'TL', 1),
(2174, 138, 'Veracruz-Llave', 'VE', 1),
(2175, 138, 'Yucatan', 'YU', 1),
(2176, 138, 'Zacatecas', 'ZA', 1),
(2177, 139, 'Chuuk', 'C', 1),
(2178, 139, 'Kosrae', 'K', 1),
(2179, 139, 'Pohnpei', 'P', 1),
(2180, 139, 'Yap', 'Y', 1),
(2181, 140, 'Gagauzia', 'GA', 1),
(2182, 140, 'Chisinau', 'CU', 1),
(2183, 140, 'Balti', 'BA', 1),
(2184, 140, 'Cahul', 'CA', 1),
(2185, 140, 'Edinet', 'ED', 1),
(2186, 140, 'Lapusna', 'LA', 1),
(2187, 140, 'Orhei', 'OR', 1),
(2188, 140, 'Soroca', 'SO', 1),
(2189, 140, 'Tighina', 'TI', 1),
(2190, 140, 'Ungheni', 'UN', 1),
(2191, 140, 'St‚nga Nistrului', 'SN', 1),
(2192, 141, 'Fontvieille', 'FV', 1),
(2193, 141, 'La Condamine', 'LC', 1),
(2194, 141, 'Monaco-Ville', 'MV', 1),
(2195, 141, 'Monte-Carlo', 'MC', 1),
(2196, 142, 'Ulanbaatar', '1', 1),
(2197, 142, 'Orhon', '035', 1),
(2198, 142, 'Darhan uul', '037', 1),
(2199, 142, 'Hentiy', '039', 1),
(2200, 142, 'Hovsgol', '041', 1),
(2201, 142, 'Hovd', '043', 1),
(2202, 142, 'Uvs', '046', 1),
(2203, 142, 'Tov', '047', 1),
(2204, 142, 'Selenge', '049', 1),
(2205, 142, 'Suhbaatar', '051', 1),
(2206, 142, 'Omnogovi', '053', 1),
(2207, 142, 'Ovorhangay', '055', 1),
(2208, 142, 'Dzavhan', '057', 1),
(2209, 142, 'DundgovL', '059', 1),
(2210, 142, 'Dornod', '061', 1),
(2211, 142, 'Dornogov', '063', 1),
(2212, 142, 'Govi-Sumber', '064', 1),
(2213, 142, 'Govi-Altay', '065', 1),
(2214, 142, 'Bulgan', '067', 1),
(2215, 142, 'Bayanhongor', '069', 1),
(2216, 142, 'Bayan-Olgiy', '071', 1),
(2217, 142, 'Arhangay', '073', 1),
(2218, 143, 'Saint Anthony', 'A', 1),
(2219, 143, 'Saint Georges', 'G', 1),
(2220, 143, 'Saint Peter', 'P', 1),
(2221, 144, 'Agadir', 'AGD', 1),
(2222, 144, 'Al Hoceima', 'HOC', 1),
(2223, 144, 'Azilal', 'AZI', 1),
(2224, 144, 'Beni Mellal', 'BME', 1),
(2225, 144, 'Ben Slimane', 'BSL', 1),
(2226, 144, 'Boulemane', 'BLM', 1),
(2227, 144, 'Casablanca', 'CBL', 1),
(2228, 144, 'Chaouen', 'CHA', 1),
(2229, 144, 'El Jadida', 'EJA', 1),
(2230, 144, 'El Kelaa des Sraghna', 'EKS', 1),
(2231, 144, 'Er Rachidia', 'ERA', 1),
(2232, 144, 'Essaouira', 'ESS', 1),
(2233, 144, 'Fes', 'FES', 1),
(2234, 144, 'Figuig', 'FIG', 1),
(2235, 144, 'Guelmim', 'GLM', 1),
(2236, 144, 'Ifrane', 'IFR', 1),
(2237, 144, 'Kenitra', 'KEN', 1),
(2238, 144, 'Khemisset', 'KHM', 1),
(2239, 144, 'Khenifra', 'KHN', 1),
(2240, 144, 'Khouribga', 'KHO', 1),
(2241, 144, 'Laayoune', 'LYN', 1),
(2242, 144, 'Larache', 'LAR', 1),
(2243, 144, 'Marrakech', 'MRK', 1),
(2244, 144, 'Meknes', 'MKN', 1),
(2245, 144, 'Nador', 'NAD', 1),
(2246, 144, 'Ouarzazate', 'ORZ', 1),
(2247, 144, 'Oujda', 'OUJ', 1),
(2248, 144, 'Rabat-Sale', 'RSA', 1),
(2249, 144, 'Safi', 'SAF', 1),
(2250, 144, 'Settat', 'SET', 1),
(2251, 144, 'Sidi Kacem', 'SKA', 1),
(2252, 144, 'Tangier', 'TGR', 1),
(2253, 144, 'Tan-Tan', 'TAN', 1),
(2254, 144, 'Taounate', 'TAO', 1),
(2255, 144, 'Taroudannt', 'TRD', 1),
(2256, 144, 'Tata', 'TAT', 1),
(2257, 144, 'Taza', 'TAZ', 1),
(2258, 144, 'Tetouan', 'TET', 1),
(2259, 144, 'Tiznit', 'TIZ', 1),
(2260, 144, 'Ad Dakhla', 'ADK', 1),
(2261, 144, 'Boujdour', 'BJD', 1),
(2262, 144, 'Es Smara', 'ESM', 1),
(2263, 145, 'Cabo Delgado', 'CD', 1),
(2264, 145, 'Gaza', 'GZ', 1),
(2265, 145, 'Inhambane', 'IN', 1),
(2266, 145, 'Manica', 'MN', 1),
(2267, 145, 'Maputo (city)', 'MC', 1),
(2268, 145, 'Maputo', 'MP', 1),
(2269, 145, 'Nampula', 'NA', 1),
(2270, 145, 'Niassa', 'NI', 1),
(2271, 145, 'Sofala', 'SO', 1),
(2272, 145, 'Tete', 'TE', 1),
(2273, 145, 'Zambezia', 'ZA', 1),
(2274, 146, 'Ayeyarwady', 'AY', 1),
(2275, 146, 'Bago', 'BG', 1),
(2276, 146, 'Magway', 'MG', 1),
(2277, 146, 'Mandalay', 'MD', 1),
(2278, 146, 'Sagaing', 'SG', 1),
(2279, 146, 'Tanintharyi', 'TN', 1),
(2280, 146, 'Yangon', 'YG', 1),
(2281, 146, 'Chin State', 'CH', 1),
(2282, 146, 'Kachin State', 'KC', 1),
(2283, 146, 'Kayah State', 'KH', 1),
(2284, 146, 'Kayin State', 'KN', 1),
(2285, 146, 'Mon State', 'MN', 1),
(2286, 146, 'Rakhine State', 'RK', 1),
(2287, 146, 'Shan State', 'SH', 1),
(2288, 147, 'Caprivi', 'CA', 1),
(2289, 147, 'Erongo', 'ER', 1),
(2290, 147, 'Hardap', 'HA', 1),
(2291, 147, 'Karas', 'KR', 1),
(2292, 147, 'Kavango', 'KV', 1),
(2293, 147, 'Khomas', 'KH', 1),
(2294, 147, 'Kunene', 'KU', 1),
(2295, 147, 'Ohangwena', 'OW', 1),
(2296, 147, 'Omaheke', 'OK', 1),
(2297, 147, 'Omusati', 'OT', 1),
(2298, 147, 'Oshana', 'ON', 1),
(2299, 147, 'Oshikoto', 'OO', 1),
(2300, 147, 'Otjozondjupa', 'OJ', 1),
(2301, 148, 'Aiwo', 'AO', 1),
(2302, 148, 'Anabar', 'AA', 1),
(2303, 148, 'Anetan', 'AT', 1),
(2304, 148, 'Anibare', 'AI', 1),
(2305, 148, 'Baiti', 'BA', 1),
(2306, 148, 'Boe', 'BO', 1),
(2307, 148, 'Buada', 'BU', 1),
(2308, 148, 'Denigomodu', 'DE', 1),
(2309, 148, 'Ewa', 'EW', 1),
(2310, 148, 'Ijuw', 'IJ', 1),
(2311, 148, 'Meneng', 'ME', 1),
(2312, 148, 'Nibok', 'NI', 1),
(2313, 148, 'Uaboe', 'UA', 1),
(2314, 148, 'Yaren', 'YA', 1),
(2315, 149, 'Bagmati', 'BA', 1),
(2316, 149, 'Bheri', 'BH', 1),
(2317, 149, 'Dhawalagiri', 'DH', 1),
(2318, 149, 'Gandaki', 'GA', 1),
(2319, 149, 'Janakpur', 'JA', 1),
(2320, 149, 'Karnali', 'KA', 1),
(2321, 149, 'Kosi', 'KO', 1),
(2322, 149, 'Lumbini', 'LU', 1),
(2323, 149, 'Mahakali', 'MA', 1),
(2324, 149, 'Mechi', 'ME', 1),
(2325, 149, 'Narayani', 'NA', 1),
(2326, 149, 'Rapti', 'RA', 1),
(2327, 149, 'Sagarmatha', 'SA', 1),
(2328, 149, 'Seti', 'SE', 1),
(2329, 150, 'Drenthe', 'DR', 1),
(2330, 150, 'Flevoland', 'FL', 1),
(2331, 150, 'Friesland', 'FR', 1),
(2332, 150, 'Gelderland', 'GE', 1),
(2333, 150, 'Groningen', 'GR', 1),
(2334, 150, 'Limburg', 'LI', 1),
(2335, 150, 'Noord Brabant', 'NB', 1),
(2336, 150, 'Noord Holland', 'NH', 1),
(2337, 150, 'Overijssel', 'OV', 1),
(2338, 150, 'Utrecht', 'UT', 1),
(2339, 150, 'Zeeland', 'ZE', 1),
(2340, 150, 'Zuid Holland', 'ZH', 1),
(2341, 152, 'Iles Loyaute', 'L', 1),
(2342, 152, 'Nord', 'N', 1),
(2343, 152, 'Sud', 'S', 1),
(2344, 153, 'Auckland', 'AUK', 1),
(2345, 153, 'Bay of Plenty', 'BOP', 1),
(2346, 153, 'Canterbury', 'CAN', 1),
(2347, 153, 'Coromandel', 'COR', 1),
(2348, 153, 'Gisborne', 'GIS', 1),
(2349, 153, 'Fiordland', 'FIO', 1),
(2350, 153, 'Hawke\'s Bay', 'HKB', 1),
(2351, 153, 'Marlborough', 'MBH', 1),
(2352, 153, 'Manawatu-Wanganui', 'MWT', 1),
(2353, 153, 'Mt Cook-Mackenzie', 'MCM', 1),
(2354, 153, 'Nelson', 'NSN', 1),
(2355, 153, 'Northland', 'NTL', 1),
(2356, 153, 'Otago', 'OTA', 1),
(2357, 153, 'Southland', 'STL', 1),
(2358, 153, 'Taranaki', 'TKI', 1),
(2359, 153, 'Wellington', 'WGN', 1),
(2360, 153, 'Waikato', 'WKO', 1),
(2361, 153, 'Wairarapa', 'WAI', 1),
(2362, 153, 'West Coast', 'WTC', 1),
(2363, 154, 'Atlantico Norte', 'AN', 1),
(2364, 154, 'Atlantico Sur', 'AS', 1),
(2365, 154, 'Boaco', 'BO', 1),
(2366, 154, 'Carazo', 'CA', 1),
(2367, 154, 'Chinandega', 'CI', 1),
(2368, 154, 'Chontales', 'CO', 1),
(2369, 154, 'Esteli', 'ES', 1),
(2370, 154, 'Granada', 'GR', 1),
(2371, 154, 'Jinotega', 'JI', 1),
(2372, 154, 'Leon', 'LE', 1),
(2373, 154, 'Madriz', 'MD', 1),
(2374, 154, 'Managua', 'MN', 1),
(2375, 154, 'Masaya', 'MS', 1),
(2376, 154, 'Matagalpa', 'MT', 1),
(2377, 154, 'Nuevo Segovia', 'NS', 1),
(2378, 154, 'Rio San Juan', 'RS', 1),
(2379, 154, 'Rivas', 'RI', 1),
(2380, 155, 'Agadez', 'AG', 1),
(2381, 155, 'Diffa', 'DF', 1),
(2382, 155, 'Dosso', 'DS', 1),
(2383, 155, 'Maradi', 'MA', 1),
(2384, 155, 'Niamey', 'NM', 1),
(2385, 155, 'Tahoua', 'TH', 1),
(2386, 155, 'Tillaberi', 'TL', 1),
(2387, 155, 'Zinder', 'ZD', 1),
(2388, 156, 'Abia', 'AB', 1),
(2389, 156, 'Abuja Federal Capital Territory', 'CT', 1),
(2390, 156, 'Adamawa', 'AD', 1),
(2391, 156, 'Akwa Ibom', 'AK', 1),
(2392, 156, 'Anambra', 'AN', 1),
(2393, 156, 'Bauchi', 'BC', 1),
(2394, 156, 'Bayelsa', 'BY', 1),
(2395, 156, 'Benue', 'BN', 1),
(2396, 156, 'Borno', 'BO', 1),
(2397, 156, 'Cross River', 'CR', 1),
(2398, 156, 'Delta', 'DE', 1),
(2399, 156, 'Ebonyi', 'EB', 1),
(2400, 156, 'Edo', 'ED', 1),
(2401, 156, 'Ekiti', 'EK', 1),
(2402, 156, 'Enugu', 'EN', 1),
(2403, 156, 'Gombe', 'GO', 1),
(2404, 156, 'Imo', 'IM', 1),
(2405, 156, 'Jigawa', 'JI', 1),
(2406, 156, 'Kaduna', 'KD', 1),
(2407, 156, 'Kano', 'KN', 1),
(2408, 156, 'Katsina', 'KT', 1),
(2409, 156, 'Kebbi', 'KE', 1),
(2410, 156, 'Kogi', 'KO', 1),
(2411, 156, 'Kwara', 'KW', 1),
(2412, 156, 'Lagos', 'LA', 1),
(2413, 156, 'Nassarawa', 'NA', 1),
(2414, 156, 'Niger', 'NI', 1),
(2415, 156, 'Ogun', 'OG', 1),
(2416, 156, 'Ondo', 'ONG', 1),
(2417, 156, 'Osun', 'OS', 1),
(2418, 156, 'Oyo', 'OY', 1),
(2419, 156, 'Plateau', 'PL', 1),
(2420, 156, 'Rivers', 'RI', 1),
(2421, 156, 'Sokoto', 'SO', 1),
(2422, 156, 'Taraba', 'TA', 1),
(2423, 156, 'Yobe', 'YO', 1),
(2424, 156, 'Zamfara', 'ZA', 1),
(2425, 159, 'Northern Islands', 'N', 1),
(2426, 159, 'Rota', 'R', 1),
(2427, 159, 'Saipan', 'S', 1),
(2428, 159, 'Tinian', 'T', 1),
(2429, 160, 'Akershus', 'AK', 1),
(2430, 160, 'Aust-Agder', 'AA', 1),
(2431, 160, 'Buskerud', 'BU', 1),
(2432, 160, 'Finnmark', 'FM', 1),
(2433, 160, 'Hedmark', 'HM', 1),
(2434, 160, 'Hordaland', 'HL', 1),
(2435, 160, 'More og Romdal', 'MR', 1),
(2436, 160, 'Nord-Trondelag', 'NT', 1),
(2437, 160, 'Nordland', 'NL', 1),
(2438, 160, 'Ostfold', 'OF', 1),
(2439, 160, 'Oppland', 'OP', 1),
(2440, 160, 'Oslo', 'OL', 1),
(2441, 160, 'Rogaland', 'RL', 1),
(2442, 160, 'Sor-Trondelag', 'ST', 1),
(2443, 160, 'Sogn og Fjordane', 'SJ', 1),
(2444, 160, 'Svalbard', 'SV', 1),
(2445, 160, 'Telemark', 'TM', 1),
(2446, 160, 'Troms', 'TR', 1),
(2447, 160, 'Vest-Agder', 'VA', 1),
(2448, 160, 'Vestfold', 'VF', 1),
(2449, 161, 'Ad Dakhiliyah', 'DA', 1),
(2450, 161, 'Al Batinah', 'BA', 1),
(2451, 161, 'Al Wusta', 'WU', 1),
(2452, 161, 'Ash Sharqiyah', 'SH', 1),
(2453, 161, 'Az Zahirah', 'ZA', 1),
(2454, 161, 'Masqat', 'MA', 1),
(2455, 161, 'Musandam', 'MU', 1),
(2456, 161, 'Zufar', 'ZU', 1),
(2457, 162, 'Balochistan', 'B', 1),
(2458, 162, 'Federally Administered Tribal Areas', 'T', 1),
(2459, 162, 'Islamabad Capital Territory', 'I', 1),
(2460, 162, 'North-West Frontier', 'N', 1),
(2461, 162, 'Punjab', 'P', 1),
(2462, 162, 'Sindh', 'S', 1),
(2463, 163, 'Aimeliik', 'AM', 1),
(2464, 163, 'Airai', 'AR', 1),
(2465, 163, 'Angaur', 'AN', 1),
(2466, 163, 'Hatohobei', 'HA', 1),
(2467, 163, 'Kayangel', 'KA', 1),
(2468, 163, 'Koror', 'KO', 1),
(2469, 163, 'Melekeok', 'ME', 1),
(2470, 163, 'Ngaraard', 'NA', 1),
(2471, 163, 'Ngarchelong', 'NG', 1),
(2472, 163, 'Ngardmau', 'ND', 1),
(2473, 163, 'Ngatpang', 'NT', 1),
(2474, 163, 'Ngchesar', 'NC', 1),
(2475, 163, 'Ngeremlengui', 'NR', 1),
(2476, 163, 'Ngiwal', 'NW', 1),
(2477, 163, 'Peleliu', 'PE', 1),
(2478, 163, 'Sonsorol', 'SO', 1),
(2479, 164, 'Bocas del Toro', 'BT', 1),
(2480, 164, 'Chiriqui', 'CH', 1),
(2481, 164, 'Cocle', 'CC', 1),
(2482, 164, 'Colon', 'CL', 1),
(2483, 164, 'Darien', 'DA', 1),
(2484, 164, 'Herrera', 'HE', 1),
(2485, 164, 'Los Santos', 'LS', 1),
(2486, 164, 'Panama', 'PA', 1),
(2487, 164, 'San Blas', 'SB', 1),
(2488, 164, 'Veraguas', 'VG', 1),
(2489, 165, 'Bougainville', 'BV', 1),
(2490, 165, 'Central', 'CE', 1),
(2491, 165, 'Chimbu', 'CH', 1),
(2492, 165, 'Eastern Highlands', 'EH', 1),
(2493, 165, 'East New Britain', 'EB', 1),
(2494, 165, 'East Sepik', 'ES', 1),
(2495, 165, 'Enga', 'EN', 1),
(2496, 165, 'Gulf', 'GU', 1),
(2497, 165, 'Madang', 'MD', 1),
(2498, 165, 'Manus', 'MN', 1),
(2499, 165, 'Milne Bay', 'MB', 1),
(2500, 165, 'Morobe', 'MR', 1),
(2501, 165, 'National Capital', 'NC', 1),
(2502, 165, 'New Ireland', 'NI', 1),
(2503, 165, 'Northern', 'NO', 1),
(2504, 165, 'Sandaun', 'SA', 1),
(2505, 165, 'Southern Highlands', 'SH', 1),
(2506, 165, 'Western', 'WE', 1),
(2507, 165, 'Western Highlands', 'WH', 1),
(2508, 165, 'West New Britain', 'WB', 1),
(2509, 166, 'Alto Paraguay', 'AG', 1),
(2510, 166, 'Alto Parana', 'AN', 1),
(2511, 166, 'Amambay', 'AM', 1),
(2512, 166, 'Asuncion', 'AS', 1),
(2513, 166, 'Boqueron', 'BO', 1),
(2514, 166, 'Caaguazu', 'CG', 1),
(2515, 166, 'Caazapa', 'CZ', 1),
(2516, 166, 'Canindeyu', 'CN', 1),
(2517, 166, 'Central', 'CE', 1),
(2518, 166, 'Concepcion', 'CC', 1),
(2519, 166, 'Cordillera', 'CD', 1),
(2520, 166, 'Guaira', 'GU', 1),
(2521, 166, 'Itapua', 'IT', 1),
(2522, 166, 'Misiones', 'MI', 1),
(2523, 166, 'Neembucu', 'NE', 1),
(2524, 166, 'Paraguari', 'PA', 1),
(2525, 166, 'Presidente Hayes', 'PH', 1),
(2526, 166, 'San Pedro', 'SP', 1),
(2527, 167, 'Amazonas', 'AM', 1),
(2528, 167, 'Ancash', 'AN', 1),
(2529, 167, 'Apurimac', 'AP', 1),
(2530, 167, 'Arequipa', 'AR', 1),
(2531, 167, 'Ayacucho', 'AY', 1),
(2532, 167, 'Cajamarca', 'CJ', 1),
(2533, 167, 'Callao', 'CL', 1),
(2534, 167, 'Cusco', 'CU', 1),
(2535, 167, 'Huancavelica', 'HV', 1),
(2536, 167, 'Huanuco', 'HO', 1),
(2537, 167, 'Ica', 'IC', 1),
(2538, 167, 'Junin', 'JU', 1),
(2539, 167, 'La Libertad', 'LD', 1),
(2540, 167, 'Lambayeque', 'LY', 1),
(2541, 167, 'Lima', 'LI', 1),
(2542, 167, 'Loreto', 'LO', 1),
(2543, 167, 'Madre de Dios', 'MD', 1),
(2544, 167, 'Moquegua', 'MO', 1),
(2545, 167, 'Pasco', 'PA', 1),
(2546, 167, 'Piura', 'PI', 1),
(2547, 167, 'Puno', 'PU', 1),
(2548, 167, 'San Martin', 'SM', 1),
(2549, 167, 'Tacna', 'TA', 1),
(2550, 167, 'Tumbes', 'TU', 1),
(2551, 167, 'Ucayali', 'UC', 1),
(2552, 168, 'Abra', 'ABR', 1),
(2553, 168, 'Agusan del Norte', 'ANO', 1),
(2554, 168, 'Agusan del Sur', 'ASU', 1),
(2555, 168, 'Aklan', 'AKL', 1),
(2556, 168, 'Albay', 'ALB', 1),
(2557, 168, 'Antique', 'ANT', 1),
(2558, 168, 'Apayao', 'APY', 1),
(2559, 168, 'Aurora', 'AUR', 1),
(2560, 168, 'Basilan', 'BAS', 1),
(2561, 168, 'Bataan', 'BTA', 1),
(2562, 168, 'Batanes', 'BTE', 1),
(2563, 168, 'Batangas', 'BTG', 1),
(2564, 168, 'Biliran', 'BLR', 1),
(2565, 168, 'Benguet', 'BEN', 1),
(2566, 168, 'Bohol', 'BOL', 1),
(2567, 168, 'Bukidnon', 'BUK', 1),
(2568, 168, 'Bulacan', 'BUL', 1),
(2569, 168, 'Cagayan', 'CAG', 1),
(2570, 168, 'Camarines Norte', 'CNO', 1),
(2571, 168, 'Camarines Sur', 'CSU', 1),
(2572, 168, 'Camiguin', 'CAM', 1),
(2573, 168, 'Capiz', 'CAP', 1),
(2574, 168, 'Catanduanes', 'CAT', 1),
(2575, 168, 'Cavite', 'CAV', 1),
(2576, 168, 'Cebu', 'CEB', 1),
(2577, 168, 'Compostela', 'CMP', 1),
(2578, 168, 'Davao del Norte', 'DNO', 1),
(2579, 168, 'Davao del Sur', 'DSU', 1),
(2580, 168, 'Davao Oriental', 'DOR', 1),
(2581, 168, 'Eastern Samar', 'ESA', 1),
(2582, 168, 'Guimaras', 'GUI', 1),
(2583, 168, 'Ifugao', 'IFU', 1),
(2584, 168, 'Ilocos Norte', 'INO', 1),
(2585, 168, 'Ilocos Sur', 'ISU', 1),
(2586, 168, 'Iloilo', 'ILO', 1),
(2587, 168, 'Isabela', 'ISA', 1),
(2588, 168, 'Kalinga', 'KAL', 1),
(2589, 168, 'Laguna', 'LAG', 1),
(2590, 168, 'Lanao del Norte', 'LNO', 1),
(2591, 168, 'Lanao del Sur', 'LSU', 1),
(2592, 168, 'La Union', 'UNI', 1),
(2593, 168, 'Leyte', 'LEY', 1),
(2594, 168, 'Maguindanao', 'MAG', 1),
(2595, 168, 'Marinduque', 'MRN', 1),
(2596, 168, 'Masbate', 'MSB', 1),
(2597, 168, 'Mindoro Occidental', 'MIC', 1),
(2598, 168, 'Mindoro Oriental', 'MIR', 1),
(2599, 168, 'Misamis Occidental', 'MSC', 1),
(2600, 168, 'Misamis Oriental', 'MOR', 1),
(2601, 168, 'Mountain', 'MOP', 1),
(2602, 168, 'Negros Occidental', 'NOC', 1),
(2603, 168, 'Negros Oriental', 'NOR', 1),
(2604, 168, 'North Cotabato', 'NCT', 1),
(2605, 168, 'Northern Samar', 'NSM', 1),
(2606, 168, 'Nueva Ecija', 'NEC', 1),
(2607, 168, 'Nueva Vizcaya', 'NVZ', 1),
(2608, 168, 'Palawan', 'PLW', 1),
(2609, 168, 'Pampanga', 'PMP', 1),
(2610, 168, 'Pangasinan', 'PNG', 1),
(2611, 168, 'Quezon', 'QZN', 1),
(2612, 168, 'Quirino', 'QRN', 1),
(2613, 168, 'Rizal', 'RIZ', 1),
(2614, 168, 'Romblon', 'ROM', 1),
(2615, 168, 'Samar', 'SMR', 1),
(2616, 168, 'Sarangani', 'SRG', 1),
(2617, 168, 'Siquijor', 'SQJ', 1),
(2618, 168, 'Sorsogon', 'SRS', 1),
(2619, 168, 'South Cotabato', 'SCO', 1),
(2620, 168, 'Southern Leyte', 'SLE', 1),
(2621, 168, 'Sultan Kudarat', 'SKU', 1),
(2622, 168, 'Sulu', 'SLU', 1),
(2623, 168, 'Surigao del Norte', 'SNO', 1),
(2624, 168, 'Surigao del Sur', 'SSU', 1),
(2625, 168, 'Tarlac', 'TAR', 1),
(2626, 168, 'Tawi-Tawi', 'TAW', 1),
(2627, 168, 'Zambales', 'ZBL', 1),
(2628, 168, 'Zamboanga del Norte', 'ZNO', 1),
(2629, 168, 'Zamboanga del Sur', 'ZSU', 1),
(2630, 168, 'Zamboanga Sibugay', 'ZSI', 1),
(2631, 170, 'Dolnoslaskie', 'DO', 1),
(2632, 170, 'Kujawsko-Pomorskie', 'KP', 1),
(2633, 170, 'Lodzkie', 'LO', 1),
(2634, 170, 'Lubelskie', 'LL', 1),
(2635, 170, 'Lubuskie', 'LU', 1),
(2636, 170, 'Malopolskie', 'ML', 1),
(2637, 170, 'Mazowieckie', 'MZ', 1),
(2638, 170, 'Opolskie', 'OP', 1),
(2639, 170, 'Podkarpackie', 'PP', 1),
(2640, 170, 'Podlaskie', 'PL', 1),
(2641, 170, 'Pomorskie', 'PM', 1),
(2642, 170, 'Slaskie', 'SL', 1),
(2643, 170, 'Swietokrzyskie', 'SW', 1),
(2644, 170, 'Warminsko-Mazurskie', 'WM', 1),
(2645, 170, 'Wielkopolskie', 'WP', 1),
(2646, 170, 'Zachodniopomorskie', 'ZA', 1),
(2647, 198, 'Saint Pierre', 'P', 1),
(2648, 198, 'Miquelon', 'M', 1),
(2649, 171, 'A&ccedil;ores', 'AC', 1),
(2650, 171, 'Aveiro', 'AV', 1),
(2651, 171, 'Beja', 'BE', 1),
(2652, 171, 'Braga', 'BR', 1),
(2653, 171, 'Bragan&ccedil;a', 'BA', 1),
(2654, 171, 'Castelo Branco', 'CB', 1),
(2655, 171, 'Coimbra', 'CO', 1),
(2656, 171, '&Eacute;vora', 'EV', 1),
(2657, 171, 'Faro', 'FA', 1),
(2658, 171, 'Guarda', 'GU', 1),
(2659, 171, 'Leiria', 'LE', 1),
(2660, 171, 'Lisboa', 'LI', 1),
(2661, 171, 'Madeira', 'ME', 1),
(2662, 171, 'Portalegre', 'PO', 1),
(2663, 171, 'Porto', 'PR', 1),
(2664, 171, 'Santar&eacute;m', 'SA', 1),
(2665, 171, 'Set&uacute;bal', 'SE', 1),
(2666, 171, 'Viana do Castelo', 'VC', 1),
(2667, 171, 'Vila Real', 'VR', 1),
(2668, 171, 'Viseu', 'VI', 1),
(2669, 173, 'Ad Dawhah', 'DW', 1),
(2670, 173, 'Al Ghuwayriyah', 'GW', 1),
(2671, 173, 'Al Jumayliyah', 'JM', 1),
(2672, 173, 'Al Khawr', 'KR', 1),
(2673, 173, 'Al Wakrah', 'WK', 1),
(2674, 173, 'Ar Rayyan', 'RN', 1),
(2675, 173, 'Jarayan al Batinah', 'JB', 1),
(2676, 173, 'Madinat ash Shamal', 'MS', 1),
(2677, 173, 'Umm Sa\'id', 'UD', 1),
(2678, 173, 'Umm Salal', 'UL', 1),
(2679, 175, 'Alba', 'AB', 1),
(2680, 175, 'Arad', 'AR', 1),
(2681, 175, 'Arges', 'AG', 1),
(2682, 175, 'Bacau', 'BC', 1),
(2683, 175, 'Bihor', 'BH', 1),
(2684, 175, 'Bistrita-Nasaud', 'BN', 1),
(2685, 175, 'Botosani', 'BT', 1),
(2686, 175, 'Brasov', 'BV', 1),
(2687, 175, 'Braila', 'BR', 1),
(2688, 175, 'Bucuresti', 'B', 1),
(2689, 175, 'Buzau', 'BZ', 1),
(2690, 175, 'Caras-Severin', 'CS', 1),
(2691, 175, 'Calarasi', 'CL', 1),
(2692, 175, 'Cluj', 'CJ', 1),
(2693, 175, 'Constanta', 'CT', 1),
(2694, 175, 'Covasna', 'CV', 1),
(2695, 175, 'Dimbovita', 'DB', 1),
(2696, 175, 'Dolj', 'DJ', 1),
(2697, 175, 'Galati', 'GL', 1),
(2698, 175, 'Giurgiu', 'GR', 1),
(2699, 175, 'Gorj', 'GJ', 1),
(2700, 175, 'Harghita', 'HR', 1),
(2701, 175, 'Hunedoara', 'HD', 1),
(2702, 175, 'Ialomita', 'IL', 1),
(2703, 175, 'Iasi', 'IS', 1),
(2704, 175, 'Ilfov', 'IF', 1),
(2705, 175, 'Maramures', 'MM', 1),
(2706, 175, 'Mehedinti', 'MH', 1),
(2707, 175, 'Mures', 'MS', 1),
(2708, 175, 'Neamt', 'NT', 1),
(2709, 175, 'Olt', 'OT', 1),
(2710, 175, 'Prahova', 'PH', 1),
(2711, 175, 'Satu-Mare', 'SM', 1),
(2712, 175, 'Salaj', 'SJ', 1),
(2713, 175, 'Sibiu', 'SB', 1),
(2714, 175, 'Suceava', 'SV', 1),
(2715, 175, 'Teleorman', 'TR', 1),
(2716, 175, 'Timis', 'TM', 1),
(2717, 175, 'Tulcea', 'TL', 1),
(2718, 175, 'Vaslui', 'VS', 1),
(2719, 175, 'Valcea', 'VL', 1),
(2720, 175, 'Vrancea', 'VN', 1),
(2721, 176, 'Abakan', 'AB', 1),
(2722, 176, 'Aginskoye', 'AG', 1),
(2723, 176, 'Anadyr', 'AN', 1),
(2724, 176, 'Arkahangelsk', 'AR', 1),
(2725, 176, 'Astrakhan', 'AS', 1),
(2726, 176, 'Barnaul', 'BA', 1),
(2727, 176, 'Belgorod', 'BE', 1),
(2728, 176, 'Birobidzhan', 'BI', 1),
(2729, 176, 'Blagoveshchensk', 'BL', 1),
(2730, 176, 'Bryansk', 'BR', 1),
(2731, 176, 'Cheboksary', 'CH', 1),
(2732, 176, 'Chelyabinsk', 'CL', 1),
(2733, 176, 'Cherkessk', 'CR', 1),
(2734, 176, 'Chita', 'CI', 1),
(2735, 176, 'Dudinka', 'DU', 1),
(2736, 176, 'Elista', 'EL', 1),
(2737, 176, 'Gomo-Altaysk', 'GO', 1),
(2738, 176, 'Gorno-Altaysk', 'GA', 1),
(2739, 176, 'Groznyy', 'GR', 1),
(2740, 176, 'Irkutsk', 'IR', 1),
(2741, 176, 'Ivanovo', 'IV', 1),
(2742, 176, 'Izhevsk', 'IZ', 1),
(2743, 176, 'Kalinigrad', 'KA', 1),
(2744, 176, 'Kaluga', 'KL', 1),
(2745, 176, 'Kasnodar', 'KS', 1),
(2746, 176, 'Kazan', 'KZ', 1),
(2747, 176, 'Kemerovo', 'KE', 1),
(2748, 176, 'Khabarovsk', 'KH', 1),
(2749, 176, 'Khanty-Mansiysk', 'KM', 1),
(2750, 176, 'Kostroma', 'KO', 1),
(2751, 176, 'Krasnodar', 'KR', 1),
(2752, 176, 'Krasnoyarsk', 'KN', 1),
(2753, 176, 'Kudymkar', 'KU', 1),
(2754, 176, 'Kurgan', 'KG', 1),
(2755, 176, 'Kursk', 'KK', 1),
(2756, 176, 'Kyzyl', 'KY', 1),
(2757, 176, 'Lipetsk', 'LI', 1),
(2758, 176, 'Magadan', 'MA', 1),
(2759, 176, 'Makhachkala', 'MK', 1),
(2760, 176, 'Maykop', 'MY', 1),
(2761, 176, 'Moscow', 'MO', 1),
(2762, 176, 'Murmansk', 'MU', 1),
(2763, 176, 'Nalchik', 'NA', 1),
(2764, 176, 'Naryan Mar', 'NR', 1),
(2765, 176, 'Nazran', 'NZ', 1),
(2766, 176, 'Nizhniy Novgorod', 'NI', 1),
(2767, 176, 'Novgorod', 'NO', 1),
(2768, 176, 'Novosibirsk', 'NV', 1),
(2769, 176, 'Omsk', 'OM', 1),
(2770, 176, 'Orel', 'OR', 1),
(2771, 176, 'Orenburg', 'OE', 1),
(2772, 176, 'Palana', 'PA', 1),
(2773, 176, 'Penza', 'PE', 1),
(2774, 176, 'Perm', 'PR', 1),
(2775, 176, 'Petropavlovsk-Kamchatskiy', 'PK', 1),
(2776, 176, 'Petrozavodsk', 'PT', 1),
(2777, 176, 'Pskov', 'PS', 1),
(2778, 176, 'Rostov-na-Donu', 'RO', 1),
(2779, 176, 'Ryazan', 'RY', 1),
(2780, 176, 'Salekhard', 'SL', 1),
(2781, 176, 'Samara', 'SA', 1),
(2782, 176, 'Saransk', 'SR', 1),
(2783, 176, 'Saratov', 'SV', 1),
(2784, 176, 'Smolensk', 'SM', 1),
(2785, 176, 'St. Petersburg', 'SP', 1),
(2786, 176, 'Stavropol', 'ST', 1),
(2787, 176, 'Syktyvkar', 'SY', 1),
(2788, 176, 'Tambov', 'TA', 1),
(2789, 176, 'Tomsk', 'TO', 1),
(2790, 176, 'Tula', 'TU', 1),
(2791, 176, 'Tura', 'TR', 1),
(2792, 176, 'Tver', 'TV', 1),
(2793, 176, 'Tyumen', 'TY', 1),
(2794, 176, 'Ufa', 'UF', 1),
(2795, 176, 'Ul\'yanovsk', 'UL', 1),
(2796, 176, 'Ulan-Ude', 'UU', 1),
(2797, 176, 'Ust\'-Ordynskiy', 'US', 1),
(2798, 176, 'Vladikavkaz', 'VL', 1),
(2799, 176, 'Vladimir', 'VA', 1),
(2800, 176, 'Vladivostok', 'VV', 1),
(2801, 176, 'Volgograd', 'VG', 1),
(2802, 176, 'Vologda', 'VD', 1),
(2803, 176, 'Voronezh', 'VO', 1),
(2804, 176, 'Vyatka', 'VY', 1),
(2805, 176, 'Yakutsk', 'YA', 1),
(2806, 176, 'Yaroslavl', 'YR', 1),
(2807, 176, 'Yekaterinburg', 'YE', 1),
(2808, 176, 'Yoshkar-Ola', 'YO', 1),
(2809, 177, 'Butare', 'BU', 1),
(2810, 177, 'Byumba', 'BY', 1),
(2811, 177, 'Cyangugu', 'CY', 1),
(2812, 177, 'Gikongoro', 'GK', 1),
(2813, 177, 'Gisenyi', 'GS', 1),
(2814, 177, 'Gitarama', 'GT', 1),
(2815, 177, 'Kibungo', 'KG', 1),
(2816, 177, 'Kibuye', 'KY', 1),
(2817, 177, 'Kigali Rurale', 'KR', 1),
(2818, 177, 'Kigali-ville', 'KV', 1),
(2819, 177, 'Ruhengeri', 'RU', 1),
(2820, 177, 'Umutara', 'UM', 1),
(2821, 178, 'Christ Church Nichola Town', 'CCN', 1),
(2822, 178, 'Saint Anne Sandy Point', 'SAS', 1),
(2823, 178, 'Saint George Basseterre', 'SGB', 1),
(2824, 178, 'Saint George Gingerland', 'SGG', 1),
(2825, 178, 'Saint James Windward', 'SJW', 1),
(2826, 178, 'Saint John Capesterre', 'SJC', 1),
(2827, 178, 'Saint John Figtree', 'SJF', 1),
(2828, 178, 'Saint Mary Cayon', 'SMC', 1),
(2829, 178, 'Saint Paul Capesterre', 'CAP', 1),
(2830, 178, 'Saint Paul Charlestown', 'CHA', 1),
(2831, 178, 'Saint Peter Basseterre', 'SPB', 1),
(2832, 178, 'Saint Thomas Lowland', 'STL', 1),
(2833, 178, 'Saint Thomas Middle Island', 'STM', 1),
(2834, 178, 'Trinity Palmetto Point', 'TPP', 1),
(2835, 179, 'Anse-la-Raye', 'AR', 1),
(2836, 179, 'Castries', 'CA', 1),
(2837, 179, 'Choiseul', 'CH', 1),
(2838, 179, 'Dauphin', 'DA', 1),
(2839, 179, 'Dennery', 'DE', 1),
(2840, 179, 'Gros-Islet', 'GI', 1),
(2841, 179, 'Laborie', 'LA', 1),
(2842, 179, 'Micoud', 'MI', 1),
(2843, 179, 'Praslin', 'PR', 1),
(2844, 179, 'Soufriere', 'SO', 1),
(2845, 179, 'Vieux-Fort', 'VF', 1),
(2846, 180, 'Charlotte', 'C', 1),
(2847, 180, 'Grenadines', 'R', 1),
(2848, 180, 'Saint Andrew', 'A', 1),
(2849, 180, 'Saint David', 'D', 1),
(2850, 180, 'Saint George', 'G', 1),
(2851, 180, 'Saint Patrick', 'P', 1),
(2852, 181, 'A\'ana', 'AN', 1),
(2853, 181, 'Aiga-i-le-Tai', 'AI', 1),
(2854, 181, 'Atua', 'AT', 1),
(2855, 181, 'Fa\'asaleleaga', 'FA', 1),
(2856, 181, 'Gaga\'emauga', 'GE', 1),
(2857, 181, 'Gagaifomauga', 'GF', 1),
(2858, 181, 'Palauli', 'PA', 1),
(2859, 181, 'Satupa\'itea', 'SA', 1),
(2860, 181, 'Tuamasaga', 'TU', 1),
(2861, 181, 'Va\'a-o-Fonoti', 'VF', 1),
(2862, 181, 'Vaisigano', 'VS', 1),
(2863, 182, 'Acquaviva', 'AC', 1),
(2864, 182, 'Borgo Maggiore', 'BM', 1),
(2865, 182, 'Chiesanuova', 'CH', 1),
(2866, 182, 'Domagnano', 'DO', 1),
(2867, 182, 'Faetano', 'FA', 1),
(2868, 182, 'Fiorentino', 'FI', 1),
(2869, 182, 'Montegiardino', 'MO', 1),
(2870, 182, 'Citta di San Marino', 'SM', 1),
(2871, 182, 'Serravalle', 'SE', 1),
(2872, 183, 'Sao Tome', 'S', 1),
(2873, 183, 'Principe', 'P', 1),
(2874, 184, 'Al Bahah', 'BH', 1),
(2875, 184, 'Al Hudud ash Shamaliyah', 'HS', 1),
(2876, 184, 'Al Jawf', 'JF', 1),
(2877, 184, 'Al Madinah', 'MD', 1),
(2878, 184, 'Al Qasim', 'QS', 1),
(2879, 184, 'Ar Riyad', 'RD', 1),
(2880, 184, 'Ash Sharqiyah (Eastern)', 'AQ', 1),
(2881, 184, '\'Asir', 'AS', 1),
(2882, 184, 'Ha\'il', 'HL', 1),
(2883, 184, 'Jizan', 'JZ', 1),
(2884, 184, 'Makkah', 'ML', 1),
(2885, 184, 'Najran', 'NR', 1),
(2886, 184, 'Tabuk', 'TB', 1),
(2887, 185, 'Dakar', 'DA', 1),
(2888, 185, 'Diourbel', 'DI', 1),
(2889, 185, 'Fatick', 'FA', 1),
(2890, 185, 'Kaolack', 'KA', 1),
(2891, 185, 'Kolda', 'KO', 1),
(2892, 185, 'Louga', 'LO', 1),
(2893, 185, 'Matam', 'MA', 1),
(2894, 185, 'Saint-Louis', 'SL', 1),
(2895, 185, 'Tambacounda', 'TA', 1),
(2896, 185, 'Thies', 'TH', 1),
(2897, 185, 'Ziguinchor', 'ZI', 1),
(2898, 186, 'Anse aux Pins', 'AP', 1),
(2899, 186, 'Anse Boileau', 'AB', 1),
(2900, 186, 'Anse Etoile', 'AE', 1),
(2901, 186, 'Anse Louis', 'AL', 1),
(2902, 186, 'Anse Royale', 'AR', 1),
(2903, 186, 'Baie Lazare', 'BL', 1),
(2904, 186, 'Baie Sainte Anne', 'BS', 1),
(2905, 186, 'Beau Vallon', 'BV', 1),
(2906, 186, 'Bel Air', 'BA', 1),
(2907, 186, 'Bel Ombre', 'BO', 1),
(2908, 186, 'Cascade', 'CA', 1),
(2909, 186, 'Glacis', 'GL', 1),
(2910, 186, 'Grand\' Anse (on Mahe)', 'GM', 1),
(2911, 186, 'Grand\' Anse (on Praslin)', 'GP', 1),
(2912, 186, 'La Digue', 'DG', 1),
(2913, 186, 'La Riviere Anglaise', 'RA', 1),
(2914, 186, 'Mont Buxton', 'MB', 1),
(2915, 186, 'Mont Fleuri', 'MF', 1),
(2916, 186, 'Plaisance', 'PL', 1),
(2917, 186, 'Pointe La Rue', 'PR', 1),
(2918, 186, 'Port Glaud', 'PG', 1),
(2919, 186, 'Saint Louis', 'SL', 1),
(2920, 186, 'Takamaka', 'TA', 1),
(2921, 187, 'Eastern', 'E', 1),
(2922, 187, 'Northern', 'N', 1),
(2923, 187, 'Southern', 'S', 1),
(2924, 187, 'Western', 'W', 1),
(2925, 189, 'Banskobystrický', 'BA', 1),
(2926, 189, 'Bratislavský', 'BR', 1),
(2927, 189, 'Košický', 'KO', 1),
(2928, 189, 'Nitriansky', 'NI', 1),
(2929, 189, 'Prešovský', 'PR', 1),
(2930, 189, 'Trenčiansky', 'TC', 1),
(2931, 189, 'Trnavský', 'TV', 1),
(2932, 189, 'Žilinský', 'ZI', 1),
(2933, 191, 'Central', 'CE', 1),
(2934, 191, 'Choiseul', 'CH', 1),
(2935, 191, 'Guadalcanal', 'GC', 1),
(2936, 191, 'Honiara', 'HO', 1),
(2937, 191, 'Isabel', 'IS', 1),
(2938, 191, 'Makira', 'MK', 1),
(2939, 191, 'Malaita', 'ML', 1),
(2940, 191, 'Rennell and Bellona', 'RB', 1),
(2941, 191, 'Temotu', 'TM', 1),
(2942, 191, 'Western', 'WE', 1),
(2943, 192, 'Awdal', 'AW', 1),
(2944, 192, 'Bakool', 'BK', 1),
(2945, 192, 'Banaadir', 'BN', 1),
(2946, 192, 'Bari', 'BR', 1),
(2947, 192, 'Bay', 'BY', 1),
(2948, 192, 'Galguduud', 'GA', 1),
(2949, 192, 'Gedo', 'GE', 1),
(2950, 192, 'Hiiraan', 'HI', 1),
(2951, 192, 'Jubbada Dhexe', 'JD', 1),
(2952, 192, 'Jubbada Hoose', 'JH', 1),
(2953, 192, 'Mudug', 'MU', 1),
(2954, 192, 'Nugaal', 'NU', 1),
(2955, 192, 'Sanaag', 'SA', 1),
(2956, 192, 'Shabeellaha Dhexe', 'SD', 1),
(2957, 192, 'Shabeellaha Hoose', 'SH', 1),
(2958, 192, 'Sool', 'SL', 1),
(2959, 192, 'Togdheer', 'TO', 1),
(2960, 192, 'Woqooyi Galbeed', 'WG', 1),
(2961, 193, 'Eastern Cape', 'EC', 1),
(2962, 193, 'Free State', 'FS', 1),
(2963, 193, 'Gauteng', 'GT', 1),
(2964, 193, 'KwaZulu-Natal', 'KN', 1),
(2965, 193, 'Limpopo', 'LP', 1),
(2966, 193, 'Mpumalanga', 'MP', 1),
(2967, 193, 'North West', 'NW', 1),
(2968, 193, 'Northern Cape', 'NC', 1),
(2969, 193, 'Western Cape', 'WC', 1),
(2970, 195, 'La Coru&ntilde;a', 'CA', 1),
(2971, 195, '&Aacute;lava', 'AL', 1),
(2972, 195, 'Albacete', 'AB', 1),
(2973, 195, 'Alicante', 'AC', 1),
(2974, 195, 'Almeria', 'AM', 1),
(2975, 195, 'Asturias', 'AS', 1),
(2976, 195, '&Aacute;vila', 'AV', 1),
(2977, 195, 'Badajoz', 'BJ', 1),
(2978, 195, 'Baleares', 'IB', 1),
(2979, 195, 'Barcelona', 'BA', 1),
(2980, 195, 'Burgos', 'BU', 1),
(2981, 195, 'C&aacute;ceres', 'CC', 1),
(2982, 195, 'C&aacute;diz', 'CZ', 1),
(2983, 195, 'Cantabria', 'CT', 1),
(2984, 195, 'Castell&oacute;n', 'CL', 1),
(2985, 195, 'Ceuta', 'CE', 1),
(2986, 195, 'Ciudad Real', 'CR', 1),
(2987, 195, 'C&oacute;rdoba', 'CD', 1),
(2988, 195, 'Cuenca', 'CU', 1),
(2989, 195, 'Girona', 'GI', 1),
(2990, 195, 'Granada', 'GD', 1),
(2991, 195, 'Guadalajara', 'GJ', 1),
(2992, 195, 'Guip&uacute;zcoa', 'GP', 1),
(2993, 195, 'Huelva', 'HL', 1),
(2994, 195, 'Huesca', 'HS', 1),
(2995, 195, 'Ja&eacute;n', 'JN', 1),
(2996, 195, 'La Rioja', 'RJ', 1),
(2997, 195, 'Las Palmas', 'PM', 1),
(2998, 195, 'Leon', 'LE', 1),
(2999, 195, 'Lleida', 'LL', 1),
(3000, 195, 'Lugo', 'LG', 1),
(3001, 195, 'Madrid', 'MD', 1),
(3002, 195, 'Malaga', 'MA', 1),
(3003, 195, 'Melilla', 'ML', 1),
(3004, 195, 'Murcia', 'MU', 1),
(3005, 195, 'Navarra', 'NV', 1),
(3006, 195, 'Ourense', 'OU', 1),
(3007, 195, 'Palencia', 'PL', 1),
(3008, 195, 'Pontevedra', 'PO', 1),
(3009, 195, 'Salamanca', 'SL', 1),
(3010, 195, 'Santa Cruz de Tenerife', 'SC', 1),
(3011, 195, 'Segovia', 'SG', 1),
(3012, 195, 'Sevilla', 'SV', 1),
(3013, 195, 'Soria', 'SO', 1),
(3014, 195, 'Tarragona', 'TA', 1),
(3015, 195, 'Teruel', 'TE', 1),
(3016, 195, 'Toledo', 'TO', 1),
(3017, 195, 'Valencia', 'VC', 1),
(3018, 195, 'Valladolid', 'VD', 1),
(3019, 195, 'Vizcaya', 'VZ', 1),
(3020, 195, 'Zamora', 'ZM', 1),
(3021, 195, 'Zaragoza', 'ZR', 1),
(3022, 196, 'Central', 'CE', 1),
(3023, 196, 'Eastern', 'EA', 1),
(3024, 196, 'North Central', 'NC', 1),
(3025, 196, 'Northern', 'NO', 1),
(3026, 196, 'North Western', 'NW', 1),
(3027, 196, 'Sabaragamuwa', 'SA', 1),
(3028, 196, 'Southern', 'SO', 1),
(3029, 196, 'Uva', 'UV', 1),
(3030, 196, 'Western', 'WE', 1),
(3032, 197, 'Saint Helena', 'S', 1),
(3034, 199, 'A\'ali an Nil', 'ANL', 1),
(3035, 199, 'Al Bahr al Ahmar', 'BAM', 1),
(3036, 199, 'Al Buhayrat', 'BRT', 1),
(3037, 199, 'Al Jazirah', 'JZR', 1),
(3038, 199, 'Al Khartum', 'KRT', 1),
(3039, 199, 'Al Qadarif', 'QDR', 1),
(3040, 199, 'Al Wahdah', 'WDH', 1),
(3041, 199, 'An Nil al Abyad', 'ANB', 1),
(3042, 199, 'An Nil al Azraq', 'ANZ', 1),
(3043, 199, 'Ash Shamaliyah', 'ASH', 1),
(3044, 199, 'Bahr al Jabal', 'BJA', 1),
(3045, 199, 'Gharb al Istiwa\'iyah', 'GIS', 1),
(3046, 199, 'Gharb Bahr al Ghazal', 'GBG', 1),
(3047, 199, 'Gharb Darfur', 'GDA', 1),
(3048, 199, 'Gharb Kurdufan', 'GKU', 1),
(3049, 199, 'Janub Darfur', 'JDA', 1),
(3050, 199, 'Janub Kurdufan', 'JKU', 1),
(3051, 199, 'Junqali', 'JQL', 1),
(3052, 199, 'Kassala', 'KSL', 1),
(3053, 199, 'Nahr an Nil', 'NNL', 1),
(3054, 199, 'Shamal Bahr al Ghazal', 'SBG', 1),
(3055, 199, 'Shamal Darfur', 'SDA', 1),
(3056, 199, 'Shamal Kurdufan', 'SKU', 1),
(3057, 199, 'Sharq al Istiwa\'iyah', 'SIS', 1),
(3058, 199, 'Sinnar', 'SNR', 1),
(3059, 199, 'Warab', 'WRB', 1),
(3060, 200, 'Brokopondo', 'BR', 1),
(3061, 200, 'Commewijne', 'CM', 1),
(3062, 200, 'Coronie', 'CR', 1),
(3063, 200, 'Marowijne', 'MA', 1),
(3064, 200, 'Nickerie', 'NI', 1),
(3065, 200, 'Para', 'PA', 1),
(3066, 200, 'Paramaribo', 'PM', 1),
(3067, 200, 'Saramacca', 'SA', 1),
(3068, 200, 'Sipaliwini', 'SI', 1),
(3069, 200, 'Wanica', 'WA', 1),
(3070, 202, 'Hhohho', 'H', 1),
(3071, 202, 'Lubombo', 'L', 1),
(3072, 202, 'Manzini', 'M', 1),
(3073, 202, 'Shishelweni', 'S', 1),
(3074, 203, 'Blekinge', 'K', 1),
(3075, 203, 'Dalarna', 'W', 1),
(3076, 203, 'G&auml;vleborg', 'X', 1),
(3077, 203, 'Gotland', 'I', 1),
(3078, 203, 'Halland', 'N', 1),
(3079, 203, 'J&auml;mtland', 'Z', 1),
(3080, 203, 'J&ouml;nk&ouml;ping', 'F', 1),
(3081, 203, 'Kalmar', 'H', 1),
(3082, 203, 'Kronoberg', 'G', 1),
(3083, 203, 'Norrbotten', 'BD', 1),
(3084, 203, '&Ouml;rebro', 'T', 1),
(3085, 203, '&Ouml;sterg&ouml;tland', 'E', 1),
(3086, 203, 'Sk&aring;ne', 'M', 1),
(3087, 203, 'S&ouml;dermanland', 'D', 1),
(3088, 203, 'Stockholm', 'AB', 1),
(3089, 203, 'Uppsala', 'C', 1),
(3090, 203, 'V&auml;rmland', 'S', 1),
(3091, 203, 'V&auml;sterbotten', 'AC', 1),
(3092, 203, 'V&auml;sternorrland', 'Y', 1),
(3093, 203, 'V&auml;stmanland', 'U', 1),
(3094, 203, 'V&auml;stra G&ouml;taland', 'O', 1),
(3095, 204, 'Aargau', 'AG', 1),
(3096, 204, 'Appenzell Ausserrhoden', 'AR', 1),
(3097, 204, 'Appenzell Innerrhoden', 'AI', 1),
(3098, 204, 'Basel-Stadt', 'BS', 1),
(3099, 204, 'Basel-Landschaft', 'BL', 1),
(3100, 204, 'Bern', 'BE', 1),
(3101, 204, 'Fribourg', 'FR', 1),
(3102, 204, 'Gen&egrave;ve', 'GE', 1),
(3103, 204, 'Glarus', 'GL', 1),
(3104, 204, 'Graub&uuml;nden', 'GR', 1),
(3105, 204, 'Jura', 'JU', 1),
(3106, 204, 'Luzern', 'LU', 1),
(3107, 204, 'Neuch&acirc;tel', 'NE', 1),
(3108, 204, 'Nidwald', 'NW', 1),
(3109, 204, 'Obwald', 'OW', 1),
(3110, 204, 'St. Gallen', 'SG', 1),
(3111, 204, 'Schaffhausen', 'SH', 1),
(3112, 204, 'Schwyz', 'SZ', 1),
(3113, 204, 'Solothurn', 'SO', 1),
(3114, 204, 'Thurgau', 'TG', 1),
(3115, 204, 'Ticino', 'TI', 1),
(3116, 204, 'Uri', 'UR', 1),
(3117, 204, 'Valais', 'VS', 1),
(3118, 204, 'Vaud', 'VD', 1),
(3119, 204, 'Zug', 'ZG', 1),
(3120, 204, 'Z&uuml;rich', 'ZH', 1),
(3121, 205, 'Al Hasakah', 'HA', 1),
(3122, 205, 'Al Ladhiqiyah', 'LA', 1),
(3123, 205, 'Al Qunaytirah', 'QU', 1),
(3124, 205, 'Ar Raqqah', 'RQ', 1),
(3125, 205, 'As Suwayda', 'SU', 1),
(3126, 205, 'Dara', 'DA', 1),
(3127, 205, 'Dayr az Zawr', 'DZ', 1),
(3128, 205, 'Dimashq', 'DI', 1),
(3129, 205, 'Halab', 'HL', 1),
(3130, 205, 'Hamah', 'HM', 1),
(3131, 205, 'Hims', 'HI', 1),
(3132, 205, 'Idlib', 'ID', 1);
INSERT INTO `oc_zone` (`zone_id`, `country_id`, `name`, `code`, `status`) VALUES
(3133, 205, 'Rif Dimashq', 'RD', 1),
(3134, 205, 'Tartus', 'TA', 1),
(3135, 206, 'Chang-hua', 'CH', 1),
(3136, 206, 'Chia-i', 'CI', 1),
(3137, 206, 'Hsin-chu', 'HS', 1),
(3138, 206, 'Hua-lien', 'HL', 1),
(3139, 206, 'I-lan', 'IL', 1),
(3140, 206, 'Kao-hsiung county', 'KH', 1),
(3141, 206, 'Kin-men', 'KM', 1),
(3142, 206, 'Lien-chiang', 'LC', 1),
(3143, 206, 'Miao-li', 'ML', 1),
(3144, 206, 'Nan-t\'ou', 'NT', 1),
(3145, 206, 'P\'eng-hu', 'PH', 1),
(3146, 206, 'P\'ing-tung', 'PT', 1),
(3147, 206, 'T\'ai-chung', 'TG', 1),
(3148, 206, 'T\'ai-nan', 'TA', 1),
(3149, 206, 'T\'ai-pei county', 'TP', 1),
(3150, 206, 'T\'ai-tung', 'TT', 1),
(3151, 206, 'T\'ao-yuan', 'TY', 1),
(3152, 206, 'Yun-lin', 'YL', 1),
(3153, 206, 'Chia-i city', 'CC', 1),
(3154, 206, 'Chi-lung', 'CL', 1),
(3155, 206, 'Hsin-chu', 'HC', 1),
(3156, 206, 'T\'ai-chung', 'TH', 1),
(3157, 206, 'T\'ai-nan', 'TN', 1),
(3158, 206, 'Kao-hsiung city', 'KC', 1),
(3159, 206, 'T\'ai-pei city', 'TC', 1),
(3160, 207, 'Gorno-Badakhstan', 'GB', 1),
(3161, 207, 'Khatlon', 'KT', 1),
(3162, 207, 'Sughd', 'SU', 1),
(3163, 208, 'Arusha', 'AR', 1),
(3164, 208, 'Dar es Salaam', 'DS', 1),
(3165, 208, 'Dodoma', 'DO', 1),
(3166, 208, 'Iringa', 'IR', 1),
(3167, 208, 'Kagera', 'KA', 1),
(3168, 208, 'Kigoma', 'KI', 1),
(3169, 208, 'Kilimanjaro', 'KJ', 1),
(3170, 208, 'Lindi', 'LN', 1),
(3171, 208, 'Manyara', 'MY', 1),
(3172, 208, 'Mara', 'MR', 1),
(3173, 208, 'Mbeya', 'MB', 1),
(3174, 208, 'Morogoro', 'MO', 1),
(3175, 208, 'Mtwara', 'MT', 1),
(3176, 208, 'Mwanza', 'MW', 1),
(3177, 208, 'Pemba North', 'PN', 1),
(3178, 208, 'Pemba South', 'PS', 1),
(3179, 208, 'Pwani', 'PW', 1),
(3180, 208, 'Rukwa', 'RK', 1),
(3181, 208, 'Ruvuma', 'RV', 1),
(3182, 208, 'Shinyanga', 'SH', 1),
(3183, 208, 'Singida', 'SI', 1),
(3184, 208, 'Tabora', 'TB', 1),
(3185, 208, 'Tanga', 'TN', 1),
(3186, 208, 'Zanzibar Central/South', 'ZC', 1),
(3187, 208, 'Zanzibar North', 'ZN', 1),
(3188, 208, 'Zanzibar Urban/West', 'ZU', 1),
(3189, 209, 'Amnat Charoen', 'Amnat Charoen', 1),
(3190, 209, 'Ang Thong', 'Ang Thong', 1),
(3191, 209, 'Ayutthaya', 'Ayutthaya', 1),
(3192, 209, 'Bangkok', 'Bangkok', 1),
(3193, 209, 'Buriram', 'Buriram', 1),
(3194, 209, 'Chachoengsao', 'Chachoengsao', 1),
(3195, 209, 'Chai Nat', 'Chai Nat', 1),
(3196, 209, 'Chaiyaphum', 'Chaiyaphum', 1),
(3197, 209, 'Chanthaburi', 'Chanthaburi', 1),
(3198, 209, 'Chiang Mai', 'Chiang Mai', 1),
(3199, 209, 'Chiang Rai', 'Chiang Rai', 1),
(3200, 209, 'Chon Buri', 'Chon Buri', 1),
(3201, 209, 'Chumphon', 'Chumphon', 1),
(3202, 209, 'Kalasin', 'Kalasin', 1),
(3203, 209, 'Kamphaeng Phet', 'Kamphaeng Phet', 1),
(3204, 209, 'Kanchanaburi', 'Kanchanaburi', 1),
(3205, 209, 'Khon Kaen', 'Khon Kaen', 1),
(3206, 209, 'Krabi', 'Krabi', 1),
(3207, 209, 'Lampang', 'Lampang', 1),
(3208, 209, 'Lamphun', 'Lamphun', 1),
(3209, 209, 'Loei', 'Loei', 1),
(3210, 209, 'Lop Buri', 'Lop Buri', 1),
(3211, 209, 'Mae Hong Son', 'Mae Hong Son', 1),
(3212, 209, 'Maha Sarakham', 'Maha Sarakham', 1),
(3213, 209, 'Mukdahan', 'Mukdahan', 1),
(3214, 209, 'Nakhon Nayok', 'Nakhon Nayok', 1),
(3215, 209, 'Nakhon Pathom', 'Nakhon Pathom', 1),
(3216, 209, 'Nakhon Phanom', 'Nakhon Phanom', 1),
(3217, 209, 'Nakhon Ratchasima', 'Nakhon Ratchasima', 1),
(3218, 209, 'Nakhon Sawan', 'Nakhon Sawan', 1),
(3219, 209, 'Nakhon Si Thammarat', 'Nakhon Si Thammarat', 1),
(3220, 209, 'Nan', 'Nan', 1),
(3221, 209, 'Narathiwat', 'Narathiwat', 1),
(3222, 209, 'Nong Bua Lamphu', 'Nong Bua Lamphu', 1),
(3223, 209, 'Nong Khai', 'Nong Khai', 1),
(3224, 209, 'Nonthaburi', 'Nonthaburi', 1),
(3225, 209, 'Pathum Thani', 'Pathum Thani', 1),
(3226, 209, 'Pattani', 'Pattani', 1),
(3227, 209, 'Phangnga', 'Phangnga', 1),
(3228, 209, 'Phatthalung', 'Phatthalung', 1),
(3229, 209, 'Phayao', 'Phayao', 1),
(3230, 209, 'Phetchabun', 'Phetchabun', 1),
(3231, 209, 'Phetchaburi', 'Phetchaburi', 1),
(3232, 209, 'Phichit', 'Phichit', 1),
(3233, 209, 'Phitsanulok', 'Phitsanulok', 1),
(3234, 209, 'Phrae', 'Phrae', 1),
(3235, 209, 'Phuket', 'Phuket', 1),
(3236, 209, 'Prachin Buri', 'Prachin Buri', 1),
(3237, 209, 'Prachuap Khiri Khan', 'Prachuap Khiri Khan', 1),
(3238, 209, 'Ranong', 'Ranong', 1),
(3239, 209, 'Ratchaburi', 'Ratchaburi', 1),
(3240, 209, 'Rayong', 'Rayong', 1),
(3241, 209, 'Roi Et', 'Roi Et', 1),
(3242, 209, 'Sa Kaeo', 'Sa Kaeo', 1),
(3243, 209, 'Sakon Nakhon', 'Sakon Nakhon', 1),
(3244, 209, 'Samut Prakan', 'Samut Prakan', 1),
(3245, 209, 'Samut Sakhon', 'Samut Sakhon', 1),
(3246, 209, 'Samut Songkhram', 'Samut Songkhram', 1),
(3247, 209, 'Sara Buri', 'Sara Buri', 1),
(3248, 209, 'Satun', 'Satun', 1),
(3249, 209, 'Sing Buri', 'Sing Buri', 1),
(3250, 209, 'Sisaket', 'Sisaket', 1),
(3251, 209, 'Songkhla', 'Songkhla', 1),
(3252, 209, 'Sukhothai', 'Sukhothai', 1),
(3253, 209, 'Suphan Buri', 'Suphan Buri', 1),
(3254, 209, 'Surat Thani', 'Surat Thani', 1),
(3255, 209, 'Surin', 'Surin', 1),
(3256, 209, 'Tak', 'Tak', 1),
(3257, 209, 'Trang', 'Trang', 1),
(3258, 209, 'Trat', 'Trat', 1),
(3259, 209, 'Ubon Ratchathani', 'Ubon Ratchathani', 1),
(3260, 209, 'Udon Thani', 'Udon Thani', 1),
(3261, 209, 'Uthai Thani', 'Uthai Thani', 1),
(3262, 209, 'Uttaradit', 'Uttaradit', 1),
(3263, 209, 'Yala', 'Yala', 1),
(3264, 209, 'Yasothon', 'Yasothon', 1),
(3265, 210, 'Kara', 'K', 1),
(3266, 210, 'Plateaux', 'P', 1),
(3267, 210, 'Savanes', 'S', 1),
(3268, 210, 'Centrale', 'C', 1),
(3269, 210, 'Maritime', 'M', 1),
(3270, 211, 'Atafu', 'A', 1),
(3271, 211, 'Fakaofo', 'F', 1),
(3272, 211, 'Nukunonu', 'N', 1),
(3273, 212, 'Ha\'apai', 'H', 1),
(3274, 212, 'Tongatapu', 'T', 1),
(3275, 212, 'Vava\'u', 'V', 1),
(3276, 213, 'Couva/Tabaquite/Talparo', 'CT', 1),
(3277, 213, 'Diego Martin', 'DM', 1),
(3278, 213, 'Mayaro/Rio Claro', 'MR', 1),
(3279, 213, 'Penal/Debe', 'PD', 1),
(3280, 213, 'Princes Town', 'PT', 1),
(3281, 213, 'Sangre Grande', 'SG', 1),
(3282, 213, 'San Juan/Laventille', 'SL', 1),
(3283, 213, 'Siparia', 'SI', 1),
(3284, 213, 'Tunapuna/Piarco', 'TP', 1),
(3285, 213, 'Port of Spain', 'PS', 1),
(3286, 213, 'San Fernando', 'SF', 1),
(3287, 213, 'Arima', 'AR', 1),
(3288, 213, 'Point Fortin', 'PF', 1),
(3289, 213, 'Chaguanas', 'CH', 1),
(3290, 213, 'Tobago', 'TO', 1),
(3291, 214, 'Ariana', 'AR', 1),
(3292, 214, 'Beja', 'BJ', 1),
(3293, 214, 'Ben Arous', 'BA', 1),
(3294, 214, 'Bizerte', 'BI', 1),
(3295, 214, 'Gabes', 'GB', 1),
(3296, 214, 'Gafsa', 'GF', 1),
(3297, 214, 'Jendouba', 'JE', 1),
(3298, 214, 'Kairouan', 'KR', 1),
(3299, 214, 'Kasserine', 'KS', 1),
(3300, 214, 'Kebili', 'KB', 1),
(3301, 214, 'Kef', 'KF', 1),
(3302, 214, 'Mahdia', 'MH', 1),
(3303, 214, 'Manouba', 'MN', 1),
(3304, 214, 'Medenine', 'ME', 1),
(3305, 214, 'Monastir', 'MO', 1),
(3306, 214, 'Nabeul', 'NA', 1),
(3307, 214, 'Sfax', 'SF', 1),
(3308, 214, 'Sidi', 'SD', 1),
(3309, 214, 'Siliana', 'SL', 1),
(3310, 214, 'Sousse', 'SO', 1),
(3311, 214, 'Tataouine', 'TA', 1),
(3312, 214, 'Tozeur', 'TO', 1),
(3313, 214, 'Tunis', 'TU', 1),
(3314, 214, 'Zaghouan', 'ZA', 1),
(3315, 215, 'Adana', 'ADA', 1),
(3316, 215, 'Adıyaman', 'ADI', 1),
(3317, 215, 'Afyonkarahisar', 'AFY', 1),
(3318, 215, 'Ağrı', 'AGR', 1),
(3319, 215, 'Aksaray', 'AKS', 1),
(3320, 215, 'Amasya', 'AMA', 1),
(3321, 215, 'Ankara', 'ANK', 1),
(3322, 215, 'Antalya', 'ANT', 1),
(3323, 215, 'Ardahan', 'ARD', 1),
(3324, 215, 'Artvin', 'ART', 1),
(3325, 215, 'Aydın', 'AYI', 1),
(3326, 215, 'Balıkesir', 'BAL', 1),
(3327, 215, 'Bartın', 'BAR', 1),
(3328, 215, 'Batman', 'BAT', 1),
(3329, 215, 'Bayburt', 'BAY', 1),
(3330, 215, 'Bilecik', 'BIL', 1),
(3331, 215, 'Bingöl', 'BIN', 1),
(3332, 215, 'Bitlis', 'BIT', 1),
(3333, 215, 'Bolu', 'BOL', 1),
(3334, 215, 'Burdur', 'BRD', 1),
(3335, 215, 'Bursa', 'BRS', 1),
(3336, 215, 'Çanakkale', 'CKL', 1),
(3337, 215, 'Çankırı', 'CKR', 1),
(3338, 215, 'Çorum', 'COR', 1),
(3339, 215, 'Denizli', 'DEN', 1),
(3340, 215, 'Diyarbakır', 'DIY', 1),
(3341, 215, 'Düzce', 'DUZ', 1),
(3342, 215, 'Edirne', 'EDI', 1),
(3343, 215, 'Elazığ', 'ELA', 1),
(3344, 215, 'Erzincan', 'EZC', 1),
(3345, 215, 'Erzurum', 'EZR', 1),
(3346, 215, 'Eskişehir', 'ESK', 1),
(3347, 215, 'Gaziantep', 'GAZ', 1),
(3348, 215, 'Giresun', 'GIR', 1),
(3349, 215, 'Gümüşhane', 'GMS', 1),
(3350, 215, 'Hakkari', 'HKR', 1),
(3351, 215, 'Hatay', 'HTY', 1),
(3352, 215, 'Iğdır', 'IGD', 1),
(3353, 215, 'Isparta', 'ISP', 1),
(3354, 215, 'İstanbul', 'IST', 1),
(3355, 215, 'İzmir', 'IZM', 1),
(3356, 215, 'Kahramanmaraş', 'KAH', 1),
(3357, 215, 'Karabük', 'KRB', 1),
(3358, 215, 'Karaman', 'KRM', 1),
(3359, 215, 'Kars', 'KRS', 1),
(3360, 215, 'Kastamonu', 'KAS', 1),
(3361, 215, 'Kayseri', 'KAY', 1),
(3362, 215, 'Kilis', 'KLS', 1),
(3363, 215, 'Kırıkkale', 'KRK', 1),
(3364, 215, 'Kırklareli', 'KLR', 1),
(3365, 215, 'Kırşehir', 'KRH', 1),
(3366, 215, 'Kocaeli', 'KOC', 1),
(3367, 215, 'Konya', 'KON', 1),
(3368, 215, 'Kütahya', 'KUT', 1),
(3369, 215, 'Malatya', 'MAL', 1),
(3370, 215, 'Manisa', 'MAN', 1),
(3371, 215, 'Mardin', 'MAR', 1),
(3372, 215, 'Mersin', 'MER', 1),
(3373, 215, 'Muğla', 'MUG', 1),
(3374, 215, 'Muş', 'MUS', 1),
(3375, 215, 'Nevşehir', 'NEV', 1),
(3376, 215, 'Niğde', 'NIG', 1),
(3377, 215, 'Ordu', 'ORD', 1),
(3378, 215, 'Osmaniye', 'OSM', 1),
(3379, 215, 'Rize', 'RIZ', 1),
(3380, 215, 'Sakarya', 'SAK', 1),
(3381, 215, 'Samsun', 'SAM', 1),
(3382, 215, 'Şanlıurfa', 'SAN', 1),
(3383, 215, 'Siirt', 'SII', 1),
(3384, 215, 'Sinop', 'SIN', 1),
(3385, 215, 'Şırnak', 'SIR', 1),
(3386, 215, 'Sivas', 'SIV', 1),
(3387, 215, 'Tekirdağ', 'TEL', 1),
(3388, 215, 'Tokat', 'TOK', 1),
(3389, 215, 'Trabzon', 'TRA', 1),
(3390, 215, 'Tunceli', 'TUN', 1),
(3391, 215, 'Uşak', 'USK', 1),
(3392, 215, 'Van', 'VAN', 1),
(3393, 215, 'Yalova', 'YAL', 1),
(3394, 215, 'Yozgat', 'YOZ', 1),
(3395, 215, 'Zonguldak', 'ZON', 1),
(3396, 216, 'Ahal Welayaty', 'A', 1),
(3397, 216, 'Balkan Welayaty', 'B', 1),
(3398, 216, 'Dashhowuz Welayaty', 'D', 1),
(3399, 216, 'Lebap Welayaty', 'L', 1),
(3400, 216, 'Mary Welayaty', 'M', 1),
(3401, 217, 'Ambergris Cays', 'AC', 1),
(3402, 217, 'Dellis Cay', 'DC', 1),
(3403, 217, 'French Cay', 'FC', 1),
(3404, 217, 'Little Water Cay', 'LW', 1),
(3405, 217, 'Parrot Cay', 'RC', 1),
(3406, 217, 'Pine Cay', 'PN', 1),
(3407, 217, 'Salt Cay', 'SL', 1),
(3408, 217, 'Grand Turk', 'GT', 1),
(3409, 217, 'South Caicos', 'SC', 1),
(3410, 217, 'East Caicos', 'EC', 1),
(3411, 217, 'Middle Caicos', 'MC', 1),
(3412, 217, 'North Caicos', 'NC', 1),
(3413, 217, 'Providenciales', 'PR', 1),
(3414, 217, 'West Caicos', 'WC', 1),
(3415, 218, 'Nanumanga', 'NMG', 1),
(3416, 218, 'Niulakita', 'NLK', 1),
(3417, 218, 'Niutao', 'NTO', 1),
(3418, 218, 'Funafuti', 'FUN', 1),
(3419, 218, 'Nanumea', 'NME', 1),
(3420, 218, 'Nui', 'NUI', 1),
(3421, 218, 'Nukufetau', 'NFT', 1),
(3422, 218, 'Nukulaelae', 'NLL', 1),
(3423, 218, 'Vaitupu', 'VAI', 1),
(3424, 219, 'Kalangala', 'KAL', 1),
(3425, 219, 'Kampala', 'KMP', 1),
(3426, 219, 'Kayunga', 'KAY', 1),
(3427, 219, 'Kiboga', 'KIB', 1),
(3428, 219, 'Luwero', 'LUW', 1),
(3429, 219, 'Masaka', 'MAS', 1),
(3430, 219, 'Mpigi', 'MPI', 1),
(3431, 219, 'Mubende', 'MUB', 1),
(3432, 219, 'Mukono', 'MUK', 1),
(3433, 219, 'Nakasongola', 'NKS', 1),
(3434, 219, 'Rakai', 'RAK', 1),
(3435, 219, 'Sembabule', 'SEM', 1),
(3436, 219, 'Wakiso', 'WAK', 1),
(3437, 219, 'Bugiri', 'BUG', 1),
(3438, 219, 'Busia', 'BUS', 1),
(3439, 219, 'Iganga', 'IGA', 1),
(3440, 219, 'Jinja', 'JIN', 1),
(3441, 219, 'Kaberamaido', 'KAB', 1),
(3442, 219, 'Kamuli', 'KML', 1),
(3443, 219, 'Kapchorwa', 'KPC', 1),
(3444, 219, 'Katakwi', 'KTK', 1),
(3445, 219, 'Kumi', 'KUM', 1),
(3446, 219, 'Mayuge', 'MAY', 1),
(3447, 219, 'Mbale', 'MBA', 1),
(3448, 219, 'Pallisa', 'PAL', 1),
(3449, 219, 'Sironko', 'SIR', 1),
(3450, 219, 'Soroti', 'SOR', 1),
(3451, 219, 'Tororo', 'TOR', 1),
(3452, 219, 'Adjumani', 'ADJ', 1),
(3453, 219, 'Apac', 'APC', 1),
(3454, 219, 'Arua', 'ARU', 1),
(3455, 219, 'Gulu', 'GUL', 1),
(3456, 219, 'Kitgum', 'KIT', 1),
(3457, 219, 'Kotido', 'KOT', 1),
(3458, 219, 'Lira', 'LIR', 1),
(3459, 219, 'Moroto', 'MRT', 1),
(3460, 219, 'Moyo', 'MOY', 1),
(3461, 219, 'Nakapiripirit', 'NAK', 1),
(3462, 219, 'Nebbi', 'NEB', 1),
(3463, 219, 'Pader', 'PAD', 1),
(3464, 219, 'Yumbe', 'YUM', 1),
(3465, 219, 'Bundibugyo', 'BUN', 1),
(3466, 219, 'Bushenyi', 'BSH', 1),
(3467, 219, 'Hoima', 'HOI', 1),
(3468, 219, 'Kabale', 'KBL', 1),
(3469, 219, 'Kabarole', 'KAR', 1),
(3470, 219, 'Kamwenge', 'KAM', 1),
(3471, 219, 'Kanungu', 'KAN', 1),
(3472, 219, 'Kasese', 'KAS', 1),
(3473, 219, 'Kibaale', 'KBA', 1),
(3474, 219, 'Kisoro', 'KIS', 1),
(3475, 219, 'Kyenjojo', 'KYE', 1),
(3476, 219, 'Masindi', 'MSN', 1),
(3477, 219, 'Mbarara', 'MBR', 1),
(3478, 219, 'Ntungamo', 'NTU', 1),
(3479, 219, 'Rukungiri', 'RUK', 1),
(3480, 220, 'Cherkas\'ka Oblast\'', '71', 1),
(3481, 220, 'Chernihivs\'ka Oblast\'', '74', 1),
(3482, 220, 'Chernivets\'ka Oblast\'', '77', 1),
(3483, 220, 'Crimea', '43', 1),
(3484, 220, 'Dnipropetrovs\'ka Oblast\'', '12', 1),
(3485, 220, 'Donets\'ka Oblast\'', '14', 1),
(3486, 220, 'Ivano-Frankivs\'ka Oblast\'', '26', 1),
(3487, 220, 'Khersons\'ka Oblast\'', '65', 1),
(3488, 220, 'Khmel\'nyts\'ka Oblast\'', '68', 1),
(3489, 220, 'Kirovohrads\'ka Oblast\'', '35', 1),
(3490, 220, 'Kyiv', '30', 1),
(3491, 220, 'Kyivs\'ka Oblast\'', '32', 1),
(3492, 220, 'Luhans\'ka Oblast\'', '09', 1),
(3493, 220, 'L\'vivs\'ka Oblast\'', '46', 1),
(3494, 220, 'Mykolayivs\'ka Oblast\'', '48', 1),
(3495, 220, 'Odes\'ka Oblast\'', '51', 1),
(3496, 220, 'Poltavs\'ka Oblast\'', '53', 1),
(3497, 220, 'Rivnens\'ka Oblast\'', '56', 1),
(3498, 220, 'Sevastopol\'', '40', 1),
(3499, 220, 'Sums\'ka Oblast\'', '59', 1),
(3500, 220, 'Ternopil\'s\'ka Oblast\'', '61', 1),
(3501, 220, 'Vinnyts\'ka Oblast\'', '05', 1),
(3502, 220, 'Volyns\'ka Oblast\'', '07', 1),
(3503, 220, 'Zakarpats\'ka Oblast\'', '21', 1),
(3504, 220, 'Zaporiz\'ka Oblast\'', '23', 1),
(3505, 220, 'Zhytomyrs\'ka oblast\'', '18', 1),
(3506, 221, 'Abu Dhabi', 'ADH', 1),
(3507, 221, '\'Ajman', 'AJ', 1),
(3508, 221, 'Al Fujayrah', 'FU', 1),
(3509, 221, 'Ash Shariqah', 'SH', 1),
(3510, 221, 'Dubai', 'DU', 1),
(3511, 221, 'R\'as al Khaymah', 'RK', 1),
(3512, 221, 'Umm al Qaywayn', 'UQ', 1),
(3513, 222, 'Aberdeen', 'ABN', 1),
(3514, 222, 'Aberdeenshire', 'ABNS', 1),
(3515, 222, 'Anglesey', 'ANG', 1),
(3516, 222, 'Angus', 'AGS', 1),
(3517, 222, 'Argyll and Bute', 'ARY', 1),
(3518, 222, 'Bedfordshire', 'BEDS', 1),
(3519, 222, 'Berkshire', 'BERKS', 1),
(3520, 222, 'Blaenau Gwent', 'BLA', 1),
(3521, 222, 'Bridgend', 'BRI', 1),
(3522, 222, 'Bristol', 'BSTL', 1),
(3523, 222, 'Buckinghamshire', 'BUCKS', 1),
(3524, 222, 'Caerphilly', 'CAE', 1),
(3525, 222, 'Cambridgeshire', 'CAMBS', 1),
(3526, 222, 'Cardiff', 'CDF', 1),
(3527, 222, 'Carmarthenshire', 'CARM', 1),
(3528, 222, 'Ceredigion', 'CDGN', 1),
(3529, 222, 'Cheshire', 'CHES', 1),
(3530, 222, 'Clackmannanshire', 'CLACK', 1),
(3531, 222, 'Conwy', 'CON', 1),
(3532, 222, 'Cornwall', 'CORN', 1),
(3533, 222, 'Denbighshire', 'DNBG', 1),
(3534, 222, 'Derbyshire', 'DERBY', 1),
(3535, 222, 'Devon', 'DVN', 1),
(3536, 222, 'Dorset', 'DOR', 1),
(3537, 222, 'Dumfries and Galloway', 'DGL', 1),
(3538, 222, 'Dundee', 'DUND', 1),
(3539, 222, 'Durham', 'DHM', 1),
(3540, 222, 'East Ayrshire', 'ARYE', 1),
(3541, 222, 'East Dunbartonshire', 'DUNBE', 1),
(3542, 222, 'East Lothian', 'LOTE', 1),
(3543, 222, 'East Renfrewshire', 'RENE', 1),
(3544, 222, 'East Riding of Yorkshire', 'ERYS', 1),
(3545, 222, 'East Sussex', 'SXE', 1),
(3546, 222, 'Edinburgh', 'EDIN', 1),
(3547, 222, 'Essex', 'ESX', 1),
(3548, 222, 'Falkirk', 'FALK', 1),
(3549, 222, 'Fife', 'FFE', 1),
(3550, 222, 'Flintshire', 'FLINT', 1),
(3551, 222, 'Glasgow', 'GLAS', 1),
(3552, 222, 'Gloucestershire', 'GLOS', 1),
(3553, 222, 'Greater London', 'LDN', 1),
(3554, 222, 'Greater Manchester', 'MCH', 1),
(3555, 222, 'Gwynedd', 'GDD', 1),
(3556, 222, 'Hampshire', 'HANTS', 1),
(3557, 222, 'Herefordshire', 'HWR', 1),
(3558, 222, 'Hertfordshire', 'HERTS', 1),
(3559, 222, 'Highlands', 'HLD', 1),
(3560, 222, 'Inverclyde', 'IVER', 1),
(3561, 222, 'Isle of Wight', 'IOW', 1),
(3562, 222, 'Kent', 'KNT', 1),
(3563, 222, 'Lancashire', 'LANCS', 1),
(3564, 222, 'Leicestershire', 'LEICS', 1),
(3565, 222, 'Lincolnshire', 'LINCS', 1),
(3566, 222, 'Merseyside', 'MSY', 1),
(3567, 222, 'Merthyr Tydfil', 'MERT', 1),
(3568, 222, 'Midlothian', 'MLOT', 1),
(3569, 222, 'Monmouthshire', 'MMOUTH', 1),
(3570, 222, 'Moray', 'MORAY', 1),
(3571, 222, 'Neath Port Talbot', 'NPRTAL', 1),
(3572, 222, 'Newport', 'NEWPT', 1),
(3573, 222, 'Norfolk', 'NOR', 1),
(3574, 222, 'North Ayrshire', 'ARYN', 1),
(3575, 222, 'North Lanarkshire', 'LANN', 1),
(3576, 222, 'North Yorkshire', 'YSN', 1),
(3577, 222, 'Northamptonshire', 'NHM', 1),
(3578, 222, 'Northumberland', 'NLD', 1),
(3579, 222, 'Nottinghamshire', 'NOT', 1),
(3580, 222, 'Orkney Islands', 'ORK', 1),
(3581, 222, 'Oxfordshire', 'OFE', 1),
(3582, 222, 'Pembrokeshire', 'PEM', 1),
(3583, 222, 'Perth and Kinross', 'PERTH', 1),
(3584, 222, 'Powys', 'PWS', 1),
(3585, 222, 'Renfrewshire', 'REN', 1),
(3586, 222, 'Rhondda Cynon Taff', 'RHON', 1),
(3587, 222, 'Rutland', 'RUT', 1),
(3588, 222, 'Scottish Borders', 'BOR', 1),
(3589, 222, 'Shetland Islands', 'SHET', 1),
(3590, 222, 'Shropshire', 'SPE', 1),
(3591, 222, 'Somerset', 'SOM', 1),
(3592, 222, 'South Ayrshire', 'ARYS', 1),
(3593, 222, 'South Lanarkshire', 'LANS', 1),
(3594, 222, 'South Yorkshire', 'YSS', 1),
(3595, 222, 'Staffordshire', 'SFD', 1),
(3596, 222, 'Stirling', 'STIR', 1),
(3597, 222, 'Suffolk', 'SFK', 1),
(3598, 222, 'Surrey', 'SRY', 1),
(3599, 222, 'Swansea', 'SWAN', 1),
(3600, 222, 'Torfaen', 'TORF', 1),
(3601, 222, 'Tyne and Wear', 'TWR', 1),
(3602, 222, 'Vale of Glamorgan', 'VGLAM', 1),
(3603, 222, 'Warwickshire', 'WARKS', 1),
(3604, 222, 'West Dunbartonshire', 'WDUN', 1),
(3605, 222, 'West Lothian', 'WLOT', 1),
(3606, 222, 'West Midlands', 'WMD', 1),
(3607, 222, 'West Sussex', 'SXW', 1),
(3608, 222, 'West Yorkshire', 'YSW', 1),
(3609, 222, 'Western Isles', 'WIL', 1),
(3610, 222, 'Wiltshire', 'WLT', 1),
(3611, 222, 'Worcestershire', 'WORCS', 1),
(3612, 222, 'Wrexham', 'WRX', 1),
(3613, 223, 'Alabama', 'AL', 1),
(3614, 223, 'Alaska', 'AK', 1),
(3615, 223, 'American Samoa', 'AS', 1),
(3616, 223, 'Arizona', 'AZ', 1),
(3617, 223, 'Arkansas', 'AR', 1),
(3618, 223, 'Armed Forces Africa', 'AF', 1),
(3619, 223, 'Armed Forces Americas', 'AA', 1),
(3620, 223, 'Armed Forces Canada', 'AC', 1),
(3621, 223, 'Armed Forces Europe', 'AE', 1),
(3622, 223, 'Armed Forces Middle East', 'AM', 1),
(3623, 223, 'Armed Forces Pacific', 'AP', 1),
(3624, 223, 'California', 'CA', 1),
(3625, 223, 'Colorado', 'CO', 1),
(3626, 223, 'Connecticut', 'CT', 1),
(3627, 223, 'Delaware', 'DE', 1),
(3628, 223, 'District of Columbia', 'DC', 1),
(3629, 223, 'Federated States Of Micronesia', 'FM', 1),
(3630, 223, 'Florida', 'FL', 1),
(3631, 223, 'Georgia', 'GA', 1),
(3632, 223, 'Guam', 'GU', 1),
(3633, 223, 'Hawaii', 'HI', 1),
(3634, 223, 'Idaho', 'ID', 1),
(3635, 223, 'Illinois', 'IL', 1),
(3636, 223, 'Indiana', 'IN', 1),
(3637, 223, 'Iowa', 'IA', 1),
(3638, 223, 'Kansas', 'KS', 1),
(3639, 223, 'Kentucky', 'KY', 1),
(3640, 223, 'Louisiana', 'LA', 1),
(3641, 223, 'Maine', 'ME', 1),
(3642, 223, 'Marshall Islands', 'MH', 1),
(3643, 223, 'Maryland', 'MD', 1),
(3644, 223, 'Massachusetts', 'MA', 1),
(3645, 223, 'Michigan', 'MI', 1),
(3646, 223, 'Minnesota', 'MN', 1),
(3647, 223, 'Mississippi', 'MS', 1),
(3648, 223, 'Missouri', 'MO', 1),
(3649, 223, 'Montana', 'MT', 1),
(3650, 223, 'Nebraska', 'NE', 1),
(3651, 223, 'Nevada', 'NV', 1),
(3652, 223, 'New Hampshire', 'NH', 1),
(3653, 223, 'New Jersey', 'NJ', 1),
(3654, 223, 'New Mexico', 'NM', 1),
(3655, 223, 'New York', 'NY', 1),
(3656, 223, 'North Carolina', 'NC', 1),
(3657, 223, 'North Dakota', 'ND', 1),
(3658, 223, 'Northern Mariana Islands', 'MP', 1),
(3659, 223, 'Ohio', 'OH', 1),
(3660, 223, 'Oklahoma', 'OK', 1),
(3661, 223, 'Oregon', 'OR', 1),
(3662, 223, 'Palau', 'PW', 1),
(3663, 223, 'Pennsylvania', 'PA', 1),
(3664, 223, 'Puerto Rico', 'PR', 1),
(3665, 223, 'Rhode Island', 'RI', 1),
(3666, 223, 'South Carolina', 'SC', 1),
(3667, 223, 'South Dakota', 'SD', 1),
(3668, 223, 'Tennessee', 'TN', 1),
(3669, 223, 'Texas', 'TX', 1),
(3670, 223, 'Utah', 'UT', 1),
(3671, 223, 'Vermont', 'VT', 1),
(3672, 223, 'Virgin Islands', 'VI', 1),
(3673, 223, 'Virginia', 'VA', 1),
(3674, 223, 'Washington', 'WA', 1),
(3675, 223, 'West Virginia', 'WV', 1),
(3676, 223, 'Wisconsin', 'WI', 1),
(3677, 223, 'Wyoming', 'WY', 1),
(3678, 224, 'Baker Island', 'BI', 1),
(3679, 224, 'Howland Island', 'HI', 1),
(3680, 224, 'Jarvis Island', 'JI', 1),
(3681, 224, 'Johnston Atoll', 'JA', 1),
(3682, 224, 'Kingman Reef', 'KR', 1),
(3683, 224, 'Midway Atoll', 'MA', 1),
(3684, 224, 'Navassa Island', 'NI', 1),
(3685, 224, 'Palmyra Atoll', 'PA', 1),
(3686, 224, 'Wake Island', 'WI', 1),
(3687, 225, 'Artigas', 'AR', 1),
(3688, 225, 'Canelones', 'CA', 1),
(3689, 225, 'Cerro Largo', 'CL', 1),
(3690, 225, 'Colonia', 'CO', 1),
(3691, 225, 'Durazno', 'DU', 1),
(3692, 225, 'Flores', 'FS', 1),
(3693, 225, 'Florida', 'FA', 1),
(3694, 225, 'Lavalleja', 'LA', 1),
(3695, 225, 'Maldonado', 'MA', 1),
(3696, 225, 'Montevideo', 'MO', 1),
(3697, 225, 'Paysandu', 'PA', 1),
(3698, 225, 'Rio Negro', 'RN', 1),
(3699, 225, 'Rivera', 'RV', 1),
(3700, 225, 'Rocha', 'RO', 1),
(3701, 225, 'Salto', 'SL', 1),
(3702, 225, 'San Jose', 'SJ', 1),
(3703, 225, 'Soriano', 'SO', 1),
(3704, 225, 'Tacuarembo', 'TA', 1),
(3705, 225, 'Treinta y Tres', 'TT', 1),
(3706, 226, 'Andijon', 'AN', 1),
(3707, 226, 'Buxoro', 'BU', 1),
(3708, 226, 'Farg\'ona', 'FA', 1),
(3709, 226, 'Jizzax', 'JI', 1),
(3710, 226, 'Namangan', 'NG', 1),
(3711, 226, 'Navoiy', 'NW', 1),
(3712, 226, 'Qashqadaryo', 'QA', 1),
(3713, 226, 'Qoraqalpog\'iston Republikasi', 'QR', 1),
(3714, 226, 'Samarqand', 'SA', 1),
(3715, 226, 'Sirdaryo', 'SI', 1),
(3716, 226, 'Surxondaryo', 'SU', 1),
(3717, 226, 'Toshkent City', 'TK', 1),
(3718, 226, 'Toshkent Region', 'TO', 1),
(3719, 226, 'Xorazm', 'XO', 1),
(3720, 227, 'Malampa', 'MA', 1),
(3721, 227, 'Penama', 'PE', 1),
(3722, 227, 'Sanma', 'SA', 1),
(3723, 227, 'Shefa', 'SH', 1),
(3724, 227, 'Tafea', 'TA', 1),
(3725, 227, 'Torba', 'TO', 1),
(3726, 229, 'Amazonas', 'AM', 1),
(3727, 229, 'Anzoategui', 'AN', 1),
(3728, 229, 'Apure', 'AP', 1),
(3729, 229, 'Aragua', 'AR', 1),
(3730, 229, 'Barinas', 'BA', 1),
(3731, 229, 'Bolivar', 'BO', 1),
(3732, 229, 'Carabobo', 'CA', 1),
(3733, 229, 'Cojedes', 'CO', 1),
(3734, 229, 'Delta Amacuro', 'DA', 1),
(3735, 229, 'Dependencias Federales', 'DF', 1),
(3736, 229, 'Distrito Federal', 'DI', 1),
(3737, 229, 'Falcon', 'FA', 1),
(3738, 229, 'Guarico', 'GU', 1),
(3739, 229, 'Lara', 'LA', 1),
(3740, 229, 'Merida', 'ME', 1),
(3741, 229, 'Miranda', 'MI', 1),
(3742, 229, 'Monagas', 'MO', 1),
(3743, 229, 'Nueva Esparta', 'NE', 1),
(3744, 229, 'Portuguesa', 'PO', 1),
(3745, 229, 'Sucre', 'SU', 1),
(3746, 229, 'Tachira', 'TA', 1),
(3747, 229, 'Trujillo', 'TR', 1),
(3748, 229, 'Vargas', 'VA', 1),
(3749, 229, 'Yaracuy', 'YA', 1),
(3750, 229, 'Zulia', 'ZU', 1),
(3751, 230, 'An Giang', '', 1),
(3752, 230, 'Bắc Giang', '', 1),
(3753, 230, 'Bắc Kạn', '', 1),
(3754, 230, 'Bạc Liêu', '', 1),
(3755, 230, 'Bắc Ninh', '', 1),
(3756, 230, 'Bà Rịa Vũng Tàu', '', 1),
(3757, 230, 'Bến Tre', '', 1),
(3758, 230, 'Bình Định', '', 1),
(3759, 230, 'Bình Dương', '', 1),
(3760, 230, 'Bình Phước', '', 1),
(3761, 230, 'Bình Thuận', '', 1),
(3762, 230, 'Cà Mau', '', 1),
(3763, 230, 'Cần Thơ', '', 1),
(3764, 230, 'Cao Bằng', '', 1),
(3765, 230, 'Đắk Lắk', '', 1),
(3766, 230, 'Đắk Nông', '', 1),
(3767, 230, 'Đà Nẵng', '', 1),
(3768, 230, 'Điện Biên', '', 1),
(3769, 230, 'Đồng Nai', '', 1),
(3770, 230, 'Đồng Tháp', '', 1),
(3771, 230, 'Gia Lai', '', 1),
(3772, 230, 'Hà Giang', '', 1),
(3773, 230, 'Hải Dương', '', 1),
(3774, 230, 'Hải Phòng', '', 1),
(3775, 230, 'Hà Nam', '', 1),
(3776, 230, 'Hà Nội', '', 1),
(3778, 230, 'Hà Tĩnh', '', 1),
(3779, 230, 'Hòa Bình', '', 1),
(3780, 230, 'Thành Phố Hồ Chí Minh', '', 1),
(3781, 230, 'Hậu Giang', '', 1),
(3782, 230, 'Hưng Yên', '', 1),
(3783, 230, 'Khánh Hòa', '', 1),
(3784, 230, 'Kiên Giang', '', 1),
(3785, 230, 'Kon Tum', '', 1),
(3786, 230, 'Lai Châu', '', 1),
(3787, 230, 'Lâm Đồng', '', 1),
(3788, 230, 'Lạng Sơn', '', 1),
(3789, 230, 'Lào Cai', '', 1),
(3790, 230, 'Long An', '', 1),
(3791, 230, 'Nam Định', '', 1),
(3792, 230, 'Nghệ An', '', 1),
(3793, 230, 'Ninh Bình', '', 1),
(3794, 230, 'Ninh Thuận', '', 1),
(3795, 230, 'Thanh Hóa', '', 1),
(3796, 230, 'Thừa Thiên Huế', '', 1),
(3797, 235, 'Al Hudaydah', 'HU', 1),
(3798, 235, 'Ibb', 'IB', 1),
(3799, 235, 'Al Jawf', 'JA', 1),
(3800, 235, 'Lahij', 'LA', 1),
(3801, 235, 'Ma\'rib', 'MA', 1),
(3802, 235, 'Al Mahrah', 'MR', 1),
(3803, 235, 'Al Mahwit', 'MW', 1),
(3804, 235, 'Sa\'dah', 'SD', 1),
(3805, 235, 'San\'a', 'SN', 1),
(3806, 235, 'Shabwah', 'SH', 1),
(3807, 235, 'Ta\'izz', 'TA', 1),
(3812, 237, 'Bas-Congo', 'BC', 1),
(3813, 237, 'Bandundu', 'BN', 1),
(3814, 237, 'Equateur', 'EQ', 1),
(3815, 237, 'Katanga', 'KA', 1),
(3816, 237, 'Kasai-Oriental', 'KE', 1),
(3817, 237, 'Kinshasa', 'KN', 1),
(3818, 237, 'Kasai-Occidental', 'KW', 1),
(3819, 237, 'Maniema', 'MA', 1),
(3820, 237, 'Nord-Kivu', 'NK', 1),
(3821, 237, 'Orientale', 'OR', 1),
(3822, 237, 'Sud-Kivu', 'SK', 1),
(3823, 238, 'Central', 'CE', 1),
(3824, 238, 'Copperbelt', 'CB', 1),
(3825, 238, 'Eastern', 'EA', 1),
(3826, 238, 'Luapula', 'LP', 1),
(3827, 238, 'Lusaka', 'LK', 1),
(3828, 238, 'Northern', 'NO', 1),
(3829, 238, 'North-Western', 'NW', 1),
(3830, 238, 'Southern', 'SO', 1),
(3831, 238, 'Western', 'WE', 1),
(3832, 239, 'Bulawayo', 'BU', 1),
(3833, 239, 'Harare', 'HA', 1),
(3834, 239, 'Manicaland', 'ML', 1),
(3835, 239, 'Mashonaland Central', 'MC', 1),
(3836, 239, 'Mashonaland East', 'ME', 1),
(3837, 239, 'Mashonaland West', 'MW', 1),
(3838, 239, 'Masvingo', 'MV', 1),
(3839, 239, 'Matabeleland North', 'MN', 1),
(3840, 239, 'Matabeleland South', 'MS', 1),
(3841, 239, 'Midlands', 'MD', 1),
(3861, 105, 'Campobasso', 'CB', 1),
(3862, 105, 'Carbonia-Iglesias', 'CI', 1),
(3863, 105, 'Caserta', 'CE', 1),
(3864, 105, 'Catania', 'CT', 1),
(3865, 105, 'Catanzaro', 'CZ', 1),
(3866, 105, 'Chieti', 'CH', 1),
(3867, 105, 'Como', 'CO', 1),
(3868, 105, 'Cosenza', 'CS', 1),
(3869, 105, 'Cremona', 'CR', 1),
(3870, 105, 'Crotone', 'KR', 1),
(3871, 105, 'Cuneo', 'CN', 1),
(3872, 105, 'Enna', 'EN', 1),
(3873, 105, 'Ferrara', 'FE', 1),
(3874, 105, 'Firenze', 'FI', 1),
(3875, 105, 'Foggia', 'FG', 1),
(3876, 105, 'Forli-Cesena', 'FC', 1),
(3877, 105, 'Frosinone', 'FR', 1),
(3878, 105, 'Genova', 'GE', 1),
(3879, 105, 'Gorizia', 'GO', 1),
(3880, 105, 'Grosseto', 'GR', 1),
(3881, 105, 'Imperia', 'IM', 1),
(3882, 105, 'Isernia', 'IS', 1),
(3883, 105, 'L&#39;Aquila', 'AQ', 1),
(3884, 105, 'La Spezia', 'SP', 1),
(3885, 105, 'Latina', 'LT', 1),
(3886, 105, 'Lecce', 'LE', 1),
(3887, 105, 'Lecco', 'LC', 1),
(3888, 105, 'Livorno', 'LI', 1),
(3889, 105, 'Lodi', 'LO', 1),
(3890, 105, 'Lucca', 'LU', 1),
(3891, 105, 'Macerata', 'MC', 1),
(3892, 105, 'Mantova', 'MN', 1),
(3893, 105, 'Massa-Carrara', 'MS', 1),
(3894, 105, 'Matera', 'MT', 1),
(3895, 105, 'Medio Campidano', 'VS', 1),
(3896, 105, 'Messina', 'ME', 1),
(3897, 105, 'Milano', 'MI', 1),
(3898, 105, 'Modena', 'MO', 1),
(3899, 105, 'Napoli', 'NA', 1),
(3900, 105, 'Novara', 'NO', 1),
(3901, 105, 'Nuoro', 'NU', 1),
(3902, 105, 'Ogliastra', 'OG', 1),
(3903, 105, 'Olbia-Tempio', 'OT', 1),
(3904, 105, 'Oristano', 'OR', 1),
(3905, 105, 'Padova', 'PD', 1),
(3906, 105, 'Palermo', 'PA', 1),
(3907, 105, 'Parma', 'PR', 1),
(3908, 105, 'Pavia', 'PV', 1),
(3909, 105, 'Perugia', 'PG', 1),
(3910, 105, 'Pesaro e Urbino', 'PU', 1),
(3911, 105, 'Pescara', 'PE', 1),
(3912, 105, 'Piacenza', 'PC', 1),
(3913, 105, 'Pisa', 'PI', 1),
(3914, 105, 'Pistoia', 'PT', 1),
(3915, 105, 'Pordenone', 'PN', 1),
(3916, 105, 'Potenza', 'PZ', 1),
(3917, 105, 'Prato', 'PO', 1),
(3918, 105, 'Ragusa', 'RG', 1),
(3919, 105, 'Ravenna', 'RA', 1),
(3920, 105, 'Reggio Calabria', 'RC', 1),
(3921, 105, 'Reggio Emilia', 'RE', 1),
(3922, 105, 'Rieti', 'RI', 1),
(3923, 105, 'Rimini', 'RN', 1),
(3924, 105, 'Roma', 'RM', 1),
(3925, 105, 'Rovigo', 'RO', 1),
(3926, 105, 'Salerno', 'SA', 1),
(3927, 105, 'Sassari', 'SS', 1),
(3928, 105, 'Savona', 'SV', 1),
(3929, 105, 'Siena', 'SI', 1),
(3930, 105, 'Siracusa', 'SR', 1),
(3931, 105, 'Sondrio', 'SO', 1),
(3932, 105, 'Taranto', 'TA', 1),
(3933, 105, 'Teramo', 'TE', 1),
(3934, 105, 'Terni', 'TR', 1),
(3935, 105, 'Torino', 'TO', 1),
(3936, 105, 'Trapani', 'TP', 1),
(3937, 105, 'Trento', 'TN', 1),
(3938, 105, 'Treviso', 'TV', 1),
(3939, 105, 'Trieste', 'TS', 1),
(3940, 105, 'Udine', 'UD', 1),
(3941, 105, 'Varese', 'VA', 1),
(3942, 105, 'Venezia', 'VE', 1),
(3943, 105, 'Verbano-Cusio-Ossola', 'VB', 1),
(3944, 105, 'Vercelli', 'VC', 1),
(3945, 105, 'Verona', 'VR', 1),
(3946, 105, 'Vibo Valentia', 'VV', 1),
(3947, 105, 'Vicenza', 'VI', 1),
(3948, 105, 'Viterbo', 'VT', 1),
(3949, 222, 'County Antrim', 'ANT', 1),
(3950, 222, 'County Armagh', 'ARM', 1),
(3951, 222, 'County Down', 'DOW', 1),
(3952, 222, 'County Fermanagh', 'FER', 1),
(3953, 222, 'County Londonderry', 'LDY', 1),
(3954, 222, 'County Tyrone', 'TYR', 1),
(3955, 222, 'Cumbria', 'CMA', 1),
(3956, 190, 'Pomurska', '1', 1),
(3957, 190, 'Podravska', '2', 1),
(3958, 190, 'Koroška', '3', 1),
(3959, 190, 'Savinjska', '4', 1),
(3960, 190, 'Zasavska', '5', 1),
(3961, 190, 'Spodnjeposavska', '6', 1),
(3962, 190, 'Jugovzhodna Slovenija', '7', 1),
(3963, 190, 'Osrednjeslovenska', '8', 1),
(3964, 190, 'Gorenjska', '9', 1),
(3965, 190, 'Notranjsko-kraška', '10', 1),
(3966, 190, 'Goriška', '11', 1),
(3967, 190, 'Obalno-kraška', '12', 1),
(3968, 33, 'Ruse', '', 1),
(3969, 101, 'Alborz', 'ALB', 1),
(3970, 21, 'Brussels-Capital Region', 'BRU', 1),
(3971, 138, 'Aguascalientes', 'AG', 1),
(3973, 242, 'Andrijevica', '01', 1),
(3974, 242, 'Bar', '02', 1),
(3975, 242, 'Berane', '03', 1),
(3976, 242, 'Bijelo Polje', '04', 1),
(3977, 242, 'Budva', '05', 1),
(3978, 242, 'Cetinje', '06', 1),
(3979, 242, 'Danilovgrad', '07', 1),
(3980, 242, 'Herceg-Novi', '08', 1),
(3981, 242, 'Kolašin', '09', 1),
(3982, 242, 'Kotor', '10', 1),
(3983, 242, 'Mojkovac', '11', 1),
(3984, 242, 'Nikšić', '12', 1),
(3985, 242, 'Plav', '13', 1),
(3986, 242, 'Pljevlja', '14', 1),
(3987, 242, 'Plužine', '15', 1),
(3988, 242, 'Podgorica', '16', 1),
(3989, 242, 'Rožaje', '17', 1),
(3990, 242, 'Šavnik', '18', 1),
(3991, 242, 'Tivat', '19', 1),
(3992, 242, 'Ulcinj', '20', 1),
(3993, 242, 'Žabljak', '21', 1),
(3994, 243, 'Belgrade', '00', 1),
(3995, 243, 'North Bačka', '01', 1),
(3996, 243, 'Central Banat', '02', 1),
(3997, 243, 'North Banat', '03', 1),
(3998, 243, 'South Banat', '04', 1),
(3999, 243, 'West Bačka', '05', 1),
(4000, 243, 'South Bačka', '06', 1),
(4001, 243, 'Srem', '07', 1),
(4002, 243, 'Mačva', '08', 1),
(4003, 243, 'Kolubara', '09', 1),
(4004, 243, 'Podunavlje', '10', 1),
(4005, 243, 'Braničevo', '11', 1),
(4006, 243, 'Šumadija', '12', 1),
(4007, 243, 'Pomoravlje', '13', 1),
(4008, 243, 'Bor', '14', 1),
(4009, 243, 'Zaječar', '15', 1),
(4010, 243, 'Zlatibor', '16', 1),
(4011, 243, 'Moravica', '17', 1),
(4012, 243, 'Raška', '18', 1),
(4013, 243, 'Rasina', '19', 1),
(4014, 243, 'Nišava', '20', 1),
(4015, 243, 'Toplica', '21', 1),
(4016, 243, 'Pirot', '22', 1),
(4017, 243, 'Jablanica', '23', 1),
(4018, 243, 'Pčinja', '24', 1),
(4020, 245, 'Bonaire', 'BO', 1),
(4021, 245, 'Saba', 'SA', 1),
(4022, 245, 'Sint Eustatius', 'SE', 1),
(4023, 248, 'Central Equatoria', 'EC', 1),
(4024, 248, 'Eastern Equatoria', 'EE', 1),
(4025, 248, 'Jonglei', 'JG', 1),
(4026, 248, 'Lakes', 'LK', 1),
(4027, 248, 'Northern Bahr el-Ghazal', 'BN', 1),
(4028, 248, 'Unity', 'UY', 1),
(4029, 248, 'Upper Nile', 'NU', 1),
(4030, 248, 'Warrap', 'WR', 1),
(4031, 248, 'Western Bahr el-Ghazal', 'BW', 1),
(4032, 248, 'Western Equatoria', 'EW', 1),
(4036, 117, 'Ainaži, Salacgrīvas novads', '0661405', 1),
(4037, 117, 'Aizkraukle, Aizkraukles novads', '0320201', 1),
(4038, 117, 'Aizkraukles novads', '0320200', 1),
(4039, 117, 'Aizpute, Aizputes novads', '0640605', 1),
(4040, 117, 'Aizputes novads', '0640600', 1),
(4041, 117, 'Aknīste, Aknīstes novads', '0560805', 1),
(4042, 117, 'Aknīstes novads', '0560800', 1),
(4043, 117, 'Aloja, Alojas novads', '0661007', 1),
(4044, 117, 'Alojas novads', '0661000', 1),
(4045, 117, 'Alsungas novads', '0624200', 1),
(4046, 117, 'Alūksne, Alūksnes novads', '0360201', 1),
(4047, 117, 'Alūksnes novads', '0360200', 1),
(4048, 117, 'Amatas novads', '0424701', 1),
(4049, 117, 'Ape, Apes novads', '0360805', 1),
(4050, 117, 'Apes novads', '0360800', 1),
(4051, 117, 'Auce, Auces novads', '0460805', 1),
(4052, 117, 'Auces novads', '0460800', 1),
(4053, 117, 'Ādažu novads', '0804400', 1),
(4054, 117, 'Babītes novads', '0804900', 1),
(4055, 117, 'Baldone, Baldones novads', '0800605', 1),
(4056, 117, 'Baldones novads', '0800600', 1),
(4057, 117, 'Baloži, Ķekavas novads', '0800807', 1),
(4058, 117, 'Baltinavas novads', '0384400', 1),
(4059, 117, 'Balvi, Balvu novads', '0380201', 1),
(4060, 117, 'Balvu novads', '0380200', 1),
(4061, 117, 'Bauska, Bauskas novads', '0400201', 1),
(4062, 117, 'Bauskas novads', '0400200', 1),
(4063, 117, 'Beverīnas novads', '0964700', 1),
(4064, 117, 'Brocēni, Brocēnu novads', '0840605', 1),
(4065, 117, 'Brocēnu novads', '0840601', 1),
(4066, 117, 'Burtnieku novads', '0967101', 1),
(4067, 117, 'Carnikavas novads', '0805200', 1),
(4068, 117, 'Cesvaine, Cesvaines novads', '0700807', 1),
(4069, 117, 'Cesvaines novads', '0700800', 1),
(4070, 117, 'Cēsis, Cēsu novads', '0420201', 1),
(4071, 117, 'Cēsu novads', '0420200', 1),
(4072, 117, 'Ciblas novads', '0684901', 1),
(4073, 117, 'Dagda, Dagdas novads', '0601009', 1),
(4074, 117, 'Dagdas novads', '0601000', 1),
(4075, 117, 'Daugavpils', '0050000', 1),
(4076, 117, 'Daugavpils novads', '0440200', 1),
(4077, 117, 'Dobele, Dobeles novads', '0460201', 1),
(4078, 117, 'Dobeles novads', '0460200', 1),
(4079, 117, 'Dundagas novads', '0885100', 1),
(4080, 117, 'Durbe, Durbes novads', '0640807', 1),
(4081, 117, 'Durbes novads', '0640801', 1),
(4082, 117, 'Engures novads', '0905100', 1),
(4083, 117, 'Ērgļu novads', '0705500', 1),
(4084, 117, 'Garkalnes novads', '0806000', 1),
(4085, 117, 'Grobiņa, Grobiņas novads', '0641009', 1),
(4086, 117, 'Grobiņas novads', '0641000', 1),
(4087, 117, 'Gulbene, Gulbenes novads', '0500201', 1),
(4088, 117, 'Gulbenes novads', '0500200', 1),
(4089, 117, 'Iecavas novads', '0406400', 1),
(4090, 117, 'Ikšķile, Ikšķiles novads', '0740605', 1),
(4091, 117, 'Ikšķiles novads', '0740600', 1),
(4092, 117, 'Ilūkste, Ilūkstes novads', '0440807', 1),
(4093, 117, 'Ilūkstes novads', '0440801', 1),
(4094, 117, 'Inčukalna novads', '0801800', 1),
(4095, 117, 'Jaunjelgava, Jaunjelgavas novads', '0321007', 1),
(4096, 117, 'Jaunjelgavas novads', '0321000', 1),
(4097, 117, 'Jaunpiebalgas novads', '0425700', 1),
(4098, 117, 'Jaunpils novads', '0905700', 1),
(4099, 117, 'Jelgava', '0090000', 1),
(4100, 117, 'Jelgavas novads', '0540200', 1),
(4101, 117, 'Jēkabpils', '0110000', 1),
(4102, 117, 'Jēkabpils novads', '0560200', 1),
(4103, 117, 'Jūrmala', '0130000', 1),
(4104, 117, 'Kalnciems, Jelgavas novads', '0540211', 1),
(4105, 117, 'Kandava, Kandavas novads', '0901211', 1),
(4106, 117, 'Kandavas novads', '0901201', 1),
(4107, 117, 'Kārsava, Kārsavas novads', '0681009', 1),
(4108, 117, 'Kārsavas novads', '0681000', 1),
(4109, 117, 'Kocēnu novads ,bij. Valmieras)', '0960200', 1),
(4110, 117, 'Kokneses novads', '0326100', 1),
(4111, 117, 'Krāslava, Krāslavas novads', '0600201', 1),
(4112, 117, 'Krāslavas novads', '0600202', 1),
(4113, 117, 'Krimuldas novads', '0806900', 1),
(4114, 117, 'Krustpils novads', '0566900', 1),
(4115, 117, 'Kuldīga, Kuldīgas novads', '0620201', 1),
(4116, 117, 'Kuldīgas novads', '0620200', 1),
(4117, 117, 'Ķeguma novads', '0741001', 1),
(4118, 117, 'Ķegums, Ķeguma novads', '0741009', 1),
(4119, 117, 'Ķekavas novads', '0800800', 1),
(4120, 117, 'Lielvārde, Lielvārdes novads', '0741413', 1),
(4121, 117, 'Lielvārdes novads', '0741401', 1),
(4122, 117, 'Liepāja', '0170000', 1),
(4123, 117, 'Limbaži, Limbažu novads', '0660201', 1),
(4124, 117, 'Limbažu novads', '0660200', 1),
(4125, 117, 'Līgatne, Līgatnes novads', '0421211', 1),
(4126, 117, 'Līgatnes novads', '0421200', 1),
(4127, 117, 'Līvāni, Līvānu novads', '0761211', 1),
(4128, 117, 'Līvānu novads', '0761201', 1),
(4129, 117, 'Lubāna, Lubānas novads', '0701413', 1),
(4130, 117, 'Lubānas novads', '0701400', 1),
(4131, 117, 'Ludza, Ludzas novads', '0680201', 1),
(4132, 117, 'Ludzas novads', '0680200', 1),
(4133, 117, 'Madona, Madonas novads', '0700201', 1),
(4134, 117, 'Madonas novads', '0700200', 1),
(4135, 117, 'Mazsalaca, Mazsalacas novads', '0961011', 1),
(4136, 117, 'Mazsalacas novads', '0961000', 1),
(4137, 117, 'Mālpils novads', '0807400', 1),
(4138, 117, 'Mārupes novads', '0807600', 1),
(4139, 117, 'Mērsraga novads', '0887600', 1),
(4140, 117, 'Naukšēnu novads', '0967300', 1),
(4141, 117, 'Neretas novads', '0327100', 1),
(4142, 117, 'Nīcas novads', '0647900', 1),
(4143, 117, 'Ogre, Ogres novads', '0740201', 1),
(4144, 117, 'Ogres novads', '0740202', 1),
(4145, 117, 'Olaine, Olaines novads', '0801009', 1),
(4146, 117, 'Olaines novads', '0801000', 1),
(4147, 117, 'Ozolnieku novads', '0546701', 1),
(4148, 117, 'Pārgaujas novads', '0427500', 1),
(4149, 117, 'Pāvilosta, Pāvilostas novads', '0641413', 1),
(4150, 117, 'Pāvilostas novads', '0641401', 1),
(4151, 117, 'Piltene, Ventspils novads', '0980213', 1),
(4152, 117, 'Pļaviņas, Pļaviņu novads', '0321413', 1),
(4153, 117, 'Pļaviņu novads', '0321400', 1),
(4154, 117, 'Preiļi, Preiļu novads', '0760201', 1),
(4155, 117, 'Preiļu novads', '0760202', 1),
(4156, 117, 'Priekule, Priekules novads', '0641615', 1),
(4157, 117, 'Priekules novads', '0641600', 1),
(4158, 117, 'Priekuļu novads', '0427300', 1),
(4159, 117, 'Raunas novads', '0427700', 1),
(4160, 117, 'Rēzekne', '0210000', 1),
(4161, 117, 'Rēzeknes novads', '0780200', 1),
(4162, 117, 'Riebiņu novads', '0766300', 1),
(4163, 117, 'Rīga', '0010000', 1),
(4164, 117, 'Rojas novads', '0888300', 1),
(4165, 117, 'Ropažu novads', '0808400', 1),
(4166, 117, 'Rucavas novads', '0648500', 1),
(4167, 117, 'Rugāju novads', '0387500', 1),
(4168, 117, 'Rundāles novads', '0407700', 1),
(4169, 117, 'Rūjiena, Rūjienas novads', '0961615', 1),
(4170, 117, 'Rūjienas novads', '0961600', 1),
(4171, 117, 'Sabile, Talsu novads', '0880213', 1),
(4172, 117, 'Salacgrīva, Salacgrīvas novads', '0661415', 1),
(4173, 117, 'Salacgrīvas novads', '0661400', 1),
(4174, 117, 'Salas novads', '0568700', 1),
(4175, 117, 'Salaspils novads', '0801200', 1),
(4176, 117, 'Salaspils, Salaspils novads', '0801211', 1),
(4177, 117, 'Saldus novads', '0840200', 1),
(4178, 117, 'Saldus, Saldus novads', '0840201', 1),
(4179, 117, 'Saulkrasti, Saulkrastu novads', '0801413', 1),
(4180, 117, 'Saulkrastu novads', '0801400', 1),
(4181, 117, 'Seda, Strenču novads', '0941813', 1),
(4182, 117, 'Sējas novads', '0809200', 1),
(4183, 117, 'Sigulda, Siguldas novads', '0801615', 1),
(4184, 117, 'Siguldas novads', '0801601', 1),
(4185, 117, 'Skrīveru novads', '0328200', 1),
(4186, 117, 'Skrunda, Skrundas novads', '0621209', 1),
(4187, 117, 'Skrundas novads', '0621200', 1),
(4188, 117, 'Smiltene, Smiltenes novads', '0941615', 1),
(4189, 117, 'Smiltenes novads', '0941600', 1),
(4190, 117, 'Staicele, Alojas novads', '0661017', 1),
(4191, 117, 'Stende, Talsu novads', '0880215', 1),
(4192, 117, 'Stopiņu novads', '0809600', 1),
(4193, 117, 'Strenči, Strenču novads', '0941817', 1),
(4194, 117, 'Strenču novads', '0941800', 1),
(4195, 117, 'Subate, Ilūkstes novads', '0440815', 1),
(4196, 117, 'Talsi, Talsu novads', '0880201', 1),
(4197, 117, 'Talsu novads', '0880200', 1),
(4198, 117, 'Tērvetes novads', '0468900', 1),
(4199, 117, 'Tukuma novads', '0900200', 1),
(4200, 117, 'Tukums, Tukuma novads', '0900201', 1),
(4201, 117, 'Vaiņodes novads', '0649300', 1),
(4202, 117, 'Valdemārpils, Talsu novads', '0880217', 1),
(4203, 117, 'Valka, Valkas novads', '0940201', 1),
(4204, 117, 'Valkas novads', '0940200', 1),
(4205, 117, 'Valmiera', '0250000', 1),
(4206, 117, 'Vangaži, Inčukalna novads', '0801817', 1),
(4207, 117, 'Varakļāni, Varakļānu novads', '0701817', 1),
(4208, 117, 'Varakļānu novads', '0701800', 1),
(4209, 117, 'Vārkavas novads', '0769101', 1),
(4210, 117, 'Vecpiebalgas novads', '0429300', 1),
(4211, 117, 'Vecumnieku novads', '0409500', 1),
(4212, 117, 'Ventspils', '0270000', 1),
(4213, 117, 'Ventspils novads', '0980200', 1),
(4214, 117, 'Viesīte, Viesītes novads', '0561815', 1),
(4215, 117, 'Viesītes novads', '0561800', 1),
(4216, 117, 'Viļaka, Viļakas novads', '0381615', 1),
(4217, 117, 'Viļakas novads', '0381600', 1),
(4218, 117, 'Viļāni, Viļānu novads', '0781817', 1),
(4219, 117, 'Viļānu novads', '0781800', 1),
(4220, 117, 'Zilupe, Zilupes novads', '0681817', 1),
(4221, 117, 'Zilupes novads', '0681801', 1),
(4222, 43, 'Arica y Parinacota', 'AP', 1),
(4223, 43, 'Los Rios', 'LR', 1),
(4224, 220, 'Kharkivs\'ka Oblast\'', '63', 1),
(4225, 118, 'Beirut', 'LB-BR', 1),
(4226, 118, 'Bekaa', 'LB-BE', 1),
(4227, 118, 'Mount Lebanon', 'LB-ML', 1),
(4228, 118, 'Nabatieh', 'LB-NB', 1),
(4229, 118, 'North', 'LB-NR', 1),
(4230, 118, 'South', 'LB-ST', 1),
(4231, 99, 'Telangana', 'TS', 1),
(4232, 230, 'Phú Thọ', '', 1),
(4233, 230, 'Quảng Bình', '', 1),
(4234, 230, 'Quảng Nam', '', 1),
(4235, 230, 'Quảng Ngãi', '', 1),
(4236, 230, 'Quảng Ninh', '', 1),
(4237, 230, 'Quảng Trị', '', 1),
(4238, 230, 'Sóc Trăng', '', 1),
(4239, 230, 'Sơn La', '', 1),
(4240, 230, 'Tây Ninh', '', 1),
(4241, 230, 'Thái Bình', '', 1),
(4242, 230, 'Thái Nguyên', '', 1),
(4243, 230, 'Tiền Giang', '', 1),
(4244, 230, 'Trà Vinh', '', 1),
(4245, 230, 'Tuyên Quang', '', 1),
(4246, 230, 'Vĩnh Long', '', 1),
(4247, 230, 'Vĩnh Phúc', '', 1),
(4248, 230, 'Yên Bái', '', 1),
(4249, 230, 'Phú Yên', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_zone_to_geo_zone`
--

CREATE TABLE `oc_zone_to_geo_zone` (
  `zone_to_geo_zone_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `zone_id` int(11) NOT NULL DEFAULT '0',
  `geo_zone_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_zone_to_geo_zone`
--

INSERT INTO `oc_zone_to_geo_zone` (`zone_to_geo_zone_id`, `country_id`, `zone_id`, `geo_zone_id`, `date_added`, `date_modified`) VALUES
(1, 222, 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 222, 3513, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 222, 3514, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 222, 3515, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 222, 3516, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 222, 3517, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 222, 3518, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 222, 3519, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 222, 3520, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 222, 3521, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 222, 3522, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 222, 3523, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 222, 3524, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 222, 3525, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 222, 3526, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 222, 3527, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 222, 3528, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 222, 3529, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 222, 3530, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 222, 3531, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 222, 3532, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 222, 3533, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 222, 3534, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 222, 3535, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 222, 3536, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 222, 3537, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 222, 3538, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 222, 3539, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 222, 3540, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 222, 3541, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 222, 3542, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 222, 3543, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 222, 3544, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 222, 3545, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 222, 3546, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 222, 3547, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 222, 3548, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 222, 3549, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 222, 3550, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 222, 3551, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 222, 3552, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 222, 3553, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 222, 3554, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 222, 3555, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 222, 3556, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 222, 3557, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 222, 3558, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 222, 3559, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 222, 3560, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 222, 3561, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 222, 3562, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 222, 3563, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 222, 3564, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 222, 3565, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 222, 3566, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 222, 3567, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 222, 3568, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 222, 3569, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 222, 3570, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 222, 3571, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 222, 3572, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 222, 3573, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 222, 3574, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 222, 3575, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 222, 3576, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 222, 3577, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 222, 3578, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 222, 3579, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 222, 3580, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 222, 3581, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 222, 3582, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 222, 3583, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 222, 3584, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 222, 3585, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 222, 3586, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 222, 3587, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 222, 3588, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 222, 3589, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 222, 3590, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 222, 3591, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 222, 3592, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 222, 3593, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 222, 3594, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 222, 3595, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(85, 222, 3596, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(86, 222, 3597, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(87, 222, 3598, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(88, 222, 3599, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(89, 222, 3600, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(90, 222, 3601, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(91, 222, 3602, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(92, 222, 3603, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(93, 222, 3604, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(94, 222, 3605, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(95, 222, 3606, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(96, 222, 3607, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(97, 222, 3608, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(98, 222, 3609, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(99, 222, 3610, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(100, 222, 3611, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(101, 222, 3612, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(102, 222, 3949, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(103, 222, 3950, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(104, 222, 3951, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(105, 222, 3952, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(106, 222, 3953, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(107, 222, 3954, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(108, 222, 3955, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(109, 222, 3972, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_address`
--
ALTER TABLE `oc_address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `oc_api`
--
ALTER TABLE `oc_api`
  ADD PRIMARY KEY (`api_id`);

--
-- Indexes for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  ADD PRIMARY KEY (`api_ip_id`);

--
-- Indexes for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  ADD PRIMARY KEY (`api_session_id`);

--
-- Indexes for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  ADD PRIMARY KEY (`attribute_id`);

--
-- Indexes for table `oc_attribute_description`
--
ALTER TABLE `oc_attribute_description`
  ADD PRIMARY KEY (`attribute_id`,`language_id`);

--
-- Indexes for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  ADD PRIMARY KEY (`attribute_group_id`);

--
-- Indexes for table `oc_attribute_group_description`
--
ALTER TABLE `oc_attribute_group_description`
  ADD PRIMARY KEY (`attribute_group_id`,`language_id`);

--
-- Indexes for table `oc_banner`
--
ALTER TABLE `oc_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  ADD PRIMARY KEY (`banner_image_id`);

--
-- Indexes for table `oc_cart`
--
ALTER TABLE `oc_cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_id` (`api_id`,`customer_id`,`session_id`,`product_id`,`recurring_id`);

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_filter`
--
ALTER TABLE `oc_category_filter`
  ADD PRIMARY KEY (`category_id`,`filter_id`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
  ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_category_to_store`
--
ALTER TABLE `oc_category_to_store`
  ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- Indexes for table `oc_country`
--
ALTER TABLE `oc_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  ADD PRIMARY KEY (`coupon_id`);

--
-- Indexes for table `oc_coupon_category`
--
ALTER TABLE `oc_coupon_category`
  ADD PRIMARY KEY (`coupon_id`,`category_id`);

--
-- Indexes for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  ADD PRIMARY KEY (`coupon_history_id`);

--
-- Indexes for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  ADD PRIMARY KEY (`coupon_product_id`);

--
-- Indexes for table `oc_currency`
--
ALTER TABLE `oc_currency`
  ADD PRIMARY KEY (`currency_id`);

--
-- Indexes for table `oc_customer`
--
ALTER TABLE `oc_customer`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  ADD PRIMARY KEY (`customer_activity_id`);

--
-- Indexes for table `oc_customer_affiliate`
--
ALTER TABLE `oc_customer_affiliate`
  ADD PRIMARY KEY (`customer_id`);

--
-- Indexes for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  ADD PRIMARY KEY (`customer_approval_id`);

--
-- Indexes for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  ADD PRIMARY KEY (`customer_group_id`);

--
-- Indexes for table `oc_customer_group_description`
--
ALTER TABLE `oc_customer_group_description`
  ADD PRIMARY KEY (`customer_group_id`,`language_id`);

--
-- Indexes for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  ADD PRIMARY KEY (`customer_history_id`);

--
-- Indexes for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  ADD PRIMARY KEY (`customer_ip_id`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  ADD PRIMARY KEY (`customer_login_id`),
  ADD KEY `email` (`email`),
  ADD KEY `ip` (`ip`);

--
-- Indexes for table `oc_customer_online`
--
ALTER TABLE `oc_customer_online`
  ADD PRIMARY KEY (`ip`);

--
-- Indexes for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  ADD PRIMARY KEY (`customer_reward_id`);

--
-- Indexes for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  ADD PRIMARY KEY (`customer_search_id`);

--
-- Indexes for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  ADD PRIMARY KEY (`customer_transaction_id`);

--
-- Indexes for table `oc_customer_wishlist`
--
ALTER TABLE `oc_customer_wishlist`
  ADD PRIMARY KEY (`customer_id`,`product_id`);

--
-- Indexes for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  ADD PRIMARY KEY (`custom_field_id`);

--
-- Indexes for table `oc_custom_field_customer_group`
--
ALTER TABLE `oc_custom_field_customer_group`
  ADD PRIMARY KEY (`custom_field_id`,`customer_group_id`);

--
-- Indexes for table `oc_custom_field_description`
--
ALTER TABLE `oc_custom_field_description`
  ADD PRIMARY KEY (`custom_field_id`,`language_id`);

--
-- Indexes for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  ADD PRIMARY KEY (`custom_field_value_id`);

--
-- Indexes for table `oc_custom_field_value_description`
--
ALTER TABLE `oc_custom_field_value_description`
  ADD PRIMARY KEY (`custom_field_value_id`,`language_id`);

--
-- Indexes for table `oc_download`
--
ALTER TABLE `oc_download`
  ADD PRIMARY KEY (`download_id`);

--
-- Indexes for table `oc_download_description`
--
ALTER TABLE `oc_download_description`
  ADD PRIMARY KEY (`download_id`,`language_id`);

--
-- Indexes for table `oc_event`
--
ALTER TABLE `oc_event`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `oc_extension`
--
ALTER TABLE `oc_extension`
  ADD PRIMARY KEY (`extension_id`);

--
-- Indexes for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  ADD PRIMARY KEY (`extension_install_id`);

--
-- Indexes for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  ADD PRIMARY KEY (`extension_path_id`);

--
-- Indexes for table `oc_filter`
--
ALTER TABLE `oc_filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `oc_filter_description`
--
ALTER TABLE `oc_filter_description`
  ADD PRIMARY KEY (`filter_id`,`language_id`);

--
-- Indexes for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  ADD PRIMARY KEY (`filter_group_id`);

--
-- Indexes for table `oc_filter_group_description`
--
ALTER TABLE `oc_filter_group_description`
  ADD PRIMARY KEY (`filter_group_id`,`language_id`);

--
-- Indexes for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  ADD PRIMARY KEY (`geo_zone_id`);

--
-- Indexes for table `oc_information`
--
ALTER TABLE `oc_information`
  ADD PRIMARY KEY (`information_id`);

--
-- Indexes for table `oc_information_description`
--
ALTER TABLE `oc_information_description`
  ADD PRIMARY KEY (`information_id`,`language_id`);

--
-- Indexes for table `oc_information_to_layout`
--
ALTER TABLE `oc_information_to_layout`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_information_to_store`
--
ALTER TABLE `oc_information_to_store`
  ADD PRIMARY KEY (`information_id`,`store_id`);

--
-- Indexes for table `oc_language`
--
ALTER TABLE `oc_language`
  ADD PRIMARY KEY (`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_layout`
--
ALTER TABLE `oc_layout`
  ADD PRIMARY KEY (`layout_id`);

--
-- Indexes for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  ADD PRIMARY KEY (`layout_module_id`);

--
-- Indexes for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  ADD PRIMARY KEY (`layout_route_id`);

--
-- Indexes for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  ADD PRIMARY KEY (`length_class_id`);

--
-- Indexes for table `oc_length_class_description`
--
ALTER TABLE `oc_length_class_description`
  ADD PRIMARY KEY (`length_class_id`,`language_id`);

--
-- Indexes for table `oc_location`
--
ALTER TABLE `oc_location`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indexes for table `oc_manufacturer_to_store`
--
ALTER TABLE `oc_manufacturer_to_store`
  ADD PRIMARY KEY (`manufacturer_id`,`store_id`);

--
-- Indexes for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  ADD PRIMARY KEY (`marketing_id`);

--
-- Indexes for table `oc_modification`
--
ALTER TABLE `oc_modification`
  ADD PRIMARY KEY (`modification_id`);

--
-- Indexes for table `oc_module`
--
ALTER TABLE `oc_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `oc_option`
--
ALTER TABLE `oc_option`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `oc_option_description`
--
ALTER TABLE `oc_option_description`
  ADD PRIMARY KEY (`option_id`,`language_id`);

--
-- Indexes for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  ADD PRIMARY KEY (`option_value_id`);

--
-- Indexes for table `oc_option_value_description`
--
ALTER TABLE `oc_option_value_description`
  ADD PRIMARY KEY (`option_value_id`,`language_id`);

--
-- Indexes for table `oc_order`
--
ALTER TABLE `oc_order`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  ADD PRIMARY KEY (`order_history_id`);

--
-- Indexes for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  ADD PRIMARY KEY (`order_option_id`);

--
-- Indexes for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  ADD PRIMARY KEY (`order_product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  ADD PRIMARY KEY (`order_recurring_id`);

--
-- Indexes for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  ADD PRIMARY KEY (`order_recurring_transaction_id`);

--
-- Indexes for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  ADD PRIMARY KEY (`order_shipment_id`);

--
-- Indexes for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  ADD PRIMARY KEY (`order_status_id`,`language_id`);

--
-- Indexes for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  ADD PRIMARY KEY (`order_total_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  ADD PRIMARY KEY (`order_voucher_id`);

--
-- Indexes for table `oc_product`
--
ALTER TABLE `oc_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `oc_product_attribute`
--
ALTER TABLE `oc_product_attribute`
  ADD PRIMARY KEY (`product_id`,`attribute_id`,`language_id`);

--
-- Indexes for table `oc_product_description`
--
ALTER TABLE `oc_product_description`
  ADD PRIMARY KEY (`product_id`,`language_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  ADD PRIMARY KEY (`product_discount_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_filter`
--
ALTER TABLE `oc_product_filter`
  ADD PRIMARY KEY (`product_id`,`filter_id`);

--
-- Indexes for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  ADD PRIMARY KEY (`product_image_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  ADD PRIMARY KEY (`product_option_id`);

--
-- Indexes for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  ADD PRIMARY KEY (`product_option_value_id`);

--
-- Indexes for table `oc_product_recurring`
--
ALTER TABLE `oc_product_recurring`
  ADD PRIMARY KEY (`product_id`,`recurring_id`,`customer_group_id`);

--
-- Indexes for table `oc_product_related`
--
ALTER TABLE `oc_product_related`
  ADD PRIMARY KEY (`product_id`,`related_id`);

--
-- Indexes for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  ADD PRIMARY KEY (`product_reward_id`);

--
-- Indexes for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  ADD PRIMARY KEY (`product_special_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_product_to_category`
--
ALTER TABLE `oc_product_to_category`
  ADD PRIMARY KEY (`product_id`,`category_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `oc_product_to_download`
--
ALTER TABLE `oc_product_to_download`
  ADD PRIMARY KEY (`product_id`,`download_id`);

--
-- Indexes for table `oc_product_to_layout`
--
ALTER TABLE `oc_product_to_layout`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_product_to_store`
--
ALTER TABLE `oc_product_to_store`
  ADD PRIMARY KEY (`product_id`,`store_id`);

--
-- Indexes for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  ADD PRIMARY KEY (`recurring_id`);

--
-- Indexes for table `oc_recurring_description`
--
ALTER TABLE `oc_recurring_description`
  ADD PRIMARY KEY (`recurring_id`,`language_id`);

--
-- Indexes for table `oc_return`
--
ALTER TABLE `oc_return`
  ADD PRIMARY KEY (`return_id`);

--
-- Indexes for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  ADD PRIMARY KEY (`return_action_id`,`language_id`);

--
-- Indexes for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  ADD PRIMARY KEY (`return_history_id`);

--
-- Indexes for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  ADD PRIMARY KEY (`return_reason_id`,`language_id`);

--
-- Indexes for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  ADD PRIMARY KEY (`return_status_id`,`language_id`);

--
-- Indexes for table `oc_review`
--
ALTER TABLE `oc_review`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  ADD PRIMARY KEY (`seo_url_id`),
  ADD KEY `query` (`query`),
  ADD KEY `keyword` (`keyword`);

--
-- Indexes for table `oc_session`
--
ALTER TABLE `oc_session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `oc_setting`
--
ALTER TABLE `oc_setting`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oc_shipping_courier`
--
ALTER TABLE `oc_shipping_courier`
  ADD PRIMARY KEY (`shipping_courier_id`);

--
-- Indexes for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  ADD PRIMARY KEY (`statistics_id`);

--
-- Indexes for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  ADD PRIMARY KEY (`stock_status_id`,`language_id`);

--
-- Indexes for table `oc_store`
--
ALTER TABLE `oc_store`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  ADD PRIMARY KEY (`tax_class_id`);

--
-- Indexes for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  ADD PRIMARY KEY (`tax_rate_id`);

--
-- Indexes for table `oc_tax_rate_to_customer_group`
--
ALTER TABLE `oc_tax_rate_to_customer_group`
  ADD PRIMARY KEY (`tax_rate_id`,`customer_group_id`);

--
-- Indexes for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  ADD PRIMARY KEY (`tax_rule_id`);

--
-- Indexes for table `oc_theme`
--
ALTER TABLE `oc_theme`
  ADD PRIMARY KEY (`theme_id`);

--
-- Indexes for table `oc_translation`
--
ALTER TABLE `oc_translation`
  ADD PRIMARY KEY (`translation_id`);

--
-- Indexes for table `oc_upload`
--
ALTER TABLE `oc_upload`
  ADD PRIMARY KEY (`upload_id`);

--
-- Indexes for table `oc_user`
--
ALTER TABLE `oc_user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  ADD PRIMARY KEY (`voucher_id`);

--
-- Indexes for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  ADD PRIMARY KEY (`voucher_history_id`);

--
-- Indexes for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  ADD PRIMARY KEY (`voucher_theme_id`);

--
-- Indexes for table `oc_voucher_theme_description`
--
ALTER TABLE `oc_voucher_theme_description`
  ADD PRIMARY KEY (`voucher_theme_id`,`language_id`);

--
-- Indexes for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  ADD PRIMARY KEY (`weight_class_id`);

--
-- Indexes for table `oc_weight_class_description`
--
ALTER TABLE `oc_weight_class_description`
  ADD PRIMARY KEY (`weight_class_id`,`language_id`);

--
-- Indexes for table `oc_zone`
--
ALTER TABLE `oc_zone`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  ADD PRIMARY KEY (`zone_to_geo_zone_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_address`
--
ALTER TABLE `oc_address`
  MODIFY `address_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_api`
--
ALTER TABLE `oc_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_api_ip`
--
ALTER TABLE `oc_api_ip`
  MODIFY `api_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_api_session`
--
ALTER TABLE `oc_api_session`
  MODIFY `api_session_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_attribute`
--
ALTER TABLE `oc_attribute`
  MODIFY `attribute_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `oc_attribute_group`
--
ALTER TABLE `oc_attribute_group`
  MODIFY `attribute_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `oc_banner`
--
ALTER TABLE `oc_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_banner_image`
--
ALTER TABLE `oc_banner_image`
  MODIFY `banner_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215;
--
-- AUTO_INCREMENT for table `oc_cart`
--
ALTER TABLE `oc_cart`
  MODIFY `cart_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT for table `oc_country`
--
ALTER TABLE `oc_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;
--
-- AUTO_INCREMENT for table `oc_coupon`
--
ALTER TABLE `oc_coupon`
  MODIFY `coupon_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_coupon_history`
--
ALTER TABLE `oc_coupon_history`
  MODIFY `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_coupon_product`
--
ALTER TABLE `oc_coupon_product`
  MODIFY `coupon_product_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_currency`
--
ALTER TABLE `oc_currency`
  MODIFY `currency_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_customer`
--
ALTER TABLE `oc_customer`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_customer_activity`
--
ALTER TABLE `oc_customer_activity`
  MODIFY `customer_activity_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_approval`
--
ALTER TABLE `oc_customer_approval`
  MODIFY `customer_approval_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_group`
--
ALTER TABLE `oc_customer_group`
  MODIFY `customer_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_customer_history`
--
ALTER TABLE `oc_customer_history`
  MODIFY `customer_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_ip`
--
ALTER TABLE `oc_customer_ip`
  MODIFY `customer_ip_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_login`
--
ALTER TABLE `oc_customer_login`
  MODIFY `customer_login_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_customer_reward`
--
ALTER TABLE `oc_customer_reward`
  MODIFY `customer_reward_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_search`
--
ALTER TABLE `oc_customer_search`
  MODIFY `customer_search_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_customer_transaction`
--
ALTER TABLE `oc_customer_transaction`
  MODIFY `customer_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field`
--
ALTER TABLE `oc_custom_field`
  MODIFY `custom_field_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_custom_field_value`
--
ALTER TABLE `oc_custom_field_value`
  MODIFY `custom_field_value_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_download`
--
ALTER TABLE `oc_download`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_event`
--
ALTER TABLE `oc_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `oc_extension`
--
ALTER TABLE `oc_extension`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `oc_extension_install`
--
ALTER TABLE `oc_extension_install`
  MODIFY `extension_install_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_extension_path`
--
ALTER TABLE `oc_extension_path`
  MODIFY `extension_path_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_filter`
--
ALTER TABLE `oc_filter`
  MODIFY `filter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `oc_filter_group`
--
ALTER TABLE `oc_filter_group`
  MODIFY `filter_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_geo_zone`
--
ALTER TABLE `oc_geo_zone`
  MODIFY `geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `oc_information`
--
ALTER TABLE `oc_information`
  MODIFY `information_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `oc_language`
--
ALTER TABLE `oc_language`
  MODIFY `language_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_layout`
--
ALTER TABLE `oc_layout`
  MODIFY `layout_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `oc_layout_module`
--
ALTER TABLE `oc_layout_module`
  MODIFY `layout_module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=765;
--
-- AUTO_INCREMENT for table `oc_layout_route`
--
ALTER TABLE `oc_layout_route`
  MODIFY `layout_route_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `oc_length_class`
--
ALTER TABLE `oc_length_class`
  MODIFY `length_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_location`
--
ALTER TABLE `oc_location`
  MODIFY `location_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_manufacturer`
--
ALTER TABLE `oc_manufacturer`
  MODIFY `manufacturer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `oc_marketing`
--
ALTER TABLE `oc_marketing`
  MODIFY `marketing_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_modification`
--
ALTER TABLE `oc_modification`
  MODIFY `modification_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_module`
--
ALTER TABLE `oc_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `oc_option`
--
ALTER TABLE `oc_option`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `oc_option_value`
--
ALTER TABLE `oc_option_value`
  MODIFY `option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `oc_order`
--
ALTER TABLE `oc_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_order_history`
--
ALTER TABLE `oc_order_history`
  MODIFY `order_history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oc_order_option`
--
ALTER TABLE `oc_order_option`
  MODIFY `order_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_product`
--
ALTER TABLE `oc_order_product`
  MODIFY `order_product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oc_order_recurring`
--
ALTER TABLE `oc_order_recurring`
  MODIFY `order_recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_recurring_transaction`
--
ALTER TABLE `oc_order_recurring_transaction`
  MODIFY `order_recurring_transaction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_shipment`
--
ALTER TABLE `oc_order_shipment`
  MODIFY `order_shipment_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_order_status`
--
ALTER TABLE `oc_order_status`
  MODIFY `order_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `oc_order_total`
--
ALTER TABLE `oc_order_total`
  MODIFY `order_total_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_order_voucher`
--
ALTER TABLE `oc_order_voucher`
  MODIFY `order_voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product`
--
ALTER TABLE `oc_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `oc_product_discount`
--
ALTER TABLE `oc_product_discount`
  MODIFY `product_discount_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_product_image`
--
ALTER TABLE `oc_product_image`
  MODIFY `product_image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2785;
--
-- AUTO_INCREMENT for table `oc_product_option`
--
ALTER TABLE `oc_product_option`
  MODIFY `product_option_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=307;
--
-- AUTO_INCREMENT for table `oc_product_option_value`
--
ALTER TABLE `oc_product_option_value`
  MODIFY `product_option_value_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=417;
--
-- AUTO_INCREMENT for table `oc_product_reward`
--
ALTER TABLE `oc_product_reward`
  MODIFY `product_reward_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=548;
--
-- AUTO_INCREMENT for table `oc_product_special`
--
ALTER TABLE `oc_product_special`
  MODIFY `product_special_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=469;
--
-- AUTO_INCREMENT for table `oc_recurring`
--
ALTER TABLE `oc_recurring`
  MODIFY `recurring_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return`
--
ALTER TABLE `oc_return`
  MODIFY `return_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return_action`
--
ALTER TABLE `oc_return_action`
  MODIFY `return_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_return_history`
--
ALTER TABLE `oc_return_history`
  MODIFY `return_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_return_reason`
--
ALTER TABLE `oc_return_reason`
  MODIFY `return_reason_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `oc_return_status`
--
ALTER TABLE `oc_return_status`
  MODIFY `return_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `oc_review`
--
ALTER TABLE `oc_review`
  MODIFY `review_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `oc_seo_url`
--
ALTER TABLE `oc_seo_url`
  MODIFY `seo_url_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=873;
--
-- AUTO_INCREMENT for table `oc_setting`
--
ALTER TABLE `oc_setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=215342;
--
-- AUTO_INCREMENT for table `oc_statistics`
--
ALTER TABLE `oc_statistics`
  MODIFY `statistics_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `oc_stock_status`
--
ALTER TABLE `oc_stock_status`
  MODIFY `stock_status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_store`
--
ALTER TABLE `oc_store`
  MODIFY `store_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_tax_class`
--
ALTER TABLE `oc_tax_class`
  MODIFY `tax_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_tax_rate`
--
ALTER TABLE `oc_tax_rate`
  MODIFY `tax_rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `oc_tax_rule`
--
ALTER TABLE `oc_tax_rule`
  MODIFY `tax_rule_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `oc_theme`
--
ALTER TABLE `oc_theme`
  MODIFY `theme_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_translation`
--
ALTER TABLE `oc_translation`
  MODIFY `translation_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_upload`
--
ALTER TABLE `oc_upload`
  MODIFY `upload_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_user`
--
ALTER TABLE `oc_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `oc_user_group`
--
ALTER TABLE `oc_user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `oc_voucher`
--
ALTER TABLE `oc_voucher`
  MODIFY `voucher_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_history`
--
ALTER TABLE `oc_voucher_history`
  MODIFY `voucher_history_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oc_voucher_theme`
--
ALTER TABLE `oc_voucher_theme`
  MODIFY `voucher_theme_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `oc_weight_class`
--
ALTER TABLE `oc_weight_class`
  MODIFY `weight_class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `oc_zone`
--
ALTER TABLE `oc_zone`
  MODIFY `zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4253;
--
-- AUTO_INCREMENT for table `oc_zone_to_geo_zone`
--
ALTER TABLE `oc_zone_to_geo_zone`
  MODIFY `zone_to_geo_zone_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
