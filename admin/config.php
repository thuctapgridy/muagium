<?php
// HTTP
define('HTTP_SERVER', 'http://113.176.163.102/muagium/admin/');
define('HTTP_CATALOG', 'http://113.176.163.102/muagium/');

// HTTPS
define('HTTPS_SERVER', 'http://113.176.163.102/muagium/admin/');
define('HTTPS_CATALOG', 'http://113.176.163.102/muagium/');

// DIR
define('DIR_APPLICATION', 'C:\xampp\htdocs\muagium/admin/');
define('DIR_SYSTEM', 'C:\xampp\htdocs\muagium/system/');
define('DIR_IMAGE', 'C:\xampp\htdocs\muagium/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', 'C:\xampp\htdocs\muagium/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'muagium');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
